import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/remote-config"

import amplitude from "amplitude-js"
import { Provider } from "mobx-react"
import { IStateTreeNode } from "mobx-state-tree"
import { extendTheme, NativeBaseProvider } from "native-base"
import { contains } from "ramda"
import React, { Component, createContext, Suspense, useContext } from "react"
import {
  AppState,
  BackHandler,
  Linking,
  LogBox,
  NativeEventSubscription,
  NativeModules,
  Platform,
} from "react-native"
import OrientationLocker from "react-native-orientation-locker"
import { initialWindowSafeAreaInsets, SafeAreaProvider } from "react-native-safe-area-context"
import { mst } from "reactotron-mst"
import Reactotron, {
  default as reactotron,
  networking,
  openInEditor,
} from "reactotron-react-native"
import { TamaguiProvider } from "tamagui"

import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { NavigationContainerRef } from "@react-navigation/native"

import getTheme from "./native-base-theme/components"
import customVariables from "./native-base-theme/variables/variables"
import { exitRoutes, RootNavigation, RootNavigator, setRootNavigation } from "./navigation"
import getActiveRouteName from "./navigation/get-active-routename"
import { SWRProvider } from "./src/providers/swr-provider"
import { stores } from "./stores"
import config from "./tamagui.config"
import { styles } from "./theme"

LogBox.ignoreLogs(["Warning: ..."])
LogBox.ignoreAllLogs()
/**
 * Are we allowed to exit the app?  This is called when the back button
 * is pressed on android.
 *
 * @param routeName The currently active route name.
 */
const canExit = (routeName: string) => contains(routeName, exitRoutes)

export const NAVIGATION_PERSISTENCE_KEY = "NAVIGATION_STATE"

// start with the bare reactotron type definition
type VanillaReactotron = typeof reactotron

// extend the type with features bestowed by plugins
export interface Reactotron extends VanillaReactotron {
  trackMstNode: (mstNode: IStateTreeNode, name?: string) => any
  // Add any extra plugins here
}

// Teach TypeScript about the bad things we want to do.
declare global {
  interface Console {
    /**
     * Hey, it's Reactotron if we're in dev, and no-ops if we're in prod.
     */
    tron: Reactotron
  }
}

/** Do Nothing. */
const noop = () => undefined

let scriptHostname: any
if (__DEV__) {
  const scriptURL = NativeModules.SourceCode.scriptURL
  scriptHostname = scriptURL.split("://")[1].split(":")[0]

  const RX =
    "@APPLY_SNAPSHOT,setDurRes,addDrugToTakeId,setPrescriptionDetailsCount,clearDrugToTakeIds"
  const filter = (event: any) => RX.includes(event.name) === false

  Reactotron // Android real device 에서 로그 정보를 확인하기 위해서는 개발PC의 IP 주소를 세팅해야 함. (https://github.com/infinitered/reactotron/issues/803)
    // .configure({host: scriptHostname})
    .configure({ host: "192.168.0.242" }) // controls connection & communication settings
    // .configure()
    .setAsyncStorageHandler(AsyncStorage)
    .useReactNative()
    .use(networking())
    .use(openInEditor())
    .use(mst({ filter }))
    .connect() // let's connect!

  // Let's clear Reactotron on every time we load the app
  Reactotron.clear()

  // Totally hacky, but this allows you to not both importing reactotron-react-native
  // on every file.  This is just DEV mode, so no big deal.
  console.tron = Reactotron
  // console.tron.trackMstNode(stores.prescriptionStore)
  console.tron.trackMstNode(stores.takingDrugStore)
  // console.tron.trackMstNode(stores.accountStore)
  console.tron.logImportant("< App > Hostname: ", scriptHostname)

  firebase.analytics().setAnalyticsCollectionEnabled(false)
  firebase.remoteConfig().setConfigSettings({ isDeveloperModeEnabled: true })
} else {
  // attach a mock so if things sneaky by our __DEV__ guards, we won't crash.
  console.tron = {
    benchmark: noop,
    clear: noop,
    close: noop,
    configure: noop,
    connect: noop,
    display: noop,
    error: noop,
    image: noop,
    log: noop,
    logImportant: noop,
    overlay: noop,
    reportError: noop,
    use: noop,
    useReactNative: noop,
    warn: noop,
    trackMstNode: noop,
  }
}

/**
 * Create a context we can use to
 * - Provide access to our stores from our root component
 * - Consume stores in our screens (or other components, though it's
 *   preferable to just connect screens)
 */
const RootStoreContext = createContext(stores)

/**
 * A hook that screens can use to gain access to our stores, with
 * `const { someStore, someOtherStore } = useStores()`,
 * or less likely: `const rootStore = useStores()`
 */
export const useStores = () => useContext(RootStoreContext)

interface DataMessage {
  purpose: string // "presc", "appUpdate", "webview", "dosing", "promo", "openScreen"
  url?: string
  id?: string
  content?: string
  name?: string
  params?: any
  record_id: string
}

const toastConfig = {
  success: ({ text1, text2, props, ...rest }) => (
    <BaseToast
      {...rest}
      style={styles.TOAST_VIEW}
      contentContainerStyle={{
        marginLeft: 10,
        marginRight: 10,
      }}
      trailingIcon={null}
      text1NumberOfLines={2}
      text2NumberOfLines={5}
      text1Style={styles.TOAST_TEXT}
      text2Style={styles.TOAST_TEXT2}
      text1={text1}
      text2={text2}
    />
  ),
}

interface Props {}

interface State {
  isLoadingComplete: boolean
  appState: any
}

export default class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      isLoadingComplete: false,
      appState: AppState.currentState,
    }
  }

  removeNotificationListener: any
  removeNotificationOpenedListener: any
  changeEventListener: NativeEventSubscription
  pinCodeEnabled: undefined | string
  navigationRef = React.createRef<NavigationContainerRef>()

  async componentDidMount() {
    // 앱 세로모드 고정
    OrientationLocker.lockToPortrait()

    // this._listenForNotifications()
    this.backgroudPushWakeup()

    // 앱이 종료된 상태에서 메시지를 받을 경우 실행됨
    firebase
      .messaging()
      .getInitialNotification()
      .then(async (notificationOpen) => {
        if (notificationOpen) {
          console.tron.logImportant(
            "< App > getInitialNotification, notificationOpen: ",
            notificationOpen,
          )

          const checkHomeComplete = setInterval(async () => {
            const status = await AsyncStorage.getItem("HomeLoading")
            if (status === "OK") {
              await clearInterval(checkHomeComplete)
              await AsyncStorage.setItem("HomeLoading", "NO")
              setTimeout(async () => {
                await this.parseDataMessage(notificationOpen.data)
              }, 1000)
            }
          }, 3000)
        }
      })

    this.pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")
    this.changeEventListener = AppState.addEventListener("change", this._handleAppStateChange)
    amplitude.getInstance().init("0e890fb0f2c2f759a8d71448d290d7c9")
    setRootNavigation(this.navigationRef)
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress)
  }

  componentWillUnmount() {
    // this.removeNotificationListener()
    this.removeNotificationOpenedListener()
    this.changeEventListener.remove()
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress)
  }

  backgroudPushWakeup = () => {
    // 앱이 백그라운드에 있는 상태에서 메시지를 받는 경우 실행됨
    this.removeNotificationOpenedListener = firebase
      .messaging()
      .onNotificationOpenedApp((notificationOpen) => {
        console.tron.log("< App > onNotificationOpened, notificationOpen: ", notificationOpen)

        const checkHomeComplete = setInterval(async () => {
          const status = await AsyncStorage.getItem("HomeLoading")
          if (status === "OK") {
            await clearInterval(checkHomeComplete)
            await AsyncStorage.setItem("HomeLoading", "NO")
            await this.parseDataMessage(notificationOpen.data)
          }
        }, 1000)
      })
  }

  // We'll fire this when the back button is pressed on Android.
  onBackPress = () => {
    const navigation = this.navigationRef.current
    const routeName = getActiveRouteName(navigation.getRootState())

    if (navigation == null) {
      return false
    }

    if (canExit(routeName)) {
      BackHandler.exitApp()
      return true
    }

    if (navigation.canGoBack()) {
      navigation.goBack()
      return true
    }

    return false
  }

  render() {
    const newColorTheme = {
      // primary: "#F24A10",
      primary: {
        50: "#F24A10",
        100: "#F24A10",
        200: "#F24A10",
        300: "#F24A10",
        400: "#F24A10",
        500: "#F24A10",
        600: "#F24A10",
        700: "#F24A10",
        800: "#F24A10",
        900: "#F24A10",
      },
    }
    const theme = extendTheme({
      colors: newColorTheme,
      fontConfig: {
        Roboto: {
          100: {
            normal: Platform.select({
              ios: "NotoSansKR-Light",
              android: "NotoSansKR-Light-Hestia",
            }),
          },
          200: {
            normal: Platform.select({
              ios: "NotoSansKR-Light",
              android: "NotoSansKR-Light-Hestia",
            }),
          },
          300: {
            normal: Platform.select({
              ios: "NotoSansKR-Light",
              android: "NotoSansKR-Light-Hestia",
            }),
          },
          400: {
            normal: Platform.select({
              ios: "NotoSansKR-Regular",
              android: "NotoSansKR-Regular-Hestia",
            }),
          },
          500: {
            normal: Platform.select({
              ios: "NotoSansKR-Regular",
              android: "NotoSansKR-Regular-Hestia",
            }),
          },
          600: {
            normal: Platform.select({
              ios: "NotoSansKR-Regular",
              android: "NotoSansKR-Regular-Hestia",
            }),
          },
          // Add more variants
          700: {
            normal: Platform.select({
              ios: "NotoSansKR-Bold",
              android: "NotoSansKR-Bold-Hestia",
            }),
          },
          800: {
            normal: Platform.select({
              ios: "NotoSansKR-Bold",
              android: "NotoSansKR-Bold-Hestia",
            }),
          },
          900: {
            normal: Platform.select({
              ios: "NotoSansKR-Bold",
              android: "NotoSansKR-Bold-Hestia",
            }),
          },
        },
      },

      // Make sure values below matches any of the keys in `fontConfig`
      // fonts: {
      //   heading: "Roboto",
      //   body: "Roboto",
      //   mono: "Roboto",
      // },
    })
    // const theme = getTheme(customVariables)

    return (
      <TamaguiProvider config={config} disableInjectCSS defaultTheme="light">
        <NativeBaseProvider theme={theme}>
          <Provider {...stores}>
            <SWRProvider>
              {/* <StyleProvider style={getTheme(customVariables)}> */}
              <SafeAreaProvider initialSafeAreaInsets={initialWindowSafeAreaInsets}>
                {/* if you want nice React 18 concurrent hydration, you'll want Suspense near the root */}
                <RootNavigator ref={this.navigationRef} />
              </SafeAreaProvider>
              {/* </StyleProvider> */}
            </SWRProvider>
          </Provider>
        </NativeBaseProvider>
      </TamaguiProvider>
    )
  }

  _handleAppStateChange = async (nextAppState: string) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active" &&
      !stores.accountStore.temporaryPinCodeDisabled
    ) {
      this.pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")
      console.tron.log("< App > _handleAppStateChange, pinCodeEnabled: " + this.pinCodeEnabled)
      if (this.pinCodeEnabled && JSON.parse(this.pinCodeEnabled)) {
        RootNavigation.navigate("Auth", {
          screen: "CodeVerification",
          params: { purpose: "verification", preRoute: "Root" },
        })
      }
    }
    this.setState({ appState: nextAppState })
  }

  _handleLoadingError = (error: string) => {
    firebase.crashlytics().recordError(400, "App loading error: " + error)
    console.warn(error)
  }

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true })
  }

  /*
   * react-native-firebase v5 -> v6 변경 시 device-local notification 지원 안 되어 주석처리함.
  _listenForNotifications = async () => {
    this.removeNotificationListener = firebase.notifications().onNotification((notification) => {
      Toast.show({
        type: "success",
        text1: notification.title,
        text2:
          notification.body && notification.body.length > 0 ? "\n\n" + notification.body : null,
        visibilityTime: 3500,
        position: "top",
        onPress: () => {
          if (notification.title.includes("브리즈헬러")) {
            RootNavigation.navigate("Auth", {
              screen: "WebView",
              params: { title: "사용법", content: notification.data.content },
            })
          } else if (notification.title.includes("운동")) {
            RootNavigation.navigate("Details", {
              screen: "Activity",
              params: { title: "운동", recordId: notification.data.record_id },
            })
          } else {
            return null
          }
        },
      })

      // Toast.show({
      //   text:
      //     notification.title +
      //     (notification.body && notification.body.length > 0 ? "\n\n" + notification.body : null),
      //   duration: 3500,
      //   position: "center",
      //   textStyle: styles.TOAST_TEXT,
      //   style: styles.TOAST_VIEW,
      //   // buttonText: notification.title && "이동",
      //   // onClose: (reason) => {
      //   //   if (reason === "user") {
      //   //     RootNavigation.navigate("Details", {
      //   //       screen: "WebView",
      //   //       params: { title: "사용법", content: notification.data.content },
      //   //     })
      //   //   }
      //   // },
      // })
    })
  } */

  parseDataMessage(data: DataMessage | { [key: string]: string }) {
    if (data) {
      switch (data.purpose) {
        case "presc":
          if (data.id) {
            RootNavigation.navigate("PrescriptionsStack", {
              screen: "PrescriptionDetail",
              params: { prescription_id: Number(data.id) },
            })
          }
          break

        case "appUpdate":
          if (Platform.OS === "android") {
            Linking.openURL("market://details?id=com.onions.papricacare")
          } else {
            Linking.openURL("itms-apps://apps.apple.com/kr/app/파프리카케어/id1453787406")
          }
          break

        case "webview":
          if (data.url) {
            RootNavigation.navigate("HomeStack", {
              screen: "WebView",
              params: { url: data.url },
            })
          } else if (data.content) {
            RootNavigation.navigate("Auth", {
              screen: "WebView",
              params: { title: "사용법", content: data.content },
            })
          }
          break

        case "openScreen":
          if (data.url == "DrugAdherenceRating") {
            RootNavigation.navigate("HomeStack", {
              screen: data.url,
              params: { prescriptionId: Number(data.id) },
            })
          } else if (data.name) {
            // TODO: DataMessage 로 전달되는 json 은 전부 string 만 가능하여 string 형태의 id 값이 전달되었을 때 문제가 발생할 소지가 있는데 테스트 필요!
            // 참조 문서: https://onions.atlassian.net/wiki/spaces/~795661683/pages/291799138/Push+Data+format#%EC%83%88%EB%A1%9C-%EC%B6%94%EA%B0%80%EB%90%98%EB%8A%94-data-message-format-(2021.01)
            RootNavigation.navigate(data.name, data.params)
          }
          break

        case "VIDEO":
          RootNavigation.navigate("Details", {
            screen: "Activity",
            params: { title: "운동", recordId: data.record_id },
          })
          break

        default:
          break
      }
    }
  }
}
