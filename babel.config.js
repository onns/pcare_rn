// process.env.TAMAGUI_TARGET = "native";
const path = require("path")

module.exports = function (api) {
  api.cache(true)

  const APP_ENV = process.env.APP_ENV || "release"
  const envPath = path.resolve(__dirname, "./", `.env.${APP_ENV}`)

  require("dotenv").config({
    path: envPath,
  })

  return {
    presets: [["babel-preset-expo", { jsxRuntime: "automatic" }]],
    plugins: [
      [
        "inline-dotenv",
        {
          path: envPath,
        },
      ],
      [
        "module-resolver",
        {
          alias: {
            "@api": "./api",
            "@assets": "./assets",
            "@components": "./components",
            "@constants": "./constants",
            "@features": "./src/features",
            "@lib": "./lib",
            "@navigation": "./navigation",
            "@screens": "./screens",
            "@stores": "./stores",
            "@theme": "./theme",
          },
        },
      ],
      [
        "@tamagui/babel-plugin",
        {
          components: ["tamagui"],
          config: "./tamagui.config.ts",
          // logTimings: true,
          // disableExtraction: process.env.NODE_ENV === "development",
        },
      ],
      [
        "transform-inline-environment-variables",
        {
          include: "TAMAGUI_TARGET",
        },
      ],
      "react-native-reanimated/plugin",
    ],
  }
}
