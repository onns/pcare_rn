import { ConfigContext, ExpoConfig } from "@expo/config"

const path = require("path")

const APP_ENV: typeof process.env.APP_ENV = process.env.APP_ENV || "release"
const envPath = path.resolve(__dirname, "./", `.env.${APP_ENV}`)

require("dotenv").config({
  path: envPath,
})

const icon: string = process.env.EXPO_APP_ICON!
const backgroundColor: string = process.env.EXPO_APP_SPLASH_BACKGROUND!

export default ({ config }: ConfigContext): ExpoConfig => ({
  ...config,
  name: process.env.EXPO_APP_NAME!,
  slug: process.env.EXPO_APP_SLUG!,
  owner: process.env.EXPO_APP_OWNER!,
  // version: appVersion,
  icon,
  splash: {
    image: process.env.EXPO_APP_SPLASH_IMAGE,
    resizeMode: "contain",
    backgroundColor,
  },
  android: {
    ...config.android,
    package: process.env.EXPO_APP_ANDROID_PACKAGE,
  },
  ios: {
    ...config.ios,
    bundleIdentifier: process.env.EXPO_APP_IOS_BUNDLE_ID,
  },
  extra: {
    // isUrgentUpdate
  },
})
