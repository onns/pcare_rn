export function formatDate(d: Date | undefined) {
  if (d == undefined) {
    return ""
  }
  let month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear()

  if (month.length < 2) month = "0" + month
  if (day.length < 2) day = "0" + day

  return [year, month, day].join(".")
}

export function formatDateWithJoinChar(d: Date | undefined, joinChar: string): string {
  if (d == undefined) {
    return ""
  }
  let month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear()

  if (month.length < 2) month = "0" + month
  if (day.length < 2) day = "0" + day

  return [year, month, day].join(joinChar)
}

export function getTheDayBefore(d: Date | undefined, joinChar: string) {
  if (d == undefined) {
    return ""
  }
  d.setTime(new Date().getTime() - 1 * 24 * 60 * 60 * 1000) //1일전

  let month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear()

  if (month.length < 2) month = "0" + month
  if (day.length < 2) day = "0" + day

  return [year, month, day].join(joinChar)
}

export function getDayAfter(d: Date | undefined, daysAfter: number, joinChar: string) {
  if (d == undefined) {
    return ""
  }
  d.setTime(new Date().getTime() + daysAfter * 24 * 60 * 60 * 1000)

  let month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear()

  if (month.length < 2) month = "0" + month
  if (day.length < 2) day = "0" + day

  return [year, month, day].join(joinChar)
}

export function getDayBefore(d: Date | undefined, daysAgo: number, joinChar: string) {
  if (d == undefined) {
    return ""
  }
  d.setTime(new Date().getTime() - daysAgo * 24 * 60 * 60 * 1000)

  let month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear()

  if (month.length < 2) month = "0" + month
  if (day.length < 2) day = "0" + day

  return [year, month, day].join(joinChar)
}

export function formatDateKor(d: Date | undefined) {
  if (d == undefined) {
    return ""
  }
  const month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear()

  // if (month.length < 2) month = '0' + month;
  // if (day.length < 2) day = '0' + day;

  return year + "년 " + month + "월 " + day + "일"
}

export function formatDateFromString(d: string | undefined | null) {
  if (d == undefined || d == null) {
    return ""
  }

  return [d.substr(0, 4), parseInt(d.substr(5, 2)), parseInt(d.substr(8, 2))].join(".").concat(".")
}

export function formatDate6DigitsFromString(d: string | undefined | null) {
  if (d == undefined || d == null) {
    return ""
  }

  return [d.substr(2, 2), d.substr(5, 2), d.substr(8, 2)].join(".")
}

export function formatTime(time: string): Array<string> {
  const timeArray = time.split(":")
  let prefix
  let hour = "00",
    min = "00"
  if (timeArray.length > 1) {
    hour = timeArray[0]
    min = timeArray[1]
  }
  let hourNumber = Number(hour)
  if (hourNumber > 12) {
    prefix = "오후 "
    hourNumber -= 12
    if (hourNumber < 10) hour = "0" + hourNumber
    else hour = String(hourNumber)
  } else if (hourNumber == 12) {
    prefix = "오후 "
  } else {
    prefix = "오전 "
  }
  return [prefix, `${hour}:${min}`]
}

export function getTimeString(date: Date): string {
  let hourString, minString
  const hour = date.getHours()
  const min = date.getMinutes()

  if (hour < 10) {
    hourString = "0" + hour
  } else {
    hourString = String(hour)
  }
  // 선택 가능한 시간을 10분 단위로 설정했지만 ios 에서 다이얼을 돌리지 않고 디폴트 시간을 선택 시,
  // 분 단위로 시간 데이터가 전달되는 문제가 있어서 그런 경우에 첫째 자리 숫자를 0으로 바꿈
  if (min < 10) {
    minString = "00"
  } else if (min % 10 == 0) {
    minString = String(min)
  } else {
    minString = `${String(min)[0]}0`
  }
  return `${hourString}:${minString}`
}

export function calcAge(birth: Date): number {
  if (!birth) return -1

  const date = new Date()
  const year = date.getFullYear()
  const day = date.getDate()
  const month = date.getMonth() + 1
  let monthString = month + ""
  let dayString = day + ""
  if (month < 10) monthString = "0" + month
  if (day < 10) dayString = "0" + day
  const monthDay = monthString + dayString

  const birthDay = birth.getDate()
  const birthYear = birth.getFullYear()
  const birthMonth = birth.getMonth() + 1
  let birthMonthString = birthMonth + ""
  let birthDayString = birthDay + ""
  if (birthMonth < 10) birthMonthString = "0" + birthMonth
  if (birthDay < 10) birthDayString = "0" + birthDay
  const birthMonthDay = birthMonthString + birthDayString

  const age = parseInt(monthDay) < parseInt(birthMonthDay) ? year - birthYear - 1 : year - birthYear
  return age
}
