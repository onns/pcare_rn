import { PrescriptionShort } from "@stores/drug-to-take/PrescriptionShort"

export const getPrescriptionTitle = (prescription: PrescriptionShort) => {
  return prescription.disease && prescription.subdiseases && prescription.subdiseases.length >= 1
    ? prescription.disease.rep_title
      ? `${prescription.disease.rep_title} 외 ${prescription.subdiseases.length}`
      : `${prescription.disease.name_kr} 외 ${prescription.subdiseases.length}`
    : // 주질환, 부질환 없을때
    prescription.disease && prescription.subdiseases === null
    ? prescription.disease.rep_title
      ? prescription.disease.rep_title
      : prescription.name_kr
    : // 주질환 X, 부질환 2개 이상일때
    prescription.disease === null &&
      prescription.subdiseases &&
      prescription.subdiseases.length >= 2
    ? prescription.subdiseases[0].rep_title
      ? `${prescription.subdiseases[0].rep_title} 외 ${prescription.subdiseases.length - 1}`
      : `${prescription.subdiseases[0].name_kr} 외 ${prescription.subdiseases.length - 1}`
    : // 주질환 X, 부질환 1개 일때
    prescription.disease === null &&
      prescription.subdiseases &&
      prescription.subdiseases.length === 1
    ? prescription.subdiseases[0].rep_title
      ? prescription.subdiseases[0].rep_title
      : prescription.subdiseases[0].name_kr
    : // 주,부 질환 없을때

      `${prescription.setIssueDateObject().toFormat("M월 d일")} 처방`
}
