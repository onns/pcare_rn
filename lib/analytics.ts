import amplitude from "amplitude-js"

import analytics from "@react-native-firebase/analytics"

export const sendLogEvent = (name: string, params?: any) => {
  if (!__DEV__) {
    amplitude.getInstance().logEvent(name, params)
    analytics().logEvent(name, params)
  }
}

export const sendLogScreenView = (params: { screenClass: string; screenName: string }) => {
  if (!__DEV__) {
    analytics().logScreenView({
      screen_class: params.screenClass,
      screen_name: params.screenName,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: params.screenName })
  }
}
