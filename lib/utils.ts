import { Platform } from "react-native"

const isAndroid = Platform.OS === "android"

export const addComma = (num: number) =>
  isAndroid
    ? Number(num)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    : Number(num).toLocaleString()
