import { snapshot } from "valtio"

import { POINT_TYPES, POINTS_BY_TYPE } from "./constants"
import { store } from "./stores/total-points"

export const validatePoints = (
  pointType: number,
  drugTakeIds?: number[],
): { validPoints: number; validDrugTakeIds?: number[] } => {
  const pointStore = snapshot(store)

  // 중복된 복약 체크인지 확인
  const validDrugTakeIds: number[] = []
  if (drugTakeIds) {
    for (let i = 0; i < drugTakeIds.length; i++) {
      const drugTakeId = drugTakeIds[i]
      const found = pointStore.activities.find((item) =>
        item.drug_take_id?.includes(String(drugTakeId)),
      )
      const found2 = pointStore.todayMedication?.find((item) =>
        item.drug_take_id?.includes(String(drugTakeId)),
      )
      if (!(found || found2)) validDrugTakeIds.push(drugTakeId)
    }
    if (validDrugTakeIds.length === 0) return { validPoints: 0 }
  }

  const newPoints =
    validDrugTakeIds.length > 1 // 모두 복약 체크한 경우, 체크한 약 개수 확인
      ? validDrugTakeIds.length * POINTS_BY_TYPE.get(pointType)
      : POINTS_BY_TYPE.get(pointType)

  // 복약체크 하루 최대 30원까지 포인트 누적 허용
  let pointsToChange = pointStore.todayMedicationPoints
  if (pointType === POINT_TYPES.drugTaken) {
    if (pointsToChange < 30) {
      pointsToChange += newPoints

      if (pointsToChange > 30) {
        return { validPoints: newPoints - (30 - pointsToChange), validDrugTakeIds }
      }
    } else {
      return { validPoints: 0 }
    }
  } else if (pointType === POINT_TYPES.prescriptionRegistered) {
    // 처방전 등록 하루 최대 2회
    if (pointStore.todayPrescriptionRegCount >= 2) {
      return { validPoints: 0 }
    }
  }

  return { validPoints: newPoints, validDrugTakeIds }
}
