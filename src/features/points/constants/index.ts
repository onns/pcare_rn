export const POINT_TYPES = {
  firstPrescriptionRegistered: 1,
  prescriptionRegistered: 2,
  drugTaken: 3,
  allDrugTakenPerDay: 4,
}

export const POINTS_BY_TYPE = new Map<number, number>([
  [1, 1000],
  [2, 50],
  [3, 2],
  [4, 20],
])
