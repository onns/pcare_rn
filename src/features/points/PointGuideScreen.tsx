/* eslint-disable react/display-name */
import React, { useEffect } from "react"
import { Image, LinearGradient, Stack, styled, Text, XStack, YStack } from "tamagui"

import CloseButton from "@components/CloseButton"
import { Screen } from "@components/screen/screen"
import { DetailsStackParamList, RootStackParamList } from "@navigation/types"
import { CompositeNavigationProp, useNavigation } from "@react-navigation/native"
import { color } from "@theme/color"

import SvgCrown from "./images/crown.svg"
import SvgInfo from "./images/info.svg"
import SvgPill from "./images/pill.svg"
import SvgPrescription from "./images/prescription.svg"

import type { StackNavigationProp } from "@react-navigation/stack"

const imageMainSource = require("./images/point-top.png")

const Paragraph = styled(Text, {
  fow: "400",
  fos: 15,
  lh: 21,
  ls: -0.3,
  col: "#1B1E25",
})
const ParagraphSmall = styled(Text, {
  fow: "400",
  fos: 14,
  lh: 20,
  ls: -0.3,
  col: "#565E6D",
})
const TextCondition = styled(Text, {
  fow: "700",
  fos: 19,
  lh: 25,
  ls: -0.5,
  col: "#1B1E25",
})
const TextWon = styled(Text, {
  fow: "800",
  fos: 23,
  lh: 30,
  ls: -0.5,
  col: "#FC630E",
})

type NavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "PointGuide">,
  StackNavigationProp<RootStackParamList>
>

export function PointGuideScreen() {
  const navigation = useNavigation<NavigationProp>()

  useEffect(() => {
    // Use `setOptions` to update the button that we previously specified
    // Now the button includes an `onPress` handler to update the count
    navigation.setOptions({
      headerRight: () => (
        <CloseButton
          onPress={() => {
            // 현재 navigator(Details) 에 PointGuide 만 mount 된 경우 'Main' navigator 로 이동
            if (navigation.getState().routes.length > 1 && navigation.canGoBack()) {
              navigation.goBack()
            } else {
              navigation.pop()
              navigation.navigate("Main")
            }
          }}
          style={{ marginRight: 12 }}
        />
      ),
    })
  }, [navigation])

  return (
    <Screen preset="scroll" backgroundColor="white">
      <LinearGradient h={1070} colors={["#FFFCFB", "#FFFFFF"]} start={[0, 1]} end={[0, 0]}>
        <YStack px={24}>
          <Image width={240} height={240} src={imageMainSource} mt={23} mb={1} alignSelf="center" />
          <Text fow="700" fos={27} lh={35} ls={-0.5} col="#1B1E25" ta="center">
            건강 활동을 기록하고{"\n"} 포인트를 획득 해보세요
          </Text>
          <Stack h={32} my={8} />
          <XStack py={10}>
            <ParagraphSmall mr={8}>시범운영 기간동안 </ParagraphSmall>
            <ParagraphSmall>4월 30일까지</ParagraphSmall>
          </XStack>

          <XStack jc="space-between" ai="center" py={10}>
            <XStack>
              <SvgPrescription />
              <YStack jc="center" ml={16}>
                <TextCondition mb={4}>첫 처방전 등록 시</TextCondition>
                <ParagraphSmall>시범운영 기간 내 첫 처방전</ParagraphSmall>
              </YStack>
            </XStack>
            <TextWon>1,000원</TextWon>
          </XStack>

          <XStack jc="space-between" ai="center" py={10}>
            <XStack>
              <SvgPrescription />
              <YStack jc="center" ml={16}>
                <TextCondition mb={4}>처방전 등록 시</TextCondition>
                <ParagraphSmall>하루 최대 2회</ParagraphSmall>
              </YStack>
            </XStack>
            <TextWon>50원</TextWon>
          </XStack>

          <XStack jc="space-between" ai="center" py={10}>
            <XStack>
              <SvgPill />
              <YStack jc="center" ml={16}>
                <TextCondition mb={4}>매 복약 시</TextCondition>
                <ParagraphSmall numberOfLines={2}>
                  처방전으로 생성한 복약{"\n"}(일 최대 30원)
                </ParagraphSmall>
              </YStack>
            </XStack>
            <TextWon>2원</TextWon>
          </XStack>

          <XStack bc={color.surface.neutralSubded} p={16} br={16} mt={16} mb={37}>
            <SvgInfo />
            <Paragraph f={1} ml={8} numberOfLines={2}>
              중복 처방전을 등록시 획득된 포인트가 차감 되실 수 있습니다.
            </Paragraph>
          </XStack>
        </YStack>

        <Stack bc={color.surface.neutralDefault} py={24} px={28} pb={104}>
          <Text fow="500" fos={15} lh={18} ls={-0.3} mb={16}>
            포인트 사용 안내
          </Text>
          <Text>
            • 3000원 부터 현금으로 환전이 가능합니다. {"\n"}• 시범운영 기간 종료시 남은 포인트는
            모두 현금으로 전환하여 지급됩니다.
          </Text>
        </Stack>
      </LinearGradient>
    </Screen>
  )
}
