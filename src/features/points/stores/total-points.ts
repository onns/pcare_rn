/* eslint-disable camelcase */
import { proxy } from "valtio"

type Activity = {
  created_at: string | null
  desc: string
  drug_take_id: string | null
  id: number
  p_id: number
  point: number
  point_type: number
  timeslot_id: number | null
  user_id: number
  alarm_name: string | null
}

type Store = {
  isTodayMedicationPointsLoaded: boolean
  todayMedicationPoints: number | undefined
  todayPrescriptionRegCount: number | undefined
  // eslint-disable-next-line camelcase
  todayMedication: Array<{ point: number; drug_take_id: string | null }> | undefined
  totalPoints: number | undefined
  activities: Array<Activity>
}

export const store = proxy<Store>({
  isTodayMedicationPointsLoaded: false,
  todayMedicationPoints: undefined,
  todayPrescriptionRegCount: undefined,
  todayMedication: undefined,
  totalPoints: undefined,
  activities: [],
})

export const setActivities = (activities: Array<Activity>) => {
  store.activities = activities
}

/** api 조회 후 세팅 */
export const setMedicationPoints = (points: number) => {
  store.todayMedicationPoints = points
  store.isTodayMedicationPointsLoaded = true
}
export const addTodayMedication = (medication: { point: number; drug_take_id: string | null }) => {
  store.todayMedicationPoints += medication.point
  store.todayMedication.push(medication)
}

// eslint-disable-next-line camelcase
export const setTodayMedication = (data: Array<{ point: number; drug_take_id: string | null }>) => {
  store.todayMedication = data
}

export const setPrescriptionRegCount = (count: number) => {
  store.todayPrescriptionRegCount = count
}
export const incPrescriptionRegCount = () => {
  store.todayPrescriptionRegCount += 1
}

export const setTotalPoints = (points: number) => {
  store.totalPoints = points
}
