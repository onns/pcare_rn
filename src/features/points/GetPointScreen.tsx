import { StatusBar } from "native-base"
import React, { useEffect } from "react"
import { Platform } from "react-native"
import ConfettiCannon from "react-native-confetti-cannon"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { Button, Image, LinearGradient, styled, Text, useWindowDimensions, YStack } from "tamagui"

import { supabase } from "@api/supabase"
import { Screen } from "@components/screen/screen"
import { addComma } from "@lib/utils"
import { DetailsStackParamList } from "@navigation/types"
import remoteConfig from "@react-native-firebase/remote-config"
import { StackScreenProps } from "@react-navigation/stack"
import { AccountStore } from "@stores/AccountStore"

import { useStores } from "../../../App"
import { POINT_TYPES } from "./constants"
import { addTodayMedication, incPrescriptionRegCount, store } from "./stores/total-points"

const imageMainSource = require("./images/point-top.png")

const getReasonForPoint = (pointType: number) => {
  switch (pointType) {
    case POINT_TYPES.firstPrescriptionRegistered:
      return "첫 처방전 등록으로"

    case POINT_TYPES.prescriptionRegistered:
      return "처방전 등록으로"

    case POINT_TYPES.drugTaken:
      return "복약 완료로"

    case POINT_TYPES.allDrugTakenPerDay:
      return "모두 복약 완료로"

    default:
      break
  }
}

type Props = StackScreenProps<DetailsStackParamList, "GetPoint", "Details">

export function GetPointScreen({ navigation, route }: Props) {
  const { top, bottom } = useSafeAreaInsets()
  const { height } = useWindowDimensions()

  const { accountStore }: { accountStore: AccountStore } = useStores()
  const { point, pointType, description, pid, drugTakeIds, alarmName } = route.params

  const navigate = () => {
    if (navigation.canGoBack()) {
      navigation.goBack()
    } else {
      navigation.pop()
    }

    if (
      pointType === POINT_TYPES.firstPrescriptionRegistered ||
      pointType === POINT_TYPES.prescriptionRegistered
    ) {
      navigation.navigate("Main")
    }
  }

  useEffect(() => {
    ;(async () => {
      if (!remoteConfig().getBoolean("points_enabled")) {
        navigate()
      }

      const { data, error } = await supabase.from("point_activities").insert([
        {
          user_id: accountStore.user.id,
          p_id: pid,
          point_type: pointType,
          desc: description,
          point: point,
          drug_take_id: drugTakeIds?.toString(),
          alarm_name: alarmName,
        },
      ])

      if (!error) {
        if (pointType === POINT_TYPES.drugTaken) {
          addTodayMedication({ point: point, drug_take_id: drugTakeIds?.toString() })
        } else if (
          pointType === POINT_TYPES.firstPrescriptionRegistered ||
          pointType === POINT_TYPES.prescriptionRegistered
        ) {
          incPrescriptionRegCount()
        }

        const { data: totalPoints } = await supabase
          .from("total_points")
          .select("*")
          .eq("user_id", accountStore.user.id)

        if (totalPoints && totalPoints.length > 0) {
          const { data: updateResultData } = await supabase
            .from("total_points")
            .update({ total_points: totalPoints[0].total_points + point })
            .eq("user_id", accountStore.user.id)
        } else {
          const insertResponse = await supabase
            .from("total_points")
            .insert([{ total_points: point, user_id: accountStore.user.id }])
        }
      }
    })()
  }, [])

  return (
    <Screen preset="fixed" unsafe>
      <StatusBar backgroundColor="#FFECE8" />
      <LinearGradient f={1} h={height} colors={["#FFFFFF", "#FFECE8"]} start={[0, 1]} end={[0, 0]}>
        <YStack f={1} px={24} pt={top}>
          <YStack f={0.4} />
          <Image width={240} height={240} src={imageMainSource} mt={23} mb={1} alignSelf="center" />
          <Text fow="600" fos={32} lh={42} ls={-0.5} col="#1B1E25" ta="center" mb={12}>
            {addComma(point)}원
          </Text>
          <DisplaySmall mr={8} textAlign="center">
            {getReasonForPoint(pointType)}
            {"\n"}포인트를 획득했습니다{" "}
          </DisplaySmall>
          <YStack f={0.6} jc="flex-end" pb={bottom + 27}>
            <Button chromeless onPress={() => navigate()}>
              <ButtonScaledUpText>닫기</ButtonScaledUpText>
            </Button>
          </YStack>
        </YStack>
      </LinearGradient>
      <ConfettiCannon count={200} origin={{ x: -10, y: 0 }} />
    </Screen>
  )
}

const DisplaySmall = styled(Text, {
  fow: "500",
  fos: 19,
  lh: 25,
  ls: -0.5,
  col: "#565E6D",
})
const ButtonScaledUpText = styled(Text, {
  fow: "500",
  fos: 16,
  lh: 18,
  ls: -0.3,
  col: "#565E6D",
})
