import { DateTime } from "luxon"
import React, { useEffect, useLayoutEffect, useRef, useState } from "react"
import { FlatList } from "react-native"
import { Button, Stack, XStack, YStack } from "tamagui"
import { useSnapshot } from "valtio"

import { supabase } from "@api/supabase"
import { Screen } from "@components/screen/screen"
import { Text } from "@components/StyledText"
import { getText } from "@components/TimeSlotRow"
import { addComma } from "@lib/utils"
import { DetailsStackParamList } from "@navigation/types"
import { StackScreenProps } from "@react-navigation/stack"
import { AccountStore } from "@stores/AccountStore"
import { color } from "@theme/color"
import { typography } from "@theme/typography"

import { useStores } from "../../../../App"
import { POINT_TYPES } from "../constants"
import SvgCrown from "../images/crown.svg"
import SvgInfo from "../images/info.svg"
import SvgPrescription from "../images/prescription.svg"
import SvgTaken from "../images/taken.svg"
import { setActivities, store } from "../stores/total-points"

const renderIcon = (pointType: number) => {
  switch (pointType) {
    case POINT_TYPES.firstPrescriptionRegistered:
    case POINT_TYPES.prescriptionRegistered:
      return <SvgPrescription width={38} height={38} />

    case POINT_TYPES.drugTaken:
      return <SvgTaken width={38} height={38} />

    case POINT_TYPES.allDrugTakenPerDay:
      return <SvgCrown width={38} height={38} />

    default:
      return <SvgPrescription width={38} height={38} />
  }
}

type Props = StackScreenProps<DetailsStackParamList, "MyPoint", "Details">

export function MyPointScreen({ navigation, route }: Props) {
  const snap = useSnapshot(store)
  const createdDateRef = useRef(null)

  const { accountStore }: { accountStore: AccountStore } = useStores()

  const navigateToGuide = () => navigation.push("PointGuide")

  useLayoutEffect(() => {
    navigation.setOptions({
      // eslint-disable-next-line react/display-name
      headerRight: () => {
        return (
          <Button chromeless onPress={navigateToGuide}>
            안내
          </Button>
        )
      },
    })
  }, [navigation])

  useEffect(() => {
    ;(async () => {
      const { data, error } = await supabase
        .from("point_activities")
        .select("*")
        .eq("user_id", accountStore.user.id)
        .order("created_at", { ascending: false })

      setActivities(data)
    })()
  }, [])

  return (
    <Screen preset="fixed" backgroundColor="white">
      <YStack f={1} pt={16}>
        <XStack px={23}>
          <YStack space={8} py={16}>
            <XStack space={6}>
              <Text style={typography.heading.subheading} col={color.textColor.subdued}>
                누적 포인트
              </Text>
              <Stack onPress={navigateToGuide}>
                <SvgInfo width={20} height={20} />
              </Stack>
            </XStack>
            <Text style={typography.display.large} col={color.textColor.action}>
              {snap.totalPoints ? addComma(snap.totalPoints) : 0}원
            </Text>
          </YStack>
        </XStack>
        <Stack h={12} bc={color.backgrounds.subdued} />

        <FlatList
          data={snap.activities}
          renderItem={({ item }) => {
            let isHeaderVisible = true
            const createdDateTime = DateTime.fromISO(item.created_at)

            if (createdDateRef.current?.hasSame(createdDateTime, "day")) {
              isHeaderVisible = false
            }

            createdDateRef.current = createdDateTime
            return (
              <>
                {isHeaderVisible && (
                  <Stack h={49} px={23} py={15.5}>
                    <Text style={typography.ui.default} col={color.textColor.subdued}>
                      {createdDateTime.toLocaleString(DateTime.DATE_MED_WITH_WEEKDAY)}
                    </Text>
                  </Stack>
                )}
                <XStack jc="space-between" ai="center" px={23} py={16}>
                  {renderIcon(item.point_type)}
                  <YStack f={1} ml={12}>
                    <Text style={typography.heading.page} col={color.textColor.default} mb={4}>
                      {item.desc}
                    </Text>
                    <Text style={typography.captions.small} col={color.textColor.subdued}>
                      {item.alarm_name
                        ? `${getText(item.alarm_name)} • ${createdDateTime.toLocaleString(
                            DateTime.TIME_SIMPLE,
                          )}`
                        : createdDateTime.toLocaleString(DateTime.TIME_SIMPLE)}
                    </Text>
                  </YStack>
                  <Text style={typography.heading.page} col={color.textColor.primary}>
                    {addComma(item.point)}원
                  </Text>
                </XStack>
              </>
            )
          }}
        />
      </YStack>
    </Screen>
  )
}
