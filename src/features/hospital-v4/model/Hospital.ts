import { flow, Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"

import { api } from "@api/api"

export const Doctor = types.model("Doctor").props({
  /** 의사 총 수	int	 */
  drTotCnt: types.maybe(types.number),
  /** 의과 일반의 인원 수	int	 */
  mdeptGdrCnt: types.maybe(types.number),
  /** 의과 인턴 인원 수	int	 */
  mdeptIntnCnt: types.maybe(types.number),
  /** 의과 레지던트 인원 수	int	 */
  mdeptResdntCnt: types.maybe(types.number),
  /** 의과 전문의 인원 수	int	 */
  mdeptSdrCnt: types.maybe(types.number),
  /** 치과 일반의 인원 수	int		 */
  detyGdrCnt: types.maybe(types.number),
  /** 치과 인턴 인원 수	int */
  detyIntnCnt: types.maybe(types.number),
  /** 치과 레지던트 인원 수	int	 */
  detyResdntCnt: types.maybe(types.number),
  /** 치과 전문의 인원 수	int	 */
  detySdrCnt: types.maybe(types.number),
  /** 한방 일반의 인원 수	int	 */
  cmdcGdrCnt: types.maybe(types.number),
  /** 한방 인턴 인원 수	int	 */
  cmdcIntnCnt: types.maybe(types.number),
  /** 한방 레지던트 인원 수	int	 */
  cmdcResdntCnt: types.maybe(types.number),
  /** 한방 전문의 인원 수	int	 */
  cmdcSdrCnt: types.maybe(types.number),

  // id: types.number,
  // name: types.string,
  // major: types.maybeNull(types.string),
  // narrow_major: types.maybeNull(types.string),
  // // hospital: types.maybe(types.reference(types.late(() => Hospital))),
  // medical_specialty: types.maybeNull(types.string),
  // specialty_license: types.maybeNull(types.string),
  // medical_school: types.maybeNull(types.string),
  // graduate_year: types.maybeNull(types.number),
  // academic_association: types.maybeNull(types.string),
  // career: types.maybeNull(types.string),
})

export const SpecialtyModel = types.model("Specialty").props({
  /** 구분코드	str */
  srchCd: types.string,
  /** 구분명칭	str */
  srchCdNm: types.string,
  // m_specialty_name: "내과",
  // doctor_num: 0,
})

export const HospitalModel = types.model("Hospital").props({
  id: types.identifierNumber,
  name: types.string,
  /** 개업일자	str	 */
  estbDd: types.string,
  /** 폐업일자	str	 */
  cnclDd: types.maybe(types.string),

  tag: types.array(types.string),
  /** 종별	str	 */
  clCdNm: types.maybe(types.string),
  /** 	설립구분	str	 */
  orgTyCdNm: types.maybe(types.string),
  /** 전화번호	str	 */
  telNo: types.maybe(types.string),
  /** 우편번호	str	 */
  postNo: types.maybe(types.string),
  /** 주소	str	 */
  addr: types.string,
  /** 홈페이지	str	 */
  hospUrl: types.maybe(types.string),
  /** 위치	num[]	 */
  position: types.array(types.number),
  /** 표시과목명 (의원용)	str	 */
  shwSbjtCdNm: types.maybe(types.string),
  doctorInfo: types.maybe(Doctor),
  /** 시설정보 obj */
  facilityInfo: types.maybe(
    types.model({
      /** 일반입원실상급 병상수	int	 */
      hghrSickbdCnt: types.maybe(types.number),
      /** 일반입원실일반 병상수	int	 */
      stdSickbdCnt: types.maybe(types.number),
      /** 성인중환자 병상수	int	 */
      aduChldSprmCnt: types.maybe(types.number),
      /** 소아중환자 병상수	int	 */
      chldSprmCnt: types.maybe(types.number),
      /** 신생아중환자 병상수	int	 */
      nbySprmCnt: types.maybe(types.number),
      /** 분만실 병상수	int	 */
      partumCnt: types.maybe(types.number),
      /** 수술실 병상수	int	 */
      soprmCnt: types.maybe(types.number),
      /** 응급실 병상수	int	 */
      emymCnt: types.maybe(types.number),
      /** 물리치료실 병상수	int	 */
      ptrmCnt: types.maybe(types.number),
      /** 격리실 병상수	int	 */
      isnrSbdCnt: types.maybe(types.number),
      /** 무균치료실 병상수	int	 */
      anvirTrrmSbdCnt: types.maybe(types.number),
      /** 정신과폐쇄상급 병상수	int	 */
      psydeptClsHigSbdCnt: types.maybe(types.number),
      /** 정신과폐쇄일반 병상수	int	 */
      psydeptClsGnlSbdCnt: types.maybe(types.number),
    }),
  ),
  /** 상세정보	obj	 */
  detailInfo: types.maybe(
    types.model({
      /** 공공건물(장소) 이름	str	 */
      plcNm: types.maybe(types.string),
      /** 공공건물(장소) 방향	str	 */
      plcDir: types.maybe(types.string),
      /** 공공건물(장소) 거리	str	 */
      plcDist: types.maybe(types.string),
      /** 주차 가능대수	int	 */
      parkQty: types.maybe(types.number),
      /** 주차장 운영여부, 주차비용 부담여부	str	 */
      parkXpnsYn: types.maybe(types.string),
      /** 주차장 기타 안내사항	str	 */
      parkEtc: types.maybe(types.string),
      /** 주간 응급실 운영여부	str	 */
      emyDayYn: types.maybe(types.string),
      /** 주간 응급실 전화번호 1	str	 */
      emyDayTelNo1: types.maybe(types.string),
      /** 주간 응급실 전화번호 2	str	 */
      emyDayTelNo2: types.maybe(types.string),
      /** 야간 응급실 운영여부	str	 */
      emyNgtYn: types.maybe(types.string),
      /** 야간 응급실 전화번호 1	str	 */
      emyNgtTelNo1: types.maybe(types.string),
      /** 야간 응급실 전화번호 2	str	 */
      emyNgtTelNo2: types.maybe(types.string),
      /** 월~금 점심시간	str	 */
      lunchWeek: types.maybe(types.string),
      /** 토 점심시간	str	 */
      lunchSat: types.maybe(types.string),
      /** 일 점심시간	str	 */
      lunchSun: types.maybe(types.string),
      /** 월~금 접수시간	str	 */
      rcvWeek: types.maybe(types.string),
      /** 토 접수시간	str	 */
      rcvSat: types.maybe(types.string),
      /** 일 접수시간	str	 */
      rcvSun: types.maybe(types.string),
      /** 일요일 휴진 안내	str	 */
      noTrmtSun: types.maybe(types.string),
      /** 공휴일 휴진 안내	str	 */
      noTrmtHoli: types.maybe(types.string),
      /** 월 진료시작시간	str	 */
      trmtMonStart: types.maybe(types.string),
      /** 월 진료종료시간	str	 */
      trmtMonEnd: types.maybe(types.string),
      /** 화 진료시작시간	str	 */
      trmtTueStart: types.maybe(types.string),
      /** 화 진료종료시간	str	 */
      trmtTueEnd: types.maybe(types.string),
      /** 수 진료시작시간	str	 */
      trmtWedStart: types.maybe(types.string),
      /** 수 진료종료시간	str	 */
      trmtWedEnd: types.maybe(types.string),
      /** 목 진료시작시간	str	 */
      trmtThuStart: types.maybe(types.string),
      /** 목 진료종료시간	str	 */
      trmtThuEnd: types.maybe(types.string),
      /** 금 진료시작시간	str	 */
      trmtFriStart: types.maybe(types.string),
      /** 금 진료종료시간	str	 */
      trmtFriEnd: types.maybe(types.string),
      /** 토 진료시작시간	str	 */
      trmtSatStart: types.maybe(types.string),
      /** 토 진료종료시간	str	 */
      trmtSatEnd: types.maybe(types.string),
      /** 일 진료시작시간	str	 */
      trmtSunStart: types.maybe(types.string),
      /** 일 진료종료시간	str	 */
      trmtSunEnd: types.maybe(types.string),
      /** 공휴일 진료시작시간	str	 */
      trmtHoliStart: types.maybe(types.string),
      /** 공휴일 진료종료시간	str	 */
      trmtHoliEnd: types.maybe(types.string),
    }),
  ),
  /** 교통정보	obj[]	 */
  // transportInfo: types.maybe(types.model({
  //   trafNm	교통편	str
  // lineNo	노선번호	str
  // arivPlc	하차지점	str
  // dir	방향	str
  // dist	거리	str
  // rmk	비고	str
  // }))
  /** 진료과목정보	obj[]	 */
  mdlrtSbjectInfo: types.array(
    types.model({
      /** 진료과목코드	str	 */
      dgsbjtCd: types.string,
      /** 진료과목명칭	str	 */
      dgsbjtCdNm: types.string,
      /** 의사수	int	 */
      dgsbjtPrSdrCnt: types.number,
      /** 선택진료의사수	int	 */
      cdiagDrCnt: types.number,
    }),
  ),
  /** 의료장비정보	obj[]	 */
  // medicalEquipmentInfo:
  //   oftCd	장비코드	str
  //   oftCdNm	장비코드명칭	str
  //   oftCnt	장비보유대수	int
  /** 특수진료 (진료가능분야조회)	obj[]	 */
  // spclMdlrtInfo
  // srchCd	구분코드	str
  // srchCdNm	구분명칭	str
  /** 전문병원 지정분야	obj[] */
  spcHospAppnField: types.array(SpecialtyModel),

  // addr: types.maybeNull(types.optional(types.string, "")),
  // x_pos: types.maybeNull(types.optional(types.string, "")),
  // y_pos: types.maybeNull(types.optional(types.string, "")),
  // phone: types.maybeNull(types.optional(types.string, "")),
  // web: types.maybeNull(types.optional(types.string, "")),
  // mon_open_time: types.maybeNull(types.optional(types.string, "")),
  // mon_close_time: types.maybeNull(types.optional(types.string, "")),
  // tue_open_time: types.maybeNull(types.optional(types.string, "")),
  // tue_close_time: types.maybeNull(types.optional(types.string, "")),
  // wed_open_time: types.maybeNull(types.optional(types.string, "")),
  // wed_close_time: types.maybeNull(types.optional(types.string, "")),
  // thu_open_time: types.maybeNull(types.optional(types.string, "")),
  // thu_close_time: types.maybeNull(types.optional(types.string, "")),
  // fri_open_time: types.maybeNull(types.optional(types.string, "")),
  // fri_close_time: types.maybeNull(types.optional(types.string, "")),
  // sat_open_time: types.maybeNull(types.optional(types.string, "")),
  // sat_close_time: types.maybeNull(types.optional(types.string, "")),
  // sun_open_time: types.maybeNull(types.optional(types.string, "")),
  // sun_close_time: types.maybeNull(types.optional(types.string, "")),
  // holi_open_time: types.maybeNull(types.optional(types.string, "")),
  // holi_close_time: types.maybeNull(types.optional(types.string, "")),
  // hospital_specialties: types.array(SpecialtyModel),

  // class_code: types.number,
  // class_name: types.string,
  // address: types.string,
  // x_pos: types.string,
  // y_pos: types.string,
  // phone: types.string,
  // web: types.optional(types.string, ""),
  // type_code: types.number,
  // est_date: types.Date,
  // closed_date: types.maybe(types.Date),
  // total_doctor_num: types.number,
  // gp_num: types.number,
  // intern_num: types.number,
  // regident_num: types.number,
  // specialist_num: types.number,
  // exist_weekly_em: types.boolean,
  // weekly_em_phone1: types.string,
  // weekly_em_phone2: types.string,
  // exist_night_em: types.boolean,
  // night_em_phone1: types.string,
  // night_em_phone2: types.string,
  // weekly_rcv_time: types.string,
  // sat_rcv_time: types.string,
  // weekly_lunch_time: types.string,
  // sat_lunch_time: types.string,
  // holiday_close_info: types.string,
  // sunday_close_info: types.string,
  // etc_info: types.optional(types.string, ""),

  // avail_parking_car_num: types.number,
  // avail_parking: types.boolean,

  // mon_open_time: types.optional(types.string, ""),
  // mon_close_time: types.optional(types.string, ""),
  // tue_open_time: types.optional(types.string, ""),
  // tue_close_time: types.optional(types.string, ""),
  // wed_open_time: types.optional(types.string, ""),
  // wed_close_time: types.optional(types.string, ""),
  // thu_open_time: types.optional(types.string, ""),
  // thu_close_time: types.optional(types.string, ""),
  // fri_open_time: types.optional(types.string, ""),
  // fri_close_time: types.optional(types.string, ""),
  // sat_open_time: types.optional(types.string, ""),
  // sat_close_time: types.optional(types.string, ""),

  // doctors: types.optional(types.array(Doctor), [])
})
// .preProcessSnapshot(snapshot => ({
//   // auto convert strings to booleans as part of preprocessing
//   mon_open_time: insert(snapshot.mon_open_time, snapshot.mon_open_time.length-3, ':')
//   // mon_open_time: snapshot.mon_open_time === "true" ? true : snapshot.done === "false" ? false : snapshot.done
// }))
// .actions(self => ({
//   setHospital(hospital: any) {
//     if (hospital != null) {
//       self.hospital = hospital
//     }
//   }
// }))
// .create()

export interface Hospital extends Instance<typeof HospitalModel> {}
export interface HospitalSnapshotOut extends SnapshotOut<typeof HospitalModel> {}
export interface HospitalSnapshotIn extends SnapshotIn<typeof HospitalModel> {}
export const createHospitalDefaultModel = () => types.optional(HospitalModel, {})
