import { applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { swrService } from "@api/swr-service"
import { sendLogEvent } from "@lib/analytics"
import { withStatus } from "@stores/extensions/with-status"

import { Hospital, HospitalModel } from "./Hospital"

type HospitalSummary = {
  id: number
  name: string
  /** 개업일자	str	 */
  estbDd: string
  /** 폐업일자	str	 */
  cnclDd: string
  addr: string
  tag: string[]
}

type HospitalsPayload = {
  result: number
  list: HospitalSummary[]
}

/**
 * 검색 api 를 호출하고 검색 결과를 저장하는 store
 */
export const SearchHospitalStoreModel = types
  .model("SearchHospitalStore")
  .props({
    prevQuery: types.maybe(types.string),
    limit: types.optional(types.number, 40),
    count: types.optional(types.number, 0),
    page: types.optional(types.number, 1),
    nextPage: types.maybeNull(types.number),
    nextUri: types.maybeNull(types.string),
    prevUri: types.maybeNull(types.string),
    hospitals: types.array(HospitalModel),
    hospitalDetail: types.maybe(HospitalModel),
  })
  .extend(withStatus)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    initiate() {
      applySnapshot(self, {})
    },
    next: flow(function* () {
      if (self.nextUri == null) return

      const response = yield swrService.client.get<HospitalsPayload>(self.nextUri)
      if (response) {
        self.hospitals = self.hospitals.concat(response.list)
        self.count = response.result
        self.page = self.nextPage

        const totalPages = Math.ceil(self.count / self.limit)
        self.nextUri =
          totalPages > self.page
            ? `/hospitals?q=${self.prevQuery}&rows=${self.limit}&page=${self.page + 1}`
            : null
        self.prevUri =
          self.page > 1
            ? `/hospitals?q=${self.prevQuery}&rows=${self.limit}&page=${self.page - 1}`
            : null
      } else {
        self.setStatus("error")
      }
    }),
    search: flow(function* (query: string, page?: number, limit?: number) {
      self.setStatus("pending")
      self.limit = limit

      const response = yield swrService.client.get<HospitalsPayload>(
        `/hospitals?q=${query}&rows=${limit}&page=${page}`,
      )
      sendLogEvent("search", { search_term: query })
      if (response) {
        applySnapshot(self.hospitals, response.list)
        self.count = response.result
        self.page = page
        self.setStatus("done")
        self.prevQuery = query

        const totalPages = Math.ceil(self.count / limit)
        self.nextPage = totalPages > page ? page + 1 : null
        self.nextUri =
          totalPages > page ? `/hospitals?q=${query}&rows=${limit}&page=${page + 1}` : null
        self.prevUri = page > 1 ? `/hospitals?q=${query}&rows=${limit}&page=${page - 1}` : null
      } else {
        self.setStatus("error")
      }
    }),
    fetchHospitalDetail: flow(function* (id: number) {
      self.setStatus("pending")
      const response = yield swrService.client.get<Hospital>(`/hospitals/${id}`)
      if (response) {
        self.hospitalDetail = HospitalModel.create(response)
        self.setStatus("done")
      } else {
        self.setStatus("error")
        console.tron.log("< HospitalStore > fetchHospitalDetail, error")
      }
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type SearchHospitalStoreType = Instance<typeof SearchHospitalStoreModel>
export interface SearchHospitalStore extends SearchHospitalStoreType {}
type SearchHospitalStoreSnapshotType = SnapshotOut<typeof SearchHospitalStoreModel>
export interface SearchHospitalStoreSnapshot extends SearchHospitalStoreSnapshotType {}
