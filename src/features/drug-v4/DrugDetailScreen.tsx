import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Spinner, View } from "native-base"
import React, { Component } from "react"
import {
  Alert,
  Dimensions,
  Image,
  ImageStyle,
  Platform,
  ScrollView,
  Text,
  TextStyle,
  ViewStyle,
} from "react-native"
import { PieChart } from "react-native-svg-charts"
import { TabBar, TabView } from "react-native-tab-view"

import SvgGraph from "@assets/images/graph.svg"
import {
  AuthStackParamList,
  MainTabParamList,
  PrescriptionsStackParamList,
  RootStackParamList,
} from "@navigation/types"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"
import { NpsStore } from "@stores/nps/NpsStore"
import { color } from "@theme/color"
import { palette } from "@theme/palette"
import { typography } from "@theme/typography"

import { SearchDrugStore } from "./model/SearchDrugStore"

const CONTENT_CONTAINER: ViewStyle = {
  paddingTop: 8,
}
const DISEASE_TITLE: TextStyle = {
  ...typography.title2,
  paddingBottom: 6,
}
const DRUG_INGREDIENT: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
}
const DRUG_ONE_LINER: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 16,
  lineHeight: 24,
  color: color.text,
  textDecorationLine: "underline",
  marginBottom: 24,
}
const CONTENT_HEADER: ViewStyle = {
  backgroundColor: "#fff",
  marginBottom: 10,
  paddingLeft: 16,
  paddingRight: 16,
  paddingBottom: 22,
  borderBottomWidth: 1,
  borderBottomColor: color.palette.pale,
}
const CONTENT_TEXT: TextStyle = {
  ...typography.body,
  paddingBottom: 30,
}
const CONTENT_TEXT_SUBTITLE: TextStyle = {
  ...typography.title2,
  marginBottom: 7,
}
const PREVALENCE_DESC_ACCENT = {
  fontFamily: typography.bold,
  fontSize: 16,
  lineHeight: 24,
  color: color.primary,
}
const BOTTOM_CONTENT = {}
const BOTTOM_CONTENT_DESC: TextStyle = {
  ...typography.sub1,
}
const BOTTOM_CONTENT_DESC_BOLD: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
}
const PIE_CHART: ViewStyle = { width: 160, height: 160, marginTop: 8 }
const RANK_LIST_BOX: ViewStyle = {
  borderWidth: 1,
  borderColor: color.palette.pale,
  borderRadius: 0.5,
  paddingTop: 24,
  paddingHorizontal: 24,
  paddingBottom: 8,
}
const ROW_VERT_CENTER: ViewStyle = { flexDirection: "row", alignItems: "center" }
const RANK_CONTENT: TextStyle = {
  flex: 1,
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 24,
  color: color.text,
}
const RANK_CONTENT_ACCENT: TextStyle = {
  flex: 1,
  fontFamily: typography.bold,
  fontSize: 16,
  lineHeight: 24,
  color: color.primary,
}
const RANK_PERCENT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 24,
  color: color.text,
}
const RANK_PERCENT_ACCENT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 16,
  lineHeight: 24,
  color: color.primary,
}
const PIE_CHART_BOX: ViewStyle = {
  flexDirection: "row",
  paddingHorizontal: 10,
  paddingVertical: 20,
  justifyContent: "center",
}
const DATA_FROM_BOX: ViewStyle = {
  borderRadius: 1,
  backgroundColor: "#FCFBF9",
  marginVertical: 32,
  padding: 16,
}
const EMPTY_DATA_BOX: ViewStyle = {
  // flex: 1,
  height: 112,
  borderRadius: 2,
  backgroundColor: "#FCFBF9",
  justifyContent: "center",
  alignItems: "center",
  marginTop: 20,
}
const EMPTY_DATA_TEXT: TextStyle = {
  ...typography.sub2,
  opacity: 0.3,
  marginTop: 4,
}
const CONTENT_BOX: ViewStyle = {
  flex: 1,
  paddingHorizontal: 16,
  paddingTop: 16,
  paddingBottom: 40,
}
const TAB_BAR: ViewStyle = {
  height: 60,
  backgroundColor: color.palette.white,
  paddingLeft: 8,
  paddingRight: 8,
  marginTop: 16,
  shadowOffset: { height: 0, width: 0 },
  shadowColor: "transparent",
  shadowOpacity: 0,
  elevation: 0,
}
const FULL_WHITE: ViewStyle = { flex: 1, backgroundColor: color.palette.white }
const RANK_BOX: ViewStyle = {
  width: 20,
  height: 20,
  borderRadius: 1,
  backgroundColor: color.palette.gray3,
  marginRight: 16,
  justifyContent: "center",
  alignItems: "center",
}
const RANK_BOX_ORANGE: ViewStyle = {
  ...RANK_BOX,
  backgroundColor: color.primary,
}
const RANK_TEXT: TextStyle = {
  ...typography.sub1,
  color: color.palette.white,
}
const DRUG_IMAGE: ImageStyle = {
  width: 334.7,
  height: 182.7,
  alignSelf: "center",
  borderRadius: 5,
}
const circleColors = [
  color.palette.orange,
  color.palette.paleSalmon,
  color.palette.veryPaleRed,
  color.palette.gray2,
  color.palette.gray3,
  color.palette.grey4,
]
type DrugDetailRouteProp = RouteProp<PrescriptionsStackParamList, "DrugDetail">

type DrugDetailNavigationProp = CompositeNavigationProp<
  StackNavigationProp<PrescriptionsStackParamList, "DrugDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AuthStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  searchDrugV4Store: SearchDrugStore
  npsStore: NpsStore
  navigation: DrugDetailNavigationProp
  route: DrugDetailRouteProp
}

interface State {
  index: number
  routes: Array<any>
  data: any
  hasIngredientDiseaseData: boolean
  contentHeight: number
  count: number
}

@inject("searchDrugV4Store", "npsStore")
@observer
export default class DrugDetailScreen extends Component<Props, State> {
  static navigationOptions = {
    title: "",
    headerStyle: {
      height: Platform.OS === "ios" ? 76 : undefined,
      borderBottomWidth: 0,
      elevation: 0,
      shadowColor: "transparent",
    },
  }

  drugId = -1
  data: any
  onActionDisposer: any
  onSnapshotDisposer: any
  getIngredientDisease: any

  constructor(props: Props) {
    super(props)
    this.state = {
      index: 0,
      routes: [
        { key: "요약설명", title: "요약설명" },
        { key: "약제특성", title: "약제특성" },
        { key: "처방질병", title: "처방질병" },
      ],
      data: [
        {
          key: 1,
          name: "클라로마정500mg",
          dose: 0.5, // 1회 투여량
          unit: "정", // 1회 투여량 단위
          doses_num: 3, // 투여횟수
          per_days: 1, // 투여일 기본값1
          dosing_days_num: 3, // 총 투약일수
        },
      ],
      hasIngredientDiseaseData: false,
      contentHeight: -1,
      count: 0,
    }

    const { route } = this.props

    this.drugId = this.getDrugId(route.params)

    //   // 정우님이 처리하신 '처방질병' 데이터 호출
    // this.onActionDisposer = onAction(prescriptionStore, (call) => {
    //   if (call.name == "setDrug" && call.args) {
    //     if (call.args[0] && call.args[0].ingredient) {
    //       npsStore.fetchIngredientDisease(
    //         call.args[0].ingredient.code.substr(0, call.args[0].ingredient.code.length - 1),
    //       )
    //     } else {
    //       npsStore.clearIngredientDisease()
    //     }
    //   }
    // })
  }

  UNSAFE_componentWillMount() {
    this.props.searchDrugV4Store.fetchDrugDetail(this.drugId)
  }

  componentDidMount() {
    const { route, npsStore } = this.props
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    firebase.analytics().logEvent("drug_detail_screen")
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })
    npsStore.clearIngredientDisease()
  }
  // 정우님이 처리하신 '처방질병' 데이터 호출
  // componentWillUnmount() {
  //   this.onActionDisposer()
  // }

  getDrugId(params: any): any {
    return params.id
  }

  render() {
    const { hasIngredientDiseaseData } = this.state
    const { searchDrugV4Store, npsStore } = this.props
    const { drugDetail } = searchDrugV4Store
    const { mainIngredient } = drugDetail
    // const { ingredient, howto } = drugDetail
    /* const filteredVideo = howto
       ?.filter((data) => data !== "")
       .toString()
       .replace(`autoplay=""`, "")
       .replace(`height="100%`, `height="50%"`)
    */

    // 처방질병 데이터 호출 (ingredient.code에서 마지막 한자리를 제외해서 호출해야함. )
    // this.getIngredientDisease = async () => {
    //   try {
    //     // if (ingredient?.code && !hasIngredientDiseaseData) {
    //     //   const ingCode = ingredient?.code?.slice(0, -1)
    //     //   await npsStore.fetchIngredientDisease(ingCode)
    //     //   if (npsStore.ingredientDisease.length !== 0) {
    //     //     this.setState({ hasIngredientDiseaseData: true })
    //     //   }
    //     // }
    //   } catch (error) {
    //     console.tron.log("< DrugDetailScreen > 처방질병, error:", error)
    //   }
    // }
    // this.getIngredientDisease()

    const inPrepareText =
      // drugDetail?.category === "m"
      "해당 의약품에 대한 약제 정보는 추후 업데이트 예정입니다. 현재 파프리카케어는 국내 다빈도 처방의약품 순위 상위 95% 에 대하여 약제 정보를 제공하고 있습니다."
    // : "현재 파프리카케어는 건강기능식품 혹은 일반 식품에 대한 정보는 제공하고 있지 않습니다."

    if (searchDrugV4Store.status === "error") {
      Alert.alert(
        "서버 연결 실패",
        `서버와의 연결이 원활하지 않습니다. \n잠시 후 다시 시도해 주세요.`,
        [{ text: "확인", onPress: () => null }],
        { cancelable: false },
      )
    }

    if (searchDrugV4Store.status === "pending") {
      return <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
    }

    return (
      <View style={{ backgroundColor: color.palette.white }}>
        <ScrollView
          contentContainerStyle={{
            ...CONTENT_CONTAINER,
            height:
              this.state.contentHeight !== -1
                ? this.state.contentHeight
                : Dimensions.get("window").height,
          }}
          showsVerticalScrollIndicator={Platform.OS === "android"}
        >
          <View style={CONTENT_HEADER}>
            <Text style={DISEASE_TITLE}>{drugDetail?.name}</Text>
            <Text style={DRUG_INGREDIENT}>
              성분명: {mainIngredient.length > 0 ? mainIngredient[0].nameKo : ""}
            </Text>
          </View>

          <TabView
            navigationState={this.state}
            renderScene={({ route }) => {
              let text
              if (drugDetail) {
                switch (route.key) {
                  case "요약설명":
                    text =
                      mainIngredient.length > 0 && mainIngredient[0]?.description?.caution
                        ? mainIngredient[0]?.description.caution.replaceAll("***", drugDetail?.name)
                        : inPrepareText
                    return (
                      // 텍스트 하단 나머지 영역 모두 화이트 배경색 적용 위해 View 로 ScrollView 감쌈
                      <View style={{ ...FULL_WHITE, ...CONTENT_BOX }}>
                        {/* <ScrollView contentContainerStyle={CONTENT_BOX}> */}
                        <Text style={DRUG_ONE_LINER}>
                          {mainIngredient.length > 0 && mainIngredient[0]?.description?.feature
                            ? mainIngredient[0]?.description.feature
                            : "약제 설명을 준비 중입니다."}
                        </Text>
                        {drugDetail.feature ? (
                          <Text style={CONTENT_TEXT}>{text}</Text>
                        ) : (
                          <View style={[DATA_FROM_BOX, { marginTop: 0 }]}>
                            <Text style={BOTTOM_CONTENT_DESC_BOLD}>
                              현재 파프리카케어는 국내 다빈도 처방의약품 순위 상위 95% 에 대하여
                              약제 정보를 제공하고 있습니다. 해당 의약품에 대한 약제 정보는 추후
                              업데이트 예정입니다.
                            </Text>
                          </View>
                        )}
                        {drugDetail.image?.length > 0 ? (
                          <Image source={{ uri: drugDetail.image }} style={DRUG_IMAGE} />
                        ) : null}
                        {/* #노바티스 03
                           {filteredVideo.length ? (
                            <WebView
                              originWhitelist={["*"]}
                              allowFileAccess={true}
                              style={{ height: 350, width: "100%" }}
                              allowsFullscreenVideo={true}
                              bounces={false}
                              scrollEnabled={false}
                              showsHorizontalScrollIndicator={false}
                              showsVerticalScrollIndicator={false}
                              source={{
                                html: `${filteredVideo}`,
                              }}
                            />
                          ) : null} */}
                        {/* </ScrollView> */}
                      </View>
                    )
                    break

                  case "약제특성":
                    // text = ingredient?.ingredient_detail?.upside_kr
                    //   ? ingredient.ingredient_detail.upside_kr
                    //   : inPrepareText

                    if (mainIngredient.length > 0 && mainIngredient[0].description) {
                      return (
                        <View
                          style={{ ...FULL_WHITE, ...CONTENT_BOX }}
                          onLayout={(event) => {
                            const layoutHeight = event.nativeEvent.layout.height

                            if (this.state.contentHeight === -1 || this.state.count < 3) {
                              this.setState({
                                contentHeight: layoutHeight + 700,
                                count: this.state.count + 1,
                              })
                            }
                          }}
                        >
                          {mainIngredient.length > 0 ? (
                            <Text style={CONTENT_TEXT}>
                              <Text style={CONTENT_TEXT_SUBTITLE}>특장점</Text>
                              {"\n"}
                              {mainIngredient[0]?.description?.merit
                                ? mainIngredient[0].description.merit.replaceAll(
                                    "***",
                                    drugDetail?.name,
                                  )
                                : inPrepareText}
                              {"\n\n\n"}
                              <Text style={CONTENT_TEXT_SUBTITLE}>단점</Text>
                              {"\n"}
                              {mainIngredient[0]?.description?.fault
                                ? mainIngredient[0].description.fault.replaceAll(
                                    "***",
                                    drugDetail?.name,
                                  )
                                : inPrepareText}
                              {"\n\n\n"}
                              <Text style={CONTENT_TEXT_SUBTITLE}>원리</Text>
                              {"\n"}
                              {mainIngredient[0]?.description?.effect
                                ? mainIngredient[0].description.effect.replaceAll(
                                    "***",
                                    drugDetail?.name,
                                  )
                                : inPrepareText}
                              {"\n\n\n"}
                            </Text>
                          ) : (
                            <Text style={CONTENT_TEXT}>{text}</Text>
                          )}
                        </View>
                      )
                    } else {
                      return (
                        <View style={{ ...FULL_WHITE, ...CONTENT_BOX }}>
                          {this.renderEmptyData()}
                        </View>
                      )
                    }
                    break

                  case "처방질병":
                    return this.renderDrugAnalysis()

                  default:
                  // text = inPrepareText
                }
              } else {
                // text = inPrepareText
              }

              return (
                <View style={{ ...FULL_WHITE, ...CONTENT_BOX }}>
                  <Text style={CONTENT_TEXT}>{text}</Text>
                </View>
              )
            }}
            renderTabBar={this.renderTabBar}
            onIndexChange={(index: number) => this.setState({ index })}
            initialLayout={{
              width: Dimensions.get("window").width,
            }}
          />
        </ScrollView>
      </View>
    )
  }

  renderDrugAnalysis() {
    // const { npsStore } = this.props
    // const ingredientDisease = npsStore.ingredientDisease

    const { searchDrugV4Store, npsStore } = this.props
    const { drugDetail } = searchDrugV4Store
    const { mainIngredient } = drugDetail
    const nps = mainIngredient.length > 0 ? mainIngredient[0].nps : undefined

    const sortIngredientDisease =
      nps?.disease.slice().sort((a, b) => {
        if (a.rank < b.rank) {
          return -1
        } else if (a.rank > b.rank) {
          return 1
        } else return 0
      }) ?? []
    const pieData = sortIngredientDisease?.map((value, index) => ({
      value: value.percent,
      svg: {
        fill: circleColors[index],
        onPress: () => null,
      },
      key: value.rank,
    }))

    return (
      <View style={CONTENT_BOX}>
        {sortIngredientDisease.length > 0 ? (
          <View>
            <Text style={CONTENT_TEXT_SUBTITLE}>처방 질병 순위</Text>
            <Text style={typography.body}>
              해당 약제가 가장 빈번하게 처방되는 질병은{" "}
              <Text style={PREVALENCE_DESC_ACCENT}>
                {sortIngredientDisease.length > 0
                  ? `${sortIngredientDisease[0].name}(${sortIngredientDisease[0].code})`
                  : ""}
              </Text>{" "}
              입니다.
            </Text>
            <View style={PIE_CHART_BOX}>
              <PieChart
                style={[PIE_CHART, { marginBottom: 10 }]}
                data={pieData}
                innerRadius="60%"
                sort={(a, b) => parseInt(a.key) - parseInt(b.key)}
                padAngle={0}
              />
            </View>
            <View style={RANK_LIST_BOX}>
              {sortIngredientDisease.map((item, index) => (
                <View key={index}>
                  <View style={{ ...ROW_VERT_CENTER, marginBottom: 16 }}>
                    <View style={item.rank === "1" ? RANK_BOX_ORANGE : RANK_BOX}>
                      <Text style={RANK_TEXT}>{item.rank === "etc" ? "6" : item.rank}</Text>
                    </View>
                    <Text style={item.rank === "1" ? RANK_CONTENT_ACCENT : RANK_CONTENT}>
                      {item.name}
                      {/* {item.disease_code !== "etc" ? `(${item.disease_code})` : null} */}
                    </Text>
                    <Text style={item.rank === "1" ? RANK_PERCENT_ACCENT : RANK_PERCENT}>
                      {item.percent < 0.5 ? item.percent.toFixed(1) : Math.round(item.percent)}%
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          </View>
        ) : (
          this.renderEmptyData()
        )}
        <View style={BOTTOM_CONTENT}>
          <View style={DATA_FROM_BOX}>
            <Text style={BOTTOM_CONTENT_DESC_BOLD}>
              <Text style={BOTTOM_CONTENT_DESC}>출처: </Text>
              파프리카케어에서 제공하는 질병 및 약제 통계정보는 건강보험심사평가원에서 매년 제공하는
              145만명의 환자 표본데이터 중, 가장 최신의 2017년 자료를 의료데이터 솔루션 전문회사인
              Core-Zeta가 수행한 분석자료입니다.
            </Text>
          </View>
        </View>
      </View>
    )
  }

  renderEmptyData() {
    return (
      <View style={EMPTY_DATA_BOX}>
        <SvgGraph width={29} height={28} />
        <Text style={EMPTY_DATA_TEXT}>데이터가 없습니다.</Text>
      </View>
    )
  }

  renderTabBar = (props: any) => (
    <TabBar
      {...props}
      indicatorStyle={{
        backgroundColor: color.palette.white,
      }}
      indicatorContainerStyle={{}}
      style={TAB_BAR}
      dynamicWidth
      scrollEnabled
      tabStyle={{
        width: "auto",
        height: 42,
        padding: 0,
        margin: 0,
      }}
      renderLabel={({ route, focused, color }) => (
        <View
          style={{
            height: 36,
            borderRadius: 18,
            flexDirection: "row",
            backgroundColor: focused ? palette.orange : palette.veryLightPink3,
            justifyContent: "center",
            alignItems: "center",
            paddingHorizontal: 16,
            marginLeft: 8,
          }}
        >
          <Text
            style={{
              color,
              fontFamily: typography.bold,
              fontSize: 16,
              lineHeight: 24,
            }}
          >
            {route.title}
          </Text>
        </View>
      )}
      // labelStyle={{}}
      activeColor={color.palette.white}
      inactiveColor={`${color.text}4D`}
    />
  )
}
