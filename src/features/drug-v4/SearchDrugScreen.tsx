import debounce from "lodash.debounce"
import { observer } from "mobx-react"
import { Icon, Spinner } from "native-base"
import React, { FC, useCallback, useEffect, useRef, useState } from "react"
import {
  FlatList,
  Keyboard,
  StatusBar,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import FastImage from "react-native-fast-image"
import { useSafeAreaInsets } from "react-native-safe-area-context"

import SvgDrugEmptyImage from "@assets/images/drugEmptyImage.svg"
import SearchIcon from "@assets/images/searchIcon.svg"
import { Screen } from "@components/screen/screen"
import { Button } from "@components/StyledButton"
import { Input } from "@components/StyledInput"
import { sendLogEvent } from "@lib/analytics"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "@navigation/types"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import {
  CompositeNavigationProp,
  RouteProp,
  useNavigation,
  useRoute,
} from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"
import { EditMedicationDetailMode } from "@screens/EditMedicationDetailScreen"
import { color } from "@theme/color"
import { typography } from "@theme/typography"

import { useStores } from "../../../App"
import { Drug } from "./model/Drug"

type SearchDrugScreenRouteProp = RouteProp<DetailsStackParamList, "SearchDrug">
type SearchDrugScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "SearchDrug">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>
export interface SearchDrugScreenProps {
  navigation: SearchDrugScreenNavigationProp
  route: SearchDrugScreenRouteProp
  mode: SearchDrugMode | undefined
}

const SEARCHBOX_INPUT: ViewStyle = {
  height: 50,
  flexDirection: "row",
  backgroundColor: color.palette.veryLightPink3,
  borderRadius: 5,
  borderWidth: 1,
  borderColor: color.palette.gray4,
  marginLeft: 16,
  marginRight: 16,
  marginBottom: 17,
  alignItems: "center",
  padding: 10,
}
const TITLE2_ORANGE: TextStyle = {
  ...typography.body,
  fontFamily: typography.bold,
  color: color.primary,
}
const SEARCH_INPUT: TextStyle = {
  ...typography.title3,
  flex: 1,
  marginLeft: 5,
  height: 50,
  fontWeight: "bold",
}
const BODY_CONTAINER: ViewStyle = {
  flex: 1,
  backgroundColor: color.palette.white,
}
const SECTION_HEADER: ViewStyle = {
  paddingBottom: 16,
  paddingHorizontal: 16,
}
const HEADER_LINE: ViewStyle = {
  borderBottomColor: color.line,
  borderBottomWidth: 1,
}
const SEARCH_RESULTS_CONTAINER: ViewStyle = {
  flex: 9,
  paddingTop: 16,
}
const DRUG_BUTTON: ViewStyle = {
  // minHeight: 103,
  flexDirection: "row",
  paddingLeft: 16,
  paddingRight: 8,
  paddingTop: 16,
  paddingBottom: 24,
}
const DRUG_IMAGE: ViewStyle = {
  width: 84,
  height: 48,
  borderRadius: 5,
}
const DRUG_EMPTY_IMAGE: ViewStyle = { alignSelf: "flex-start" }
const DRUG_BUTTON_RIGHT = {
  flex: 1,
  marginLeft: 8,
  marginBottom: 4,
  paddingTop: 8,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const DRUG_NAME = { ...typography.title3, paddingRight: 10 }
const DRUG_ONE_LINER = { paddingRight: 16, marginBottom: 8, marginTop: 8 }
const DRUG_LINE = {
  backgroundColor: color.palette.pale,
  height: 1,
  marginLeft: 16,
  marginRight: 8,
}
const BOTTOM_BUTTONS: ViewStyle = {
  position: "absolute",
  bottom: 0,
  width: "100%",
}
const CONFIRM_BUTTON = {
  marginHorizontal: 16,
}
const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
  color: color.palette.white,
}
const CENTER: ViewStyle = { flex: 1, alignSelf: "center" }
const ICON_SEARCH = { fontSize: 24 }
const LIMIT = 40

export enum SearchDrugMode {
  /** 통합 검색용 */
  Search,
  /** 복약 내용 수정 모드 */
  EditPrescriptionDetail,
  /** 새로운 복약 내용을 추가하는 모드 */
  NewPrescriptionDetail,
  /** 처방전 찍기(+) 버튼 > 직접 입력 선택하여 새로운 처방전을 사용자가 입력하는 모드 */
  NewPrescription,
}

const Item = ({ item, onPress, style }) => (
  <TouchableOpacity style={[DRUG_BUTTON, style]} onPress={onPress}>
    {item.image ? (
      <FastImage source={{ uri: item.image }} style={DRUG_IMAGE} />
    ) : (
      <SvgDrugEmptyImage width={84} height={48} style={DRUG_EMPTY_IMAGE} />
    )}
    <View style={DRUG_BUTTON_RIGHT}>
      <View style={ROW}>
        <Text style={DRUG_NAME} numberOfLines={1} ellipsizeMode="tail">
          {item.name}
        </Text>
      </View>
      {/* {item.feature ? (
        <View style={DRUG_ONE_LINER}>
          <Text style={typography.sub2}>{item.feature}</Text>
        </View>
      ) : null} */}
    </View>
  </TouchableOpacity>
)

export const SearchDrugScreen: FC<SearchDrugScreenProps> = observer((props) => {
  const navigation = useNavigation()
  const route = useRoute<SearchDrugScreenRouteProp>()
  // const { mode } = props
  const { mode } = props
  // const { mode } = route.params
  const insets = useSafeAreaInsets()
  const { searchDrugV4Store: searchStore } = useStores()
  const { count, drugs, prevQuery } = searchStore

  const resultsRef = useRef<FlatList<Drug>>(null)
  const [selectedItem, setSelectedItem] = useState<Drug>()
  const [query, setQuery] = useState("")

  useEffect(() => {
    searchStore.initiate()
    console.tron.log("Drug V4 검색")
  }, [])

  const search = async () => {
    if (query.length === 0) return

    if (prevQuery !== query) {
      setSelectedItem(null)
      searchStore.search(query, 1, LIMIT)
    } else {
      searchStore.next()
    }
  }

  // A debounce function that delays function invocation by 500 milliseconds.
  const delayedQuery = useCallback(debounce(search, 500), [query])

  // Calls delayedQuery() whenever query or delayedQuery values changes
  useEffect(() => {
    delayedQuery()
    // Cancel previous debounce calls during useEffect cleanup.
    return () => {
      delayedQuery.cancel()
    }
  }, [query, delayedQuery])

  const onChangeKeywords = (value: string) => {
    setQuery(value)
  }

  const addItem = () => {
    let editMedicationDetailMode
    switch (route.params?.mode) {
      case SearchDrugMode.EditPrescriptionDetail:
        editMedicationDetailMode = EditMedicationDetailMode.EditPrescriptionDetail
        break

      case SearchDrugMode.NewPrescription:
        editMedicationDetailMode = EditMedicationDetailMode.NewPrescription
        break

      case SearchDrugMode.NewPrescriptionDetail:
        editMedicationDetailMode = EditMedicationDetailMode.NewPrescriptionDetail
        break

      default:
        break
    }

    navigation.pop()
    navigation.navigate("Details", {
      screen: "EditMedicationDetail",
      params: { drug: selectedItem, mode: editMedicationDetailMode },
    })
  }

  const renderItem = ({ item }: { item: Drug }) => {
    const backgroundColor = selectedItem?.id === item.id ? color.palette.veryLightPink3 : null
    return (
      <Item
        item={item}
        onPress={() => {
          sendLogEvent("select_content", {
            content_type: "drug",
            item_id: item.id,
            item_list_name: item.name,
          })
          if (mode === SearchDrugMode.Search) {
            navigation.navigate("DrugV4Detail", { id: item.id })
          } else {
            setSelectedItem(item)
            Keyboard.dismiss()
          }
        }}
        style={{ backgroundColor }}
      />
    )
  }

  return (
    <Screen style={BODY_CONTAINER} unsafe>
      <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
      <View style={SEARCHBOX_INPUT}>
        <SearchIcon />
        <Input
          variant="unstyled"
          style={SEARCH_INPUT}
          placeholder="약 이름으로 검색"
          placeholderTextColor={color.placeholder}
          onChangeText={onChangeKeywords}
          value={query}
        />
      </View>
      {/* <View style={HEADER_LINE} /> */}
      <View
        style={{
          ...SEARCH_RESULTS_CONTAINER,
          paddingBottom: insets.bottom + (selectedItem ? 56 : 0),
        }}
      >
        <View style={SECTION_HEADER}>
          <Text style={typography.body}>
            검색결과 <Text style={TITLE2_ORANGE}>{count}</Text>
          </Text>
        </View>
        {searchStore.status === "pending" ? (
          <Spinner color={color.primary} style={CENTER} />
        ) : (
          <FlatList<Drug>
            ref={resultsRef}
            data={drugs}
            renderItem={renderItem}
            ItemSeparatorComponent={() => <View style={DRUG_LINE} />}
            keyExtractor={(item) => item.id.toString()}
            keyboardShouldPersistTaps="handled" // Input 콤포넌트와 함께 있을 때 포커싱 문제 해결
            onEndReached={search}
            onEndReachedThreshold={3}
          />
        )}
      </View>
      {selectedItem && (
        <View style={{ ...BOTTOM_BUTTONS, paddingBottom: insets.bottom + 12 }}>
          <Button block style={CONFIRM_BUTTON} onPress={debounce(addItem, 300)}>
            <Text style={BUTTON_TEXT}>확인</Text>
          </Button>
        </View>
      )}
    </Screen>
  )
})
