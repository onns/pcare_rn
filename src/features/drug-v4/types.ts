export type DrugsPayload = {
  result: number
  list: DrugSummary[]
}

export type DrugSummary = {
  id: number
  name: string
  image: string
  form: string
}
