import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"

export const Ingredient = types.model("Ingredient").props({
  id: types.number,
  code: types.string,
  nameEn: types.maybe(types.string),
  nameKo: types.maybe(types.string),
  /** 약효분류	obj */
  effectClass: types.model({
    /** 약효분류코드	str	 */
    code: types.string,
    /** 약효분류명칭	str	 */
    name: types.string,
  }),
  /** 성분설명	obj */
  description: types.maybe(
    types.model({
      id: types.number,
      /** 명칭	str	 */
      name: types.string,
      /** 제형 (복합)	str	 */
      forms: types.maybe(types.string),
      /** 특징 (요약)	str	 */
      feature: types.string,
      /** 효능효과	str	 */
      effect: types.string,
      /** 주의사항	str	 */
      caution: types.string,
      /** 장점	str	 */
      merit: types.string,
      /** 단점	str	 */
      fault: types.string,
    }),
  ),
  /** nps 통계정보	obj	 */
  nps: types.maybe(
    types.model({
      /** 처방질병	obj[]	 */
      disease: types.array(
        types.model({
          /** 순위	str	 */
          rank: types.string,
          /** 질병코드	str	 */
          code: types.string,
          /** 질병명칭	str	 */
          name: types.string,
          /** 수치	int	 */
          count: types.number,
          /** 비율	num	 */
          percent: types.number,
        }),
      ),
    }),
  ),
})

export const DrugModel = types.model("Drug").props({
  id: types.number,
  itemNo: types.maybe(types.string),
  name: types.string,
  /** 구분	enum	[전문약, 일반약] */
  type: types.maybe(types.enumeration(["전문약", "일반약"])),
  /** 이미지 경로	str	 */
  image: types.maybe(types.string),
  /** 제형	str	 */
  form: types.maybe(types.string),
  /** 특징 (요약)	str	 */
  feature: types.maybe(types.string),
  /** 제조/판매 사	obj	 */
  company: types.maybe(types.model({ id: types.number, name: types.string })),
  /** ATC 정보	obj	 */
  atcInfo: types.maybe(types.model({ id: types.number, code: types.string, name: types.string })),
  /** 주성분	obj[]	 */
  mainIngredient: types.array(Ingredient),
})

export interface Drug extends Instance<typeof DrugModel> {}
export interface DrugSnapshotOut extends SnapshotOut<typeof DrugModel> {}
export interface DrugSnapshotIn extends SnapshotIn<typeof DrugModel> {}
export const createDrugDefaultModel = () => types.optional(DrugModel, {})
