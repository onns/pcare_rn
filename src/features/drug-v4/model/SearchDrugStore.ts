import { applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { swrService } from "@api/swr-service"
import { sendLogEvent } from "@lib/analytics"
import { withStatus } from "@stores/extensions/with-status"

import { DrugsPayload } from "../types"
import { Drug, DrugModel } from "./Drug"

/**
 * 검색 api 를 호출하고 검색 결과를 저장하는 store
 */
export const SearchDrugStoreModel = types
  .model("SearchDrugStore")
  .props({
    prevQuery: types.maybe(types.string),
    limit: types.optional(types.number, 40),
    count: types.optional(types.number, 0),
    page: types.optional(types.number, 1),
    nextPage: types.maybeNull(types.number),
    nextUri: types.maybeNull(types.string),
    prevUri: types.maybeNull(types.string),
    drugs: types.array(DrugModel),
    drugDetail: types.maybe(DrugModel),
  })
  .extend(withStatus)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setDrug(drug: Drug) {
      if (self.drugDetail === undefined) {
        self.drugDetail = DrugModel.create(drug)
      } else {
        applySnapshot(self.drugDetail, drug)
      }
    },
  }))
  .actions((self) => ({
    initiate() {
      applySnapshot(self, {})
    },
    next: flow(function* () {
      if (self.nextUri == null) return

      const response = yield swrService.client.get<DrugsPayload>(self.nextUri)
      if (response) {
        self.drugs = self.drugs.concat(response.list)
        self.count = response.result
        self.page = self.nextPage

        const totalPages = Math.ceil(self.count / self.limit)
        self.nextUri =
          totalPages > self.page
            ? `/drugs/medi?q=${self.prevQuery}&rows=${self.limit}&page=${self.page + 1}`
            : null
        self.prevUri =
          self.page > 1
            ? `/drugs/medi?q=${self.prevQuery}&rows=${self.limit}&page=${self.page - 1}`
            : null
      } else {
        self.setStatus("error")
      }
    }),
    search: flow(function* (query: string, page?: number, limit?: number) {
      self.setStatus("pending")
      self.limit = limit

      const response = yield swrService.client.get<DrugsPayload>(
        `/drugs/medi?q=${query}&rows=${limit}&page=${page}`,
      )
      sendLogEvent("search", { search_term: query })
      if (response) {
        applySnapshot(self.drugs, response.list)
        self.count = response.result
        self.page = page
        self.setStatus("done")
        self.prevQuery = query

        const totalPages = Math.ceil(self.count / limit)
        self.nextPage = totalPages > page ? page + 1 : null
        self.nextUri =
          totalPages > page ? `/drugs/medi?q=${query}&rows=${limit}&page=${page + 1}` : null
        self.prevUri = page > 1 ? `/drugs/medi?q=${query}&rows=${limit}&page=${page - 1}` : null
      } else {
        self.setStatus("error")
      }
    }),
    fetchDrugDetail: flow(function* (id: number) {
      self.drugDetail = DrugModel.create({
        id: -1,
        name: "",
        image: "",
      })

      self.setStatus("pending")
      const response = yield swrService.client.get<Drug>(`/drugs/medi/${id}?nps=true`)
      console.tron.log(response)
      if (response) {
        self.setDrug(response)
        self.setStatus("done")
      } else {
        self.setStatus("error")
        console.tron.log("< SearchDrugStore > fetchDrugDetail, error")
      }
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type SearchDrugStoreType = Instance<typeof SearchDrugStoreModel>
export interface SearchDrugStore extends SearchDrugStoreType {}
type SearchDrugStoreSnapshotType = SnapshotOut<typeof SearchDrugStoreModel>
export interface SearchDrugStoreSnapshot extends SearchDrugStoreSnapshotType {}
