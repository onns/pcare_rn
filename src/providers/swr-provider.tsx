import React from "react"
import { AppState, AppStateStatus } from "react-native"
import { SWRConfig } from "swr"

import NetInfo from "@react-native-community/netinfo"

import { useIsOnline } from "../../lib/hooks/use-is-online"

import type { Revalidator, RevalidatorOptions } from "swr"

export const SWRProvider = ({ children }: { children: React.ReactNode }): JSX.Element => {
  const { isOnline } = useIsOnline()

  return (
    <SWRConfig
      value={{
        onError: (err) => {
          if (err?.message && __DEV__) {
            console.error(err)
            // TODO: toast 메시지 표시
          }
        },
        onErrorRetry: async (
          error: {
            status: number
          },
          key: string,
          config,
          revalidate: Revalidator,
          opts: Required<RevalidatorOptions>,
        ) => {
          const maxRetryCount = config.errorRetryCount
          const currentRetryCount = opts.retryCount

          // Exponential backoff
          const timeout =
            ~~((Math.random() + 0.5) * (1 << (currentRetryCount < 8 ? currentRetryCount : 8))) *
            config.errorRetryInterval

          if (maxRetryCount !== undefined && currentRetryCount > maxRetryCount) {
            return
          }

          if (error.status === 404) {
            return
          }

          if (error.status === 401) {
            // TODO: refresh token
          }

          setTimeout(revalidate, timeout, opts)
        },
        isVisible: () => {
          return AppState.currentState === "active"
        },
        isOnline: () => {
          return isOnline
        },
        initFocus(callback) {
          const onAppStateChange = (nextAppState: AppStateStatus) => {
            /* If it's resuming from background or inactive mode to active one */
            if (nextAppState === "active") {
              callback()
            }
          }

          // Subscribe to the app state change events
          const listener = AppState.addEventListener("change", onAppStateChange)

          return () => {
            if (listener) {
              listener.remove()
            }
          }
        },
        initReconnect(callback) {
          const unsubscribe = NetInfo.addEventListener((s) => {
            if (s.isInternetReachable && s.isConnected) {
              callback()
            }
          })

          return unsubscribe
        },
      }}
    >
      {children}
    </SWRConfig>
  )
}
