import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * 14세 미만 및 14세 이상의 가족회원 정보
 */
export const FamilyMemberModel = types
  .model("FamilyMember")
  .props({
    id: types.maybe(types.number),
    created: types.maybe(types.Date),
    modified: types.maybe(types.Date),
    name: types.maybe(types.string),
    birth_date: types.maybe(types.Date),
    gender: types.maybe(types.enumeration(["M", "F", "male", "female", ""])),
    phone: types.maybeNull(types.string),
    family_user_phone: types.maybe(types.string),
    img: types.maybeNull(types.string), //types.maybe(types.string),
    status: types.maybeNull(types.string), // STATUS = ( ('WT', '대기중'), ('CC', '취소'), ('HD', '보류중'), ('RJ', '거절'), ('AC', '승인') )
    user: types.maybe(types.number), //types.reference(types.late(() => User)))

    /**
     * 14세 이상 가족회원의 user id
     */
    family_user: types.maybe(types.number),

    /**
     * 가족회원 목록에서 각각의 가족회원을 구분하기 위한 key
     * (id 는 중복 가능성 존재)
     * eg. '_u1234', '_f3452', '_c2364'
     */
    familyMemberKey: types.maybe(types.identifier),
  })
  .views((self) => ({
    get isOver14() {
      if (self.birth_date != undefined) {
        var date = new Date()
        var year = date.getFullYear()
        var month = date.getMonth() + 1
        var day = date.getDate()

        var birthYear = self.birth_date.getFullYear()
        var birthMonth = self.birth_date.getMonth() + 1
        var birthDay = self.birth_date.getDate()

        var age =
          month < birthMonth || (month == birthMonth && day < birthDay)
            ? year - birthYear - 1
            : year - birthYear

        return age >= 14
      }
      return false
    },
    get formattedPhone() {
      let start
      let middle
      let end

      if (self.phone && self.phone.length > 3) {
        start = self.phone.substr(0, 3)
      }
      if (self.phone && self.phone.length == 10) {
        middle = self.phone.substr(3, 3)
        end = self.phone.substr(6, 4)
      } else if (self.phone && self.phone.length == 11) {
        middle = self.phone.substr(3, 4)
        end = self.phone.substr(7, 4)
      }

      return [start, middle, end].join("-")
    },
  }))
  .actions((self) => ({
    setId(id: number) {
      self.id = id
    },
    setName(name: string) {
      self.name = name.trim()
    },
    setBirthDate(date: Date) {
      self.birth_date = date
    },
    setGender(gender: "" | "M" | "F" | "male" | "female") {
      self.gender = gender
    },
    setPhone(phone: string) {
      self.phone = phone.replace(/-/g, "").trim()
    },
    setProfileImage(img: string) {
      self.img = img
    },
  }))

type FamilyMemberType = Instance<typeof FamilyMemberModel>
export interface FamilyMember extends FamilyMemberType {}
type FamilyMemberSnapshotType = SnapshotOut<typeof FamilyMemberModel>
export interface FamilyMemberSnapshot extends FamilyMemberSnapshotType {}
