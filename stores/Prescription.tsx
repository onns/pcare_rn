import { DateTime } from "luxon"
import { clone, types } from "mobx-state-tree"

import { Compliance, ComplianceModel } from "./Compliance"
import { Disease, DiseaseModel } from "./Disease"
import { DurModel } from "./Dur"
import { Doctor, Hospital, HospitalModel } from "./HospitalStore"
import { Luxon } from "./Luxon"
import { PrescriptionDetailModel } from "./PrescriptionDetail"
import { PrescriptionDetail } from "./PrescriptionStore"

// import durJson from "../assets/dur.json"

export const PrescriptionModel = types
  .model("Prescription")
  .props({
    id: types.number,
    image: types.maybeNull(types.string),
    disease: types.array(DiseaseModel),
    subdiseases: types.array(DiseaseModel),
    hospital: types.maybeNull(HospitalModel),
    user: types.maybeNull(types.number),
    upload_user: types.maybeNull(types.number),
    children: types.maybeNull(types.number),
    issue_date: types.maybeNull(types.string),
    start_at: types.maybeNull(types.string),
    end_at: types.maybeNull(types.string),
    confirm_date: types.maybeNull(types.string),
    status: types.string,
    doctor: types.maybeNull(Doctor),
    returnReason: types.maybeNull(types.string),
    prescription_details: types.array(PrescriptionDetailModel),
    issueDateObject: types.maybe(Luxon),
    todayObject: types.optional(
      Luxon,
      DateTime.local().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }),
    ),
    dur_res: types.optional(types.string, ""),
    durRes: types.maybe(DurModel),
    /** 복약순응도 */
    f_value: types.maybeNull(types.number),
    /** 증상개선도 */
    f_effect: types.maybeNull(types.number),
    f_memo: types.maybeNull(types.string),
    pendingNumber: types.maybe(types.number),
    /**
     * 남은 복용일수: 정렬할 때 이것으로 복용중인 상태인지 확인함.
     */
    remainingDosingDays: types.optional(types.number, 0),
    compliance: types.maybe(ComplianceModel),
  })
  .views((self) => ({
    get diffDays() {
      // 두 날짜 차이 구하기
      return self.todayObject.diff(self.issueDateObject, "days").days
    },
    get dosingDays() {
      let dosingDays = 0
      self.prescription_details.map((prescriptionDetail: PrescriptionDetail) => {
        if (dosingDays < prescriptionDetail.dosing_days_num) {
          dosingDays = prescriptionDetail.dosing_days_num
        }
      })
      return dosingDays
    },
    get drugsBeingTaken() {
      const drugsBeingTaken = new Array<PrescriptionDetail>()
      // 두 날짜 차이 구하기
      const diffDays = self.todayObject.diff(self.issueDateObject, "days").days
      self.prescription_details.map((prescriptionDetail: PrescriptionDetail) => {
        if (diffDays < prescriptionDetail.dosing_days_num) {
          drugsBeingTaken.push(prescriptionDetail)
        }
      })
      return drugsBeingTaken
    },
    get drugsTaken() {
      const drugsTaken = new Array<PrescriptionDetail>()
      // 두 날짜 차이 구하기
      const diffDays = self.todayObject.diff(self.issueDateObject, "days").days
      self.prescription_details.map((prescriptionDetail: PrescriptionDetail) => {
        if (diffDays >= prescriptionDetail.dosing_days_num) {
          drugsTaken.push(prescriptionDetail)
        }
      })
      return drugsTaken
    },
    /** 가장 빠른 시작일 구하기 */
    get minStartDate() {
      let minStartDate: DateTime
      for (let i = 0; i < self.prescription_details.length; i++) {
        const prescriptionDetail = self.prescription_details[i]
        if (i == 0) {
          minStartDate = prescriptionDetail.start_at
          continue
        }
        if (minStartDate > prescriptionDetail.start_at) {
          minStartDate = prescriptionDetail.start_at
        }
      }
      return minStartDate.toFormat("yyyy-MM-dd")
    },
  }))
  .actions((self) => ({
    setIssueDate(dateString: string) {
      self.issue_date = dateString
      self.issueDateObject = DateTime.fromISO(dateString)
      // self.issueDateObject = new Date(dateString)
    },
    setDurRes() {
      if (self.dur_res.length > 38 && !self.durRes) {
        try {
          self.durRes = JSON.parse(self.dur_res)
        } catch (error) {
          console.tron.logImportant(
            "< Prescription > setDurRes, *** DUR json data parsing ERROR !!!!!!!",
          )
        }
      }
      // self.durRes = JSON.parse(`{"preg": [{"detail_id": 4428, "drug_id": 105664, "drug_name": "조이록신주400밀리그램(목시플록사신)", "preg_grade": "2", "preg_detail": "임부에 대한 안전성 미확립.동물실험에서 골격기형, 유산 증가, 태자 무게감소, 출생전 사망 증가, 임신기간 연장 등 보고."}, {"detail_id": 4427, "drug_id": 15417, "drug_name": "뉴신타서방정200밀리그램(타펜타돌염산염)", "preg_grade": "2", "preg_detail": "과다한 약리작용을 초래하는 용량에서 발생 지연 및 배태자 독성 관찰"}, {"detail_id": 4426, "drug_id": 24082, "drug_name": "쎄레브렉스캡슐200밀리그램(세레콕시브)", "preg_grade": "2", "preg_detail": "임부에 대한 안전성 미확립.동물실험에서 생식독성(기형발생 포함) 보고.임신말기에 투여시 자궁무력증, 태아 동맥관조기폐쇄 가능성."}], "hng": [{"presc1_id": 1842, "detail1_id": 4426, "drug1_id": 24082, "drug1_name": "쎄레브렉스캡슐200밀리그램(세레콕시브)", "presc2_id": 1841, "detail2_id": 4430, "drug2_id": 21168, "drug2_name": "쎄브렉스캡슐200밀리그램(세레콕시브)", "hng_group": "해열진통소염제 ", "hng_groupn": "Group01"}], "dis": [{"presc1_id": 1842, "detail1_id": 4426, "drug1_id": 24082, "drug1_name": "쎄레브렉스캡슐200밀리그램(세레콕시브)", "presc2_id": 1841, "detail2_id": 4430, "drug2_id": 21168, "drug2_name": "쎄브렉스캡슐200밀리그램(세레콕시브)", "dur1": "D000138", "dur2": "", "dur3": "", "dur4": ""}], "byg": [{"presc1_id": 1927, "detail1_id": 4428, "drug1_id": 105664, "drug1_name": "조이록신주400밀리그램(목시플록사신)", "presc2_id": 1841, "detail2_id": 4429, "drug2_id": 93513, "drug2_name": "아디팜정(히드록시진염산염)", "side_effect": "심부정맥 위험 증가 가능", "threshold": ""}]}`)
      // self.durRes = require("../assets/dur.json")
    },
    setIdForSample(id: number) {
      self.id = id
    },

    setCompliance(compliance: Compliance) {
      self.compliance = compliance
    },

    setDrugAdherenceRating(rating: number) {
      self.f_value = rating
    },
    setSymptomImprovementRating(rating: number) {
      self.f_effect = rating
    },
    setMemo(text: string) {
      self.f_memo = text
    },
    setPendingNumber(number: number) {
      self.pendingNumber = number
    },
    setRemainingDosingDays(days: number) {
      self.remainingDosingDays = days
    },
    addPrescriptionDetail(prescriptionDetail: PrescriptionDetail) {
      let item
      if (prescriptionDetail.id == -1) {
        const {
          dose,
          dosing_days_num,
          drug,
          doses_num,
          per_days,
          start_at,
          end_at,
          timeslots,
        } = prescriptionDetail

        const endAt = end_at
        let correctDosingDays = dosing_days_num
        if (dosing_days_num == 1) {
          correctDosingDays =
            endAt
              .set({
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0,
              })
              .diff(start_at, "days").days + 1
        }

        item = PrescriptionDetailModel.create({
          id: self.prescription_details.length,
          drug: clone(drug),
          dose: dose,
          unit: "정", // view 에서 미사용?
          doses_num: doses_num,
          per_days: per_days,
          dosing_days_num: correctDosingDays,
          start_at: start_at,
          end_at: endAt,
          timeslots: clone(timeslots),
          prescription: -1,
        })
      } else {
        item = clone(prescriptionDetail)
      }
      self.prescription_details.push(item)
    },
    removePrescriptionDetail(prescriptionDetail: PrescriptionDetail) {
      self.prescription_details.remove(prescriptionDetail)
    },
    removeDisease(disease: Disease, isSubDisease: boolean) {
      if (!isSubDisease) {
        self.disease.remove(disease)
      } else {
        self.subdiseases.remove(disease)
      }
    },
    removeHospital(hospital: Hospital) {
      if (hospital) {
        self.hospital = null
      }
    },
    addDisease(disease: Disease) {
      // TODO: 검토 필요
      const copyDisease = clone(disease)
      if (copyDisease.feature) {
        copyDisease.setRepTitle(copyDisease.feature)
      } else if (copyDisease.one_liner) {
        copyDisease.setRepTitle(copyDisease.one_liner)
      }
      if (self.disease.length === 0) {
        self.disease.push(copyDisease)
      } else {
        self.subdiseases.push(copyDisease)
      }
    },
    setHospital(hospital: Hospital) {
      self.hospital = hospital ? clone(hospital) : null
    },
  }))
