import { applySnapshot, destroy, flow, getEnv, IModelType, types } from "mobx-state-tree"

import { api } from "@api/api"

import { GetFamilyResult, User as UserType } from "../api/api.types"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { RequestAccount } from "./account/RequestAccount"
import { UserModel } from "./account/User"
import { Environment } from "./environment"
import { FamilyMember, FamilyMemberModel } from "./FamilyMember"
import { NoticeModel } from "./Notice"

export type __IModelType = IModelType<any, any>

export const AccountStoreModel = types
  .model("AccountStore")
  .props({
    reqAccount: types.maybe(RequestAccount),
    user: UserModel,
    family: types.array(FamilyMemberModel),
    familyToAgree: types.array(FamilyMemberModel),
    familyToRequest: types.array(FamilyMemberModel),
    myPrescriptionManagers: types.array(FamilyMemberModel),
    selectedMemberKey: types.maybe(types.string),
    selectedMemberName: types.optional(types.string, ""),
    selectedMemberProfileImage: types.maybeNull(types.string),
    signPath: types.optional(
      types.enumeration(["email", "facebook", "kakao", "naver", "apple"]),
      "email",
    ),
    status: types.optional(types.enumeration(["idle", "pending", "done", "error"]), "idle"),
    /**
     * pinCodeEnabled 가 true 인 경우, BG -> FG 시 잠금화면이 뜸.
     * SNS로그인, 사진첩불러오기 등 잠금화면이 뜰 필요없는 케이스에 일시적으로 true 로 변경하고 해당 케이스 작업이 끝난 후 false 로 변경할 것.
     */
    temporaryPinCodeDisabled: types.optional(types.boolean, false),

    noticeList: types.array(NoticeModel),
    noticeDetail: types.maybe(NoticeModel),
    lastNoticeId: types.maybe(types.number),
  })
  .actions((self) => ({
    setFamily(family: any) {
      self.family = family
    },
    addFamilyMember(member: any) {
      self.family.push(member)
    },
    changeFamilyMember(index: number, member: any) {
      self.family.splice(index, 1, member)
    },
    removeFamilyMember(index: number, pStore: PrescriptionStore) {
      pStore.setStatus("pending")
      if (self.selectedMemberKey === self.family[index].familyMemberKey) {
        self.selectedMemberKey = `_u${self.user.id}`
      }
      self.family.splice(index, 1)
      pStore.setStatus("done")
    },
    removeFamilyToRequest(index: number, pStore: PrescriptionStore) {
      self.familyToRequest.splice(index, 1)
    },
    removeMyManager(index: number, pStore: PrescriptionStore) {
      self.myPrescriptionManagers.splice(index, 1)
    },
    setUser(user: UserType) {
      self.user.username = user.username
      self.user.show_activity_button = user.show_activity_button
      self.user.email = user.email
      self.user.first_name = user.first_name
      self.user.last_name = user.last_name
      self.user.password = user.password
      self.user.user_type = user.user_type
      self.user.name = user.name
      self.user.nickname = user.nickname
      self.user.address = user.address
      self.user.is_authenticated = user.is_authenticated
      self.user.is_agreed = user.is_agreed
      self.user.phone = user.phone
      self.user.profile_image = user.profile_image

      self.user.birth = user.birth
      self.user.gender = user.gender
      ;(self.user.firebase_token0 = user.firebase_token0),
        (self.user.dur_preg = user.dur_preg),
        (self.user.dur_byg = user.dur_byg),
        (self.user.dur_hng = user.dur_hng),
        (self.user.dur_dis = user.dur_dis),
        (self.user.dur_nosale = user.dur_nosale),
        (self.user.presc_noti = user.presc_noti),
        (self.user.dur_noti = user.dur_noti)
      self.user.ad_noti = user.ad_noti
      self.user.ad_noti_date = user.ad_noti_date
      self.user.newly_agreed = user.newly_agreed
      self.user.newly_agreed_date = user.newly_agreed_date

      self.selectedMemberKey = `_u${self.user.id}`
      self.selectedMemberName = user.nickname
      self.selectedMemberProfileImage = user.profile_image
    },
    createRequestAccount() {
      if (self.reqAccount != undefined) {
        destroy(self.reqAccount)
      }
      self.reqAccount = RequestAccount.create()
    },
    initiate() {
      applySnapshot(self, {
        user: {
          id: -1,
        },
        family: [],
        familyToAgree: [],
      })
      // destroy(self.user)
      // self.user = User.create({ id: -1 })
      // destroy(self.family)
    },
    setSelectedMemberKey(memberKey: string) {
      self.selectedMemberKey = memberKey
    },
    setSelectedMemberName(name: string) {
      self.selectedMemberName = name
    },
    setSelectedMemberProfileImage(profileImage: string) {
      self.selectedMemberProfileImage = profileImage
    },
    setSignPath(path: "email" | "facebook" | "kakao" | "naver" | "apple") {
      self.signPath = path
    },
    hasName(name: string, memberId?: number): boolean {
      if (self.user.name === name) {
        return true
      } else {
        for (let i = 0; i < self.family.length; i++) {
          if (self.family[i].id != memberId && self.family[i].name === name) {
            return true
          }
        }
      }
      return false
    },
    setStatus(value: "idle" | "pending" | "done" | "error") {
      self.status = value
    },
    setTemporaryPinCodeDisabled(disabled: boolean) {
      self.temporaryPinCodeDisabled = disabled
    },
    setNoticeList(noticeList: any) {
      self.noticeList = noticeList
    },
    setNoticeDetail(noticeDetail: any) {
      self.noticeDetail = noticeDetail
    },
    setLastNoticeId(lasNoticeId: number) {
      self.lastNoticeId = lasNoticeId
    },
  }))
  .views((self) => ({
    get environment() {
      return getEnv(self) as Environment
    },
    get countFamilyUnder14() {
      let count = 0
      self.family.map((member) => {
        if (!member.isOver14) {
          count = count + 1
        }
      })
      return count
    },
    get isNoticeNew() {
      if (self.noticeList.length == 0) {
        return false
      } else if (self.noticeList.length > 0) {
        return self.lastNoticeId != self.noticeList[0].id
      }
      return true
    },
  }))
  .actions((self) => ({
    fetchUser: flow(function* () {
      if (!api.apisauce.headers.Authorization) return

      const result = yield api.getMyInfo(self.user.id)
      if (result.kind === "ok") {
        self.setUser(result.user)
      } else {
        console.tron.log("< AccountStore > fetchUser, error")
      }
    }),
    fetchFamily: flow(function* () {
      if (!api.apisauce.headers.Authorization) return
      if (self.status === "pending" && self.family.length > 0) return

      // try {
      const result = yield api.getMyChildren()
      if (result.kind === "ok") {
        if (result.family.length > 0) {
          // 프로필 사진 깜빡임 문제 해결
          if (self.family.length > 0) {
            result.family.map((resultMember: FamilyMember) => {
              let exist = false
              for (let i = 0; i < self.family.length; i++) {
                const member = self.family[i]
                if (member.familyMemberKey === resultMember.familyMemberKey) {
                  exist = true
                }
              }
              if (!exist) {
                self.addFamilyMember(resultMember)
              }
            })
          } else {
            applySnapshot(self.family, result.family)
          }
        }
      } else {
        console.tron.log("< AccountStore > getMyChildren, error")
      }

      const result2: GetFamilyResult = yield api.getMyFamily(self.user.id, self.family)
      if (result2.kind === "ok") {
        if (result2.family.length > 0) {
          if (self.family.length > 0) {
            result2.family.map((resultMember: FamilyMember) => {
              let exist = false
              for (let i = 0; i < self.family.length; i++) {
                const member = self.family[i]
                if (member.familyMemberKey === resultMember.familyMemberKey) {
                  exist = true
                }
              }
              if (!exist) {
                self.addFamilyMember(resultMember)
              }
            })
          } else {
            applySnapshot(self.family, self.family.concat(result2.family))
          }
        }
      } else {
        console.tron.log("< AccountStore > getMyFamily, error")
      }
    }),
    fetchFamilyToRequest: flow(function* () {
      if (!api.apisauce.headers.Authorization) return
      const result = yield api.getFamilyToRequest(self.user.id)
      if (result.kind === "ok") {
        applySnapshot(self.familyToRequest, result.family)
      } else {
        console.tron.log("< AccountStore > getFamilyToRequest, error")
      }
    }),

    fetchFamilyToAgree: flow(function* () {
      if (!api.apisauce.headers.Authorization) return
      const result = yield api.getMyFamilyToAgree(self.user.id)
      if (result.kind === "ok") {
        if (result.family.length > 0 || self.familyToAgree.length > 0) {
          applySnapshot(self.familyToAgree, result.family)
        }
      } else {
        console.tron.log("< AccountStore > getMyFamilyToAgree, error")
      }
    }),
    fetchMyPrescriptionManagers: flow(function* () {
      if (!api.apisauce.headers.Authorization) return
      const result = yield api.getMyPrescriptionManagers(self.user.id)
      if (result.kind === "ok") {
        if (result.family.length > 0) {
          applySnapshot(self.myPrescriptionManagers, result.family)
        }
      } else {
        console.tron.log("< AccountStore > fetchMyPrescriptionManagers, error")
      }
    }),

    /** 공지사항 */
    fetchNoticeList: flow(function* () {
      const result = yield api.getNoticeList()
      if (result.kind === "ok") {
        applySnapshot(self.noticeList, result.data)
      } else {
        console.tron.log("< AccountStore > fetchNoticeList, error")
      }
    }),

    fetchNoticeDetail: flow(function* (id: number) {
      self.setStatus("pending")
      const result = yield api.getNoticeDetail(id)
      if (result.kind === "ok") {
        self.setNoticeDetail(result.data)
      } else {
        console.tron.log("< AccountStore > fetchNoticeDetail, error")
      }
      self.setStatus("done")
    }),

    /** 개발자 모드 변경 */
    changeDeveloperMode: flow(function* (type: number) {
      yield api.changeDeveloperMode(type)
    }),
  }))

export type AccountStore = typeof AccountStoreModel.Type
