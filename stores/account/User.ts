import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const UserModel = types
  .model("User")
  .props({
    id: types.number,
    show_activity_button: types.maybe(types.boolean),
    username: types.optional(types.string, ""),
    email: types.optional(types.string, ""),
    first_name: types.optional(types.string, ""),
    last_name: types.optional(types.string, ""),
    profile_image: types.maybeNull(types.string),
    password: types.optional(types.string, ""),
    name: types.optional(types.string, ""),
    nickname: types.optional(types.string, ""),
    birth: types.maybeNull(types.Date),
    gender: types.optional(types.enumeration(["male", "female", ""]), ""),
    phone: types.maybeNull(types.string),
    is_authenticated: types.optional(types.boolean, false),
    is_agreed: types.optional(types.boolean, false),
    user_type: types.optional(types.string, ""),
    /**
     * choice = { 'y' (모두 동의), 'n' (모두 거절), 'pre' (이전 것만 동의), 'post' (이후 것만 동의) }
     */
    newly_agreed: types.maybeNull(types.enumeration(["y", "n", "pre", "post"])),
    newly_agreed_date: types.maybeNull(types.Date),
    address: types.optional(types.string, ""),
    // token: types.optional(types.string, ""),
    firebase_token0: types.optional(types.string, ""),
    /**
     * 임부금기 PUSH 허용 여부
     */
    dur_preg: types.optional(types.enumeration(["y", "n"]), "n"),
    /**
     * 병용 금기 PUSH 허용 여부
     */
    dur_byg: types.optional(types.enumeration(["y", "n"]), "n"),
    /**
     * 효능군 중복 PUSH 허용 여부
     */
    dur_hng: types.optional(types.enumeration(["y", "n"]), "n"),
    /**
     * 동일 성분 중복 PUSH 허용 여부
     */
    dur_dis: types.optional(types.enumeration(["y", "n"]), "n"),
    /**
     * 판매중지 의약품 PUSH 허용 여부
     */
    dur_nosale: types.optional(types.enumeration(["y", "n"]), "n"),
    /**
     * 처방전 처리 완료 PUSH 허용 여부
     */
    presc_noti: types.optional(types.enumeration(["y", "n"]), "n"),
    /**
     * DUR 알림 허용 여부
     */
    dur_noti: types.optional(types.enumeration(["y", "n"]), "n"),
    /**
     * 마케팅 정보 수신 허용 여부
     */
    ad_noti: types.optional(types.enumeration(["y", "n"]), "n"),
    /**
     * 마케팅 정보 수신 동의한 날짜
     */
    ad_noti_date: types.maybeNull(types.Date),
  })
  .actions((self) => ({
    setNickname(nickname: string) {
      self.nickname = nickname
    },
    // setAddress(address: string) {
    //   self.address = address
    // },
    setShowActivityButton(show_activity_button: boolean) {
      self.show_activity_button = show_activity_button
    },
    setEmail(email: string) {
      self.email = email
    },
    setPassword(password: string) {
      self.password = password
    },
    setId(id: number) {
      self.id = id
    },
    setAddress(address: string) {
      self.address = address
    },
    setProfileImage(image: string) {
      self.profile_image = image
    },
    setCertificated(certificated: boolean) {
      self.is_authenticated = certificated
    },
    setFirebaseToken0(token: string) {
      self.firebase_token0 = token
    },
    setDurPreg(value: "y" | "n") {
      self.dur_preg = value
    },
    setDurByg(value: "y" | "n") {
      self.dur_byg = value
    },
    setDurNosale(value: "y" | "n") {
      self.dur_nosale = value
    },
    setDurHng(value: "y" | "n") {
      self.dur_hng = value
    },
    setDurDis(value: "y" | "n") {
      self.dur_dis = value
    },
    setDurNoti(value: "y" | "n") {
      self.dur_noti = value
    },
    setPrescNoti(value: "y" | "n") {
      self.presc_noti = value
    },
    setAdNoti(value: "y" | "n") {
      self.ad_noti = value
    },
    setAdNotiDate(date: Date) {
      self.ad_noti_date = date
    },
    setNewlyAgreed(value: "y" | "n" | "pre" | "post") {
      self.newly_agreed = value
    },
    setNewlyAgreedDate(date: Date) {
      self.newly_agreed_date = date
    },
  }))

type UserType = Instance<typeof UserModel>
export interface User extends UserType {}
type UserSnapshotType = SnapshotOut<typeof UserModel>
export interface UserSnapshot extends UserSnapshotType {}
