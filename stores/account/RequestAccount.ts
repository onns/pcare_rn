import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { UserModel } from "./User"

export const RequestAccount = types
  .model("RequestAccount", {
    email: types.optional(types.string, ""),
    password: types.optional(types.string, ""),
    password2: types.optional(types.string, ""),
    emailError: types.optional(types.string, ""),
    passwordError: types.optional(types.string, ""),
    passwordError2: types.optional(types.string, ""),
    isValid: types.optional(types.boolean, false),
    mode: types.optional(types.enumeration(["login", "signUp", "reset"]), "signUp"),
  })
  .actions((self) => {
    function emailOnChange(email: string) {
      self.email = email
      validateEmail()
    }
    function validateEmail() {
      const emailPatter = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
      const required = self.email ? "" : "이메일 주소를 입력해 주세요."
      self.emailError = required
        ? required
        : emailPatter.test(self.email)
        ? ""
        : "올바른 이메일 주소를 입력해 주세요."
    }
    function passwordOnChange(password: string) {
      self.password = password
      validatePassword()
    }
    function password2OnChange(password2: string) {
      self.password2 = password2
      validatePassword()
    }
    function validatePassword() {
      // const alphaNumeric = /[^a-zA-Z0-9#?!@$%^&*- ]/i.test(self.password) ? "Only alphanumeric characters" : "";
      const alphaNumeric = /^[a-zA-Z]{1,28}[0-9][0-9a-zA-Z#?!@$%^&*-]{0,28}/i.test(self.password)
        ? ""
        : "비밀번호는 영문으로 시작하며 숫자를 포함하여 8자 이상으로 설정이 가능합니다."
      const maxLength = self.password.length > 15 ? "15자 이하의 비밀번호를 입력해 주세요." : "" //"Must be 15 characters or less"
      const minLength = self.password.length < 8 ? "8자 이상의 비밀번호를 입력해 주세요." : "" //"Must be 8 characters or more"
      const samePassword =
        self.password !== self.password2
          ? `비밀번호가 서로 다르게 입력되었습니다. \n다시 확인하여 입력해주세요.`
          : "" //"Must be 15 characters or less"
      const required = self.password ? "" : "비밀번호를 입력해 주세요."
      const required2 = self.password2 ? "" : "비밀번호를 모두 입력해 주세요."
      if (self.mode === "signUp") {
        self.passwordError = required
          ? required
          : alphaNumeric
          ? alphaNumeric
          : maxLength
          ? maxLength
          : minLength
      } else {
        self.passwordError = required ? required : maxLength ? maxLength : minLength
      }
      self.passwordError2 = required2 ? required2 : samePassword ? samePassword : ""
    }
    function validateForm() {
      if (
        self.mode === "login" &&
        self.emailError === "" &&
        self.passwordError === "" &&
        self.email !== "" &&
        self.password !== ""
      ) {
        self.isValid = true
      } else if (
        self.mode === "signUp" &&
        self.password === self.password2 &&
        self.emailError === "" &&
        self.passwordError === "" &&
        self.email !== "" &&
        self.password !== ""
      ) {
        self.isValid = true
      } else if (self.mode === "reset" && self.emailError === "" && self.email !== "") {
        self.isValid = true
      } else {
        self.isValid = false
      }
    }
    function clearStore() {
      self.email = ""
      self.isValid = false
      self.emailError = ""
      self.password = ""
      self.password2 = ""
      self.passwordError = ""
      self.passwordError2 = ""
    }
    function setMode(mode: "login" | "signUp" | "reset") {
      self.mode = mode
    }
    function setEmail(email: string) {
      self.email = email
    }
    return {
      emailOnChange,
      validateEmail,
      passwordOnChange,
      password2OnChange,
      validatePassword,
      validateForm,
      clearStore,
      setMode,
      setEmail,
    }
  })

type UserType = Instance<typeof UserModel>
export interface User extends UserType {}
type UserSnapshotType = SnapshotOut<typeof UserModel>
export interface UserSnapshot extends UserSnapshotType {}
