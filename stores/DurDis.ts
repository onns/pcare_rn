import { types } from "mobx-state-tree" 
import { DrugModel } from "./Drug"

// 'dis': [ {'presc1_id': 1,'drug1_id': 1, 'drug1_name', 'XX정', 'presc2_id':2, 'drug2_id': 2, 'drug2_name', 'XX' 'dur1': 'D000282', 'dur2':'', 'dur3':'', dur4:''}, ]
// "dis": [{"presc1_id": 1842, "detail1_id": 4426, "drug1_id": 24082, "drug1_name": "쎄레브렉스캡슐200밀리그램(세레콕시브)", "presc2_id": 1841, "detail2_id": 4430, "drug2_id": 21168, "drug2_name": "쎄브렉스캡슐200밀리그램(세레콕시브)", "dur1": "D000138", "dur2": "", "dur3": "", "dur4": ""}], 

export const DurDisModel = types.model("DurDis")
.props({
  presc1_id: types.number,
  detail1_id: types.number,
  // drug1_id: types.reference(DrugModel),
  drug1_id: types.number,
  drug1_name: types.string,
  presc2_id: types.number,
  detail2_id: types.number,
  // drug2_id: types.reference(DrugModel),
  drug2_id: types.number,
  drug2_name: types.string,
  dur1: types.string,
  dur2: types.string,
  dur3: types.string,
  dur4: types.string,
})

type DurDisType = typeof DurDisModel.Type
export interface DurDis extends DurDisType {}