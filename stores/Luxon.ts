import { types } from "mobx-state-tree"
import { DateTime } from "luxon"

export const Luxon = types.custom<string, DateTime>({
  name: "Luxon",
  fromSnapshot(value: string) {
    return DateTime.fromISO(value)
  },
  toSnapshot(value: DateTime) {
    return value.toISO()
  },
  isTargetType(value: string | DateTime): boolean {
    return value instanceof DateTime
  },
  getValidationMessage(value: string): string {
    if (
      /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(\.\d+([+-][0-2]\d:[0-5]\d|Z))?/.test(
        value
      )
    ) {
      return ""
    }
    return `'${value}' doesn't look like 'YYYY-MM-DDTHH:mm:ss.SSSZ'`
  }
})
