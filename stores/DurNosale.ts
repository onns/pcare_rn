import { types } from "mobx-state-tree"

export const DurNosaleModel = types.model("DurPregnancy").props({
  detail_id: types.identifierNumber,
  drug_id: types.number,
  drug_name: types.string,
  group: types.string
})

type DurNosaleType = typeof DurNosaleModel.Type
export interface DurNosale extends DurNosaleType {}
