import { DateTime } from "luxon"
import { getEnv, types } from "mobx-state-tree"

import { Environment } from "./environment"
import { Luxon } from "./Luxon"

/** 공지사항 */
export const NoticeModel = types
  .model("Notice")
  .props({
    id: types.identifierNumber,
    title: types.string,
    created: types.Date,
    subtitle: types.optional(types.string, ""),
    content: types.optional(types.string, ""),
    createdObject: types.maybe(Luxon),
    todayObject: types.optional(
      Luxon,
      DateTime.local().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }),
    ),
  })
  .views((self) => ({
    get createdDate() {
      return self.created
    },
    get environment() {
      return getEnv(self) as Environment
    },
    get diffDays() {
      // 두 날짜 차이 구하기
      return self.todayObject.diff(DateTime.fromJSDate(self.created)).days
    },
  }))

type NoticeType = typeof NoticeModel.Type
export interface Notice extends NoticeType {}
