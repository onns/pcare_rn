import { flow, getEnv, IModelType, types } from "mobx-state-tree"

import { api } from "@api/api"

import { Environment } from "./environment"

export type __IModelType = IModelType<any, any>

function insert(str: string, index: number, value: string) {
  return str.substr(0, index) + value + str.substr(index)
}

/*
  doctor_number = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=100)
  hospital = models.ForeignKey(Hospital, related_name='hospital_doctors', on_delete=models.SET_NULL, null=True, blank=False)
  medical_specialty = models.CharField(max_length=100)
  specialty_license = models.CharField(max_length=100)
  medical_school = models.CharField(max_length=100)
  graduate_year = models.IntegerField()
  academic_association = models.CharField(max_length=100)
  career = models.CharField(blank=True, default="", max_length=1000)
*/
export const Doctor = types.model("Doctor").props({
  id: types.number,
  name: types.string,
  major: types.maybeNull(types.string),
  narrow_major: types.maybeNull(types.string),
  // hospital: types.maybe(types.reference(types.late(() => Hospital))),
  medical_specialty: types.maybeNull(types.string),
  specialty_license: types.maybeNull(types.string),
  medical_school: types.maybeNull(types.string),
  graduate_year: types.maybeNull(types.number),
  academic_association: types.maybeNull(types.string),
  career: types.maybeNull(types.string),
})

export const SpecialtyModel = types.model("Specialty").props({
  m_specialty_name: "내과",
  doctor_num: 0,
})

export const HospitalModel = types.model("Hospital").props({
  id: types.identifierNumber,
  name: types.string,
  address: types.maybeNull(types.optional(types.string, "")),
  x_pos: types.maybeNull(types.optional(types.string, "")),
  y_pos: types.maybeNull(types.optional(types.string, "")),
  phone: types.maybeNull(types.optional(types.string, "")),
  web: types.maybeNull(types.optional(types.string, "")),
  mon_open_time: types.maybeNull(types.optional(types.string, "")),
  mon_close_time: types.maybeNull(types.optional(types.string, "")),
  tue_open_time: types.maybeNull(types.optional(types.string, "")),
  tue_close_time: types.maybeNull(types.optional(types.string, "")),
  wed_open_time: types.maybeNull(types.optional(types.string, "")),
  wed_close_time: types.maybeNull(types.optional(types.string, "")),
  thu_open_time: types.maybeNull(types.optional(types.string, "")),
  thu_close_time: types.maybeNull(types.optional(types.string, "")),
  fri_open_time: types.maybeNull(types.optional(types.string, "")),
  fri_close_time: types.maybeNull(types.optional(types.string, "")),
  sat_open_time: types.maybeNull(types.optional(types.string, "")),
  sat_close_time: types.maybeNull(types.optional(types.string, "")),
  sun_open_time: types.maybeNull(types.optional(types.string, "")),
  sun_close_time: types.maybeNull(types.optional(types.string, "")),
  holi_open_time: types.maybeNull(types.optional(types.string, "")),
  holi_close_time: types.maybeNull(types.optional(types.string, "")),
  hospital_specialties: types.array(SpecialtyModel),
  // class_code: types.number,
  // class_name: types.string,
  // address: types.string,
  // x_pos: types.string,
  // y_pos: types.string,
  // phone: types.string,
  // web: types.optional(types.string, ""),
  // type_code: types.number,
  // est_date: types.Date,
  // closed_date: types.maybe(types.Date),
  // total_doctor_num: types.number,
  // gp_num: types.number,
  // intern_num: types.number,
  // regident_num: types.number,
  // specialist_num: types.number,
  // exist_weekly_em: types.boolean,
  // weekly_em_phone1: types.string,
  // weekly_em_phone2: types.string,
  // exist_night_em: types.boolean,
  // night_em_phone1: types.string,
  // night_em_phone2: types.string,
  // weekly_rcv_time: types.string,
  // sat_rcv_time: types.string,
  // weekly_lunch_time: types.string,
  // sat_lunch_time: types.string,
  // holiday_close_info: types.string,
  // sunday_close_info: types.string,
  // etc_info: types.optional(types.string, ""),

  // avail_parking_car_num: types.number,
  // avail_parking: types.boolean,

  // mon_open_time: types.optional(types.string, ""),
  // mon_close_time: types.optional(types.string, ""),
  // tue_open_time: types.optional(types.string, ""),
  // tue_close_time: types.optional(types.string, ""),
  // wed_open_time: types.optional(types.string, ""),
  // wed_close_time: types.optional(types.string, ""),
  // thu_open_time: types.optional(types.string, ""),
  // thu_close_time: types.optional(types.string, ""),
  // fri_open_time: types.optional(types.string, ""),
  // fri_close_time: types.optional(types.string, ""),
  // sat_open_time: types.optional(types.string, ""),
  // sat_close_time: types.optional(types.string, ""),

  // doctors: types.optional(types.array(Doctor), [])
})
// .preProcessSnapshot(snapshot => ({
//   // auto convert strings to booleans as part of preprocessing
//   mon_open_time: insert(snapshot.mon_open_time, snapshot.mon_open_time.length-3, ':')
//   // mon_open_time: snapshot.mon_open_time === "true" ? true : snapshot.done === "false" ? false : snapshot.done
// }))
// .actions(self => ({
//   setHospital(hospital: any) {
//     if (hospital != null) {
//       self.hospital = hospital
//     }
//   }
// }))
// .create()

export const HospitalStoreModel = types
  .model("HospitalStore")
  .props({
    hospital: types.maybe(HospitalModel),
    status: types.optional(types.enumeration(["idle", "pending", "done", "error"]), "idle"),
  })
  .actions((self) => ({
    setStatus(value: "idle" | "pending" | "done" | "error") {
      self.status = value
    },
    setHospital(hospital: Hospital) {
      self.hospital = HospitalModel.create(hospital)
    },
  }))
  .views((self) => ({
    get environment() {
      return getEnv(self) as Environment
    },
  }))
  .actions((self) => ({
    fetchHospitalDetail: flow(function* (id: number) {
      self.setStatus("pending")
      const result = yield api.getHospitalDetail(id)
      if (result.kind === "ok") {
        self.setHospital(result.data)
        self.setStatus("done")
      } else {
        self.setStatus("error")
        console.tron.log("< HospitalStore > fetchHospitalDetail, error")
      }
    }),
  }))

// export default HospitalStore
export type Hospital = typeof HospitalModel.Type
export type HospitalStore = typeof HospitalStoreModel.Type
export type Specialty = typeof SpecialtyModel.Type

// export default Hospital
