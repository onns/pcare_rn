import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * 질병-유병률
 */
export const DiseasePrevalenceModel = types
  .model("DiseasePrevalence")
  .props({
    disease_code: types.string,

    /**
     * 처방전 발급연도
     */
    year: types.number,

    /**
     * 연령대
     */
    age: types.string,

    /**
     * 1: 남자, 2: 여자
     */
    gender: types.number,

    /** 환자수 */
    count: types.number,

    /**
     * 환자수 비율
     */
    percent: types.number
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars


type DiseasePrevalenceType = Instance<typeof DiseasePrevalenceModel>
export interface DiseasePrevalence extends DiseasePrevalenceType {}
type DiseasePrevalenceSnapshotType = SnapshotOut<typeof DiseasePrevalenceModel>
export interface DiseasePrevalenceSnapshot extends DiseasePrevalenceSnapshotType {}
