import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * 질병-과목
 */
export const DiseaseDepartmentModel = types
  .model("DiseaseDepartment")
  .props({
    disease_code: types.string,

    /** 처방 과목 */
    department: types.string,

    /** 환자수 */
    count: types.number,

    /** 순위 */
    rank: types.string,

    /** 환자수 비율 */
    percent: types.number,
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars


type DiseaseDepartmentType = Instance<typeof DiseaseDepartmentModel>
export interface DiseaseDepartment extends DiseaseDepartmentType {}
type DiseaseDepartmentSnapshotType = SnapshotOut<typeof DiseaseDepartmentModel>
export interface DiseaseDepartmentSnapshot extends DiseaseDepartmentSnapshotType {}
