import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * 질병-동반질병
 */
export const DiseaseComorbModel = types
  .model("DiseaseComorb")
  .props({
    disease_code: types.string,

    /** 동반질병 코드 */
    comorb_code: types.string,
    comorb_name_kr: types.string,

    /** 환자수 */
    count: types.number,

    /** 순위 */
    rank: types.string,

    /** 환자수 비율 */
    percent: types.number,
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars


type DiseaseComorbType = Instance<typeof DiseaseComorbModel>
export interface DiseaseComorb extends DiseaseComorbType {}
type DiseaseComorbSnapshotType = SnapshotOut<typeof DiseaseComorbModel>
export interface DiseaseComorbSnapshot extends DiseaseComorbSnapshotType {}
