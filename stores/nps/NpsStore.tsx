import { applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { api } from "@api/api"

import { withStatus } from "../extensions"
import { DiseaseComorbModel } from "./DiseaseComorb"
import { DiseaseDepartmentModel } from "./DiseaseDepartment"
import { DiseasePrevalenceModel } from "./DiseasePrevalence"
import { IngredientDiseaseModel } from "./IngredientDisease"

/**
 * Model description here for TypeScript hints.
 */
export const NpsStoreModel = types
  .model("NpsStore")
  .props({
    diseasePrevalence: types.array(DiseasePrevalenceModel),
    diseaseDepartment: types.array(DiseaseDepartmentModel),
    diseaseComorb: types.array(DiseaseComorbModel),
    ingredientDisease: types.array(IngredientDiseaseModel),
  })
  .views((self) => ({
    get prevalenceOfMen() {
      if (self.diseasePrevalence) {
        return self.diseasePrevalence.filter((value) => value.gender == 1)
      } else {
        return []
      }
    },
    get prevalenceOfWomen() {
      if (self.diseasePrevalence) {
        return self.diseasePrevalence.filter((value) => value.gender == 2)
      } else {
        return []
      }
    },
    get maxPercentOfPrevalence() {
      let percentMax = 0
      for (let i = 0; i < self.diseasePrevalence.length; i++) {
        const element = self.diseasePrevalence[i]
        if (percentMax < element.percent) {
          percentMax = element.percent
        }
      }
      return percentMax
    },
    get averagePercentOfPrevalence() {
      let average = 0
      let totalCount = 0
      for (let i = 0; i < self.diseasePrevalence.length; i++) {
        const element = self.diseasePrevalence[i]
        average += element.percent * element.count
        totalCount += element.count
      }
      average = average / totalCount
      return average
    },
    // get xAxisOfPrevalence() {
    //   if (self.diseasePrevalence) {
    //     let percentMax = 0
    //     for (let i = 0; i < self.diseasePrevalence.length; i++) {
    //       const element = self.diseasePrevalence[i]
    //       if (percentMax < element.percent) {
    //         percentMax = element.percent
    //       }
    //     }
    //   } else {
    //     return []
    //   }
    // },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .extend(withStatus)
  .actions((self) => ({
    clearIngredientDisease() {
      self.ingredientDisease.clear()
    },
    fetchDiseasePrevalence: flow(function* (diseaseCode: string) {
      self.setStatus("pending")
      const result = yield api.getDiseasePrevalence(diseaseCode)

      if (result.kind === "ok") {
        applySnapshot(self.diseasePrevalence, result.data)
        self.setStatus("done")
      } else if (result.kind === "cannot-connect" || result.temporary) {
        self.setStatus("done")
      } else {
        self.setStatus("error")
      }
    }),
    fetchDiseaseDepartment: flow(function* (diseaseCode: string) {
      self.setStatus("pending")
      const result = yield api.getDiseaseDepartment(diseaseCode)

      if (result.kind === "ok") {
        applySnapshot(self.diseaseDepartment, result.data)
        self.setStatus("done")
      } else if (result.kind === "cannot-connect" || result.temporary) {
        self.setStatus("done")
      } else {
        self.setStatus("error")
      }
    }),
    fetchDiseaseComorb: flow(function* (diseaseCode: string) {
      self.setStatus("pending")
      const result = yield api.getDiseaseComorb(diseaseCode)

      if (result.kind === "ok") {
        applySnapshot(self.diseaseComorb, result.data)
        self.setStatus("done")
      } else if (result.kind === "cannot-connect" || result.temporary) {
        self.setStatus("done")
      } else {
        self.setStatus("error")
      }
    }),
    fetchIngredientDisease: flow(function* (ingredientCode: string) {
      self.setStatus("pending")
      const result = yield api.getIngredientDisease(ingredientCode)

      if (result.kind === "ok") {
        applySnapshot(self.ingredientDisease, result.data)
        self.setStatus("done")
      } else if (result.kind === "cannot-connect" || result.temporary) {
        self.setStatus("done")
      } else {
        self.setStatus("error")
      }
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type NpsStoreType = Instance<typeof NpsStoreModel>
export interface NpsStore extends NpsStoreType {}
type NpsStoreSnapshotType = SnapshotOut<typeof NpsStoreModel>
export interface NpsStoreSnapshot extends NpsStoreSnapshotType {}
