import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * 약제-동반질병
 */
export const IngredientDiseaseModel = types
  .model("IngredientDisease")
  .props({

    /** 해당 약제의 ingredient code - 마지막 한개의 letter 제외 */
    ing_code: types.string,

    disease_code: types.string,

    /** 동반질병명 */
    disease_name_kr: types.string, 

    /** 환자수 */
    count: types.number, 

    /** 정렬순위 */
    rank: types.string, 

    /** 환자수 비율 */
    percent: types.number
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars


type IngredientDiseaseType = Instance<typeof IngredientDiseaseModel>
export interface IngredientDisease extends IngredientDiseaseType {}
type IngredientDiseaseSnapshotType = SnapshotOut<typeof IngredientDiseaseModel>
export interface IngredientDiseaseSnapshot extends IngredientDiseaseSnapshotType {}
