import { applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { api } from "@api/api"

import { sendLogEvent } from "../lib/analytics"
import { Disease, DiseaseModel } from "./Disease"
import { DrugModel } from "./Drug"
import { withStatus } from "./extensions/with-status"
import { Hospital, HospitalModel } from "./HospitalStore"

/**
 * 검색 api 를 호출하고 검색 결과를 저장하는 store
 */
export const SearchHospitalStoreModel = types
  .model("SearchHospitalStore")
  .props({
    prevQuery: types.maybe(types.string),
    count: types.optional(types.number, 0),
    nextUri: types.maybeNull(types.string),
    prevUri: types.maybeNull(types.string),
    drugs: types.array(DrugModel),
    hospitals: types.array(HospitalModel),
    selectedDisease: types.maybe(types.reference(DiseaseModel)),
    selectedHospital: types.maybe(types.reference(HospitalModel)),
  })
  .extend(withStatus)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    initiate() {
      applySnapshot(self, {})
    },
    setSelectedDisease(disease: Disease) {
      self.selectedDisease = disease
    },
    setSelectedHospital(hospital: Hospital) {
      self.selectedHospital = hospital
    },
    next: flow(function* () {
      if (self.nextUri == null) return

      const result = yield api.getNextResults(self.nextUri)
      if (result.kind === "ok") {
        const uri = self.nextUri.substr(1)
        const category = uri.substring(0, uri.indexOf("/"))

        switch (category) {
          case "drug": {
            const resultsData = result.data.results
            resultsData.map((data) => {
              return data.howto.length === 0
                ? (data.howto = null)
                : (data.howto = data.howto
                    .filter((data) => data !== "")
                    .toString()
                    .replace(`autoplay=""`, "")
                    .replace(`height="100%`, `height="50%"`))
            })
            self.drugs = self.drugs.concat(result.data.results)
            break
          }
          case "disease":
            self.diseases = self.diseases.concat(result.data.results)
            break

          case "hospital":
            self.hospitals = self.hospitals.concat(result.data.results)

          default:
            break
        }

        self.count = result.data.count
        const next: string | null = result.data.next
        const prev: string | null = result.data.previous
        const regExp = /^[a-zA-Z]{3,5}\:\/{2}[a-zA-Z0-9_.:-]+\//
        self.nextUri = next ? next.replace(regExp, "/") : null
        self.prevUri = prev ? prev.replace(regExp, "/") : null
      } else {
        self.setStatus("error")
      }
    }),
    search: flow(function* (
      category: "drug" | "disease" | "hospital",
      query: string,
      offset?: number,
      limit?: number,
    ) {
      self.setStatus("pending")
      const result = yield api.getSearchResults(category, query, offset, limit)
      sendLogEvent("search", { search_term: query })
      if (result.kind === "ok") {
        switch (category) {
          case "drug": {
            const resultsData = result.data.results
            applySnapshot(self.drugs, resultsData)
            break
          }
          // case "disease":
          //   applySnapshot(self.diseases, result.data.results)
          //   break
          case "hospital":
            applySnapshot(self.hospitals, result.data.results)
            break

          default:
            break
        }
        self.count = result.data.count
        self.setStatus("done")
        self.prevQuery = query
        const next: string | null = result.data.next
        const prev: string | null = result.data.previous
        const regExp = /^[a-zA-Z]{3,5}\:\/{2}[a-zA-Z0-9_.:-]+\//
        self.nextUri = next ? next.replace(regExp, "/") : null
        self.prevUri = prev ? prev.replace(regExp, "/") : null
      } else {
        self.setStatus("error")
      }
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type SearchHospitalStoreType = Instance<typeof SearchHospitalStoreModel>
export interface SearchHospitalStore extends SearchHospitalStoreType {}
type SearchHospitalStoreSnapshotType = SnapshotOut<typeof SearchHospitalStoreModel>
export interface SearchHospitalStoreSnapshot extends SearchHospitalStoreSnapshotType {}
