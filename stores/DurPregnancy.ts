import { types } from "mobx-state-tree" 
import { DrugModel } from "./Drug"

// 'preg': [ { 'detail_id', 1111, 'drug_id': 9803, 'drug_name', 'XX정', 'preg_grade': '2', 'preg_detail': 'ibuprofen: 임부에 대한 안전성 미확립. 임신 말기에 투여시 태아의 동맥관조기폐쇄 가능성.'}, ]  

export const DurPregnancyModel = types.model("DurPregnancy")
.props({
  detail_id: types.identifierNumber,
  // drug_id: types.reference(DrugModel),
  drug_id: types.number,
  drug_name: types.string,
  preg_grade: types.string,
  preg_detail: types.string
})

type DurPregnancyType = typeof DurPregnancyModel.Type
export interface DurPregnancy extends DurPregnancyType {}