import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const ComplianceDetailModel = types
  .model("ComplianceDetail")
  .props({
    drug_name: types.string,
    pause_take_dt: types.maybeNull(types.string), //Luxon? string?
    compliance: types.maybeNull(types.number),
  })
  .views((self) => ({
    get shortenedName() {
      let end = self.drug_name.indexOf("[")
      if (end != -1) return self.drug_name.substring(0, end)

      end = self.drug_name.indexOf("(")
      if (end != -1) return self.drug_name.substring(0, end)
      end = self.drug_name.indexOf("/")
      if (end != -1) {
        return self.drug_name.substring(0, end)
      } else {
        return self.drug_name.substring(0, self.drug_name.length)
      }
    },
  }))
  .actions((self) => ({}))

export const ComplianceTimeSlotsModel = types.model("ComplianceTimeSlots").props({
  timeslot_name: types.maybeNull(types.string),
  compliance: types.maybeNull(types.number),
})

export const ComplianceModel = types.model("Compliance").props({
  total: types.maybe(types.number),
  prescription_details: types.array(ComplianceDetailModel),
  timeslots: types.array(ComplianceTimeSlotsModel),
})

type ComplianceType = Instance<typeof ComplianceModel>
export interface Compliance extends ComplianceType {}
type ComplianceSnapshotType = SnapshotOut<typeof ComplianceModel>
export interface ComplianceSnapshot extends ComplianceSnapshotType {}

type ComplianceTimeSlotsType = Instance<typeof ComplianceTimeSlotsModel>
export interface ComplianceTimeSlots extends ComplianceTimeSlotsType {}
type ComplianceTimeSlotsSnapshotType = SnapshotOut<typeof ComplianceTimeSlotsModel>
export interface ComplianceTimeSlotsSnapshot extends ComplianceTimeSlotsSnapshotType {}

type ComplianceDetailType = Instance<typeof ComplianceDetailModel>
export interface ComplianceDetail extends ComplianceDetailType {}
type ComplianceDetailSnapshotType = SnapshotOut<typeof ComplianceDetailModel>
export interface ComplianceDetailSnapshot extends ComplianceDetailSnapshotType {}
