import { types } from "mobx-state-tree"

export const IngredientDetail = types.model("IngredientDetail").props({
  id: types.number,
  // ingredient_detail: types.maybeNull(types.optional(types.string, "")),
  one_liner_kr: types.optional(types.string, ""),
  bottom_line_kr: types.optional(types.string, ""),
  upside_kr: types.optional(types.string, ""),
  downside_kr: types.optional(types.string, ""),
  mechnism_kr: types.optional(types.string, ""),
  indication_kr: types.optional(types.string, ""),
  sideeffect_kr: types.optional(types.string, ""),
  drugbank_url: types.maybeNull(types.string),
  // name_kr: types.optional(types.string, ""),
})

export const Ingredient = types.model("Ingredient").props({
  id: types.number,
  ingredient_detail: types.maybeNull(IngredientDetail),
  // created: "2019-01-10T11:19:07.024299+09:00",
  // modified: "2019-01-14T11:35:54.430641+09:00",
  // name_en: "Coptis Rhizome/Ivy Leaf",
  name_kr: "황련 Rhizome/Ivy Leaf",
  dur: "H533400",
  code: "533400ASY",
  nosale_group: types.maybe(types.string),
})

export const DrugModel = types
  .model("Drug")
  .props({
    id: types.number,
    // kd_code: types.number,
    ingredient: types.maybeNull(Ingredient),
    howto: types.array(types.string),
    name: types.string,
    ingredient_name_kr: types.maybeNull(types.string),
    // indication: types.string,
    dose_admin: types.maybeNull(types.string),
    caution: types.maybeNull(types.string),
    image_url: types.maybeNull(types.string),
    one_liner: types.maybe(types.string),
    /**  의약품은 'm', 건기식은 's' */
    category: types.optional(types.enumeration(["m", "s"]), "m"),
    unit: types.maybe(types.string),
  })
  .views((self) => ({
    get unitConverted() {
      switch (self.unit) {
        case "패취":
        case "필름":
          return "매"
        case "점안제":
        case "분무제":
          return "회"
        case "겔":
        case "로션":
        case "크림":
        case "연고":
        case "외용액":
        case "네일라카":
          return "적당량"
        case "주사제":
        case "건강기능식품": // TODO: 건기식 제품에 따라 적절한 단위 매칭 필요(정/포/ml..)
          return "단위"
        case "액제":
          return "mL"
        case "가루":
          return "포"
        case "기타":
          return "정"
        default:
          return self.unit
      }
    },
    get shortenedName() {
      let end = self.name.indexOf("[")
      if (end != -1) return self.name.substring(0, end)

      end = self.name.indexOf("(")
      if (end != -1) return self.name.substring(0, end)

      end = self.name.indexOf("/")
      if (end != -1) {
        return self.name.substring(0, end)
      } else {
        return self.name.substring(0, self.name.length)
      }
    },
  }))

type DrugType = typeof DrugModel.Type
export interface Drug extends DrugType {}
