import { DateTime } from "luxon"
import { values } from "mobx"
import { applyPatch, applySnapshot, clone, flow, getEnv, types } from "mobx-state-tree"

import { api } from "@api/api"
import { swrService } from "@api/swr-service"

import { formatTime, getDayAfter, getDayBefore } from "../lib/DateUtil"
import { Disease, DiseaseModel } from "./Disease"
import { Drug, DrugModel } from "./Drug"
import { DurAttributeModel } from "./DurAttribute"
import { NpsStore } from "./nps/NpsStore"
import { PrescriptionModel } from "./Prescription"
import { PrescriptionDetailModel } from "./PrescriptionDetail"
import { TimeSlotModel, TimeSlotName } from "./TimeSlot"

const escapeRegExp = (string: string) => {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")
}

export const PrescriptionStoreModel = types
  .model({
    prescriptions: types.map(types.array(PrescriptionModel)),
    prescriptionDetail: types.maybeNull(PrescriptionModel),
    editingPrescription: types.maybe(PrescriptionModel),
    disease: types.maybe(DiseaseModel),
    drugDetail: types.maybe(DrugModel),
    drugSearchInfo: types.array(DrugModel),
    drugSelectedToAdd: types.maybe(DrugModel),
    status: types.optional(
      types.enumeration(["idle", "pending", "done", "error", "unauthorized"]),
      "idle",
    ),
    statusOfPrescriptions: types.map(
      types.optional(
        types.enumeration(["idle", "pending", "done", "error", "unauthorized"]),
        "idle",
      ),
    ),
    selectedFamilyMemberKey: types.optional(types.string, ""),
    hasDurContents: types.map(DurAttributeModel),
    dosingEnded: types.array(PrescriptionModel),
    timeSlots: types.array(TimeSlotModel),
    newPrescriptionDetail: types.maybe(PrescriptionDetailModel),
  })
  .views((self) => ({
    get distinctTimeSlots() {
      return self.timeSlots.filter(
        (slot) => slot.category === "pcare5" || slot.category === "user_created",
      )
    },
    get userCreatedTimeSlots() {
      return self.timeSlots.filter((slot) => slot.category === "user_created")
    },
    get wakeupTimeString() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.WakeUp,
      )
      return slots.length > 0 ? formatTime(slots[0].when) : ""
    },
    get morningTimeString() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.Morning,
      )
      return slots.length > 0 ? formatTime(slots[0].when) : ""
    },
    get afternoonTimeString() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.Afternoon,
      )
      return slots.length > 0 ? formatTime(slots[0].when) : ""
    },
    get eveningTimeString() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.Evening,
      )
      return slots.length > 0 ? formatTime(slots[0].when) : ""
    },
    get nightTimeString() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.Night,
      )
      return slots.length > 0 ? formatTime(slots[0].when) : ""
    },
    get wakeupDateTime() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.WakeUp,
      )
      return slots.length > 0 ? DateTime.fromSQL(slots[0].when) : DateTime.local()
    },
    get morningDateTime() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.Morning,
      )
      return slots.length > 0 ? DateTime.fromSQL(slots[0].when) : DateTime.local()
    },
    get afternoonDateTime() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.Afternoon,
      )
      return slots.length > 0 ? DateTime.fromSQL(slots[0].when) : DateTime.local()
    },
    get eveningDateTime() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.Evening,
      )
      return slots.length > 0 ? DateTime.fromSQL(slots[0].when) : DateTime.local()
    },
    get nightDateTime() {
      const slots = self.timeSlots.filter(
        (slot) => slot.category === "pcare5" && slot.name === TimeSlotName.Night,
      )
      return slots.length > 0 ? DateTime.fromSQL(slots[0].when) : DateTime.local()
    },
    findPrescription(prescriptionId: number): undefined | Prescription {
      const prescriptions = values(self.prescriptions)
      for (let i = 0; i < prescriptions.length; i++) {
        const prescription = prescriptions[i]
        if (prescription.id === prescriptionId) {
          return prescription
        }
      }
      return undefined
    },
    get isLoading() {
      return self.status === "pending"
    },
    get arePrescriptionsLoading() {
      const myStatus = self.statusOfPrescriptions.get(self.selectedFamilyMemberKey)
      return myStatus === "pending"
    },
    hasDurContentFromMember(selectedFamilyMemberKey: string) {
      return self.hasDurContents.get(selectedFamilyMemberKey)
    },
    get prescriptionsNumber() {
      const prescriptions = values(self.prescriptions)
      return prescriptions.length
    },
  }))
  .views((self) => ({
    get wakeupInterval() {
      return self.wakeupDateTime.until(self.morningDateTime)
    },
    get morningInterval() {
      return self.morningDateTime.until(self.afternoonDateTime)
    },
    get afternoonInterval() {
      return self.afternoonDateTime.until(self.eveningDateTime)
    },
    get eveningInterval() {
      if (self.nightDateTime.hour >= 0 && self.nightDateTime.hour < 12) {
        return self.eveningDateTime.until(
          DateTime.fromSQL(self.nightDateTime.toSQL()).plus({ days: 1 }),
        )
      } else {
        return self.eveningDateTime.until(self.nightDateTime)
      }
    },
    get nightInterval() {
      if (self.nightDateTime.hour > 12 && self.nightDateTime.hour < 24) {
        return self.nightDateTime.until(
          DateTime.fromSQL(self.wakeupDateTime.toSQL()).plus({ days: 1 }),
        )
      } else {
        return self.nightDateTime.until(self.wakeupDateTime)
      }
    },
  }))
  .actions((self) => ({
    initiate() {
      applySnapshot(self, {
        drugSearchInfo: [],
      })
    },
    setStatus(value: "idle" | "pending" | "done" | "error" | "unauthorized") {
      self.status = value
    },
    setStatusOfPrescriptions(
      key: string,
      value: "idle" | "pending" | "done" | "error" | "unauthorized",
    ) {
      self.statusOfPrescriptions.set(key, value)
    },
    setSelectedFamilyMemberKey(key: string) {
      self.selectedFamilyMemberKey = key
    },
    setPrescription(prescription: Prescription) {
      if (!self.prescriptionDetail) {
        self.prescriptionDetail = PrescriptionModel.create(prescription)
        self.editingPrescription = PrescriptionModel.create(prescription)
      } else {
        applySnapshot(self.prescriptionDetail, prescription)
        applySnapshot(self.editingPrescription, prescription)
      }
      if (prescription.issue_date != null && prescription.issue_date) {
        self.prescriptionDetail!.issueDateObject = DateTime.fromISO(prescription.issue_date)
      }
      self.prescriptionDetail!.disease.map((disease) => {
        if (disease.one_liner.length > 0) {
          disease.rep_title = disease.one_liner
          disease.one_liner = ""
        }
      })
      self.prescriptionDetail!.subdiseases.map((disease) => {
        if (disease.one_liner.length > 0) {
          disease.rep_title = disease.one_liner
          disease.one_liner = ""
        }
      })
      self.editingPrescription!.disease.map((disease) => {
        if (disease.one_liner.length > 0) {
          disease.rep_title = disease.one_liner
          disease.one_liner = ""
        }
      })
      self.editingPrescription!.subdiseases.map((disease) => {
        if (disease.one_liner.length > 0) {
          disease.rep_title = disease.one_liner
          disease.one_liner = ""
        }
      })
      self.prescriptionDetail.setDurRes()
    },
    setDisease(
      diseaseFullData: Disease,
      noPcareData?: boolean,
      diseaseDetail?: any,
      htmlGuide?: string,
    ) {
      self.disease = diseaseFullData
      // TODO: 이슈 검토하기
      // if (htmlGuide) {
      //   self.disease.guide = htmlGuide
      // }

      // pcare data, cdc data 모두 없으면
      // if (noPcareData) {
      //   self.disease.nameEn = diseaseFullData.is_rep
      //     ? diseaseFullData.name
      //     : diseaseFullData.feature
      //   self.disease.feature = ""
      //   self.disease.pcare_desc_copy = ""
      //   return
      // } else if (diseaseFullData.description.length === 0 && diseaseDetail) {
      //   self.disease.pcare_desc_copy = diseaseDetail.pcare_desc
      //   self.disease.feature = diseaseDetail.one_liner
      //   self.disease.nameEn = diseaseDetail.rep_title
      // }
      // if (rep_title) {
      //   self.disease.nameEn = rep_title
      // }
      // if (one_liner) {
      //   self.disease.feature = one_liner
      // }
      // for (let j = 0; j < diseaseFullData.diseaseDetail.length; j++) {
      //   const diseaseDetail = diseaseFullData.diseaseDetail[j]
      //   if (diseaseDetail.pcare_desc != null && diseaseDetail.pcare_desc != "") {
      //     // pcare 내용 있으면
      //     self.disease.pcare_desc_copy = diseaseDetail.pcare_desc
      //     self.disease.feature = diseaseDetail.one_liner
      //     self.disease.nameEn = diseaseDetail.rep_title
      //   }
      // }
    },
    setDiseaseInPrescription(diseaseFullData: Disease, noPcareAndCdcData?: boolean) {
      for (let i = 0; i < self.prescriptionDetail.disease.length; i++) {
        if (self.prescriptionDetail.disease[i].id === diseaseFullData.id) {
          // TODO: refactor setDiseaseInPrescription
          // pcare data, cdc data 모두 없으면
          // if (noPcareAndCdcData) {
          //   // self.prescriptionDetail.disease[i].pcare_desc_copy = ""
          //   self.prescriptionDetail.disease[i].feature = ""
          //   self.prescriptionDetail.disease[i].nameEn = diseaseFullData.is_rep
          //     ? diseaseFullData.name
          //     : diseaseFullData.feature
          //   return
          // }
          // 앱에서 가공한 rep_title, one_liner 정보 유지
          // const rep_title = self.prescriptionDetail.disease[i].nameEn
          // const one_liner = self.prescriptionDetail.disease[i].feature
          // self.prescriptionDetail.disease[i] = diseaseFullData
          // self.prescriptionDetail.disease[i].nameEn = rep_title
          // self.prescriptionDetail.disease[i].feature = one_liner

          // for (let j = 0; j < diseaseFullData.description.length; j++) {
          //   const diseaseDetail = diseaseFullData.diseaseDetail[j]
          //   if (diseaseDetail.pcare_desc != null && diseaseDetail.pcare_desc != "") {
          //     // pcare 내용 있으면
          //     // self.prescriptionDetail.disease[i].pcare_desc_copy = diseaseDetail.pcare_desc
          //     self.prescriptionDetail.disease[i].feature = diseaseDetail.one_liner
          //     self.prescriptionDetail.disease[i].rep_title = diseaseDetail.title
          //   }
          // }

          self.prescriptionDetail.disease[i].description = diseaseFullData.description
          self.prescriptionDetail.disease[i].name = diseaseFullData.name
          self.prescriptionDetail.disease[i].feature = diseaseFullData.feature
        }
      }
    },
    setSubDisease(diseaseFullData: Disease) {
      for (let i = 0; i < self.prescriptionDetail.subdiseases.length; i++) {
        if (self.prescriptionDetail.subdiseases[i].id === diseaseFullData.id) {
          // TODO: refactor setSubDisease
          // 앱에서 가공한 rep_title, one_liner 정보 유지
          // const rep_title = self.prescriptionDetail.subdiseases[i].nameEn
          // const one_liner = self.prescriptionDetail.subdiseases[i].feature
          // self.prescriptionDetail.subdiseases[i] = diseaseFullData
          // self.prescriptionDetail.subdiseases[i].nameEn = rep_title
          // self.prescriptionDetail.subdiseases[i].feature = one_liner
          // for (let j = 0; j < diseaseFullData.diseaseDetail.length; j++) {
          //   const diseaseDetail = diseaseFullData.diseaseDetail[j]
          //   if (diseaseDetail.pcare_desc != null && diseaseDetail.pcare_desc != "") {
          //     self.prescriptionDetail.subdiseases[i].pcare_desc_copy = diseaseDetail.pcare_desc
          //     self.prescriptionDetail.subdiseases[i].feature = diseaseDetail.one_liner
          //     self.prescriptionDetail.subdiseases[i].nameEn = diseaseDetail.rep_title
          //   }
          // }

          self.prescriptionDetail.subdiseases[i].description = diseaseFullData.description
          self.prescriptionDetail.subdiseases[i].name = diseaseFullData.name
          self.prescriptionDetail.subdiseases[i].feature = diseaseFullData.feature
        }
      }
    },
    addDiseaseDetail(id: number, diseaseDetail: any) {
      if (self.prescriptionDetail) {
        self.prescriptionDetail?.disease.map((disease) => {
          if (disease.id === id) {
            disease.description.push(diseaseDetail)
          }
        })
      }
      if (self.disease && self.disease.id === id) {
        self.disease.description.push(diseaseDetail)
      }
    },
    addSubDiseaseDetail(id: number, diseaseDetail: any) {
      self.prescriptionDetail?.subdiseases.map((disease) => {
        if (disease.id === id) {
          disease.description.push(diseaseDetail)
        }
      })
    },
    setDiseasePcareContent(id: number, diseaseDetail: any) {
      // TODO: refactor setDiseasePcareContent
      if (self.prescriptionDetail) {
        self.prescriptionDetail?.disease.map((disease) => {
          if (disease.id === id) {
            console.tron.log("diseaseDetail1", diseaseDetail)
            // disease.diseaseDetail.push(diseaseDetail)
            // disease.nameEn = diseaseDetail.rep_title
            // disease.rep_code = diseaseDetail.rep_code
            // disease.pcare_desc_copy = diseaseDetail.pcare_desc
            // disease.feature = diseaseDetail.one_liner
          }
        })
      }
      if (self.disease && self.disease.id === id) {
        console.tron.log("diseaseDetail2", diseaseDetail)
        // self.disease.diseaseDetail.push(diseaseDetail)
        // self.disease.nameEn = diseaseDetail.rep_title
        // self.disease.rep_code = diseaseDetail.rep_code
        // self.disease.pcare_desc_copy = diseaseDetail.pcare_desc
        // self.disease.feature = diseaseDetail.one_liner
      }
    },
    setSubDiseasePcareContent(id: number, diseaseDetail: any) {
      // TODO: refactor setSubDiseasePcareContent
      self.prescriptionDetail!.subdiseases.map((disease) => {
        if (disease.id === id) {
          console.tron.log("diseaseDetail3", diseaseDetail)
          // disease.diseaseDetail.push(diseaseDetail)
          // disease.nameEn = diseaseDetail.rep_title
          // disease.rep_code = diseaseDetail.rep_code
          // disease.pcare_desc_copy = diseaseDetail.pcare_desc
          // disease.feature = diseaseDetail.one_liner
        }
      })
    },
    setDrug(drug: Drug) {
      if (self.drugDetail === undefined) {
        self.drugDetail = DrugModel.create(drug)
      } else {
        applySnapshot(self.drugDetail, drug)
      }

      if (
        self.drugDetail.ingredient !== undefined &&
        self.drugDetail.ingredient.ingredient_detail !== undefined
      ) {
        const ingredientDetail = self.drugDetail.ingredient.ingredient_detail
        if (ingredientDetail) {
          self.drugDetail.ingredient.ingredient_detail.bottom_line_kr = ingredientDetail.bottom_line_kr.replace(
            /\*+/g,
            self.drugDetail.name,
          )
          self.drugDetail.ingredient.ingredient_detail.upside_kr = ingredientDetail.upside_kr.replace(
            /\*+/g,
            self.drugDetail.name,
          )
          self.drugDetail.ingredient.ingredient_detail.downside_kr = ingredientDetail.downside_kr.replace(
            /\*+/g,
            self.drugDetail.name,
          )
          self.drugDetail.ingredient.ingredient_detail.mechnism_kr = ingredientDetail.mechnism_kr.replace(
            /\*+/g,
            self.drugDetail.name,
          )
        }
      }
    },
    setTimeSlots(timeSlots: any) {
      applySnapshot(self.timeSlots, timeSlots)
    },
    removeTimeSlot(id: number) {
      let indexToRemove: number
      for (let i = 0; i < self.timeSlots.length; i++) {
        if (self.timeSlots[i].id === id) {
          indexToRemove = i
          break
        }
      }
      self.timeSlots.splice(indexToRemove, 1)
    },
    addDrug(drugId: number) {
      for (let i = 0; i < self.drugSearchInfo.length; i++) {
        if (drugId === self.drugSearchInfo[i].id) {
          self.drugSelectedToAdd = DrugModel.create(self.drugSearchInfo[i])
          // self.editingPrescription.prescription_details.push(self.drugSearchInfo[i])
          break
        }
      }
    },
    removePrescription(prescription: Prescription, memberIndex: string) {
      let indexToRemove = -1
      const prescriptions = self.prescriptions.get(memberIndex)
      if (prescriptions) {
        prescriptions.map((p, index) => {
          if (p.id === prescription.id) {
            indexToRemove = index
          }
        })
        prescriptions.splice(indexToRemove, 1)
      }
    },
    addPrescription(snapshot: PrescriptionSnapshot, familyMemberKey: string) {
      const pInstance = PrescriptionModel.create(snapshot)
      self.prescriptions.get(familyMemberKey).unshift(pInstance)
      pInstance.setDurRes()

      const durAttr = DurAttributeModel.create()
      if (pInstance.durRes?.byg.length > 0) {
        durAttr.setByg(true)
      }
      if (pInstance.durRes?.dis.length > 0) {
        durAttr.setDis(true)
      }
      if (pInstance.durRes?.hng.length > 0) {
        durAttr.setHng(true)
      }
      if (pInstance.durRes?.preg.length > 0) {
        durAttr.setPreg(true)
      }
      if (pInstance.durRes?.nosale.length > 0) {
        durAttr.setNosale(true)
      }
      self.hasDurContents.set(familyMemberKey, durAttr)
    },
    addDosingEndedPrescription(prescription: Prescription) {
      let exist = false
      for (let i = 0; i < self.dosingEnded.length; i++) {
        const p = self.dosingEnded[i]
        if (p.id === prescription.id) {
          exist = true
          break
        }
      }
      if (!exist) {
        self.dosingEnded.push(clone(prescription))
      }
    },
    removeDoingEndedPrescription(prescriptionId: number) {
      for (let i = 0; i < self.dosingEnded.length; i++) {
        const prescription = self.dosingEnded[i]
        if (prescription.id === prescriptionId) {
          self.dosingEnded.splice(i, 1)
        }
      }
    },
    savePrescriptionDetail(prescriptionDetail: any, index: number) {
      applyPatch(self.prescriptionDetail.prescription_details, {
        op: "replace",
        path: `/${index}`,
        value: prescriptionDetail,
      })
      applyPatch(self.editingPrescription.prescription_details, {
        op: "replace",
        path: `/${index}`,
        value: prescriptionDetail,
      })
    },
    setDiseaseGuideInPrescription(id: number, htmlGuide: string) {
      // TODO: refactor setDiseaseGuideInPrescription
      // self.prescriptionDetail?.disease.map((disease) => {
      //   if (disease.id == id) {
      //     disease.guide = htmlGuide
      //   }
      // })
    },
    /** 가족회원 별 처방전 목록에서 해당하는 처방전의 질병 찾아서 삭제 */
    removeDisease(pid: number, diseaseId: number, isSubDisease?: boolean) {
      const prescriptions = self.prescriptions.get(self.selectedFamilyMemberKey)
      for (let i = 0; i < prescriptions.length; i++) {
        const prescription = prescriptions[i]
        let removed = false
        if (prescription.id == pid) {
          let diseases
          if (isSubDisease) {
            diseases = prescription.subdiseases
          } else {
            diseases = prescription.disease
          }

          for (let i = 0; i < diseases.length; i++) {
            if (diseases[i].id == diseaseId) {
              prescription.removeDisease(diseases[i], isSubDisease)
            }
            removed = true
            break
          }
        }
        if (removed) break
      }
    },
    /** 가족회원 별 처방전 목록에서 해당하는 약 처방 찾아서 삭제 */
    removePrescriptionDetail(pid: number, prescriptionDetailId: number) {
      const prescriptions = self.prescriptions.get(self.selectedFamilyMemberKey)
      for (let i = 0; i < prescriptions.length; i++) {
        const prescription = prescriptions[i]
        let removed = false
        if (prescription.id == pid) {
          const prescriptionDetails = prescription.prescription_details
          for (let i = 0; i < prescriptionDetails.length; i++) {
            if (prescriptionDetails[i].id == prescriptionDetailId) {
              prescription.removePrescriptionDetail(prescriptionDetails[i])
            }
            removed = true
            break
          }
        }
        if (removed) break
      }
    },
    removeHospital(pid: number, hospitalId: number) {
      const prescriptions = self.prescriptions.get(self.selectedFamilyMemberKey)
      for (let i = 0; i < prescriptions.length; i++) {
        const prescription = prescriptions[i]
        let removed = false
        if (prescription.id == pid) {
          const hospital = prescription.hospital
          if (hospital.id == hospitalId) {
            prescription.removeHospital(hospital)
          }
          removed = true
          break
        }
        if (removed) break
      }
    },
    setNewPrescriptionDetail(prescriptionDetail: PrescriptionDetail) {
      self.newPrescriptionDetail = prescriptionDetail
    },
  }))
  .actions((self) => ({
    fetchSamplePrescription: flow(function* (id: number, userId: number, familyMemberKey: string) {
      self.setStatusOfPrescriptions(familyMemberKey, "pending")
      const result = yield api.getPrescriptionDetail(id, userId)
      if (result.kind === "ok") {
        const today = new Date()
        result.data.issue_date = getDayBefore(today, 1, "-") // 오늘 날짜로부터 하루 전 날짜로 교부날짜 수정
        result.data.start_at = result.data.issue_date // 오늘 날짜로부터 하루 전 날짜로 시작일 수정
        result.data.end_at = getDayAfter(today, 4, "-") // 오늘 날짜로부터 4일 후 날짜로 시작일 수정
        self.addPrescription(result.data, familyMemberKey)
      }
      self.setStatusOfPrescriptions(familyMemberKey, "done")
    }),
  }))
  .actions((self) => ({
    setPrescriptions(prescriptions: any, familyMemberKey: string) {
      applyPatch(self.prescriptions, {
        op: "replace",
        path: "/" + familyMemberKey,
        value: prescriptions,
      })
      // applySnapshot(self.prescriptions, prescriptions)

      // Check DUR
      const durAttr = DurAttributeModel.create()
      const storedPrescriptions = self.prescriptions.get(familyMemberKey)
      let pendingNumber = 0
      for (let i = 0; i < storedPrescriptions.length; i++) {
        const prescription: Prescription = storedPrescriptions[i]
        if (prescription.dur_res.length > 38) {
          prescription.setDurRes()
          if (prescription.durRes?.byg.length > 0) {
            durAttr.setByg(true)
          }
          if (prescription.durRes?.dis.length > 0) {
            durAttr.setDis(true)
          }
          if (prescription.durRes?.hng.length > 0) {
            durAttr.setHng(true)
          }
          if (prescription.durRes?.preg.length > 0) {
            durAttr.setPreg(true)
          }
          if (prescription.durRes?.nosale.length > 0) {
            durAttr.setNosale(true)
          }
        }

        // 처리중인 처방전 numbering
        if (prescription.status === "UL" || prescription.status === "IP") {
          prescription.setPendingNumber(++pendingNumber)
        }

        if (prescription.status === "CF" && prescription.start_at && prescription.end_at) {
          const startDate = DateTime.fromISO(prescription.start_at)
          const endDate = DateTime.fromISO(prescription.end_at)
          const today = prescription.todayObject
          const dosingDays = endDate.diff(startDate, "days").days + 1
          // 두 날짜 차이 구하기
          const diffDays = today.diff(startDate, "days").days + 1

          const remainingDosingDays = dosingDays - diffDays
          prescription.setRemainingDosingDays(remainingDosingDays)
        }
      }
      self.hasDurContents.set(familyMemberKey, durAttr)
    },
    fetchDiseaseSummaryInPrescription: flow(function* (id: number, code: string) {
      if (self.prescriptionDetail) {
        let foundDisease = false
        self.setStatus("pending")

        for (let i = 0; i < self.prescriptionDetail.disease.length; i++) {
          const disease = self.prescriptionDetail.disease[i]
          if (disease.id === id && disease.description.length === 0) {
            foundDisease = true
            const response = yield swrService.client.get<Disease>(`/diseases/${id}`)
            if (response) {
              if (!response.description || response.description.length === 0) {
                const result2 = yield api.getRepDiseaseSummary(code)
                if (result2.kind === "ok") {
                  if (result2.data.is_pcare) {
                    self.setDiseasePcareContent(id, result2.data.pcare)
                  }
                  if (result2.data.is_cdc) {
                    result2.data.cdc_list.map((diseaseDetail: any) => {
                      self.addDiseaseDetail(id, diseaseDetail)
                    })
                  }
                  if (!result2.data.is_pcare && !result2.data.is_cdc) {
                    console.tron.log("1 setDiseaseInPrescription")
                    self.setDiseaseInPrescription(response, true)
                  }
                  if (result2.data.guide) {
                    self.setDiseaseGuideInPrescription(id, result2.data.guide)
                  }
                }
              } else {
                console.tron.log("2 setDiseaseInPrescription")
                self.setDiseaseInPrescription(response)
              }
            } else {
              self.setStatus("error")
              console.tron.log("< PrescriptionStore > fetchDiseaseSummaryInPrescription, error")
            }
          } else {
            // TODO: DiseaseDescription type 형태로 저장
            console.tron.log("TODO: DiseaseDescription type 형태로 저장")
          }
        }

        if (foundDisease) {
          self.setStatus("done")
          return
        }

        for (let i = 0; i < self.prescriptionDetail.subdiseases.length; i++) {
          const disease = self.prescriptionDetail.subdiseases[i]
          if (disease.id === id && disease.description.length === 0) {
            // const result = yield api.getDiseaseSummary(id)
            const response = yield swrService.client.get<Disease>(`/diseases/${id}`)
            if (response) {
              if (!response.description || response.description.length === 0) {
                const result2 = yield api.getRepDiseaseSummary(code)
                if (result2.kind === "ok") {
                  if (result2.data.is_pcare) {
                    self.setSubDiseasePcareContent(id, result2.data.pcare)
                  }
                  if (result2.data.is_cdc) {
                    result2.data.cdc_list.map((diseaseDetail: any) => {
                      self.addSubDiseaseDetail(id, diseaseDetail)
                    })
                  }
                  if (!result2.data.is_pcare && !result2.data.is_cdc) {
                    self.setSubDisease(response)
                  }
                }
              } else {
                self.setSubDisease(response)
              }
            } else {
              self.setStatus("error")
              console.tron.log("< PrescriptionStore > fetchDiseaseSummaryInPrescription, error")
            }
          }
        }
      }
      self.setStatus("done")
    }),
  }))
  .actions((self) => ({
    // add(member: any) {
    //   self.family.push(member)
    // },
    // remove(member: any) {
    //   destroy(member)
    // }
    fetchPrescriptions: flow(function* (
      userId: number,
      familyMemberKey: string,
      childId?: number,
      noSpinner?: boolean,
    ) {
      if (!api.apisauce.headers.Authorization) {
        self.setPrescriptions([], familyMemberKey)
        return
      }
      // try {
      if (!noSpinner) {
        self.setStatusOfPrescriptions(familyMemberKey, "pending")
      }
      const result = yield api.getPrescriptions(userId, childId)

      if (result.kind === "ok") {
        self.setPrescriptions(result.prescriptions, familyMemberKey)
        self.setStatusOfPrescriptions(familyMemberKey, "done")
      } else if (result.kind === "cannot-connect" || result.temporary) {
        self.setStatusOfPrescriptions(familyMemberKey, "done")
      } else if (result.kind === "unauthorized") {
        self.setStatusOfPrescriptions(familyMemberKey, "unauthorized")
      } else {
        self.setStatusOfPrescriptions(familyMemberKey, "error")
        console.tron.log("< PrescriptionStore > fetchPrescriptions, error: " + result.kind)
      }
    }),
    fetchPrescriptionDetail: flow(function* (id: number, userId: number) {
      self.setStatus("pending")
      const result = yield api.getPrescriptionDetail(id, userId)
      if (result.kind === "ok") {
        // sample 처방전의 처방약 시작일/종료일 처리
        if (id === 2394 || id === 2395) {
          const prescriptionDetails = result.data.prescription_details
          const startDate = DateTime.local().minus({ days: 1 }).toJSON()
          const endDate = DateTime.local().plus({ days: 4 }).toJSON()
          for (let i = 0; i < prescriptionDetails.length; i++) {
            const detail = prescriptionDetails[i]
            detail.start_at = startDate
            detail.end_at = endDate
          }
        }
        self.setPrescription(result.data)
        self.setStatus("done")

        self.prescriptionDetail!.disease.map((disease) => {
          self.fetchDiseaseSummaryInPrescription(disease.id, disease.code)
        })

        self.prescriptionDetail!.subdiseases.map((disease) => {
          self.fetchDiseaseSummaryInPrescription(disease.id, disease.code)
        })
      } else {
        self.setStatus("error")
        console.tron.log("< PrescriptionStore > fetchPrescriptionDetail, error")
      }
    }),
    fetchDrugDetail: flow(function* (id: number) {
      self.drugDetail = DrugModel.create({
        id: -1,
        name: "",
        one_liner: "",
      })

      self.setStatus("pending")
      const result = yield api.getDrugDetail(id)
      if (result.kind === "ok") {
        self.setDrug(result.data)
        self.setStatus("done")
      } else {
        self.setStatus("error")
        console.tron.log("< PrescriptionStore > fetchDrugDetail, error")
      }
    }),
    fetchDisease: flow(function* (id: number, code: string, npsStore?: NpsStore) {
      if (self.disease?.id === id) {
        return
      } else if (self.disease) {
        self.disease = undefined
      }
      self.setStatus("pending")

      // const result = yield api.getDiseaseSummary(id)
      const response = yield swrService.client.get<Disease>(`/diseases/${id}?guide=true`)
      if (response) {
        if (!response.description || response.description.length === 0) {
          const result2 = yield api.getRepDiseaseSummary(code || response.code)
          if (result2.kind === "ok") {
            if (result2.data.is_pcare) {
              self.setDiseasePcareContent(id, result2.data.pcare)
            }
            if (result2.data.is_cdc) {
              result2.data.cdc_list.map((diseaseDetail: any) => {
                self.addDiseaseDetail(id, diseaseDetail)
              })
            }
            if (!result2.data.is_pcare) {
              self.setDisease(response, true)
            } else {
              self.setDisease(response, false, result2.data.pcare, result2.data.guide)
            }
          } else {
            const diseaseDetail = {
              pcare_desc: "login-request",
              one_liner: "",
              // rep_title: result.data.is_rep ? response.name : response.feature,
              rep_title: response.feature,
            }
            self.setDisease(response, false, diseaseDetail)
          }
        } else {
          let noPcareData = true
          response.description.forEach((diseaseDetail) => {
            // if (diseaseDetail?.source === "pcare") {
            //   noPcareData = false
            // }
            noPcareData = false
          })
          self.setDisease(response, noPcareData)
        }
        self.setStatus("done")

        // if (npsStore && (code || response.code)) {
        //   let repDiseaseCode = result.data.rep_code
        //   if (repDiseaseCode.length === 0) {
        //     repDiseaseCode = result.data.code.split(".")[0]
        //   }
        //   npsStore.fetchDiseasePrevalence(repDiseaseCode)
        //   npsStore.fetchDiseaseDepartment(repDiseaseCode)
        //   npsStore.fetchDiseaseComorb(repDiseaseCode)
        // }
      } else {
        self.setStatus("error")
        console.tron.log("< PrescriptionStore > fetchDisease, error")
      }
    }),
    fetchTimeSlots: flow(function* () {
      if (!api.apisauce.headers.Authorization) {
        return
      }
      self.setStatus("pending")
      const result = yield api.getTimeSlots()

      if (result.kind === "ok") {
        self.setTimeSlots(result.data)
        self.setStatus("done")
      } else if (result.kind === "cannot-connect" || result.temporary) {
        self.setStatus("error")
      } else if (result.kind === "unauthorized") {
        self.setStatus("unauthorized")
      } else {
        console.tron.log("< PrescriptionStore > fetchTimeSlots, error: " + result.kind)
        self.setStatus("error")
      }
    }),
  }))

type PrescriptionType = typeof PrescriptionModel.Type
export interface Prescription extends PrescriptionType {}

export type PrescriptionSnapshot = typeof PrescriptionModel.SnapshotType

type PrescriptionDetailType = typeof PrescriptionDetailModel.Type
export interface PrescriptionDetail extends PrescriptionDetailType {}

// type ToDoStoreType = typeof ToDoListStoreModel.Type
// export interface ToDoStore extends ToDoStoreType {}

export type PrescriptionStore = typeof PrescriptionStoreModel.Type
