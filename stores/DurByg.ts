import { types } from "mobx-state-tree" 
import { DrugModel } from "./Drug"

// 'byg': [ {'presc1_id': 1,'drug1_id': 1, 'drug1_name', 'XX정', 'presc2_id':2, 'drug2_id': 2, 'drug2_name', 'XX', 'side_effect':'...', 'threshold':'...'}, ]
// "byg": [{"presc1_id": 1842, "detail1_id": 4428, "drug1_id": 105664, "drug1_name": "조이록신주400밀리그램(목시플록사신)", "presc2_id": 1841, "detail2_id": 4429, "drug2_id": 93513, "drug2_name": "아디팜정(히드록시진염산염)", "side_effect": "심부정맥 위험 증가 가능", "threshold": ""}]}

export const DurBygModel = types.model("DurByg")
.props({
  presc1_id: types.number,
  detail1_id: types.number,
  // drug1_id: types.reference(DrugModel),
  drug1_id: types.number,
  drug1_name: types.string,
  presc2_id: types.number,
  detail2_id: types.number,
  // drug2_id: types.reference(DrugModel),
  drug2_id: types.number,
  drug2_name: types.string,
  side_effect: types.string,
  threshold: types.string
})

type DurBygType = typeof DurBygModel.Type
export interface DurByg extends DurBygType {}