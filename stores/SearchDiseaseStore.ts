import { applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { DiseaseSummary } from "@api/api-v4.types"

import { swrService } from "../api/swr-service"
import { sendLogEvent } from "../lib/analytics"
import { Disease, DiseaseModel } from "./Disease"
import { withStatus } from "./extensions/with-status"

type DiseasesPayload = {
  result: number
  list: DiseaseSummary[]
}

/**
 * 검색 api 를 호출하고 검색 결과를 저장하는 store
 */
export const SearchDiseaseStoreModel = types
  .model("SearchDiseaseStore")
  .props({
    prevQuery: types.maybe(types.string),
    limit: types.optional(types.number, 40),
    count: types.optional(types.number, 0),
    page: types.optional(types.number, 1),
    nextPage: types.maybeNull(types.number),
    nextUri: types.maybeNull(types.string),
    prevUri: types.maybeNull(types.string),
    diseases: types.array(DiseaseModel),
    selectedDisease: types.maybe(types.reference(DiseaseModel)),
  })
  .extend(withStatus)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    initiate() {
      applySnapshot(self, {})
    },
    setSelectedDisease(disease: Disease) {
      self.selectedDisease = disease
    },
    next: flow(function* () {
      if (self.nextUri == null) return

      const response = yield swrService.client.get<DiseasesPayload>(self.nextUri)
      if (response) {
        self.diseases = self.diseases.concat(response.list)
        self.count = response.result
        self.page = self.nextPage

        const totalPages = Math.ceil(self.count / self.limit)
        self.nextUri =
          totalPages > self.page
            ? `/diseases?q=${self.prevQuery}&rows=${self.limit}&page=${self.page + 1}`
            : null
        self.prevUri =
          self.page > 1
            ? `/diseases?q=${self.prevQuery}&rows=${self.limit}&page=${self.page - 1}`
            : null
      } else {
        self.setStatus("error")
      }
    }),
    search: flow(function* (
      category: "drug" | "disease" | "hospital",
      query: string,
      page?: number,
      limit?: number,
    ) {
      self.setStatus("pending")
      self.limit = limit

      const response = yield swrService.client.get<DiseasesPayload>(
        `/diseases?q=${query}&rows=${limit}&page=${page}`,
      )
      sendLogEvent("search", { search_term: query })
      if (response) {
        applySnapshot(self.diseases, response.list)
        self.count = response.result
        self.page = page
        self.setStatus("done")
        self.prevQuery = query
        // const next: string | null = result.data.next
        // const prev: string | null = result.data.previous
        // const regExp = /^[a-zA-Z]{3,5}\:\/{2}[a-zA-Z0-9_.:-]+\//
        // self.nextUri = next ? next.replace(regExp, "/") : null
        // self.prevUri = prev ? prev.replace(regExp, "/") : null

        const totalPages = Math.ceil(self.count / limit)
        self.nextPage = totalPages > page ? page + 1 : null
        self.nextUri =
          totalPages > page ? `/diseases?q=${query}&rows=${limit}&page=${page + 1}` : null
        self.prevUri = page > 1 ? `/diseases?q=${query}&rows=${limit}&page=${page - 1}` : null
      } else {
        self.setStatus("error")
      }
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type SearchDiseaseStoreType = Instance<typeof SearchDiseaseStoreModel>
export interface SearchDiseaseStore extends SearchDiseaseStoreType {}
type SearchDiseaseStoreSnapshotType = SnapshotOut<typeof SearchDiseaseStoreModel>
export interface SearchDiseaseStoreSnapshot extends SearchDiseaseStoreSnapshotType {}
