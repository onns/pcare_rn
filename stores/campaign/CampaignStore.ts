import { applyPatch, applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { api } from "@api/api"

import { withStatus } from "../extensions"
import { CampaignModel } from "./Campaign"
import { RankingModel } from "./Ranking"

export const CampaignStoreModel = types
  .model("CampaignStore")
  .props({
    campaigns: types.array(CampaignModel),
    rankings: types.map(types.array(RankingModel)),
  })
  .extend(withStatus)
  .views((self) => ({
    get sortedCampaigns() {
      return self.campaigns.slice().sort((a, b) => a.priority - b.priority)
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    fetchCampaigns: flow(function* () {
      const result = yield api.getCampaigns()
      if (result.kind === "ok") {
        applySnapshot(self.campaigns, result.data)
      } else {
        console.tron.log("< CampaignStore > fetchCampaigns, error")
      }
    }),
    fetchRanking: flow(function* (kind: "disease" | "drug") {
      const result = yield api.getRanking(kind)
      if (result.kind === "ok") {
        applyPatch(self.rankings, {
          op: "replace",
          path: "/" + kind,
          value: result.data,
        })
      } else {
        console.tron.log("< CampaignStore > fetchRanking, error")
      }
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type CampaignStoreType = Instance<typeof CampaignStoreModel>
export interface CampaignStore extends CampaignStoreType {}
type CampaignStoreSnapshotType = SnapshotOut<typeof CampaignStoreModel>
export interface CampaignStoreSnapshot extends CampaignStoreSnapshotType {}
