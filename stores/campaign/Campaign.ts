import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { EventModel } from "./Event"

export const CampaignModel = types
  .model("Campaign")
  .props({
    title: types.string,
    events: types.array(EventModel),
    position: types.optional(types.enumeration(["top", "center", "bottom"]), "top"),
    priority: types.optional(types.number, 0),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type CampaignType = Instance<typeof CampaignModel>
export interface Campaign extends CampaignType {}
type CampaignSnapshotType = SnapshotOut<typeof CampaignModel>
export interface CampaignSnapshot extends CampaignSnapshotType {}
