import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { DiseaseShortModel } from "../drug-to-take/DiseaseShort"
import { DrugShortModel } from "./DrugShort"

export const RankingModel = types
  .model("Ranking")
  .props({
    /** drug id or disease id */
    id: types.number,
    /** 순위 */
    cnt: types.number,
    drug: types.maybe(DrugShortModel),
    disease: types.maybe(DiseaseShortModel),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type RankingType = Instance<typeof RankingModel>
export interface Ranking extends RankingType {}
type RankingSnapshotType = SnapshotOut<typeof RankingModel>
export interface RankingSnapshot extends RankingSnapshotType {}
