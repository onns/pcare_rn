import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Event 는 하나의 배너를 생성하는데 필요한 정보를 담는다.
 */
export const EventModel = types
  .model("Event")
  .props({
    title: types.string,
    target_type: types.optional(types.enumeration(["link", "content"]), "link"),
    target_event: types.maybeNull(types.late(() => EventModel)),
    target_url: types.maybeNull(types.string),
    priority: types.number,
    content: types.string,
    image_url: types.maybeNull(types.string),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type EventType = Instance<typeof EventModel>
export interface Event extends EventType {}
type EventSnapshotType = SnapshotOut<typeof EventModel>
export interface EventSnapshot extends EventSnapshotType {}
