import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * 기존 Drug 는 id 가 필수여서 ranking api 로부터 오는 drug data 를 담기 위한 short model 을 생성함.
 */
export const DrugShortModel = types
  .model("DrugShort")
  .props({
    name: types.maybe(types.string),
    one_liner: types.maybe(types.string),
  })
  .views((self) => ({}))
  .actions((self) => ({}))

type DrugShortType = Instance<typeof DrugShortModel>
export interface DrugShort extends DrugShortType {}
type DrugShortSnapshotType = SnapshotOut<typeof DrugShortModel>
export interface DrugShortSnapshot extends DrugShortSnapshotType {}
