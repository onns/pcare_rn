import { Instance, SnapshotOut, types } from "mobx-state-tree"

// import { PrescriptionDetailShortModel } from "./PrescriptionDetailShort"
// import { PrescriptionShortModel } from "./PrescriptionShort"

// "id": 1171951,
// "category": "pcare5",
// "name": "night",
// "owner": 754,
// "when": "22:00:00"

export enum TimeSlotName {
  WakeUp = "wakeup",
  Morning = "morning",
  Afternoon = "afternoon",
  Evening = "evening",
  Night = "night",
  Once = "once",
  Custom = "custom",
}

export const TimeSlotModel = types
  .model("TimeSlot")
  .props({
    id: types.maybe(types.number),
    name: types.enumeration<TimeSlotName>("TimeSlotName", Object.values(TimeSlotName)),
    /** HH:mm 형식 */
    when: types.string,
    category: types.maybe(types.string),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setWhen(when: string) {
      self.when = when
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type TimeSlotType = Instance<typeof TimeSlotModel>
export interface TimeSlot extends TimeSlotType {}
type TimeSlotSnapshotType = SnapshotOut<typeof TimeSlotModel>
export interface TimeSlotSnapshot extends TimeSlotSnapshotType {}
