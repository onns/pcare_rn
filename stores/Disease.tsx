import { Instance, types } from "mobx-state-tree"

/*
  id:4915
  code:I10
  name:본태성(원발성) 고혈압
  feature:고혈압
*/
export const diseaseDetailModel = types.model("DiseaseDetail").props({
  id: types.identifierNumber,
  title: types.string,
  feature: types.string,
  desc: types.string,
  // pcare_desc: types.maybeNull(types.string),
  // rep_title: types.string,
  // source: types.string,
  // one_liner: types.optional(types.string, ""),
  // title1: types.maybeNull(types.string),
  // desc1: types.maybeNull(types.string),
  // title2: types.maybeNull(types.string),
  // desc2: types.maybeNull(types.string),
  // title3: types.maybeNull(types.string),
  // desc3: types.maybeNull(types.string),
  // title4: types.maybeNull(types.string),
  // desc4: types.maybeNull(types.string),
  // title5: types.maybeNull(types.string),
  // desc5: types.maybeNull(types.string),
  // title6: types.maybeNull(types.string),
  // desc6: types.maybeNull(types.string),
  // title7: types.maybeNull(types.string),
  // desc7: types.maybeNull(types.string),
  // title8: types.maybeNull(types.string),
  // desc8: types.maybeNull(types.string),
  // title9: types.maybeNull(types.string),
  // desc9: types.maybeNull(types.string),
  // title10: types.maybeNull(types.string),
  // desc10: types.maybeNull(types.string),
})

export const DiseaseModel = types
  .model("Disease")
  .props({
    id: types.identifierNumber,
    nameEn: types.maybe(types.string),
    name: types.optional(types.string, ""),
    feature: types.optional(types.string, ""),
    description: types.array(diseaseDetailModel),
    code: types.optional(types.string, ""),

    // 이하 v3 api 에서 사용하는 properties
    rep_title: types.optional(types.string, ""),
    name_kr: types.optional(types.string, ""),
    one_liner: types.optional(types.string, ""),
    diseaseDetail: types.array(diseaseDetailModel),
    pcare_desc_copy: types.optional(types.string, ""),
    rep_code: types.optional(types.string, ""),
    is_rep: types.optional(types.boolean, false),
    name_en: types.optional(types.string, ""),
    guide: types.maybe(
      types.model({ id: types.number, message: types.string, content: types.string }),
    ),
  })
  .actions((self) => ({
    setOneLiner(oneLiner: string) {
      self.one_liner = oneLiner
    },
    setRepTitle(value: string) {
      self.rep_title = value
    },
  }))

type DiseaseDetailType = typeof diseaseDetailModel.Type
export interface DiseaseDetail extends DiseaseDetailType {}

export interface Disease extends Instance<typeof DiseaseModel> {}
