import { SearchDrugStoreModel as SearchDrugV4StoreModel } from "@features/drug-v4/model/SearchDrugStore"
import { SearchHospitalStoreModel as SearchHospitalV4StoreModel } from "@features/hospital-v4/model/SearchHospitalStore"

import { AccountStoreModel } from "./AccountStore"
import { CampaignStoreModel } from "./campaign/CampaignStore"
import { TakingDrugStoreModel } from "./drug-to-take/TakingDrugStore"
import { HospitalStoreModel } from "./HospitalStore"
import { NpsStoreModel } from "./nps/NpsStore"
import { PrescriptionStoreModel } from "./PrescriptionStore"
import { RecordStoreModel } from "./record/record-store"
import { SearchDiseaseStoreModel } from "./SearchDiseaseStore"
import { SearchDrugStoreModel } from "./SearchDrugStore"
import { SearchHospitalStoreModel } from "./SearchHospitalStore"

export const stores = {
  accountStore: AccountStoreModel.create({
    user: {
      id: -1,
    },
    family: [],
  }),
  campaignStore: CampaignStoreModel.create({}),
  hospitalStore: HospitalStoreModel.create({}),
  prescriptionStore: PrescriptionStoreModel.create({}),
  npsStore: NpsStoreModel.create({}),
  takingDrugStore: TakingDrugStoreModel.create({}),
  searchDiseaseStore: SearchDiseaseStoreModel.create({}),
  searchDrugStore: SearchDrugStoreModel.create({}),
  searchHospitalStore: SearchHospitalStoreModel.create({}),
  recordStore: RecordStoreModel.create({}),

  searchDrugV4Store: SearchDrugV4StoreModel.create({}),
  searchHospitalV4Store: SearchHospitalV4StoreModel.create({}),
}
