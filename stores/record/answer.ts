import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const AnswerModel = types
  .model("Answer")
  .props({
    order: 0,
    score: 0,
    description: "1: 나는 기운이 왕성하다.",
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type AnswerType = Instance<typeof AnswerModel>
export interface Answer extends AnswerType {}
type AnswerSnapshotType = SnapshotOut<typeof AnswerModel>
export interface AnswerSnapshot extends AnswerSnapshotType {}
export const createAnswerDefaultModel = () => types.optional(AnswerModel, {})
