import { Instance, SnapshotOut, types } from "mobx-state-tree"

const GraphPeriodDataModel = types.model("GraphPeriodData").props({
  date: types.maybeNull(types.string),
  count: types.maybeNull(types.number),
  avg: types.maybeNull(types.number),
})

const GraphPeriodModel = types.model("GraphPeriod").props({
  count: types.maybeNull(types.number),
  next: types.maybeNull(types.string),
  previous: types.maybeNull(types.string),
  results: types.array(GraphPeriodDataModel),
})

export const GraphModel = types.model("Graph").props({
  count: types.maybeNull(types.number),
  sum: types.maybeNull(types.number),
  avg: types.maybeNull(types.number),
  min: types.maybeNull(types.number),
  max: types.maybeNull(types.number),
  daily: types.maybe(GraphPeriodModel),
  monthly: types.maybe(GraphPeriodModel),
})

type GraphModelType = Instance<typeof GraphModel>
export interface Graph extends GraphModelType {}
type GraphSnapshotType = SnapshotOut<typeof GraphModel>
export interface GraphSnapshot extends GraphSnapshotType {}
export const createRecordDefaultModel = () => types.optional(GraphModel, {})
