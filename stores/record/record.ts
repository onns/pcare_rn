import { Instance, SnapshotOut, types } from "mobx-state-tree"

const SurveyShortModel = types.model("SurveyShort").props({
  /** 높을수록 좋음: true, 낮을수록 좋음: false */
  thtb: types.maybeNull(types.boolean),
  normal_range_start: types.maybeNull(types.number),
  normal_range_end: types.maybeNull(types.number),
})

/**
 * 활동 기록 데이터 모델
 */
export const RecordModel = types
  .model("Record")
  .props({
    id: types.maybeNull(types.number),
    name: types.maybeNull(types.string),
    title: types.maybeNull(types.string),
    unit: types.maybeNull(types.string),
    type: types.maybeNull(types.string),
    survey: types.maybeNull(SurveyShortModel),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type RecordType = Instance<typeof RecordModel>
export interface Record extends RecordType {}
type RecordSnapshotType = SnapshotOut<typeof RecordModel>
export interface RecordSnapshot extends RecordSnapshotType {}
export const createRecordDefaultModel = () => types.optional(RecordModel, {})
