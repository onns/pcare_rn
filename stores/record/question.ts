import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { AnswerModel, AnswerSnapshot } from "./answer"

/**
 * json 으로 전달받은 answers 배열이 order 오름차순으로 정렬되어 있지 않기에
 * applySnapshot 직전에 order 순으로 정렬 수행
 */
const Answers = types.snapshotProcessor(types.array(AnswerModel), {
  // from snapshot to instance
  preProcessor(sn: Array<AnswerSnapshot>) {
    return sn.sort((a, b) => a.order - b.order)
  },
})

export const QuestionModel = types
  .model("Question")
  .props({
    order: 8,
    description: "나는 기운이 왕성하다 <-> 나는 전혀 기운이 없다",
    image: types.maybeNull(types.string),
    answers: Answers,
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type QuestionType = Instance<typeof QuestionModel>
export interface Question extends QuestionType {}
type QuestionSnapshotType = SnapshotOut<typeof QuestionModel>
export interface QuestionSnapshot extends QuestionSnapshotType {}
export const createQuestionDefaultModel = () => types.optional(QuestionModel, {})
