import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const EndingMessageModel = types
  .model("EndingMessage")
  .props({
    message: "저증상군에 해당합니다.",
    range_start: 0,
    range_end: 10,
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type EndingMessageType = Instance<typeof EndingMessageModel>
export interface EndingMessage extends EndingMessageType {}
type EndingMessageSnapshotType = SnapshotOut<typeof EndingMessageModel>
export interface EndingMessageSnapshot extends EndingMessageSnapshotType {}
export const createEndingMessageDefaultModel = () => types.optional(EndingMessageModel, {})
