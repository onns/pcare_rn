import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * 호흡운동 기록 데이터 모델
 */
export const RecordVideoModel = types
  .model("RecordVideo")
  .props({
    source: types.maybeNull(types.string),
    poster: types.maybeNull(types.string),
    title: types.maybeNull(types.string),
    sub_title: types.maybeNull(types.string),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type RecordVideoType = Instance<typeof RecordVideoModel>
export interface RecordVideo extends RecordVideoType {}
type RecordVideoSnapshotType = SnapshotOut<typeof RecordVideoModel>
export interface RecordVideoSnapshot extends RecordVideoSnapshotType {}
export const createRecordVideoDefaultModel = () => types.optional(RecordVideoModel, {})
