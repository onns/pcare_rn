import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { Luxon } from "../Luxon"

/**
 * RecordDetailScreen 의 FlatList에 쓰이는 model
 */
export const RecordDetailItemModel = types
  .model("RecordDetailItem")
  .props({
    data: types.maybeNull(types.number),
    when: Luxon,
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type RecordDetailItemType = Instance<typeof RecordDetailItemModel>
export interface RecordDetailItem extends RecordDetailItemType {}
type RecordDetailItemSnapshotType = SnapshotOut<typeof RecordDetailItemModel>
export interface RecordDetailItemSnapshot extends RecordDetailItemSnapshotType {}
export const createRecordDetailItemDefaultModel = () => types.optional(RecordDetailItemModel, {})
