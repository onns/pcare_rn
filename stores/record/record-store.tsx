import { applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { api } from "@api/api"

import { GraphModel } from "./graph"
import { RecordModel } from "./record"
import { RecordDetailItemModel } from "./record-detail-item"
import { RecordVideoModel } from "./record-video"
import { SurveyModel } from "./survey"

const RouteModel = types.model("RecordRoute").props({
  key: types.identifier,
  title: types.string,
  name: types.string,
})

const GraphPeriodDataModel = types.model("GraphPeriodData").props({
  date: types.maybeNull(types.string),
  count: types.maybeNull(types.number),
  avg: types.maybeNull(types.number),
})

/**
 * 유저 활동 기록, 설문 양식 데이터를 저장하는 스토어
 * 추후 혈압, 혈당 데이터도 추가될 예정
 */
export const RecordStoreModel = types
  .model("RecordStore")
  .props({
    /** 사용자에게 요청된 설문조사 양식 데이터 */
    requestedSurvey: types.maybe(SurveyModel),
    /** ACT, CAT, 호흡운동 그래프를 위한 데이터 */
    records: types.array(RecordModel),
    recordVideo: types.array(RecordVideoModel),
    /** TabView component 에서 사용하는 데이터 */
    recordRoutes: types.array(RouteModel),
    /** RecordDetailScreen(자세히 버튼 클릭 시) 에서 보여줄 데이터 */
    recordDetail: types.array(RecordDetailItemModel),
    /** RecordGraphScreen 에서 보여줄 그래프 데이터 */
    graphDetail: types.maybe(GraphModel),
    graphDate: types.optional(types.string, "daily"),
    graphDaily: types.array(GraphPeriodDataModel),
    graphMonthly: types.array(GraphPeriodDataModel),
    count: types.optional(types.number, 0),
    nextUri: types.maybeNull(types.string),
    prevUri: types.maybeNull(types.string),
    seletedTab: types.optional(types.string, ""),
    seletedTabNumber: types.optional(types.number, 0),
    graphStatus: types.optional(types.enumeration(["idle", "pending", "done", "error"]), "idle"),
    status: types.optional(types.enumeration(["idle", "pending", "done", "error"]), "idle"),
  })
  .actions((self) => ({
    setGraphDaily(data) {
      self.graphDaily = data
    },
    setGraphMonthly(data) {
      self.graphMonthly = data
    },
  }))
  .actions((self) => ({
    setRequestedSurvey(survey: any) {
      self.requestedSurvey = SurveyModel.create(survey)
    },
    setRecordRoutes(routes: any) {
      applySnapshot(self.recordRoutes, routes)
    },
    setGraph(data) {
      self.graphDetail = GraphModel.create(data)
    },
    setStatus(value: "idle" | "pending" | "done" | "error") {
      self.status = value
    },
    setGraphStatus(value: "idle" | "pending" | "done" | "error") {
      self.graphStatus = value
    },
    setSeletedTab(value: string) {
      self.seletedTab = value
    },
    setGraphDate(value: string) {
      self.graphDate = value
    },
    setSeletedTabNumber(value: string) {
      switch (value) {
        case "CAT":
          self.seletedTabNumber = 2
          break
        case "ACT":
          self.seletedTabNumber = 3
          break
        case "BREATHE":
          self.seletedTabNumber = 4
          break
        default:
          self.seletedTabNumber = 0
          break
      }
    },
    clearStore() {
      applySnapshot(self, {})
    },
  }))
  .actions((self) => ({
    fetchVideo: flow(function* (recordId: number) {
      self.setStatus("pending")
      const result = yield api.getRecordVideo(recordId)
      if (result.kind === "ok") {
        applySnapshot(self.recordVideo, result.data)
        self.setStatus("done")
      } else {
        self.setStatus("error")
        console.tron.log("< record-store > fetchVideo, error")
      }
    }),
    fetchSurvey: flow(function* (recordId: number) {
      self.setStatus("pending")
      const result = yield api.getSurvey(recordId)
      if (result.kind === "ok") {
        self.setRequestedSurvey(result.data)
        self.setStatus("done")
      } else {
        self.setStatus("error")
        console.tron.log("< record-store > fetchSurvey, error")
      }
    }),
    fetchRecords: flow(function* () {
      self.setStatus("pending")
      const result = yield api.getRecords()
      if (result.kind === "ok") {
        result.data.sort((a, b) => {
          return a.id - b.id
        })

        applySnapshot(self.records, result.data)
        const routes = []
        for (const { id, title, name } of self.records) {
          routes.push({ key: String(id), title, name })
        }
        self.setRecordRoutes(routes)

        self.setStatus("done")
      } else {
        self.setStatus("error")
        console.tron.log("< record-store > fetchRecords, error")
      }
    }),
    fetchRecordDetail: flow(function* (id: number) {
      self.setStatus("pending")
      const result = yield api.getRecordDetail(id)
      if (result.kind === "ok") {
        applySnapshot(self.recordDetail, result.data.results)

        self.count = result.data.count
        const next: string | null = result.data.next
        const prev: string | null = result.data.previous
        const regExp = /^[a-zA-Z]{3,5}\:\/{2}[a-zA-Z0-9_.:-]+\//
        self.nextUri = next ? next.replace(regExp, "/") : null
        self.prevUri = prev ? prev.replace(regExp, "/") : null

        self.setStatus("done")
      } else {
        self.setStatus("error")
        console.tron.log("< record-store > fetchRecordDetail, error")
      }
    }),
    fetchGraphDetail: flow(function* (recordId) {
      self.setStatus("pending")
      self.setGraphStatus("pending")

      const result = yield api.getGraph(recordId)

      if (result.kind === "ok") {
        const year = new Date().getFullYear()
        result.data.daily.results.forEach((element) => {
          element.date = element.date.replace(`${year}-`, "")
        })

        self.setGraph(result.data)
        self.setGraphDaily(result.data.daily.results)
        self.setGraphMonthly(result.data.monthly.results)

        setTimeout(() => {
          self.setStatus("done")
          self.setGraphStatus("done")
        }, 500)
      } else {
        self.setStatus("error")
        console.tron.log("< record-store > fetchGraphDetail, error")
      }
    }),
    next: flow(function* () {
      if (self.nextUri == null) return

      const result = yield api.getNextResults(self.nextUri)

      if (result.kind === "ok") {
        applySnapshot(self.recordDetail, result.data.results)

        self.count = result.data.count
        const next: string | null = result.data.next
        const prev: string | null = result.data.previous
        const regExp = /^[a-zA-Z]{3,5}\:\/{2}[a-zA-Z0-9_.:-]+\//
        self.nextUri = next ? next.replace(regExp, "/") : null
        self.prevUri = prev ? prev.replace(regExp, "/") : null
      } else {
        self.setStatus("error")
      }
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type RecordStoreType = Instance<typeof RecordStoreModel>
export interface RecordStore extends RecordStoreType {}
type RecordStoreSnapshotType = SnapshotOut<typeof RecordStoreModel>
export interface RecordStoreSnapshot extends RecordStoreSnapshotType {}
export const createRecordStoreDefaultModel = () => types.optional(RecordStoreModel, {})
