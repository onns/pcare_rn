import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { EndingMessageModel } from "./ending-message"
import { QuestionModel, QuestionSnapshot } from "./question"

/**
 * json 으로 전달받은 questions 배열이 order 오름차순으로 정렬되어 있지 않기에
 * applySnapshot 직전에 order 순으로 정렬 수행
 */
const Questions = types.snapshotProcessor(types.array(QuestionModel), {
  // from snapshot to instance
  preProcessor(sn: Array<QuestionSnapshot>) {
    return sn.sort((a, b) => a.order - b.order)
  },
})

/**
 * 설문조사 양식 데이터 모델
 */
export const SurveyModel = types
  .model("Survey")
  .props({
    id: 2,
    name: "cat",
    title: "만성폐쇄성 폐질환 평가시험",
    sub_title: "CAT",
    description: "각 항목마다 현재 귀하의 건강 상태를 가장 잘 표현하는 점수를 체크해 주세요.",
    expected_time: "1~2분",
    /** 높을수록 좋음: true, 낮을수록 좋음: false */
    thtb: false,
    questions: Questions,
    ending_messages: types.array(EndingMessageModel),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type SurveyType = Instance<typeof SurveyModel>
export interface Survey extends SurveyType {}
type SurveySnapshotType = SnapshotOut<typeof SurveyModel>
export interface SurveySnapshot extends SurveySnapshotType {}
export const createSurveyDefaultModel = () => types.optional(SurveyModel, {})
