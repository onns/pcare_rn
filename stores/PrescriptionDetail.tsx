import { DateTime } from "luxon"
import { clone, types } from "mobx-state-tree"

import { stores } from "./"
import { DrugModel } from "./Drug"
import { Luxon } from "./Luxon"
import { TimeSlot, TimeSlotModel, TimeSlotName } from "./TimeSlot"

function getDefaultTimeFor(timeSlotName: TimeSlotName) {
  const { prescriptionStore } = stores
  const {
    wakeupDateTime,
    morningDateTime,
    afternoonDateTime,
    eveningDateTime,
    nightDateTime,
  } = prescriptionStore
  switch (timeSlotName) {
    case TimeSlotName.WakeUp:
      return wakeupDateTime.toFormat("HH:mm")

    case TimeSlotName.Morning:
      return morningDateTime.toFormat("HH:mm")

    case TimeSlotName.Afternoon:
      return afternoonDateTime.toFormat("HH:mm")

    case TimeSlotName.Evening:
      return eveningDateTime.toFormat("HH:mm")

    case TimeSlotName.Night:
      return nightDateTime.toFormat("HH:mm")

    case TimeSlotName.Once:
    default:
      return ""
  }
}

export const PrescriptionDetailModel = types
  .model("PrescriptionDetail")
  .props({
    id: types.identifierNumber,
    drug: DrugModel,
    /**
     * 1회 투여량
     */
    dose: types.number,
    /**
     * 1회 투여량 단위
     */
    unit: types.string,
    /**
     * 투여횟수
     */
    doses_num: types.number,
    /**
     * 투여일 기본값1
     */
    per_days: types.number,
    /**
     * 총 투약일수
     */
    dosing_days_num: types.number,
    start_at: types.maybeNull(Luxon),
    end_at: types.maybeNull(Luxon),
    timeslots: types.array(TimeSlotModel),
    /**
     * 처방전 ID
     */
    prescription: types.number,
  })
  .views((self) => ({
    get timeSlotIds() {
      const { timeSlots } = stores.prescriptionStore
      const filteredTimeSlots = timeSlots.filter(
        (timeSlot) =>
          timeSlot.category === "pcare1" ||
          timeSlot.category === "pcare5" ||
          timeSlot.category === "user_created",
      )
      const ids = []

      for (let i = 0; i < self.timeslots.length; i++) {
        const timeSlot = self.timeslots[i]

        for (let j = 0; j < filteredTimeSlots.length; j++) {
          const timeSlotWithId = filteredTimeSlots[j]
          // TODO: when 값이 분 단위까지로 통일되면 substr 제거할 것
          // 기본적으로 when 값은 중복되지 않으나 예외적으로 once 의 when 값은 다른 name 의 when 값과 중복될 가능성이 있기에 name 이 once 인 경우와 아닌 경우를 구분해야 함.
          if (
            (timeSlot.name !== "once" &&
              timeSlot.when.substr(0, 5) === timeSlotWithId.when.substr(0, 5)) ||
            (timeSlot.name === "once" && timeSlot.name === timeSlotWithId.name)
          ) {
            ids.push(timeSlotWithId.id)
            break
          }
        }
      }
      return ids
    },
    get selectedUserCreatedTimeSlots(): Array<{ selected: boolean; timeSlot: TimeSlot }> {
      const { timeSlots } = stores.prescriptionStore
      const userCreatedTimeSlots = timeSlots.filter(
        (timeSlot) => timeSlot.category === "user_created",
      )

      const selected = []
      const timeSlotsForMyDrug = self.timeslots.slice()

      for (let i = 0; i < userCreatedTimeSlots.length; i++) {
        const userCreatedTimeSlot = userCreatedTimeSlots[i]
        let found = false

        for (let j = 0; j < timeSlotsForMyDrug.length; j++) {
          const selectedTimeSlot = timeSlotsForMyDrug[j]
          if (userCreatedTimeSlot.id == selectedTimeSlot.id) {
            selected.push({ selected: true, timeSlot: userCreatedTimeSlot })
            found = true
            timeSlotsForMyDrug.splice(j, 1)
          }
        }
        if (!found) {
          selected.push({ selected: false, timeSlot: userCreatedTimeSlot })
        }
      }
      return selected
    },
  }))
  .actions((self) => ({
    setDose(dose: number) {
      self.dose = dose
    },
    setUnit(unit: string) {
      self.unit = unit
    },
    setDosesNum(dosesNum: number) {
      self.doses_num = dosesNum
    },
    setPerDays(perDays: number) {
      self.per_days = perDays
    },
    setDosingDaysNum(dosingDaysNum: number) {
      self.dosing_days_num = dosingDaysNum
    },
    setStartDate(startDate: string) {
      self.start_at = DateTime.fromSQL(startDate)
    },
    setEndDate(endDate: string) {
      self.end_at = DateTime.fromSQL(`${endDate} 23:59:59.000+09:00`)
    },
    includesTimeSlot(timeSlotName: TimeSlotName) {
      for (let i = 0; i < self.timeslots.length; i++) {
        const timeSlot = self.timeslots[i]
        if (timeSlot.name == timeSlotName) {
          return true
        }
      }
      return false
    },
  }))
  .actions((self) => ({
    updateTimeSlot(timeSlotName: TimeSlotName, addMode: boolean) {
      let index = -1
      let onceIndex = -1
      for (let i = 0; i < self.timeslots.length; i++) {
        const timeSlot = self.timeslots[i]
        if (timeSlot.name == timeSlotName) {
          index = i
        } else if (timeSlot.name == TimeSlotName.Once) {
          onceIndex = i
        }
      }
      if (addMode) {
        if (index == -1) {
          if (timeSlotName == TimeSlotName.Once) {
            self.timeslots.clear()
          } else if (onceIndex != -1) {
            self.timeslots.splice(onceIndex, 1)
          }
          self.timeslots.push(
            TimeSlotModel.create({ name: timeSlotName, when: getDefaultTimeFor(timeSlotName) }),
          )
        }
      } else if (index != -1) {
        self.timeslots.splice(index, 1)
      }
      self.setDosesNum(self.timeslots.length)
    },
    updateCreatedTimeSlot(selectedTimeSlots: Array<{ selected: boolean; timeSlot: TimeSlot }>) {
      const createdTimeSlots = self.timeslots.filter((timeSlot) => timeSlot.name === "custom")
      for (let i = 0; i < createdTimeSlots.length; i++) {
        self.timeslots.remove(createdTimeSlots[i])
      }

      for (let j = 0; j < selectedTimeSlots.length; j++) {
        const { selected, timeSlot } = selectedTimeSlots[j]
        if (timeSlot.name === "custom" && selected) {
          self.timeslots.push(clone(timeSlot))
        }
      }
      self.setDosesNum(self.timeslots.length)
    },
  }))
