import { types } from "mobx-state-tree"

// import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const PrescriptionDetailBannerModel = types
  .model("PrescriptionDetailBanner")
  .props({
    id: types.number,
    intro: types.optional(types.string, ""),
    howto: types.maybeNull(types.string),
    banner_kind: types.maybeNull(types.string),
    record_id: types.maybeNull(types.number),
  })
  .views((self) => ({}))
  .actions((self) => ({}))

type PrescriptionDetailBannerType = typeof PrescriptionDetailBannerModel.Type
export interface PrescriptionDetailBanner extends PrescriptionDetailBannerType {}
