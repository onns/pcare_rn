import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const MedicationProgressModel = types
  .model("MedicationProgress")
  .props({
    date: types.string,
    ratio: types.number,
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type MedicationProgressType = Instance<typeof MedicationProgressModel>
export interface MedicationProgress extends MedicationProgressType {}
type MedicationProgressSnapshotType = SnapshotOut<typeof MedicationProgressModel>
export interface MedicationProgressSnapshot extends MedicationProgressSnapshotType {}
