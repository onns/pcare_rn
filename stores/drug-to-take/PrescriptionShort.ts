import { DateTime } from "luxon"
import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { Luxon } from "../Luxon"
import { DiseaseShortModel } from "./DiseaseShort"

export const PrescriptionShortModel = types
  .model("PrescriptionShort")
  .props({
    id: types.number,
    disease: types.maybeNull(DiseaseShortModel),
    subdiseases: types.maybeNull(types.array(DiseaseShortModel)),
    issue_date: types.maybeNull(types.string),
    issueDateObject: types.maybe(Luxon),
    name_kr: types.maybe(types.string),
  })
  .views((self) => ({}))
  .actions((self) => ({
    setIssueDate(dateString: string) {
      self.issue_date = dateString
      self.issueDateObject = DateTime.fromISO(dateString)
    },
    setIssueDateObject() {
      if (!self.issueDateObject) {
        self.issueDateObject = DateTime.fromISO(self.issue_date)
      }
      return self.issueDateObject
    },
  }))

type PrescriptionShortType = Instance<typeof PrescriptionShortModel>
export interface PrescriptionShort extends PrescriptionShortType {}
type PrescriptionShortSnapshotType = SnapshotOut<typeof PrescriptionShortModel>
export interface PrescriptionShortSnapshot extends PrescriptionShortSnapshotType {}
