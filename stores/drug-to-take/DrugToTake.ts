/**
 * [
  {
    id: 1051,
    alarm_name: "morning",
    prescription: { disease: { code: "M77.1", name_kr: "외측상과염" }, id: 103 },
    prescription_detail: { drug: "콜킨정(콜키신)", id: 65 },
    status: "ready",
  },
  {
    id: 1052,
    alarm_name: "evening",
    prescription: { disease: { code: "M77.1", name_kr: "외측상과염" }, id: 103 },
    prescription_detail: { drug: "콜킨정(콜키신)", id: 65 },
    status: "ready",
  },
]
 */

import { DateTime } from "luxon"
import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { stores } from "../"
import { TimeSlotName } from "../TimeSlot"
import { OwnerModel } from "./Owner"
import { PrescriptionDetailShortModel } from "./PrescriptionDetailShort"
import { PrescriptionShortModel } from "./PrescriptionShort"

export const DrugToTakeModel = types
  .model("DrugToTake")
  .props({
    id: types.identifierNumber,
    alarm_name: types.string,
    when: types.string,
    prescription: types.maybeNull(PrescriptionShortModel),
    prescription_detail: types.maybeNull(PrescriptionDetailShortModel),
    status: types.optional(types.enumeration(["ready", "taken"]), "ready"),
    owner: types.maybe(OwnerModel),

    /**
     * 홈화면에 처방전끼리 묶어서 처방약 개수를 표시할 때 사용합니다.
     */
    prescription_details_count: types.optional(types.number, 0),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setPrescriptionDetailsCount(count: number) {
      self.prescription_details_count = count
    },
    setStatus(status: "ready" | "taken") {
      self.status = status
    },
    belongsTo(timeSlotName) {
      const dateTimeWhen = DateTime.fromSQL(self.when)
      const { prescriptionStore } = stores
      const nightDateTime = prescriptionStore.nightDateTime

      switch (timeSlotName) {
        case TimeSlotName.WakeUp:
          return prescriptionStore.wakeupInterval.contains(dateTimeWhen)

        case TimeSlotName.Morning:
          return prescriptionStore.morningInterval.contains(dateTimeWhen)

        case TimeSlotName.Afternoon:
          return prescriptionStore.afternoonInterval.contains(dateTimeWhen)

        case TimeSlotName.Evening:
          if (
            nightDateTime.hour >= 0 &&
            nightDateTime.hour < 12 &&
            dateTimeWhen.hour >= 0 &&
            dateTimeWhen.hour < 12
          ) {
            return prescriptionStore.eveningInterval.contains(dateTimeWhen.plus({ days: 1 }))
          }
          return prescriptionStore.eveningInterval.contains(dateTimeWhen)

        case TimeSlotName.Night:
          if (
            nightDateTime.hour > 12 &&
            nightDateTime.hour < 24 &&
            dateTimeWhen.hour >= 0 &&
            dateTimeWhen.hour < 12
          ) {
            return prescriptionStore.nightInterval.contains(dateTimeWhen.plus({ days: 1 }))
          }
          return prescriptionStore.nightInterval.contains(dateTimeWhen)
      }
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type DrugToTakeType = Instance<typeof DrugToTakeModel>
export interface DrugToTake extends DrugToTakeType {}
type DrugToTakeSnapshotType = SnapshotOut<typeof DrugToTakeModel>
export interface DrugToTakeSnapshot extends DrugToTakeSnapshotType {}
