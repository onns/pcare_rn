import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import { applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"

import { withStatus } from "../extensions"
import { DrugToTake, DrugToTakeModel } from "./DrugToTake"
import { DrugToTakeGroup, DrugToTakeGroupModel } from "./DrugToTakeGroup"
import { MedicationProgressModel } from "./MedicationProgress"
import { MedicationScheduleModel } from "./MedicationSchedule"

export const TakingDrugStoreModel = types
  .model("TakingDrugStore")
  .props({
    drugsToTake: types.array(DrugToTakeModel),
    medicationSchedule: types.array(MedicationScheduleModel),
    medicationProgress: types.array(MedicationProgressModel),
    selectedTimeSlot: types.maybe(
      types.enumeration(["wakeup", "morning", "afternoon", "evening", "night"]),
    ),
    /**
     * 같은 처방 및 같은 복용알림 시간끼리 grouping 한 복약할 약제들. (key string: prescription id + drug2take's when value  eg. '123416:00')
     */
    drugToTakeGroups: types.map(DrugToTakeGroupModel),

    /**
     * 하루에 한번 먹을 약들을 저장합니다. alarm_name 이 'once' 인 data
     */
    drugsOnce: types.map(DrugToTakeGroupModel),
  })
  .views((self) => ({
    drugsForTime(timeSlot: string) {
      return self.drugsToTake.filter((drugToTake) => drugToTake.alarm_name === timeSlot)
    },
    get visibleDefaultTimeSlots() {
      const slots: Array<string> = []
      for (let i = 0; i < self.drugsToTake.length; i++) {
        const drugToTake = self.drugsToTake[i]
        if (
          !slots.includes(drugToTake.alarm_name) &&
          drugToTake.alarm_name !== "once" &&
          drugToTake.alarm_name !== "custom"
        ) {
          slots.push(drugToTake.alarm_name)
        }
      }

      // default 로 아침, 점심, 저녁 slot 표시하기 (https://onions.atlassian.net/browse/PAP-435)
      if (!slots.includes("morning")) {
        slots.push("morning")
      }
      if (!slots.includes("afternoon")) {
        slots.push("afternoon")
      }
      if (!slots.includes("evening")) {
        slots.push("evening")
      }

      // 시간대 순서대로 정렬
      slots.sort((a, b) => {
        // 0보다 작은 경우 a를 b보다 낮은 색인으로 정렬 (a가 먼저옴)
        if (
          a == "wakeup" &&
          (b == "morning" || b == "afternoon" || b == "evening" || b == "night")
        ) {
          return -1
        }
        if (a == "morning" && (b == "afternoon" || b == "evening" || b == "night")) {
          return -1
        }
        if (a == "afternoon" && (b == "evening" || b == "night")) {
          return -1
        }
        if (a == "evening" && b == "night") {
          return -1
        }

        if (a == "morning" && b == "wakeup") {
          return 1
        }
        if (a == "afternoon" && (b == "wakeup" || b == "morning")) {
          return 1
        }
        if (a == "evening" && (b == "wakeup" || b == "morning" || b == "afternoon")) {
          return 1
        }
        if (
          a == "night" &&
          (b == "wakeup" || b == "morning" || b == "afternoon" || b == "evening")
        ) {
          return 1
        }
        return 0
      })
      return slots
    },
    get onceDataForList() {
      const keys = self.drugsOnce.keys()
      const onceData = new Array<DrugToTake>()
      let key = keys.next()
      while (!key.done) {
        onceData.push(self.drugsOnce.get(key.value)[0])
        key = keys.next()
      }
      return onceData
    },
  }))
  .actions((self) => ({
    initiate() {
      self.drugsOnce.clear()
      self.drugToTakeGroups.clear()
      applySnapshot(self.drugsToTake, [])
      applySnapshot(self.medicationSchedule, [])
      applySnapshot(self.medicationProgress, [])
    },
    setDrugsToTake(drugsToTake: any) {
      self.drugToTakeGroups.clear()
      applySnapshot(self.drugsToTake, drugsToTake)

      // drugToTakeGroups 구성 (key: pid+when)
      const drugsNotOnce = drugsToTake.filter((value) => value.alarm_name !== "once")
      for (let i = 0; i < drugsNotOnce.length; i++) {
        const drugToTake = drugsNotOnce[i]
        const key = `${drugToTake.prescription.id}${drugToTake.when}`
        const group: DrugToTakeGroup | undefined = self.drugToTakeGroups.get(key)
        if (group) {
          group.drugsToTake.push(drugToTake.id)
        } else {
          self.drugToTakeGroups.set(
            key,
            DrugToTakeGroupModel.create({ id: key, drugsToTake: [drugToTake.id] }),
          )
        }
      }

      // 하루에 한번 먹는 약들만 재구성하여 drugsOnce 에 저장
      self.drugsOnce.clear()
      const drugsOnce = drugsToTake.filter((value) => value.alarm_name === "once")
      for (let i = 0; i < drugsOnce.length; i++) {
        const drugOnce = drugsOnce[i]
        const key = `${drugOnce.prescription.id}${drugOnce.when}`
        const group: DrugToTakeGroup | undefined = self.drugsOnce.get(key)
        if (group) {
          group.drugsToTake.push(drugOnce.id)
        } else {
          self.drugsOnce.set(
            key,
            DrugToTakeGroupModel.create({ id: key, drugsToTake: [drugOnce.id] }),
          )
        }
      }
    },
    setMedicationSchedule(schedule: any, familyMemberKey: string) {
      applySnapshot(self.medicationSchedule, schedule)
    },
    setMedicationProgress(progress: any, familyMemberKey: string) {
      applySnapshot(self.medicationProgress, progress)
    },
    setSelectedTimeSlot(timeSlot: "wakeup" | "morning" | "afternoon" | "evening" | "night") {
      self.selectedTimeSlot = timeSlot
    },
    clearAllGroup() {
      self.drugToTakeGroups.clear()
    },
    setMedicationStatus(
      prescriptionIdAndWhen: string,
      status: "ready" | "taken",
      type: "once" | undefined,
    ) {
      if (!type) {
        self.drugToTakeGroups.get(prescriptionIdAndWhen)?.drugsToTake.map((drugToTake) => {
          drugToTake.setStatus(status)
        })
      } else {
        self.drugsOnce.get(prescriptionIdAndWhen)?.drugsToTake.map((drugToTake) => {
          drugToTake.setStatus(status)
        })
      }
    },
  }))
  .extend(withStatus)
  .views((self) => ({
    get isLoading() {
      return self.status === "pending"
    },
  }))
  .actions((self) => ({
    fetchDrugsToTake: flow(function* (
      userId: number,
      familyMemberKey: string,
      childId?: number,
      noSpinner?: boolean,
      date?: string,
    ) {
      if (!api.apisauce.headers.Authorization) {
        return
      }
      if (!noSpinner) {
        // self.setStatusOfDrugsToTake(familyMemberKey, "pending")
        self.setStatus("pending")
      }
      const result = yield api.getDrugsToTake(userId, familyMemberKey, childId, date)
      if (result.kind === "ok") {
        firebase.analytics().logEvent("get_drug2take")
        amplitude.getInstance().logEvent("get_drug2take")
        self.setDrugsToTake(result.data)
        // self.setStatusOfDrugsToTake(familyMemberKey, "done")
        self.setStatus("done")
      } else if (result.kind === "cannot-connect" || result.temporary) {
        self.setStatus("done")
      } else if (result.kind === "unauthorized") {
        // self.setStatusOfDrugsToTake(familyMemberKey, "unauthorized")
        self.setStatus("error")
      } else {
        // self.setStatusOfDrugsToTake(familyMemberKey, "error")
        self.setStatus("error")
        console.tron.log("< TakingDrugStore > fetchDrugsToTake, error: " + result.kind)
      }
    }),
    fetchMedicationSchedule: flow(function* (
      userId: number,
      familyMemberKey: string,
      childId?: number,
      noSpinner?: boolean,
      date?: string,
    ) {
      if (!api.apisauce.headers.Authorization) {
        return
      }
      if (!noSpinner) {
        self.setStatus("pending")
      }
      const result = yield api.getMedicationSchedule(userId, familyMemberKey, childId, date)
      if (result.kind === "ok") {
        firebase.analytics().logEvent("get_prescription_schedule")
        amplitude.getInstance().logEvent("get_prescription_schedule")
        self.setMedicationSchedule(result.schedule, familyMemberKey)
        self.setMedicationProgress(result.progress, familyMemberKey)
        self.setStatus("done")
      } else if (result.kind === "cannot-connect" || result.temporary) {
        self.setStatus("done")
      } else if (result.kind === "unauthorized") {
        // self.setStatusOfDrugsToTake(familyMemberKey, "unauthorized")
        self.setStatus("error")
      } else {
        // self.setStatusOfDrugsToTake(familyMemberKey, "error")
        self.setStatus("error")
        console.tron.log("< TakingDrugStore > fetchMedicationSchedule, error: " + result.kind)
      }
    }),
  }))

type TakingDrugStoreType = Instance<typeof TakingDrugStoreModel>
export interface TakingDrugStore extends TakingDrugStoreType {}
type TakingDrugStoreSnapshotType = SnapshotOut<typeof TakingDrugStoreModel>
export interface TakingDrugStoreSnapshot extends TakingDrugStoreSnapshotType {}
