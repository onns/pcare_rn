import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const OwnerModel = types
  .model("Owner")
  .props({
    id: types.number,
    type: types.enumeration(["user", "family", "children"]),
  })
  .views((self) => ({}))
  .actions((self) => ({}))

type OwnerType = Instance<typeof OwnerModel>
export interface Owner extends OwnerType {}
type OwnerSnapshotType = SnapshotOut<typeof OwnerModel>
export interface OwnerSnapshot extends OwnerSnapshotType {}
