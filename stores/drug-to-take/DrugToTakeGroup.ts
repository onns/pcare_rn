import { DateTime } from "luxon"
import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { stores } from "../"
import { TimeSlotName } from "../TimeSlot"
import { DrugToTakeModel } from "./DrugToTake"

export const DrugToTakeGroupModel = types
  .model("DrugToTakeGroup")
  .props({
    id: types.identifier,
    drugsToTake: types.array(types.reference(DrugToTakeModel)),

    /**
     * 홈화면에 처방전끼리 묶어서 처방약 개수를 표시할 때 사용합니다.
     */
    prescription_details_count: types.optional(types.number, 0),
  })
  .views((self) => ({
    belongsTo(timeSlotName: string) {
      const dateTimeWhen = DateTime.fromSQL(self.id.slice(-5))
      const { prescriptionStore } = stores
      const nightDateTime = prescriptionStore.nightDateTime

      switch (timeSlotName) {
        case TimeSlotName.WakeUp:
          return prescriptionStore.wakeupInterval.contains(dateTimeWhen)

        case TimeSlotName.Morning:
          return prescriptionStore.morningInterval.contains(dateTimeWhen)

        case TimeSlotName.Afternoon:
          return prescriptionStore.afternoonInterval.contains(dateTimeWhen)

        case TimeSlotName.Evening:
          if (
            nightDateTime.hour >= 0 &&
            nightDateTime.hour < 12 &&
            dateTimeWhen.hour >= 0 &&
            dateTimeWhen.hour < 12
          ) {
            return prescriptionStore.eveningInterval.contains(dateTimeWhen.plus({ days: 1 }))
          }
          return prescriptionStore.eveningInterval.contains(dateTimeWhen)

        case TimeSlotName.Night:
          if (
            nightDateTime.hour > 12 &&
            nightDateTime.hour < 24 &&
            dateTimeWhen.hour >= 0 &&
            dateTimeWhen.hour < 12
          ) {
            return prescriptionStore.nightInterval.contains(dateTimeWhen.plus({ days: 1 }))
          }
          return prescriptionStore.nightInterval.contains(dateTimeWhen)
      }
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setPrescriptionDetailsCount(count: number) {
      self.prescription_details_count = count
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type DrugToTakeGroupType = Instance<typeof DrugToTakeGroupModel>
export interface DrugToTakeGroup extends DrugToTakeGroupType {}
type DrugToTakeGroupSnapshotType = SnapshotOut<typeof DrugToTakeGroupModel>
export interface DrugToTakeGroupSnapshot extends DrugToTakeGroupSnapshotType {}
