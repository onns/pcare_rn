import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { Luxon } from "../Luxon"
import { DiseaseShortModel } from "./DiseaseShort"

export const MedicationScheduleModel = types
  .model("MedicationSchedule")
  .props({
    id: types.identifierNumber,
    disease: DiseaseShortModel,
    start: Luxon,
    end: Luxon,
    issue_date: Luxon,
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type MedicationScheduleType = Instance<typeof MedicationScheduleModel>
export interface MedicationSchedule extends MedicationScheduleType {}
type MedicationScheduleSnapshotType = SnapshotOut<typeof MedicationScheduleModel>
export interface MedicationScheduleSnapshot extends MedicationScheduleSnapshotType {}
