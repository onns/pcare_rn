import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { PrescriptionDetailBannerModel } from "./PrescriptionDetailBanner"

export const PrescriptionDetailShortModel = types
  .model("PrescriptionDetailShort")
  .props({
    id: types.number,
    drug: types.string,
    drug_eng: types.maybeNull(types.string),
    /**
     * 1회 투여량
     */
    dose: types.number,
    /**
     * 1회 투여량 단위
     */
    unit: types.string,
    /**
     *  약제 이미지 URL
     */
    image_url: types.maybeNull(types.string),
    pause_take_dt: types.maybeNull(types.string),
    banners: types.maybeNull(types.array(PrescriptionDetailBannerModel)),
  })
  .views((self) => ({
    get shortenedName() {
      let end = self.drug.indexOf("[")
      if (end != -1) return self.drug.substring(0, end)

      end = self.drug.indexOf("(")
      if (end != -1) return self.drug.substring(0, end)

      end = self.drug.indexOf("/")
      if (end != -1) {
        return self.drug.substring(0, end)
      } else {
        return self.drug.substring(0, self.drug.length)
      }
    },
    get unitConverted() {
      switch (self.unit) {
        case "tab":
          return "정"
        case "pack":
          return "팩"
        case "btl":
          return "병"
        case "cap":
          return "캡슐"
        default:
          return self.unit
      }
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type PrescriptionDetailShortType = Instance<typeof PrescriptionDetailShortModel>
export interface PrescriptionDetailShort extends PrescriptionDetailShortType {}
type PrescriptionDetailShortSnapshotType = SnapshotOut<typeof PrescriptionDetailShortModel>
export interface PrescriptionDetailShortSnapshot extends PrescriptionDetailShortSnapshotType {}
