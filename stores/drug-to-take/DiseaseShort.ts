import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const DiseaseShortModel = types
  .model("DiseaseShort")
  .props({
    id: types.maybe(types.number),
    name_kr: types.maybe(types.string),
    code: types.maybe(types.string),
    rep_title: types.maybe(types.string),
    guide_message: types.maybe(types.string),
  })
  .views((self) => ({}))
  .actions((self) => ({}))

type DiseaseShortType = Instance<typeof DiseaseShortModel>
export interface DiseaseShort extends DiseaseShortType {}
type DiseaseShortSnapshotType = SnapshotOut<typeof DiseaseShortModel>
export interface DiseaseShortSnapshot extends DiseaseShortSnapshotType {}
