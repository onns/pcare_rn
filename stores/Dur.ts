import { types } from "mobx-state-tree" 
import { DurPregnancyModel } from "./DurPregnancy"
import { DurDisModel } from "./DurDis"
import { DurHngModel } from "./DurHng"
import { DurBygModel } from "./DurByg"
import { DurNosaleModel } from "./DurNosale"

// 'preg': [ {'detail_id', 1111, 'drug_id': 9803, 'drug_name', 'XX정', 'preg_grade': '2', 'preg_detail': 'ibuprofen: 임부에 대한 안전성 미확립. 임신 말기에 투여시 태아의 동맥관조기폐쇄 가능성.'}, ]  
// 'hng': [ {'presc1_id': 1,'drug1_id': 1, 'drug1_name', 'XX정', 'presc2_id':2, 'drug2_id': 2, 'drug2_name', 'XX', 'hng_group': '....', 'hng_groupn': 'Group01'}, ] 
// 'dis': [ {'presc1_id': 1,'drug1_id': 1, 'drug1_name', 'XX정', 'presc2_id':2, 'drug2_id': 2, 'drug2_name', 'XX' 'dur1': 'D000282', 'dur2':'', 'dur3':'', dur4:''}, ]
// 'byg': [ {'presc1_id': 1,'drug1_id': 1, 'drug1_name', 'XX정', 'presc2_id':2, 'drug2_id': 2, 'drug2_name', 'XX', 'side_effect':'...', 'threshold':'...'}, ]



export const DurModel = types.model("Dur")
.props({
  /**
   * 임부금기
   */
  preg: types.array(DurPregnancyModel),
  /**
   * 효능군 중복
   */
  hng: types.array(DurHngModel),
  /**
   * 동일 성분 중복
   */
  dis: types.array(DurDisModel),
  /**
   * 병용금기
   */
  byg: types.array(DurBygModel),
  /**
   * 판매중지 의약품
   */
  nosale: types.array(DurNosaleModel)
})

type DurType = typeof DurModel.Type
export interface Dur extends DurType {}