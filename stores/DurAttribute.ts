import { types } from "mobx-state-tree"

export const DurAttributeModel = types
  .model("DurAttribute")
  .props({
    /**
     * 임부금기
     */
    preg: types.optional(types.boolean, false),
    /**
     * 효능군 중복
     */
    hng: types.optional(types.boolean, false),
    /**
     * 동일 성분 중복
     */
    dis: types.optional(types.boolean, false),
    /**
     * 병용금기
     */
    byg: types.optional(types.boolean, false),
    /**
     * 판매중지
     */
    nosale: types.optional(types.boolean, false)
  })
  .actions(self => ({
    setPreg(enable: boolean) {
      self.preg = enable
    },
    setHng(enable: boolean) {
      self.hng = enable
    },
    setDis(enable: boolean) {
      self.dis = enable
    },
    setByg(enable: boolean) {
      self.byg = enable
    },
    setNosale(enable: boolean) {
      self.nosale = enable
    }
  }))

type DurAttributeType = typeof DurAttributeModel.Type
export interface DurAttribute extends DurAttributeType {}
