import React from "react"
import { Image, ImageStyle, TouchableOpacity, ViewStyle } from "react-native"

import SvgApple from "../assets/images/apple.svg"
import { color } from "../theme"

export interface Props {
  source: any
  onPress: any
  selected?: any
  style: ViewStyle
  circleStyle?: ImageStyle
  onLayout?: (event: {
    nativeEvent: { layout: { x: number; y: number; width: number; height: number } }
  }) => void
}

const androidRipple = { color: "rgba(0, 0, 0, .16)", radius: 28 }
const IMAGE: ImageStyle = {
  width: 56,
  height: 56,
  borderRadius: 28,
  backgroundColor: color.input,
}

export class CircleButton extends React.Component<Props> {
  render() {
    // grab the props
    const { source, onPress, selected, style, circleStyle, ...rest } = this.props

    return (
      <TouchableOpacity onPress={onPress} {...rest} style={style}>
        {source === "apple" ? (
          <SvgApple width={56} height={56} />
        ) : (
          <Image source={source} style={[IMAGE, circleStyle]} />
        )}
      </TouchableOpacity>
    )
  }
}

export default CircleButton
