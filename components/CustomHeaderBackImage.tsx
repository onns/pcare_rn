import React from "react"
import { Platform, View } from "react-native"

import SvgBackOrange from "../assets/images/back-orange.svg"
import SvgBack from "../assets/images/back.svg"

const CustomHeaderBackImage = (props) => {
  const { orange, ...rest } = props
  return (
    <View
      style={[
        {
          width: 60,
          height: 40,
          justifyContent: "center",
        },
        Platform.OS === "ios" ? { paddingLeft: 10 } : null,
      ]}
    >
      {orange ? <SvgBackOrange width={24} height={24} /> : <SvgBack width={24} height={24} />}
    </View>
  )
}

export default CustomHeaderBackImage
