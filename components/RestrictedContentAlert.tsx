/* eslint-disable react-native/no-color-literals */
import React, { FC, useState } from "react"
import { TextStyle, TouchableOpacity } from "react-native"
import { XStack } from "tamagui"

import { useNavigation } from "@react-navigation/native"
import { typography } from "@theme/typography"

import { getPopupButton, Popup } from "./Popup"
import { Text } from "./StyledText"

const GUIDE_TEXT: TextStyle = {
  ...typography.subButton,
  color: "#565E6D",
  paddingVertical: 16,
  paddingLeft: 10,
}

const BUTTON_TEXT: TextStyle = {
  fontSize: 15,
  lineHeight: 24,
  letterSpacing: -0.3,
  color: "#565E6D",
}
const hitSlop10 = {
  top: 10,
  left: 0,
  bottom: 10,
  right: 0,
}

/** component: RestrictedContentAlert
 * 둘러보기 기능 사용중 제한된 컨텐츠 영역 진입 시 표시하는 안내바
 **/
const RestrictedContentAlert: FC = () => {
  const navigation = useNavigation()
  const [isPopupVisible, setIsPopupVisible] = useState(false)

  return (
    <>
      <XStack br="$4" bc="$surfaceNeutral" alignItems="center" space>
        <Text style={GUIDE_TEXT}>둘러보기에서 제한된 컨텐츠가 있습니다.</Text>
        <TouchableOpacity hitSlop={hitSlop10} onPress={() => setIsPopupVisible(true)}>
          <Text style={BUTTON_TEXT}>로그인</Text>
        </TouchableOpacity>
      </XStack>
      <Popup
        isVisible={isPopupVisible}
        title={"로그인이 필요한 정보"}
        message={`해당 정보는 로그인을 통해 확인 할 수 있습니다.`}
        button1={getPopupButton(() => navigation.navigate("Auth", { screen: "SignIn" }), "로그인")}
        button2={getPopupButton(() => navigation.goBack(), "돌아가기", "cancel")}
      />
    </>
  )
}

export { RestrictedContentAlert }
