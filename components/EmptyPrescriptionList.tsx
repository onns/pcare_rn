import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { Image, TextStyle, View, ViewStyle } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"

import SvgEmpty from "../assets/images/emptyPrescriptions.svg"
import { Text } from "../components/StyledText"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"

const PRESCRIPTION_ITEM: ViewStyle = {
  height: 350,
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  paddingHorizontal: 16,
  paddingTop: 12,
  paddingBottom: 9,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const TITLE: TextStyle = {
  ...typography.title3,
  opacity: 0.5,
}
const DESCRIPTION: TextStyle = {
  ...typography.sub2,
  color: "#999999",
  textAlign: "center",
}
const SAMPLE_BUTTON: ViewStyle = {
  width: 295,
  height: 56,
  borderRadius: 5,
  backgroundColor: color.palette.white,
  borderWidth: 1,
  borderColor: color.palette.paleSalmon,
  justifyContent: "center",
  alignItems: "center",
}
const SAMPLE_BUTTON_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 17,
  lineHeight: 20,
  letterSpacing: -0.34,
  color: color.primary,
}

export interface Props {
  onLoadSamplePress?: () => void
  showSampleButton: boolean
  userId: number
  prescriptionStore?: PrescriptionStore
}

@inject("prescriptionStore")
@observer
export default class EmptyPrescriptionList extends Component<Props> {
  render() {
    const { onLoadSamplePress, showSampleButton, userId, prescriptionStore } = this.props
    return (
      <View>
        <View style={PRESCRIPTION_ITEM}>
          <SvgEmpty width={41} height={54.64} style={{ marginBottom: 19 }} />
          <View style={ROW}>
            <Text style={TITLE}>등록된 처방이 없습니다.</Text>
          </View>
          <View style={{ height: 10 }}></View>
          <View style={ROW}>
            <Text style={DESCRIPTION}>
              화면 아래{" "}
              <Text style={{ ...typography.title3, color: color.palette.orange }}> + </Text>를 눌러
              처방전 또는 약봉투를 찍으세요.
            </Text>
          </View>
          {showSampleButton ? (
            <View style={{ flexDirection: "row", paddingTop: 30 }}>
              <TouchableOpacity
                disabled={prescriptionStore.statusOfPrescriptions.get("_u" + userId) === "pending"}
                style={SAMPLE_BUTTON}
                onPress={onLoadSamplePress}
              >
                <Text style={SAMPLE_BUTTON_TEXT}>예시 보기</Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      </View>
    )
  }
}
