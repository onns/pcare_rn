import { DateTime } from "luxon"
import { observer } from "mobx-react"
import { SectionList } from "native-base"
import View from "native-base-theme/components/View"
import React, { useRef, useState } from "react"
import { FlatList, ScrollView, TextStyle, TouchableOpacity, ViewStyle } from "react-native"

import SvgAfternoon from "../assets/images/timeAfternoonOrange.svg"
import SvgEvening from "../assets/images/timeEveningOrange.svg"
import SvgMorning from "../assets/images/timeMorningOrange.svg"
import SvgNight from "../assets/images/timeNightOrange.svg"
import SvgWakeup from "../assets/images/timeWakeupOrange.svg"
import { stores } from "../stores"
import { TimeSlotName } from "../stores/TimeSlot"
import { color, typography } from "../theme"
import { Text } from "./StyledText"

const SLOT_TEXT_SELECTED: TextStyle = {
  ...typography.heading.section,
  color: color.palette.orange,
}
const SLOT_TEXT: TextStyle = {
  ...typography.heading.section,
}
const ALIGNMENT_ROW: ViewStyle = {
  overflow: "hidden",
  marginTop: 24,
  marginHorizontal: 16,
  flexDirection: "column",
  alignItems: "flex-end",
}

export interface TimeSlotRowProps {
  onPress: (timeSlot: string) => void
  slots: Array<string>
}
const SLOT_BUTTON: ViewStyle = {
  height: 48,
  flexDirection: "row",
  paddingHorizontal: 12,
  paddingVertical: 8,
  marginRight: 8,
  alignItems: "center",
}
const SLOT_BUTTON_SELECTED: ViewStyle = {
  ...SLOT_BUTTON,
  // flexDirection: "column",
  borderColor: color.palette.orange,
  borderBottomWidth: 4,
}

export const getNow = () => {
  const now = DateTime.local()
  const { prescriptionStore } = stores
  if (prescriptionStore.wakeupInterval.contains(now)) {
    return "wakeup"
  } else if (prescriptionStore.morningInterval.contains(now)) {
    return "morning"
  } else if (prescriptionStore.afternoonInterval.contains(now)) {
    return "afternoon"
  } else if (prescriptionStore.eveningInterval.contains(now)) {
    return "evening"
  } else {
    return "night"
  }
}

export const getText = (slot: string) => {
  if (!slot) {
    slot = getNow()
  }

  switch (slot) {
    case "wakeup":
      return "기상직후"
    case "morning":
      return "아침"
    case "afternoon":
      return "점심"
    case "evening":
      return "저녁"
    case "night":
      return "취침전"
    case "once":
      return "아무때나 한번"
  }
}

export const getTimeText = (slot: TimeSlotName) => {
  const { prescriptionStore } = stores
  let time
  switch (slot) {
    case TimeSlotName.WakeUp:
      time = prescriptionStore.wakeupTimeString
      break
    case TimeSlotName.Morning:
      time = prescriptionStore.morningTimeString
      break
    case TimeSlotName.Afternoon:
      time = prescriptionStore.afternoonTimeString
      break
    case TimeSlotName.Evening:
      time = prescriptionStore.eveningTimeString
      break
    case TimeSlotName.Night:
      time = prescriptionStore.nightTimeString
      break
    default:
      time = ["", ""]
  }
  return `${time[0]}${time[1]}`
}

export const TimeSlotRowNew: React.FunctionComponent<TimeSlotRowProps> = observer((props) => {
  const scrollViewRef = useRef(null)
  const { onPress, slots } = props
  const nowSlotName = getNow()
  const index = slots.indexOf(nowSlotName)

  const [selected, setSelected] = useState(
    index != -1 ? slots[index] : slots.length > 0 ? slots[0] : undefined,
  )

  const onSelect = (slot: string, index: number) => {
    setSelected(slot)
    // scrollViewRef.current.scrollTo({ x: 37, y: 0, animated: true })
    onPress(slot)
  }

  return (
    <ScrollView
      ref={scrollViewRef}
      contentContainerStyle={ALIGNMENT_ROW}
      horizontal={true}
      showsHorizontalScrollIndicator={true}
    >
      {slots.map((slot, index) => {
        return (
          <TouchableOpacity
            key={index}
            onPress={() => onSelect(slot, index)}
            style={slot === selected ? SLOT_BUTTON_SELECTED : SLOT_BUTTON}
            activeOpacity={0.8}
          >
            {/* {slot === "wakeup" && (
              <SvgWakeup width={slot === selected ? 24 : 0} height={slot === selected ? 24 : 0} />
            )}
            {slot === "morning" && (
              <SvgMorning width={slot === selected ? 24 : 0} height={slot === selected ? 24 : 0} />
            )}
            {slot === "afternoon" && (
              <SvgAfternoon
                width={slot === selected ? 24 : 0}
                height={slot === selected ? 24 : 0}
              />
            )}
            {slot === "evening" && (
              <SvgEvening width={slot === selected ? 24 : 0} height={slot === selected ? 24 : 0} />
            )}
            {slot === "night" && (
              <SvgNight width={slot === selected ? 24 : 0} height={slot === selected ? 24 : 0} />
            )}
             */}

            {slot === "wakeup" && <SvgWakeup width={24} height={24} />}
            {slot === "morning" && <SvgMorning width={24} height={24} />}
            {slot === "afternoon" && <SvgAfternoon width={24} height={24} />}
            {slot === "evening" && <SvgEvening width={24} height={24} />}
            {slot === "night" && <SvgNight width={24} height={24} />}
            <Text style={slot === selected ? SLOT_TEXT_SELECTED : SLOT_TEXT}>{getText(slot)}</Text>
          </TouchableOpacity>
        )
      })}
    </ScrollView>
  )
})
