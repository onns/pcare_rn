import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { ScrollView } from "native-base"
import React from "react"
import { Alert, TextStyle, TouchableOpacity, ViewStyle } from "react-native"
import FastImage, { ImageStyle } from "react-native-fast-image"

import { api } from "@api/api"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import SvgAdd from "../assets/images/addFamilyMember.svg"
import { Text } from "../components/StyledText"
import { RootNavigation } from "../navigation/NavigationService"
import { AccountStore } from "../stores/AccountStore"
import { FamilyMember } from "../stores/FamilyMember"
import { color, typography } from "../theme"

const FAMILY_ROW_CONTAINER: ViewStyle = {
  height: 123,
  flexDirection: "row",
  alignItems: "center",
  paddingLeft: 12,
  margin: 0,
  backgroundColor: color.palette.white,
}
const MEMBER_BUTTON: ViewStyle = {
  width: 80,
  alignItems: "center",
  paddingHorizontal: 8,
  marginRight: 8,
}
const CIRCLE_SELECTED: ImageStyle = {
  width: 64,
  height: 64,
  borderRadius: 32,
  borderWidth: 2,
  borderColor: color.primary,
  justifyContent: "center",
  alignItems: "center",
}
const CIRCLE: ImageStyle = {
  width: 64,
  height: 64,
  borderRadius: 32,
  justifyContent: "center",
  alignItems: "center",
}
const NAME_SELECTED: TextStyle = {
  ...typography.sub1,
  lineHeight: 19,
  color: color.text,
  marginTop: 8,
}
const NAME: TextStyle = {
  ...NAME_SELECTED,
  opacity: 0.3,
}
const ADD_MEMBER_TEXT: TextStyle = {
  ...typography.sub1,
  lineHeight: 19,
  color: color.primary,
  marginTop: 8,
}

export interface Props {
  accountStore?: AccountStore
  onSelect: any
}

export interface State {
  showFamily: boolean
}

@inject("accountStore")
@observer
export default class FamilyRow extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      showFamily: false,
    }
  }

  onAddMemberPress = () => {
    const { accountStore } = this.props
    amplitude.getInstance().logEvent("press AddFamilyMember")
    if (!api.apisauce.headers.Authorization) {
      Alert.alert(
        "회원가입이 필요한 서비스입니다",
        "가족회원 관리는 회원가입이 필요한 서비스입니다. 서비스 사용을 위해 회원가입을 해주세요.",
        [
          { text: "나중에", onPress: () => null },
          {
            text: "회원가입",
            onPress: () =>
              RootNavigation.navigate("Auth", {
                screen: "SignUp",
                params: { hideTourButton: true, prevScreen: "HomeStack" },
              }),
          },
        ],
        { cancelable: false },
      )
    } else {
      RootNavigation.navigate("Details", {
        screen: "AddFamilyMember",
      })
    }
  }

  render() {
    const { accountStore, onSelect, ...rest } = this.props
    const { user, family, selectedMemberKey } = accountStore

    return (
      <ScrollView
        contentContainerStyle={FAMILY_ROW_CONTAINER}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        bounces={false}
      >
        <TouchableOpacity
          key={user!.id}
          onPress={() => this.onPress(`_u${user.id}`)}
          {...rest}
          style={MEMBER_BUTTON}
        >
          <FastImage
            style={selectedMemberKey === `_u${user.id}` ? CIRCLE_SELECTED : CIRCLE}
            source={this.getImageSource(user.profile_image)}
            resizeMode={FastImage.resizeMode.cover}
          />
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={selectedMemberKey === `_u${user.id}` ? NAME_SELECTED : NAME}
          >
            {user!.nickname}
          </Text>
        </TouchableOpacity>
        {family.map((member: FamilyMember) => {
          return (
            <TouchableOpacity
              key={member.familyMemberKey}
              onPress={() => this.onPress(member.familyMemberKey, member)}
              {...rest}
              style={MEMBER_BUTTON}
            >
              <FastImage
                style={selectedMemberKey === member.familyMemberKey ? CIRCLE_SELECTED : CIRCLE}
                source={this.getImageSource(member.img)}
                resizeMode={FastImage.resizeMode.cover}
              />
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={selectedMemberKey === member.familyMemberKey ? NAME_SELECTED : NAME}
              >
                {member.name}
              </Text>
            </TouchableOpacity>
          )
        })}
        <TouchableOpacity onPress={this.onAddMemberPress} {...rest} style={MEMBER_BUTTON}>
          <SvgAdd width={64} height={64} />
          <Text style={ADD_MEMBER_TEXT}>가족 추가</Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }

  getImageSource(img: any): any {
    if (img == null || img == "") {
      return {
        uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png`,
        priority: FastImage.priority.high,
      }
    } else {
      return {
        uri: img,
        priority: FastImage.priority.high,
      }
    }
  }

  onPress(memberKey: string, member?: FamilyMember) {
    amplitude.getInstance().logEvent("press member", { memberKey: memberKey })
    const { accountStore } = this.props
    accountStore.setSelectedMemberKey(memberKey)
    accountStore.setSelectedMemberName(
      memberKey.includes("u")
        ? accountStore.user.nickname
        : member?.name ?? accountStore.selectedMemberName,
    )
    accountStore.setSelectedMemberProfileImage(
      memberKey.includes("u")
        ? accountStore.user.profile_image
        : member?.img ?? `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png`,
    )
    this.props.onSelect(memberKey, member)
  }
}
