import React from "react"
import { StyleSheet, View, ViewStyle } from "react-native"

type Props = {
  style?: any
  fillStyle?: ViewStyle
  isDown?: boolean
}

const Triangle = ({ style, fillStyle, isDown }: Props) => (
  <View style={[styles.triangle, style, isDown ? styles.down : {}]}>
    <View style={[styles.fill, fillStyle]}></View>
  </View>
)

const styles = StyleSheet.create({
  down: {
    transform: [{ rotate: "180deg" }],
  },
  triangle: {
    // width: 0,
    // height: 0,
    // backgroundColor: 'transparent',
    // borderStyle: 'solid',
    // borderLeftWidth: 8,
    // borderRightWidth: 8,
    // borderBottomWidth: 8,
    // borderLeftColor: 'transparent',
    // borderRightColor: 'transparent',
    // borderBottomColor: 'white',

    height: 0,
    width: 0,
    borderStyle: "solid",
    borderLeftWidth: 8,
    borderRightWidth: 8,
    borderBottomWidth: 8,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "#e4e4e4",
    left: 0,
    top: 1,
    marginLeft: 62,
    zIndex: 1,
    position: "relative",
  },
  fill: {
    position: "relative",
    left: -7,
    top: 1,
    height: 0,
    width: 0,
    borderStyle: "solid",
    borderLeftWidth: 7,
    borderRightWidth: 7,
    borderBottomWidth: 7,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "#fff",
    zIndex: 2,
  },
})

// position: absolute;
// bottom: ${(props: any) => props.bottom - props.size};
// left: ${(props: any) => props.left};
// width: ${(props: any) => props.size};
// height: ${(props: any) => props.size};
// background-color: transparent;
// border-style: solid;
// border-top-width: ${(props: any) => props.size};
// border-right-width: ${(props: any) => props.size};
// border-bottom-width: 0;
// border-left-width: ${(props: any) => props.size};
// border-top-color: ${(props: any) => props.backgroundColor || '#fff'};
// border-right-color: transparent;
// border-bottom-color: transparent;
// border-left-color: transparent;

export default Triangle
