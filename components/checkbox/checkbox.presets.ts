import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { color, spacing } from "../../theme"


const ROOT: ViewStyle = {
  
  flexDirection: "row",
  paddingRight: spacing[2],
  paddingBottom: spacing[1],
}

const DIMENSIONS = { width: 26, height: 26, }

const OUTLINE: ViewStyle = {
  ...DIMENSIONS,
  justifyContent: "center",
  alignItems: "center",
}

const FILL: ViewStyle = {
  width: DIMENSIONS.width,
  height: DIMENSIONS.height,
  justifyContent: "center",
  alignItems: "center"
}

const IMAGE: ImageStyle = {
  width: 16.3,
  height: 12
}

const BASE_TEXT: TextStyle = {
  paddingHorizontal: spacing[3],
}

export const rootPresets = {
  /**
   * 박스 표시는 없고, 체크 표시만 있는 형태 
   */
  primary: { ...ROOT } as ViewStyle,

  /**
   * 회색 원 바탕에 체크하면 흰 체크에 다른 바탕색이 채워지는 형태
   */
  bgCircle: {
    ...ROOT,
  } as ViewStyle,

  bgRect: {
    ...ROOT,
  } as ViewStyle,
}

export const outlinePresets = {
  /**
   * 박스 표시는 없고, 체크 표시만 있는 형태 
   */
  primary: { ...OUTLINE } as ViewStyle,

  /**
   * 회색 원 바탕에 체크하면 흰 체크에 다른 바탕색이 채워지는 형태
   */
  bgCircle: {
    ...OUTLINE,
    borderRadius: 13,
    backgroundColor: color.palette.grey5,
  } as ViewStyle,

  /**
   * 회색 사각형 바탕에 체크하면 흰 체크에 다른 바탕색이 채워지는 형태
   */
  bgRect: {
    ...OUTLINE,
    borderRadius: 4,
    backgroundColor: color.palette.grey5,
  } as ViewStyle,
}

export const fillPresets = {
  /**
   * 박스 표시는 없고, 체크 표시만 있는 형태 
   */
  primary: { ...FILL } as ViewStyle,

  /**
   * 회색 원 바탕에 체크하면 흰 체크에 다른 바탕색이 채워지는 형태
   */
  bgCircle: {
    ...FILL,
    borderRadius: 13,
    backgroundColor: color.palette.brightCyan2,
  } as ViewStyle,

  /**
   * 회색 사각형 바탕에 체크하면 흰 체크에 다른 바탕색이 채워지는 형태
   */
  bgRect: {
    ...FILL,
    borderRadius: 4,
    backgroundColor: color.palette.brightCyan2,
  } as ViewStyle,
}

export const imagePresets = {
  /**
   * 박스 표시는 없고, 체크 표시만 있는 형태 
   */
  primary: { ...IMAGE, tintColor: color.palette.brightCyan2 } as ImageStyle,

  /**
   * 회색 원 바탕에 체크하면 흰 체크에 다른 바탕색이 채워지는 형태
   */
  bgCircle: {
    ...IMAGE,
  } as ImageStyle,

  /**
   * 회색 사각형 바탕에 체크하면 흰 체크에 다른 바탕색이 채워지는 형태
   */
  bgRect: {
    ...IMAGE,
  } as ImageStyle,
}

/**
 * A list of preset names.
 */
export type CheckboxPresetNames = keyof typeof rootPresets
