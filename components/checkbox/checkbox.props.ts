import { ViewStyle } from "react-native"
import { CheckboxPresetNames } from "./checkbox.presets"

export interface CheckboxProps {
  /**
   * Additional container style. Useful for margins.
   */
  style?: ViewStyle | ViewStyle[]

  /**
   * Additional outline style.
   */
  outlineStyle?: ViewStyle | ViewStyle[]

  /**
   * Additional fill style. Only visible when checked.
   */
  fillStyle?: ViewStyle | ViewStyle[]

  imageStyle?: ViewStyle | ViewStyle[]

  /**
   * Is the checkbox checked?
   */
  value?: boolean

  /**
   * The text to display
   */
  text?: string

  /**
   * Multiline or clipped single line?
   */
  multiline?: boolean

  /**
   * Fires when the user tabs to change the value.
   */
  onToggle?: (newValue: boolean) => void

  /**
   * One of the different types of text presets.
   */
  preset?: CheckboxPresetNames
}
