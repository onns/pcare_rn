import * as React from "react"
import { TouchableOpacity, TextStyle, ViewStyle, View, Image, ImageStyle } from "react-native"
import { Text } from "../StyledText"
import { color, spacing } from "../../theme"
import { CheckboxProps } from "./checkbox.props"
import { mergeAll, flatten } from "ramda"
import { rootPresets, outlinePresets, fillPresets, imagePresets } from "./checkbox.presets"


const checkImageSource = require("../../assets/images/iconCheck.png")

const LABEL: TextStyle = { paddingLeft: spacing[2] }
const TRANSPARENT: ViewStyle = { backgroundColor: "transparent" }
const FALSE_IMAGE: ImageStyle = { tintColor: color.palette.grey5 }

export function Checkbox(props: CheckboxProps) {
  const {
    preset = "primary"
  } = props
  const numberOfLines = props.multiline ? 0 : 1

  const rootStyle = mergeAll(flatten([rootPresets[preset] || rootPresets.primary, props.style]))
  const outlineStyle = mergeAll(flatten([outlinePresets[preset] || outlinePresets.primary, props.outlineStyle]))
  const fillStyle = mergeAll(flatten([fillPresets[preset] || fillPresets.primary, props.fillStyle]))
  const imageStyle = mergeAll(flatten([imagePresets[preset] || imagePresets.primary, props.imageStyle]))

  const onPress = props.onToggle ? () => props.onToggle && props.onToggle(!props.value) : null


  return (
    <TouchableOpacity
      activeOpacity={1}
      disabled={!props.onToggle}
      onPress={onPress}
      style={rootStyle}
    >
      <View style={outlineStyle}>
        <View style={props.value ? fillStyle : TRANSPARENT}>
          <Image source={checkImageSource} style={props.value ? imageStyle : FALSE_IMAGE} />
        </View>
      </View>
      <Text numberOfLines={numberOfLines} style={LABEL}>{props.text}</Text>
    </TouchableOpacity>
  )
}