import SvgBad from "@assets/images/emojies_bad.svg"
import SvgGood from "@assets/images/emojies_good.svg"
import SvgNeutral from "@assets/images/emojies_neutral.svg"
import SvgSoBad from "@assets/images/emojies_sobad.svg"
import SvgPrettyGood from "@assets/images/emojies_sogood.svg"

export const SvgImporter = { SvgSoBad, SvgBad, SvgNeutral, SvgGood, SvgPrettyGood }
