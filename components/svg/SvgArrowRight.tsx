import * as React from "react"
import Svg, { Path, SvgProps } from "react-native-svg"

export const SvgArrowRight = ({ fill = "#C2C1C1", ...props }: SvgProps) => (
  <Svg width={24} height={24} fill="none" {...props}>
    <Path
      d="m7.795 17.9 5.579-5.591-5.579-5.591L9.51 5l7.309 7.309-7.309 7.308L7.795 17.9Z"
      fill={fill}
    />
  </Svg>
)
