/** URL polyfill. Required for Supabase queries to work in React Native. */
import "react-native-url-polyfill/auto"

import { array } from "mobx-state-tree/dist/internal"
import { empty } from "ramda"
import React, { useEffect, useState } from "react"
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from "react-native"
import { Button, Input, ListItem } from "react-native-elements"
import { ScrollView } from "react-native-gesture-handler"

import { getMemos, supabase } from "@api/supabase"
import { SvgImporter } from "@components/svg/SvgImporter"

import { useStores } from "../../App"
import { color, typography } from "../../theme"

type DailyMemo = {
  id: number
  user_id: string
  content: string
  rating: number
  created_at: string | null
}

type EmojiType = "so_bad" | "bad" | "neutral" | "good" | "pretty_good"
type EmojiStatus = "selected" | "unselected"

const EmojiType = [
  { type: "so_bad", rating: 1 },
  { type: "bad", rating: 2 },
  { type: "neutral", rating: 3 },
  { type: "good", rating: 4 },
  { type: "pretty_good", rating: 5 },
]

export default function DailyFeelinNote() {
  const [memos, setMemos] = useState<Array<DailyMemo>>([])
  if (!memos) {
    throw new Error("memos is null")
  }
  const [newMemoText, setNewMemoText] = useState<string>("")
  const accountStore = useStores().accountStore
  const [feelingRating, setFeelingRating] = useState<number>(0)

  const [selectedEmoji, setSelectedEmoji] = useState<EmojiType | null>(null)
  const handleEmojiClick = (type: EmojiType, rating: number) => {
    setFeelingRating(rating)
    setSelectedEmoji(type)
  }
  const emojiList = [
    {
      type: "so_bad",
      status: {
        selected: require("@assets/images/emojies_sobad.png"),
        unselected: require("@assets/images/emojies_sobad_unactivated.png"),
      },
      rating: 1,
    },
    {
      type: "bad",
      status: {
        selected: require("@assets/images/emojies_bad.png"),
        unselected: require("@assets/images/emojies_bad_unactivated.png"),
      },
      rating: 2,
    },
    {
      type: "neutral",
      status: {
        selected: require("@assets/images/emojies_neutral.png"),
        unselected: require("@assets/images/emojies_neutral_unactivated.png"),
      },
      rating: 3,
    },
    {
      type: "good",
      status: {
        selected: require("@assets/images/emojies_good.png"),
        unselected: require("@assets/images/emojies_good_unactivated.png"),
      },
      rating: 4,
    },
    {
      type: "pretty_good",
      status: {
        selected: require("@assets/images/emojies_sogood.png"),
        unselected: require("@assets/images/emojies_sogood_unactivated.png"),
      },
      rating: 5,
    },
  ]

  const renderEmoji = () => {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: 8,
          marginBottom: 16,
        }}
      >
        {emojiList.map((emoji) => {
          const selected = emoji.type === selectedEmoji
          const icon = selected ? emoji.status.selected : emoji.status.unselected
          return (
            <TouchableOpacity
              key={emoji.type}
              onPress={() => handleEmojiClick(emoji.type, emoji.rating)}
              activeOpacity={0.9}
            >
              <Image source={icon} style={{ height: 56, width: 56 }} />
            </TouchableOpacity>
          )
        })}
        {/* <Text>컨디셤 점수: {feelingRating}</Text> */}
      </View>
    )
  }

  useEffect(() => {
    fetchMemos()
  }, [])

  function timestampFormat(timestamp) {
    // timestamp를 Date 객체로 변환
    const date = new Date(timestamp)
    // 시간 12시간제로 변환
    let ampm = date.getHours() >= 12 ? "오후 " : "오전 "
    const translateHours = date.getHours() >= 12 ? date.getHours() - 12 : date.getHours()

    // 연도, 월, 일, 시, 분, 초
    const year = date.getFullYear()
    const month = ("0" + (date.getMonth() + 1).toString()).slice(-2)
    const day = ("0" + date.getDate().toString()).slice(-2)
    const hour = ampm + ("0" + translateHours.toString()).slice(-2)
    const minute = ("0" + date.getMinutes().toString()).slice(-2)

    return `${year}년 ${month}월 ${day}일, ${hour}시 ${minute}분`
  }

  // API Handling
  const fetchMemos = async () => {
    const { data: memos, error } = await supabase
      .from<DailyMemo>("daily_memo")
      .select("*")
      .order("id", { ascending: false })
      .eq("user_id", accountStore.user.id)

    if (error) {
      console.log("error", error)
    } else {
      setMemos(memos!)
      console.log("fetching:", memos)
    }
  }

  const addMemo = async (contentText: string) => {
    const content = contentText.trim()
    // // 글자수 1000자 못넘게 만들기
    // if (content.length > 20) {
    //   Alert.alert("글자수가 너무 많습니다.")
    //   return
    // }
    console.log("newcontent:", content)
    if (content.length) {
      const { data: memo, error } = await supabase.from<DailyMemo>("daily_memo").insert({
        user_id: accountStore.user.id,
        content,
        rating: feelingRating,
        created_at: new Date(),
      })
      // .single()
      if (error) {
        console.log(error.message)
      } else {
        setMemos([memo!, ...memos]) // 새로운 memo를 기존 memos에 추가
        setNewMemoText("") // 인풋창 빈 글로 초기화
        fetchMemos()
      }
    }
  }

  const deleteMemo = async (id: number) => {
    const { error } = await supabase.from<DailyMemo>("daily_memo").delete().eq("id", id)
    if (error) {
      console.log("error", error)
    } else {
      setMemos(memos.filter((x) => x.id !== Number(id)))
    }
  }

  return (
    <View style={styles.container}>
      <View style={{ paddingVertical: 16, paddingHorizontal: 16 }}>
        <View>{renderEmoji()}</View>
        <Input
          label="메모(증상, 기분 등을 남겨보세요)"
          labelStyle={{
            ...typography.ui.default,
            fontWeight: "500",
            color: color.textColor.subdued,
          }}
          inputContainerStyle={styles.inputContainerStyle}
          // leftIcon={{ type: "font-awesome", name: "contents" }}
          onChangeText={(text) => setNewMemoText(text)}
          value={newMemoText}
          containerStyle={{ paddingHorizontal: 4 }}
        />

        <Button
          onPress={() => addMemo(newMemoText)}
          title="저장"
          titleStyle={{ ...typography.buttons.scaledUp, color: color.textOn.primary }}
          buttonStyle={{
            backgroundColor: color.actionPrimary.default,
            borderRadius: 12,
            height: 48,
          }}
          disabled={!newMemoText || !feelingRating}
          disabledStyle={{
            backgroundColor: color.actionPrimary.disabled,
          }}
          disabledTitleStyle={{ ...typography.buttons.scaledUp, color: color.textOn.primary }}
        />
      </View>
      <View style={styles.sectionDivider} />
      <ScrollView style={styles.verticallySpaced}>
        <FlatList
          scrollEnabled={true}
          data={memos}
          keyExtractor={(item) => `${item?.id}`}
          renderItem={({ item: memo }) => (
            <>
              <ListItem>
                <View style={{ flexDirection: "row" }}>
                  {memo?.rating === 1 && (
                    <Image
                      source={require("@assets/images/emojies_sobad.png")}
                      style={{ height: 32, width: 32, marginRight: 8 }}
                    />
                  )}
                  {memo?.rating === 2 && (
                    <Image
                      source={require("@assets/images/emojies_bad.png")}
                      style={{ height: 32, width: 32, marginRight: 8 }}
                    />
                  )}
                  {memo?.rating === 3 && (
                    <Image
                      source={require("@assets/images/emojies_neutral.png")}
                      style={{ height: 32, width: 32, marginRight: 8 }}
                    />
                  )}
                  {memo?.rating === 4 && (
                    <Image
                      source={require("@assets/images/emojies_good.png")}
                      style={{ height: 32, width: 32, marginRight: 8 }}
                    />
                  )}
                  {memo?.rating === 5 && (
                    <Image
                      source={require("@assets/images/emojies_sogood.png")}
                      style={{ height: 32, width: 32, marginRight: 8 }}
                    />
                  )}
                  <View style={{ flexDirection: "column", flex: 1 }}>
                    {memo?.content === "" ? (
                      <Text style={{ ...typography.paragraph.default }}>
                        작성된 메모가 없습니다.
                      </Text>
                    ) : (
                      <Text style={{ ...typography.paragraph.default }}>{memo?.content}</Text>
                    )}
                    {/* <Text style={{ ...typography.paragraph.default }}>{memo?.content}</Text> */}
                    <Text style={{ ...typography.captions.small, color: color.textColor.subdued }}>
                      {timestampFormat(memo?.created_at)}
                    </Text>
                  </View>
                  <Button
                    onPress={() => deleteMemo(memo?.id)}
                    title="삭제"
                    titleStyle={{ ...typography.buttons.small, color: color.textColor.action }}
                    buttonStyle={{
                      backgroundColor: color.actionSecondary.default,
                      borderRadius: 12,
                      paddingHorizontal: 12,
                      height: 40,
                    }}
                  />
                </View>
              </ListItem>
              <View style={{ paddingLeft: 20 }}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderColor: color.borders.dividerBetweenUI,
                  }}
                />
              </View>
            </>
          )}
          ListEmptyComponent={() => (
            <View style={{ padding: 16 }}>
              <Text>아직 기록이 없습니다.</Text>
            </View>
          )}
        />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  verticallySpaced: {
    paddingVertical: 4,
    // alignSelf: "stretch",
    width: "100%",
  },
  sectionDivider: {
    height: 16,
    backgroundColor: color.surface.neutralDefault,
  },
  inputContainerStyle: {
    borderBottomWidth: 1,
    borderBottomColor: color.borders.neutralSubded,
  },
})
