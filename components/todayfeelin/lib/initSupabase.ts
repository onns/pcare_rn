import { supabase_anon_key, supabase_url } from "@api/supabase"
import AsyncStorage from "@react-native-async-storage/async-storage"
import { createClient } from "@supabase/supabase-js"

import { Database } from "./database.types"

// const supabase_url = "https://iwqlvaajryklkjvbwzby.supabase.co"
// const supabase_anon_key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Iml3cWx2YWFqcnlrbGtqdmJ3emJ5Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2Nzg0MTI5MjEsImV4cCI6MTk5Mzk4ODkyMX0.njWQyWrjS2EpgmJU4pVOqiqxhtGISjSZ8pqg5S0zyz4"

export const supabase = createClient<Database>(supabase_url, supabase_anon_key, {
  localStorage: AsyncStorage as any,
  autoRefreshToken: true,
  persistSession: true,
  detectSessionInUrl: false,
})

export async function getMemos(params: type) {
  return await supabase.from("daily_memo").select("*")
}
type MemosResponse = Awaited<ReturnType<typeof getMemos>>
export type MemosResponseSuccess = MemosResponse["data"]
export type MemosResponseError = MemosResponse["error"]
