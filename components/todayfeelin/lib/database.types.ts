export type Json = string | number | boolean | null | { [key: string]: Json } | Json[]

export interface Database {
  public: {
    Tables: {
      daily_memo: {
        Row: {
          id: number
          user_id: number
          content: string
          created_at: string | null
        } // The data expected to be returned from a "select" statement.
        Insert: {
          id: number
          user_id: number
          content: string
          created_at: string | null
        } // The data expected passed to an "insert" statement.
        Update: {
          id: number
          user_id: number
          content: string
          created_at: string | null
        } // The data expected passed to an "update" statement.
      }
    }
  }
}
