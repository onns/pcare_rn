import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { Dimensions, StyleSheet, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import FastImage from "react-native-fast-image"

import { Text } from "../components/StyledText"
import { formatDateFromString } from "../lib/DateUtil"
import { AccountStore } from "../stores/AccountStore"
import { Prescription } from "../stores/PrescriptionStore"
import { color } from "../theme"
import { BackgroundImage } from "./BackgroundImage"

const PHOTO_FRAME: ViewStyle = {
  flexDirection: "column",
  height: 172.5,
  backgroundColor: "rgb(153, 153, 153)",
  borderRadius: 10,
}

const REJECT_VIEW: ViewStyle = {
  height: 32,
  backgroundColor: "rgba(0, 0, 0, 0.7)",
  paddingTop: 8,
  marginTop: 30,
}

const REJECT_TEXT: TextStyle = {
  textAlign: "center",
  fontSize: 15,
  color: "#fff",
  lineHeight: 17,
}

const TITLE: TextStyle = {
  fontSize: 17,
  fontWeight: "bold",
  letterSpacing: -0.34,
  color: "#000",
}

const SUB_TITLE: TextStyle = {
  fontSize: 13,
  fontWeight: "bold",
  color: color.palette.brownGrey,
}

const TITLE_REJECT: TextStyle = {
  fontSize: 17,
  fontWeight: "bold",
  letterSpacing: -0.34,
  color: color.palette.orange,
}

export interface Props {
  index: number
  item: Prescription
  imageUrl: string | null
  // uploadDate: Date;
  status: string
  onPress: any
  accountStore: AccountStore
}

@inject("accountStore")
@observer
export default class PendingPrescItem extends Component<Props> {
  render() {
    const item = this.props.item

    // [('UL', 'Upload'), ('IP', 'Input'), ('RV', 'Review'), ('CF', 'Confirm'), ('RJ', 'Reject')]

    return (
      <TouchableOpacity
        // {...this.props}
        style={styles.pendingPrescItem}
        onPress={() => this.props.onPress(this.props.item, this.props.index)}
      >
        <View style={PHOTO_FRAME}>
          <BackgroundImage
            resizeMode={FastImage.resizeMode.contain}
            image={styles.cover}
            source={{ uri: this.props.imageUrl }}
          />
          {this.props.status == "RJ" ? (
            <View>
              <FastImage
                source={require("../assets/images/warning_mark.png")}
                style={{ width: 40.3, height: 41 }}
              />
              <View style={REJECT_VIEW}>
                <Text style={REJECT_TEXT}>반려사유보기</Text>
                {/* <Image 
                    style={{ width: 8, height: 13 }}
                    source={require("../assets/images/right-arrow.png")}/> */}
              </View>
            </View>
          ) : null}
        </View>
        <View style={{ padding: 10 }}>
          {this.props.status == "RJ" ? (
            <Text style={TITLE_REJECT}>반려</Text>
          ) : (
            <Text style={TITLE}>처리중</Text>
          )}
          <Text numberOfLines={1} ellipsizeMode="tail" style={SUB_TITLE}>
            {formatDateFromString(this.props.item.issue_date)}
            {this.renderMemberName()}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  renderMemberName(): string {
    const user = this.props.accountStore.user
    const item = this.props.item
    let name
    if (item.children) {
      name = this.findFamilyMemberName(item.children, false)
    } else if (user.id == item.user) {
      name = user.nickname
    } else {
      name = this.findFamilyMemberName(item.user, true)
    }
    return " / " + name
  }

  findFamilyMemberName(memberId: number, isOver14: boolean): string {
    const family = this.props.accountStore.family
    if (isOver14) {
      for (let i = 0; i < family.length; i++) {
        const member = family[i]
        if (member.family_user == memberId) {
          return member.name
        }
      }
    } else {
      for (let i = 0; i < family.length; i++) {
        const member = family[i]
        if (member.id == memberId) {
          return member.name
        }
      }
    }
    return ""
  }
}

const styles = StyleSheet.create({
  pendingPrescItem: {
    width: (Dimensions.get("window").width - 40) / 2, //172.5,
    height: 228, //235 - 53
    marginHorizontal: 5,
    marginTop: 10,
  },
  thumbnail: {},
  upload_date: {},
  status: {},
  cover: {
    maxWidth: 113.7,
    maxHeight: 151.2,
    paddingVertical: 10,
  },
  info1: {
    flexDirection: "row",
  },
  info2: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  info: {
    flex: 3,
    alignItems: "flex-end",
    flexDirection: "column",
    alignSelf: "center",
    padding: 20,
  },
  issue_date: {
    fontSize: 18,
  },
  disease: {
    fontSize: 16,
    fontWeight: "bold",
  },
  hospital: {
    fontSize: 14,
  },
})
