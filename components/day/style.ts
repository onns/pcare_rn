import { Platform, StyleSheet } from "react-native"

import { color } from "../../theme"
import * as defaultStyle from "../../theme/calendar"

const STYLESHEET_ID = "stylesheet.day.period"
const FILLER_HEIGHT = 32

export default function styleConstructor(theme = {}) {
  const appStyle = { ...defaultStyle, ...theme }
  return StyleSheet.create({
    wrapper: {
      alignItems: "center",
      alignSelf: "stretch",
      // marginLeft: -1,
    },
    base: {
      //borderWidth: 1,
      width: 32,
      height: FILLER_HEIGHT,
      alignItems: "center",
      marginLeft: 9,
      // paddingLeft: Platform.OS === "android" ? 0 : 1,
    },
    fillers: {
      position: "absolute",
      height: FILLER_HEIGHT,
      flexDirection: "row",
      left: 0,
      right: 0,
    },
    leftFiller: {
      height: FILLER_HEIGHT,
      flex: 1,
    },
    rightFiller: {
      height: FILLER_HEIGHT,
      flex: 1,
    },
    text: {
      marginTop: Platform.OS === "android" ? 4 : 5,
      fontSize: appStyle.textDayFontSize,
      fontFamily: appStyle.textDayFontFamily,
      color: color.text,
      backgroundColor: "rgba(255, 255, 255, 0)",
    },
    today: {
      borderWidth: 1,
      borderColor: color.primary,
      borderRadius: 16,
      backgroundColor: appStyle.todayBackgroundColor,
    },
    todayText: {
      // color: theme.todayTextColor || appStyle.dayTextColor,
      color: color.primary,
    },
    disabledText: {
      color: "#33302f80",
    },
    dot: {
      // width: 42,
      minHeight: 24,
      marginVertical: 1,
      // borderRadius: 2,
      opacity: 0,
    },
    visibleDot: {
      opacity: 1,
      backgroundColor: appStyle.dotColor,
    },
    selectedDot: {
      backgroundColor: appStyle.selectedDotColor,
    },
    disabledDot: {
      backgroundColor: appStyle.disabledDotColor || appStyle.dotColor,
    },
    todayDot: {
      backgroundColor: appStyle.todayDotColor || appStyle.dotColor,
    },
    quickAction: {
      backgroundColor: "white",
      borderWidth: 1,
      borderColor: "#c1e4fe",
    },
    quickActionText: {
      marginTop: 6,
      color: appStyle.textColor,
    },
    firstQuickAction: {
      backgroundColor: appStyle.textLinkColor,
    },
    firstQuickActionText: {
      color: "white",
    },
    naText: {
      color: "#b6c1cd",
    },
    selected: {
      backgroundColor: color.palette.greenishTeal,
      borderRadius: 16,
    },
    selectedText: {
      color: appStyle.selectedDayTextColor,
    },
    unTaken: {
      backgroundColor: "#E5FAF2",
      borderRadius: 16,
    },
    unTakenText: {
      color: color.palette.black,
    },
    // ...(theme[STYLESHEET_ID] || {}),
    // ...(appStyle[STYLESHEET_ID] || {}),
  })
}
