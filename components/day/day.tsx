import React, { Component } from "react"
import { TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { CalendarTheme, DateObject, Marking, MultiPeriodMarking } from "react-native-calendars"

import { color, typography } from "../../theme"
import * as defaultStyle from "../../theme/calendar"
import { Text } from "../StyledText"
import styleConstructor from "./style"

export interface DayProps {
  state: "" | "selected" | "disabled" | "today"
  theme?: CalendarTheme
  marking: false | Marking[]
  onPress: (date: DateObject) => any
  onLongPress: (date: DateObject) => any
  date: DateObject
  dayOfWeek: number
  children: any
}

const _ = require("lodash")

function shouldUpdate(a, b, paths) {
  for (let i = 0; i < paths.length; i++) {
    const equals = _.isEqual(_.get(a, paths[i]), _.get(b, paths[i]))
    if (!equals) {
      return true
    }
  }
  return false
}

class Day extends Component<DayProps> {
  static displayName = "IGNORE"

  theme = { ...defaultStyle }
  style = styleConstructor(this.props.theme)

  constructor(props: DayProps) {
    super(props)

    this.onDayPress = this.onDayPress.bind(this)
    this.onDayLongPress = this.onDayLongPress.bind(this)
  }

  onDayPress() {
    this.props.onPress(this.props.date)
  }

  onDayLongPress() {
    this.props.onLongPress(this.props.date)
  }

  shouldComponentUpdate(nextProps) {
    return shouldUpdate(this.props, nextProps, [
      "state",
      "children",
      "marking",
      "onPress",
      "onLongPress",
    ])
  }

  renderPeriods(marking: MultiPeriodMarking) {
    const isDisabled =
      typeof marking.disabled !== "undefined" ? marking.disabled : this.props.state === "disabled"

    const baseDotStyle = [this.style.dot, this.style.visibleDot]
    if (marking.periods && Array.isArray(marking.periods) && marking.periods.length > 0) {
      // Filter out dots so that we we process only those items which have key and color property
      const validPeriods = marking.periods.filter((d) => d && d.color)

      return validPeriods.map((period, index) => {
        const style = [
          ...baseDotStyle,
          {
            marginTop: 8,
            backgroundColor: "transparent",
          },
        ]
        if (period.startingDay) {
          style.push({
            borderTopLeftRadius: 1,
            borderBottomLeftRadius: 1,
            marginLeft: 4,
          })
        }
        if (period.endingDay) {
          style.push({
            borderTopRightRadius: 1,
            borderBottomRightRadius: 1,
            marginRight: 4,
          })
        }

        // TODO: 다음 주 일요일에 질병명 표시

        return (
          <View key={index} style={style}>
            <View
              style={{
                height: 6,
                backgroundColor: isDisabled ? `${period.color}32` : `${period.color}`,
              }}
            />
            {(period.startingDay ||
              this.props.dayOfWeek == 0 ||
              (this.props.children == 1 && this.props.dayOfWeek != 0)) && (
              <View
                style={{
                  position:
                    (period.startingDay && this.props.dayOfWeek != 6) ||
                    this.props.dayOfWeek == 0 ||
                    (this.props.children == 1 && this.props.dayOfWeek != 0)
                      ? "absolute"
                      : "relative",
                }}
              >
                <Text
                  style={{
                    position:
                      (period.startingDay && this.props.dayOfWeek != 6) ||
                      this.props.dayOfWeek == 0 ||
                      (this.props.children == 1 && this.props.dayOfWeek != 0)
                        ? "absolute"
                        : "relative",
                    fontFamily: typography.primary,
                    fontSize: 13,
                    lineHeight: 19,
                    color: color.text,
                    marginLeft: 4,
                    marginTop: 6,
                    marginBottom: 0,
                    width: 32 * (7 - this.props.dayOfWeek),
                  }}
                  allowFontScaling={false}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  {period.diseaseName ? period.diseaseName : period.issueDate}
                </Text>
              </View>
            )}
          </View>
        )
      })
    }
    return
  }

  render() {
    const containerStyle: Array<ViewStyle> = [this.style.base]
    const textStyle: Array<TextStyle> = [this.style.text]
    const marking: MultiPeriodMarking = this.props.marking || {}
    const periods = this.renderPeriods(marking)
    const isDisabled =
      marking.disabled !== "undefined" ? marking.disabled : this.props.state === "disabled"

    if (marking.selected) {
      containerStyle.push(this.style.selected)
      textStyle.push(this.style.selectedText)
    } else if (isDisabled) {
      textStyle.push(this.style.disabledText)
    } else if (this.props.state === "today") {
      containerStyle.push(this.style.today)
      textStyle.push(this.style.todayText)
    }

    if (marking.unTaken) {
      containerStyle.push(this.style.unTaken)
      textStyle.push(this.style.unTakenText)
    }

    return (
      <View style={{ alignSelf: "stretch" }}>
        <TouchableOpacity
          style={containerStyle}
          onPress={this.onDayPress}
          onLongPress={this.onDayLongPress}
        >
          <Text allowFontScaling={false} style={textStyle}>
            {String(this.props.children)}
          </Text>
        </TouchableOpacity>
        <View style={{ alignSelf: "stretch" }}>{periods}</View>
      </View>
    )
  }
}

export default Day
