import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { useObserver } from "mobx-react"
import { onAction } from "mobx-state-tree"
import React, { useEffect, useState } from "react"
import { TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import Collapsible from "react-native-collapsible"
import ReactNativeHapticFeedback from "react-native-haptic-feedback"

import { sendLogEvent } from "@lib/analytics"
import firebase from "@react-native-firebase/app"
import remoteConfig from "@react-native-firebase/remote-config"
import { useNavigation } from "@react-navigation/native"
import { HomeNavigationProp } from "@screens/HomeScreen"

import { useStores } from "../App"
import SvgArrowDown from "../assets/images/arrowDown.svg"
import SvgDisease from "../assets/images/ic_pictogram24px_disease.svg"
import SvgLight from "../assets/images/ic_pictogram24px_light.svg"
import SvgPrescription from "../assets/images/ic_pictogram24px_onlinemedi.svg"
import SvgOptions from "../assets/images/ic_ui24px_caret_line_right.svg"
import { Text } from "../components/StyledText"
import { DrugToTake } from "../stores/drug-to-take/DrugToTake"
import { DrugToTakeGroup } from "../stores/drug-to-take/DrugToTakeGroup"
import { color, typography } from "../theme"
import TouchableItem from "./button/TouchableItem"
import { TakingItem } from "./TakingItem"

export interface TakingPrescriptionProps {
  drugToTakeGroup: DrugToTakeGroup
  index: number
  listLength: number
  onPress: () => void
  onTakePress: (
    drugToTake: DrugToTake,
    takeAllPressed: boolean,
    allTaken: boolean,
    type?: "once" | undefined,
  ) => void
  type?: "once" | undefined
  date: any
}

// ----------------------------------------------------------------
// 아이템 컨테이너

const TAKING_ITEM_CONTAINER: ViewStyle = {
  // borderRadius: 24,
  // borderBottomWidth: 1,
  // borderColor: color.borders.neutralSubded,
  // shadowColor: color.surface.neutralSubded,
  // shadowOffset: {
  //   width: 0,
  //   height: 12,
  // },
  // shadowOpacity: 1, //0.3
  // shadowRadius: 0, //10
  backgroundColor: color.palette.white,
}
const END_ITEM_CONTAINER: ViewStyle = {
  ...TAKING_ITEM_CONTAINER,
}

// ----------------------------------------------------------------
// 알람 박스 영역

const ALARM_GUIDE_BOX_CONTAINER: ViewStyle = {
  paddingHorizontal: 16,
  paddingVertical: 16,
}
const ALARM_GUIDE_BOX: ViewStyle = {
  flex: 1,
  borderRadius: 16,
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: 16,
  paddingVertical: 12,
  backgroundColor: color.surface.neutralDefault,
}
const ALARM_GUIDE_TEXT: TextStyle = {
  ...typography.ui.default,
  flex: 1,
}
const ALARM_GUIDE_TEXT_ACCENT: TextStyle = {
  ...typography.ui.defaultEmphasized,
  color: color.textColor.primary,
}
const CHANGE_ALARM_TIME_TEXT: TextStyle = {
  ...typography.buttons.small,
  color: color.textColor.action,
}
const CHANGE_ALARM_TIME: ViewStyle = {
  // borderWidth: 1,
  borderColor: color.palette.salmonGray,
  borderRadius: 16,
  paddingHorizontal: 8,
  paddingVertical: 4,
}

// ----------------------------------------------------------------
// 아이템 영역

const PRESCRIPTION_ITEM_CONTAINER: ViewStyle = {
  flex: 1,
  paddingBottom: 8,
}

// ----------------------------------------------------------------
// 처방전 헤더 영역

const PRESCRIPTION_HEADER: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: 16,
  paddingTop: 24,
}
const PRESCRIPTION_TITLE_CONTAINER: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  alignItems: "center",
  paddingRight: 16,
  // paddingBottom: 8,
}
const PRESCRIPTION_TITLE_LEFTAREA: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const PRESCRIPTION_TITLE_TEXT: TextStyle = { ...typography.ui.default }
const ROTATE_180 = { transform: [{ rotate: "180deg" }] }
const SVG_ICON: ViewStyle = {
  marginRight: 8,
}

// ----------------------------------------------------------------
// 카드 줄였을 때 디스크립션 영역

const DESCRIPTION_CONTAINER: ViewStyle = {
  // borderWidth: 1,
  borderColor: color.palette.salmonGray,
  borderRadius: 16,
  paddingLeft: 48,
  paddingVertical: 8,
}
const DESCRIPTION_TEXT: TextStyle = {
  ...typography.captions.default,
  color: color.textColor.subdued,
}
// const DISEASE_TITLE: ViewStyle = { flex: 1, marginHorizontal: 16 }
// const DISEASE_AND_DRUG_TITLE: ViewStyle = { ...DISEASE_TITLE /* marginBottom: 0 */ }
// const { width } = Dimensions.get("window")

// ----------------------------------------------------------------
// 모두복약 영역

const TAKE_ALL_BUTTON: ViewStyle = {
  flex: 1,
  height: 48,
  marginBottom: 8,
  // borderRadius: 45,
  // borderTopWidth: 0.5,
  // borderColor: color.borders.dividerBetweenUI,
  justifyContent: "center",
  alignItems: "center",
}

const TAKE_ALL_BUTTON_TEXT: TextStyle = {
  ...typography.buttons.default,
  color: color.textColor.primary,
}
const TAKEN_ALL_BUTTON_TEXT: TextStyle = {
  ...TAKE_ALL_BUTTON_TEXT,
  color: color.textColor.disabled,
}
const DEACTIVATE: ViewStyle = {
  flex: 1,
  height: 44,
  borderRadius: 45,
  borderColor: "#EBEAEA",
  paddingHorizontal: 16,
  justifyContent: "center",
  alignItems: "center",
  marginHorizontal: 40,
}
const DEACTIVATE_TEXT: TextStyle = {
  ...typography.buttons.default,
  color: color.palette.grey30,
}

const GUIDE_MESSAGE_BAR: ViewStyle = {
  // backgroundColor: color.surface.neutralSubded,
  borderRadius: 100,
  alignItems: "center",
  paddingHorizontal: 16,
  paddingVertical: 4,
  marginHorizontal: 12,
  flexDirection: "row",
  borderWidth: 1,
  borderColor: color.borders.neutralSubded,
}
const GUIDE_SYMBOL_BOX: ViewStyle = {
  width: 18, //64,
  height: 18, //42,
  // borderRadius: 4,
  // backgroundColor: color.surface.neutralSubded,
  alignItems: "center",
  justifyContent: "center",
}

const GUIDE_MESSAGE_TEXT: TextStyle = {
  ...typography.ui.default,
  color: color.textColor.default,
}
const GUIDE_MESSAGE_TEXT_BOX: ViewStyle = {
  paddingLeft: 10,
  paddingRight: 8,
  flex: 1,
}
const VIEW_TIP_BUTTON: ViewStyle = {
  // minWidth: 56,
  // height: 40,
  // borderRadius: 8,
  // borderColor: color.actionPrimary.default,
  // borderColor: color.surface.neutralSubded,
  // backgroundColor: color.surface.successDefault,
  paddingRight: 4,
  paddingVertical: 12,
  alignItems: "center",
  justifyContent: "center",
}
const VIEW_TIP_BUTTON_TEXT: TextStyle = {
  ...typography.buttons.default,
  color: color.textColor.action,
}

const ROW_VERT_CENTER: ViewStyle = { flexDirection: "row", alignItems: "center", flex: 1 }
const hitSlop10 = { top: 10, left: 10, right: 10, bottom: 10 }
const checkButton = require("../assets/lottie-files/checkButton.json")

/**
 * 현 instance 에 속한 약제들의 상태에 따라 "모두 복약" 버튼의 초기 상태를 결정하기 위해 사용.
 * 속한 약제들 모두 복용한 상태이면 allTaken 을 true 로 설정
 */
function useAllTaken(drugToTakeGroup: DrugToTakeGroup) {
  const { drugsToTake } = drugToTakeGroup
  // useObserver를 사용해서 리턴하는 값의 업데이트를 계속 반영한다
  return useObserver(() => {
    let allTaken = true
    if (drugsToTake) {
      for (let i = 0; i < drugsToTake.length; i++) {
        const drugToTake = drugsToTake[i]
        if (drugToTake.status === "ready") {
          allTaken = false
        }
      }
    } else {
      allTaken = false
    }
    return { allTaken: allTaken }
  })
}

export const TakingPrescription: React.FunctionComponent<TakingPrescriptionProps> = (props) => {
  const { drugToTakeGroup, index, listLength, onPress, onTakePress, type, date } = props
  const { takingDrugStore, accountStore } = useStores()
  const { allTaken } = useAllTaken(drugToTakeGroup)
  const [takePressed, setTakePressed] = useState(allTaken)
  const [processing, setProcessing] = useState(false)
  const [hiddenButtonName, setHiddenButtonName] = useState(allTaken)
  const [collapsed, setCollapsed] = useState(allTaken)
  // const animationRef = useRef(null)
  const navigation = useNavigation<HomeNavigationProp>()
  const { drugsOnce, drugsToTake } = takingDrugStore
  const issueDate = drugToTakeGroup.drugsToTake[0].prescription.setIssueDateObject()
  const currentDate = DateTime.local()
  const pickedDate = date

  /**
   * 마지막 복약 체크되지 않은 약제까지 복약 체크하려는 경우, '모두 복약' 버튼을 체크(takePressed) 상태로 바꾼다.
   */
  useEffect(() => {
    const actionDisposer = onAction(takingDrugStore, (call) => {
      if (call.name == "setStatus" && call.args) {
        const drugsToTakeIdx = Number(call.path.substring(call.path.lastIndexOf("/") + 1))
        const prescriptionId = drugToTakeGroup.id.substring(0, drugToTakeGroup.id.length - 5)
        const pidAndWhen = drugToTakeGroup.id
        const actionDrugToTake = drugsToTake[drugsToTakeIdx]

        if (actionDrugToTake && actionDrugToTake.prescription.id == prescriptionId) {
          if (call.args[0] === "taken") {
            const rxGroup = drugToTakeGroup.drugsToTake
            let allTakenPressedInRxGroup = true
            for (let i = 0; i < rxGroup.length; i++) {
              const drugToTake = rxGroup[i]
              if (drugToTake.status === "ready" && actionDrugToTake.id != drugToTake.id) {
                allTakenPressedInRxGroup = false
              }
            }

            const onceGroup = drugsOnce.get(pidAndWhen)
            let allTakenPressedInOnceGroup = true
            if (onceGroup) {
              for (let i = 0; i < onceGroup.length; i++) {
                const drugToTake = onceGroup[i]
                if (drugToTake.status === "ready" && actionDrugToTake.id != drugToTake.id) {
                  allTakenPressedInOnceGroup = false
                }
              }
            }

            if (
              (allTakenPressedInRxGroup && !type) ||
              (allTakenPressedInOnceGroup && type === "once")
            ) {
              setTakePressed(true)
              setHiddenButtonName(true)
            }
          }
        }
      }
    })
    return () => actionDisposer()
  }, [takePressed])

  const onTakePressInternal = () => {
    setProcessing(true)
    if (!takePressed) {
      setHiddenButtonName(true)
      // animationRef.current.play()
    } else {
      // animationRef.current.reset()
      setHiddenButtonName(false)
    }
    const pid = drugToTakeGroup.id.substring(0, drugToTakeGroup.id.length - 5)
    if (!takePressed) {
      firebase.analytics().logEvent("press_take_drug_all", { prescription_id: pid })
      amplitude.getInstance().logEvent("press_take_drug_all", { prescription_id: pid })
    } else {
      firebase.analytics().logEvent("press_take_drug_all_cancel", { prescription_id: pid })
      amplitude.getInstance().logEvent("press_take_drug_all_cancel", { prescription_id: pid })
    }

    const options = {
      enableVibrateFallback: true,
      ignoreAndroidSystemSettings: true,
    }
    ReactNativeHapticFeedback.trigger("impactLight", options)

    setTimeout(() => {
      if (!(takePressed || collapsed)) {
        setCollapsed(true)
      } else {
        setCollapsed(false)
      }
      setTakePressed(!takePressed)
      // 하나의 질병안에 2개 이상의 약이 포함되어 있을 경우 반복문으로 하나씩 상태를 업데이트
      if (drugToTakeGroup.drugsToTake.length > 1) {
        drugToTakeGroup.drugsToTake.map((item) => onTakePress(item, true, !takePressed, type))
      } else {
        onTakePress(drugToTakeGroup.drugsToTake[0], true, !takePressed, type)
      }
      setProcessing(false)
    }, 200)
  }

  const renderTakingItem = (drugToTake, index) => {
    return (
      <TakingItem
        key={drugToTake.id}
        drugToTake={drugToTake}
        banners={drugToTake.prescription_detail?.banners}
        index={index}
        listLength={listLength}
        onPress={() => null}
        onTakePress={() => {
          if (drugToTake.status === "taken") {
            // 복약 취소한 경우(아직 status 에 반영되지 않은 상태임)
            setTakePressed(false)
            setHiddenButtonName(false)
          }
          onTakePress(drugToTake, false, false)
        }}
        date={date}
      />
    )
  }

  const renderContent = () => (
    <View>
      {/* {api.apisauce.headers.Authorization &&
      takingDrugStore.drugsToTake.length > 0 &&
      type !== "once" ? (
        <View style={ALARM_GUIDE_BOX_CONTAINER}>
          <View style={ALARM_GUIDE_BOX}>
            <Text style={ALARM_GUIDE_TEXT}>
              <Text style={ALARM_GUIDE_TEXT_ACCENT}>
                {formatTime(drugToTakeGroup.id.slice(-5))}
              </Text>{" "}
              에 복약하세요.
            </Text>
            <TouchableOpacity
              style={CHANGE_ALARM_TIME}
              onPress={() =>
                navigation.navigate("Details", {
                  screen: "TimeSlotSettings",
                })
              }
            >
              <Text style={CHANGE_ALARM_TIME_TEXT}>시간변경</Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : null} */}

      <View
        // style={collapsed ? DISEASE_AND_DRUG_TITLE : DISEASE_TITLE}
        style={PRESCRIPTION_ITEM_CONTAINER}
      >
        <View
          // style={ type === "once" ? PRESCRIPTION_ONCE_BUTTON : PRESCRIPTION_BUTTON}
          style={PRESCRIPTION_HEADER}
        >
          <TouchableOpacity style={PRESCRIPTION_TITLE_CONTAINER} onPress={onPress}>
            {/*  주질환, 부질환 1개 이상일때 */}
            {drugToTakeGroup.drugsToTake[0].prescription.disease &&
            drugToTakeGroup.drugsToTake[0].prescription.subdiseases &&
            drugToTakeGroup.drugsToTake[0].prescription.subdiseases.length >= 1 ? (
              <View style={PRESCRIPTION_TITLE_LEFTAREA}>
                <SvgPrescription style={SVG_ICON} />
                <Text numberOfLines={1} ellipsizeMode="tail" style={PRESCRIPTION_TITLE_TEXT}>
                  {drugToTakeGroup.drugsToTake[0].prescription.disease.rep_title
                    ? `${drugToTakeGroup.drugsToTake[0].prescription.disease.rep_title} 외 ${drugToTakeGroup.drugsToTake[0].prescription.subdiseases.length}`
                    : `${drugToTakeGroup.drugsToTake[0].prescription.disease.name_kr} 외 ${drugToTakeGroup.drugsToTake[0].prescription.subdiseases.length}`}
                </Text>
                <SvgOptions />
              </View>
            ) : // 주질환, 부질환 없을때
            drugToTakeGroup.drugsToTake[0].prescription.disease &&
              drugToTakeGroup.drugsToTake[0].prescription.subdiseases === null ? (
              <View style={PRESCRIPTION_TITLE_LEFTAREA}>
                <SvgPrescription style={SVG_ICON} />
                <Text numberOfLines={1} ellipsizeMode="tail" style={PRESCRIPTION_TITLE_TEXT}>
                  {drugToTakeGroup.drugsToTake[0].prescription.disease.rep_title
                    ? drugToTakeGroup.drugsToTake[0].prescription.disease.rep_title
                    : drugToTakeGroup.drugsToTake[0].prescription.name_kr}
                </Text>
                <SvgOptions />
              </View>
            ) : // 주질환 X, 부질환 2개 이상일때
            drugToTakeGroup.drugsToTake[0].prescription.disease === null &&
              drugToTakeGroup.drugsToTake[0].prescription.subdiseases &&
              drugToTakeGroup.drugsToTake[0].prescription.subdiseases.length >= 2 ? (
              <View style={PRESCRIPTION_TITLE_LEFTAREA}>
                <SvgPrescription style={SVG_ICON} />
                <Text numberOfLines={1} ellipsizeMode="tail" style={PRESCRIPTION_TITLE_TEXT}>
                  {drugToTakeGroup.drugsToTake[0].prescription.subdiseases[0].rep_title
                    ? `${drugToTakeGroup.drugsToTake[0].prescription.subdiseases[0].rep_title} 외 ${
                        drugToTakeGroup.drugsToTake[0].prescription.subdiseases.length - 1
                      }`
                    : `${drugToTakeGroup.drugsToTake[0].prescription.subdiseases[0].name_kr} 외 ${
                        drugToTakeGroup.drugsToTake[0].prescription.subdiseases.length - 1
                      }`}
                </Text>
                <SvgOptions />
              </View>
            ) : // 주질환 X, 부질환 1개 일때
            drugToTakeGroup.drugsToTake[0].prescription.disease === null &&
              drugToTakeGroup.drugsToTake[0].prescription.subdiseases &&
              drugToTakeGroup.drugsToTake[0].prescription.subdiseases.length === 1 ? (
              <View style={PRESCRIPTION_TITLE_LEFTAREA}>
                <SvgPrescription style={SVG_ICON} />
                <Text numberOfLines={1} ellipsizeMode="tail" style={PRESCRIPTION_TITLE_TEXT}>
                  {drugToTakeGroup.drugsToTake[0].prescription.subdiseases[0].rep_title
                    ? drugToTakeGroup.drugsToTake[0].prescription.subdiseases[0].rep_title
                    : drugToTakeGroup.drugsToTake[0].prescription.subdiseases[0].name_kr}
                </Text>
                <SvgOptions />
              </View>
            ) : (
              // 주,부 질환 없을때
              <View style={PRESCRIPTION_TITLE_LEFTAREA}>
                <SvgDisease style={SVG_ICON} />
                <Text numberOfLines={1} ellipsizeMode="tail" style={PRESCRIPTION_TITLE_TEXT}>
                  {`${drugToTakeGroup.drugsToTake[0].prescription
                    .setIssueDateObject()
                    .toFormat("M월 d일")} 처방`}
                </Text>
                <SvgOptions />
              </View>
            )}
          </TouchableOpacity>
          {drugToTakeGroup.drugsToTake.length > 1 ? (
            <TouchableOpacity onPress={() => setCollapsed(!collapsed)} hitSlop={hitSlop10}>
              <SvgArrowDown width={24} height={24} style={collapsed ? null : ROTATE_180} />
            </TouchableOpacity>
          ) : null}
        </View>
        {drugToTakeGroup.drugsToTake.length > 1 ? (
          <Collapsible style={DESCRIPTION_CONTAINER} collapsed={!collapsed}>
            <Text style={DESCRIPTION_TEXT} ellipsizeMode="middle" numberOfLines={1}>
              {drugToTakeGroup.drugsToTake[0].prescription_detail.shortenedName}
              {drugToTakeGroup.drugsToTake.length > 1
                ? ` 외 ${drugToTakeGroup.drugsToTake.length - 1}종`
                : null}
            </Text>
          </Collapsible>
        ) : null}
      </View>

      {/* 복약 할 아이템 리스트 */}
      <Collapsible collapsed={collapsed} style={{ paddingBottom: 4 }}>
        {takingDrugStore.isLoading
          ? null
          : drugToTakeGroup.drugsToTake[0].alarm_name === "once"
          ? drugsOnce.get(drugToTakeGroup.id)?.drugsToTake.map(renderTakingItem)
          : drugToTakeGroup.drugsToTake.map(renderTakingItem)}
      </Collapsible>
      {collapsed ? (
        <View style={{ paddingBottom: 16 }}>
          {collapsed && drugToTakeGroup.drugsToTake.length > 1
            ? null
            : drugToTakeGroup.drugsToTake[0].alarm_name === "once"
            ? drugsOnce.get(drugToTakeGroup.id)?.drugsToTake.map(renderTakingItem)
            : drugToTakeGroup.drugsToTake.map(renderTakingItem)}
        </View>
      ) : null}

      {remoteConfig().getBoolean("disease_target_content_enabled") &&
      drugToTakeGroup.drugsToTake[0].prescription.disease?.guide_message ? (
        <View>
          <View
            style={{
              ...GUIDE_MESSAGE_BAR,
              marginBottom: collapsed ? 24 : 16,
            }}
          >
            <View style={ROW_VERT_CENTER}>
              <View style={GUIDE_SYMBOL_BOX}>
                <SvgLight width={64 * 0.6} height={40 * 0.5} />
              </View>
              <View style={GUIDE_MESSAGE_TEXT_BOX}>
                <Text style={GUIDE_MESSAGE_TEXT}>
                  {drugToTakeGroup.drugsToTake[0].prescription.disease.guide_message}
                </Text>
              </View>
            </View>
            <TouchableItem
              style={VIEW_TIP_BUTTON}
              pressColor={color.palette.foreverGreen}
              onPress={() => {
                navigation.navigate("Details", {
                  screen: "DiseaseSummary",
                  params: {
                    id: drugToTakeGroup.drugsToTake[0].prescription.disease.id,
                    code: drugToTakeGroup.drugsToTake[0].prescription.disease.code,
                    guideTabVisible: true,
                    tabIndex: 1,
                  },
                })
                sendLogEvent("press_disease_guide_content", {
                  guide_disease: drugToTakeGroup.drugsToTake[0].prescription.disease.code,
                  guide_content_title:
                    drugToTakeGroup.drugsToTake[0].prescription.disease.guide_message,
                })
              }}
            >
              <Text style={VIEW_TIP_BUTTON_TEXT}>꿀팁보기</Text>
            </TouchableItem>
          </View>
        </View>
      ) : (
        <View style={{ height: 11 }} />
      )}

      {/* 모두 복약 버튼 */}
      {!collapsed ? (
        <View>
          {drugToTakeGroup.drugsToTake.length > 1 ? (
            <View>
              <TouchableOpacity
                style={[
                  pickedDate
                    ? issueDate < pickedDate && pickedDate < currentDate
                      ? TAKE_ALL_BUTTON
                      : DEACTIVATE
                    : TAKE_ALL_BUTTON, // HomeScreen 에서 사용할 때와 MedicationScheduleDetailScreen(date prop 활용) 에서 사용할 때를 구분
                  {
                    // borderWidth: !hiddenButtonName ? 1 : 0,
                    backgroundColor: !hiddenButtonName
                      ? color.surface.uiDefault
                      : color.surface.uiDefault,
                  },
                ]}
                hitSlop={hitSlop10}
                onPress={() => onTakePressInternal()}
                disabled={
                  pickedDate
                    ? issueDate < pickedDate && pickedDate < currentDate
                      ? processing
                      : !processing
                    : processing
                }
              >
                {/* <LottieView
              ref={animationRef}
              resizeMode={"center"}
              key={1}
              source={checkButton}
              speed={2}
              loop={false}
              duration={300}
              progress={takePressed ? 1 : undefined}
            /> */}
                <Text style={!hiddenButtonName ? TAKE_ALL_BUTTON_TEXT : TAKEN_ALL_BUTTON_TEXT}>
                  {!hiddenButtonName ? "모두 복약" : "👍🏻 모두 먹음"}
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      ) : null}
    </View>
  )

  return (
    <View style={index === listLength - 1 ? END_ITEM_CONTAINER : TAKING_ITEM_CONTAINER}>
      {renderContent()}
    </View>
  )
}
