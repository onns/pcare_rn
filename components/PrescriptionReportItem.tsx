import { DateTime } from "luxon"
import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { Dimensions, TextStyle, TouchableHighlight, View, ViewStyle } from "react-native"

import { Slider } from "@miblanchard/react-native-slider"

import SvgInfoDark from "../assets/images/infoDark.svg"
import SvgPrescription from "../assets/images/prescription.svg"
import SvgProcessing from "../assets/images/processing.svg"
import SvgRejected from "../assets/images/rejected.svg"
import { formatDateFromString } from "../lib/DateUtil"
import { AccountStore } from "../stores/AccountStore"
import { Prescription, PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"
import { Text } from "./StyledText"

export interface Props {
  index: number
  prescription: Prescription
  prescriptionStore?: PrescriptionStore
  accountStore?: AccountStore
  pressItem?: () => void
  pressItemModify?: () => void
  onWithinDurNotiPeriod?: () => void
}

interface State {
  dosingDays: number
  remainingDosingDays: number
  withinDurNotiPeriod: boolean
  isSample: boolean
}

const { width } = Dimensions.get("window")
const FIRST_ITEM_CONTAINER: ViewStyle = {
  width: width - 32,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: color.palette.pale,
  shadowColor: "#14453b30",
  shadowOffset: {
    width: 0,
    height: 5,
  },
  shadowOpacity: 0.3,
  shadowRadius: 10,
  backgroundColor: color.palette.white,
  marginHorizontal: 10,
  marginTop: 10,
}

const ITEM_CONTAINER: ViewStyle = {
  width: width - 32,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: color.palette.pale,
  shadowColor: "#14453b30",
  shadowOffset: {
    width: 0,
    height: 5,
  },
  shadowOpacity: 0.3,
  shadowRadius: 10,
  backgroundColor: color.palette.white,
  marginHorizontal: 10,
  marginTop: 10,
}

const ISSUE_DATE: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
}
const DISEASE: TextStyle = {
  ...typography.title3,
  lineHeight: 25,
  marginBottom: 12,
}
const REJECTED_TEXT: TextStyle = {
  ...typography.title3,
  color: color.primary,
}
const PENDING_TEXT: TextStyle = {
  ...typography.title3,
  opacity: 0.5,
}

const THUMBNAIL: ViewStyle = { marginTop: -7.5, marginLeft: -3, marginRight: 3 }

const ROW: ViewStyle = {
  flexDirection: "row",
}
const JUSTIFY_CENTER: ViewStyle = {
  justifyContent: "flex-end",
  alignItems: "flex-end",
}
const CHECK_DATE: ViewStyle = {
  height: 32,
  flexDirection: "row",
  backgroundColor: "rgb(255,243,237)",
  alignItems: "center",
  marginTop: 15,
  borderRadius: 4,
}
const CHECK_DATE_ICON: ViewStyle = {
  marginLeft: 15,
}

const DISEASE_NAME_BOX: ViewStyle = { justifyContent: "flex-start" }
const REMAIN_TO_TAKE: TextStyle = { ...typography.title2, marginRight: 8 }
const REMAINING_DAYS: TextStyle = { fontSize: 11, color: color.palette.grey50, marginRight: 8 }
const SLIDER_TRACK: ViewStyle = { width: "100%", height: 4, backgroundColor: color.palette.pale }
const SLIDER_PRESCRIPTION: ViewStyle = { width: "70%", height: 8, marginBottom: 8 }
const END_TEXT_VIEW: ViewStyle = { height: 45, justifyContent: "flex-end" }
const END_TEXT: TextStyle = {
  ...typography.sub2,
  opacity: 0.3,
  marginRight: 8,
  textAlign: "center",
  lineHeight: 25,
  fontSize: 15,
}
const VISIT_DATE: ViewStyle = { ...typography.sub2, marginLeft: 5 }

const EDIT_BUTTON: ViewStyle = {
  alignSelf: "flex-end",
  backgroundColor: "#f8f8f8",
  borderTopRightRadius: 7,
  borderBottomLeftRadius: 16,
  width: 58,
  height: 28,
  marginTop: -16,
  marginRight: -16,
}

const EDIT_BUTTON_TEXT: TextStyle = {
  ...typography.sub1,
  textAlign: "center",
  lineHeight: 28,
  fontSize: 12,
  color: color.palette.grey50,
}

@inject("accountStore", "prescriptionStore")
@observer
export default class PrescriptionReportItem extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
  }

  renderContent() {
    const { prescription, pressItemModify } = this.props
    const { start_at, end_at } = prescription

    const startDate = DateTime.fromISO(prescription.start_at)
    const endDate = DateTime.fromISO(prescription.end_at)
    const today = prescription.todayObject
    const dosingDays = endDate.diff(startDate, "days").days + 1
    // 두 날짜 차이 구하기
    const diffDays = today.diff(startDate, "days").days + 1
    const remainingDosingDays = dosingDays - diffDays

    return (
      <View
        style={{
          padding: 16,
        }}
      >
        <TouchableHighlight
          style={EDIT_BUTTON}
          onPress={() => pressItemModify()}
          underlayColor="#fff"
        >
          <Text style={EDIT_BUTTON_TEXT}>수정</Text>
        </TouchableHighlight>
        <View style={ROW}>
          {prescription.status === "RJ" ? (
            <SvgRejected width={40} height={40} style={THUMBNAIL} />
          ) : prescription.status === "UL" || prescription.status === "IP" ? (
            <SvgProcessing width={40} height={40} style={THUMBNAIL} />
          ) : (
            <SvgPrescription width={40} height={40} style={[THUMBNAIL, { borderRadius: 1 }]} />
          )}
          <View style={{ flex: 1 }}>
            <View style={DISEASE_NAME_BOX}>
              {prescription.status === "RJ" ? (
                <Text style={REJECTED_TEXT}>처방전 반려됨</Text>
              ) : prescription.status === "UL" || prescription.status === "IP" ? (
                <Text style={PENDING_TEXT}>처방전 처리중{prescription.pendingNumber}</Text>
              ) : prescription.disease.length + prescription.subdiseases.length === 0 ? (
                <Text numberOfLines={1} style={DISEASE}>
                  질병명 없음
                </Text>
              ) : prescription.disease.length === 0 && prescription.subdiseases.length >= 1 ? (
                <Text numberOfLines={1} style={DISEASE}>
                  {prescription.subdiseases.length >= 2 && prescription.subdiseases[0].one_liner
                    ? prescription.subdiseases[0].one_liner +
                      " 외 " +
                      (prescription.subdiseases.length - 1)
                    : prescription.subdiseases[0].name_kr}
                </Text>
              ) : prescription.disease.length + prescription.subdiseases.length > 1 ? (
                <Text numberOfLines={1} style={DISEASE}>
                  {(prescription.disease[0].one_liner.length > 1
                    ? prescription.disease[0].one_liner
                    : prescription.disease[0].name_kr) +
                    " 외 " +
                    (prescription.disease.length + prescription.subdiseases.length - 1)}
                </Text>
              ) : (
                <Text numberOfLines={1} style={DISEASE}>
                  {prescription.disease[0] !== undefined
                    ? prescription.disease[0].one_liner.length > 1
                      ? prescription.disease[0].one_liner
                      : prescription.disease[0].name_kr
                    : ""}
                </Text>
              )}
            </View>
            {remainingDosingDays <= 0 ? (
              <View style={{ marginBottom: 0 }}></View>
            ) : (
              <View>
                <Slider
                  disabled
                  containerStyle={SLIDER_PRESCRIPTION}
                  value={diffDays / dosingDays}
                  thumbStyle={{ width: 0 }}
                  trackStyle={SLIDER_TRACK}
                  minimumTrackTintColor={color.palette.greenishTeal}
                />
              </View>
            )}
            {prescription.status === "CF" ? (
              <View>
                <Text style={ISSUE_DATE}>{`${formatDateFromString(
                  start_at,
                )} ~ ${formatDateFromString(end_at)}`}</Text>
              </View>
            ) : null}
          </View>
          <View style={JUSTIFY_CENTER}>
            {remainingDosingDays <= 0 ? null : <Text style={REMAINING_DAYS}>남은기간</Text>}
            {remainingDosingDays <= 0 ? (
              <View style={END_TEXT_VIEW}>
                <Text style={END_TEXT}>복약종료</Text>
              </View>
            ) : (
              <Text
                style={[
                  REMAIN_TO_TAKE,
                  remainingDosingDays <= 2 ? { color: color.palette.orange } : null,
                ]}
              >
                {remainingDosingDays}일
              </Text>
            )}
          </View>
        </View>
        {remainingDosingDays <= 0 ? null : remainingDosingDays <= 5 ? (
          <View style={CHECK_DATE}>
            <SvgInfoDark style={CHECK_DATE_ICON} />
            <Text style={VISIT_DATE}>다음 병원 방문일을 확인하세요.</Text>
          </View>
        ) : null}
      </View>
    )
  }

  render() {
    const { index, prescription, pressItem } = this.props
    return (
      <TouchableHighlight
        onPress={() => pressItem()}
        style={
          prescription.status === "RJ" ||
          prescription.status === "UL" ||
          prescription.status === "IP"
            ? { display: "none" }
            : index == 0
            ? FIRST_ITEM_CONTAINER
            : ITEM_CONTAINER
        }
        underlayColor="#F6F3F2"
      >
        {this.renderContent()}
      </TouchableHighlight>
    )
  }
}
