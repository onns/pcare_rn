import { Button as BasicButton } from "native-base"
import React, { Component } from "react"
import { ViewStyle } from "react-native"

export interface Props {
  dark?: boolean
  style?: ViewStyle
  numberOfLines?: number
  onPress?: () => void
  primary?: boolean
  light?: boolean
  block?: boolean
  bordered?: boolean
  rounded?: boolean
  disabled?: boolean
  transparent?: boolean
}

export class Button extends Component<Props> {
  render() {
    return (
      <BasicButton
        {...this.props}
        variant={this.props?.transparent ? "ghost" : this.props?.bordered ? "outline" : undefined}
        borderRadius={this.props?.rounded ? "full" : undefined}
        colorScheme={
          this.props?.primary
            ? "primary"
            : this.props?.dark
            ? "dark"
            : this.props?.light
            ? "light"
            : undefined
        }
        style={[
          {
            shadowOffset: { height: 0, width: 0 },
            shadowOpacity: 0,
            elevation: 0,
            borderRadius: 45,
          },
          this.props?.block
            ? {
                justifyContent: "center",
                alignSelf: "stretch",
                ...this.props.style,
              }
            : this.props.style,
        ]}
      />
    )
  }
}
