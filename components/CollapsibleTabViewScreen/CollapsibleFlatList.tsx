import React, { Component } from "react"
import { Animated, Dimensions, StyleSheet, Text, View } from "react-native"

const sampleData = new Array(100).fill(0)
const window = Dimensions.get("window")

class CollapsibleFlatList extends Component {
  constructor(props) {
    super(props)
    this.listRef = React.createRef()
  }

  renderItem = ({ item, index }) => {
    return (
      <View
        style={{
          ...styles.itemContainer,
          backgroundColor: index % 2 === 0 ? "#587498" : "#E86850",
        }}
      >
        <Text style={styles.itemText}>{index} TEST</Text>
      </View>
    )
  }

  keyExtractor = (item, index) => {
    return index.toString()
  }

  render() {
    const {
      headerHeight,
      tabBarHeight,
      tabRoute,
      isTabFocused,
      scrollY,
      onMomentumScrollBegin,
      onMomentumScrollEnd,
      onScrollEndDrag,
    } = this.props

    return (
      <View style={styles.rootContainer}>
        <Animated.FlatList
          ref={this.listRef}
          data={sampleData}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          contentContainerStyle={{
            paddingTop: headerHeight,
            minHeight: window.height + headerHeight - tabBarHeight,
          }}
          scrollEventThrottle={16}
          onScroll={
            isTabFocused
              ? Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }], {
                  useNativeDriver: true,
                })
              : null
          }
          onMomentumScrollBegin={onMomentumScrollBegin}
          onMomentumScrollEnd={onMomentumScrollEnd}
          onScrollEndDrag={onScrollEndDrag}
          bounces={false}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
  },
  itemContainer: {
    width: "100%",
    height: 400,
    justifyContent: "center",
    alignItems: "center",
  },
  itemText: {
    fontSize: 25,
    color: "#FFD800",
  },
})

export default CollapsibleFlatList
