import React, { Component } from "react"
import { Animated, Text, TouchableOpacity, View } from "react-native"
import { TabView } from "react-native-tab-view"

import CollapsibleFlatList from "./CollapsibleFlatList"
import CollapsibleHeader from "./CollapsibleHeader"

const TABBAR_HEIGHT = 48

class CollapsibleTabViewTestScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      headerHeight: 0,
      tabRoutes: [
        { key: "screen1", title: "screen1" },
        { key: "screen2", title: "screen2" },
      ],
      tabIndex: 0,
    }

    this.tabIndexRef = React.createRef(0)
    this.isListGlidingRef = React.createRef(false)
    this.listArrRef = React.createRef([])
    this.listOffsetRef = React.createRef({})

    this.scrollY = new Animated.Value(0)
  }

  componentDidMount() {
    this.scrollY.addListener(({ value }) => {})
  }

  componentWillUnmount() {
    this.scrollY.removeListener()
  }

  headerOnLayout = (event) => {
    const { height } = event.nativeEvent.layout
    this.setState({ headerHeight: height })
  }

  onTabIndexChange = (id) => {
    this.setState({ tabIndex: id })
    this.tabIndexRef.current = id
  }

  onTabPress = (idx) => {
    if (!this.isListGlidingRef.current) {
      this.setState({ tabIndex: idx })
      this.tabIndexRef.current = idx
    }
  }

  syncScrollOffset = () => {
    const focusedTabKey = this.state.tabRoutes[this.tabIndexRef.current].key

    this.listArrRef.current.forEach((item) => {
      if (item.key !== focusedTabKey) {
        if (this.scrollY._value < this.state.headerHeight && this.scrollY._value >= 0) {
          if (item.value) {
            item.value.scrollToOffset({
              offset: this.scrollY._value,
              animated: false,
            })
            this.listOffsetRef.current[item.key] = this.scrollY._value
          }
        } else if (this.scrollY._value >= this.state.headerHeight) {
          if (
            this.listOffsetRef.current[item.key] < this.state.headerHeight ||
            this.listOffsetRef.current[item.key] === null
          ) {
            if (item.value) {
              item.value.scrollToOffset({
                offset: this.state.headerHeight,
                animated: false,
              })
              this.listOffsetRef.current[item.key] = this.state.headerHeight
            }
          }
        }
      } else {
        if (item.value) {
          this.listOffsetRef.current[item.key] = this.scrollY._value
        }
      }
    })
  }

  onMomentumScrollBegin = () => {
    this.isListGlidingRef.current = true
  }

  onMomentumScrollEnd = () => {
    this.isListGlidingRef.current = false
    this.syncScrollOffset()
  }

  onScrollEndDrag = () => {
    this.syncScrollOffset()
  }

  renderTabBar = (props) => {
    const tabBarTranslateY = this.scrollY.interpolate({
      inputRange: [0, this.state.headerHeight],
      outputRange: [this.state.headerHeight, 0],
      extrapolateRight: "clamp",
    })

    return (
      <View>
        {headerHeight > 0 ? (
          <TabView
            navigationState={{ index: tabIndex, routes: tabRoutes }}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={onTabIndexChange}
          />
        ) : null}
        <Animated.View
          style={[styles.collapsibleTabBar, { transform: [{ translateY: tabBarTranslateY }] }]}
        >
          <CollapsibleHeader />
        </Animated.View>
      </View>
    )
  }
}
