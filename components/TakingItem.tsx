import "@react-native-firebase/analytics"

/* eslint-disable camelcase */
import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { observer, useObserver } from "mobx-react"
import React, { useEffect, useRef, useState } from "react"
import { Dimensions, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import FastImage from "react-native-fast-image"
import ReactNativeHapticFeedback from "react-native-haptic-feedback"
import { WebView } from "react-native-webview"

import { sendLogEvent } from "@lib/analytics"
import firebase from "@react-native-firebase/app"
import { useNavigation } from "@react-navigation/native"
import { HomeNavigationProp } from "@screens/HomeScreen"

import { useStores } from "../App"
import SvgCloseBanner from "../assets/images/closeBanner.svg"
import SvgDrugEmptyImage from "../assets/images/ic_pictogram24px_grouppill.svg"
import { Text } from "../components/StyledText"
import { DrugToTake } from "../stores/drug-to-take/DrugToTake"
import { DrugToTakeGroup } from "../stores/drug-to-take/DrugToTakeGroup"
import { PrescriptionDetailBanner } from "../stores/drug-to-take/PrescriptionDetailBanner"
import { PrescriptionDetailShort } from "../stores/drug-to-take/PrescriptionDetailShort"
import { RecordStore } from "../stores/record/record-store"
import { color, typography } from "../theme"

export interface TakingItemProps {
  recordStore: RecordStore
  drugToTake: DrugToTake
  drugToTakeGroup: DrugToTakeGroup
  banners: PrescriptionDetailBanner
  index: number
  listLength: number
  onPress: () => void
  onTakePress: () => void
  date: DateTime
}

const DESCRIPTION: ViewStyle = {
  ...typography.sub2,
  opacity: 0.5,
  marginTop: 4,
}
const DRUG_ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: 16,
  paddingVertical: 8,
}
const DRUG_NAME: TextStyle = {
  ...typography.ui.defaultEmphasized,
}
const TEXT_BOX = { flex: 1, paddingLeft: 16, paddingRight: 16 }

// ----------------------------------------------------------------
// 복약 버튼

const TAKE_BUTTON: ViewStyle = {
  minWidth: 56,
  height: 40,
  // borderWidth: 1,
  borderRadius: 8,
  // borderColor: color.actionPrimary.default,
  borderColor: color.surface.neutralSubded,
  backgroundColor: color.surface.primarySubded,
  paddingHorizontal: 12,
  paddingVertical: 6,
  alignItems: "center",
  justifyContent: "center",
}
const TAKEN_BUTTON: ViewStyle = {
  ...TAKE_BUTTON,
  backgroundColor: color.surface.neutralSubded,
}
const DEACTIVATE: ViewStyle = {
  minWidth: 56,
  height: 40,
  borderRadius: 12,
  overflow: "hidden",
  borderColor: "#EBEAEA",
  paddingHorizontal: 16,
  paddingVertical: 6,
}
const SEPARATOR: ViewStyle = {
  flex: 1,
}
// ----------------------------------------------------------------
// 리딩 이미지

const DRUG_IMAGE: ViewStyle = {
  width: 64,
  height: 42,
  borderRadius: 4,
  backgroundColor: color.surface.neutralSubded,
  // borderWidth: 1,
  borderColor: color.borders.neutralSubded,
  alignItems: "center",
  justifyContent: "center",
}
const TAKE_BUTTON_TEXT: TextStyle = {
  ...typography.buttons.default,
  color: color.textColor.primary,
}
const TAKEN_BUTTON_TEXT: TextStyle = { ...TAKE_BUTTON_TEXT, color: color.textColor.disabled }
const DEACTIVATE_TEXT = { ...typography.buttons.default, color: color.palette.grey30 }
const hitSlop10 = { top: 10, left: 10, right: 10, bottom: 10 }
const SLIDER: ViewStyle = {
  overflow: "visible", // for custom animations
  right: 28,
}
const CLOSE_BANNER: ViewStyle = {
  position: "absolute",
  right: 10,
  zIndex: 1,
}
const { width: viewportWidth } = Dimensions.get("window")
export const sliderWidth = viewportWidth
const itemHorizontalMargin = 15
const itemWidth = sliderWidth - itemHorizontalMargin
const slideHeight = 64
const SLIDE_INNER_CONTAINER: ViewStyle = {
  width: "88%",
  height: slideHeight,
  marginVertical: 5,
  marginLeft: 32,
}
const PAGINATION_DOT = {
  width: 5,
  height: 5,
  borderRadius: 4,
  marginHorizontal: -5,
  backgroundColor: color.palette.white,
}
const PAGINATION_INACTIVE_DOT = {
  width: 5,
  height: 5,
  borderRadius: 4,
  backgroundColor: `${color.dim}32`,
}
const PAGINATION_CONTAINER = {
  paddingVertical: 0,
  bottom: 16,
}

// const checkButton = require("../assets/lottie-files/checkButton.json")

// custom useStores
function useDrugToTake(id: number) {
  const { takingDrugStore } = useStores()
  const data = takingDrugStore.drugsToTake.filter((value) => value.id === id)
  // useObserver를 사용해서 리턴하는 값의 업데이트를 계속 반영한다
  return useObserver(() => ({
    status: data[0]?.status ?? "ready",
  }))
}

export const TakingItem: React.FunctionComponent<TakingItemProps> = observer((props) => {
  const { drugToTake, onTakePress, date } = props
  const { status } = useDrugToTake(drugToTake.id)
  const [processing, setProcessing] = useState(false)
  const { recordStore } = useStores()
  const [takePressed, setTakePressed] = useState(drugToTake.status === "taken")
  const [hiddenButtonName, setHiddenButtonName] = useState(drugToTake.status === "taken")
  const animationRef = useRef(null)
  const navigation = useNavigation<HomeNavigationProp>()
  const { prescription, prescription_detail } = drugToTake
  const { banners, image_url } = prescription_detail
  const issueDate = prescription.setIssueDateObject()
  const currentDate = DateTime.local()
  const pickedDate = date
  const [bannerVisible, setBannerVisible] = useState(true)
  const carouselRef = useRef(null)
  const webViewRefs = useRef([]) // Multiple references
  const [activeSlide, setActiveSlide] = useState(0)
  const [currentWebViewRef, setCurrentWebViewRef] = useState<WebView>()
  useEffect(() => {
    setTakePressed(status === "taken")
    setHiddenButtonName(status === "taken")
  }, [status]) // Only re-run the effect if status changes

  const onTakePressInternal = () => {
    setProcessing(true)
    if (!takePressed) {
      setHiddenButtonName(true)
      // animationRef.current.play()
    } else {
      // animationRef.current.reset()
      setHiddenButtonName(false)
    }
    if (!takePressed) {
      firebase.analytics().logEvent("press_take_drug", { drug2take_id: drugToTake.id })
      amplitude.getInstance().logEvent("press_take_drug", { drug2take_id: drugToTake.id })
    } else {
      firebase.analytics().logEvent("press_take_drug_cancel", { drug2take_id: drugToTake.id })
      amplitude.getInstance().logEvent("press_take_drug_cancel", { drug2take_id: drugToTake.id })
    }

    const options = {
      enableVibrateFallback: true,
      ignoreAndroidSystemSettings: true,
    }
    ReactNativeHapticFeedback.trigger("impactLight", options)

    setTimeout(() => {
      setTakePressed(!takePressed)
      onTakePress()
      setProcessing(false)
    }, 200)
  }

  const renderItem = ({ item, index }: { item: PrescriptionDetailBanner; index: number }) => {
    const getPrescriptionDetail: PrescriptionDetailShort = prescription_detail
    const { id, drug_eng } = getPrescriptionDetail
    return (
      <View style={!bannerVisible && { display: "none" }}>
        <View>
          <TouchableOpacity
            onPress={() => {
              setBannerVisible(false)
            }}
            style={CLOSE_BANNER}
          >
            <SvgCloseBanner />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.5}
            style={SLIDE_INNER_CONTAINER}
            onPress={async () => {
              switch (item.banner_kind) {
                case "DISEASE":
                  navigation.navigate("Details", {
                    screen: "DiseaseSummary",
                    params: {
                      id: drugToTake.prescription.disease.id,
                      code: drugToTake.prescription.disease.code,
                      guideTabVisible: true,
                      tabIndex: 1,
                    },
                  })
                  break

                case "VIDEO":
                  await recordStore.fetchVideo(item.record_id)
                  if (recordStore.status === "done" && recordStore.recordVideo) {
                    navigation.navigate("Details", {
                      screen: "Activity",
                      params: { title: "운동", recordId: item?.record_id },
                    })
                  }
                  break

                case "webview":
                  // 약 사용법 이벤트 기록  ex) xoterna_1048974_7
                  sendLogEvent(`${drug_eng}_${id}_${item.id}`)
                  navigation.navigate("Auth", {
                    screen: "WebView",
                    params: { title: "사용법", content: item?.howto },
                  })
                  break

                case "SURVEY":
                  navigation.navigate("Details", {
                    screen: "SurveyIntro",
                    params: { recordId: item.record_id },
                  })
                  break

                default:
                  break
              }
            }}
          >
            <WebView
              ref={(ref) => {
                webViewRefs.current[index] = ref
                if (index == 0) {
                  setCurrentWebViewRef(ref)
                }
              }}
              androidLayerType={"hardware"}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              allowFileAccess={true}
              originWhitelist={["*"]}
              containerStyle={{ borderRadius: 8 }}
              source={{ html: item?.intro }}
              bounces={false}
              scrollEnabled={false}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              textZoom={100}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  return (
    <View>
      <View style={DRUG_ROW}>
        {image_url ? (
          <FastImage source={{ uri: image_url }} style={DRUG_IMAGE} resizeMode="cover" />
        ) : (
          <View style={DRUG_IMAGE}>
            <SvgDrugEmptyImage width={64 * 0.8} height={40 * 0.8} />
          </View>
        )}

        <View style={TEXT_BOX}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={DRUG_NAME}>
            {prescription_detail.drug}
          </Text>
          <Text style={DESCRIPTION}>
            {prescription_detail.dose}
            {prescription_detail.unitConverted}
          </Text>
        </View>
        <TouchableOpacity
          style={[
            issueDate < pickedDate && pickedDate < currentDate ? TAKE_BUTTON : DEACTIVATE,
            takePressed ? TAKEN_BUTTON : TAKE_BUTTON,
          ]}
          hitSlop={hitSlop10}
          onPress={() => onTakePressInternal()}
          disabled={issueDate < pickedDate && pickedDate < currentDate ? processing : !processing}
        >
          {/* <LottieView
            ref={animationRef}
            resizeMode={"cover"}
            key={1}
            source={checkButton}
            speed={2}
            loop={false}
            duration={300}
            progress={takePressed ? 1 : undefined}
          /> */}
          <Text
            style={[
              issueDate < pickedDate && pickedDate < currentDate
                ? TAKE_BUTTON_TEXT
                : DEACTIVATE_TEXT,
              takePressed ? TAKEN_BUTTON_TEXT : TAKE_BUTTON_TEXT,
            ]}
          >
            {/* {!hiddenButtonName && "복약"} */}
            {takePressed ? "먹음" : "복약"}
          </Text>
        </TouchableOpacity>
      </View>
      {/* #노바티스 02
      {banners ? (
        banners.length === 1 ? (
          renderItem({ item: banners[0], index: 0 })
        ) : (
          <View style={!bannerVisible && { display: "none" }}>
            <Carousel
              ref={carouselRef}
              data={banners}
              renderItem={renderItem}
              sliderWidth={sliderWidth}
              itemWidth={itemWidth}
              inactiveSlideOpacity={1}
              containerCustomStyle={SLIDER}
              decelerationRate={1}
              loop={false}
              loopClonesPerSide={2}
              onSnapToItem={(index) => {
                setActiveSlide(index)
                setCurrentWebViewRef(webViewRefs.current[index])
              }}
            />
            <Pagination
              dotsLength={banners.length}
              activeDotIndex={activeSlide}
              containerStyle={PAGINATION_CONTAINER}
              dotStyle={PAGINATION_DOT}
              inactiveDotStyle={PAGINATION_INACTIVE_DOT}
              inactiveDotOpacity={1}
              inactiveDotScale={1}
              carouselRef={carouselRef.current}
            />
          </View>
        )
      ) : null} */}
    </View>
  )
})
