import React from "react"
import { Image, TextStyle, View, ViewStyle } from "react-native"

// import ImgEmptyDrugToTakeList from "../assets/images/ic_pictogram80px_emptyTask.png"
// import SvgEmptyDrugToTakeList from "../assets/images/ic_pictogram80px_emptyTask.svg"
import { Text } from "../components/StyledText"
import { color, typography } from "../theme"
import { getText } from "./TimeSlotRow"

export interface EmptyDrugsToTakeListProps {
  timeSlot: string
}

const CONTAINER: ViewStyle = {
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: color.palette.white,
  paddingVertical: 24,
  // borderRadius: 5,
  borderBottomWidth: 1,
  borderBottomColor: color.borders.neutralSubded,
}

const DESCRIPTION: TextStyle = {
  ...typography.captions.default,
  color: color.textColor.subdued,
  textAlign: "center",
}

export const EmptyDrugsToTakeList: React.FunctionComponent<EmptyDrugsToTakeListProps> = (props) => {
  const { timeSlot } = props

  return (
    <View style={CONTAINER}>
      {/* <SvgEmptyDrugToTakeList /> */}
      <Image
        source={require("../assets/images/ic_pictogram80px_emptyTask.png")}
        style={[
          {
            width: 80,
            height: 80,
            // tintColor: color.palette.white,
          },
        ]}
      />
      <View style={{ height: 12 }} />
      <Text style={DESCRIPTION}>{getText(timeSlot)}에 복용할 약이 없습니다.</Text>
    </View>
  )
}
