import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { observer } from "mobx-react"
import React, { useEffect, useRef, useState } from "react"
import { Dimensions, TouchableOpacity, View, ViewStyle } from "react-native"
import Carousel, { Pagination } from "react-native-snap-carousel"
import WebView from "react-native-webview"

import firebase from "@react-native-firebase/app"

import { useStores } from "../App"
import { RootNavigation } from "../navigation"
import { Campaign } from "../stores/campaign/Campaign"
import { Event } from "../stores/campaign/Event"
import { color } from "../theme"

export interface CampaignRowProps {
  data: Campaign
}

const { width: viewportWidth } = Dimensions.get("window")
export const sliderWidth = viewportWidth
const itemHorizontalMargin = 16
const itemWidth = sliderWidth - itemHorizontalMargin * 2
const slideHeight = 64
// const slideHeight = ((157 * itemWidth) / 360) * 1.05
const SLIDER: ViewStyle = {
  overflow: "visible", // for custom animations
}
const SLIDER_CONTENT_CONTAINER = {
  height: slideHeight,
}
const SLIDE_INNER_CONTAINER: ViewStyle = {
  width: itemWidth,
  height: slideHeight,
}
const PAGINATION_DOT = {
  width: 4,
  height: 4,
  borderRadius: 4,
  marginLeft: -12,
  backgroundColor: `${color.palette.black}60`,
}
const PAGINATION_INACTIVE_DOT = {
  width: 4,
  height: 4,
  borderRadius: 4,
  backgroundColor: `${color.dim}30`,
}
const PAGINATION_CONTAINER: ViewStyle = {
  // height: 8,
  paddingTop: 8,
  paddingBottom: 0,
  alignItems: "center",
}
const hitSlop10 = { top: 10, left: 10, right: 10, bottom: 10 }

export const CampaignRow: React.FunctionComponent<CampaignRowProps> = observer((props) => {
  const carouselRef = useRef(null)
  const { data } = props
  const [activeSlide, setActiveSlide] = useState(0)
  const { campaignStore } = useStores()
  const webViewRefs = useRef([]) // Multiple references
  const [currentWebViewRef, setCurrentWebViewRef] = useState<WebView>()
  const [currentItemIndex, setCurrentItemIndex] = useState(-1)

  useEffect(() => {
    webViewRefs.current = new Array(data.events.length)
  }, [])

  useEffect(() => {
    setTimeout(() => {
      const javascript = injectJavaScript(data.events[activeSlide])
      if (javascript) {
        currentWebViewRef?.injectJavaScript(javascript)
      }
    }, 300)
  }, [currentWebViewRef])

  const injectJavaScript = (item: Event) => {
    let javascript
    switch (item.title) {
      case "가장 많이 등록된 약":
        const drugRanking = campaignStore.rankings.get("drug")
        if (drugRanking) {
          javascript = `
            document.getElementById("rollText1").innerHTML = "`
          for (let i = 0; i < drugRanking.length; i++) {
            const element = drugRanking[i]
            const name = element.drug.name
            javascript += `<div>${i + 1}위 <span id='medicine_rank_${i}'>${name.substring(
              0,
              name.indexOf("(") === -1
                ? name.indexOf("/") === -1
                  ? name.length
                  : name.indexOf("/")
                : name.indexOf("("),
            )}</span></div>`
          }
          javascript += `";`
          return javascript
        }
        break

      case "가장 많이 등록된 질병":
        const diseaseRanking = campaignStore.rankings.get("disease")
        if (diseaseRanking) {
          javascript = `
            document.getElementById("rollText1").innerHTML = "`
          for (let i = 0; i < diseaseRanking.length; i++) {
            const element = diseaseRanking[i]
            const title = element.disease.rep_title
            javascript += `<div>${i + 1}위 <span id='disease_rank_${i}'>${title}</span></div>`
          }
          javascript += `";`
          return javascript
        }
        break
    }
    return undefined
  }

  const getJavascriptForDetailScreen = (item: Event) => {
    let javascript
    switch (item.title) {
      case "가장 많이 등록된 약":
        const drugRanking = campaignStore.rankings.get("drug")
        if (drugRanking) {
          javascript = `
            document.getElementById("list").innerHTML = "`

          for (let i = 0; i < drugRanking.length; i++) {
            const element = drugRanking[i]
            const name = element.drug.name
            javascript += `<div onclick='pressDrug(${
              element.id
            })' style='display: inline-flex; flex-direction: row; justify-content: space-between; align-items: center; padding: 18px; margin-bottom: 12px; background-color: white; border-radius: 8px;'><div style='display: inline-flex; flex-direction: row; align-items: center'><div style='display: inline-flex; justify-content: center; align-items: center; background-color: #faf2da; width: 36px; height: 36px; border-radius: 18px; margin-right: 13px;'><span style='color: #56461e; font-size: 14px; font-weight: 400'>${
              i + 1
            }위</span></div><span id='disease_list_${i}' style='font-size: 20px; font-weight: 400; color: #56461e; max-width: 80%; overflow: hidden;'>${name.substring(
              0,
              name.indexOf("(") == -1
                ? name.indexOf("/") == -1
                  ? name.length
                  : name.indexOf("/")
                : name.indexOf("("),
            )}</span></div><div style='display: inline-flex; align-items: center; color: rgba(86, 70, 30, 0.7); text-decoration: underline; font-size: 14px; font-weight: 400; white-space: nowrap; padding-right: 8px;'><span>보기</span></div></div>`
          }
          javascript += `";
            document.getElementById("time_base").innerHTML = "`
          javascript +=
            DateTime.local().minus({ days: 1 }).toFormat("yyyy년 MM월 dd일") + ", 자정 기준"
          javascript += `";true;`
        }
        break

      case "가장 많이 등록된 질병":
        const diseaseRanking = campaignStore.rankings.get("disease")
        if (diseaseRanking) {
          javascript = `
            document.getElementById("list").innerHTML = "`
          for (let i = 0; i < diseaseRanking.length; i++) {
            const element = diseaseRanking[i]
            const title = element.disease.rep_title
            javascript += `<div onclick='pressDisease(${
              element.id
            })' style='display: inline-flex; flex-direction: row; justify-content: space-between; align-items: center; padding: 18px; margin-bottom: 12px; background-color: white; border-radius: 8px;'><div style='display: inline-flex; flex-direction: row; align-items: center;'><div style='display: inline-flex; justify-content: center; align-items: center; background-color: #d8eaf4; width: 36px; height: 36px; border-radius: 18px; margin-right: 13px;'><span style='color: #43609b; font-size: 14px; font-weight: 400'>${
              i + 1
            }위</span></div><span id='disease_list_${i}' style='font-size: 20px; font-weight: 400; color: #43609b; max-width: 80%; overflow: hidden;'>${title}</span></div><div style='display: inline-flex; align-items: center; color: rgba(67, 96, 155, 0.7); text-decoration: underline; font-size: 14px; font-weight: 700; white-space: nowrap;'><span>보기</span></div></div>`
          }
          javascript += `";
            document.getElementById("time_base").innerHTML = "`
          javascript +=
            DateTime.local().minus({ days: 1 }).toFormat("yyyy년 MM월 dd일") + ", 자정 기준"
          javascript += `";true;`
        }
    }
    return javascript
  }

  const renderItem = ({ item, index }: { item: Event; index: number }) => {
    return (
      <TouchableOpacity
        // activeOpacity={1}
        style={SLIDE_INNER_CONTAINER}
        hitSlop={hitSlop10}
        onPress={() => {
          firebase.analytics().logEvent(`press_banner`, { title: item.title })
          amplitude.getInstance().logEvent("press_banner", { title: item.title })
          RootNavigation.navigate("Details", {
            screen: "WebView",
            params:
              item.target_type === "link"
                ? { url: item.target_url, title: item.title }
                : {
                    content: item.target_event?.content ?? "",
                    title: item.title,
                    javaScript: getJavascriptForDetailScreen(item),
                  },
          })
        }}
      >
        <WebView
          ref={(ref) => {
            webViewRefs.current[index] = ref
            if (index === 0 && currentItemIndex !== 0) {
              setCurrentWebViewRef(ref)
              setCurrentItemIndex(index)
            }
          }}
          androidLayerType={"hardware"}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          allowFileAccess={true}
          originWhitelist={["*"]}
          containerStyle={{ borderRadius: 8 }}
          source={{ html: item.content }}
          bounces={false}
          scrollEnabled={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          textZoom={100}
          textInteractionEnabled={false}
          pointerEvents="none"
        />
      </TouchableOpacity>
    )
  }

  if (data.events.length === 0) {
    return null
  } else if (data.events.length === 1) {
    return renderItem({ item: data.events[0], index: 0 })
  } else {
    return (
      <>
        <Carousel
          ref={carouselRef}
          data={data.events}
          renderItem={renderItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          // hasParallaxImages={true}
          // inactiveSlideScale={0.94}
          inactiveSlideOpacity={1}
          // enableMomentum={true}
          // inactiveSlideShift={20}
          containerCustomStyle={SLIDER}
          // decelerationRate={1} // 가속 끔
          contentContainerCustomStyle={SLIDER_CONTENT_CONTAINER}
          loop={false}
          loopClonesPerSide={2}
          onSnapToItem={(index) => {
            setActiveSlide(index)
            setCurrentWebViewRef(webViewRefs.current[index])
          }}
          vertical={false}
        />
        <Pagination
          dotsLength={data.events.length}
          activeDotIndex={activeSlide}
          containerStyle={PAGINATION_CONTAINER}
          dotStyle={PAGINATION_DOT}
          inactiveDotStyle={PAGINATION_INACTIVE_DOT}
          inactiveDotOpacity={1}
          inactiveDotScale={1}
          carouselRef={carouselRef.current}
          tappableDots={false}
        />
      </>
    )
  }
})
