import React from "react"
import { Image, ImageStyle, StyleSheet, TouchableOpacity, ViewStyle } from "react-native"

import { color } from "../theme"

export interface Props {
  onPress: any
  imageStyle?: ImageStyle
  style?: ViewStyle
}

export class CloseButton extends React.Component<Props> {
  render() {
    // grab the props
    // const { option, onPress, selected, ...rest } = this.props
    const { onPress, style, imageStyle, ...rest } = this.props

    return (
      <TouchableOpacity
        onPress={onPress}
        {...rest}
        style={[styles.button, style]}
        hitSlop={{ top: 13, left: 13, right: 13, bottom: 13 }}
      >
        <Image
          source={require("../assets/images/close.png")}
          style={[
            {
              width: 15.7,
              height: 15.7,
              // tintColor: color.palette.white,
            },
            imageStyle,
          ]}
        />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    marginRight: 10,
  },
  selectedButton: {
    backgroundColor: "#fff",
  },
  textBox: {
    height: 72,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 12,
    color: "#282828",
  },
})

export default CloseButton
