import { DateTime } from "luxon"
import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { Dimensions, Platform, TextStyle, TouchableHighlight, View, ViewStyle } from "react-native"

import { Slider } from "@miblanchard/react-native-slider"

import SvgDurWarning from "../assets/images/durWarning.svg"
import SvgPrescription from "../assets/images/prescription.svg"
import SvgProcessing from "../assets/images/processing.svg"
import SvgRejected from "../assets/images/rejected.svg"
import { Text } from "../components/StyledText"
import { formatDateFromString } from "../lib/DateUtil"
import { AccountStore } from "../stores/AccountStore"
import { Prescription, PrescriptionDetail, PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"

export interface Props {
  index: number
  prescription: Prescription
  prescriptionStore?: PrescriptionStore
  accountStore?: AccountStore
  onPress?: (dosingDays: number) => void
  onWithinDurNotiPeriod?: () => void
}

interface State {
  dosingDays: number
  diffDays: number
  remainingDosingDays: number
  withinDurNotiPeriod: boolean
  isSample: boolean
}

const { width } = Dimensions.get("window")
const FIRST_ITEM_CONTAINER: ViewStyle = {
  width: width - 32,

  borderRadius: 5,
  borderWidth: 1,
  borderColor: color.palette.pale,
  shadowColor: "#14453b30",
  shadowOffset: {
    width: 0,
    height: 5,
  },
  shadowOpacity: 0.3,
  shadowRadius: 10,
  backgroundColor: color.palette.white,
  marginHorizontal: 16,
  // marginTop: 24,
  padding: 16,
}
const ITEM_CONTAINER: ViewStyle = {
  width: width - 32,
  borderRadius: 5,
  borderWidth: 1,
  borderColor: color.palette.pale,
  shadowColor: "#14453b30",
  shadowOffset: {
    width: 0,
    height: 5,
  },
  shadowOpacity: 0.3,
  shadowRadius: 10,
  backgroundColor: color.palette.white,
  marginHorizontal: 16,
  padding: 16,
}
const ISSUE_DATE: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
  marginLeft: 12,
}
const DISEASE: TextStyle = {
  ...typography.title3,
  lineHeight: 25,
}
const HOSPITAL: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
  flex: 1.4,
  lineHeight: 19,
}
const PROGRESS_BAR_ROW: ViewStyle = {
  paddingTop: 4,
  paddingLeft: 48,
  flexDirection: "row",
  alignItems: "center",
}
const DOSING_DAYS_TEXT: TextStyle = {
  ...typography.sub2,
  lineHeight: 19,
  opacity: 0.5,
}
const REJECTED_TEXT: TextStyle = {
  ...typography.title3,
  color: color.primary,
}
const PENDING_TEXT: TextStyle = {
  ...typography.title3,
  opacity: 0.5,
}
const SAMPLE_TAG_BOX: ViewStyle = {
  width: 36,
  height: 18,
  borderColor: color.primary,
  borderRadius: 9,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  marginLeft: 4,
  marginBottom: Platform.OS === "android" ? 2 : 0,
}
const SAMPLE_TEXT: TextStyle = {
  fontFamily: Platform.select({
    ios: "NotoSansKR-Regular",
    android: "NotoSansKR-Regular-Hestia",
  }),
  fontSize: 13,
  color: color.primary,
  textAlignVertical: "center",
  marginTop: Platform.OS === "ios" ? -2 : 0,
}
const THUMBNAIL: ViewStyle = { marginRight: 8 }
const REJECTED_DESCRIPTION: TextStyle = {
  ...typography.sub2,
  flex: 1.4,
  lineHeight: 19,
  color: color.primary,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const DUR_WARNING_TEXT: TextStyle = {
  ...typography.sub1,
  color: color.palette.copper,
  marginLeft: 4,
}
const DUR_WARNING_BOX: ViewStyle = {
  backgroundColor: "#fff3ed",
  borderRadius: 1,
  paddingVertical: 4,
  paddingHorizontal: 8,
  marginTop: 16,
  flexDirection: "row",
  alignItems: "center",
}
const TAKEN_DAYS_TEXT: TextStyle = {
  ...typography.sub2,
  lineHeight: 19,
  color: color.palette.darkMint,
  marginLeft: 10,
}
const SLIDER: ViewStyle = { flex: 1, height: 4, borderRadius: 1, margin: 0, padding: 0 }
const SLIDER_TRACK: ViewStyle = { height: 4, borderRadius: 1, backgroundColor: color.palette.pale }
const DOSING_DAYS: ViewStyle = { flexDirection: "row", flex: 0.48, justifyContent: "flex-end" }
const DISEASE_NAME_BOX: ViewStyle = { flex: 1, flexDirection: "row", alignItems: "center" }

@inject("accountStore", "prescriptionStore")
@observer
export default class PrescriptionItem extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      dosingDays: 0,
      diffDays: 0,
      remainingDosingDays: 0,
      withinDurNotiPeriod: false,
      isSample: false,
    }
  }

  UNSAFE_componentWillMount() {
    this.computeDosingDays()
    this.parseDurRes()
  }

  parseDurRes() {
    const prescription = this.props.prescription
    prescription.setDurRes()
  }

  computeDosingDays() {
    let isSample = false

    const { accountStore, prescription, prescriptionStore } = this.props

    if (prescription.status === "CF" && prescription.start_at && prescription.end_at) {
      const startDate = DateTime.fromISO(prescription.start_at)
      const endDate = DateTime.fromISO(prescription.end_at)
      const today = prescription.todayObject
      const dosingDays = endDate.diff(startDate, "days").days + 1
      // 두 날짜 차이 구하기
      const diffDays = today.diff(startDate, "days").days + 1

      const remainingDosingDays = dosingDays - diffDays

      // validate DUR Notification period
      let withinDurNotiPeriod = false
      if (diffDays <= 30 && remainingDosingDays > 0) {
        withinDurNotiPeriod = true
        this.props.onWithinDurNotiPeriod()
      }

      if (
        diffDays <= dosingDays &&
        prescription.disease.length > 0 &&
        (prescription.disease[0].name_kr.includes("샘플 처방전") ||
          prescription.disease[0].name_kr.includes("처방전 예시"))
      ) {
        isSample = true
      }

      if (prescription.user === accountStore.user.id && remainingDosingDays <= 0) {
        prescriptionStore.addDosingEndedPrescription(prescription)
      }

      this.setState({
        dosingDays: dosingDays,
        diffDays: diffDays,
        remainingDosingDays: remainingDosingDays,
        withinDurNotiPeriod: withinDurNotiPeriod,
        isSample: isSample,
      })
    }
  }

  renderContent = () => {
    const { prescription } = this.props
    const durRes = prescription.durRes
    // 괄호 안 텍스트 제거 '(처방전 예시)'
    // refresh 할 때 별도로 샘플 처방전을 fetch 하는 item 은 UNSAFE_componentWillMount 가 호출되지 않아 render 로 이동함.
    if (this.state.isSample) {
      const disease = prescription.disease[0]
      if (disease.one_liner.includes("(")) {
        prescription.disease[0].setOneLiner(
          disease.one_liner.substring(0, disease.one_liner.indexOf("(")),
        )
      }
    }
    return (
      <View>
        <View style={ROW}>
          {prescription.status === "RJ" ? (
            <SvgRejected width={40} height={40} style={THUMBNAIL} />
          ) : prescription.status === "UL" || prescription.status === "IP" ? (
            <SvgProcessing width={40} height={40} style={THUMBNAIL} />
          ) : (
            <SvgPrescription width={40} height={40} style={THUMBNAIL} />
          )}

          <View style={{ flex: 1 }}>
            <View style={[ROW, { flex: 1 }]}>
              <View style={DISEASE_NAME_BOX}>
                {prescription.status === "RJ" ? (
                  <Text style={REJECTED_TEXT}>처방전 반려됨</Text>
                ) : prescription.status === "UL" || prescription.status === "IP" ? (
                  <Text style={PENDING_TEXT}>처방전 처리중{prescription.pendingNumber}</Text>
                ) : prescription.disease.length + prescription.subdiseases.length === 0 ? (
                  <Text numberOfLines={1} style={DISEASE}>
                    질병명 없음
                  </Text>
                ) : prescription.disease.length === 0 && prescription.subdiseases.length >= 1 ? (
                  <Text numberOfLines={1} style={DISEASE}>
                    {prescription.subdiseases.length >= 2 && prescription.subdiseases[0].one_liner
                      ? prescription.subdiseases[0].one_liner +
                        " 외 " +
                        (prescription.subdiseases.length - 1)
                      : prescription.subdiseases[0].name_kr}
                  </Text>
                ) : prescription.disease.length + prescription.subdiseases.length > 1 ? (
                  <Text numberOfLines={1} style={DISEASE}>
                    {(prescription.disease[0].one_liner.length > 1
                      ? prescription.disease[0].one_liner
                      : prescription.disease[0].name_kr) +
                      " 외 " +
                      (prescription.disease.length + prescription.subdiseases.length - 1)}
                  </Text>
                ) : (
                  <Text numberOfLines={1} style={DISEASE}>
                    {prescription.disease[0] !== undefined
                      ? prescription.disease[0].one_liner.length > 1
                        ? prescription.disease[0].one_liner
                        : prescription.disease[0].name_kr
                      : ""}
                  </Text>
                )}
                {this.state.isSample ? (
                  <View style={SAMPLE_TAG_BOX}>
                    <Text style={SAMPLE_TEXT} allowFontScaling={false}>
                      예시
                    </Text>
                  </View>
                ) : null}
              </View>
              {prescription.status === "CF" ? (
                <View>
                  <Text style={ISSUE_DATE}>{formatDateFromString(prescription.issue_date)}</Text>
                </View>
              ) : null}
            </View>
            <View style={[ROW, { flex: 1, alignItems: "center", marginTop: 3 }]}>
              {prescription.status === "RJ" ? (
                <Text style={REJECTED_DESCRIPTION}>반려 사유 보기</Text>
              ) : prescription.status === "UL" || prescription.status === "IP" ? (
                <Text style={HOSPITAL}>분석 내용을 검토중입니다.</Text>
              ) : prescription.hospital ? (
                <Text numberOfLines={1} style={HOSPITAL}>
                  {prescription.hospital.name}
                </Text>
              ) : (
                <Text style={HOSPITAL}>병원 정보 없음</Text>
              )}
            </View>
          </View>
        </View>
        {this.state.remainingDosingDays > 0 ? (
          <View style={PROGRESS_BAR_ROW}>
            <Slider
              disabled
              value={this.state.diffDays / this.state.dosingDays}
              containerStyle={SLIDER}
              trackStyle={SLIDER_TRACK}
              thumbStyle={{ width: 0 }}
              minimumTrackTintColor={color.palette.darkMint}
            />
            <View style={DOSING_DAYS}>
              <Text style={TAKEN_DAYS_TEXT}>{`${this.state.diffDays}일`}</Text>
              <Text style={DOSING_DAYS_TEXT}>{`/${this.state.dosingDays}일`}</Text>
            </View>
          </View>
        ) : null}
        {durRes &&
        this.props.accountStore.user.dur_noti === "y" &&
        ((durRes.preg.length > 0 && this.props.accountStore.user.dur_preg === "y") ||
          // (durRes.hng.length > 0 && this.props.accountStore.user.dur_hng === "y") ||
          // (durRes.dis.length > 0 && this.props.accountStore.user.dur_dis === "y") ||
          // (durRes.byg.length > 0 && this.props.accountStore.user.dur_byg === "y") ||
          (durRes.nosale.length > 0 && this.props.accountStore.user.dur_nosale === "y")) &&
        this.state.withinDurNotiPeriod ? (
          <View style={DUR_WARNING_BOX}>
            <SvgDurWarning width={16} height={16} />
            <Text style={DUR_WARNING_TEXT}>약제에 대한 주의사항이 있습니다.</Text>
          </View>
        ) : null}
      </View>
    )
  }

  render() {
    const { index, onPress } = this.props
    const { dosingDays } = this.state
    return (
      <TouchableHighlight
        onPress={() => onPress(dosingDays)}
        style={index == 0 ? FIRST_ITEM_CONTAINER : ITEM_CONTAINER}
        underlayColor="#F6F3F2"
      >
        {this.renderContent()}
      </TouchableHighlight>
    )
  }
}
