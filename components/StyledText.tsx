import { GetProps, styled, Text as TamaguiText } from "tamagui"

import { typography } from "../theme"

export type Props = GetProps<typeof Text>

export const Text = styled(TamaguiText, {
  maxFontSizeMultiplier: 1.3,
  fontFamily: typography.primary,
})
