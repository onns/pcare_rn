import { Input as BasicInput } from "native-base"
import React, { Component } from "react"
import { ReturnKeyTypeOptions, TextStyle } from "react-native"

import { typography } from "../theme"

export interface Props {
  style?: TextStyle
  selectionColor?: string
  placeholder?: string
  placeholderTextColor?: string
  onChangeText?: (text: string) => void
  value?: string
  returnKeyType?: ReturnKeyTypeOptions
  onSubmitEditing?: () => void
  variant?: string
}

export class Input extends Component<Props> {
  render() {
    return (
      <BasicInput
        {...this.props}
        style={[{ fontFamily: typography.primary, padding: 0 }, this.props.style]}
      />
    )
  }
}
