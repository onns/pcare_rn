import { observer } from "mobx-react-lite"
import React, { useEffect, useState } from "react"
import { TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { FlatList } from "react-native-gesture-handler"

import { api } from "@api/api"
import { useFocusEffect, useNavigation } from "@react-navigation/native"

import { useStores } from "../App"
import SvgEmptyMedicineSmall from "../assets/images/medicineSmall.svg"
import { Prescription } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"
import { NoPrescriptionBeingTaken } from "./NoPrescriptionBeingTaken"
import PrescriptionReportItem from "./PrescriptionReportItem"
import { Text } from "./StyledText"

const BUTTON_CONTAINER: ViewStyle = {
  flexDirection: "row",
  marginTop: 30,
  marginBottom: 20,
}

const CENTER: ViewStyle = {
  alignItems: "center",
}

const BUTTON_START: ViewStyle = {
  height: 36,
  paddingHorizontal: 16,
  borderRadius: 16,
  justifyContent: "center",
  backgroundColor: color.palette.veryLightPink3,
}

const BUTTON_CENTER: ViewStyle = {
  height: 36,
  paddingHorizontal: 16,
  marginHorizontal: 10,
  borderRadius: 16,
  justifyContent: "center",
  backgroundColor: color.palette.veryLightPink3,
}

const BUTTON_END: ViewStyle = {
  height: 36,
  paddingHorizontal: 16,
  borderRadius: 16,
  justifyContent: "center",
  backgroundColor: color.palette.veryLightPink3,
}

const BUTTON_TEXT: TextStyle = {
  ...typography.subButton,
  fontSize: 16,
}

const CONTAINER: ViewStyle = {
  height: 131,
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "transparent",
  paddingRight: 10,
  paddingTop: 20,
  paddingBottom: 20,
  borderRadius: 5,
  margin: 16,
}

const ROW_MARGIN: ViewStyle = {
  flexDirection: "row",
  marginTop: 10,
}

const DESCRIPTION: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
  textAlign: "center",
}

export const PrescriptionReport: React.FunctionComponent = observer((props) => {
  const { accountStore, prescriptionStore } = useStores()
  const { selectedMemberKey } = accountStore
  const [isTakingButton, setIsTakingButton] = useState(false)
  const [isEndButton, setIsEndButton] = useState(false)
  const [isTotalButton, setIsTotalButton] = useState(true)
  const [withinDurNotiPeriod, setWithinDurNotiPeriod] = useState(false)
  const pStore = prescriptionStore
  const prescriptionData = pStore.prescriptions.get(selectedMemberKey)
  // const { navigation, route } = props
  const [filteredPrescriptionData, setFilteredPrescriptionData] = useState(
    prescriptionStore.prescriptions
      .get(selectedMemberKey)
      ?.filter((Item) => Item.status === "CF") ?? [],
  )
  const totalLength =
    prescriptionStore.prescriptions.get(selectedMemberKey)?.filter((Item) => Item.status === "CF")
      .length ?? 0
  const navigation = useNavigation()

  const takingActiveBtn = () => {
    setFilteredPrescriptionData(
      prescriptionStore.prescriptions
        .get(selectedMemberKey)
        ?.filter((item) => item.status === "CF" && item.remainingDosingDays > 0) ?? [],
    )
    setIsTakingButton(true)
    setIsEndButton(false)
    setIsTotalButton(false)
  }
  const endActiveBtn = () => {
    setFilteredPrescriptionData(
      prescriptionStore.prescriptions
        .get(selectedMemberKey)
        ?.filter((item) => item.status === "CF" && item.remainingDosingDays <= 0) ?? [],
    )
    setIsEndButton(true)
    setIsTakingButton(false)
    setIsTotalButton(false)
  }
  const totalActiveBtn = () => {
    setFilteredPrescriptionData(
      prescriptionStore.prescriptions
        .get(selectedMemberKey)
        ?.filter((item) => item.status === "CF") ?? [],
    )
    setIsTotalButton(true)
    setIsTakingButton(false)
    setIsEndButton(false)
  }

  const refresh = () => {
    const { selectedMemberKey } = accountStore
    if (!selectedMemberKey) {
      return
    }
    setFilteredPrescriptionData(
      prescriptionStore.prescriptions
        .get(selectedMemberKey)
        ?.filter((Item) => Item.status === "CF") ?? [],
    )
  }

  const initFetch = () => {
    const { user } = accountStore
    const myPrescriptions = prescriptionStore.prescriptions.get(`_u${user.id}`)
    if (!myPrescriptions || (myPrescriptions.length > 0 && myPrescriptions[0].user != user.id)) {
      prescriptionStore.fetchPrescriptions(user.id, `_u${user.id}`)
    }

    // 모든 가족회원 정보(14세 이상 가족회원) 가지고 오기까지 시간 걸림
    for (let i = 0; i < accountStore.family.length; i++) {
      const member = accountStore.family[i]

      if (member.family_user) {
        if (!prescriptionStore.prescriptions.get(`_f${member.family_user}`)) {
          prescriptionStore.fetchPrescriptions(
            member.family_user,
            `_f${member.family_user}`,
            undefined,
          )
        }
      } else {
        prescriptionStore.fetchPrescriptions(accountStore.user.id, `_c${member.id}`, member.id)
      }
    }
  }

  /** 처방전 데이터를 가지고 있지 않을 때만 fetch 하도록 함. */
  useEffect(() => {
    initFetch
  }, [])

  useEffect(() => {
    refresh
  }, [accountStore.selectedMemberKey])

  useFocusEffect(
    React.useCallback(() => {
      refresh
      initFetch
    }, []),
  )

  const getTakingDrugLength = () => {
    const takeDrugInfo = prescriptionData.filter(
      (item) => item.status === "CF" && item.remainingDosingDays > 0,
    )
    return takeDrugInfo.length
  }

  const getEndDoseLength = () => {
    const doseInfo = prescriptionData.filter(
      (item) => item.status === "CF" && item.remainingDosingDays <= 0,
    )
    return doseInfo.length
  }

  const onPressItem = async (item: Prescription) => {
    const prescriptionId = item.id
    const response = await api.getCompliance(prescriptionId)
    if (response.kind === "ok") {
      item.setCompliance(response.data)
    }
    navigation.navigate("MedicationReportDetail", {
      prescriptionId: item.id,
      prescription: item,
      issueDate: item.issue_date,
    })
  }
  const onPressItemModify = (item: Prescription) => {
    navigation.navigate("Details", {
      screen: "MedicationSettings",
      params: {
        prescriptionId: item.id,
        issueDate: item.issue_date,
      },
    })
  }

  const dosingDrugs = () => <NoPrescriptionBeingTaken backgroundColor={"transparent"} />

  const endedDosingDrugs = () => (
    <View style={CONTAINER}>
      <SvgEmptyMedicineSmall />
      <View style={ROW_MARGIN}>
        <Text style={DESCRIPTION}>복약을 종료한 일정이 없습니다.</Text>
      </View>
    </View>
  )

  const renderItem = ({ item, index }: { item: Prescription; index: number }) => {
    return (
      <PrescriptionReportItem
        key={item.id}
        index={index}
        prescription={item}
        pressItem={() => onPressItem(item)}
        pressItemModify={() => onPressItemModify(item)}
        onWithinDurNotiPeriod={() => {
          setWithinDurNotiPeriod(!withinDurNotiPeriod)
        }}
      />
    )
  }
  return (
    <View style={{ flex: 1, backgroundColor: color.palette.lightGrayishOrange, paddingBottom: 73 }}>
      <View style={CENTER}>
        {/* <Text style={TITLE}>
            {accountStore.user.name}님은 현재 {"\n"}
            <Text style={TITLE_ORANGE}>{prescriptionData.length}가지 </Text>
            복약 일정을 관리중입니다.
          </Text> */}

        <View style={BUTTON_CONTAINER}>
          <TouchableOpacity onPress={totalActiveBtn} style={BUTTON_START}>
            <Text
              style={
                isTotalButton
                  ? [BUTTON_TEXT, { color: color.palette.black }]
                  : [BUTTON_TEXT, { color: color.palette.gray3 }]
              }
            >
              전체{totalLength > 0 && ` (${totalLength})`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={takingActiveBtn} style={BUTTON_CENTER}>
            <Text
              style={
                isTakingButton
                  ? [BUTTON_TEXT, { color: color.palette.black }]
                  : [BUTTON_TEXT, { color: color.palette.gray3 }]
              }
            >
              복약중{totalLength > 0 && ` (${getTakingDrugLength()})`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={endActiveBtn} style={BUTTON_END}>
            <Text
              style={
                isEndButton
                  ? [BUTTON_TEXT, { color: color.palette.black }]
                  : [BUTTON_TEXT, { color: color.palette.gray3 }]
              }
            >
              종료{totalLength > 0 && ` (${getEndDoseLength()})`}
            </Text>
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={filteredPrescriptionData}
            renderItem={renderItem}
            ListEmptyComponent={!isEndButton ? dosingDrugs : endedDosingDrugs}
          />
        </View>
      </View>
    </View>
  )
})
export default PrescriptionReport
