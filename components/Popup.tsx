import LottieView from "lottie-react-native"
import * as React from "react"
import {
  Dimensions,
  Image,
  ImageSourcePropType,
  ImageStyle,
  StatusBar,
  TextStyle,
  View,
  ViewStyle,
} from "react-native"
import Modal from "react-native-modal"

import { Text } from "../components/StyledText"
import { color, typography } from "../theme"
import { Button } from "./StyledButton"

export const deviceWidth = Dimensions.get("window").width
export const deviceHeight = Dimensions.get("window").height

const POPUP_CONTAINER: ViewStyle = {
  paddingTop: 24,
  paddingHorizontal: 16,
  borderRadius: 10,
  backgroundColor: "#fff",
  marginHorizontal: 15,
}
const IMAGE: ImageStyle = {
  width: 162,
  height: 162,
  resizeMode: "contain",
  marginBottom: 24,
}
const TITLE: TextStyle = {
  ...typography.title3,
  textAlign: "center",
}

const MESSAGE: TextStyle = {
  ...typography.sub2,
  marginTop: 16,
  textAlign: "center",
}

const DEFAULT_BUTTON_GROUP: ViewStyle = { paddingBottom: 8 }
const DEFAULT_BUTTON1: ViewStyle = {
  backgroundColor: color.palette.veryLightPink3,
  borderRadius: 45,
}
const DEFAULT_BUTTON2: ViewStyle = { backgroundColor: color.palette.orange, borderRadius: 45 }

export function getPopupButton(onPress: any, text: string, type?: "cancel" | "transparent") {
  if (type === "transparent") {
    return (
      <Button transparent block onPress={onPress} style={{ marginBottom: 16 }}>
        <Text style={{ ...typography.title3, color: color.palette.grey1 }}>{text}</Text>
      </Button>
    )
  }

  return (
    <Button block onPress={onPress} style={type === "cancel" ? DEFAULT_BUTTON1 : DEFAULT_BUTTON2}>
      <Text style={{ ...typography.title3, color: type === "cancel" ? `${color.text}80` : "#fff" }}>
        {text}
      </Text>
    </Button>
  )
}

export interface PopupProps {
  isVisible: boolean
  imageSource?: ImageSourcePropType

  title: string | React.ReactNode

  message: string | React.ReactNode

  button1?: React.ReactElement
  button2?: React.ReactElement
  button3?: React.ReactElement
  buttonGroupStyle?: ViewStyle
  onBackButtonPress?: () => void
}

export class Popup extends React.Component<PopupProps, {}> {
  constructor(props: PopupProps) {
    super(props)
    // this.state = {
    // }
  }

  state = {
    isVisible: false,
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    this.setState({
      isVisible: nextProps.isVisible,
    })
  }

  render() {
    const {
      isVisible,
      imageSource,
      title,
      message,
      button1,
      button2,
      button3,
      buttonGroupStyle,
      onBackButtonPress,
    } = this.props
    return (
      <Modal
        deviceWidth={deviceWidth}
        deviceHeight={deviceHeight}
        isVisible={isVisible}
        onBackButtonPress={onBackButtonPress}
      >
        {Popup.renderPopupContent(
          imageSource,
          title,
          message,
          button1,
          button2,
          button3,
          buttonGroupStyle,
        )}
      </Modal>
    )
  }

  static renderPopupContent(
    imageSource: ImageSourcePropType,
    title: string | React.ReactNode,
    message: string | React.ReactNode | undefined,
    button1: React.ReactElement,
    button2?: React.ReactElement,
    button3?: React.ReactElement,
    buttonGroupStyle?: ViewStyle,
  ): React.ReactNode {
    return (
      <View style={POPUP_CONTAINER}>
        <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
        <View style={{ alignItems: "center", marginBottom: 32 }}>
          {imageSource ? <Image source={imageSource} style={IMAGE} /> : null}
          {typeof title === "string" ? <Text style={TITLE}>{title}</Text> : title}
          {typeof message === "string" ? <Text style={MESSAGE}>{message}</Text> : message}
        </View>
        <View style={buttonGroupStyle ? buttonGroupStyle : DEFAULT_BUTTON_GROUP}>
          {button1 ? button1 : null}
          {button2 ? <View style={{ marginTop: 8 }}>{button2}</View> : null}
          {button3 ? (
            <View style={{ marginTop: 8 }}>{button3}</View>
          ) : (
            <View style={{ height: 8 }} />
          )}
        </View>
      </View>
    )
  }

  static renderAnimationPopupContent(
    animationSource: any,
    title: string,
    message: string,
    button1: React.ReactElement,
    button2?: React.ReactElement,
    button3?: React.ReactElement,
    buttonGroupStyle?: ViewStyle,
  ): React.ReactNode {
    return (
      <View
        style={{
          paddingTop: 20,
          paddingBottom: 0,
          borderRadius: 10,
          backgroundColor: "#fff",
        }}
      >
        <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
        <LottieView
          style={{ width: deviceWidth - 74, marginBottom: 16, alignSelf: "center" }}
          source={animationSource}
          autoPlay
          loop
        />
        <View style={{ paddingHorizontal: 18 }}>
          <View style={{ alignItems: "center", marginBottom: 40 }}>
            <Text style={TITLE}>{title}</Text>
            {message ? <Text style={MESSAGE}>{message}</Text> : null}
          </View>
          <View style={buttonGroupStyle ? buttonGroupStyle : DEFAULT_BUTTON_GROUP}>
            {button1 ? button1 : null}
            <View style={{ height: 10 }}></View>
            {button2 ? button2 : null}
            <View style={{ height: 10 }}></View>
            {button3 ? button3 : null}
          </View>
        </View>
      </View>
    )
  }
}
