import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content, Icon, ScrollView, View } from "native-base"
import React from "react"
import { Platform, TouchableOpacity, ViewStyle } from "react-native"
import DeviceInfo from "react-native-device-info"
import email from "react-native-email"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgArrowRight from "../assets/images/arrowRight.svg"
import SvgPappricaCostum from "../assets/images/ic_pictogram24px_paprica_custom_service.svg"
import { Text } from "../components/StyledText"
import { sendLogEvent } from "../lib/analytics"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { color } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  // flex: 1, 스크롤 가능하게 주석 처리함.
  backgroundColor: "rgb(239, 239, 239)",
  paddingHorizontal: 18,
  paddingVertical: 12,
}

const SETTING_ITEM: ViewStyle = {
  height: 56,
  flexDirection: "row",
  alignItems: "center",
  backgroundColor: "#fff",
  borderRadius: 20,
  paddingLeft: 12,
  paddingRight: 8,
  marginBottom: 6,
}

const ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}

// type HelpCenterRouteProp = RouteProp<MyPageStackParamList, "HelpCenter">

// type HelpCenterNavigationProp = CompositeNavigationProp<
//   StackNavigationProp<MyPageStackParamList, "HelpCenter">,
//   CompositeNavigationProp<
//     BottomTabNavigationProp<MainTabParamList>,
//     CompositeNavigationProp<
//       StackNavigationProp<DetailsStackParamList>,
//       StackNavigationProp<RootStackParamList>
//     >
//   >
// >

export interface Props {
  accountStore: AccountStore
  // navigation: HelpCenterNavigationProp
  // route: HelpCenterRouteProp
}

@inject("accountStore")
@observer
export default class SendingUserQuestion extends React.Component<Props> {
  // static navigationOptions = {
  //   title: "건강질문",
  // }

  componentDidMount() {
    // firebase.analytics().logScreenView({
    //   screen_class: this.props.route.name,
    //   screen_name: this.props.route.name,
    // })
    // amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  handleEmail = () => {
    const user = this.props.accountStore.user
    const to = ["onions@onns.co.kr"] // string or array of email addresses

    let body =
      "파프리카케어 회원님만을 위한 건강정보 질의응답 서비스입니다. 아래 빈칸에 질문을 남겨보세요! 👇🏼 \n--------------\n\n\n\n\n"
    body =
      body +
      "--------------\n\n 몸의 이상징후, 약에 대한 궁금증, 약과 영양제의 상관관계, 특정 질병의 병원 추천 등 다양한 질문을 여쭤보세요.\n\n 파프리카케어에서 보유한 정보와 데이터를 기반으로 가능한 답변을 드립니다. \n\n📍 질문 예시\n - 오른쪽 엄지 발가락에 이유없이 찌르는 듯한 통증이 생겼어요. 어떻게 해야할지 알려주세요.\n- 서울시 강남구에 고혈압으로 환자들이 많이 찾아간 병원을 알려주세요.\n - 내가 먹는 약과 오메가3를 같이 복용해도 되나요?(병용복약 상호작용)\n\n📣안내\n\n 의학적 소견이 필요한 질문에 대해서는 전문의료진의 역활을 대신할 수 없음을 알려드립니다.\n 답변에 시간이 다소 소요될 수 도 있으니 이점 양해 미리 부탁드려요."
    body = body + `\n\n파프리카케어 ID: ${user.id}, 앱 버전: ${packageJson.version}, `
    // body = body + `플랫폼 OS: ${Platform.OS} v${Platform.Version}`
    // body = body + `디바이스 정보: ${DeviceInfo.getModel()}`

    email(to, {
      subject: "건강 정보 질문하기",
      body: body,
    }).catch(console.error)

    sendLogEvent("press_sending_user_question")
  }

  render() {
    return (
      <View>
        <View style={{ marginTop: 12, backgroundColor: "white" }}></View>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <TouchableOpacity style={SETTING_ITEM} onPress={this.handleEmail} activeOpacity={0.9}>
            <View style={ROW}>
              <SvgPappricaCostum width={32} height={32} />
              <View style={{ flexDirection: "column", marginLeft: 8 }}>
                <Text style={{ fontSize: 15 }}>건강 정보에 대해 물어보세요.</Text>
                <Text style={{ fontSize: 13, color: color.textColor.action }}>
                  파프리카케어의 통계 정보를 통해 답변 드려요
                </Text>
              </View>
              <View style={{ flex: 1, justifyContent: "center", alignItems: "flex-end" }}>
                <SvgArrowRight width={24} height={24} color={color.primary} />
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    )
  }
}
