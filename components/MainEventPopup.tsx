import { DateTime } from "luxon"
import React, { memo, useEffect, useState } from "react"
import { Pressable, StatusBar, ViewStyle } from "react-native"
import Modal from "react-native-modal"
import { Button, Image, Separator, XStack, YStack } from "tamagui"

import { loadString, saveString } from "@lib/storage"
import remoteConfig from "@react-native-firebase/remote-config"
import { useNavigation } from "@react-navigation/native"

const fullWidthStyle: ViewStyle = { margin: 0, alignItems: "center" }

type Props = {
  opacity?: number
}

const MainEventPopup = ({ opacity = 0.5 }: Props) => {
  const navigation = useNavigation()
  const [isVisible, setIsVisible] = useState(true)
  const [isStopped, setIsStopped] = useState(true)

  useEffect(() => {
    ;(async () => {
      const title = await loadString("stop_main_popup_title")
      const date = await loadString("stop_main_popup_date")

      if (title === remoteConfig().getString("main_popup_title")) {
        // return
      } else if (date === DateTime.now().toSQLDate()) {
        // return
      } else {
        setIsStopped(false)
      }
    })()
  }, [])

  const onImagePress = () => {
    navigation.navigate("Details", {
      screen: "WebView",
      params: {
        url: remoteConfig().getString("main_popup_link"),
        title: remoteConfig().getString("main_popup_title"),
      },
    })
    setIsVisible(false)
  }

  const onStopPress = () => {
    saveString("stop_main_popup_title", remoteConfig().getString("main_popup_title"))
    setIsVisible(false)
  }

  const handleClosePress = () => {
    saveString("stop_main_popup_date", DateTime.now().toSQLDate())
    setIsVisible(false)
  }

  return (
    <>
      {!isStopped && (
        <Modal
          isVisible={isVisible}
          style={fullWidthStyle}
          backdropOpacity={opacity}
          animationIn="fadeIn"
          animationOut="fadeOut"
        >
          <StatusBar backgroundColor={`rgba(0, 0, 0, ${opacity})`} />
          <YStack f={1} w={297} jc="center" style={{ position: "absolute", overflow: "hidden" }}>
            <Pressable onPress={onImagePress}>
              <Image
                src={remoteConfig().getString("main_popup_image_url")}
                width={297}
                height={420}
                resizeMode="cover"
              />
            </Pressable>
            <XStack ai="center" mt={16}>
              <Button w="45%" size="$3.5" onPress={onStopPress} color="#fff" chromeless>
                그만 보기
              </Button>
              <Separator als="stretch" vertical mx={15} />
              <Button w="45%" size="$3.5" onPress={handleClosePress} color="#fff" chromeless>
                닫기
              </Button>
            </XStack>
          </YStack>
        </Modal>
      )}
    </>
  )
}

export default memo(MainEventPopup)
