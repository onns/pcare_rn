import { observer } from "mobx-react"
import React, { useState } from "react"
import {
  ImageStyle,
  TextStyle,
  TouchableOpacity,
  useWindowDimensions,
  View,
  ViewStyle,
} from "react-native"
import FastImage from "react-native-fast-image"

import { RootNavigation } from "@navigation/NavigationService"
import remoteConfig from "@react-native-firebase/remote-config"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import { useStores } from "../App"
import SvgArrowDown from "../assets/images/ic_ui24px_caret_line_down.svg"
import SvgSearch from "../assets/images/ic_ui24px_searches__plain.svg"
import { Text } from "../components/StyledText"
import { sendLogEvent } from "../lib/analytics"
import { FamilyMember } from "../stores/FamilyMember"
import { color } from "../theme/color"
import { typography } from "../theme/typography"
import FamilyRow from "./FamilyRow"

export interface MemberSelectHeaderProps {
  onSelectMember: (memberKey: string, member?: FamilyMember) => void
  hideNotice?: boolean
  navigation: { HomeNavigationProp }
}

const HEADER: ViewStyle = {
  elevation: 0,
  backgroundColor: color.palette.white,
}
const CIRCLE: ImageStyle = {
  width: 38,
  height: 38,
  borderRadius: 16,
  borderWidth: 1,
  borderColor: color.palette.white,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: color.background,
  marginRight: 12,
}
const ORANGE_DOT: ViewStyle = {
  position: "absolute",
  width: 12,
  height: 12,
  borderRadius: 6,
  borderWidth: 2,
  borderColor: color.palette.white,
  marginLeft: 11,
  backgroundColor: color.palette.orange,
}
const SELECTED_MEMBER_NAME: TextStyle = {
  ...typography.button,
  lineHeight: 25,
  marginRight: 2,
}
const SELECTED_MEMBER_ROW: ViewStyle = {
  height: 48,
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: 12,
  width: "100%",
}

// 검색바 스타일링
const SEARCH_BUTTON: ViewStyle = {
  height: 45,
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: 8,
  backgroundColor: color.surface.uiPressed,
  borderRadius: 12,
  flex: 1,
}

const PLACEHOLDER_SEARCH: TextStyle = {
  ...typography.ui.default,
  paddingHorizontal: 4,
  color: color.textColor.subdued,
}

const LEFT_AREA: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  marginRight: 16,
}
const RIGHT_AREA: ViewStyle = {
  flex: 1,
}

const ROTATE_180 = { transform: [{ rotate: "180deg" }] }
const hitSlop10 = {
  top: 10,
  left: 10,
  bottom: 10,
  right: 10,
}

function getImageSource(img: any): any {
  if (img == null || img == "") {
    return {
      uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png`,
      priority: FastImage.priority.high,
    }
  } else {
    return {
      uri: img,
      priority: FastImage.priority.high,
    }
  }
}

export const MemberSelectHeader: React.FunctionComponent<MemberSelectHeaderProps> = observer(
  (props) => {
    const { onSelectMember, hideNotice, navigation } = props
    // const { navigation } = this.props

    const [expandFamily, setExpandFamily] = useState(false)
    const { accountStore } = useStores()
    const { selectedMemberName, selectedMemberProfileImage } = accountStore
    const { width } = useWindowDimensions()

    const onSelectMemberInternal = (memberKey: string, member?: FamilyMember) => {
      setExpandFamily(false)
      onSelectMember(memberKey, member)
    }

    return (
      <View style={[HEADER, { height: expandFamily ? "100%" : 48 }]}>
        <View style={SELECTED_MEMBER_ROW}>
          <TouchableOpacity
            onPress={() => setExpandFamily(!expandFamily)}
            style={LEFT_AREA}
            activeOpacity={0.8}
            // hitSlop={hitSlop10}
          >
            <FastImage
              style={CIRCLE}
              source={getImageSource(selectedMemberProfileImage)}
              resizeMode={FastImage.resizeMode.cover}
            />
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={{ maxWidth: width - 180, ...SELECTED_MEMBER_NAME }}
            >
              {selectedMemberName}
            </Text>
            <SvgArrowDown width={24} height={24} style={expandFamily ? ROTATE_180 : null} />
          </TouchableOpacity>
          {/* {!hideNotice ? (
            <TouchableOpacity
              onPress={() => {
                amplitude.getInstance().logEvent("screen_view", { screen: "NoticeList" })
                RootNavigation.navigate("HomeStack", {
                  screen: "NoticeList",
                })
              }}
              hitSlop={hitSlop10}
            >
              <SvgNotice width={20.8} height={24.2} />
              {accountStore.isNoticeNew ? <View style={ORANGE_DOT} /> : null}
            </TouchableOpacity>
          ) : null} */}

          {/* 검색바 */}
          {remoteConfig().getBoolean("search_bar_on_home_enabled") && (
            <View style={RIGHT_AREA}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={SEARCH_BUTTON}
                onPress={() => {
                  sendLogEvent("press_search_home")
                  RootNavigation.navigate("Details", { screen: "Search" })
                }}
              >
                <SvgSearch width={24} height={24} />
                <Text style={PLACEHOLDER_SEARCH}>질병,약,병원 검색</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
        {expandFamily && <FamilyRow onSelect={onSelectMemberInternal} />}
      </View>
    )
  },
)
