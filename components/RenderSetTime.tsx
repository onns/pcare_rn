import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import LottieView from "lottie-react-native"
import { DateTime } from "luxon"
import { useObserver } from "mobx-react"
import { onAction } from "mobx-state-tree"
import React, { useEffect, useRef, useState } from "react"
import { Dimensions, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import Collapsible from "react-native-collapsible"
import ReactNativeHapticFeedback from "react-native-haptic-feedback"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { useNavigation } from "@react-navigation/native"

import { useStores } from "../App"
import SvgArrowDown from "../assets/images/arrowDown.svg"
import SvgPrescription from "../assets/images/ic_pictogram24px_onlinemedi.svg"
import SvgVideo from "../assets/images/video.svg"
import { Text } from "../components/StyledText"
import { formatTime } from "../lib/DateUtil"
import { DrugToTake } from "../stores/drug-to-take/DrugToTake"
import { DrugToTakeGroup } from "../stores/drug-to-take/DrugToTakeGroup"
import { color, styles, typography } from "../theme"
import TouchableItem from "./button/TouchableItem"
import { TakingItem } from "./TakingItem"

export interface RenterSetTimeProps {
  setTime: DrugToTakeGroup
}

// ----------------------------------------------------------------
// 아이템 컨테이너

const TAKING_ITEM_CONTAINER: ViewStyle = {
  marginBottom: 16,
  // borderRadius: 24,
  borderBottomWidth: 1,
  borderColor: color.borders.neutralSubded,
  shadowColor: color.surface.neutralDefault,
  shadowOffset: {
    width: 0,
    height: 16,
  },
  shadowOpacity: 1, //0.3
  shadowRadius: 0, //10
  backgroundColor: color.palette.white,
}
const END_ITEM_CONTAINER: ViewStyle = {
  ...TAKING_ITEM_CONTAINER,
}

// ----------------------------------------------------------------
// 알람 박스 영역

const ALARM_GUIDE_BOX_CONTAINER: ViewStyle = {
  paddingHorizontal: 16,
  paddingVertical: 16,
}
const ALARM_GUIDE_BOX: ViewStyle = {
  flex: 1,
  borderRadius: 16,
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: 16,
  paddingVertical: 12,
  backgroundColor: color.surface.neutralDefault,
}
const ALARM_GUIDE_TEXT: TextStyle = {
  ...typography.ui.default,
  flex: 1,
}
const ALARM_GUIDE_TEXT_ACCENT: TextStyle = {
  ...typography.ui.defaultEmphasized,
  color: color.textColor.primary,
}
const CHANGE_ALARM_TIME_TEXT: TextStyle = {
  ...typography.buttons.small,
  color: color.textColor.action,
}
const CHANGE_ALARM_TIME: ViewStyle = {
  // borderWidth: 1,
  borderColor: color.palette.salmonGray,
  borderRadius: 16,
  paddingHorizontal: 8,
  paddingVertical: 4,
}

/**
 * 현 instance 에 속한 약제들의 상태에 따라 "모두 복약" 버튼의 초기 상태를 결정하기 위해 사용.
 * 속한 약제들 모두 복용한 상태이면 allTaken 을 true 로 설정
 */

export const RenderSetTime: React.FunctionComponent<RenterSetTimeProps> = (props) => {
  const { setTime } = props
  const navigation = useNavigation()

  const renderContent = () => (
    <View>
      <View style={ALARM_GUIDE_BOX_CONTAINER}>
        <View style={ALARM_GUIDE_BOX}>
          <Text style={ALARM_GUIDE_TEXT}>
            <Text style={ALARM_GUIDE_TEXT_ACCENT}>
              {setTime.when}
              {/* {setTime === "wakeup" && <Text>tt</Text>}
              {setTime === "morning" && <Text>tt</Text>}
              {setTime === "afternoon" && <Text>tt</Text>}
              {setTime === "evening" && <Text>tt</Text>}
              {setTime === "night" && <Text>tt</Text>} */}
            </Text>{" "}
            에 복약하세요.
          </Text>

          <TouchableOpacity
            style={CHANGE_ALARM_TIME}
            onPress={() =>
              navigation.navigate("Details", {
                screen: "TimeSlotSettings",
              })
            }
          >
            <Text style={CHANGE_ALARM_TIME_TEXT}>시간변경</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )

  return <View>{renderContent()}</View>
}
