import React from "react"
import { Image, TextStyle, View, ViewStyle } from "react-native"
import { Button, Stack, XStack, YStack } from "tamagui"

import remoteConfig from "@react-native-firebase/remote-config"
import { useNavigation } from "@react-navigation/native"

import { color, typography } from "../theme"
import { Text } from "./StyledText"
import { SvgArrowRight } from "./svg/SvgArrowRight"

export interface NoPrescriptionBeingTakenProps {
  backgroundColor?: string
}

const CONTAINER: ViewStyle = {
  flexDirection: "column",
  justifyContent: "center",
  alignContent: "center",
  alignItems: "center",
  backgroundColor: color.palette.white,
  paddingVertical: 24,
  flex: 1,
  paddingTop: "40%",
}
const ROW_MARGIN: ViewStyle = {
  flexDirection: "row",
  marginTop: 10,
}
const DESCRIPTION_CONTAINER: ViewStyle = {
  flexDirection: "column",
  marginTop: 12,
}
const DESCRIPTION: TextStyle = {
  ...typography.ui.scaledUp,
  color: color.textColor.subdued,
  textAlign: "center",
}

export const NoPrescriptionBeingTaken: React.FunctionComponent<NoPrescriptionBeingTakenProps> = (
  props,
) => {
  const { backgroundColor } = props
  const pointsEnabled = remoteConfig().getBoolean("points_enabled")
  const navigation = useNavigation()

  if (pointsEnabled) {
    return (
      <YStack bc="white" ai="center" pt="25%">
        <Image
          source={require("../assets/images/pillbox.png")}
          style={{
            width: 205,
            height: 204,
          }}
        />
        <Text fow="700" fos={27} lh={35} ls={-0.5} col="#565E6D" ta="center">
          처방전이 있으신가요?
        </Text>
        <Text fow="500" fos={16} lh={18} ls={-0.3} col="#7F8694" mt={8}>
          <Text ff={typography.bold}>첫 처방전</Text> 등록시{" "}
          <Text ff={typography.bold}>1,000원 적립</Text>
        </Text>

        <XStack h={84} my={14} px="$4" pt={10} pb={26}>
          <Button
            theme="orange"
            bc={color.primary}
            w="100%"
            h={48}
            br={15}
            onPress={() => navigation.navigate("Details", { screen: "PrescriptionScanner" })}
          >
            <Text fow="500" fos={16} ls={-0.3} col="white">
              첫 처방전 등록 1,000원
            </Text>
          </Button>
        </XStack>

        <XStack
          ai="center"
          onPress={() => navigation.navigate("Details", { screen: "PointGuide" })}
        >
          <Text fow="700" fos={16} lh={18} ls={-0.3} col="#7F8694">
            건강 활동을 기록하고 포인트를 획득하세요
          </Text>
          <SvgArrowRight fill="#7F8694" />
        </XStack>
      </YStack>
    )
  }

  return (
    <View style={[CONTAINER, { backgroundColor: backgroundColor }]}>
      <Image
        source={require("../assets/images/ic_pictogram80px_emptyTask.png")}
        style={{
          width: 120,
          height: 120,
          // tintColor: color.palette.white,
        }}
      />

      <View style={DESCRIPTION_CONTAINER}>
        <Text style={DESCRIPTION}>현재 복약 중인 약이 없습니다.</Text>
        <View style={{ flexDirection: "row", alignItems: "center", marginTop: 4 }}>
          <Text style={DESCRIPTION}>화면 하단</Text>
          <Text style={{ ...typography.display.medium, color: color.palette.orange }}> + </Text>
          <Text style={DESCRIPTION}>버튼을 눌러 복약을 시작하세요.</Text>
        </View>
      </View>
    </View>
  )
}
