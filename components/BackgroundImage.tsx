import React from 'react';
import { View, Image } from 'react-native';
import FastImage from 'react-native-fast-image'

const BackgroundImage = (props: any) => {

  const { container, image } = styles;

  return (
    <View style={[ container, props.containerStyle ? props.containerStyle : null ]}>
      <FastImage
      style={[image, 
        { opacity: props.opacity}, props.image
      ]}  
      source={props.source}
      resizeMode={props.resizeMode}
      />
    </View>
  )
};

const styles = {
  container: {
    position: 'absolute',
    // top: 0,
    // left: 0,   
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {  
    // flex: 1,  
    width: '100%',
    height: '100%',
  }
};

export {BackgroundImage, FastImage};