import * as React from "react"

const SvgComponent = (props) => (
  <svg width={24} height={24} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path
      clipRule="evenodd"
      d="M16.747 10.357a6.392 6.392 0 1 1-12.786 0 6.392 6.392 0 0 1 12.787 0h0Z"
      stroke="#2F3233"
      strokeWidth={1.6}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="m20.12 20.121-5.25-5.25"
      stroke="#2F3233"
      strokeWidth={1.6}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export default SvgComponent
