import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import FastImage from "react-native-fast-image"

import SvgAlert16 from "../assets/images/alert16.svg"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import SvgCalendar from "../assets/images/calendar.svg"
import SvgDrugEmptyImage from "../assets/images/drugEmptyImage.svg"
import SvgMedicine from "../assets/images/medicine.svg"
import { Text } from "../components/StyledText"
import { getText, getTimeText } from "../components/TimeSlotRow"
import { formatDateFromString, formatTime } from "../lib/DateUtil"
import { AccountStore } from "../stores/AccountStore"
import { Dur } from "../stores/Dur"
import { PrescriptionDetail, PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"

export interface Props {
  prescriptionDetail: PrescriptionDetail
  onPress?: () => void
  prescriptionStore?: PrescriptionStore
  accountStore?: AccountStore
  style?: ViewStyle
  diffDays: number
  durRes: Dur
  enableDur?: boolean
}

interface State {
  oneLiner: string
  existDurContents: boolean
  expandingDur: boolean
  selectedDurType: string
  durAttributeTitle: string
  durAttributeDescription: string
  durAttributeCount: number
  durPregIndex: number
  durBygIndex: number
  durHngIndex: number
  durDisIndex: number
  durNosaleIndex: number
  durTitle: string
}
const DOSE_INFO_TEXT = { ...typography.sub2, paddingLeft: 8, marginBottom: 8 }
const FULL = { flex: 1 }
const DRUG_ONE_LINER = { paddingLeft: 8, paddingRight: 16, marginBottom: 8 }

const ALARM_BOX: ViewStyle = {
  backgroundColor: color.palette.gray5,
  borderRadius: 1,
  borderWidth: 1,
  borderColor: color.palette.pale,
  paddingHorizontal: 7,
  paddingTop: 2,
  paddingBottom: 3,
  marginLeft: 8,
  marginBottom: 8,
}
const ALARM_TIME: TextStyle = { ...typography.sub2, opacity: 0.7 }

const MEDICATION_PERIOD = {
  ...typography.sub2,
  color: color.palette.grey50,
  marginBottom: 4,
}
const DOSING_DAYS = {
  ...typography.sub2,
  color: color.palette.black,
}
const DRUG_ITEM: ViewStyle = {
  backgroundColor: "#fff",
  paddingTop: 16,
  paddingBottom: 16,
}
const DRUG_BUTTON: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingLeft: 16,
  paddingTop: 16,
  paddingRight: 8,
  marginBottom: 4,
}
const DRUG_BUTTON_RIGHT = {
  flex: 1,
  marginLeft: 8,
  marginBottom: 4,
}
const DRUG_IMAGE: ViewStyle = {
  width: 84,
  height: 48,
  borderRadius: 5,
}
const ROW_VERT_CENTER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const DRUG_NAME = {
  flex: 1,
  ...typography.title3,
  paddingRight: 16,
  // backgroundColor: "gray"
}
const ONE_LINER = {
  ...typography.body,
  paddingLeft: 16,
  paddingRight: 19,
}
const PROGRESS_BAR_ROW: ViewStyle = {
  ...ROW_VERT_CENTER,
  paddingLeft: 55,
  paddingRight: 12,
  marginTop: 6,
  marginBottom: 10,
}
const DOSING_TABLE: ViewStyle = {
  ...ROW_VERT_CENTER,
  height: 68,
  borderWidth: 1,
  borderColor: color.palette.grey2,
  marginLeft: 20,
  marginRight: 12,
  borderRadius: 4,
}
const DOSING_VALUE = {
  ...typography.sub2,
  opacity: 0.7,
}
const DOSING_ATTRIBUTE = {
  fontFamily: typography.primary,
  fontSize: 11,
  lineHeight: 18,
  color: color.palette.grey1,
  marginBottom: 4,
}
const TOTAL_DOSING_NUM = {
  fontFamily: typography.primary,
  fontSize: 13,
  lineHeight: 18,
  color: color.palette.brownGrey,
}
const REMAIN_NUM = {
  fontFamily: typography.primary,
  fontSize: 13,
  lineHeight: 18,
  color: color.text,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const SLIDER = {
  flex: 1,
  height: 6,
  margin: 0,
  padding: 0,
  marginRight: 13,
}
const SLIDER_TRACK = {
  height: 6,
  borderRadius: 10,
  backgroundColor: "rgb(228, 228, 228)",
}
const DOSING_TABLE_CELL: ViewStyle = {
  height: 44,
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
}
const LEFT_BORDER = {
  borderLeftWidth: 1,
  borderLeftColor: color.palette.grey2,
}
const TRIANGLE: ViewStyle = {
  height: 0,
  width: 0,
  borderStyle: "solid",
  borderLeftWidth: 8,
  borderRightWidth: 8,
  borderBottomWidth: 8,
  borderLeftColor: "transparent",
  borderRightColor: "transparent",
  borderBottomColor: color.primary,
  left: 0,
  top: 1,
  zIndex: 1,
  position: "relative",
}
const DOWN = { transform: [{ rotate: "180deg" }] }

const DUR_DESCRIPTION: TextStyle = { fontFamily: typography.primary, fontSize: 15, lineHeight: 23 }
const DUR_SUBTITLE = { fontFamily: typography.primary, lineHeight: 23, color: "#000" }
const DUR_DRUG_NAME = { color: color.palette.darkSkyBlue }
const DUR_ATTR_TITLE = { fontFamily: typography.primary, marginTop: 13, marginBottom: 5 }
const DUR_ATTR_DESCRIPTION = {
  fontFamily: typography.primary,
  fontSize: 13,
  lineHeight: 19.5,
  color: "rgb(132, 132, 132)",
}
const DUR_LINE = { height: 1, backgroundColor: color.line, marginVertical: 15 }

const pregTitle = "임부금기"
const pregDescription =
  "임신 또는 임신하고 있을 가능성이 있는 환자가 복용하면 태아기형 및 태아독성 등 태아에 대한 위험성이 있는 약물"
const nosaleTitle = "사용중지"
const nosaleDescription =
  "해외 또는 국내에서의 약물 부작용보고, 품질부적합 등으로 사용이 중지된 약물"
const bygTitle = "병용금기"
const bygDescription =
  "두개 이상의 약물을 같이 복용했을 때 예상하지 못한 부작용이 나타나거나 치료 효과가 떨어지는 약물"
const disTitle = "동일성분중복"
const disDescription = "약의 효능효과, 성분이 동일한 약물이 2가지 이상 있는 경우"
const hngTitle = "효능군중복"
const hngDescription = "약의 성분은 다르나 효능이 동일한 약물이 2가지 이상 있는 경우"

const DUR_ATTR_BUTTON = {
  backgroundColor: color.primary,
  borderRadius: 18,
  paddingHorizontal: 16,
  paddingTop: 10,
  paddingBottom: 7,
  marginRight: 10,
  marginBottom: 7,
}
const DUR_ATTR_BUTTON_TEXT = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 19,
  color: "#fff",
}
const DUR_ATTR_BUTTON_UNSELECTED_TEXT = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 19,
  color: color.primary,
}
const DUR_ATTR_UNSELECTED_BUTTON = {
  borderWidth: 1,
  borderColor: color.primary,
  borderRadius: 18,
  paddingHorizontal: 16,
  paddingTop: 10,
  paddingBottom: 7,
  marginRight: 10,
  marginBottom: 7,
}
const DOSING_VALUE_BOX: ViewStyle = {
  borderRadius: 1,
  borderWidth: 1,
  borderColor: color.palette.pale,
  backgroundColor: color.palette.gray5,
  paddingHorizontal: 10,
  paddingTop: 3,
  paddingBottom: 2,
  marginRight: 4,
}
const DUR_SHOW_BUTTON_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  lineHeight: 20,
  color: color.text,
}

@inject("prescriptionStore", "accountStore")
@observer
export default class PrescriptionDetailItem extends Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      oneLiner: "",
      existDurContents: false,
      expandingDur: false,
      selectedDurType: "preg",
      durAttributeTitle: "",
      durAttributeDescription: "",
      durAttributeCount: 0,
      durPregIndex: -1,
      durBygIndex: -1,
      durDisIndex: -1,
      durHngIndex: -1,
      durNosaleIndex: -1,
      durTitle: "",
    }
  }

  UNSAFE_componentWillMount() {
    if (this.props.enableDur) {
      this.parseMyDur()
    }
  }

  parseMyDur() {
    const { accountStore, durRes, prescriptionDetail, prescriptionStore } = this.props
    const { user } = accountStore
    const { dosingEnded } = prescriptionStore
    if (!durRes) {
      return
    }

    const { drug } = prescriptionDetail
    let existDurPreg = false
    let existDurByg = false
    let existDurDis = false
    let existDurHng = false
    let existDurNosale = false

    let durAttributeCount = 0
    if (durRes.preg.length > 0 && user.dur_preg === "y") {
      for (let i = 0; i < durRes.preg.length; i++) {
        const element = durRes.preg[i]
        if (element.drug_id == drug.id) {
          durAttributeCount = durAttributeCount + 1
          this.setState({
            existDurContents: true,
            selectedDurType: "preg",
            // durPregIndex: 0,
            durPregIndex: i,
            durAttributeTitle: pregTitle,
            durAttributeDescription: pregDescription,
            durAttributeCount: durAttributeCount,
          })
          existDurPreg = true
        }
      }
    }
    if (durRes.nosale.length > 0 && user.dur_nosale === "y") {
      for (let i = 0; i < durRes.nosale.length; i++) {
        const element = durRes.nosale[i]
        if (element.drug_id == drug.id) {
          durAttributeCount = durAttributeCount + 1
          this.setState({
            existDurContents: true,
            selectedDurType: "nosale",
            durNosaleIndex: i,
            durAttributeTitle: nosaleTitle,
            durAttributeDescription: nosaleDescription,
            durAttributeCount: durAttributeCount,
          })
          existDurNosale = true
        }
      }
    }
    if (durRes.dis.length > 0 && user.dur_dis === "y") {
      for (let i = 0; i < durRes.dis.length; i++) {
        const element = durRes.dis[i]
        let beingTakingDrug = true
        dosingEnded.some((prescription) => {
          if (prescription.id == element.presc1_id || prescription.id == element.presc2_id) {
            beingTakingDrug = false
          }
          return !beingTakingDrug
        })

        if ((element.drug1_id == drug.id || element.drug2_id == drug.id) && beingTakingDrug) {
          durAttributeCount = durAttributeCount + 1
          this.setState({
            existDurContents: true,
            selectedDurType: "dis",
            durDisIndex: i,
            // durDisIndex: 0,
            durAttributeTitle: disTitle,
            durAttributeDescription: disDescription,
            durAttributeCount: durAttributeCount,
          })
          existDurDis = true
        }
      }
    }
    if (durRes.hng.length > 0 && user.dur_hng === "y") {
      for (let i = 0; i < durRes.hng.length; i++) {
        const element = durRes.hng[i]
        let beingTakingDrug = true
        dosingEnded.some((prescription) => {
          if (prescription.id == element.presc1_id || prescription.id == element.presc2_id) {
            beingTakingDrug = false
          }
          return !beingTakingDrug
        })

        if ((element.drug1_id == drug.id || element.drug2_id == drug.id) && beingTakingDrug) {
          durAttributeCount = durAttributeCount + 1
          this.setState({
            existDurContents: true,
            selectedDurType: "hng",
            // durHngIndex: 0,
            durHngIndex: i,
            durAttributeTitle: hngTitle,
            durAttributeDescription: hngDescription,
            durAttributeCount: durAttributeCount,
          })
          existDurHng = true
        }
      }
    }
    if (durRes.byg.length > 0 && user.dur_byg === "y") {
      for (let i = 0; i < durRes.byg.length; i++) {
        const element = durRes.byg[i]
        let beingTakingDrug = true
        dosingEnded.some((prescription) => {
          if (prescription.id == element.presc1_id || prescription.id == element.presc2_id) {
            beingTakingDrug = false
          }
          return !beingTakingDrug
        })

        if ((element.drug1_id == drug.id || element.drug2_id == drug.id) && beingTakingDrug) {
          durAttributeCount = durAttributeCount + 1
          this.setState({
            existDurContents: true,
            selectedDurType: "byg",
            durBygIndex: i,
            // durBygIndex: 0,
            durAttributeTitle: bygTitle,
            durAttributeDescription: bygDescription,
            durAttributeCount: durAttributeCount,
          })
          existDurByg = true
        }
      }
    }

    let durCount = 0
    const durTitleArray = []
    if (existDurNosale) {
      switch (durCount) {
        case 0:
        case 1:
          durTitleArray.push("사용중지")
          break
        default:
          break
      }
      durCount = durCount + 1
    }
    if (existDurByg) {
      switch (durCount) {
        case 0:
        case 1:
          durTitleArray.push("병용금기")
          break
        default:
          break
      }
      durCount = durCount + 1
    }
    if (existDurHng) {
      switch (durCount) {
        case 0:
        case 1:
          durTitleArray.push("효능군중복")
          break
        default:
          break
      }
      durCount = durCount + 1
    }
    if (existDurDis) {
      switch (durCount) {
        case 0:
        case 1:
          durTitleArray.push("동일성분중복")
          break
        default:
          break
      }
      durCount = durCount + 1
    }
    if (existDurPreg) {
      switch (durCount) {
        case 0:
        case 1:
          durTitleArray.push("임부금기")
          break
        default:
          break
      }
      durCount = durCount + 1
    }

    let durTitle = durTitleArray.join(", ")

    if (durCount == 1 || 2) {
      durTitle = `${durTitle} 주의사항`
    } else if (durCount > 2) {
      durTitle = `${durTitle} 외 ${durCount - 2}가지 주의사항`
    }

    this.setState({ durTitle: durTitle })
  }

  render() {
    // grab the props
    const { accountStore, prescriptionDetail, onPress, style: styleOverride, durRes } = this.props

    const totalDosingNum = Math.round(
      (prescriptionDetail.dosing_days_num / prescriptionDetail.per_days) *
        prescriptionDetail.doses_num,
    )
    const resultDays = prescriptionDetail.dosing_days_num - this.props.diffDays
    let remainNum = 0
    if (resultDays > 0) {
      remainNum = Math.round(
        ((prescriptionDetail.dosing_days_num - this.props.diffDays) / prescriptionDetail.per_days) *
          prescriptionDetail.doses_num,
      )
    }

    const unitConverted = prescriptionDetail.drug.unitConverted

    return (
      <View style={[DRUG_ITEM, styleOverride]}>
        <TouchableOpacity style={DRUG_BUTTON} onPress={onPress}>
          {/* {this.props.accountStore.user.dur_noti === "y" &&
          ((this.state.durBygIndex != -1 && this.props.accountStore.user.dur_byg === "y") ||
            (this.state.durPregIndex != -1 && this.props.accountStore.user.dur_preg === "y") ||
            (this.state.durHngIndex != -1 && this.props.accountStore.user.dur_hng === "y") ||
            (this.state.durDisIndex != -1 && this.props.accountStore.user.dur_dis === "y") ||
            (this.state.durNosaleIndex != -1 && this.props.accountStore.user.dur_nosale === "y")) &&
          this.props.diffDays < 31 // 처방전 복용완료 시점 1개월 이후에는 warning mark 해제
            ? this.renderDrugIcon(unitConverted, true)
            : this.renderDrugIcon(unitConverted)} */}
          {prescriptionDetail.drug.image_url ? (
            <FastImage source={{ uri: prescriptionDetail.drug.image_url }} style={DRUG_IMAGE} />
          ) : (
            <SvgDrugEmptyImage width={84} height={48} style={{ alignSelf: "flex-start" }} />
          )}
          <View style={DRUG_BUTTON_RIGHT}>
            <View style={ROW}>
              <Text style={DRUG_NAME} numberOfLines={1} ellipsizeMode="tail">
                {prescriptionDetail.drug.shortenedName}
              </Text>
              <SvgArrowRight width={24} height={24} />
            </View>
            <View style={ROW}>
              <SvgCalendar width={16} height={16} style={{ marginTop: 1, marginRight: 4 }} />
              <Text style={MEDICATION_PERIOD}>
                {prescriptionDetail.start_at
                  ? `${prescriptionDetail.start_at.toFormat(
                      "yyyy.MM.dd.",
                    )} ~ ${prescriptionDetail.end_at.toFormat("yyyy.MM.dd.")} | `
                  : null}
                <Text style={DOSING_DAYS}>총{prescriptionDetail.dosing_days_num}일</Text>
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        {/* {totalDosingNum != 0 ? (
          <View style={PROGRESS_BAR_ROW}>
            <Slider
              disabled
              value={(totalDosingNum - remainNum) / totalDosingNum}
              style={SLIDER}
              trackStyle={SLIDER_TRACK}
              thumbStyle={{ width: 0 }}
              minimumTrackTintColor={
                (totalDosingNum - remainNum) / totalDosingNum == 1
                  ? "#A8A8A8"
                  : color.palette.brightCyan
              }
            />
            <Text style={TOTAL_DOSING_NUM}>총{totalDosingNum}회 / </Text>
            <Text style={REMAIN_NUM}>{remainNum}회남음</Text>
          </View>
        ) : (
          <View style={{ marginBottom: 5 }} />
        )} */}
        <View style={{ ...ROW, paddingLeft: 16 }}>
          <SvgMedicine width={84} height={24} />
          <View style={FULL}>
            <Text style={DOSE_INFO_TEXT}>
              {prescriptionDetail.dose}
              {prescriptionDetail.drug.unitConverted} | 하루 {prescriptionDetail.doses_num}번 |{" "}
              {prescriptionDetail.per_days == 1 ? "매일" : `${prescriptionDetail.per_days}일간격`}
            </Text>
            <View style={{ ...ROW, flexWrap: "wrap" }}>
              {prescriptionDetail.timeslots.map((timeSlot, index) => (
                <View key={index} style={ALARM_BOX}>
                  <Text style={ALARM_TIME}>
                    {timeSlot.name == "custom" ? formatTime(timeSlot.when) : getText(timeSlot.name)}
                  </Text>
                </View>
              ))}
            </View>
            {prescriptionDetail.drug.one_liner ? (
              <View style={DRUG_ONE_LINER}>
                <Text style={typography.body}>{prescriptionDetail.drug.one_liner}</Text>
              </View>
            ) : null}
          </View>
        </View>
        {this.state.existDurContents && accountStore.user.dur_noti === "y" ? (
          <View
            style={{
              backgroundColor: color.palette.veryLightPink,
              borderRadius: 1,
              marginTop: 16,
              marginLeft: 16,
              marginRight: 16,
              paddingLeft: 8,
              paddingRight: 16,
              paddingBottom: 8,
              paddingTop: 8,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                amplitude
                  .getInstance()
                  .logEvent("press DUR detail", { dur_title: this.state.durTitle })
                this.setState({ expandingDur: !this.state.expandingDur })
              }}
              style={{ flexDirection: "row", alignItems: "center" }}
              hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
            >
              <SvgAlert16 width={16} height={16} style={{ marginRight: 4 }} />
              <Text
                style={{
                  ...typography.sub1,
                  color: color.palette.copper,
                  flex: 1,
                }}
                ellipsizeMode="middle"
                numberOfLines={1}
              >
                {this.state.durTitle}
              </Text>
              <Text style={DUR_SHOW_BUTTON_TEXT}>보기</Text>
            </TouchableOpacity>
            {this.state.expandingDur ? (
              <View>
                <View style={{ height: 1, backgroundColor: color.line, marginTop: 15 }} />
                {this.state.durAttributeCount > 1 ? (
                  <View style={{ flexDirection: "row", flexWrap: "wrap", marginTop: 15 }}>
                    {this.state.durBygIndex != -1 ? (
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({
                            selectedDurType: "byg",
                            durAttributeTitle: bygTitle,
                            durAttributeDescription: bygDescription,
                          })
                        }
                        style={
                          this.state.selectedDurType === "byg"
                            ? DUR_ATTR_BUTTON
                            : DUR_ATTR_UNSELECTED_BUTTON
                        }
                      >
                        <Text
                          style={
                            this.state.selectedDurType === "byg"
                              ? DUR_ATTR_BUTTON_TEXT
                              : DUR_ATTR_BUTTON_UNSELECTED_TEXT
                          }
                        >
                          병용금기
                        </Text>
                      </TouchableOpacity>
                    ) : null}
                    {this.state.durHngIndex != -1 ? (
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({
                            selectedDurType: "hng",
                            durAttributeTitle: hngTitle,
                            durAttributeDescription: hngDescription,
                          })
                        }
                        style={
                          this.state.selectedDurType === "hng"
                            ? DUR_ATTR_BUTTON
                            : DUR_ATTR_UNSELECTED_BUTTON
                        }
                      >
                        <Text
                          style={
                            this.state.selectedDurType === "hng"
                              ? DUR_ATTR_BUTTON_TEXT
                              : DUR_ATTR_BUTTON_UNSELECTED_TEXT
                          }
                        >
                          효능군중복
                        </Text>
                      </TouchableOpacity>
                    ) : null}
                    {this.state.durDisIndex != -1 ? (
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({
                            selectedDurType: "dis",
                            durAttributeTitle: disTitle,
                            durAttributeDescription: disDescription,
                          })
                        }
                        style={
                          this.state.selectedDurType === "dis"
                            ? DUR_ATTR_BUTTON
                            : DUR_ATTR_UNSELECTED_BUTTON
                        }
                      >
                        <Text
                          style={
                            this.state.selectedDurType === "dis"
                              ? DUR_ATTR_BUTTON_TEXT
                              : DUR_ATTR_BUTTON_UNSELECTED_TEXT
                          }
                        >
                          동일성분중복
                        </Text>
                      </TouchableOpacity>
                    ) : null}
                    {this.state.durPregIndex != -1 ? (
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({
                            selectedDurType: "preg",
                            durAttributeTitle: pregTitle,
                            durAttributeDescription: pregDescription,
                          })
                        }
                        style={
                          this.state.selectedDurType === "preg"
                            ? DUR_ATTR_BUTTON
                            : DUR_ATTR_UNSELECTED_BUTTON
                        }
                      >
                        <Text
                          style={
                            this.state.selectedDurType === "preg"
                              ? DUR_ATTR_BUTTON_TEXT
                              : DUR_ATTR_BUTTON_UNSELECTED_TEXT
                          }
                        >
                          임부금기
                        </Text>
                      </TouchableOpacity>
                    ) : null}
                    {this.state.durNosaleIndex != -1 ? (
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({
                            selectedDurType: "nosale",
                            durAttributeTitle: nosaleTitle,
                            durAttributeDescription: nosaleDescription,
                          })
                        }
                        style={
                          this.state.selectedDurType === "nosale"
                            ? DUR_ATTR_BUTTON
                            : DUR_ATTR_UNSELECTED_BUTTON
                        }
                      >
                        <Text
                          style={
                            this.state.selectedDurType === "nosale"
                              ? DUR_ATTR_BUTTON_TEXT
                              : DUR_ATTR_BUTTON_UNSELECTED_TEXT
                          }
                        >
                          사용중지
                        </Text>
                      </TouchableOpacity>
                    ) : null}
                  </View>
                ) : null}
                <Text style={DUR_ATTR_TITLE}>
                  {this.state.durAttributeTitle}{" "}
                  {this.state.selectedDurType === "preg"
                    ? durRes.preg[this.state.durPregIndex].preg_grade + "등급"
                    : ""}
                </Text>
                <Text style={DUR_ATTR_DESCRIPTION}>{this.state.durAttributeDescription}</Text>
                {this.state.selectedDurType === "byg" ? this.getDurBygView() : null}
                {this.state.selectedDurType === "dis" ? this.getDurDisView() : null}
                {this.state.selectedDurType === "hng" ? this.getDurHngView() : null}
                {this.state.selectedDurType === "preg" ? (
                  <View>
                    <View style={DUR_LINE} />
                    <Text style={DUR_DESCRIPTION}>
                      <Text style={DUR_SUBTITLE}>
                        {durRes.preg[this.state.durPregIndex].preg_grade}등급 금기{" "}
                      </Text>
                      약물입니다. {durRes.preg[this.state.durPregIndex].preg_detail}
                    </Text>
                  </View>
                ) : null}
                {this.state.selectedDurType === "nosale" ? this.getDurNosaleView() : null}
              </View>
            ) : null}
          </View>
        ) : null}
      </View>
      // <TouchableOpacity style={DRUG_ITEM}
      //   onPress={onPress}>
      //   <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 4 }}>
      //     <View style={{ flex: 2, flexDirection: 'row'}}>
      //       <Image source={prescriptionDetail.unitConverted === "병" ? drugBottleSource : drugTabSource } style={DRUG_ICON} />
      //       <Text numberOfLines={1} style={{ fontSize: 15, fontWeight: "bold", color: "#000" }}>{prescriptionDetail.drug.name}</Text>
      //     </View>
      //     <View style={{ flex: 3, flexDirection: 'row', alignItems: 'center' }}>
      //       <SvgArrowRight width={24} height={24} color={color.palette.gray3}
      //         style={{ flex: 1.8, fontSize: 24, color: prescriptionDetail.drug.one_liner && prescriptionDetail.drug.one_liner.length > 0 ? color.primary : '#fff' }} />
      //       <Text style={{ flex: 3.6, fontSize: 15, color: color.text, textAlign: 'center' }}>{prescriptionDetail.dose}{prescriptionDetail.unitConverted}</Text>
      //       <Text style={{ flex: 3.8, fontSize: 15, color: color.text, textAlign: 'center' }}>{ prescriptionDetail.per_days + '일' + prescriptionDetail.doses_num + '회' }</Text>
      //       <Text style={{ flex: 3.4, fontSize: 15, color: color.text, textAlign: 'center' }}>{ prescriptionDetail.dosing_days_num + '일' }</Text>
      //     </View>
      //   </View>
      //   <Text numberOfLines={1} style={{ fontSize: 13, color: 'rgb(132, 132, 132)' }}>
      //   {
      //     prescriptionDetail.drug.one_liner && prescriptionDetail.drug.one_liner.length > 0 ? prescriptionDetail.drug.one_liner : "약제 설명을 준비 중입니다."
      //   }
      //     </Text>
      // </TouchableOpacity>
    )
  }

  /*
  renderDrugIcon(unitConverted: string, durWarning?: boolean) {
    let source
    switch (unitConverted) {
      case "병":
      case "통":
      case "건식":
      case "건강기능식품":
      case "일반식품":
        source = durWarning ? drugBottleDurSource : drugBottleSource
        break

      case "캡슐":
        source = durWarning ? drugCapsuleDurSource : drugCapsuleSource
        break

      case "액상":
      case "ml":
      case "l":
      case "cc":
      case "시럽":
      case "연고":
        source = durWarning ? drugLiquidDurSource : drugLiquidSource
        break

      case "가루":
      case "mg":
      case "g":
      case "건조시럽":
      case "과립":
        source = durWarning ? drugPowderDurSource : drugPowderSource
        break

      case "팩":
      case "포":
        source = durWarning ? drugPackDurSource : drugPackSource
        break

      case "매":
        source = durWarning ? drugMeaDurSource : drugMeaSource
        break

      case "스프레이":
        source = durWarning ? drugSprayDurSource : drugSpraySource
        break

      case "패치":
      case "패취":
        source = durWarning ? drugPatchDurSource : drugPatchSource
        break

      case "주사제":
      case "주사":
        source = durWarning ? drugInjectionDurSource : drugInjectionSource
        break

      default:
        source = durWarning ? drugTabDurSource : drugTabSource
        break
    }

    return <Image style={DRUG_ICON} source={source} />
  } */

  getDurDisView(): React.ReactNode {
    const curDis = this.props.durRes.dis[this.state.durDisIndex]
    const drugInfo = this.getDurDrugInfo(
      curDis.presc1_id,
      curDis.presc2_id,
      curDis.detail1_id,
      curDis.detail2_id,
      curDis.drug1_name,
      curDis.drug2_name,
    )

    return (
      <View>
        <View style={DUR_LINE} />
        <Text style={DUR_DESCRIPTION}>
          <Text style={DUR_SUBTITLE}>[{drugInfo.prescriptionInfo}] </Text>
          <Text style={DUR_DRUG_NAME}>{drugInfo.drugName}</Text> 와(과) 동일성분 중복입니다.
        </Text>
      </View>
    )
  }

  getDurHngView(): React.ReactNode {
    const curHng = this.props.durRes.hng[this.state.durHngIndex]
    const drugInfo = this.getDurDrugInfo(
      curHng.presc1_id,
      curHng.presc2_id,
      curHng.detail1_id,
      curHng.detail2_id,
      curHng.drug1_name,
      curHng.drug2_name,
    )

    return (
      <View>
        <View style={DUR_LINE} />
        <Text style={DUR_DESCRIPTION}>
          <Text style={DUR_SUBTITLE}>[{drugInfo.prescriptionInfo}] </Text>
          <Text style={DUR_DRUG_NAME}>{drugInfo.drugName}</Text> 와(과) 효능군 중복(
          {curHng.hng_group})입니다.
        </Text>
      </View>
    )
  }

  getDurBygView(): React.ReactNode {
    const curByg = this.props.durRes.byg[this.state.durBygIndex]
    const drugInfo = this.getDurDrugInfo(
      curByg.presc1_id,
      curByg.presc2_id,
      curByg.detail1_id,
      curByg.detail2_id,
      curByg.drug1_name,
      curByg.drug2_name,
    )

    return (
      <View>
        <View style={DUR_LINE} />
        <Text style={DUR_DESCRIPTION}>
          <Text style={DUR_SUBTITLE}>[{drugInfo.prescriptionInfo}] </Text>
          <Text style={DUR_DRUG_NAME}>{drugInfo.drugName}</Text> 와(과) 병용금기입니다.{" "}
          {curByg.side_effect}
        </Text>
      </View>
    )
  }

  getDurNosaleView(): React.ReactNode {
    const curNosale = this.props.durRes.nosale[this.state.durNosaleIndex]
    return (
      <View>
        <View style={DUR_LINE} />
        <Text style={DUR_DESCRIPTION}>{curNosale.group}</Text>
      </View>
    )
  }

  getDurDrugInfo(
    presc1_id: number,
    presc2_id: number,
    detail1_id: number,
    detail2_id: number,
    drug1_name: string,
    drug2_name: string,
  ): { prescriptionInfo: string; drugName: string } {
    const store = this.props.prescriptionStore
    const curPrescriptionId = this.props.prescriptionDetail.prescription
    const prescriptions = store.prescriptions.get(store.selectedFamilyMemberKey)
    const pDetail = this.props.prescriptionDetail

    for (let i = 0; i < prescriptions.length; i++) {
      const prescription = prescriptions[i]

      if (prescription.id == presc1_id && curPrescriptionId == presc2_id) {
        if (presc1_id == presc2_id) {
          if (detail1_id == pDetail.id) {
            return {
              prescriptionInfo:
                (prescription.disease.length > 0
                  ? prescription.disease[0].feature
                  : "질병정보 없음") +
                ", " +
                formatDateFromString(prescription.issue_date),
              drugName: drug2_name,
            }
          } else {
            return {
              prescriptionInfo:
                (prescription.disease.length > 0
                  ? prescription.disease[0].feature
                  : "질병정보 없음") +
                ", " +
                formatDateFromString(prescription.issue_date),
              drugName: drug1_name,
            }
          }
        }

        if (prescription.disease.length > 0) {
          return {
            prescriptionInfo:
              prescription.disease[0].feature +
              ", " +
              formatDateFromString(prescription.issue_date),
            drugName: drug1_name,
          }
        } else {
          return {
            prescriptionInfo: "질병정보 없음, " + formatDateFromString(prescription.issue_date),
            drugName: drug1_name,
          }
        }
      } else if (prescription.id == presc2_id && curPrescriptionId == presc1_id) {
        if (prescription.disease.length > 0) {
          return {
            prescriptionInfo:
              prescription.disease[0].feature +
              ", " +
              formatDateFromString(prescription.issue_date),
            drugName: drug2_name,
          }
        } else {
          return {
            prescriptionInfo: "질병정보 없음, " + formatDateFromString(prescription.issue_date),
            drugName: drug2_name,
          }
        }
      }
    }
    return { prescriptionInfo: "처방전정보 오류", drugName: "약제정보 없음" }
  }
}
