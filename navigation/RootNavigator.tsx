import React from "react"

import {
  NavigationContainer,
  NavigationContainerRef,
  NavigatorScreenParams,
} from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"

import AuthLoadingScreen from "../screens/AuthLoadingScreen"
import { FirstUseGuideScreen } from "../screens/FirstUseGuideScreen"
import { AuthNavigator, AuthStackParamList } from "./AuthNavigator"
import { DetailsNavigator, DetailsStackParamList } from "./DetailsNavigator"
import { HealthTemplateNavigator, HealthTemplateStackParamList } from "./HealthTemplateNavigator"
import { MainTabNavigator, MainTabParamList } from "./MainTabNavigator"

export type RootStackParamList = {
  AuthLoading: undefined
  Auth: NavigatorScreenParams<AuthStackParamList>
  Main: NavigatorScreenParams<MainTabParamList>
  Details: NavigatorScreenParams<DetailsStackParamList>
  HealthTemplates: NavigatorScreenParams<HealthTemplateStackParamList>
  FirstUseGuide: undefined
}

const Stack = createStackNavigator<RootStackParamList>()

function AppNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        // presentation: "modal",
      }}
    >
      <Stack.Screen
        name="AuthLoading"
        component={AuthLoadingScreen}
        options={() => ({ gestureEnabled: false })}
      />
      <Stack.Screen name="Main" component={MainTabNavigator} options={{ gestureEnabled: false }} />
      <Stack.Screen name="Auth" component={AuthNavigator} options={{ gestureEnabled: false }} />
      <Stack.Screen
        name="HealthTemplates"
        component={HealthTemplateNavigator}
        options={{ gestureEnabled: false }}
      />
      <Stack.Screen name="Details" component={DetailsNavigator} />
      <Stack.Screen
        name="FirstUseGuide"
        component={FirstUseGuideScreen}
        options={{ gestureEnabled: false }}
      />
    </Stack.Navigator>
  )
}

export const RootNavigator = React.forwardRef<
  NavigationContainerRef,
  Partial<React.ComponentProps<typeof NavigationContainer>>
>((props, ref) => {
  return (
    <NavigationContainer {...props} ref={ref}>
      <AppNavigator />
    </NavigationContainer>
  )
})

RootNavigator.displayName = "RootNavigator"
