import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import CertificationScreen from "../screens/CertificationScreen"
import PrescriptionPreviewScreen from "../screens/PrescriptionPreviewScreen"
import PrescriptionScannerScreen from "../screens/PrescriptionScannerScreen"
import { styles, typography } from "../theme"

export type AddPrescriptionStackParamList = {
  PrescriptionScanner: {
    rePhotoIndex: number
    rePhotoId: number
    handleBackPress: () => void
  }
  PrescriptionPreview: {
    uri: string
    type?: string
    name?: string
    ocrMessage?: any
    rePhotoIndex?: any
    rePhotoId?: any
  }
  Certification: {
    previousScreen: string
  }
}

const Stack = createStackNavigator<AddPrescriptionStackParamList>()

const SCANNER_HEADER = {
  backgroundColor: "rgb(52, 52, 52)",
  borderBottomWidth: 0,
  elevation: 0,
  shadowColor: "transparent",
}
export function AddPrescriptionNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitleAlign: "center",
        headerBackTitle: null,
        headerBackImage: () => <CustomHeaderBackImage />,
        headerStyle: styles.HEADER,
        headerTitleStyle: {
          fontFamily: typography.primary,
          fontWeight: "bold",
          fontSize: 20,
          color: "#fff",
        },
        headerTruncatedBackTitle: "",
      }}
    >
      <Stack.Screen
        name="PrescriptionScanner"
        component={PrescriptionScannerScreen}
        options={({ route }) => {
          return {
            title: "처방전/약봉투 촬영",
            headerStyle: SCANNER_HEADER,
            headerLeft: null,
          }
        }}
      />
      <Stack.Screen
        name="PrescriptionPreview"
        component={PrescriptionPreviewScreen}
        options={PrescriptionPreviewScreen.navigationOptions}
      />
      <Stack.Screen
        name="Certification"
        component={CertificationScreen}
        options={CertificationScreen.navigationOptions}
      />
    </Stack.Navigator>
  )
}
