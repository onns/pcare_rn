import React from "react"
import { Image, Platform, ViewStyle } from "react-native"

import { createStackNavigator } from "@react-navigation/stack"

import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import CertificationScreen from "../screens/CertificationScreen"
import ChangeProfileImageScreen from "../screens/ChangeProfileImageScreen"
import CodeVerificationScreen from "../screens/CodeVerificationScreen"
import DeleteAccountScreen from "../screens/DeleteAccountScreen"
import DeleteAccountWhyScreen from "../screens/DeleteAccountWhyScreen"
import { EditNicknameScreen } from "../screens/EditNicknameScreen"
import { EditPasswordScreen } from "../screens/EditPasswordScreen"
import EditUserProfileScreen from "../screens/EditUserProfileScreen"
import FamilyMembersScreen from "../screens/FamilyMembersScreen"
import HelpCenterScreen from "../screens/HelpCenterScreen"
import MarketingAgreementScreen from "../screens/MarketingAgreementScreen"
import MedicalInfoSourceScreen from "../screens/MedicalInfoSourceScreen"
import MyPageScreen from "../screens/MyPageScreen"
import PrivacyPolicyAndTermsScreen from "../screens/PrivacyPolicyAndTermsScreen"
import PrivacyPolicyScreen from "../screens/PrivacyPolicyScreen"
import SettingsScreen from "../screens/SettingsScreen"
import TermsOfServiceScreen from "../screens/TermsOfServiceScreen"
import WebViewScreen from "../screens/WebViewScreen"
import { styles } from "../theme"

export type MyPageStackParamList = {
  MyPage: undefined
  EditUserProfile: {
    imageUri: string
    imageType: string
  }
  EditNickname: {
    nickname: string
  }
  EditPassword: undefined
  FamilyMembers: undefined
  Settings: undefined
  TermsOfService: undefined
  PrivacyPolicy: undefined
  WebView: {
    url: string
  }
  HelpCenter: undefined
  DeleteAccount: undefined
  DeleteAccountWhy: undefined
  ChangeProfileImage: {
    prevRouteName: string
    prevPrevRouteName: string
    profileName?: string
    profileGender?: "" | "M" | "F" | "male" | "female"
    profileBirth?: Date
    memberIndex?: number
  }
  CertificationDetail: {
    previousScreen: string
  }
  MedicalInfoSource: undefined
  CodeVerification: {
    purpose: string
    preRoute: string
  }
  PrivacyPolicyAndTerms: undefined
  MarketingAgreement: undefined
}

const Stack = createStackNavigator<MyPageStackParamList>()

const closeImageSource = require("../assets/images/close.png")
const CLOSE = { width: 15.7, height: 15.7, margin: Platform.OS === "ios" ? 24 : 10 }
const EDIT_SCREEN_HEADER: ViewStyle = {
  borderBottomWidth: 0,
  elevation: 0,
  shadowColor: "transparent",
}
export function MyPageNavigator({ navigation, route }) {
  if (route.state && route.state.index > 0) {
    navigation.setOptions({ tabBarVisible: false })
  } else {
    navigation.setOptions({ tabBarVisible: true })
  }
  return (
    <Stack.Navigator
      initialRouteName="MyPage"
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitleAlign: "center",
        headerBackTitle: null,
        headerBackImage: () => <CustomHeaderBackImage />,
        headerStyle: styles.HEADER,
        headerTitleStyle: styles.HEADER_TITLE,
        headerTruncatedBackTitle: "",
      }}
    >
      <Stack.Screen
        name="MyPage"
        component={MyPageScreen}
        options={MyPageScreen.navigationOptions}
      />
      <Stack.Screen
        name="EditUserProfile"
        component={EditUserProfileScreen}
        options={EditUserProfileScreen.navigationOptions}
      />
      <Stack.Screen
        name="EditNickname"
        component={EditNicknameScreen}
        options={{
          headerBackImage: () => <Image source={closeImageSource} style={CLOSE} />,
          title: "",
          headerStyle: EDIT_SCREEN_HEADER,
        }}
      />
      <Stack.Screen
        name="EditPassword"
        component={EditPasswordScreen}
        options={{
          headerBackImage: () => <Image source={closeImageSource} style={CLOSE} />,
          title: "",
          headerStyle: EDIT_SCREEN_HEADER,
        }}
      />
      <Stack.Screen
        name="FamilyMembers"
        component={FamilyMembersScreen}
        options={FamilyMembersScreen.navigationOptions}
      />
      <Stack.Screen
        name="Settings"
        component={SettingsScreen}
        options={SettingsScreen.navigationOptions}
      />
      <Stack.Screen
        name="TermsOfService"
        component={TermsOfServiceScreen}
        options={TermsOfServiceScreen.navigationOptions}
      />
      <Stack.Screen
        name="PrivacyPolicy"
        component={PrivacyPolicyScreen}
        options={PrivacyPolicyScreen.navigationOptions}
      />
      <Stack.Screen
        name="WebView"
        component={WebViewScreen}
        options={WebViewScreen.navigationOptions}
      />
      <Stack.Screen
        name="HelpCenter"
        component={HelpCenterScreen}
        options={HelpCenterScreen.navigationOptions}
      />
      <Stack.Screen
        name="DeleteAccount"
        component={DeleteAccountScreen}
        options={DeleteAccountScreen.navigationOptions}
      />
      <Stack.Screen
        name="DeleteAccountWhy"
        component={DeleteAccountWhyScreen}
        options={DeleteAccountWhyScreen.navigationOptions}
      />
      <Stack.Screen
        name="ChangeProfileImage"
        component={ChangeProfileImageScreen}
        options={ChangeProfileImageScreen.navigationOptions}
      />
      <Stack.Screen
        name="CertificationDetail"
        component={CertificationScreen}
        options={CertificationScreen.navigationOptions}
      />
      <Stack.Screen
        name="MedicalInfoSource"
        component={MedicalInfoSourceScreen}
        options={MedicalInfoSourceScreen.navigationOptions}
      />
      <Stack.Screen
        name="CodeVerification"
        component={CodeVerificationScreen}
        options={CodeVerificationScreen.navigationOptions}
      />
      <Stack.Screen
        name="PrivacyPolicyAndTerms"
        component={PrivacyPolicyAndTermsScreen}
        options={PrivacyPolicyAndTermsScreen.navigationOptions}
      />
      <Stack.Screen
        name="MarketingAgreement"
        component={MarketingAgreementScreen}
        options={MarketingAgreementScreen.navigationOptions}
      />
    </Stack.Navigator>
  )
}
