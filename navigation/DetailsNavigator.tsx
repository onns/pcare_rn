/* eslint-disable react/display-name */
import React from "react"
import { Image, Platform, ViewStyle } from "react-native"

import { Drug, FamilyMember } from "@api/api.types"
import CloseButton from "@components/CloseButton"
import DrugV4DetailScreen from "@features/drug-v4/DrugDetailScreen"
import { SearchDrugScreen as SearchDrugV4Screen } from "@features/drug-v4/SearchDrugScreen"
import HospitalV4DetailScreen from "@features/hospital-v4/HospitalDetailScreen"
import { GetPointScreen } from "@features/points/GetPointScreen"
import { MyPointScreen } from "@features/points/mypage/MyPointScreen"
import { PointGuideScreen } from "@features/points/PointGuideScreen"
import { useNavigation } from "@react-navigation/native"
import { CardStyleInterpolators, createStackNavigator } from "@react-navigation/stack"

import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { ActivityScreen } from "../screens/ActivityScreen"
import AddFamilyMemberScreen from "../screens/AddFamilyMemberScreen"
import AddPrescriptionScreen from "../screens/AddPrescriptionScreen"
import CertificationScreen from "../screens/CertificationScreen"
import ChangeProfileImageScreen from "../screens/ChangeProfileImageScreen"
import DiseaseSummaryScreen from "../screens/DiseaseSummaryScreen"
import DrugDetailScreen from "../screens/DrugDetailScreen"
import EditAddressScreen from "../screens/EditAddressScreen"
import EditFamilyMemberScreen from "../screens/EditFamilyMemberScreen"
import EditMedicationDetailScreen, {
  EditMedicationDetailMode,
} from "../screens/EditMedicationDetailScreen"
import { EditMemberNameScreen } from "../screens/EditMemberNameScreen"
import EditUserProfileScreen from "../screens/EditUserProfileScreen"
import HospitalDetailScreen from "../screens/HospitalDetailScreen"
import MedicationSettingsScreen from "../screens/MedicationSettingsScreen"
import PendingPrescriptionDetailScreen from "../screens/PendingPrescriptionDetailScreen"
import { PrescriptionCaptureGuideScreen } from "../screens/PrescriptionCaptureGuide"
import PrescriptionPreviewScreen from "../screens/PrescriptionPreviewScreen"
import PrescriptionScannerScreen from "../screens/PrescriptionScannerScreen"
import { RecordDetailScreen } from "../screens/RecordDetailScreen"
import { RecordsScreen } from "../screens/RecordsScreen"
import ReportGraphScreen from "../screens/ReportGraphScreen"
import { SearchDiseaseScreen } from "../screens/SearchDiseaseScreen"
import { SearchDrugMode, SearchDrugScreen } from "../screens/SearchDrugScreen"
import { SearchHospitalScreen } from "../screens/SearchHospitalScreen"
import SearchScreen from "../screens/SearchScreen"
import { SurveyIntroScreen } from "../screens/SurveyIntroScreen"
import { SurveyQuestionsScreen } from "../screens/SurveyQuestionsScreen"
import { TimeSlotSettingsScreen } from "../screens/TimeSlotSettingsScreen"
import WebViewScreen from "../screens/WebViewScreen"
import { color, styles, typography } from "../theme"

export type DetailsStackParamList = {
  AddFamilyMember: {
    prevRouteName: string
    imageUri: string
    imageType: string
    profileName: string
    profileGender: "" | "M" | "F" | "male" | "female"
    profileBirth: Date
  }
  EditUserProfile: {
    imageUri: string
    imageType: string
  }
  EditFamilyMember: {
    imageUri?: string
    imageType?: string
    profileName?: string
    profileGender?: "" | "M" | "F" | "male" | "female"
    profileBirth?: Date
    memberInfo?: FamilyMember
    memberIndex?: number
    isManager?: boolean
  }
  EditAddress: undefined
  MedicationSettings: {
    prescriptionId: number
    issueDate: string
  }
  SearchDrug: {
    mode: SearchDrugMode
  }
  SearchDisease: undefined
  SearchHospital: undefined
  AddPrescription: undefined
  EditMedicationDetail: {
    mode: EditMedicationDetailMode
    prescriptionDetailIdx?: number
    drug?: Drug
  }
  EditPassword: undefined
  PendingPrescriptionDetail: undefined
  ChangeProfileImage: {
    prevRouteName: string
    prevPrevRouteName: string
    profileName?: string
    profileGender?: "" | "M" | "F" | "male" | "female"
    profileBirth?: Date
  }
  EditMemberName: {
    memberIndex: number
  }
  TimeSlotSettings: undefined
  SurveyIntro: {
    recordId: number
  }
  SurveyQuestions: undefined
  Records: {
    key: string
    title: string
    name: string
  }
  RecordDetail: {
    id: number
    title: string
  }
  ReportGraph: undefined
  Activity: {
    title: string
    recordId: number
  }
  PrescriptionScanner: {
    rePhotoIndex: number
    rePhotoId: number
    handleBackPress: () => void
  }
  PrescriptionPreview: {
    uri: string
    type?: string
    name?: string
    ocrMessage?: any
    rePhotoIndex?: any
    rePhotoId?: any
  }
  Certification: {
    previousScreen: string
  }
  PrescriptionCaptureGuide: undefined
  DiseaseSummary: {
    id: number
    code: string
    diseaseId?: number
    diseaseCode?: string
    index?: number
    isSubDisease?: boolean
    age?: number
    ageRange?: string
    guideTabVisible?: boolean
    tabIndex?: number
  }
  DiseaseDetail: undefined
  DrugDetail: {
    id: number
  }
  DrugV4Detail: {
    id: number
  }
  HospitalDetail: {
    id: number
  }
  HospitalV4Detail: {
    id: number
  }
  PointGuide: undefined
  GetPoint: {
    pointType: number
    description: string
    pid?: number
    drugTakeIds?: number[]
    alarmName?: string
    point: number
  }
  MyPoint: undefined
}

const Stack = createStackNavigator<DetailsStackParamList>()

const closeImageSource = require("../assets/images/close.png")
const CLOSE = { width: 15.7, height: 15.7, margin: Platform.OS === "ios" ? 24 : 10 }
const EDIT_SCREEN_HEADER: ViewStyle = {
  borderBottomWidth: 0,
  elevation: 0,
  shadowColor: "transparent",
}
const SCANNER_HEADER = {
  backgroundColor: color.palette.gray1,
  borderBottomWidth: 0,
  elevation: 0,
  shadowColor: "transparent",
}
export function DetailsNavigator() {
  const navigation = useNavigation()

  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitleAlign: "center",
        headerBackTitle: null,
        headerBackImage: () => <CustomHeaderBackImage />,
        headerStyle: styles.HEADER,
        headerTitleStyle: styles.HEADER_TITLE,
        headerTruncatedBackTitle: "",
        gestureEnabled: false,
      }}
    >
      {/* AddPrescriptionStack */}
      <Stack.Screen
        name="PrescriptionScanner"
        component={PrescriptionScannerScreen}
        options={() => {
          return {
            title: "처방전/약봉투 촬영",
            headerStyle: SCANNER_HEADER,
            headerLeft: null,
            headerTitleStyle: {
              fontFamily: typography.primary,
              fontWeight: "bold",
              fontSize: 20,
              color: "#fff",
            },
          }
        }}
      />
      <Stack.Screen
        name="PrescriptionCaptureGuide"
        component={PrescriptionCaptureGuideScreen}
        options={() => {
          return {
            title: "사진 등록 가이드",
            headerStyle: SCANNER_HEADER,
            headerLeft: null,
            headerTitleStyle: {
              fontFamily: typography.primary,
              fontWeight: "bold",
              fontSize: 20,
              color: "#fff",
            },
          }
        }}
      />
      <Stack.Screen
        name="PrescriptionPreview"
        component={PrescriptionPreviewScreen}
        options={PrescriptionPreviewScreen.navigationOptions}
      />
      <Stack.Screen
        name="Certification"
        component={CertificationScreen}
        options={CertificationScreen.navigationOptions}
      />
      {/*  */}
      <Stack.Screen
        name="AddFamilyMember"
        component={AddFamilyMemberScreen}
        options={AddFamilyMemberScreen.navigationOptions}
      />
      <Stack.Screen
        name="EditUserProfile"
        component={EditUserProfileScreen}
        options={EditUserProfileScreen.navigationOptions}
      />
      <Stack.Screen
        name="EditFamilyMember"
        component={EditFamilyMemberScreen}
        options={EditFamilyMemberScreen.navigationOptions}
      />
      <Stack.Screen
        name="EditMemberName"
        component={EditMemberNameScreen}
        options={{
          headerBackImage: () => <Image source={closeImageSource} style={CLOSE} />,
          title: "",
          headerStyle: EDIT_SCREEN_HEADER,
        }}
      />
      <Stack.Screen name="EditAddress" component={EditAddressScreen} />
      <Stack.Screen
        name="PendingPrescriptionDetail"
        component={PendingPrescriptionDetailScreen}
        options={PendingPrescriptionDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="ChangeProfileImage"
        component={ChangeProfileImageScreen}
        options={ChangeProfileImageScreen.navigationOptions}
      />
      <Stack.Screen
        name="WebView"
        component={WebViewScreen}
        options={WebViewScreen.navigationOptions}
      />
      <Stack.Screen
        name="TimeSlotSettings"
        component={TimeSlotSettingsScreen}
        options={{
          cardStyleInterpolator:
            Platform.OS === "ios"
              ? CardStyleInterpolators.forVerticalIOS
              : CardStyleInterpolators.forFadeFromBottomAndroid,
        }}
      />
      <Stack.Screen name="MedicationSettings" component={MedicationSettingsScreen} />
      <Stack.Screen name="AddPrescription" component={AddPrescriptionScreen} />
      <Stack.Screen
        name="EditMedicationDetail"
        component={EditMedicationDetailScreen}
        options={{ gestureEnabled: false }}
      />
      <Stack.Screen
        name="SearchDrug"
        component={SearchDrugScreen}
        options={{
          title: "약 입력",
          headerStyle: {
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 0,
            },
            elevation: 0,
          },
        }}
      />
      <Stack.Screen
        name="SearchDrugV4"
        component={SearchDrugV4Screen}
        options={{
          title: "약 입력",
          headerStyle: {
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 0,
            },
            elevation: 0,
          },
        }}
      />
      <Stack.Screen
        name="SearchDisease"
        component={SearchDiseaseScreen}
        options={{
          title: "질병 입력",
          headerStyle: {
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 0,
            },
            elevation: 0,
          },
        }}
      />
      <Stack.Screen
        name="SearchHospital"
        component={SearchHospitalScreen}
        options={{
          title: "병원 입력",
          headerStyle: {
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 0,
            },
            elevation: 0,
          },
        }}
      />
      <Stack.Screen name="Activity" component={ActivityScreen} />
      <Stack.Screen name="SurveyIntro" component={SurveyIntroScreen} />
      <Stack.Screen name="SurveyQuestions" component={SurveyQuestionsScreen} />
      <Stack.Screen name="Records" component={RecordsScreen} />
      <Stack.Screen name="RecordDetail" component={RecordDetailScreen} options={{ title: "" }} />
      <Stack.Screen name="ReportGraph" component={ReportGraphScreen} options={{ title: "" }} />
      <Stack.Screen
        name="Search"
        component={SearchScreen}
        options={{
          title: "검색",
        }}
      />
      <Stack.Screen
        name="DiseaseSummary"
        component={DiseaseSummaryScreen}
        options={DiseaseSummaryScreen.navigationOptions}
      />
      <Stack.Screen
        name="DrugDetail"
        component={DrugDetailScreen}
        options={DrugDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="DrugV4Detail"
        component={DrugV4DetailScreen}
        options={DrugV4DetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="HospitalDetail"
        component={HospitalDetailScreen}
        options={HospitalDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="HospitalV4Detail"
        component={HospitalV4DetailScreen}
        options={HospitalV4DetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="PointGuide"
        component={PointGuideScreen}
        options={{
          gestureEnabled: false,
          title: "포인트 운영 안내",
          headerShadowVisible: false,
          headerLeft: null,
          headerRight: () => (
            <CloseButton
              onPress={() => {
                if (navigation.canGoBack()) {
                  navigation.goBack()
                }
                navigation.navigate("Main")
              }}
            />
          ),
        }}
      />
      <Stack.Screen
        name="GetPoint"
        component={GetPointScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="MyPoint"
        component={MyPointScreen}
        options={{
          title: "",
          headerStyle: {
            elevation: 0,
          },
          gestureEnabled: false,
        }}
      />
    </Stack.Navigator>
  )
}
