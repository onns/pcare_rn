import React from "react"

import { Drug } from "@api/api.types"
import { createStackNavigator } from "@react-navigation/stack"
import { Prescription } from "@stores/PrescriptionStore"

import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import DiseaseDetailScreen from "../screens/DiseaseDetailScreen"
import DiseaseSummaryScreen from "../screens/DiseaseSummaryScreen"
import DrugAdherenceRatingScreen from "../screens/DrugAdherenceRatingScreen"
import DrugDetailScreen from "../screens/DrugDetailScreen"
import HospitalDetailScreen from "../screens/HospitalDetailScreen"
import NoticeDetailScreen from "../screens/NoticeDetailScreen"
import NoticeListScreen from "../screens/NoticeListScreen"
import PendingPrescriptionDetailScreen from "../screens/PendingPrescriptionDetailScreen"
import PrePrescriptionsAgreementScreen from "../screens/PrePrescriptionsAgreementScreen"
import PrescriptionDetailScreen from "../screens/PrescriptionDetailScreen"
import PrescriptionsScreen from "../screens/PrescriptionsScreen"
import SymptomImprovementRatingScreen from "../screens/SymptomImprovementRatingScreen"
import WebViewScreen from "../screens/WebViewScreen"
import { color, styles } from "../theme"

export type PrescriptionsStackParamList = {
  Prescriptions: {
    prescriptionStore?: any
  }
  PrescriptionDetail: {
    prescriptionId: number
    startDate: string
    endDate: string
    handleMorePress: () => void
  }
  DrugDetail: {
    id: number
  }
  HospitalDetail: {
    id: number
  }
  DiseaseSummary: {
    id: number
    code: string
    diseaseId?: number
    diseaseCode?: string
    index?: number
    isSubDisease?: boolean
    age?: number
    ageRange?: string
    guideTabVisible?: boolean
    tabIndex?: number
  }
  DiseaseDetail: {
    id: number
    diseaseIdx: number
    diseaseDetailIdx: number
    isSubDisease: boolean
  }
  DrugAdherenceRating: {
    rating?: number
    prescriptionId?: number
    issueDate?: string
    repTitle?: string
    neededSymptomImprovementRating?: boolean
  }
  SymptomImprovementRating: {
    rating?: number
    prescriptionId?: any
    repTitle?: any
    issueDate?: any
  }
  PendingPrescriptionDetail: {
    item: Prescription
    index: number
    prevRouteName: string
  }
  NoticeList: undefined
  NoticeDetail: undefined
  WebView: {
    url: string
  }
  PrePrescriptionsAgreement: undefined
  EditMedicationDetail: {
    prescriptionDetailIdx?: number
    drug?: Drug
  }
}

const Stack = createStackNavigator<PrescriptionsStackParamList>()

export function PrescriptionsNavigator({ navigation, route }) {
  if (route.state && route.state.index > 0) {
    navigation.setOptions({ tabBarVisible: false })
  } else {
    navigation.setOptions({ tabBarVisible: true })
  }
  return (
    <Stack.Navigator
      initialRouteName="Prescriptions"
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitleAlign: "center",
        headerBackTitle: null,
        headerBackImage: () => <CustomHeaderBackImage />,
        headerStyle: styles.HEADER,
        headerTintColor: color.palette.orange,
        headerTitleStyle: styles.HEADER_TITLE,
        headerTruncatedBackTitle: "",
      }}
      // 처방내역 상세 화면의 경우 header 높이가 길어서 float 설정이었을 때 약제/병원/질병 상세 화면 들어갈 시 이전 header 의 잔상이 남는 문제가 있음
      headerMode="screen"
    >
      <Stack.Screen
        name="Prescriptions"
        component={PrescriptionsScreen}
        options={PrescriptionsScreen.navigationOptions}
      />
      <Stack.Screen
        name="PrescriptionDetail"
        component={PrescriptionDetailScreen}
        options={PrescriptionDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="DrugDetail"
        component={DrugDetailScreen}
        options={DrugDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="HospitalDetail"
        component={HospitalDetailScreen}
        options={HospitalDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="DiseaseSummary"
        component={DiseaseSummaryScreen}
        options={DiseaseSummaryScreen.navigationOptions}
      />
      <Stack.Screen
        name="DiseaseDetail"
        component={DiseaseDetailScreen}
        options={DiseaseDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="DrugAdherenceRating"
        component={DrugAdherenceRatingScreen}
        options={DrugAdherenceRatingScreen.navigationOptions}
      />
      <Stack.Screen
        name="SymptomImprovementRating"
        component={SymptomImprovementRatingScreen}
        options={SymptomImprovementRatingScreen.navigationOptions}
      />
      <Stack.Screen
        name="PendingPrescriptionDetail"
        component={PendingPrescriptionDetailScreen}
        options={PendingPrescriptionDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="NoticeList"
        component={NoticeListScreen}
        options={NoticeListScreen.navigationOptions}
      />
      <Stack.Screen
        name="NoticeDetail"
        component={NoticeDetailScreen}
        options={NoticeDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="WebView"
        component={WebViewScreen}
        options={WebViewScreen.navigationOptions}
      />
      <Stack.Screen
        name="PrePrescriptionsAgreement"
        component={PrePrescriptionsAgreementScreen}
        options={PrePrescriptionsAgreementScreen.navigationOptions}
      />
    </Stack.Navigator>
  )
}
