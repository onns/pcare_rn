import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { Column } from "native-base"
/* eslint-disable react/display-name */
import React, { useRef } from "react"
import {
  Platform,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import RBSheet from "react-native-raw-bottom-sheet"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { fromBottom, fromLeft, zoomIn, zoomOut } from "react-navigation-transitions"

import { sendLogEvent } from "@lib/analytics"
import firebase from "@react-native-firebase/app"
import remoteConfig from "@react-native-firebase/remote-config"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { NavigatorScreenParams } from "@react-navigation/native"

import SvgCamera from "../assets/images/ic_pictogram24px_camera.svg"
import SvgMemo from "../assets/images/ic_pictogram24px_memo.svg"
import SvgPencil from "../assets/images/ic_pictogram24px_pencil.svg"
import SvgAdd from "../assets/images/tabbarAdd.svg"
import SvgAdd2 from "../assets/images/tabbarAdd2.svg"
import SvgDrug from "../assets/images/tabbarDrug.svg"
import SvgDrugActive from "../assets/images/tabbarDrugActive.svg"
import SvgHistory from "../assets/images/tabbarHistory.svg"
import SvgHistoryActive from "../assets/images/tabbarHistoryActive.svg"
import SvgHome from "../assets/images/tabbarHome.svg"
import SvgHomeActive from "../assets/images/tabbarHomeActive.svg"
import SvgMyPage from "../assets/images/tabbarMyPage.svg"
import SvgMyPageActive from "../assets/images/tabbarMyPageActive.svg"
import { Text } from "../components/StyledText"
import { loadString } from "../lib/storage"
import { color, styles, typography } from "../theme"
import { DetailsNavigator } from "./DetailsNavigator"
import { HomeNavigator } from "./HomeNavigator"
import { MyPageNavigator } from "./MyPageNavigator"
import { PrescriptionsNavigator } from "./PrescriptionsNavigator"
import { TakingDrugNavigator } from "./TakingDrugNavigator"
import {
  AddPrescriptionStackParamList,
  HomeStackParamList,
  MyPageStackParamList,
  PrescriptionsStackParamList,
  TakingDrugStackParamList,
} from "./types"

const TAB_BAR: ViewStyle = {
  flexDirection: "row",
  backgroundColor: color.palette.white,
  borderTopWidth: 1,
  borderTopColor: color.line,
  alignItems: "center",
  justifyContent: "center",
  paddingTop: 5,
}

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2]
  const nextScene = scenes[scenes.length - 1]

  // Custom transitions go there
  if (
    prevScene &&
    prevScene.route.routeName === "Welcome" &&
    nextScene.route.routeName === "Home"
  ) {
    return fromBottom(1000)
  }
  // else if (prevScene
  //   && prevScene.route.routeName === 'ScreenB'
  //   && nextScene.route.routeName === 'ScreenC') {
  //   return zoomOut();
  // }
  // return fromLeft();
}

const homeNavigatorOptions = ({ navigation, route }) => {
  const tabBarLabel = "홈"

  let tabBarVisible = true
  if (navigation.getState().index > 0) {
    tabBarVisible = false
  }

  return {
    tabBarLabel,
    tabBarVisible,
  }
}

const prescriptionsNavigatorOptions = ({ navigation, route }) => {
  const tabBarLabel = "의료기록"

  let tabBarVisible = true
  if (navigation.getState().index > 0) {
    tabBarVisible = false
  }

  return {
    tabBarLabel,
    tabBarVisible,
  }
}

const healthTemplatesNavigatorOptions = {
  title: "",
  tabBarVisible: false,
}

const addPrescriptionNavigatorOptions = {
  title: "",
  // tabBarLabel: "",
  tabBarVisible: false,
}

const takingDrugNavigatorOptions = ({ navigation, route }) => {
  const tabBarLabel = "복약관리"

  let tabBarVisible = true
  if (navigation.getState().index > 0) {
    tabBarVisible = false
  }

  return {
    tabBarLabel,
    tabBarVisible,
  }
}

function TabBar({ state, descriptors, navigation }) {
  const insets = useSafeAreaInsets()
  const bottomSheetRef = useRef<RBSheet>(null)

  const renderBottomSheet = () => {
    const checkShowCaptureGuide = async () => {
      const doNotShowGuide = await loadString("doNotDisplayCaptureGuide")
      if (Platform.OS === "ios") {
        if (doNotShowGuide === "true") {
          navigation.navigate("Details", { screen: "PrescriptionScanner" })
        } else {
          navigation.navigate("Details", { screen: "PrescriptionCaptureGuide" })
        }
      } else {
        navigation.navigate("Details", { screen: "PrescriptionScanner" })
      }
    }
    return (
      <View>
        {/* 헤더 */}
        <View style={styles.BOTTOM_SHEET_HEADER}>
          <View style={{ height: 16 }} />
          <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>건강기록 만들기</Text>
        </View>
        {/* 메뉴 아이템 */}
        <View>
          {remoteConfig().getBoolean("todayfeelin_enabled") && (
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_BUTTON}
              underlayColor={color.surface.uiPressed}
              onPress={() => {
                sendLogEvent("press_todayfeelin_memo")
                bottomSheetRef.current.close()
                navigation.navigate("HealthTemplates", { screen: "MyFeelinNotes" })
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <SvgMemo />
                <View style={{ width: 12 }} />
                <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>오늘의 컨디션 메모</Text>
              </View>
            </TouchableHighlight>
          )}
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.surface.uiPressed}
            onPress={() => {
              bottomSheetRef.current.close()
              navigation.navigate("Details", { screen: "AddPrescription" })
            }}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <SvgPencil />
              <View style={{ width: 12 }} />
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>복약 스케쥴(직접 입력)</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_LAST_BUTTON}
            underlayColor={color.surface.uiPressed}
            onPress={() => {
              bottomSheetRef.current.close()
              // navigation.navigate("AddPrescriptionStack")
              // navigation.navigate("Details", { screen: "PrescriptionScanner" })
              checkShowCaptureGuide()
            }}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <SvgCamera />
              <View style={{ width: 12 }} />
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>복약 스케쥴(처방전 사진)</Text>
            </View>
          </TouchableHighlight>
        </View>
        {/* 취소 버튼 */}
        <View style={{ backgroundColor: color.palette.white }}>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_CANCEL_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => bottomSheetRef.current.close()}
          >
            <Text style={styles.BOTTOM_SHEET_CANCEL_BUTTON_TEXT}>취소</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  return (
    <View style={[TAB_BAR, { paddingBottom: 5 + insets.bottom }]}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key]
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name

        const isFocused = state.index === index

        const onPress = (tabBarIndex: number) => {
          if (tabBarIndex == 2) {
            firebase.analytics().logEvent(`press_healthTemplate`)
            amplitude.getInstance().logEvent("press_healthTemplate")
          }
          const event = navigation.emit({
            type: "tabPress",
            target: route.key,
            canPreventDefault: true,
          })

          if (!isFocused && !event.defaultPrevented) {
            // '+' button 인 경우 bottom sheet
            if (tabBarIndex == 2) {
              bottomSheetRef.current.open()
            } else {
              navigation.navigate(route.name)
            }
          }
        }

        const onLongPress = () => {
          navigation.emit({
            type: "tabLongPress",
            target: route.key,
          })
        }

        let tabBarIcon
        switch (index) {
          case 0:
            tabBarIcon = (focused: boolean) => (focused ? <SvgHomeActive /> : <SvgHome />)
            break

          case 1:
            tabBarIcon = (focused: boolean) => (focused ? <SvgHistoryActive /> : <SvgHistory />)
            break

          case 2:
            tabBarIcon = () => {
              if (remoteConfig().getBoolean("points_enabled")) {
                return <SvgAdd2 width={56} height={56} />
              }
              return <SvgAdd width={50} height={50} />
            }
            break

          case 3:
            tabBarIcon = (focused: boolean) => (focused ? <SvgDrugActive /> : <SvgDrug />)
            break

          case 4:
            tabBarIcon = (focused: boolean) => (focused ? <SvgMyPageActive /> : <SvgMyPage />)
            break
        }

        const TAB_BAR_LABEL: TextStyle = {
          fontFamily: typography.bold,
          fontSize: 12,
          lineHeight: 18,
          marginTop: 2,
          color: isFocused ? color.palette.cordovanBrown : color.palette.grey50,
        }
        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityStates={isFocused ? ["selected"] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={() => onPress(index)}
            onLongPress={onLongPress}
            style={{ flex: 1, alignItems: "center" }}
          >
            {tabBarIcon(isFocused)}
            {label ? (
              <Text allowFontScaling={false} style={TAB_BAR_LABEL}>
                {label}
              </Text>
            ) : null}
          </TouchableOpacity>
        )
      })}
      <RBSheet
        ref={bottomSheetRef}
        dragFromTopOnly={true}
        height={
          styles.bottomSheetHeaderHeight +
          styles.bottomSheetButtonHeight *
            (remoteConfig().getBoolean("todayfeelin_enabled") ? 4 : 3) +
          14
        }
        openDuration={250}
        closeDuration={250}
        customStyles={{
          // wrapper: {
          //   backgroundColor: "transparent",
          // },
          container: {
            backgroundColor: "transparent",
          },
          draggableIcon: {
            backgroundColor: color.palette.grey50,
          },
        }}
      >
        <StatusBar backgroundColor={color.dimmedStatusBar} />
        {renderBottomSheet()}
      </RBSheet>
    </View>
  )
}

export type MainTabParamList = {
  HomeStack: NavigatorScreenParams<HomeStackParamList>
  PrescriptionsStack: NavigatorScreenParams<PrescriptionsStackParamList>
  AddPrescriptionStack: NavigatorScreenParams<AddPrescriptionStackParamList>
  TakingDrugStack: NavigatorScreenParams<TakingDrugStackParamList>
  MyPageStack: NavigatorScreenParams<MyPageStackParamList>
}

const Tab = createBottomTabNavigator<MainTabParamList>()

const myPageNavigatorOptions = ({ navigation, route }) => {
  const tabBarLabel = "내정보"

  let tabBarVisible = true
  if (route.state?.index > 0 ?? false) {
    tabBarVisible = false
  }

  return {
    tabBarLabel,
    tabBarVisible,
  }
}

export function MainTabNavigator() {
  return (
    <Tab.Navigator
      tabBar={(props) => <TabBar {...props} />}
      initialRouteName="HomeStack"
      screenOptions={{ headerShown: false }}
    >
      <Tab.Screen name="HomeStack" component={HomeNavigator} options={homeNavigatorOptions} />
      <Tab.Screen
        name="PrescriptionsStack"
        component={PrescriptionsNavigator}
        options={prescriptionsNavigatorOptions}
      />
      <Tab.Screen
        name="AddPrescriptionStack"
        component={DetailsNavigator}
        options={addPrescriptionNavigatorOptions}
      />
      <Tab.Screen
        name="TakingDrugStack"
        component={TakingDrugNavigator}
        options={takingDrugNavigatorOptions}
      />
      <Tab.Screen name="MyPageStack" component={MyPageNavigator} options={myPageNavigatorOptions} />
    </Tab.Navigator>
  )
}
