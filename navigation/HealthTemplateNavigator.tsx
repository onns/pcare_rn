import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import MyFeelinNotesScreen from "../screens/MyFeelinNotesScreen"
import { styles, typography } from "../theme"

export type HealthTemplateStackParamList = {
  MyFeelinNotes: undefined
}

const Stack = createStackNavigator<HealthTemplateStackParamList>()

export function HealthTemplateNavigator({ navigation, route }) {
  // if (route.state && route.state.index > 0) {
  //   navigation.setOptions({ tabBarVisible: false })
  // } else {
  //   navigation.setOptions({ tabBarVisible: true })
  // }
  return (
    <>
      <Stack.Navigator
        screenOptions={{
          headerBackTitleVisible: false,
          headerTitleAlign: "center",
          headerBackTitle: null,
          headerBackImage: () => <CustomHeaderBackImage />,
          headerStyle: styles.HEADER,
          headerTitleStyle: {
            fontFamily: typography.primary,
            fontWeight: "bold",
            fontSize: 20,
            color: "#fff",
          },
          headerTruncatedBackTitle: "",
        }}
      >
        <Stack.Screen
          name="MyFeelinNotes"
          component={MyFeelinNotesScreen}
          options={MyFeelinNotesScreen.navigationOptions}
        />
      </Stack.Navigator>
    </>
  )
}
