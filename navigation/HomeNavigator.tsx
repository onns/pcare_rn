import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import DiseaseDetailScreen from "../screens/DiseaseDetailScreen"
import DiseaseSummaryScreen from "../screens/DiseaseSummaryScreen"
import DrugAdherenceRatingScreen from "../screens/DrugAdherenceRatingScreen"
import DrugDetailScreen from "../screens/DrugDetailScreen"
import DrugSettingsScreen from "../screens/DrugSettingsScreen"
import HomeScreen from "../screens/HomeScreen"
import HospitalDetailScreen from "../screens/HospitalDetailScreen"
import MyFeelinNotesScreen from "../screens/MyFeelinNotesScreen"
import NoticeDetailScreen from "../screens/NoticeDetailScreen"
import NoticeListScreen from "../screens/NoticeListScreen"
import PendingPrescriptionDetailScreen from "../screens/PendingPrescriptionDetailScreen"
import PrePrescriptionsAgreementScreen from "../screens/PrePrescriptionsAgreementScreen"
import PrescriptionDetailScreen from "../screens/PrescriptionDetailScreen"
import SearchScreen from "../screens/SearchScreen"
import SymptomImprovementRatingScreen from "../screens/SymptomImprovementRatingScreen"
import WebViewScreen from "../screens/WebViewScreen"
import { color, styles } from "../theme"

export type HomeStackParamList = {
  Home: undefined
  Search: undefined
  DrugDetail: {
    id: number
  }
  PrescriptionDetail: {
    prescriptionId: number
    issueDate: string
  }
  HospitalDetail: {
    id: number
  }
  DiseaseSummary: {
    id: number
    code: string
    diseaseId?: number
    diseaseCode?: string
    index?: number
    isSubDisease?: boolean
    age?: number
    ageRange?: string
    guideTabVisible?: boolean
    tabIndex?: number
  }
  DiseaseDetail: undefined
  DrugAdherenceRating: {
    rating?: number
    prescriptionId?: number
    issueDate?: string
    repTitle?: string
    neededSymptomImprovementRating?: boolean
  }
  SymptomImprovementRating: {
    rating?: number
    prescriptionId?: any
    repTitle?: any
    issueDate?: any
  }
  PendingPrescriptionDetail: {
    prescription: any
    id: number
  }
  NoticeList: undefined
  // MyFeelinNotes: undefined
  NoticeDetail: {
    id: number
  }
  WebView: {
    url?: string
    content?: string
    javaScript?: string
  }
  PrePrescriptionsAgreement: undefined
  DrugSettings: {
    prescriptionId: number
  }
  TimeSlotSettings: undefined
  EditMedicationDetail: {
    prescriptionDetailIdx?: number
  }
}

const Stack = createStackNavigator<HomeStackParamList>()

export function HomeNavigator({ navigation, route }) {
  if (route.state && route.state.index > 0) {
    navigation.setOptions({ tabBarVisible: false })
  } else {
    navigation.setOptions({ tabBarVisible: true })
  }
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitleAlign: "center",
        headerBackTitle: null,
        headerBackImage: () => <CustomHeaderBackImage />,
        headerStyle: styles.HEADER,
        headerTintColor: color.palette.orange,
        headerTitleStyle: styles.HEADER_TITLE,
        headerTruncatedBackTitle: "",
      }}
      initialRouteName="Home"
      // 처방내역 상세 화면의 경우 header 높이가 길어서 float 설정이었을 때 약제/병원/질병 상세 화면 들어갈 시 이전 header 의 잔상이 남는 문제가 있음
      headerMode="screen"
    >
      <Stack.Screen name="Home" component={HomeScreen} options={HomeScreen.navigationOptions} />
      <Stack.Screen
        name="PrescriptionDetail"
        component={PrescriptionDetailScreen}
        options={PrescriptionDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="DrugDetail"
        component={DrugDetailScreen}
        options={DrugDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="HospitalDetail"
        component={HospitalDetailScreen}
        options={HospitalDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="DiseaseSummary"
        component={DiseaseSummaryScreen}
        options={DiseaseSummaryScreen.navigationOptions}
      />
      <Stack.Screen
        name="DiseaseDetail"
        component={DiseaseDetailScreen}
        options={DiseaseDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="DrugAdherenceRating"
        component={DrugAdherenceRatingScreen}
        options={DrugAdherenceRatingScreen.navigationOptions}
      />
      <Stack.Screen
        name="SymptomImprovementRating"
        component={SymptomImprovementRatingScreen}
        options={SymptomImprovementRatingScreen.navigationOptions}
      />
      <Stack.Screen
        name="PendingPrescriptionDetail"
        component={PendingPrescriptionDetailScreen}
        options={PendingPrescriptionDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="NoticeList"
        component={NoticeListScreen}
        options={NoticeListScreen.navigationOptions}
      />
      {/* <Stack.Screen
        name="MyFeelinNotes"
        component={MyFeelinNotesScreen}
        options={MyFeelinNotesScreen.navigationOptions}
      /> */}
      <Stack.Screen
        name="NoticeDetail"
        component={NoticeDetailScreen}
        options={NoticeDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="WebView"
        component={WebViewScreen}
        options={WebViewScreen.navigationOptions}
      />
      <Stack.Screen
        name="PrePrescriptionsAgreement"
        component={PrePrescriptionsAgreementScreen}
        options={PrePrescriptionsAgreementScreen.navigationOptions}
      />
      <Stack.Screen
        name="DrugSettings"
        component={DrugSettingsScreen}
        options={{
          title: "약 설정",
        }}
      />
    </Stack.Navigator>
  )
}

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 */
export const exitRoutes: string[] = [
  "Walkthrough",
  "Home",
  "Prescriptions",
  "MedicationSchedule",
  "MyPage",
]
