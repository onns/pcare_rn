import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import CertificationGuideScreen from "../screens/CertificationGuideScreen"
import CertificationScreen from "../screens/CertificationScreen"
import CheckPregnancyScreen from "../screens/CheckPregnancyScreen"
import CodeVerificationScreen from "../screens/CodeVerificationScreen"
import DeleteAccountScreen from "../screens/DeleteAccountScreen"
import DeleteAccountWhyScreen from "../screens/DeleteAccountWhyScreen"
import InitialScreen from "../screens/InitialScreen"
import PasswordResetScreen from "../screens/PasswordResetScreen"
import PrivacyPolicyGuideScreen from "../screens/PrivacyPolicyGuideScreen"
import PrivacyPolicyScreen from "../screens/PrivacyPolicyScreen"
import ServiceAgreementScreen from "../screens/ServiceAgreementScreen"
import SignInScreen from "../screens/SignInScreen"
import SignUpEmailScreen from "../screens/SignUpEmailScreen"
import SignUpScreen from "../screens/SignUpScreen"
import TermsOfServiceScreen from "../screens/TermsOfServiceScreen"
import TourGuideScreen from "../screens/TourGuideScreen"
import UseOfDurServiceScreen from "../screens/UseOfDurServiceScreen"
import WalkthroughScreen from "../screens/WalkthroughScreen"
import WebViewScreen from "../screens/WebViewScreen"
import WelcomeScreen from "../screens/WelcomeScreen"
import { color, styles } from "../theme"

export type AuthStackParamList = {
  Initial: undefined
  Walkthrough: {
    prevRouteName: string
  }
  SignUp: {
    hideTourButton?: boolean
    prevScreen?: string
  }
  SignUpEmail: undefined
  TourGuide: undefined
  SignIn: undefined
  Reset: undefined
  PrivacyPolicyGuide: undefined
  ServiceAgreement: {
    isLoginProcess?: boolean
  }
  DeleteAccount: undefined
  DeleteAccountWhy: undefined
  UseOfDurService: {
    isLoginProcess?: boolean
  }
  Certification: {
    previousScreen: string
  }
  CertificationGuide: undefined
  CheckPregnancy: undefined
  Welcome: undefined
  TermsOfService: undefined
  PrivacyPolicy: undefined
  CodeVerification: {
    purpose: string
    preRoute: string
  }
  WebView: {
    title?: string
    url?: string
    content?: string
    javaScript?: string
  }
  PasswordReset: undefined
}

const Stack = createStackNavigator()

export function AuthNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Walkthrough"
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitleAlign: "center",
        headerBackTitle: null,
        headerBackImage: () => <CustomHeaderBackImage />,
        headerStyle: {
          borderBottomWidth: 0,
          height: 76, // default is 64
          elevation: 0,
          shadowColor: "transparent",
        },
        headerTintColor: color.palette.orange,
        headerTitleStyle: styles.HEADER_TITLE,
        headerTruncatedBackTitle: "",
      }}
    >
      <Stack.Screen
        name="Initial"
        component={InitialScreen}
        options={InitialScreen.navigationOptions}
      />
      <Stack.Screen
        name="Walkthrough"
        component={WalkthroughScreen}
        options={WalkthroughScreen.navigationOptions}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUpScreen}
        options={SignUpScreen.navigationOptions}
      />
      <Stack.Screen
        name="SignUpEmail"
        component={SignUpEmailScreen}
        options={SignUpEmailScreen.navigationOptions}
      />
      <Stack.Screen
        name="TourGuide"
        component={TourGuideScreen}
        options={TourGuideScreen.navigationOptions}
      />

      <Stack.Screen
        name="SignIn"
        component={SignInScreen}
        options={SignInScreen.navigationOptions}
      />
      <Stack.Screen
        name="Reset"
        component={PasswordResetScreen}
        options={PasswordResetScreen.navigationOptions}
      />
      {/* <Stack.Screen
        name="PrivacyPolicyGuide"
        component={PrivacyPolicyGuideScreen}
        options={PrivacyPolicyGuideScreen.navigationOptions}
      /> */}
      <Stack.Screen
        name="ServiceAgreement"
        component={ServiceAgreementScreen}
        options={ServiceAgreementScreen.navigationOptions}
      />
      <Stack.Screen
        name="DeleteAccount"
        component={DeleteAccountScreen}
        options={DeleteAccountScreen.navigationOptions}
      />
      <Stack.Screen
        name="DeleteAccountWhy"
        component={DeleteAccountWhyScreen}
        options={DeleteAccountWhyScreen.navigationOptions}
      />
      {/* <Stack.Screen
        name="UseOfDurService"
        component={UseOfDurServiceScreen}
        options={UseOfDurServiceScreen.navigationOptions}
      /> */}
      <Stack.Screen
        name="Certification"
        component={CertificationScreen}
        options={CertificationScreen.navigationOptions}
      />
      <Stack.Screen
        name="CertificationGuide"
        component={CertificationGuideScreen}
        options={{ title: "" }}
      />
      <Stack.Screen
        name="CheckPregnancy"
        component={CheckPregnancyScreen}
        options={{ title: "" }}
      />
      <Stack.Screen
        name="Welcome"
        component={WelcomeScreen}
        options={WelcomeScreen.navigationOptions}
      />
      <Stack.Screen
        name="TermsOfService"
        component={TermsOfServiceScreen}
        options={TermsOfServiceScreen.navigationOptions}
      />
      <Stack.Screen
        name="PrivacyPolicy"
        component={PrivacyPolicyScreen}
        options={PrivacyPolicyScreen.navigationOptions}
      />
      <Stack.Screen
        name="CodeVerification"
        component={CodeVerificationScreen}
        options={CodeVerificationScreen.navigationOptions}
      />
      <Stack.Screen
        name="WebView"
        component={WebViewScreen}
        options={WebViewScreen.navigationOptions}
      />
    </Stack.Navigator>
  )
}
