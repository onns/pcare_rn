import type { AddPrescriptionStackParamList } from "./AddPrescriptionNavigator"
import type { AuthStackParamList } from "./AuthNavigator"
import type { DetailsStackParamList } from "./DetailsNavigator"
import type { HomeStackParamList } from "./HomeNavigator"
import type { MainTabParamList } from "./MainTabNavigator"
import type { MyPageStackParamList } from "./MyPageNavigator"
import type { PrescriptionsStackParamList } from "./PrescriptionsNavigator"
import type { RootStackParamList } from "./RootNavigator"
import type { TakingDrugStackParamList } from "./TakingDrugNavigator"
import type { HealthTemplateStackParamList } from "./HealthTemplateNavigator"

export type {
  AddPrescriptionStackParamList,
  AuthStackParamList,
  DetailsStackParamList,
  HomeStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  PrescriptionsStackParamList,
  RootStackParamList,
  TakingDrugStackParamList,
  HealthTemplateStackParamList,
}
