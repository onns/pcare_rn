import { DateTime } from "luxon"
import React from "react"

import { createStackNavigator } from "@react-navigation/stack"
import { Prescription } from "@stores/PrescriptionStore"

import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import MedicationReportDetailScreen from "../screens/MedicationReportDetailScreen"
import MedicationScheduleDetailScreen from "../screens/MedicationScheduleDetailScreen"
import MedicationScheduleScreen from "../screens/MedicationScheduleScreen"
import NoticeDetailScreen from "../screens/NoticeDetailScreen"
import NoticeListScreen from "../screens/NoticeListScreen"
import PrescriptionDetailScreen from "../screens/PrescriptionDetailScreen"
import { color, styles } from "../theme"

export type TakingDrugStackParamList = {
  MedicationSchedule: undefined
  MedicationScheduleDetail: {
    date: DateTime
  }
  TimeSlotSettingsScreen: undefined
  MedicationReportDetail: {
    prescriptionId: number
    prescription: Prescription
    issueDate: string
  }
}

const Stack = createStackNavigator<TakingDrugStackParamList>()

export function TakingDrugNavigator({ navigation, route }) {
  if (route.state && route.state.index > 0) {
    navigation.setOptions({ tabBarVisible: false })
  } else {
    navigation.setOptions({ tabBarVisible: true })
  }
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitleAlign: "center",
        headerBackTitle: null,
        headerBackImage: () => <CustomHeaderBackImage />,
        headerStyle: styles.HEADER,
        headerTintColor: color.palette.orange,
        headerTitleStyle: styles.HEADER_TITLE,
      }}
    >
      <Stack.Screen
        name="MedicationSchedule"
        component={MedicationScheduleScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MedicationScheduleDetail"
        component={MedicationScheduleDetailScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NoticeList"
        component={NoticeListScreen}
        options={NoticeListScreen.navigationOptions}
      />
      <Stack.Screen
        name="NoticeDetail"
        component={NoticeDetailScreen}
        options={NoticeDetailScreen.navigationOptions}
      />
      <Stack.Screen
        name="MedicationReportDetail"
        component={MedicationReportDetailScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PrescriptionDetail"
        component={PrescriptionDetailScreen}
        options={PrescriptionDetailScreen.navigationOptions}
      />
    </Stack.Navigator>
  )
}
