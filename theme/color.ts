import { avocadoPallete, palette } from "./palette"

/**
 * Roles for colors.  Prefer using these over the palette.  It makes it easier
 * to change things.
 *
 * The only roles we need to place in here are the ones that span through the app.
 *
 * If you have a specific use-case, like a spinner color.  It makes more sense to
 * put that in the <Spinner /> component.
 */
export const color = {
  /**
   * The palette is available to use, but prefer using the name.
   */
  palette,
  /**
   * A helper for making something see-thru. Use sparingly as many layers of transparency
   * can cause older Android devices to slow down due to the excessive compositing required
   * by their under-powered GPUs.
   */
  transparent: "rgba(0, 0, 0, 0)",

  input: palette.white,
  /**
   * The screen background.
   */
  background: "#fcfbf9", //palette.white,
  appBackground: palette.white,
  /**
   * The main tinting color.
   */
  primary: palette.orange,
  /**
   * The main tinting color, but darker.
   */
  primaryDarker: palette.orangeDarker,
  /**
   * A subtle color used for borders and lines.
   */
  line: palette.pale,
  /**
   * The default color of text in many components.
   */
  text: palette.gray1,

  placeholder: "rgb(217, 217, 217)",
  /**
   * Secondary information.
   */
  dim: palette.lightGrey,
  /**
   * Error messages and icons.
   */
  error: palette.angry,
  /**
   * StatusBar color when a modal open
   */
  dimmedStatusBar: "rgba(0,0,0,0.45)",

  /**
   * Storybook background for Text stories, or any stories where
   * the text color is color.text, which is white by default, and does not show
   * in Stories against the default white background
   */
  storybookDarkBg: palette.black,

  /**
   * Storybook text color for stories that display Text components against the
   * white background
   */
  storybookTextColor: palette.black,

  // DesignSystem - Seed Token
  actionPrimary: {
    default: avocadoPallete.avcdPaprica500,
    pressed: avocadoPallete.avcdPaprica600,
    disabled: avocadoPallete.avcdPaprica100,
  },
  actionSecondary: {
    default: avocadoPallete.avcdNeutralblue050,
    pressed: avocadoPallete.avcdNeutralblue100,
    disabled: avocadoPallete.avcdNeutralblue010,
  },

  backgrounds: {
    default: avocadoPallete.avcdNeutralblue040,
    subdued: avocadoPallete.avcdNeutralblue010,
    darkmode: avocadoPallete.avcdNeutralblue800,
    lightmode: avocadoPallete.avcdWhite,
  },
  textColor: {
    default: avocadoPallete.avcdNeutralblue900,
    subdued: avocadoPallete.avcdNeutralblue500,
    action: avocadoPallete.avcdNeutralblue800,
    disabled: avocadoPallete.avcdNeutralblue300,
    primary: avocadoPallete.avcdPaprica500,
    critical: avocadoPallete.avcdRed500,
    warning: avocadoPallete.avcdYellow500,
    success: avocadoPallete.avcdGreen500,
    accent: avocadoPallete.avcdBlue500,
  },
  textOn: {
    primary: avocadoPallete.avcdWhite,
  },

  borders: {
    neutralSubded: avocadoPallete.avcdNeutralblue100,
    dividerBetweenUI: avocadoPallete.avcdNeutralblue040,
  },
  surface: {
    primaryPressed: avocadoPallete.avcdPaprica100,
    primarySubded: avocadoPallete.avcdPaprica040,
    successDefault: avocadoPallete.avcdGreen050,
    uiDefault: avocadoPallete.avcdWhite,
    uiSubdued: avocadoPallete.avcdGray005,
    uiPressed: avocadoPallete.avcdGray010,
    neutralDefault: avocadoPallete.avcdNeutralblue040,
    neutralSubded: avocadoPallete.avcdNeutralblue020,
  },
  decorative: {
    surfaceGreen: avocadoPallete.avcdGreen030,
    surfaceSolidPrimary: avocadoPallete.avcdPaprica400,
    surfaceSolidNeutral: avocadoPallete.avcdNeutralblue500,
  },
}
