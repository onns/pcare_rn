import { Platform } from "react-native"

import { color } from "./color"

export const foregroundColor = "white"
export const backgroundColor = "#f2F4f5" // Agenda's reservations bg color

export const separatorColor = "#f2F4f5" // not in use
export const processedColor = "#a7e0a3" // not in use
export const processingColor = "#ffce5c" // not in use
export const failedColor = "#f67e7e" // not in use
export const textSecondaryColor = "#7a92a5" // not in use

export const textDefaultColor = "#2d4150"
export const textColor = "#43515c"
export const textLinkColor = "#00BBF2"

export const textDayFontFamily = "System"
export const textMonthFontFamily = Platform.select({
  ios: "NotoSansKR-Bold",
  android: "NotoSansKR-Bold-Hestia",
})
export const textDayHeaderFontFamily = Platform.select({
  ios: "NotoSansKR-Regular",
  android: "NotoSansKR-Regular-Hestia",
})
export const todayButtonFontFamily = Platform.select({
  ios: "NotoSansKR-Regular",
  android: "NotoSansKR-Regular-Hestia",
})

export const textDayFontWeight = "300"
export const textMonthFontWeight = "300"
export const textDayHeaderFontWeight: any = undefined
export const todayButtonFontWeight = "600"

export const textDayFontSize = 16
export const textMonthFontSize = 20
export const textDayHeaderFontSize = 13
export const todayButtonFontSize = 14

export const textDayStyle: any = undefined
export const dotStyle: any = undefined
export const arrowStyle: any = undefined

export const calendarBackground = foregroundColor
export const textSectionTitleColor = "#b6c1cd"
export const selectedDayBackgroundColor = textLinkColor
export const selectedDayTextColor = foregroundColor
export const todayBackgroundColor: any = undefined
export const todayTextColor = color.primary
export const dayTextColor = textDefaultColor
export const textDisabledColor = "#d9e1e8"
export const dotColor = textLinkColor
export const selectedDotColor = foregroundColor
export const disabledDotColor: any = undefined
export const todayDotColor: any = undefined
export const arrowColor = textLinkColor
export const disabledArrowColor = "#d9e1e8"
export const monthTextColor = "#272727"
export const indicatorColor: any = undefined // use the default color of React Native ActivityIndicator
export const agendaDayTextColor = "#7a92a5"
export const agendaDayNumColor = "#7a92a5"
export const agendaTodayColor = textLinkColor
export const agendaKnobColor = "#f2F4f5"
export const todayButtonTextColor = textLinkColor
export const todayButtonPosition: any = undefined // right' / 'left'(default)
