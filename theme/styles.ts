import { TextStyle, ViewStyle } from "react-native"

import { spacing } from "./"
import { color } from "./color"
import { typography } from "./typography"

const bottomSheetButtonHeight = 64
const bottomSheetHeaderHeight = 64
const largeRadiusButtonHeight = 40

const BOTTOM_SHEET_BUTTON: ViewStyle = {
  height: bottomSheetButtonHeight,
  flexDirection: "row",
  justifyContent: "flex-start",
  alignItems: "center",
  paddingHorizontal: 23,
  backgroundColor: color.palette.white,
  // borderTopWidth: 0.5,
  // borderTopColor: color.line,
}
const BOTTOM_SHEET_BUTTON_TEXT: TextStyle = {
  ...typography.buttons.scaledUp,
  color: color.textColor.action,
}
const BOTTOM_SHEET_CANCEL_BUTTON_TEXT: TextStyle = {
  ...typography.buttons.scaledUp,
  color: color.textColor.action,
}
const BOTTOM_SHEET_LAST_BUTTON: ViewStyle = {
  ...BOTTOM_SHEET_BUTTON,
}
const BOTTOM_SHEET_CANCEL_BUTTON: ViewStyle = {
  height: 56,
  backgroundColor: color.surface.uiDefault,
  borderRadius: 12,

  justifyContent: "center",
  alignItems: "center",
  marginBottom: 24,
}
const BOTTOM_SHEET_HEADER: ViewStyle = {
  height: bottomSheetHeaderHeight,
  paddingHorizontal: 23,
  backgroundColor: color.surface.uiDefault,
  borderTopLeftRadius: 12,
  borderTopRightRadius: 12,
  justifyContent: "center",
  alignItems: "flex-start",
}

const BOTTOM_SHEET_HEADER_TEXT: TextStyle = {
  ...typography.heading.section,
  color: color.textColor.default,
}
const HEADER = {
  // borderBottomWidth: 1,
  borderBottomColor: color.line,
  elevation: 0,
}
const HEADER_TITLE = {
  ...typography.title3,
}

const TOAST_TEXT: TextStyle = {
  fontFamily: typography.primary,
  color: color.palette.offWhite,
  fontSize: 14,
  marginTop: 20,
  paddingHorizontal: 8,
  textAlign: "center",
}
const TOAST_TEXT2: TextStyle = {
  color: color.palette.offWhite,
  fontSize: 12,
  marginBottom: 20,
  paddingHorizontal: 8,
  textAlign: "center",
}

const TOAST_VIEW: ViewStyle = {
  backgroundColor: "#202124",
  // height: 140,
  minHeight: 100,
  marginTop: 64,
  marginHorizontal: 10,
  marginBottom: 20,
  borderRadius: 6,
  // paddingVertical: 16,
  // paddingBottom: 16,
  // justifyContent: "center",
  alignContent: "center",
}

const LARGE_RADIUS_BUTTON: ViewStyle = {
  height: largeRadiusButtonHeight,
  borderRadius: 20,
  borderWidth: 1,
  borderColor: color.palette.paleSalmon,
  justifyContent: "center",
  alignItems: "center",
  marginHorizontal: 56,
  marginTop: 16,
}
const LARGE_RADIUS_BUTTON_TEXT: TextStyle = {
  ...typography.subButton,
  lineHeight: 20,
  color: color.palette.orange,
}
export const styles = {
  bottomSheetButtonHeight: bottomSheetButtonHeight,
  bottomSheetHeaderHeight: bottomSheetHeaderHeight,
  BOTTOM_SHEET_BUTTON: BOTTOM_SHEET_BUTTON,
  BOTTOM_SHEET_BUTTON_TEXT: BOTTOM_SHEET_BUTTON_TEXT,
  BOTTOM_SHEET_CANCEL_BUTTON: BOTTOM_SHEET_CANCEL_BUTTON,
  BOTTOM_SHEET_CANCEL_BUTTON_TEXT: BOTTOM_SHEET_CANCEL_BUTTON_TEXT,
  BOTTOM_SHEET_LAST_BUTTON: BOTTOM_SHEET_LAST_BUTTON,
  BOTTOM_SHEET_HEADER: BOTTOM_SHEET_HEADER,
  BOTTOM_SHEET_HEADER_TEXT: BOTTOM_SHEET_HEADER_TEXT,
  TOAST_TEXT: TOAST_TEXT,
  TOAST_TEXT2: TOAST_TEXT2,
  TOAST_VIEW: TOAST_VIEW,
  HEADER: HEADER,
  HEADER_TITLE: HEADER_TITLE,
  LARGE_RADIUS_BUTTON: LARGE_RADIUS_BUTTON,
  LARGE_RADIUS_BUTTON_TEXT: LARGE_RADIUS_BUTTON_TEXT,
}
