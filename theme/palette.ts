export const palette = {
  realBlack: "#000000",
  black: "#2F3233",
  white: "#ffffff",
  offWhite: "#e6e6e6",

  gray1: "#33302f",
  gray2: "#7f7f7f",
  gray3: "#b2b2b2",
  gray4: "#e5e5e5",
  gray6: "#EBEAEA",
  gray5: "#f9f7f4",

  copper: "#d83e25",
  orange: "#ff5b2c",
  brightOrange: "#FF7042",
  orange2: "#F57D5B", // temporarily
  red70: "#FF8C6B",
  red50: "#FFAD95",
  red10: "#FFEFEA",
  red20: "#FFDED5",
  lightOrange: "#ffece2",
  softBloom: "#fff3ef",

  brightCyan3: "#5ac9ef",
  deepLavender: "#8649ba",
  peaGreen: "#7ac713",
  marigold: "#ffc300",
  salmon: "#ff6965",
  salmon2: "#ff8e6e",
  paleSalmon: "#ffbca8",
  lightGrayishOrange: "#FCFBF9",
  veryPaleRed: "#FFE6DE",
  veryLightPink: "#fff3ed",
  veryLightPink3: "#f6f3f2",
  salmonGray: "#D8D8D8",

  greenishTeal: "#2fcb89",
  darkMint: "#40cb90",

  orangeDarker: "#EB9918",
  macaroniAndCheese: "#ffbd26",
  pissYellow: "rgb(229, 181, 23)",
  lighterGrey: "#CDD4DA",
  grey7: "rgb(249, 249, 249)",
  grey6: "rgb(239, 239, 239)",
  pale: "#eeece9",
  grey4: "rgb(230, 230, 230)",
  grey5: "rgb(228, 228, 228)",
  lightGrey2: "rgb(217, 217, 217)",
  grey2: "rgb(206, 206, 206)",
  grey30: "#C2C1C1",
  grey50: "#999797",
  grey70: "#707070",
  lightGrey: "#939AA4",
  grey3: "rgb(168, 168, 168)",
  brownGrey: "rgb(153, 153, 153)",
  grey1: "rgb(132, 132, 132)",
  grey: "rgb(85, 85, 85)",
  charcoalGrey: "#393e46",
  lightBlack2: "#33302f",
  lightBlack: "rgb(52, 52, 52)",

  cordovanBrown: "#413632",
  orangeyRed: "rgb(232, 71, 21)",
  angry: "#dd3333",
  tomato: "rgb(231, 57, 48)",
  appleGreen: "rgb(120, 201, 26)",
  mintyGreen: "rgb(32, 224, 130)",
  lightEmeraldGreen: "#1BB775",
  foreverGreen: "#2E8C64",
  darkSkyBlue: "rgb(67, 154, 242)",
  waterBlue: "rgb(25, 163, 226)",
  brightCyan: "rgb(76, 211, 229)",
  brightCyan2: "rgb(82, 211, 229)",
}

export const avocadoPallete = {
  // Black and white
  avcdWhite: "#ffffff",

  // Blue
  avcdBlue500: "#0085FF",

  // Green
  avcdGreen030: "#F2FAF5",
  avcdGreen050: "#E0F3E8",
  avcdGreen500: "#338054",

  // Yellow
  avcdYellow500: "#F8CB2E",

  // Red
  avcdRed500: "#DD3F3F",

  // Paprica
  avcdPaprica040: "#FFEFE7",
  avcdPaprica100: "#FED7C1",
  avcdPaprica400: "#FF7629",
  avcdPaprica500: "#FC630E",
  avcdPaprica600: "#DB4E00",

  // Gray
  avcdGray005: "#FCFCFC",
  avcdGray010: "#F5F5F5",

  // Nuetral Blue
  avcdNeutralblue010: "#F9FAFB",
  avcdNeutralblue020: "#F6F7F8",
  avcdNeutralblue040: "#F0F2F4",
  avcdNeutralblue050: "#EBEDF1",
  avcdNeutralblue100: "#E6E8ED",
  avcdNeutralblue300: "#C3C9D4",
  avcdNeutralblue500: "#7F8694",
  avcdNeutralblue600: "#565E6D",
  avcdNeutralblue700: "#484F5C",
  avcdNeutralblue800: "#383D46",
  avcdNeutralblue900: "#1B1E25",
}
