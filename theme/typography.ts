import { Platform } from "react-native"
import { Paragraph } from "tamagui"

import { Bold } from "@tamagui/lucide-icons"

import { textColor } from "./calendar"
import { color } from "./color"

/**
 * Just the font names.
 *
 * The various styles of fonts are defined in the <Text /> component.
 */

export const textElement = {
  weight: {
    extraBold: "SpoqaHanSansNeo-Bold",
    bold: "SpoqaHanSansNeo-Medium",
    semibold: "SpoqaHanSansNeo-Medium",
    medium: "SpoqaHanSansNeo-Medium",
    regular: "SpoqaHanSansNeo-Regular",
  },
  lineHeight: {
    tight: 1.2,
    base: 1.3,
    relaxed: 1.4,
  },
  letterSpacing: {
    tight: -0.5,
    base: -0.3,
    relaxed: 0.5,
  },
  fontSize: {
    xlarge: 32,
    largest: 27,
    large: 23,
    medium: 19,
    baseScaledUp: 16,
    base: 15,
    small: 14,
    smaller: 13,
  },
}

export const typography = {
  /**
   * The primary font.  Used in most places. "Noto Sans CJK KR"
   */
  primary: Platform.select({
    ios: "NotoSansKR-Regular",
    android: "NotoSansKR-Regular-Hestia",
  }),
  bold: Platform.select({
    ios: "NotoSansKR-Bold",
    android: "NotoSansKR-Bold-Hestia",
  }),

  title: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Bold",
      android: "NotoSansKR-Bold-Hestia",
    }),
    fontSize: 32,
    lineHeight: 42,
    color: color.palette.lightBlack2,
  },
  title2: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Bold",
      android: "NotoSansKR-Bold-Hestia",
    }),
    fontSize: 20,
    lineHeight: 29,
    color: color.palette.lightBlack2,
  },
  title3: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Bold",
      android: "NotoSansKR-Bold-Hestia",
    }),
    fontSize: 17,
    lineHeight: 25,
    color: color.palette.lightBlack2,
  },
  title4: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Bold",
      android: "NotoSansKR-Bold-Hestia",
    }),
    fontSize: 15,
    lineHeight: 25,
    color: color.palette.lightBlack2,
  },
  body: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Regular",
      android: "NotoSansKR-Regular-Hestia",
    }),
    fontSize: 16,
    lineHeight: 24,
    color: color.palette.lightBlack2,
  },
  sub1: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Bold",
      android: "NotoSansKR-Bold-Hestia",
    }),
    fontSize: 13,
    lineHeight: 19,
    color: color.palette.lightBlack2,
  },
  sub2: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Regular",
      android: "NotoSansKR-Regular-Hestia",
    }),
    fontSize: 13,
    lineHeight: 19,
    color: color.palette.lightBlack2,
  },
  button: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Bold",
      android: "NotoSansKR-Bold-Hestia",
    }),
    fontSize: 17,
    lineHeight: 25,
    color: color.palette.lightBlack2,
  },
  subButton: {
    fontFamily: Platform.select({
      ios: "NotoSansKR-Bold",
      android: "NotoSansKR-Bold-Hestia",
    }),
    fontSize: 14,
    color: color.palette.lightBlack2,
  },

  // DesginSystem - Seed Token
  display: {
    Xlarge: {
      fontSize: textElement.fontSize.xlarge,
      fontFamily: textElement.weight.extraBold,
      lineHeight: textElement.fontSize.xlarge * textElement.lineHeight.base,
      color: color.textColor.default,
    },
    large: {
      fontSize: textElement.fontSize.largest,
      fontFamily: textElement.weight.bold,
      lineHeight: textElement.fontSize.largest * textElement.lineHeight.base,
      color: color.textColor.default,
    },
    medium: {
      fontSize: textElement.fontSize.large,
      fontFamily: textElement.weight.bold,
      lineHeight: textElement.fontSize.large * textElement.lineHeight.base,
      color: color.textColor.default,
    },
    small: {
      fontSize: textElement.fontSize.medium,
      fontFamily: textElement.weight.bold,
      lineHeight: textElement.fontSize.medium * textElement.lineHeight.base,
      color: color.textColor.default,
    },
  },
  heading: {
    page: {
      fontSize: textElement.fontSize.baseScaledUp,
      fontFamily: textElement.weight.bold,
      lineHeight: textElement.fontSize.baseScaledUp * textElement.lineHeight.base,
      color: color.textColor.default,
    },
    section: {
      fontSize: textElement.fontSize.medium,
      fontFamily: textElement.weight.bold,
      lineHeight: textElement.fontSize.medium * textElement.lineHeight.base,
      color: color.textColor.default,
    },
    subheading: {
      fontSize: textElement.fontSize.base,
      fontFamily: textElement.weight.bold,
      lineHeight: textElement.fontSize.base * textElement.lineHeight.base,
      color: color.textColor.default,
    },
  },
  ui: {
    scaledUp: {
      fontSize: textElement.fontSize.baseScaledUp,
      fontFamily: textElement.weight.regular,
      lineHeight: textElement.fontSize.base * textElement.lineHeight.tight,
      color: color.textColor.default,
    },
    default: {
      fontSize: textElement.fontSize.base,
      fontFamily: textElement.weight.regular,
      lineHeight: textElement.fontSize.base * textElement.lineHeight.tight,
      color: color.textColor.default,
    },
    defaultEmphasized: {
      fontSize: textElement.fontSize.base,
      fontFamily: textElement.weight.bold,
      lineHeight: textElement.fontSize.base * textElement.lineHeight.tight,
      color: color.textColor.default,
    },
  },
  buttons: {
    scaledUp: {
      fontSize: textElement.fontSize.baseScaledUp,
      fontFamily: textElement.weight.medium,
      lineHeight: textElement.fontSize.baseScaledUp * textElement.lineHeight.base,
      color: color.textColor.default,
    },
    default: {
      fontSize: textElement.fontSize.base,
      fontFamily: textElement.weight.medium,
      lineHeight: textElement.fontSize.base * textElement.lineHeight.base,
      color: color.textColor.default,
    },
    small: {
      fontSize: textElement.fontSize.small,
      fontFamily: textElement.weight.medium,
      lineHeight: textElement.fontSize.small * textElement.lineHeight.base,
      color: color.textColor.default,
    },
  },
  captions: {
    default: {
      fontSize: textElement.fontSize.small,
      fontFamily: textElement.weight.regular,
      lineHeight: textElement.fontSize.small * textElement.lineHeight.base,
      color: color.textColor.default,
    },
    small: {
      fontSize: textElement.fontSize.smaller,
      fontFamily: textElement.weight.regular,
      lineHeight: textElement.fontSize.smaller * textElement.lineHeight.base,
      color: color.textColor.default,
    },
  },
  paragraph: {
    scaledUp: {
      fontSize: textElement.fontSize.medium,
      fontFamily: textElement.weight.regular,
      lineHeight: textElement.fontSize.medium * textElement.lineHeight.relaxed,
      color: color.textColor.default,
    },
    default: {
      fontSize: textElement.fontSize.base,
      fontFamily: textElement.weight.regular,
      lineHeight: textElement.fontSize.base * textElement.lineHeight.relaxed,
      color: color.textColor.default,
    },
    small: {
      fontSize: textElement.fontSize.small,
      fontFamily: textElement.weight.regular,
      lineHeight: textElement.fontSize.small * textElement.lineHeight.relaxed,
      color: color.textColor.default,
    },
  },
}
