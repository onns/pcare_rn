import { createFont, createTamagui } from "tamagui"

import { createTokens } from "@tamagui/core"
import { shorthands } from "@tamagui/shorthands"
import { themes, tokens as baseTokens } from "@tamagui/theme-base"

import { animations } from "./constants/animations"
import { textElement } from "./theme/typography"

const spoqaHanSansFont = createFont({
  family: textElement.weight.extraBold,
  size: {
    1: 12,
    2: 14,
    3: 15,
    4: 16,
    $smaller: 13,
    $small: 14,
    $true: 15,
    $base: 15,
    $baseScaledUp: 16,
    $medium: 19,
    $large: 23,
    $largest: 27,
    $xlarge: 32,
    // ...
  },
  lineHeight: {
    1: 17,
    2: 17,
    3: 18,
    4: 18,
    // ...
  },
  weight: {
    4: "300",
    6: "600",
  },
  color: {
    subdued: "#7F8694",
  },
  letterSpacing: {
    5: 2,
    6: 1,
    7: 0,
    8: -1,
    9: -2,
    10: -3,
    12: -4,
    14: -5,
    15: -6,
  },
  // (native only) swap out fonts by face/style
  face: {
    300: { normal: "SpoqaHanSansNeo-Regular" },
    400: { normal: "SpoqaHanSansNeo-Medium" },
    800: { normal: "SpoqaHanSansNeo-Bold" },
  },
  // you may also set `transform` as textTransform values
  // and `style` as fontStyle values
})

// const headingFont = createInterFont({
//   size: {
//     6: 15,
//   },
//   transform: {
//     6: "uppercase",
//     7: "none",
//   },
//   weight: {
//     6: "400",
//     7: "700",
//   },
//   color: {
//     6: "$colorFocus",
//     7: "$color",
//   },
//   letterSpacing: {
//     5: 2,
//     6: 1,
//     7: 0,
//     8: -1,
//     9: -2,
//     10: -3,
//     12: -4,
//     14: -5,
//     15: -6,
//   },
//   face: {
//     700: { normal: "InterBold" },
//   },
// })

// const bodyFont = createInterFont(
//   {
//     face: {
//       700: { normal: "InterBold" },
//     },
//   },
//   {
//     sizeSize: (size) => Math.round(size * 1.1),
//     sizeLineHeight: (size) => Math.round(size * 1.1 + (size > 20 ? 10 : 10)),
//   },
// )

export const tokens = createTokens({
  ...baseTokens,
  // size,
  // space: { ...size, '-1': -5, '-2': -10 },
  // radius: { 0: 0, 1: 3 },
  // zIndex: { 0: 0, 1: 100, 2: 200 },
  color: {
    action: "#FC630E",
    surfaceNeutral: "#F0F2F4",
    white: "#fff",
    black: "#000",
  },
})

const config = createTamagui({
  animations,
  defaultTheme: "light",
  shouldAddPrefersColorThemes: true,
  themeClassNameOnRoot: true,
  shorthands,
  fonts: {
    heading: spoqaHanSansFont,
    body: spoqaHanSansFont,
  },
  // fonts: {
  //   heading: headingFont,
  //   body: bodyFont,
  // },
  themes,
  tokens,
  media: {
    xs: { maxWidth: 660 },
    sm: { maxWidth: 800 },
    md: { maxWidth: 1020 },
    lg: { maxWidth: 1280 },
    xl: { maxWidth: 1420 },
    xxl: { maxWidth: 1600 },
    gtXs: { minWidth: 660 + 1 },
    gtSm: { minWidth: 800 + 1 },
    gtMd: { minWidth: 1020 + 1 },
    gtLg: { minWidth: 1280 + 1 },
    short: { maxHeight: 820 },
    tall: { minHeight: 820 },
    hoverNone: { hover: "none" },
    pointerCoarse: { pointer: "coarse" },
  },
})

export type AppConfig = typeof config

declare module "tamagui" {
  // overrides TamaguiCustomConfig so your custom types
  // work everywhere you import `tamagui`
  interface TamaguiCustomConfig extends AppConfig {}
}

export default config
