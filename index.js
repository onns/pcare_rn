/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-unused-expressions */
// This is the first file that ReactNative will run when it starts up.
//
// We jump out of here immediately and into our main entry point instead.
//
// It is possible to have React Native load our main module first, but we'd have to
// change that in both AppDelegate.m and MainApplication.java.  This would have the
// side effect of breaking other tooling like mobile-center and react-native-rename.
//
// It's easier just to leave it here.
import React from "react"
import "react-native-gesture-handler"
import "expo-asset"
import codePush from "react-native-code-push"
import { Text, View } from "react-native"
import { registerRootComponent } from "expo"
import App from "./App"
import { name as appName } from "./app.json"

if (Platform.OS === "ios") {
  // Polyfills required to use Intl with Hermes engine
  require("@formatjs/intl-getcanonicallocales/polyfill").default
  require("@formatjs/intl-locale/polyfill").default
  require("@formatjs/intl-pluralrules/polyfill").default
  require("@formatjs/intl-pluralrules/locale-data/en").default
  require("@formatjs/intl-numberformat/polyfill").default
  require("@formatjs/intl-numberformat/locale-data/en").default
  require("@formatjs/intl-datetimeformat/polyfill").default
  require("@formatjs/intl-datetimeformat/locale-data/en").default
  require("@formatjs/intl-datetimeformat/add-all-tz").default
}

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_START,
  installMode: codePush.InstallMode.IMMEDIATE,
  updateDialog: false,
}

function Test() {
  return (
    <View>
      <Text>Open up App.js to start working on your app!</Text>
    </View>
  )
}

// AppRegistry.registerComponent(appName, () => codePush(codePushOptions)(App))

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately
registerRootComponent(codePush(codePushOptions)(App))
