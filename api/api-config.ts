export interface ApiConfig {
  /**
   * The URL of the api.
   */
  productionUrl: string
  stagingUrl: string
  localUrl1: string
  localUrl2: string
  localUrl3: string
  localUrl4: string
  localUrl5: string
  localUrl6: string

  /**
   * The URL of static images
   */
  publicUrl: string

  /**
   * Milliseconds before we timeout the request.
   */
  timeout: number
}
/**
 * The default configuration for the app.
 */
export const DEFAULT_API_CONFIG: ApiConfig = {
  productionUrl: "https://prod.onionsapp.com/",
  stagingUrl: "https://staging.onionsapp.com/",
  publicUrl: "http://public.onionsapp.com/",
  localUrl1: "http://local1.onionsapp.com:8000/",
  localUrl2: "http://local2.onionsapp.com:8000/",
  localUrl3: "http://local3.onionsapp.com:8000/",
  localUrl4: "http://local4.onionsapp.com:8000/",
  localUrl5: "http://local5.onionsapp.com:8000/",
  localUrl6: "http://local6.onionsapp.com:8000/",
  timeout: 20000,
}
