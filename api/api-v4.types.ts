export type DiseaseSummary = {
  id: number
  code: string
  name: string
  feature: string
}

type DiseaseDescription = {
  id: number
  title: string
  feature: string
  desc: string
}

/** 질병 조회 응답 */
export type Disease = {
  id: number
  code: string
  name: string
  nameEn: string
  feature: string // 특징 (요약)
  /** 상세 설명 */
  description: DiseaseDescription[]
  /** nps 통계정보 */
  nps: {
    /** 진단현황 */
    diagnosis: {
      id: number
      year: number
      /** 나이대 */
      age: string
      /** 성별	enum	[M, F] */
      gender: string
      /** 수치	int */
      count: number
      /** 비율	num */
      percent: number
      /** 	진료과	obj	 */
      department: {
        id: number
        /** 과목명칭	str	 */
        departmentName: string
        /** 순위	int	 */
        rank: number
        /** 수치	int */
        count: number
        /** 비율	num	 */
        percent: number
      }
      /** 동반질병	obj	 */
      disease: {
        id: number
        /** 질병코드	str */
        diseaseCode: string
        /** 	질병명칭	str	 */
        diseaseName: string
        /** 순위	int	 */
        rank: number
        /** 수치	int	 */
        count: number
        /** 비율	num */
        percent: number
      }
    }
  }
}
