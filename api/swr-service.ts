import { ApiResponse, ApisauceInstance, create } from "apisauce"

import remoteConfig from "@react-native-firebase/remote-config"

import { DEFAULT_API_CONFIG } from "./api-config"
import HttpException from "./exceptions/HttpException"

// Apisauce instance for SWR
const instance = create({
  // baseURL: DEFAULT_API_CONFIG.productionUrl,
  baseURL: remoteConfig().getString("v4_api_url"),
  timeout: DEFAULT_API_CONFIG.timeout,
  headers: {
    Accept: "application/json",
    ServiceId: "85326991-3865-4224-8386-7b3fd045b7ca",
  },
})

type Args = Parameters<ApisauceInstance["get"]>

/**
 * Wrapper around API instance.
 * By default, apisauce does not t  hrow on failure.
 *
 * In order for useSWR() to correctly return the error,
 * the fetcher needs to throw on failure.
 */
const client = {
  /**
   * We're forwarding the types and arguments and then throwing on error.
   */
  ...instance,
  get: async <T>(...args: Args) => {
    const res = (await instance.get(...args)) as ApiResponse<T>
    return util.throwOnError<T>(res)
  },
  // override more methods as needed
}

// Logging
__DEV__ && client.addMonitor(console.log)

// API utilities
const util = {
  throwOnError: <T>(response: ApiResponse<T>) => {
    if (!response.ok) {
      const error = new HttpException(
        response.status || "unknown",
        response.problem,
        response.config?.url || "unknown",
        response,
      )

      __DEV__ && console.log(error)

      throw error
    } else {
      // all good!
      return response.data!
    }
  },
}

export const swrService = {
  client,
}
