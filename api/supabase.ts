// Fix URLSearchParms not implemented error
// https://github.com/facebook/react-native/issues/23922#issuecomment-648096619
import "react-native-url-polyfill/auto"

import AsyncStorage from "@react-native-async-storage/async-storage"
import { createClient } from "@supabase/supabase-js"

import { Database } from "./supabase-types"

export const supabaseUrl = process.env.SUPABASE_URL
export const supabaseKey = process.env.SUPABASE_KEY

// export const supabase = createClient<Database>(supabaseUrl, supabaseKey, {
export const supabase = createClient<Database>(supabaseUrl, supabaseKey, {
  auth: {
    localStorage: AsyncStorage as any,
    autoRefreshToken: true,
    persistSession: true,
    detectSessionInUrl: false,
  },
})

export async function getMemos(params: type) {
  return await supabase.from("daily_memo").select("*")
}
type MemosResponse = Awaited<ReturnType<typeof getMemos>>
export type MemosResponseSuccess = MemosResponse["data"]
export type MemosResponseError = MemosResponse["error"]
