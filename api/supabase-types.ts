/* eslint-disable camelcase */
export type Json = string | number | boolean | null | { [key: string]: Json } | Json[]

export interface Database {
  public: {
    Tables: {
      point_activities: {
        Row: {
          created_at: string | null
          desc: string
          drug_take_id: string | null
          id: number
          p_id: number
          point: number
          point_type: number
          timeslot_id: number | null
          user_id: number
          alarm_name: string | null
        }
        Insert: {
          created_at?: string | null
          desc: string
          drug_take_id?: string | null
          id?: number
          p_id: number
          point?: number
          point_type: number
          timeslot_id?: number | null
          user_id: number
        }
        Update: {
          created_at?: string | null
          desc?: string
          drug_take_id?: string | null
          id?: number
          p_id?: number
          point?: number
          point_type?: number
          timeslot_id?: number | null
          user_id?: number
        }
      }
      total_points: {
        Row: {
          created_at: string | null
          id: number
          total_points: number
          user_id: number
        }
        Insert: {
          created_at?: string | null
          id?: number
          total_points: number
          user_id: number
        }
        Update: {
          created_at?: string | null
          id?: number
          total_points?: number
          user_id?: number
        }
      }
      daily_memo: {
        Row: {
          id: number
          user_id: number
          rating: number
          content: string
          created_at: string | null
        } // The data expected to be returned from a "select" statement.
        Insert: {
          id: number
          user_id: number
          rating: number
          content: string
          created_at: string | null
        } // The data expected passed to an "insert" statement.
        Update: {
          id: number
          user_id: number
          rating: number
          content: string
          created_at: string | null
        } // The data expected passed to an "update" statement.
      }
      daily_memo_dev: {
        Row: {
          id: number
          user_id: number
          rating: number
          content: string
          created_at: string | null
        } // The data expected to be returned from a "select" statement.
        Insert: {
          id: number
          user_id: number
          rating: number
          content: string
          created_at: string | null
        } // The data expected passed to an "insert" statement.
        Update: {
          id: number
          user_id: number
          rating: number
          content: string
          created_at: string | null
        } // The data expected passed to an "update" statement.
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
