import { GeneralApiProblem } from "./api-problem"

export interface User {
  username: string
  email: string
  first_name: string
  last_name: string
  // user_profile: string,
  password: string
  show_activity_button?: boolean
  name: string
  nickname: string
  phone: string | null
  is_authenticated: false
  is_agreed: false
  user_type: string
  newly_agreed: "y" | "n" | "pre" | "post" | null
  newly_agreed_date: Date | null
  address: string
  profile_image: string
  gender: "" | "male" | "female"
  birth: Date | null
  firebase_token0: string
  dur_preg: "y" | "n"
  dur_byg: "y" | "n"
  dur_hng: "y" | "n"
  dur_dis: "y" | "n"
  dur_nosale: "y" | "n"
  presc_noti: "y" | "n"
  dur_noti: "y" | "n"
  ad_noti: "y" | "n"
  ad_noti_date: Date | null
}

export interface FamilyMember {
  id: number
  created: Date | string | number
  modified: Date | string | number
  name: string
  birth_date: Date
  gender: string
  img: string
  status?: string
  user: number
  family_user: any
  familyMemberKey?: string
  family_user_phone: number
}

export interface PrescriptionItem {
  key: number
  // diseases: string,
  // hospital: types.reference(Hospital),
  user: number
  upload_user: number
  children: []
  // issue_date: types.Date,
  // confirm_date: types.maybe(types.Date),
  // status: string,
  // doctor: string,
  // return_reason: types.string,
  image: string
}

export interface Drug {
  id: number
  kd_code: number
  created: string
  modified: string
  ref_code: string
  name: string
  company: string
  efficacy_code: number
  efficacy_category: string
  image_url: string
  prescription_otc: string
  medicine_category: string
  ingredients: string
  mechanism_short: string
  mechanism_detail: string
  dosage: string
  caution: string
  manual_url: string
  howto_storage: string
  exp_date: string
}

export interface MedicationSchedule {
  id: number
  disease: any
  start: string
  end: string
  issue_date: string
}

export interface MedicationProgress {
  date: string
  ratio: number
}

// interface SignUpParams {
//   username: string,
//   email: string,
//   password1: string,
//   password2: string
// }

export type PutUserParams = {
  username: string
  email: string
  // first_name: string,
  // last_name: string,
  password?: string
  // password1: string,
  // password2: string,
  user_profile: any
}

export type SignUpParams = {
  // username: string,
  email: string
  // first_name: string,
  // last_name: string,
  // password: string,
  password1: string
  password2: string
  // user_profile: any
}

export type LoginParams = {
  username: string
  email: string
  password: string
}

export interface UpdateUserParams {
  username?: string
  email?: string
  user_profile?: {
    phone?: string
    is_agree?: boolean
    address?: string
  }
}

export interface Notice {
  id: number
  created: Date | string | number
  title: string
  subtitle: string
  content: string
}
export interface GraphInit {
  count: number | null
  next: string | null
  previous: string | null
  results: []
}
export interface Graph {
  count: number
  sum: number
  avg: number
  min: number
  max: number
  daily: GraphObject
  monthly: GraphObject
}
interface GraphObject {
  count: number
  next: string | null
  previous: string | null
  results: []
}

export type SignUpResult = { kind: "ok"; key: string } | GeneralApiProblem
export type LoginResult = { key: string } | GeneralApiProblem

export type GetUserResult = { kind: "ok"; user: User } | GeneralApiProblem
export type GetFamilyResult = { kind: "ok"; family: FamilyMember[] } | GeneralApiProblem
// export type GetChildrenResult = { kind: "ok"; children: User } | GeneralApiProblem
export type GetPrescriptionsResult = { kind: "ok"; prescriptions: any } | GeneralApiProblem
export type GetMedicationScheduleResult =
  | { kind: "ok"; schedule: MedicationSchedule[]; progress: MedicationProgress[] }
  | GeneralApiProblem
export type DataResult = { kind: "ok"; data: any } | GeneralApiProblem
export type FamilyMemberResult = { kind: "ok"; member: any } | GeneralApiProblem

export type GeneralResult = { kind: "ok" } | GeneralApiProblem
