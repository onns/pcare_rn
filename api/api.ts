import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import { ApiResponse, ApisauceInstance, create } from "apisauce"
import { DateTime } from "luxon"

import firebase from "@react-native-firebase/app"

import { DrugToTakeGroup } from "../stores/drug-to-take/DrugToTakeGroup"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import { getGeneralApiProblem } from "./api-problem"
import * as Types from "./api.types"

// import CookieManager from 'react-native-cookies'

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config

    this.apisauce = create({
      baseURL: this.config.productionUrl,
      timeout: this.config.timeout,
      headers: {
        // "Authorization": "Token " + "e61f9792a634019cfd929cdd79a99c00a9d54343",
        Accept: "application/json",
      },
    })
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  // setup() {
  //   // construct the apisauce instance
  //   this.apisauce = create({
  //     baseURL: this.config.url,
  //     timeout: this.config.timeout,
  //     headers: {
  //       Accept: "application/vnd.github.v3+json",
  //     },
  //   })
  // }

  async signUp(params: Types.SignUpParams): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.post("/rest-auth/registration/", params)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    // 응답 데이터에 대한 검증은 생략됨.
    return { kind: "ok", data: response.data }
  }

  async login(params: Types.LoginParams): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.post(`/rest-auth/login/`, params)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    // 응답 데이터에 대한 검증은 생략됨.
    return { kind: "ok", data: response.data }
  }

  async deleteAccount(userId: number): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.delete(`/user/profiles/${userId}/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async getMyInfo(userId: number, signPath?: string): Promise<Types.GetUserResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/user/users/${userId}/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    // 유저 프로필 NULL 인 경우 생성하기
    if (response.data.user_profile == null) {
      if (!signPath || signPath === "email") {
        const params = {
          user: userId,
          name: response.data.email.slice(0, response.data.email.indexOf("@")),
          nickname: response.data.email.slice(0, response.data.email.indexOf("@")),
        }
        await this.apisauce.post(`/user/profiles/`, params)
      } else if (signPath === "kakao") {
        // kakao login 프로세스에서 프로필 생성함
      } else if (signPath === "facebook") {
        // facebook login 프로세스에서 프로필 생성함
      }
    }

    // transform the data into the format we are expecting
    // try {
    let phone: string | null = null
    let birthString: string | null = null

    if (response.data.user_profile) {
      birthString = String(response.data.user_profile.birth)
      if (birthString != null && birthString.length > 0) {
        birthString = birthString.substring(0, 10)
      }

      phone = response.data.user_profile.phone
      if (phone != null) {
        if (phone.length == 11) {
          phone = phone.substr(0, 3) + " " + phone.substr(3, 4) + " " + phone.substr(7, 4)
        } else if (phone.length == 10) {
          phone = phone.substr(0, 3) + " " + phone.substr(3, 3) + " " + phone.substr(6, 4)
        }
      }
    }

    const user_profile = response.data.user_profile

    const resultUser: Types.User = {
      username: response.data.username,
      email: response.data.email,
      first_name: response.data.first_name,
      last_name: response.data.last_name,
      // user_profile: response.data.user_profile,
      password: response.data.password,
      name: user_profile ? user_profile.name : "",
      nickname: user_profile ? user_profile.nickname : "",
      phone: phone,
      user_type: user_profile ? user_profile.user_type : "",
      is_authenticated: user_profile ? user_profile.is_authenticated : false,
      is_agreed: user_profile ? user_profile.is_agree : false,
      newly_agreed: user_profile && user_profile.newly_agreed ? user_profile.newly_agreed : null,
      newly_agreed_date:
        user_profile && user_profile.newly_agreed_date
          ? new Date(user_profile.newly_agreed_date)
          : null,
      address: user_profile ? user_profile.address : "",
      profile_image: user_profile ? user_profile.img : null,
      gender: user_profile ? user_profile.gender : "",
      birth: birthString ? (birthString.length > 0 ? new Date(birthString) : null) : null,
      firebase_token0: user_profile ? user_profile.firebase_token0 : "",
      dur_preg: user_profile ? user_profile.dur_preg : "n",
      dur_byg: user_profile ? user_profile.dur_byg : "n",
      dur_hng: user_profile ? user_profile.dur_hng : "n",
      dur_dis: user_profile ? user_profile.dur_dis : "n",
      dur_nosale: user_profile ? user_profile.dur_nosale : "n",
      presc_noti: user_profile ? user_profile.presc_noti : "n",
      dur_noti: user_profile ? user_profile.dur_noti : "n",
      ad_noti: user_profile ? user_profile.ad_noti : "n",
      ad_noti_date:
        user_profile && user_profile.ad_noti_date ? new Date(user_profile.ad_noti_date) : null,
    }
    return { kind: "ok", user: resultUser }
    // }
    // catch {
    //   return { kind: "bad-data" }
    // }
  }

  async updateMyInfo(userId: number, params: Types.UpdateUserParams): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.patch(`/user/users/${userId}/`, params)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return response.data
  }

  async updateProfile(userId: number, params: any): Promise<Types.DataResult> {
    if (userId == -1) {
      return { kind: "unauthorized" }
    }

    const headers = {
      "Content-Type": "multipart/form-data",
    }

    const response: ApiResponse<any> = await this.apisauce.patch(
      `/user/profiles/${userId}/`,
      params,
      { headers },
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getMyChildren(): Promise<Types.GetFamilyResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/user/children/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    const convertMember = (raw: Types.FamilyMember) => {
      return {
        id: raw.id,
        created: new Date(raw.created),
        modified: new Date(raw.modified),
        name: raw.name,
        birth_date: new Date(raw.birth_date),
        gender: raw.gender,
        img: raw.img,
        user: raw.user,
        familyMemberKey: `_c${raw.id}`,
      }
    }

    // transform the data into the format we are expecting
    // try {
    if (response.data.count == 0) {
      return { kind: "ok", family: [] }
    }

    const rawMembers = response.data.results
    const resultFamily: Types.FamilyMember[] = rawMembers.map(convertMember)
    return { kind: "ok", family: resultFamily }
    // } catch {
    //   return { kind: "bad-data" }
    // }
  }

  async getMyFamily(userId: number, family: any): Promise<Types.GetFamilyResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(
      `/user/family/?user=${userId}&is_agree=True`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    const convertMember = async (raw: Types.FamilyMember) => {
      const response2: ApiResponse<any> = await this.apisauce.get(
        `/user/profiles/?user=${raw.family_user}`,
      )

      let name
      let birth_date
      let gender
      let phone = ""
      let img

      if (response2.ok && response2.data.count > 0) {
        name = response2.data.results[0].name
        birth_date = new Date(response2.data.results[0].birth)
        gender = response2.data.results[0].gender
        phone = response2.data.results[0].phone
        img = response2.data.results[0].img
      }

      return Promise.resolve({
        id: raw.id,
        created: new Date(raw.created),
        modified: new Date(raw.modified),
        name: name,
        birth_date: birth_date,
        gender: gender,
        phone: phone,
        img: img,
        user: raw.user,
        family_user: raw.family_user,
        familyMemberKey: `_f${raw.family_user}`,
      })
    }

    // transform the data into the format we are expecting
    // try {
    const rawMembers = response.data.results
    const resultFamily: Types.FamilyMember[] = await Promise.all(rawMembers.map(convertMember))
    return { kind: "ok", family: resultFamily }
    // } catch {
    //   return { kind: "bad-data" }
    // }
  }

  async getFamilyToRequest(userId: number): Promise<Types.GetFamilyResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(
      `/user/family/?user=${userId}&is_agree=False`,
    )
    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        return problem
      }
    }

    const convertMember = (raw: Types.FamilyMember) => {
      return {
        id: raw.id,
        created: new Date(raw.created),
        modified: new Date(raw.modified),
        name: raw.name,
        birth_date: new Date(raw.birth_date),
        gender: raw.gender,
        img: raw.img,
        status: raw.status,
        user: raw.user,
        family_user: raw.family_user,
        familyMemberKey: `_f${raw.family_user}`,
        family_user_phone: raw.family_user_phone,
      }
    }

    // transform the data into the format we are expecting
    // try {
    if (response.data.count == 0) {
      return { kind: "ok", family: [] }
    }
    const rawMembers = response.data.results
    const resultFamily: Types.FamilyMember[] = await Promise.all(rawMembers.map(convertMember))
    return { kind: "ok", family: resultFamily }
    // } catch {
    //   return { kind: "bad-data" }
    // }
  }

  async deleteFamilyToRequest(familyId: number): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.delete(`/user/family/${familyId}/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        return problem
      }
    }
    return { kind: "ok" }
  }

  async getMyFamilyToAgree(userId: number): Promise<Types.GetFamilyResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(
      `/user/family/?family_user=${userId}&is_agree=False`,
    )
    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    const convertMember = async (raw: Types.FamilyMember) => {
      const response2: ApiResponse<any> = await this.apisauce.get(
        `/user/profiles/?user=${raw.user}`,
      )

      let name
      let birth_date
      let gender
      let phone = ""
      let img

      if (response2.ok && response2.data.count > 0) {
        name = response2.data.results[0].name
        birth_date = new Date(response2.data.results[0].birth)
        gender = response2.data.results[0].gender
        phone = response2.data.results[0].phone
        img = response2.data.results[0].img
      }

      return Promise.resolve({
        id: raw.id,
        created: new Date(raw.created),
        modified: new Date(raw.modified),
        name: name,
        // birth_date: raw.birth_date ? new Date(raw.birth_date) : undefined,
        birth_date: birth_date,
        gender: gender,
        phone: phone,
        img: img,
        status: raw.status,
        user: raw.user,
        family_user: raw.family_user,
        familyMemberKey: `_f${raw.family_user}`,
      })
    }

    // transform the data into the format we are expecting
    // try {
    const rawMembers = response.data.results
    const resultFamily: Types.FamilyMember[] = await Promise.all(rawMembers.map(convertMember))
    return { kind: "ok", family: resultFamily }
    // } catch {
    //   return { kind: "bad-data" }
    // }
  }

  async getMyPrescriptionManagers(userId: number): Promise<Types.GetFamilyResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(
      `/user/family/?family_user=${userId}&is_agree=True`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    const convertMember = async (raw: Types.FamilyMember) => {
      const response2: ApiResponse<any> = await this.apisauce.get(
        `/user/profiles/?user=${raw.user}`,
      )

      let name
      let birth_date
      let gender
      let phone = ""
      let img

      if (response2.ok && response2.data.count > 0) {
        name = response2.data.results[0].name
        birth_date = new Date(response2.data.results[0].birth)
        gender = response2.data.results[0].gender
        phone = response2.data.results[0].phone
        img = response2.data.results[0].img
      }

      return Promise.resolve({
        id: raw.id,
        created: new Date(raw.created),
        modified: new Date(raw.modified),
        name: name,
        birth_date: birth_date,
        gender: gender,
        phone: phone,
        img: img,
        status: raw.status,
        user: raw.user,
        family_user: raw.family_user,
        familyMemberKey: `_f${raw.family_user}`,
      })
    }

    // transform the data into the format we are expecting
    // try {
    const rawMembers = response.data.results
    const resultFamily: Types.FamilyMember[] = await Promise.all(rawMembers.map(convertMember))
    return { kind: "ok", family: resultFamily }
    // } catch {
    //   return { kind: "bad-data" }
    // }
  }

  async getWaitingRequestMember(userId: number): Promise<Types.GetFamilyResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(
      `/user/family/?user=${userId}&is_agree=False`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    const convertMember = async (raw: Types.FamilyMember) => {
      const response2: ApiResponse<any> = await this.apisauce.get(
        `/user/profiles/?user=${raw.user}`,
      )

      let name
      let birth_date
      let gender
      let phone = ""
      let img

      if (response2.ok && response2.data.count > 0) {
        name = response2.data.results[0].name
        birth_date = new Date(response2.data.results[0].birth)
        gender = response2.data.results[0].gender
        phone = response2.data.results[0].phone
        img = response2.data.results[0].img
      }

      return Promise.resolve({
        id: raw.id,
        created: new Date(raw.created),
        modified: new Date(raw.modified),
        name: name,
        // birth_date: raw.birth_date ? new Date(raw.birth_date) : undefined,
        birth_date: birth_date,
        gender: gender,
        phone: phone,
        img: img,
        status: raw.status,
        user: raw.user,
        family_user: raw.family_user,
        familyMemberKey: `_f${raw.family_user}`,
      })
    }

    // transform the data into the format we are expecting
    // try {
    const rawMembers = response.data.results
    const resultFamily: Types.FamilyMember[] = await Promise.all(rawMembers.map(convertMember))
    return { kind: "ok", family: resultFamily }
    // } catch {
    //   return { kind: "bad-data" }
    // }
  }

  // async getDrug(drugId: number): Promise<Types.GetDrugResult> {

  //   const response: ApiResponse<any> = await this.apisauce
  //     .get(`/drug/drugs/${drugId}/`)

  //   // the typical ways to die when calling an api
  //   if (!response.ok) {
  //     const problem = getGeneralApiProblem(response)
  //     if (problem) return problem
  //   }

  //   // transform the data into the format we are expecting
  //   try {
  //     const resultRepo: Types.Drug = {
  //       id: response.data.id,
  //       kd_code: response.data.kd_code,
  //       created: response.data.created,
  //       modified: response.data.modified,
  //       ref_code: response.data.ref_code,
  //       name: response.data.name,
  //       company: response.data.company,
  //       efficacy_code: response.data.efficacy_code,
  //       efficacy_category: response.data.efficacy_category,
  //       image_url: response.data.image_url,
  //       prescription_otc: response.data.prescription_otc,
  //       medicine_category: response.data.medicine_category,
  //       ingredients: response.data.ingredients,
  //       mechanism_short: response.data.mechanism_short,
  //       mechanism_detail: response.data.mechanism_detail,
  //       dosage: response.data.dosage,
  //       caution: response.data.caution,
  //       manual_url: response.data.manual_url,
  //       howto_storage: response.data.howto_storage,
  //       exp_date: response.data.exp_date
  //     }
  //     return { kind: "ok", drug: resultRepo }
  //   } catch {
  //     return { kind: "bad-data" }
  //   }
  // }

  async updateFamilyMember(params: any): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.patch(
      `/user/family/${params.id}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async deleteFamilyMember(params: any): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.delete(
      `/user/family/${params.id}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async findFamilyMember(phone: string): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/user/profiles/?phone=${phone}`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getUserProfile(userId: number): Promise<Types.DataResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/user/profiles/?user=${userId}`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async addFamilyMember(params: any): Promise<Types.FamilyMemberResult> {
    const response: ApiResponse<any> = await this.apisauce.post(`/user/family/`, params)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", member: response.data }
  }

  async updateChild(params: any): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.patch(
      `/user/children/${params.id}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async deleteChild(params: any): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.delete(
      `/user/children/${params.id}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async addChild(params: any): Promise<Types.FamilyMemberResult> {
    const response: ApiResponse<any> = await this.apisauce.post(`/user/children/`, params)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", member: response.data }
  }

  async createProfile(params: FormData): Promise<Types.GeneralResult> {
    const headers = {
      "Content-Type": "multipart/form-data",
    }

    const response: ApiResponse<any> = await this.apisauce.post(`/user/profiles/`, params, {
      headers,
    })

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async uploadPrescriptionImage(params: FormData): Promise<Types.DataResult> {
    const headers = {
      "Content-Type": "multipart/form-data",
    }

    const response: ApiResponse<any> = await this.apisauce.post(
      `/prescription/prescriptions/`,
      params,
      { headers },
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async updatePrescription(prescriptionId: number, params: any): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.patch(
      `/prescription/prescriptions/${prescriptionId}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async deletePrescription(prescriptionId: number): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.delete(
      `/prescription/prescriptions/${prescriptionId}/`,
      { id: prescriptionId },
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      firebase.analytics().logEvent("prescription_deleted")
      amplitude.getInstance().logEvent("prescription_deleted")
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async uploadChildProfileImage(params: any, memberId: number): Promise<Types.DataResult> {
    const headers = {
      "Content-Type": "multipart/form-data",
    }

    const response: ApiResponse<any> = await this.apisauce.patch(
      `/user/children/${memberId}/`,
      params,
      { headers },
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async uploadFamilyProfileImage(params: any, memberId: number): Promise<Types.DataResult> {
    const headers = {
      "Content-Type": "multipart/form-data",
    }

    const response: ApiResponse<any> = await this.apisauce.patch(
      `/user/family/${memberId}/`,
      params,
      { headers },
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async updateProfileWithUser(params: any, userId: number): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.patch(`/user/users/${userId}/`, params)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async getAllPrescriptions(): Promise<Types.GetPrescriptionsResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/prescription/all_prescriptions/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", prescriptions: response.data }
  }

  async getPrescriptions(
    userId: number,
    childId?: number,
    isFamilyAll?: boolean,
  ): Promise<Types.GetPrescriptionsResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      "/prescription/prescriptions/?is_sampled=False&user=" +
        userId +
        (childId
          ? "&children=" + childId
          : isFamilyAll
          ? "&is_family=True"
          : "&is_children=False&is_family=False"),
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", prescriptions: response.data.results }
  }

  async getDrugsToTake(
    userId: number,
    familyMemberKey: string,
    childId?: number,
    date?: string,
  ): Promise<Types.DataResult> {
    let queryString
    if (childId) {
      queryString = `?children=${childId}`
      if (date) {
        queryString += `&theday=${date}`
      }
    } else if (familyMemberKey.includes("f")) {
      queryString = `?family=${userId}`
      if (date) {
        queryString += `&theday=${date}`
      }
    } else if (date) {
      queryString = `?theday=${date}`
    } else {
      queryString = ""
    }

    const response: ApiResponse<any> = await this.apisauce.get(
      `/prescription/drug2take/${queryString}`,
    )
    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async getMedicationSchedule(
    userId: number,
    familyMemberKey: string,
    childId?: number,
    date?: string,
  ): Promise<Types.GetMedicationScheduleResult> {
    let response: ApiResponse<any> = null
    if (date === undefined) {
      response = await this.apisauce.get(
        `/prescription/schedule/?need_progress=True` +
          (childId
            ? `&children=${childId}`
            : familyMemberKey.includes("f")
            ? `&family=${userId}`
            : ""),
      )
    } else {
      response = await this.apisauce.get(
        `/prescription/schedule/?month=${date}&need_progress=True` +
          (childId
            ? `&children=${childId}`
            : familyMemberKey.includes("f")
            ? `&family=${userId}`
            : ""),
      )
    }

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    const convertData = (raw: Types.MedicationSchedule) => {
      return {
        id: raw.id,
        disease: raw.disease,
        start: DateTime.fromISO(raw.start),
        end: DateTime.fromISO(raw.end),
        issue_date: DateTime.fromISO(raw.issue_date),
      }
    }

    const progressData = (raw: Types.MedicationProgress) => {
      return {
        date: raw.date,
        ratio: raw.ratio,
      }
    }

    if (response.data.count == 0) {
      return { kind: "ok", schedule: [], progress: [] }
    }

    const rawData = response.data
    const result: Types.MedicationSchedule[] = rawData.schedule.map(convertData)
    const progressResult: Types.MedicationProgress[] = rawData.progress.map(progressData)

    return { kind: "ok", schedule: result, progress: progressResult }
  }

  async getPrescriptionDetail(prescriptionId: number, userId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/prescription/prescriptions/${prescriptionId}/` + (userId != -1 ? `?user=${userId}` : ""),
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getDiseaseSummary(diseaseId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/disease/diseases/${diseaseId}/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async getRepDiseaseSummary(diseaseCode: string): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/disease/get_disease_details/?code=${diseaseCode}`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getDiseaseDetail(diseaseId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/disease/details/${diseaseId}/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getDrugDetail(drugId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/drug/drugs/${drugId}/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getNextResults(next: string): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(next)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getSearchResults(
    category: "drug" | "disease" | "hospital",
    query: string,
    offset?: number,
    limit?: number,
  ): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/${category}/search/?limit=${limit}&offset=${offset}&q=${query}`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getHospitalDetail(id: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/hospital/hospitals/${id}/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data }
  }

  async getDiseasePrevalence(diseaseCode: string): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/nps/disease_prevailance/?disease_code=${diseaseCode}`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results }
  }

  async getDiseaseDepartment(diseaseCode: string): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/nps/disease_department/?disease_code=${diseaseCode}`,
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results.reverse() }
  }

  async getDiseaseComorb(diseaseCode: string): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/nps/disease_comorb/?disease_code=${diseaseCode}`,
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results }
  }

  async getIngredientDisease(ingredientCode: string): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/nps/ingredient_disease/?ing_code=${ingredientCode}`,
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results.reverse() }
  }

  async getNoticeList(): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get("/notice/")

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    const convertData = (raw: Types.Notice) => {
      return {
        id: raw.id,
        created: new Date(raw.created),
        title: raw.title,
        subtitle: raw.subtitle,
        content: raw.content,
      }
    }

    if (response.data.count == 0) {
      return { kind: "ok", data: [] }
    }

    const rawResult = response.data.results
    const result: Types.DataResult[] = rawResult.map(convertData)

    return { kind: "ok", data: result }
    // return { kind: "ok", data: response.data.results }
  }

  async getNoticeDetail(noticeId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/notice/${noticeId}`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    const convertData = (raw: Types.Notice) => {
      return {
        id: raw.id,
        created: new Date(raw.created),
        title: raw.title,
        subtitle: raw.subtitle,
        content: raw.content,
      }
    }

    if (response.data.count == 0) {
      return { kind: "ok", data: [] }
    }

    const rawResult = response.data
    const result: Types.Notice = convertData(rawResult)

    return { kind: "ok", data: result }
    // return { kind: "ok", data: response.data }
  }

  async changeDeveloperMode(type: number) {
    const API_TYPE_DICT: Array<string> = []
    API_TYPE_DICT.push(this.config.productionUrl)
    API_TYPE_DICT.push(this.config.stagingUrl)
    API_TYPE_DICT.push(this.config.localUrl1)
    API_TYPE_DICT.push(this.config.localUrl2)
    API_TYPE_DICT.push(this.config.localUrl3)
    API_TYPE_DICT.push(this.config.localUrl4)
    API_TYPE_DICT.push(this.config.localUrl5)
    API_TYPE_DICT.push(this.config.localUrl6)
    this.apisauce.setBaseURL(API_TYPE_DICT[type])
  }

  async takeDrug(
    drugToTakeId: number,
    status: "taken" | "ready",
    drugToTakeGroup?: DrugToTakeGroup,
  ): Promise<Types.GeneralResult> {
    let appendix = ""
    if (drugToTakeGroup && drugToTakeGroup.drugsToTake.length > 0) {
      drugToTakeGroup.drugsToTake.forEach((drugToTake) => {
        if (drugToTakeId != drugToTake.id) {
          appendix += `,${drugToTake.id}`
        }
      })
    }

    const response: ApiResponse<any> = await this.apisauce.patch(
      `/prescription/drug2take/set_status/?taken_ids=${drugToTakeId}${appendix}`,
      { status: status },
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  async getCampaigns(): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/campaign/`)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results }
  }

  async getRanking(kind: "disease" | "drug"): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/statistics/?kind=${kind}`)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async getTimeSlots(): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get("/prescription/timeslot/")

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results }
  }

  async addTimeSlot(params: any): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.post("/prescription/timeslot/", params)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async updateTimeSlot(id: number, params: any): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.patch(
      `/prescription/timeslot/${id}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async deleteTimeSlot(params: any): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.delete(
      `/prescription/timeslot/${params.id}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async patchPrescriptionDetail(
    prescriptionDetailId: number,
    params: any,
  ): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.patch(
      `/prescription/prescriptiondetail/${prescriptionDetailId}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async deletePrescriptionDetail(prescriptionDetailId: number): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.delete(
      `/prescription/prescriptiondetail/${prescriptionDetailId}/`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok" }
  }

  async patchPrescription(prescriptionId: number, params: any): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.patch(
      `/prescription/prescriptions/${prescriptionId}/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results }
  }

  async postPrescription(params: any): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.post(
      `/prescription/prescriptions/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async postPrescriptionDetail(params: any): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.post(
      `/prescription/prescriptiondetail/`,
      params,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async getCompliance(prescriptionId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/prescription/prescriptions/${prescriptionId}/compliance/`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results[0].compliance }
  }

  /**
   * 자가기록 활동 데이터
   * @param limit(option)
   * @param offset(option)
   */
  async getRecords(limit?: number, offset?: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/record/record/?limit=${limit}?offset=${offset}`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data.results }
  }

  /**
   * 리포트 그래프 데이터
   * @param recordId(required)
   * @param disable(option)
   */
  async getGraph(recordId: number, disable?: string): Promise<Types.DataResult> {
    let response: ApiResponse<any> = null
    if (disable) {
      response = await this.apisauce.get(`/record/record/${recordId}/graph/?disable=${disable}`)
    } else {
      response = await this.apisauce.get(`/record/record/${recordId}/graph/`)
    }

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  /**
   * 자가기록 자세히 데이터
   * @param recordId(required)
   */
  async getRecordDetail(recordId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(
      `/record/record/${recordId}/record_data/`,
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  /**
   * 자가기록 데이터 입력
   * @param surveyId(required)
   * @param params(required)
   */
  async postSurvey(surveyId: number, params: { data: number }): Promise<Types.GeneralResult> {
    const response: ApiResponse<any> = await this.apisauce.post(
      `/record/record/${surveyId}/record_data/`,
      params,
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok" }
  }

  /**
   * 설문조사
   * @param recordId(required)
   */
  async getSurvey(recordId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/record/survey/${recordId}/`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }
    return { kind: "ok", data: response.data }
  }

  async getRecordVideo(recordId: number): Promise<Types.DataResult> {
    const response: ApiResponse<any> = await this.apisauce.get(`/record/video/${recordId}/`)
    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        return problem
      }
    }

    return { kind: "ok", data: response.data.video_datas }
  }
}

// API singleton instance
export const api = new Api()
