import { inject, observer } from "mobx-react"
import { ScrollView, Toast } from "native-base"
import React from "react"
import {
  Dimensions,
  Image,
  ImageStyle,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import Modal from "react-native-modal"

import { api } from "@api/api"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import CloseButton from "../components/CloseButton"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { formatDateFromString } from "../lib/DateUtil"
import {
  AuthStackParamList,
  MainTabParamList,
  PrescriptionsStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  paddingHorizontal: 20,
  paddingTop: 20,
}
const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 34,
  paddingHorizontal: 6,
  paddingBottom: 24,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 34,
  lineHeight: 40,
  letterSpacing: -1.36,
  color: color.text,
  textAlign: "center",
}
const TITLE_DOT: TextStyle = {
  fontFamily: typography.primary,
  fontWeight: "bold",
  fontSize: 34,
  lineHeight: 40,
  letterSpacing: -1.36,
  color: "rgb(255, 132, 26)",
}
const BOTTOM_BUTTON: ViewStyle = {
  flex: 1,
  flexDirection: "column",
  justifyContent: "flex-end",
  marginBottom: 30,
}
const MODAL_TITLE = {
  fontFamily: typography.primary,
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
  paddingBottom: 15,
}
const MODAL_DESCRIPTION = {
  fontSize: 15,
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.palette.black,
}
const MODAL_CONTAINER = {
  borderRadius: 10,
  backgroundColor: "rgb(239, 239, 239)",
  padding: 20,
}

const starImageSource = require("../assets/images/star.png")
const starGreyImageSource = require("../assets/images/starGray.png")

const INFO_MARK: ImageStyle = {
  width: 14,
  height: 14,
  marginLeft: 5,
  resizeMode: "contain",
}
const SMALL_TEXT = {
  fontFamily: typography.primary,
  fontSize: 13,
  lineHeight: 24,
  letterSpacing: -0.26,
  color: color.palette.brownGrey,
}

const RATING_TEXT_GRAY_BOX: ViewStyle = {
  alignSelf: "stretch",
  height: 40,
  alignItems: "center",
  justifyContent: "center",
  borderColor: "rgb(217,217,217)",
  borderWidth: 1,
  borderRadius: 20,
  marginTop: 15,
}

const RATING_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontWeight: "bold",
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
}
const RATING_AVAILABLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 24,
  letterSpacing: -0.8,
  color: "rgb(153, 153, 153)",
}
const RATING_UNAVAILABLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 24,
  letterSpacing: -0.6,
  color: color.palette.grey3,
}

const infoSource = require("../assets/images/info.png")
const hitSlop20 = { top: 20, left: 20, right: 20, bottom: 20 }

const deviceWidth = Dimensions.get("window").width
const deviceHeight = Dimensions.get("window").height
export const rating5Text = "모두 복용"
export const rating4Text = "거의 다 복용"
export const rating3Text = "보통"
export const rating2Text = "거의 복용하지 않음"
export const rating1Text = "복용하지 않음"

type DrugAdherenceRatingRouteProp = RouteProp<PrescriptionsStackParamList, "DrugAdherenceRating">

type DrugAdherenceRatingNavigationProp = CompositeNavigationProp<
  StackNavigationProp<PrescriptionsStackParamList, "DrugAdherenceRating">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AuthStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: DrugAdherenceRatingNavigationProp
  route: DrugAdherenceRatingRouteProp
}

interface State {
  starCount: number
  isModalVisible: boolean
}

const iconEmojiArray = [
  require("../assets/images/iconEmojiSoBad.png"),
  require("../assets/images/iconEmojiBad.png"),
  require("../assets/images/iconEmojiNormal.png"),
  require("../assets/images/iconEmojiNice.png"),
  require("../assets/images/iconEmojiSoNice.png"),
]
const iconCheckedEmojiArray = [
  require("../assets/images/iconEmojiSoBadChecked.png"),
  require("../assets/images/iconEmojiBadChecked.png"),
  require("../assets/images/iconEmojiNormalChecked.png"),
  require("../assets/images/iconEmojiNiceChecked.png"),
  require("../assets/images/iconEmojiSoNiceChecked.png"),
]

@inject("accountStore", "prescriptionStore")
@observer
export default class DrugAdherenceRatingScreen extends React.Component<Props, State> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route
    return {
      title: "[" + params.repTitle + ", " + formatDateFromString(params.issueDate) + "]",
      headerLeft: null,
    }
  }

  constructor(props: Props) {
    super(props)
    const { route } = this.props
    const rating = this.getRating(route.params)
    this.state = {
      starCount: rating || 2,
      isModalVisible: false,
    }
  }

  getPrescriptionId(params: any) {
    return params.prescriptionId
  }

  getRating(params: any) {
    return params.rating
  }

  getRepTitle(params: any) {
    return params.repTitle
  }

  getIssueDate(params: any) {
    return params.issueDate
  }

  getNeededSymptomImprovementRating(params: any) {
    return params.neededSymptomImprovementRating
  }

  renderEmojiByType(rating: number) {
    return (
      <TouchableOpacity
        style={{
          marginLeft: 11,
          marginRight: 11,
        }}
        onPress={() => {
          this.ratingCompleted(rating)
        }}
      >
        {this.state.starCount == rating ? (
          <Image source={iconCheckedEmojiArray[rating - 1]} style={{ height: 42, width: 42 }} />
        ) : (
          <Image source={iconEmojiArray[rating - 1]} style={{ height: 42, width: 42 }} />
        )}
      </TouchableOpacity>
    )
  }

  getDrugAdherenceRatingLabel(rating: number): string {
    switch (rating) {
      case 1:
        return rating1Text

      case 2:
        return rating2Text

      case 3:
        return rating3Text

      case 4:
        return rating4Text

      case 5:
        return rating5Text

      default:
        return "얼굴 표정을 탭하여 기록하기"
    }
  }

  render() {
    /** 이모티콘  */
    const adherenceEmojiList: Array<any> = []

    for (let index = 1; index <= 5; index++) {
      adherenceEmojiList.push(this.renderEmojiByType(index))
    }

    const rating = this.getRating(this.props.route.params)

    return (
      <View style={BACKGROUND}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={TITLE}>
            <Text style={TITLE_TEXT}>
              약은 다 드셨나요
              <Text style={TITLE_DOT}>?</Text>
            </Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>{adherenceEmojiList}</View>
            <View style={RATING_TEXT_GRAY_BOX}>
              <Text style={this.state.starCount == 0 ? RATING_AVAILABLE_TEXT : RATING_TEXT}>
                {this.getDrugAdherenceRatingLabel(this.state.starCount)}
              </Text>
            </View>
            <View
              style={{
                alignSelf: "stretch",
                borderWidth: 1,
                borderRadius: 10,
                borderColor: color.palette.grey4,
                marginTop: 20,
                marginBottom: 23,
                paddingTop: 15,
                paddingBottom: 18,
              }}
            >
              {this.renderRatingInfo(rating5Text, 5)}
              {this.renderRatingInfo(rating4Text, 4)}
              {this.renderRatingInfo(rating3Text, 3)}
              {this.renderRatingInfo(rating2Text, 2)}
              {this.renderRatingInfo(rating1Text, 1)}
            </View>
          </View>
          <View
            style={{
              height: 22,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 18,
            }}
          >
            <Text style={SMALL_TEXT}>* 복약순응도 기록은 왜 남기나요?</Text>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  isModalVisible: true,
                })
              }
              hitSlop={hitSlop20}
            >
              <Image source={infoSource} style={INFO_MARK} />
            </TouchableOpacity>
          </View>

          <View style={BOTTOM_BUTTON}>
            <Button
              block
              rounded
              onPress={() => this.cancel()}
              style={{ backgroundColor: "rgb(217, 217, 217)", marginBottom: 8 }}
            >
              <Text style={{ fontWeight: "bold", fontSize: 17, color: color.text }}>
                {rating ? "취소" : "다음에 기록"}
              </Text>
            </Button>
            <Button block rounded onPress={() => this.next()}>
              <Text style={{ fontWeight: "bold", fontSize: 17 }}>{rating ? "완료" : "다음"}</Text>
            </Button>
          </View>

          <Modal
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            isVisible={this.state.isModalVisible}
            animationOut={"fadeOutDown"}
            onBackdropPress={this.toggleModal}
          >
            <View>
              <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
              <View style={{ alignItems: "flex-end" }}>
                <CloseButton
                  imageStyle={{ tintColor: "rgb(255, 255, 255)" }}
                  onPress={() => this.toggleModal()}
                  style={{
                    width: 15.7,
                    height: 15.7,
                    marginRight: 0,
                    paddingTop: 0,
                    marginBottom: 12.4,
                  }}
                />
              </View>
              <View style={MODAL_CONTAINER}>
                <Text style={MODAL_TITLE}>복약순응도 기록은 왜 필요한가요?</Text>
                <Text style={MODAL_DESCRIPTION}>
                  복약 순응도란 환자가 처방약을 처방에 맞게 얼마나 잘 복용하는 지를 이야기 합니다.
                  처방약 복용 여부에 대한 기록은 의료진에게 유용한 진단 정보가 될 수 있습니다. 예로
                  증상 개선이 미미하거나 악화된 경우, 약을 복용하지 않아서인지 혹은 처방약을
                  변경해야하는지를 판단하는데 도움이 됩니다. 동일 질병이여도 환자에 따라 다양한
                  치료방법이 있을 수 있습니다. 나에게 맞는 치료방법을 찾 기 위한 시작을 복약순응도
                  기록으로 시작하세요.
                </Text>
                <View
                  style={{
                    borderWidth: 1,
                    borderRadius: 10,
                    borderColor: color.palette.grey2,
                    marginTop: 20,
                    marginBottom: 20,
                  }}
                >
                  {this.renderRatingInfo(rating5Text, 5, color.palette.grey6)}
                  {this.renderRatingInfo(rating4Text, 4, color.palette.grey6)}
                  {this.renderRatingInfo(rating3Text, 3, color.palette.grey6)}
                  {this.renderRatingInfo(rating2Text, 2, color.palette.grey6)}
                  {this.renderRatingInfo(rating1Text, 1, color.palette.grey6)}
                </View>
              </View>
            </View>
          </Modal>
        </ScrollView>
      </View>
    )
  }

  ratingCompleted(rating: number) {
    this.setState({
      starCount: rating,
    })
  }

  renderRatingInfo(text: string, ratingCount: number, emptyStarColor?: string) {
    return (
      <View
        style={{
          height: 34,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start",
          paddingLeft: 109,
        }}
      >
        <Image source={iconCheckedEmojiArray[ratingCount - 1]} style={{ height: 26, width: 26 }} />
        <Text
          numberOfLines={2}
          style={{
            fontFamily: typography.primary,
            fontSize: 16,
            lineHeight: 20,
            letterSpacing: -0.64,
            textAlign: "left",
            marginLeft: 10,
          }}
        >
          {text}
        </Text>
      </View>
    )
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  cancel() {
    this.props.navigation.pop()
  }

  async next() {
    const { navigation, route } = this.props
    const prescription = this.props.prescriptionStore.prescriptionDetail
    const pStore = this.props.prescriptionStore
    const params = {
      f_value: this.state.starCount,
    }

    const prescriptionId = this.getPrescriptionId(route.params)
    const neededSymptomImprovementRating = this.getNeededSymptomImprovementRating(route.params)

    if (!prescriptionId) {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => navigation.popToTop(), 2000)
      return
    }
    const result = await api.updatePrescription(prescriptionId, params)
    if (result.kind === "ok") {
      if (prescription && prescriptionId == prescription.id) {
        prescription.setDrugAdherenceRating(this.state.starCount)
      } else if (prescriptionId) {
        const found = pStore.findPrescription(prescriptionId)
        if (found) {
          found.setDrugAdherenceRating(this.state.starCount)
        }
      }
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`저장하였습니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      // pStore.removeDoingEndedPrescription(prescriptionId)
      if (neededSymptomImprovementRating) {
        navigation.navigate("SymptomImprovementRating", {
          prescriptionId: prescriptionId,
          repTitle: this.getRepTitle(route.params),
          issueDate: this.getIssueDate(route.params),
        })
      } else {
        navigation.popToTop()
      }
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => navigation.popToTop(), 2000)
    }
  }
}
