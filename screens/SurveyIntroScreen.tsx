import { observer } from "mobx-react-lite"
import { Spinner } from "native-base"
import React, { useEffect, useLayoutEffect } from "react"
import { TextStyle, View, ViewStyle } from "react-native"

import { RouteProp, useNavigation } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import CloseButton from "../components/CloseButton"
import { Screen } from "../components/screen/screen"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { DetailsStackParamList } from "../navigation/types"
import { Survey } from "../stores/record/survey"
import { color, typography } from "../theme"

type SurveyIntroScreenRouteProp = RouteProp<DetailsStackParamList, "SurveyIntro">

export interface SurveyIntroScreenProps {
  navigation: StackNavigationProp<DetailsStackParamList, "SurveyIntro">
  route: SurveyIntroScreenRouteProp
}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
}
const CENTER: ViewStyle = { flex: 1, alignSelf: "center" }
const TITLE: TextStyle = {
  ...typography.title,
  lineHeight: 47,
  color: color.palette.black,
  marginBottom: 4,
  textAlign: "center",
}
const SUB_TITLE = {
  fontFamily: typography.bold,
  fontSize: 16,
  lineHeight: 24,
  color: color.palette.grey50,
}
const FULL = { flex: 1 }
const SECTION1: ViewStyle = { flex: 1, alignItems: "center", paddingHorizontal: 24 }
const SECTION2CAT: ViewStyle = {
  flex: 1,
  alignItems: "center",
  minHeight: 100,
  maxHeight: 232,
  // paddingHorizontal: 53,
  marginHorizontal: 24,
  borderWidth: 2,
  borderColor: "#C2C1C1",
  borderStyle: "solid",
  borderRadius: 8,
}
const TEXTWRAPPER: ViewStyle = {
  flex: 1,
  margin: 16,
  justifyContent: "space-between",
}
const SECTION2: ViewStyle = {
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  paddingHorizontal: 53,
}
const SECTION3: ViewStyle = {
  flex: 1,
  justifyContent: "flex-end",
  paddingHorizontal: 24,
  paddingBottom: 44,
}
const EXPECTED_TIME: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 17,
  lineHeight: 25,
  color: color.palette.black,
  marginBottom: 24,
  textAlign: "center",
}
const EXPECTED_TIME_ACCENT = {
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
  color: color.primary,
}
const COLOR_UNDERLINE_BOX: ViewStyle = {
  position: "absolute",
  left: 24,
}
const COLOR_UNDERLINE_PAINT: ViewStyle = {
  // width: 120,
  height: 8,
  opacity: 0.2,
  backgroundColor: color.palette.darkMint,
}
const DESCRIPTION: TextStyle = {
  ...typography.title3,
  color: color.palette.black,
  // textAlign: "center",
}
const DESCRIPTIONTEXT: TextStyle = {
  ...typography.body,
  color: color.palette.black,
  // textAlign: "center",
}
const BUTTON_TEXT = { ...typography.title3, color: color.palette.white }

export const SurveyIntroScreen: React.FunctionComponent<SurveyIntroScreenProps> = observer(
  (props) => {
    const { recordId } = props.route.params
    const navigation = useNavigation()
    const { recordStore } = useStores()
    const requestedSurvey: Survey = recordStore.requestedSurvey

    useLayoutEffect(() => {
      navigation.setOptions({
        headerLeft: () => <CloseButton onPress={navigation.goBack} style={{ marginLeft: 24 }} />,
        headerTitle: "",
        headerStyle: { borderBottomWidth: 0, elevation: 0, shadowColor: "transparent" },
      })
    }, [navigation])

    useEffect(() => {
      recordStore.fetchSurvey(recordId)
    }, [])

    const start = () => {
      navigation.navigate("SurveyQuestions")
    }

    return (
      <Screen style={ROOT} preset="fixed">
        {recordStore.status === "pending" ? (
          <Spinner color={color.primary} style={CENTER} />
        ) : (
          requestedSurvey && (
            <View style={FULL}>
              <View style={SECTION1}>
                {/* <View style={COLOR_UNDERLINE_BOX}>
                  <View style={COLOR_UNDERLINE_PAINT} />
                </View> */}
                <Text style={TITLE}>{requestedSurvey.title.replace("평가검사", "\n평가검사")}</Text>
                <Text style={SUB_TITLE}>({requestedSurvey.sub_title})</Text>
              </View>
              {requestedSurvey.name === "CAT" ? (
                <View style={SECTION2CAT}>
                  <View style={TEXTWRAPPER}>
                    <Text style={DESCRIPTION}>
                      만성폐쇄성폐질환 평가 검사의 라이센스 문제로 더 이상 서비스 제공이 불가합니다.
                    </Text>
                    <Text style={DESCRIPTIONTEXT}>
                      그 동안 만성폐쇄성질환 평가 검사를 이용해 주신 회원님들께 양해를 구하며, 다음
                      업데이트에서 더욱 새로워진 모습으로 찾아뵙겠습니다.
                    </Text>
                  </View>
                </View>
              ) : (
                <View style={SECTION2}>
                  <Text style={DESCRIPTION}>{requestedSurvey.description}</Text>
                </View>
              )}
              {requestedSurvey.name === "CAT" ? (
                <View style={{ flex: 1 }} />
              ) : (
                <View style={SECTION3}>
                  <Text style={EXPECTED_TIME}>
                    예상 소요 시간:{" "}
                    <Text style={EXPECTED_TIME_ACCENT}>{requestedSurvey.expected_time}</Text>
                  </Text>
                  <Button block primary rounded onPress={start}>
                    <Text style={BUTTON_TEXT}>시작하기</Text>
                  </Button>
                </View>
              )}
            </View>
          )
        )}
      </Screen>
    )
  },
)
