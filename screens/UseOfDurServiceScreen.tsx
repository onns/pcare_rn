import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Toast } from "native-base"
import React from "react"
import {
  Alert,
  BackHandler,
  Dimensions,
  Image,
  ImageStyle,
  Linking,
  Platform,
  ScrollView,
  StatusBar,
  TextStyle,
  View,
  ViewStyle,
} from "react-native"
import AndroidOpenSettings from "react-native-android-open-settings"
import Modal from "react-native-modal"
import { requestNotifications } from "react-native-permissions"

import { api } from "@api/api"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import CloseButton from "../components/CloseButton"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, styles, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  paddingHorizontal: 16,
}
const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 34,
  paddingHorizontal: 6,
  paddingBottom: 36,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 34,
  lineHeight: 44,
  letterSpacing: -1.02,
  color: color.text,
  textAlign: "center",
}
const TITLE_DOT: TextStyle = {
  fontFamily: typography.primary,
  fontWeight: "bold",
  fontSize: 34,
  lineHeight: 44,
  letterSpacing: -1.02,
  color: "rgb(255, 132, 26)",
}
const DESCRIPTION: TextStyle = {
  fontSize: 15,
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.text,
  paddingBottom: 12,
  textAlign: "auto",
}
const DESC_ACCENT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.palette.waterBlue,
  textDecorationLine: "underline",
}
const DUR_SETTING_GUIDE: ViewStyle = {
  alignItems: "center",
  marginTop: 13,
  marginBottom: 16,
  paddingHorizontal: 14,
}
const DUR_SETTING_GUIDE_TEXT: TextStyle = {
  fontSize: 13,
  lineHeight: 19.5,
  letterSpacing: -0.7,
  color: color.palette.brownGrey,
}
const BOTTOM_BUTTON: ViewStyle = {
  flex: 1,
  flexDirection: "column",
  justifyContent: "flex-end",
  marginBottom: 20,
}
const CLOSE_BUTTON: ViewStyle = {
  width: 15.7,
  height: 15.7,
  marginRight: 0,
  paddingTop: 0,
  marginBottom: 12.4,
}
const POPUP_TITLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
  paddingBottom: 15,
  color: color.palette.lightBlack,
}
const POPUP_CONTENT: TextStyle = {
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.palette.lightBlack,
}
const POPUP_DUR_IMAGE: ImageStyle = {
  width: 295,
  height: 109,
  marginBottom: 20,
  alignSelf: "center",
}

const popupDurImage = require("../assets/images/popupDur.png")
const deviceWidth = Dimensions.get("window").width
const deviceHeight = Dimensions.get("window").height

const modalContents = {
  preg: {
    title: "임부금기 약제",
    content:
      "임신 또는 임신하고 있을 가능성이 있는 환자가 복용하면 태아기형 및 태아독성 등 태아에 대한 위험성이 있는 약물입니다.",
  },
  byg: {
    title: "병용금기 약제",
    content:
      "두개 이상의 약물을 같이 복용했을 때 예상하지 못한 부작용이 나타나거나 치료 효과가 떨어지는 약물입니다.",
  },
  hng: {
    title: "효능군중복 약제",
    content: "약의 성분은 다르나 효능이 동일한 약물이 2가지 이상있는 경우입니다.",
  },
  dis: {
    title: "동일성분중복 약제",
    content: "약의 효능효과, 성분이 동일한 약물이 2가지 이상 있는 경우입니다.",
  },
  nosale: {
    title: "사용중지 약제",
    content: "해외 또는 국내에서의 약물 부작용보고, 품질부적합 등으로 사용이 중지된 약물입니다.",
  },
  dur: {
    title: "의약품 안전 점검(DUR) 이란?",
    content:
      "환자가 여러 의사에게 진료받을 경우 의사와 약사는 환자가 복용하고 있는 약을 알지 못하고 처방·조제하여 환자가 약물 부작용에 노출될 가능성이 있습니다. 의약품 처방·조제 시 병용금기 등 부적절한 약물사용을 사전에 점검할 수 있도록 의약품 안전 정보를 제공하는 것을 “DUR (Drug Utilization Review)” 또는 “의약품 안전 사용 서비스”라고 하며 저희 파프리카케어에서는 이해가 쉽도록 “의약품 안전 점검”라고 약칭하고 있습니다.",
  },
}

type UseOfDurServiceRouteProp = RouteProp<AuthStackParamList, "UseOfDurService">

type UseOfDurServiceNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "UseOfDurService">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<HomeStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  navigation: UseOfDurServiceNavigationProp
  route: UseOfDurServiceRouteProp
}

interface State {
  isModalVisible: boolean
  modalContentType: "preg" | "byg" | "hng" | "dis" | "dur" | "nosale"
}

@inject("accountStore")
@observer
export default class UseOfDurServiceScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "",
    headerBackImage: () => <View />,
    headerStyle: {
      borderBottomWidth: 0,
      height: 0, // default is 64
      elevation: 0,
      shadowColor: "transparent",
    },
  }

  bLoginProcess: boolean | undefined

  constructor(props: Props) {
    super(props)
    this.state = {
      isModalVisible: false,
      modalContentType: "preg",
    }
    this.isLoginProcess = this.isLoginProcess(this.props.route.params)
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.closeModal)
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
    AsyncStorage.setItem("showedDurGuidance", JSON.stringify(true))
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.closeModal)
  }

  isLoginProcess(params: any) {
    if (params) {
      return params.isLoginProcess
    } else {
      return undefined
    }
  }

  render() {
    const user = this.props.accountStore.user
    return (
      <View style={BACKGROUND}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={TITLE}>
            {user.is_authenticated ? (
              <Text style={TITLE_TEXT}>의약품 안전 점검{"\n"}알림 서비스 개시</Text>
            ) : (
              <Text style={TITLE_TEXT}>
                의약품 안전 점검(DUR){"\n"}서비스를 사용하세요
                <Text style={TITLE_DOT}>.</Text>
              </Text>
            )}
          </View>
          <View style={{ paddingHorizontal: 14 }}>
            <View style={{ alignItems: "center" }}>
              <Image source={popupDurImage} style={{ resizeMode: "contain", marginBottom: 36 }} />
              <Text style={DESCRIPTION}>
                <Text style={DESC_ACCENT} onPress={() => this.openModal("dur")}>
                  의약품 안전 점검(DUR)
                </Text>{" "}
                서비스는 등록된 처방전의 모든 약제를 비교, 분석하여{" "}
                <Text style={DESC_ACCENT} onPress={() => this.openModal("preg")}>
                  임부금기
                </Text>
                ,{" "}
                <Text style={DESC_ACCENT} onPress={() => this.openModal("byg")}>
                  병용금기
                </Text>
                ,{" "}
                <Text style={DESC_ACCENT} onPress={() => this.openModal("nosale")}>
                  사용중지
                </Text>
                ,{" "}
                <Text style={DESC_ACCENT} onPress={() => this.openModal("dis")}>
                  동일성분 중복
                </Text>
                ,{" "}
                <Text style={DESC_ACCENT} onPress={() => this.openModal("hng")}>
                  동일효능군 중복
                </Text>{" "}
                약제가 있을 경우 해당 약제에 대한 알림과 상세 설명을 제공합니다.
              </Text>
            </View>
          </View>

          <View style={BOTTOM_BUTTON}>
            <View style={DUR_SETTING_GUIDE}>
              <Text style={DUR_SETTING_GUIDE_TEXT}>
                {"* 마이페이지 > 앱설정에서 언제든지 알림을 비활성화 할 수 있습니다."}
              </Text>
            </View>
            <Button
              block
              rounded
              onPress={() => this.next()}
              style={{
                backgroundColor: "rgb(217, 217, 217)",
                marginBottom: 10,
              }}
            >
              <Text style={{ fontWeight: "bold", color: color.text }}>다음에 사용하기</Text>
            </Button>
            <Button block rounded onPress={() => this.enableDurAndNext()}>
              <Text style={{ fontWeight: "bold" }}>사용하기</Text>
            </Button>
          </View>

          <Modal
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            isVisible={this.state.isModalVisible}
            animationOut={"fadeOutDown"}
            // animationOutTiming={300}
            onBackdropPress={this.toggleModal}
          >
            <ScrollView
              contentContainerStyle={
                this.state.modalContentType === "dur"
                  ? { paddingTop: 60 }
                  : { flex: 1, justifyContent: "center" }
              }
            >
              <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
              <View style={{ alignItems: "flex-end" }}>
                <CloseButton
                  imageStyle={{ tintColor: "rgb(255, 255, 255)" }}
                  onPress={() => this.toggleModal()}
                  style={CLOSE_BUTTON}
                />
              </View>
              <View
                style={{
                  borderRadius: 10,
                  backgroundColor: "rgb(239, 239, 239)",
                  padding: 20,
                }}
              >
                {this.state.modalContentType === "dur" ? (
                  <Image source={popupDurImage} style={POPUP_DUR_IMAGE} />
                ) : null}
                <Text style={POPUP_TITLE}>{modalContents[this.state.modalContentType].title}</Text>
                <Text style={POPUP_CONTENT}>
                  {modalContents[this.state.modalContentType].content}
                </Text>
              </View>
            </ScrollView>
          </Modal>
        </ScrollView>
      </View>
    )
  }

  openModal(type: "dur" | "byg" | "dis" | "hng" | "nosale" | "preg") {
    this.setState({
      isModalVisible: true,
      modalContentType: type,
    })
  }

  async updateProfile(enableDur: boolean) {
    const user = this.props.accountStore.user
    const params = new FormData()
    params.append("user", user.id)
    if (!enableDur) {
      user.setDurNoti("n")
      params.append("dur_noti", "n")
    } else {
      this.checkNotiPermission()
      params.append("dur_noti", "y")
    }
    params.append("dur_hng", enableDur ? "y" : "n")
    params.append("dur_byg", enableDur ? "y" : "n")
    params.append("dur_dis", enableDur ? "y" : "n")
    params.append("dur_preg", "n")
    params.append("dur_nosale", enableDur ? "y" : "n")
    params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
    const result = await api.updateProfile(user.id, params)
    if (result.kind == "ok") {
      // user.setFirebaseToken0("")
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{"세션이 만료되었습니다. \n다시 로그인 해주세요."}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.navigate("Auth")
      }, 2000)
    }
  }

  async checkNotiPermission() {
    const authStatus = await firebase.messaging().hasPermission()

    if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
      console.tron.log("< HomeScreen > Push Notifications AUTHORIZED")
    } else {
      console.tron.log("< SettingsScreen > onDurNotiValueChange, user doesn't have permission")

      this.props.accountStore.setTemporaryPinCodeDisabled(true)
      try {
        if (Platform.OS === "ios") {
          const authStatus = await firebase.messaging().requestPermission({
            announcement: true,
          })
          if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
            console.tron.log("User has notification permissions enabled.")
          } else if (authStatus === firebase.messaging.AuthorizationStatus.PROVISIONAL) {
            console.tron.log("User has provisional notification permissions.")
          } else {
            console.tron.log("User has notification permissions disabled")
          }
        } else {
          const { status, settings } = await requestNotifications(["alert", "sound"])
          console.tron.log(status)
          console.tron.log(settings)
        }
      } catch (error) {
        console.tron.log("< SettingsScreen > onDurNotiValueChange, rejected permissions", error)
        if (Platform.OS === "ios") {
          Alert.alert(
            "",
            `알림 설정을 변경하려면, 다음 페이지에서 "알림"을 누르고 "알림 허용"을 버튼을 다시 한번 눌러 주세요.`,
            [
              {
                text: "확인",
                onPress: () => {
                  Linking.openURL("app-settings://notification/com.onns.papricacare")
                  // AndroidOpenSettings.appNotificationSettings()
                },
              },
            ],
            { cancelable: false },
          )
        } else {
          Alert.alert(
            "",
            `알림 설정을 변경하려면, 다음 페이지에서 "알림" 버튼을 눌러 주세요.`,
            [
              {
                text: "확인",
                onPress: () => {
                  // Linking.openURL('app-settings://notification/com.onns.papricacare')
                  AndroidOpenSettings.appNotificationSettings()
                },
              },
            ],
            { cancelable: false },
          )
        }
      }
      this.props.accountStore.setTemporaryPinCodeDisabled(false)
    }
  }

  async next() {
    const { navigation } = this.props
    if (this.bLoginProcess) {
      navigation.navigate("Home")
    } else {
      navigation.navigate("Welcome")
    }
  }

  async enableDurAndNext() {
    const { navigation } = this.props
    this.updateProfile(true)
    const user = this.props.accountStore.user
    if (user.gender === "female") {
      navigation.replace("CheckPregnancy")
    } else if (this.bLoginProcess) {
      navigation.navigate("Home")
    } else {
      navigation.navigate("Welcome")
    }
  }

  closeModal = () => {
    if (this.state.isModalVisible) {
      this.setState({ isModalVisible: false })
      return true
    }
    return false
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }
}
