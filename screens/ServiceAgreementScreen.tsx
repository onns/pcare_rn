import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Icon, ScrollView, Toast } from "native-base"
import React from "react"
import { Alert, Linking, Platform, StatusBar, TextStyle, View, ViewStyle } from "react-native"
import AndroidOpenSettings from "react-native-android-open-settings"
// import RNBootSplash from "react-native-bootsplash"
import { TouchableOpacity } from "react-native-gesture-handler"
import { requestNotifications } from "react-native-permissions"

import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { api, GeneralResult } from "../api"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import { Checkbox } from "../components/checkbox"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { formatDateWithJoinChar } from "../lib/DateUtil"
import {
  AuthStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, spacing, styles, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  flex: 1,
  backgroundColor: "#fff",
}

const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  paddingHorizontal: 20,
  paddingTop: 16,
}

const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingBottom: 32,
}

const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 32,
  color: color.text,
}
const DESCRIPTION: TextStyle = {
  fontSize: 13,
  letterSpacing: -0.26,
  color: "rgb(153, 153, 153)",
}
const ALL_AGREEMENT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16.5,
  lineHeight: 20,
  letterSpacing: -0.68,
  color: color.text,
}
const ALL_AGREEMENT_ANNEX: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 13,
  lineHeight: 16,
  letterSpacing: -0.52,
  color: color.palette.brownGrey,
  marginBottom: 10,
}
const CHECKBOX_TEXT: TextStyle = {
  flex: 1,
  fontSize: 15,
  letterSpacing: -0.6,
  color: color.text,
  marginRight: 0,
}
const BOTTOM_BUTTON: ViewStyle = {
  flexDirection: "column",
  justifyContent: "flex-end",
  marginBottom: 16,
  marginTop: 40,
}
const LIST_ITEM: ViewStyle = {
  flexDirection: "row",
  borderBottomWidth: 0,
  paddingRight: 0,
  alignItems: "center",
  paddingVertical: 6,
}

type ServiceAgreementRouteProp = RouteProp<AuthStackParamList, "ServiceAgreement">

type ServiceAgreementNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "ServiceAgreement">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<HomeStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: ServiceAgreementNavigationProp
  route: ServiceAgreementRouteProp
}

interface State {
  allChecked: boolean
  serviceTermChecked: boolean
  sensitiveInfoChecked: boolean
  personalInfoChecked: boolean
  locationChecked: boolean
  notificationsChecked: boolean
  marketingInfoChecked: boolean
}

@inject("accountStore", "prescriptionStore")
@observer
export default class ServiceAgreementScreen extends React.Component<Props, State> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route

    const isLoginProcess = params ? params.isLoginProcess : undefined
    if (isLoginProcess) {
      return {
        title: "",
        headerRight: () => (
          <Text
            style={[DESCRIPTION, { marginRight: 30 }]}
            onPress={() => navigation.navigate("DeleteAccount")}
          >
            회원탈퇴
          </Text>
        ),
      }
    } else {
      return { title: "" }
    }
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      allChecked: false,
      serviceTermChecked: false,
      sensitiveInfoChecked: false,
      personalInfoChecked: false,
      locationChecked: false,
      notificationsChecked: false,
      marketingInfoChecked: false,
    }
  }

  componentDidMount() {
    // RNBootSplash.hide({ fade: true })
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  isLoginProcess(params: any) {
    if (params) {
      return params.isLoginProcess
    } else {
      return undefined
    }
  }

  render() {
    const isLoginProcess = this.isLoginProcess(this.props.route.params)
    return (
      <View style={BACKGROUND}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={TITLE}>
            <Text style={TITLE_TEXT}>이용약관 동의</Text>
          </View>
          <View style={{ marginLeft: 10, paddingRight: 8 }}>
            <View
              style={{
                flexDirection: "row",
                borderBottomWidth: 0,
                paddingRight: 0,
                alignItems: "center",
              }}
            >
              <Checkbox
                preset="bgCircle"
                value={this.state.allChecked}
                onToggle={this.toggleAllCheckbox}
              />
              <Text style={ALL_AGREEMENT}>파프리카케어의 모든 운영원칙에 동의</Text>
            </View>
            <View
              style={{
                borderBottomWidth: 1,
                borderColor: color.palette.lightGrey2,
                paddingLeft: 26 + spacing[4],
              }}
            >
              <Text style={ALL_AGREEMENT_ANNEX}>(선택항목 포함)</Text>
            </View>
            {isLoginProcess ? null : (
              <View style={LIST_ITEM}>
                <Checkbox
                  preset="primary"
                  value={this.state.serviceTermChecked}
                  onToggle={this.toggleServiceTermCheckbox}
                />
                <Text style={CHECKBOX_TEXT}>서비스 이용약관</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("TermsOfService")}>
                  <SvgArrowRight width={24} height={24} color={color.palette.grey30} />
                </TouchableOpacity>
              </View>
            )}
            <View style={LIST_ITEM}>
              <Checkbox
                preset="primary"
                value={this.state.sensitiveInfoChecked}
                onToggle={this.toggleSensitiveInfoCheckbox}
              />
              <Text style={CHECKBOX_TEXT}>민감정보 수집이용 동의</Text>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("WebView", {
                    url: "https://web.papricacare.com/agrees/sensitive-info-agreement.html",
                  })
                }
              >
                <SvgArrowRight width={24} height={24} color={color.palette.grey30} />
              </TouchableOpacity>
            </View>
            <View style={LIST_ITEM}>
              <Checkbox
                preset="primary"
                value={this.state.personalInfoChecked}
                onToggle={this.togglePersonalInfoCheckbox}
              />
              <Text style={CHECKBOX_TEXT}>개인정보 수집이용 동의</Text>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("WebView", {
                    url: "https://web.papricacare.com/agrees/personal-data-agreement.html",
                  })
                }
              >
                <SvgArrowRight width={24} height={24} color={color.palette.grey30} />
              </TouchableOpacity>
            </View>
            <View style={LIST_ITEM}>
              <Checkbox
                preset="primary"
                value={this.state.notificationsChecked}
                onToggle={this.toggleNotificationsCheckbox}
              />
              <Text style={CHECKBOX_TEXT}>파프리카케어 컨시어지 (선택)</Text>
            </View>
            <View>
              <Text style={[DESCRIPTION, { marginLeft: 42, marginBottom: 12 }]}>
                의약품 안전 정보, 복용 완료일, 처방전 분석 완료 등 파프리카케어 사용 안내 알림을
                보내드립니다.
              </Text>
            </View>
            <View style={LIST_ITEM}>
              <Checkbox
                preset="primary"
                value={this.state.marketingInfoChecked}
                onToggle={this.toggleMarketingInfoCheckbox}
              />
              <Text style={CHECKBOX_TEXT}>마케팅 정보 수신 및 이용 동의 (선택)</Text>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("WebView", {
                    url: "https://web.papricacare.com/agrees/marketing-info-agreement.html",
                  })
                }
              >
                <SvgArrowRight width={24} height={24} color={color.palette.grey30} />
              </TouchableOpacity>
            </View>
            <View>
              <Text style={[DESCRIPTION, { marginLeft: 42 }]}>
                파프리카케어에서 제공하는 특별한 해택 및 이벤트, 사은품 등에 대한 안내를 받을 수
                있습니다.
              </Text>
            </View>
          </View>

          <View style={BOTTOM_BUTTON}>
            <View style={{ alignItems: "center" }}>
              <Text style={[DESCRIPTION, { paddingBottom: 20, paddingHorizontal: 4 }]}>
                *선택 약관에 동의하지 않아도 {isLoginProcess ? "사용" : "회원 가입"}이 가능합니다
              </Text>
            </View>
            <Button block onPress={() => this.next()}>
              <Text style={{ color: "white" }}>확인</Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    )
  }

  async next() {
    const isLoginProcess = this.isLoginProcess(this.props.route.params)
    const { accountStore, prescriptionStore, navigation } = this.props
    const { notificationsChecked, marketingInfoChecked } = this.state
    if (
      (this.state.serviceTermChecked || isLoginProcess) &&
      this.state.sensitiveInfoChecked &&
      this.state.personalInfoChecked
    ) {
      const userId = this.props.accountStore.user.id

      const now = new Date()
      const sNow = formatDateWithJoinChar(now, "-")
      const params = new FormData()
      params.append("user", userId)
      params.append("is_agree", true)

      if (isLoginProcess) {
        if (prescriptionStore.prescriptionsNumber == 0) {
          params.append("newly_agreed", "y")
          params.append("newly_agreed_date", sNow)
        }
      } else {
        params.append("newly_agreed", "y") // 민감정보 수집이용 동의 포함한 새로운 필수 약관 동의 필드
        params.append("newly_agreed_date", sNow)
      }
      params.append("app_ver", packageJson.version)
      params.append("app_device", Platform.OS + " v" + Platform.Version)

      try {
        const fcmToken = await firebase.messaging().getToken()
        if (fcmToken) {
          // firebase token 은 무조건 올림
          params.append("firebase_token0", fcmToken)
        }
      } catch (e) {
        console.tron.log("getToken() error")
      }

      if (notificationsChecked || marketingInfoChecked) {
        this.enablePushNotifications()
      }

      if (notificationsChecked) {
        params.append("presc_noti", "y")
      } else {
        params.append("presc_noti", "n")
      }

      if (marketingInfoChecked) {
        params.append("ad_noti", "y")
        params.append("ad_noti_date", sNow)
      } else {
        params.append("ad_noti", "n")
      }

      // 임부금기를 제외한 나머지 DUR 알림 설정 default on
      params.append("dur_noti", "y")
      params.append("dur_hng", "y")
      params.append("dur_byg", "y")
      params.append("dur_dis", "y")
      params.append("dur_nosale", "y")
      params.append("dur_preg", "n")

      // start making calls
      const result: GeneralResult = await api.updateProfile(userId, params)

      if (result.kind === "ok") {
        amplitude.getInstance().logEvent("agreed_terms_of_service")

        if (isLoginProcess) {
          if (prescriptionStore.prescriptionsNumber == 0) {
            accountStore.user.setNewlyAgreed("y")
            accountStore.user.setNewlyAgreedDate(now)
          }
        } else {
          accountStore.user.setNewlyAgreed("y")
          accountStore.user.setNewlyAgreedDate(now)
        }
        accountStore.user.setDurNoti("y")

        // workaround: 위 put api 호출 시 newly_agreed 이 null 로 변하는 경우가 있음
        if (prescriptionStore.prescriptionsNumber == 0) {
          const response = await api.apisauce.get(`/user/confirm/privacy-imediate/${userId}/y/`)
          if (!response.ok) {
            firebase
              .crashlytics()
              .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
          }
        }

        if (notificationsChecked || marketingInfoChecked) {
          await AsyncStorage.setItem("pushNotiEnabled", JSON.stringify(true))
        }

        if (this.props.accountStore.user.is_authenticated) {
          if (isLoginProcess) {
            navigation.navigate("Home")
          } else {
            navigation.navigate("Welcome")
          }
        } else {
          navigation.navigate("CertificationGuide")
          // navigation.navigate("UseOfDurService")
        }
      } else if (result.kind === "unauthorized") {
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => {
          navigation.navigate("Auth")
        }, 2000)
      } else if (result.kind === "rejected" && result.data?.detail === "찾을 수 없습니다.") {
        // profile 이 없는 경우
        const params = {
          user: userId,
          name:
            accountStore.user.first_name.length > 0
              ? accountStore.user.first_name
              : accountStore.user.username,
          nickname:
            accountStore.user.first_name.length > 0
              ? accountStore.user.first_name
              : accountStore.user.username,
        }
        const response2 = await api.apisauce.post(`/user/profiles/`, params)
        if (response2.ok) {
          if (accountStore.user.is_authenticated) {
            if (isLoginProcess) {
              navigation.navigate("Home")
            } else {
              navigation.navigate("Welcome")
            }
          } else {
            navigation.navigate("CertificationGuide")
          }
        }
      } else {
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
      }
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`필수 약관에 모두 동의하셔야 서비스 이용이 가능합니다.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  async enablePushNotifications() {
    const pushNotiEnabled = await AsyncStorage.getItem("pushNotiEnabled")

    if (!(pushNotiEnabled && JSON.parse(pushNotiEnabled))) {
      AsyncStorage.setItem("pushNotiEnabled", JSON.stringify(true))
    }

    const authStatus = await firebase.messaging().hasPermission()

    if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
      console.tron.log("< ServiceAgreementScreen > Push Notifications AUTHORIZED")
    } else {
      console.tron.log(
        "< ServiceAgreementScreen > onDurNotiValueChange, user doesn't have permission",
      )

      this.props.accountStore.setTemporaryPinCodeDisabled(true)
      try {
        if (Platform.OS === "ios") {
          const authStatus = await firebase.messaging().requestPermission({
            announcement: true,
          })
          if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
            console.tron.log("User has notification permissions enabled.")
          } else if (authStatus === firebase.messaging.AuthorizationStatus.PROVISIONAL) {
            console.tron.log("User has provisional notification permissions.")
          } else {
            console.tron.log("User has notification permissions disabled")
          }
        } else {
          const { status, settings } = await requestNotifications(["alert", "sound"])
          console.tron.log(status)
          console.tron.log(settings)
        }
      } catch (error) {
        console.tron.log("< SettingsScreen > onDurNotiValueChange, rejected permissions", error)
        if (Platform.OS === "ios") {
          Alert.alert(
            "",
            `알림 설정을 변경하려면, 다음 페이지에서 "알림"을 누르고 "알림 허용"을 버튼을 다시 한번 눌러 주세요.`,
            [
              {
                text: "확인",
                onPress: () => {
                  Linking.openURL("app-settings://notification/com.onns.papricacare")
                  // AndroidOpenSettings.appNotificationSettings()
                },
              },
            ],
            { cancelable: false },
          )
        } else {
          Alert.alert(
            "",
            `알림 설정을 변경하려면, 다음 페이지에서 "알림" 버튼을 눌러 주세요.`,
            [
              {
                text: "확인",
                onPress: () => {
                  // Linking.openURL('app-settings://notification/com.onns.papricacare')
                  AndroidOpenSettings.appNotificationSettings()
                },
              },
            ],
            { cancelable: false },
          )
        }
      }
      this.props.accountStore.setTemporaryPinCodeDisabled(false)
    }
  }

  toggleAllCheckbox = () => {
    this.setState({
      allChecked: !this.state.allChecked,
      serviceTermChecked: !this.state.allChecked,
      sensitiveInfoChecked: !this.state.allChecked,
      personalInfoChecked: !this.state.allChecked,
      locationChecked: !this.state.allChecked,
      notificationsChecked: !this.state.allChecked,
      marketingInfoChecked: !this.state.allChecked,
    })
  }

  toggleServiceTermCheckbox = () => {
    const {
      serviceTermChecked,
      sensitiveInfoChecked,
      personalInfoChecked,
      notificationsChecked,
      marketingInfoChecked,
    } = this.state
    this.setState({
      serviceTermChecked: !serviceTermChecked,
      allChecked: !!(
        !serviceTermChecked &&
        sensitiveInfoChecked &&
        personalInfoChecked &&
        notificationsChecked &&
        marketingInfoChecked
      ),
    })
  }

  toggleSensitiveInfoCheckbox = () => {
    const {
      serviceTermChecked,
      sensitiveInfoChecked,
      personalInfoChecked,
      notificationsChecked,
      marketingInfoChecked,
    } = this.state
    this.setState({
      sensitiveInfoChecked: !sensitiveInfoChecked,
      allChecked: !!(
        serviceTermChecked &&
        !sensitiveInfoChecked &&
        personalInfoChecked &&
        notificationsChecked &&
        marketingInfoChecked
      ),
    })
  }

  togglePersonalInfoCheckbox = () => {
    const {
      serviceTermChecked,
      sensitiveInfoChecked,
      personalInfoChecked,
      notificationsChecked,
      marketingInfoChecked,
    } = this.state
    this.setState({
      personalInfoChecked: !personalInfoChecked,
      allChecked: !!(
        serviceTermChecked &&
        sensitiveInfoChecked &&
        !personalInfoChecked &&
        notificationsChecked &&
        marketingInfoChecked
      ),
    })
  }

  toggleLocationCheckbox = () => {
    this.setState({
      locationChecked: !this.state.locationChecked,
      allChecked: false,
    })
  }

  toggleNotificationsCheckbox = () => {
    const {
      serviceTermChecked,
      sensitiveInfoChecked,
      personalInfoChecked,
      notificationsChecked,
      marketingInfoChecked,
    } = this.state
    this.setState({
      notificationsChecked: !notificationsChecked,
      allChecked: !!(
        serviceTermChecked &&
        sensitiveInfoChecked &&
        personalInfoChecked &&
        !notificationsChecked &&
        marketingInfoChecked
      ),
    })
  }

  toggleMarketingInfoCheckbox = () => {
    const {
      serviceTermChecked,
      sensitiveInfoChecked,
      personalInfoChecked,
      notificationsChecked,
      marketingInfoChecked,
    } = this.state
    this.setState({
      marketingInfoChecked: !marketingInfoChecked,
      allChecked: !!(
        serviceTermChecked &&
        sensitiveInfoChecked &&
        personalInfoChecked &&
        notificationsChecked &&
        !marketingInfoChecked
      ),
    })
  }
}
