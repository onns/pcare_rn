import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { FormControl, ScrollView, Toast, View } from "native-base"
import React, { Component } from "react"
import {
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import { launchImageLibrary } from "react-native-image-picker"
import Modal from "react-native-modal"
import DateTimePicker, {
  ConfirmButton,
  confirmButtonStyles,
} from "react-native-modal-datetime-picker"
import RBSheet from "react-native-raw-bottom-sheet"
import { SafeAreaView } from "react-native-safe-area-context"

import { api } from "@api/api"
import { Picker } from "@react-native-picker/picker"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import SvgProfileChangeButton from "../assets/images/profileChangeButton.svg"
import { BackgroundImage, FastImage } from "../components/BackgroundImage"
import TouchableItem from "../components/button/TouchableItem"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { getPopupButton, Popup } from "../components/Popup"
import { Text } from "../components/StyledText"
import { calcAge, formatDateKor } from "../lib/DateUtil"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { FamilyMember } from "../stores/FamilyMember"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

export const deviceWidth = Dimensions.get("window").width
export const deviceHeight = Dimensions.get("window").height

const cover = { uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png` }

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  backgroundColor: color.palette.white,
  paddingTop: 15,
}

const HEAD_CONTAINER: ViewStyle = {
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
  marginBottom: 15,
}

const AVATAR_CONTAINER: ViewStyle = {
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
}

const BODY_CONTAINER: ViewStyle = {
  flex: 4,
  backgroundColor: color.palette.gray5,
}

const INPUTBOX: ViewStyle = {
  borderBottomColor: "rgb(217, 217, 217)",
  borderBottomWidth: 1,
  flex: 1,
  paddingVertical: Platform.OS === "ios" ? 10 : 5,
  height: Platform.OS === "android" ? 80 : 0,
}

const TOUCHBOX: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  alignItems: "center",
  height: "100%",
}

const PROFILE_CONTAINER: ViewStyle = {
  flex: Platform.OS === "ios" ? 1 : 2,
  height: 200,
  paddingLeft: 16,
  paddingRight: 16,
  backgroundColor: color.palette.white,
  marginTop: 15,
}

const BOTTOM_CONTAINER: ViewStyle = {
  flex: Platform.OS === "ios" ? 2 : 3,
  borderTopWidth: 1,
  borderTopColor: color.palette.gray4,
}

const INPUT_STYLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  backgroundColor: color.palette.white,
  color: color.palette.grey50,
  marginLeft: Platform.OS === "ios" ? 15.5 : 30,
  flex: Platform.OS === "ios" ? 2 : 5,
}

const BIRTH_DATE_INPUT: TextStyle = {
  flex: Platform.OS === "ios" ? 2 : 5,
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.palette.grey50,
  marginLeft: Platform.OS === "ios" ? 15.5 : 30,
  textAlignVertical: "center",
}

const STACKED_LABEL: TextStyle = {
  flex: Platform.OS === "ios" ? 1 : 2,
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.palette.lightBlack2,
  padding: 0,
}

const DELETE_BUTTON_TEXT: TextStyle = {
  color: color.palette.orange,
  fontSize: 13,
  paddingRight: 16,
  marginTop: 20,
}

const BUTTON_TEXT: TextStyle = {
  flex: 1,
  textAlign: "right",
  color: color.palette.gray1,
  fontSize: 13,
}

const PICKER_CONTAINER: ViewStyle = {
  minHeight: 220,
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
  backgroundColor: color.palette.white,
  justifyContent: "flex-end",
  flexDirection: "column",
}
const PICKER_BTN: ViewStyle = {
  height: 60,
  borderRadius: 45,
  marginVertical: 10,
  marginHorizontal: 20,
  backgroundColor: color.primary,
}
const PICKER_TEXT: TextStyle = {
  fontSize: 17,
  fontWeight: "bold",
  paddingTop: 15,
  color: color.palette.white,
  textAlign: "center",
}

const customConfirmButtonStyles = {
  ...confirmButtonStyles,
  text: {
    textAlign: "center",
    color: color.palette.white,
    height: 50,
    padding: 15,
    fontSize: 18,
    fontWeight: "bold",
  },
}

const DATE_PICKER_CONFIRM_BTN: ViewStyle = { backgroundColor: color.primary, borderRadius: 45 }
const DATETIMEPICKER_CONTAINER: ViewStyle = {
  paddingHorizontal: 15,
  borderTopLeftRadius: 40,
  borderTopRightRadius: 40,
  paddingBottom: 24,
  bottom: -16,
}

type EditFamilyMemberRouteProp = RouteProp<DetailsStackParamList, "EditFamilyMember">

type EditFamilyMemberNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "EditFamilyMember">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<MyPageStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: EditFamilyMemberNavigationProp
  route: EditFamilyMemberRouteProp
  prescriptionStore: PrescriptionStore
}

interface State {
  isDateTimePickerVisible: boolean
  isOver14: boolean
  isMale: boolean
  imageType: any
  imageUri: any
  fileName: any
  visiblePopup: boolean
  popupTitle: string
  popupMessage: string
  statusBtn1: string
  index: number
  isManager: boolean
  isGenderPickerVisible: boolean
  genderSelected: any
}

@inject("accountStore", "prescriptionStore")
@observer
export default class EditFamilyMemberScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation, route }) => ({
    title: route.params.isManager ? "내 처방 관리 회원" : "가족회원",
    headerLeft: () => (
      <TouchableItem
        onPress={() => navigation.pop()}
        style={{ paddingLeft: Platform.OS === "android" ? 10 : 0 }}
      >
        <CustomHeaderBackImage />
      </TouchableItem>
    ),
  })

  bottomSheetRef = React.createRef<RBSheet>()

  birthDate: Date = new Date()
  gender = ""
  member: FamilyMember
  memberIndex = -1

  constructor(props: Props) {
    super(props)

    const { params } = this.props.route
    let imageUri
    let imageType
    let fileName
    let profileName
    let profileGender
    let profileBirth
    if (params) {
      imageUri = this.getImageUri(params)
      imageType = this.getImageType(params)
      if (!String(imageUri).includes("content:")) {
        const tempArray = String(imageUri).split("/")
        fileName = tempArray[tempArray.length - 1]
      }

      profileName = this.getProfileName(params)
      profileGender = this.getProfileGender(params)
      profileBirth = this.getProfileBirth(params)
    }

    this.member = this.getMemberInfo(this.props.route.params)
    // if (params && imageUri) {
    //   this.member.setProfileImage(imageUri)
    // }
    if (profileName) {
      this.member.setName(profileName)
    }
    if (profileGender) {
      this.member.setGender(profileGender)
    }
    if (profileBirth) {
      this.member.setBirthDate(profileBirth)
    }

    this.state = {
      visiblePopup: false,
      popupTitle: "",
      popupMessage: "",
      statusBtn1: "",
      index: 0,
      isManager: false,
      isDateTimePickerVisible: false,
      isOver14: this.member.isOver14,
      isMale: profileGender ? profileGender === "M" : this.member.gender === "M",
      imageType: params && imageType ? imageType : undefined,
      imageUri: imageUri || this.member.img,
      fileName: fileName || undefined,
      genderSelected: this.member.gender,
      isGenderPickerVisible: false,
    }
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
  }

  handleBackPress = () => {
    const imageUri = this.getImageUri(this.props.route.params)
    const imageType = this.getImageType(this.props.route.params)

    this.props.navigation.pop()
    this.props.navigation.navigate("FamilyMembers")
    this._submit(imageUri, imageType)
    return true
  }

  getProfileName(params: any): any {
    if (params) {
      return params.profileName
    } else {
      return undefined
    }
  }

  getProfileGender(params: any): any {
    if (params) {
      return params.profileGender
    } else {
      return undefined
    }
  }

  getProfileBirth(params: any): any {
    if (params) {
      return params.profileBirth
    } else {
      return undefined
    }
  }

  getImageUri(params: any): any {
    if (params) {
      return params.imageUri
    } else {
      return undefined
    }
  }

  getImageType(params: any): any {
    if (params) {
      return params.imageType
    } else {
      return undefined
    }
  }

  getPrevRouteName(params: any): any {
    if (params) {
      return params.prevRouteName
    } else {
      return undefined
    }
  }

  getMemberInfo(params: any): any {
    let member
    if (params.isManager) {
      member = this.props.accountStore.myPrescriptionManagers[params.memberIndex]
    } else {
      member = this.props.accountStore.family[params.memberIndex]
    }
    return member
  }

  _handleDateTimePicked = (date: Date) => {
    this.member.setBirthDate(date)
    this.setState({ isOver14: this.member.isOver14 })
    this._hideDateTimePicker()
    this._submit()
  }

  setBirthDate(date: Date) {
    this.birthDate = date
  }

  formatDate(d: Date) {
    let month = "" + (d.getMonth() + 1)
    let day = "" + d.getDate()
    const year = d.getFullYear()

    if (month.length < 2) month = "0" + month
    if (day.length < 2) day = "0" + day

    return [year, month, day].join("-")
  }

  async _submit(imageUri?: string, imageType?: string) {
    if (this.state.isOver14) {
      this.addFamilyMember()
    } else {
      this.addChild(imageUri, imageType)
    }
  }

  // TODO: 불필요한 코드 정리
  async addFamilyMember() {
    if (!this.props.accountStore.user.is_authenticated) {
      Alert.alert(
        "",
        `본인인증 이후에 14세 이상의 가족회원을 추가하실 수 있습니다.`,
        [
          { text: "닫기", onPress: () => null },
          {
            text: "본인인증",
            onPress: () =>
              this.props.navigation.navigate("CertificationDetail", {
                previousScreen: "AddFamilyMemberScreen",
              }),
          },
        ],
        { cancelable: false },
      )
      return
    } else if (!this.member.phone || this.member.phone.length == 0) {
      Alert.alert("", `전화번호를 입력해 주세요.`, [{ text: "확인", onPress: () => null }], {
        cancelable: false,
      })
      return
    } else if (this.member.phone && this.member.phone.length != (10 && 11)) {
      Alert.alert("", `올바른 전화번호를 입력해 주세요.`, [{ text: "확인", onPress: () => null }], {
        cancelable: false,
      })
      return
    }

    const store = this.props.accountStore
    const reqParams = new FormData()

    reqParams.append("id", this.member.id)
    reqParams.append("name", this.member.name)
    reqParams.append(
      "birth_date",
      this.member.birth_date !== undefined ? this.formatDate(this.member.birth_date) : "",
    )
    reqParams.append("gender", this.member.gender)
    reqParams.append("user", this.props.accountStore.user.id)

    const result = await api.uploadFamilyProfileImage(reqParams, this.member.id)

    if (result.kind === "ok") {
      this.member.setProfileImage(result.data.img)
      store.changeFamilyMember(this.memberIndex, this.member)
      this.props.navigation.pop()
      this.props.navigation.navigate("EditFamilyMember")
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      Alert.alert(
        "",
        `서버에 저장하지 못 하였습니다. \n나중에 다시 시도해주세요.`,
        [{ text: "확인", onPress: () => null }],
        { cancelable: false },
      )
      // self.setStatus("error")
      console.tron.log("< EditFamilyMemberScreen > addFamilyMember, update error")
    }
  }

  async addChild(imageUri: string, imageType: string) {
    if (!this.member.name || this.member.name.trim().length == 0) {
      Alert.alert("", `이름을 입력하세요.`, [{ text: "확인", onPress: () => null }], {
        cancelable: false,
      })
      return
    } else if (this.props.accountStore.hasName(this.member.name.trim(), this.member.id)) {
      Alert.alert(
        "",
        `해당 가족회원 이름은 현재 사용 중 입니다. \n다른 이름으로 등록해 주세요.`,
        [{ text: "확인", onPress: () => null }],
        { cancelable: false },
      )
      return
    } else if (this.member.birth_date == undefined) {
      Alert.alert("", `생년월일을 입력하세요.`, [{ text: "확인", onPress: () => null }], {
        cancelable: false,
      })
      return
    } else if (calcAge(this.member.birth_date) > 14) {
      Alert.alert(
        "",
        `14세 이상의 가족회원은 14세 이상 가족회원으로 다시 등록해 주세요.`,
        [{ text: "확인", onPress: () => null }],
        { cancelable: false },
      )
      return
    } else if (this.member.birth_date && calcAge(this.member.birth_date) < 0) {
      Alert.alert("", `생년월일을 다시 확인해주세요.`, [{ text: "확인", onPress: () => null }], {
        cancelable: false,
      })
      return
    }

    const store = this.props.accountStore

    const now = new Date()
    const reqParams = new FormData()

    reqParams.append("id", this.member.id)
    reqParams.append("name", this.member.name)
    reqParams.append(
      "birth_date",
      this.member.birth_date != undefined ? this.formatDate(this.member.birth_date) : "",
    )
    reqParams.append("gender", this.member.gender)
    reqParams.append("user", this.props.accountStore.user.id)

    if (imageType && imageUri.length > 0) {
      const tempArray = String(imageUri).split("/")
      const fileName = tempArray[tempArray.length - 1]
      reqParams.append("img", {
        uri: imageUri,
        type: imageType,
        name: tempArray[0].includes("content:")
          ? this.props.accountStore.user.id + "_" + now.toISOString() + ".jpg"
          : fileName,
      })
    } else if (this.state.imageType) {
      reqParams.append("img", {
        uri: this.state.imageUri,
        type: this.state.imageType,
        name: this.state.fileName
          ? this.state.fileName
          : this.props.accountStore.user.id + "_" + now.toISOString() + ".jpg",
      })
    }

    const result = await api.uploadChildProfileImage(reqParams, this.member.id)

    if (result.kind === "ok") {
      this.member.setProfileImage(result.data.img)
      store.changeFamilyMember(this.props.route.params.memberIndex, this.member)
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      Alert.alert(
        "",
        `서버에 저장하지 못 하였습니다. \n나중에 다시 시도해주세요.`,
        [{ text: "확인", onPress: () => null }],
        { cancelable: false },
      )
      // self.setStatus("error")
      console.tron.log("< EditFamilyMemberScreen > addChild, update error")
    }
  }

  cancel = () => {
    // TODO: 가족회원 정보에 대해 변경한 내용이 있으면 Alert 팝업창 띄우자
    this.props.navigation.pop()
    this.props.navigation.navigate("FamilyMembers")
  }

  _handleProfileImage = () => {
    console.warn("Not implemented")
  }

  showRemoveMemberPopup(index: number, isManager?: boolean) {
    const store = this.props.accountStore
    let member
    if (isManager) {
      member = store.myPrescriptionManagers[index]
    } else {
      member = store.family[index]
    }

    if (member.family_user) {
      // 14세 이상 가족회원

      if (isManager) {
        // 내 처방전 관리 회원
        this.setState({
          visiblePopup: true,
          index: index,
          isManager: isManager,
          popupTitle: "내 처방전 관리 회원을 삭제하시겠습니까?",
          popupMessage: `내 처방 관리 회원을 삭제하면 해당 회원님이 더이상 고객님의 처방 내역을 관리 할 수 없습니다. 정말로 삭제 하시겠습니까?`,
        })
        // 14세 이상
      } else {
        this.setState({
          visiblePopup: true,
          index: index,
          isManager: isManager,
          popupTitle: "가족회원을 삭제하시겠습니까?",
          popupMessage:
            "가족회원을 삭제하면 해당 가족회원의 처방전을 더이상 관리할 수 없습니다. 정말로 삭제하시겠습니까?",
        })
      }
      // 14세 미만
    } else {
      this.setState({
        visiblePopup: true,
        index: index,
        isManager: isManager,
        popupTitle: "가족회원을 삭제하시겠습니까?",
        popupMessage:
          "가족회원을 삭제하면 해당 가족회원에 등록된 모든 처방전 내역은 삭제되며, 복구할 수 없습니다. 정말로 삭제하시겠습니까?",
      })
    }
  }

  async removeMember(index: number, isManager: boolean) {
    const store = this.props.accountStore
    let member
    if (isManager) {
      member = store.myPrescriptionManagers[index]
    } else {
      member = store.family[index]
    }

    const reqParams = {
      id: member.id,
    }
    let result
    // if (member.isOver14) {
    if (member.family_user) {
      // 14세 이상 가족회원
      result = await api.deleteFamilyMember(reqParams)
    } else {
      result = await api.deleteChild(reqParams)
    }

    if (result.kind === "ok") {
      if (isManager) {
        this.props.accountStore.removeMyManager(index, this.props.prescriptionStore)
      } else {
        this.props.accountStore.removeFamilyMember(index, this.props.prescriptionStore)
      }
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      // self.setStatus("error")
      console.tron.log("< EditFamilyMemberScreen > removeMember, deleteFamilyMember error")
    }
  }

  renderBottomSheet() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: color.palette.white,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <View style={{ marginTop: 30 }}>
          <TouchableHighlight
            style={{ ...styles.BOTTOM_SHEET_BUTTON, marginBottom: 5 }}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              setTimeout(() => {
                this.setState({ genderSelected: "M" })
                this.onGenderValueChange("M")
                this._submit()
              }, 200)
            }}
          >
            <Text style={{ ...styles.BOTTOM_SHEET_BUTTON_TEXT, fontWeight: "bold" }}>남</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={{ ...styles.BOTTOM_SHEET_BUTTON, marginBottom: 5 }}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              setTimeout(() => {
                this.setState({ genderSelected: "F" })
                this.onGenderValueChange("F")
                this._submit()
              }, 200)
            }}
          >
            <Text style={{ ...styles.BOTTOM_SHEET_BUTTON_TEXT, fontWeight: "bold" }}>여</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={PICKER_BTN}
            underlayColor={color.palette.grey2}
            onPress={() => this.bottomSheetRef.current.close()}
          >
            <Text style={PICKER_TEXT}>취소</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  deletePopupButton = () => {
    this.removeMember(this.state.index, this.state.isManager)
    this.setState({ visiblePopup: false })
    this.props.navigation.goBack()
  }

  render() {
    const { navigation, route, accountStore } = this.props
    const { family, myPrescriptionManagers } = accountStore
    const { memberIndex, isManager } = route.params
    const imageUri = this.getImageUri(route.params)
    const imageType = this.getImageType(route.params)
    const memberName = isManager
      ? myPrescriptionManagers[memberIndex]?.name
      : family[memberIndex]?.name

    return (
      <View style={{ backgroundColor: color.background, flex: 1 }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={{ flex: 1 }}>
            <FormControl style={{ flex: 1 }}>
              <View style={HEAD_CONTAINER}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("ChangeProfileImage", {
                      prevRouteName: route.name,
                      prevPrevRouteName: this.getPrevRouteName(route.params),
                      memberIndex: route.params.memberIndex,
                    })
                  }
                  style={AVATAR_CONTAINER}
                  disabled={!!this.member.family_user}
                >
                  <BackgroundImage
                    resizeMode={FastImage.resizeMode.cover}
                    image={{ width: 81, height: 81, borderRadius: 40.5 }}
                    source={
                      imageUri
                        ? { uri: imageUri }
                        : this.state.imageUri
                        ? { uri: this.state.imageUri }
                        : cover
                    }
                  />
                  <View
                    style={{
                      width: 81,
                      height: 81,
                      alignItems: "flex-end",
                      justifyContent: "flex-end",
                    }}
                  >
                    {this.member.family_user ? null : <SvgProfileChangeButton />}
                  </View>
                </TouchableOpacity>
              </View>
              <View style={BODY_CONTAINER}>
                <View style={PROFILE_CONTAINER}>
                  <View style={INPUTBOX}>
                    <TouchableOpacity
                      style={TOUCHBOX}
                      onPress={() =>
                        navigation.navigate("EditMemberName", {
                          memberIndex: route.params.memberIndex,
                        })
                      }
                      disabled={!!this.member.family_user}
                    >
                      <Text style={STACKED_LABEL}>이름</Text>
                      <Text style={[INPUT_STYLE, { borderBottomWidth: 0 }]}>{memberName}</Text>
                      {this.member.family_user === undefined ? (
                        <Text style={BUTTON_TEXT}>수정</Text>
                      ) : null}
                    </TouchableOpacity>
                  </View>
                  <View style={INPUTBOX}>
                    <TouchableOpacity
                      style={TOUCHBOX}
                      onPress={Platform.OS === "ios" ? this._showGenderPicker : this.onItemPress}
                      disabled={!!this.member.family_user}
                    >
                      <Text style={STACKED_LABEL}>성별</Text>
                      <Text style={[INPUT_STYLE, { borderBottomWidth: 0 }]}>
                        {this.member.gender
                          ? this.member.gender.toLowerCase()[0] == "m"
                            ? "남"
                            : "여"
                          : ""}
                      </Text>
                      {this.member.family_user === undefined ? (
                        <Text style={BUTTON_TEXT}>수정</Text>
                      ) : null}
                    </TouchableOpacity>
                  </View>
                  <View style={[INPUTBOX, { borderBottomWidth: 0 }]}>
                    <TouchableOpacity
                      style={TOUCHBOX}
                      onPress={this._showDateTimePicker}
                      disabled={!!this.member.family_user}
                    >
                      <Text style={STACKED_LABEL}>생년월일</Text>
                      <Text style={[BIRTH_DATE_INPUT, { borderBottomWidth: 0 }]}>
                        {this.member.birth_date ? formatDateKor(this.member.birth_date) : ""}
                      </Text>
                      {this.member.family_user === undefined ? (
                        <Text style={BUTTON_TEXT}>수정</Text>
                      ) : null}
                    </TouchableOpacity>
                  </View>
                </View>
                {isManager ? (
                  <View style={BOTTOM_CONTAINER}>
                    <TouchableOpacity
                      style={{ flexDirection: "row-reverse" }}
                      onPress={() => this.showRemoveMemberPopup(memberIndex, isManager)}
                    >
                      <Text style={DELETE_BUTTON_TEXT}>내 처방 관리 회원 삭제</Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View style={BOTTOM_CONTAINER}>
                    <TouchableOpacity
                      style={{ flexDirection: "row-reverse" }}
                      onPress={() => this.showRemoveMemberPopup(memberIndex, isManager)}
                    >
                      <Text style={DELETE_BUTTON_TEXT}>가족회원 삭제</Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            </FormControl>
          </View>
        </ScrollView>
        <DateTimePicker
          display="spinner"
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDateTimePicked}
          onCancel={this._hideDateTimePicker}
          date={this.member.birth_date}
          confirmTextIOS={"확인"}
          customCancelButtonIOS={() => <View />}
          customConfirmButtonIOS={(buttonProps) => (
            <View style={DATE_PICKER_CONFIRM_BTN}>
              <ConfirmButton {...buttonProps} style={customConfirmButtonStyles} />
            </View>
          )}
          customHeaderIOS={() => <View />}
          pickerContainerStyleIOS={DATETIMEPICKER_CONTAINER}
        />
        <Popup
          isVisible={this.state.visiblePopup}
          title={this.state.popupTitle}
          message={this.state.popupMessage}
          button1={getPopupButton(this.deletePopupButton, "삭제")}
          button2={getPopupButton(() => this.setState({ visiblePopup: false }), "취소", "cancel")}
        />
        <Modal
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
          isVisible={this.state.isGenderPickerVisible}
          onBackdropPress={() => {
            this.setState({
              isGenderPickerVisible: false,
            })
          }}
          style={{
            justifyContent: "flex-end",
            marginTop: 0,
            marginBottom: 0,
            marginLeft: 0,
            marginRight: 0,
          }}
        >
          <View style={{ ...PICKER_CONTAINER, paddingBottom: 8 }}>
            <Picker
              style={{
                height: 100,
                justifyContent: "center",
              }}
              selectedValue={this.state.genderSelected}
              onValueChange={(itemValue) => this.setState({ genderSelected: itemValue })}
              itemStyle={{
                color: color.palette.lightBlack2,
                fontWeight: "bold",
              }}
            >
              <Picker.Item label="남" value="M" />
              <Picker.Item label="여" value="F" />
            </Picker>
            <SafeAreaView>
              <TouchableOpacity
                style={PICKER_BTN}
                onPress={() => {
                  this.setState({ isGenderPickerVisible: false })
                  this.onGenderValueChange(this.state.genderSelected)
                  this._submit(imageUri, imageType)
                }}
              >
                <Text style={PICKER_TEXT}>확인</Text>
              </TouchableOpacity>
            </SafeAreaView>
          </View>
        </Modal>
        <RBSheet
          ref={this.bottomSheetRef}
          height={styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * 3 + 14}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            container: {
              backgroundColor: "transparent",
            },
          }}
        >
          <StatusBar backgroundColor={color.dimmedStatusBar} />
          {this.renderBottomSheet()}
        </RBSheet>
      </View>
    )
  }

  launchImageLibrary() {
    launchImageLibrary({ mediaType: "photo", quality: 0.8, includeBase64: true }, (response) => {
      if (response.didCancel) {
        console.tron.log(
          "< EditFamilyMemberScreen > launchImageLibrary, User cancelled image picker",
        )
      } else if (response.errorMessage) {
        console.tron.log(
          "< EditFamilyMemberScreen > launchImageLibrary, ImagePicker errorMessage: ",
          response.errorMessage,
        )
      } else {
        this.setState({
          imageType: response.type,
        })
        this.member.setProfileImage(response.uri)
      }
    })
  }

  onGenderValueChange(value: "" | "M" | "F" | "male" | "female") {
    this.member.setGender(value)
  }

  onCheckboxValueChange() {
    this.setState({
      isOver14: !this.state.isOver14,
    })
  }

  onChangePhone = (text: string) => {
    this.member.setPhone(text)
  }

  _showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true })
  }

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  _showGenderPicker = () => {
    this.setState({ isGenderPickerVisible: !this.state.isGenderPickerVisible })
  }

  onItemPress = () => {
    this.bottomSheetRef.current.open()
  }
}
