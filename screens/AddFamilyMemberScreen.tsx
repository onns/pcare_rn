import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import {
  Container,
  FormControl,
  HStack,
  InputGroup,
  ScrollView,
  Stack,
  Toast,
  View,
} from "native-base"
import React, { Component } from "react"
import {
  Alert,
  BackHandler,
  Dimensions,
  Image,
  Platform,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import Modal from "react-native-modal"
import DateTimePicker, {
  ConfirmButton,
  confirmButtonStyles,
} from "react-native-modal-datetime-picker"
import RBSheet from "react-native-raw-bottom-sheet"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"

import firebase from "@react-native-firebase/app"
import { Picker } from "@react-native-picker/picker"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { api, FamilyMemberResult } from "../api"
import { DEFAULT_API_CONFIG } from "../api/api-config"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import { BackgroundImage, FastImage } from "../components/BackgroundImage"
import TouchableItem from "../components/button/TouchableItem"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { getPopupButton, Popup } from "../components/Popup"
import { Button } from "../components/StyledButton"
import { Input } from "../components/StyledInput"
import { Text } from "../components/StyledText"
import { calcAge, formatDateKor } from "../lib/DateUtil"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { avatarSources } from "../screens/ChangeProfileImageScreen"
import { AccountStore } from "../stores/AccountStore"
import { FamilyMember, FamilyMemberModel } from "../stores/FamilyMember"
import { color, styles, typography } from "../theme"

export const deviceWidth = Dimensions.get("window").width
export const deviceHeight = Dimensions.get("window").height

// 지정된 범위의 정수 1개를 랜덤하게 반환하는 함수
// n1 은 "하한값", n2 는 "상한값"
function randomRange(n1: number, n2: number) {
  return Math.floor(Math.random() * (n2 - n1 + 1) + n1)
}

const cover = { uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png` }

const CONTENT_CONTAINER: ViewStyle = {
  paddingTop: 36,
  paddingHorizontal: 20,
}

const INPUTBOX: ViewStyle = {
  borderBottomColor: "rgb(217, 217, 217)",
  borderBottomWidth: 1,
  flexDirection: "row",
  alignItems: "center",
  flex: 1,
  paddingVertical: 16,
}

const INPUT: ViewStyle = {
  flex: 3,
}

const INPUT_STYLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  paddingLeft: 15.5,
}
const PICKER_TEXT_STYLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  paddingLeft: 22,
}

const STACKED_LABEL: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  marginTop: 2,
  flex: 1,
}

const ROW: ViewStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  alignItems: "center",
}

const MESSAGE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.text,
  paddingLeft: 12,
  marginBottom: 2,
  paddingTop: 20,
}

const BOTTOM_BUTTONS_VIEW: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  padding: 20,
}

const BOTTOM_BUTTON: ViewStyle = { height: 55, flex: 1, marginBottom: 10 }
const PHONE_INPUT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  fontWeight: "bold",
  color: color.text,
  paddingLeft: 12,
}

const OVER14_GUIDE_BOX: ViewStyle = {
  backgroundColor: color.palette.lightGrayishOrange,
  justifyContent: "center",
  alignItems: "center",
  marginTop: 20,
  padding: 16,
}

const OVER14_GUIDE_TEXT: TextStyle = {
  fontSize: 13,
  fontFamily: typography.primary,
  color: color.text,
  lineHeight: 20,
  flexDirection: "row",
}

const BUTTON_TEXT: TextStyle = {
  flex: 1,
  fontFamily: typography.bold,
  fontSize: 17,
  color: color.palette.white,
  // opacity: 0.7,
  fontWeight: "500",
  textAlign: "center",
}

const PICKER_CONTAINER: ViewStyle = {
  minHeight: 220,
  borderTopLeftRadius: 40,
  borderTopRightRadius: 40,
  backgroundColor: color.palette.white,
  justifyContent: "flex-end",
  flexDirection: "column",
}

const PICKER_BTN: ViewStyle = {
  height: 60,
  borderRadius: 45,
  marginHorizontal: 20,
  backgroundColor: color.primary,
}

const PICKER_TEXT: TextStyle = {
  fontSize: 17,
  fontWeight: "bold",
  paddingTop: 15,
  color: color.palette.white,
  textAlign: "center",
}

const customConfirmButtonStyles: ViewStyle = {
  ...confirmButtonStyles,
  text: {
    color: color.palette.white,
    textAlign: "center",
    height: 50,
    padding: 15,
    fontSize: 18,
    fontWeight: "bold",
  },
}

const DATETIMEPICKER_CONTAINER: ViewStyle = {
  paddingHorizontal: 15,
  borderTopLeftRadius: 40,
  borderTopRightRadius: 40,
  paddingBottom: 24,
  bottom: -16,
}

type AddFamilyMemberRouteProp = RouteProp<DetailsStackParamList, "AddFamilyMember">

type AddFamilyMemberScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "AddFamilyMember">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<MyPageStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  // navigation: StackNavigationProp<ParamListBase>
  navigation: AddFamilyMemberScreenNavigationProp
  route: AddFamilyMemberRouteProp
}

interface State {
  member: any
  isDateTimePickerVisible: boolean
  isGenderPickerVisible: boolean
  isOver14: boolean
  isMale: boolean
  imageType: any
  fileName: any
  radioSelected: number
  submitDisabled: boolean
  popupTitle: string
  popupMessage: string
  addFamilyOver14: boolean
  addFamilyUnder14: boolean
  genderSelected: any
  onBackButtonPress?: () => void
  date: any
  isGenderText: boolean
}

const hitSlop10 = { top: 10, left: 10, right: 10, bottom: 10 }
const HEADER_CANCEL_TEXT = {
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 24,
  color: color.primary,
}
const HEADER_RIGHT = { marginRight: 23 }
@inject("accountStore")
@observer
export default class AddFamilyMemberScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "가족회원추가",
      headerLeft: () => (
        <TouchableItem
          onPress={() => {
            navigation.pop()
            // navigation.navigate("FamilyMembers")
          }}
          style={{ paddingLeft: Platform.OS === "android" ? 10 : 0 }}
        >
          <CustomHeaderBackImage />
        </TouchableItem>
      ),
      headerRight: () => (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={HEADER_RIGHT}
          hitSlop={hitSlop10}
        >
          <Text style={HEADER_CANCEL_TEXT}>취소</Text>
        </TouchableOpacity>
      ),
    }
  }

  bottomSheetRef = React.createRef<RBSheet>()

  birthDate: Date = new Date()
  gender = ""
  member = FamilyMemberModel.create({
    id: -1,
    name: "",
    birth_date: undefined,
    gender: "",
    familyMemberKey: "_",
  })

  constructor(props: Props) {
    super(props)

    const { params } = this.props.route
    let imageUri
    let imageType
    let fileName
    let profileName
    let profileGender
    let profileBirth
    if (params) {
      imageUri = this.getImageUri(params)
      imageType = this.getImageType(params)
      if (imageUri) {
        const tempArray = String(imageUri).split("/")
        fileName = tempArray[tempArray.length - 1]
      }

      profileName = this.getProfileName(params)
      profileGender = this.getProfileGender(params)
      profileBirth = this.getProfileBirth(params)
      if (profileName) {
        this.member.setName(profileName)
      }
      if (profileGender) {
        this.member.setGender(profileGender)
      }
      if (profileBirth) {
        this.member.setBirthDate(profileBirth)
      }
    }
    const randomProfileImage = this.rotateProfileImage()
    this.state = {
      member: {
        user_id: "",
        name: profileName || "",
        birth_date: profileBirth || undefined,
        gender: profileGender || "M",
        phone: "",
        imageType: undefined,
        img: params && imageUri ? imageUri : cover.uri,
      },
      isDateTimePickerVisible: false,
      isGenderPickerVisible: false,
      isOver14: false,
      isMale: !!(!profileGender || profileGender == "M"),
      imageType: params && imageType ? imageType : "image/png",
      fileName: params && fileName ? fileName : randomProfileImage.fileName,
      radioSelected: 0,
      submitDisabled: false,
      popupTitle: "",
      popupMessage: "",
      addFamilyOver14: false,
      addFamilyUnder14: false,
      genderSelected: Platform.OS === "ios" ? "M" : "",
      isGenderText: false,
      date: new Date(1598051730000),
    }
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress)
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    const params = nextProps.route.params

    let profileName
    let profileGender
    let profileBirth
    if (params) {
      profileName = this.getProfileName(params)
      profileGender = this.getProfileGender(params)
      profileBirth = this.getProfileBirth(params)
      if (profileName) {
        this.member.setName(profileName)
      }
      if (profileGender) {
        this.member.setGender(profileGender)
      }
      if (profileBirth) {
        this.member.setBirthDate(profileBirth)
      }
    }
  }

  handleBackPress = () => {
    this.props.navigation.goBack()
    // this.goBack()
    return true
  }

  goBack() {
    // this.props.navigation.pop()
    // const prevRouteName = this.getPrevRouteName(this.props.route.params)
    // if (prevRouteName) {
    //   this.props.navigation.navigate(prevRouteName)
    // } else {
    //   this.props.navigation.navigate("FamilyMembers")
    // }
    this.props.navigation.goBack()
  }

  getPrevRouteName(params: any): any {
    if (params) {
      return params.prevRouteName
    } else {
      return undefined
    }
  }

  getProfileName(params: any): any {
    if (params) {
      return params.profileName
    } else {
      return undefined
    }
  }

  getProfileGender(params: any): any {
    if (params) {
      return params.profileGender
    } else {
      return undefined
    }
  }

  getProfileBirth(params: any): any {
    if (params) {
      return params.profileBirth
    } else {
      return undefined
    }
  }

  getImageUri(params: any): any {
    if (params) {
      return params.imageUri
    } else {
      return undefined
    }
  }

  getImageType(params: any): any {
    if (params) {
      return params.imageType
    } else {
      return undefined
    }
  }

  _handleDateTimePicked = (date: Date) => {
    this.member.setBirthDate(date)
    this._hideDateTimePicker()
  }

  setBirthDate(date: Date) {
    this.birthDate = date
  }

  formatDate(d: Date) {
    let month = "" + (d.getMonth() + 1)
    let day = "" + d.getDate()
    const year = d.getFullYear()

    if (month.length < 2) month = "0" + month
    if (day.length < 2) day = "0" + day

    return [year, month, day].join("-")
  }

  async _submit(imageUri: string, imageType: string) {
    if (this.state.isOver14) {
      this.addFamilyMember()
    } else {
      this.addChild(imageUri, imageType)
    }
  }

  // TODO: 옛 서비스와의 충돌을 막기 위해 프로필과 가족 이름 변경 시에 '_'로 시작하지 못하도록 하면 좋겠네요.
  async addFamilyMember() {
    let refinedPhone = String(this.member.phone).replace(/ /g, "")
    refinedPhone = String(this.member.phone).replace(/-/g, "")

    let refinedUserPhone = String(this.props.accountStore.user.phone)
    refinedUserPhone = refinedUserPhone.replace(/ /g, "").replace(/-/g, "")

    if (!this.props.accountStore.user.is_authenticated) {
      Alert.alert(
        "",
        `본인인증 이후에 14세 이상의 가족회원을 추가하실 수 있습니다.`,
        [
          { text: "닫기", onPress: () => this.setState({ submitDisabled: false }) },
          {
            text: "본인인증",
            onPress: () =>
              // this.props.navigation.navigate("CertificationDetail", {
              //   previousScreen: "AddFamilyMemberScreen",
              // }),
              this.props.navigation.navigate("MyPageStack", {
                screen: "CertificationDetail",
                params: { previousScreen: "AddFamilyMemberScreen" },
              }),
          },
        ],
        { cancelable: false },
      )
      return
    } else if (!this.member.phone || (this.member.phone && this.member.phone.length == 0)) {
      Alert.alert(
        "",
        `전화번호를 입력해 주세요.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    } else if (
      refinedPhone &&
      refinedPhone.length != 10 &&
      refinedPhone.length != 11 &&
      refinedPhone.length != 8
    ) {
      Alert.alert(
        "",
        `올바른 전화번호를 입력해 주세요.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    } else if (refinedPhone == refinedUserPhone) {
      Alert.alert(
        "",
        `해당 번호는 고객님의 계정으로 등록된 번호입니다. 다른 번호를 입력해 주세요. `,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    }

    const store = this.props.accountStore

    const result = await api.findFamilyMember(refinedPhone)
    let familyMemberId = -1

    if (result.kind === "ok") {
      if (result.data.count == 0) {
        // 해당 회원이 존재하지 않음 (14세 이상인 경우, 본인의 회원가입이 선행되어야 함)
        Alert.alert(
          "",
          `해당하는 회원을 찾지 못 하였습니다. \n14세 이상인 경우, 서비스에 가입된 사람만 추가할 수 있습니다.`,
          [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
          { cancelable: false },
        )
        return
      } else {
        familyMemberId = result.data.results[0].user
      }
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      console.tron.log("< AddFamilyMemberScreen > addFamilyMember, error")
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }

    const reqParams = new FormData()
    reqParams.append("user", this.props.accountStore.user.id)
    reqParams.append("family_user", familyMemberId)

    const result2 = await api.addFamilyMember(reqParams)
    if (result2.kind === "ok") {
      this.setState({ addFamilyOver14: true })
      this.props.accountStore.fetchFamilyToRequest()
    } else if (result2.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else if (
      result2.kind === "rejected" &&
      result2.data.non_field_errors &&
      String(result2.data.non_field_errors[0]).includes(
        "필드 user, family_user 는 반드시 고유(unique)해야 합니다.",
      )
    ) {
      Alert.alert(
        "",
        `이미 해당 전화번호로 추가 요청을 한 이력이 존재합니다.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
    } else {
      console.tron.log("< AddFamilyMemberScreen > addFamilyMember, error")
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.goBack(), 2000)
    }
  }

  async addChild(imageUri: string, imageType: string) {
    if (!this.member.name || this.member.name.trim().length == 0) {
      Alert.alert(
        "",
        `이름을 입력하세요.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    } else if (!(this.member.gender == "M" || this.member.gender == "F")) {
      Alert.alert(
        "",
        `성별을 입력하세요.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    } else if (this.member.name && this.props.accountStore.hasName(this.member.name.trim())) {
      Alert.alert(
        "",
        `해당 가족회원 이름은 현재 사용 중 입니다. \n다른 이름으로 등록해 주세요.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    } else if (this.member.birth_date == undefined) {
      Alert.alert(
        "",
        `생년월일을 입력하세요.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    } else if (this.member.birth_date && calcAge(this.member.birth_date) > 14) {
      Alert.alert(
        "",
        `생년월일을 다시 확인해주세요.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    } else if (this.member.birth_date && calcAge(this.member.birth_date) < 0) {
      Alert.alert(
        "",
        `생년월일을 다시 확인해주세요.`,
        [{ text: "확인", onPress: () => this.setState({ submitDisabled: false }) }],
        { cancelable: false },
      )
      return
    }

    const store = this.props.accountStore
    const now = new Date()
    const reqParams = new FormData()

    // reqParams.append('id', this.member.id);
    reqParams.append("name", this.member.name.trim())
    reqParams.append(
      "birth_date",
      this.member.birth_date != undefined ? this.formatDate(this.member.birth_date) : "",
    )
    reqParams.append("gender", this.member.gender)
    reqParams.append("user", this.props.accountStore.user.id)

    if (imageType) {
      const tempArray = String(imageUri).split("/")
      const fileName =
        Platform.OS === "android"
          ? tempArray[tempArray.length - 1] + ".jpg"
          : tempArray[tempArray.length - 1]
      reqParams.append("img", {
        uri: imageUri,
        type: imageType,
        name: fileName,
      })
    } else if (this.state.imageType) {
      reqParams.append("img", {
        uri: this.state.member.img,
        type: this.state.imageType,
        name: this.state.fileName
          ? this.state.fileName
          : this.props.accountStore.user.id + "_" + now.toISOString() + ".jpg",
      })
    }

    const result: FamilyMemberResult = await api.addChild(reqParams)

    if (result.kind === "ok") {
      this.member.setId(result.member.id)
      this.member.setProfileImage(result.member.img)
      store.addFamilyMember(
        FamilyMemberModel.create({
          id: result.member.id,
          name: this.member.name,
          birth_date: this.member.birth_date,
          gender: this.member.gender,
          img: result.member.img,
          familyMemberKey: `_c${result.member.id}`,
        }),
      )
      this.setState({ addFamilyUnder14: true })
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      console.tron.log("< AddFamilyMemberScreen > addChild, error")
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  cancel = () => {
    // TODO: 가족회원 정보에 대해 변경한 내용이 있으면 Alert 팝업창 띄우자
    this.goBack()
  }

  _handleProfileImage = () => {
    console.warn("Not implemented")
  }

  onGenderValueChange(value: "" | "M" | "F" | "male" | "female") {
    this.member.setGender(value)
  }

  onChange = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.date

    this.setState({ date: currentDate })
  }

  renderBottomSheet() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: color.palette.white,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <View style={{ marginTop: 30 }}>
          <TouchableHighlight
            style={{ ...styles.BOTTOM_SHEET_BUTTON, marginBottom: 5 }}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              setTimeout(() => {
                this.setState({ genderSelected: "M" })
                this.onGenderValueChange("M")
              }, 200)
            }}
          >
            <Text style={{ ...styles.BOTTOM_SHEET_BUTTON_TEXT, fontWeight: "bold" }}>남</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={{ ...styles.BOTTOM_SHEET_BUTTON, marginBottom: 5 }}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              setTimeout(() => {
                this.setState({ genderSelected: "F" })
                this.onGenderValueChange("F")
              }, 200)
            }}
          >
            <Text style={{ ...styles.BOTTOM_SHEET_BUTTON_TEXT, fontWeight: "bold" }}>여</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={PICKER_BTN}
            underlayColor={color.palette.grey2}
            onPress={() => this.bottomSheetRef.current.close()}
          >
            <Text style={PICKER_TEXT}>취소</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  render() {
    const { route } = this.props
    const imageUri = this.getImageUri(route.params)
    const imageType = this.getImageType(route.params)
    // 요청취소할 수신자 정보 표시용

    return (
      <View style={{ backgroundColor: color.palette.white }}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={{ flex: 1 }}>
            {this.renderRadioButtons()}
            {this.state.isOver14 ? (
              <View>
                <View style={{ paddingTop: 5 }}>
                  <Text style={MESSAGE}>동의를 얻을 가족의 핸드폰 번호를 입력해주세요.</Text>
                  <Stack style={[INPUT, { marginTop: 20, marginLeft: 0 }]}>
                    <Input
                      style={PHONE_INPUT}
                      selectionColor={color.primary}
                      placeholder='"-" 없이 핸드폰 번호 입력'
                      placeholderTextColor={color.placeholder}
                      // underlineColorAndroid={color.primary}
                      // value={this.member.phone}
                      dataDetectorTypes="phoneNumber"
                      textContentType="telephoneNumber"
                      keyboardType="number-pad"
                      onChangeText={(text) => this.member.setPhone(text)}
                    />
                  </Stack>
                </View>
                <View style={OVER14_GUIDE_BOX}>
                  <Text style={OVER14_GUIDE_TEXT}>
                    14세 이상 가족회원 추가는
                    <Text style={{ ...OVER14_GUIDE_TEXT, fontWeight: "bold" }}>
                      {" "}
                      앱을 통한 동의 요청 및 승인 과정을 거쳐야 합니다.
                    </Text>{" "}
                    동의를 얻을 가족회원님의 핸드폰에서 앱을 켜고 동의 요청을 승락하세요.
                  </Text>
                </View>
              </View>
            ) : (
              <FormControl>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("ChangeProfileImage", {
                      prevRouteName: this.props.route.name,
                      prevPrevRouteName: this.getPrevRouteName(route.params),
                      profileName: this.member.name,
                      profileGender: this.member.gender,
                      profileBirth: this.member.birth_date,
                    })
                  }
                  style={{
                    width: "100%",
                    marginTop: 30,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {/* <Thumbnail large source={this.member.img ? {uri: this.member.img} : cover} /> */}
                  <BackgroundImage
                    resizeMode={FastImage.resizeMode.cover}
                    image={{ width: 81, height: 81, borderRadius: 40.5 }}
                    source={
                      imageUri
                        ? { uri: imageUri }
                        : this.state.member.img
                        ? { uri: this.state.member.img }
                        : cover
                    }
                  />
                  <View
                    style={{
                      width: 81,
                      height: 81,
                      alignItems: "flex-end",
                      justifyContent: "flex-end",
                    }}
                  >
                    <Image
                      style={{ width: 27, height: 27 }}
                      source={require("../assets/images/profileChangeButton.png")}
                    />
                  </View>
                </TouchableOpacity>
                <View style={{ flex: 1, paddingLeft: 16, marginTop: 30 }}>
                  <View style={INPUTBOX}>
                    <Text style={STACKED_LABEL}>이름</Text>
                    <HStack style={[INPUT, { borderBottomWidth: 0, alignItems: "center" }]}>
                      <InputGroup
                        w={{
                          base: "90%",
                          // md: "285",
                        }}
                      >
                        <Input
                          variant="unstyled"
                          style={{ ...INPUT_STYLE, paddingRight: 0 }}
                          selectionColor={color.primary}
                          placeholder="이름입력"
                          placeholderTextColor={color.palette.grey50}
                          onChangeText={(text) => this.member.setName(text)}
                          value={this.member.name}
                        />
                        {/* <InputRightAddon children={<} /> */}
                        {/* <SvgArrowRight /> */}
                      </InputGroup>
                    </HStack>
                  </View>
                  <View style={INPUTBOX}>
                    <Text style={STACKED_LABEL}>성별</Text>
                    <TouchableOpacity
                      style={[INPUT, { borderBottomWidth: 0 }]}
                      onPress={Platform.OS === "ios" ? this._showGenderPicker : this.onItemPress}
                    >
                      <Text
                        style={{
                          ...PICKER_TEXT_STYLE,
                          color:
                            Platform.OS == "ios"
                              ? this.state.isGenderText
                                ? color.text
                                : color.palette.grey50
                              : this.state.genderSelected
                              ? color.text
                              : color.palette.grey50,
                        }}
                      >
                        {this.member.gender
                          ? this.member.gender === "M"
                            ? "남"
                            : "여"
                          : "성별입력"}
                      </Text>
                    </TouchableOpacity>
                    <SvgArrowRight />
                  </View>
                  <View style={[INPUTBOX, { borderBottomWidth: 0 }]}>
                    <Text style={STACKED_LABEL}>생년월일</Text>
                    <TouchableOpacity
                      style={[INPUT, { borderBottomWidth: 0, marginLeft: 0 }]}
                      onPress={this._showDateTimePicker}
                    >
                      <Text
                        style={{
                          ...PICKER_TEXT_STYLE,
                          color: this.member.birth_date ? color.text : color.palette.grey50,
                        }}
                      >
                        {this.member.birth_date
                          ? formatDateKor(this.member.birth_date)
                          : "생년월일 입력"}
                      </Text>
                    </TouchableOpacity>
                    <SvgArrowRight />
                  </View>
                </View>
                <View style={ROW}></View>
                {this.state.isOver14 ? (
                  <View style={{ paddingTop: 5 }}>
                    <Text
                      style={{
                        fontSize: 10,
                        letterSpacing: -0.2,
                        paddingLeft: 40,
                        paddingRight: 14,
                      }}
                    >
                      *14세 이상 가족의 처방전을 관리하기 위해서는 앱을 통한 동의 요청 및 승인
                      과정을 거쳐야 합니다. 동의를 얻을 가족이 앱을 다운받아 회원 가입을 하였는지
                      확인하세요.
                    </Text>
                    <Text style={MESSAGE}>동의를 얻을 가족의 핸드폰 번호를 입력해주세요.</Text>
                    <Stack rounded last style={[INPUT, { height: 40, borderRadius: 20 }]}>
                      <Input
                        style={PHONE_INPUT}
                        selectionColor={color.primary}
                        placeholder="핸드폰 번호"
                        placeholderTextColor={color.placeholder}
                        dataDetectorTypes="phoneNumber"
                        textContentType="telephoneNumber"
                        keyboardType="number-pad"
                        onChangeText={(text) => this.member.setPhone(text)}
                      />
                    </Stack>
                  </View>
                ) : (
                  <View></View>
                )}
              </FormControl>
            )}
          </View>
        </ScrollView>
        <Modal
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
          isVisible={this.state.isGenderPickerVisible}
          style={{
            justifyContent: "flex-end",
            marginTop: 0,
            marginBottom: 0,
            marginLeft: 0,
            marginRight: 0,
          }}
        >
          <SafeAreaInsetsContext.Consumer>
            {(insets) => (
              <View style={{ ...PICKER_CONTAINER, paddingBottom: insets.bottom + 16 }}>
                <Picker
                  style={{ height: 100, justifyContent: "center", marginBottom: 30 }}
                  selectedValue={this.state.genderSelected}
                  onValueChange={(itemValue) => this.setState({ genderSelected: itemValue })}
                  itemStyle={{ color: color.palette.lightBlack2, fontWeight: "bold" }}
                >
                  <Picker.Item label="남" value="M" />
                  <Picker.Item label="여" value="F" />
                </Picker>

                <TouchableOpacity
                  style={PICKER_BTN}
                  onPress={() => {
                    this.setState({
                      isGenderPickerVisible: false,
                      isGenderText: true,
                    })
                    this.onGenderValueChange(this.state.genderSelected)
                  }}
                >
                  <Text style={PICKER_TEXT}>확인</Text>
                </TouchableOpacity>
              </View>
            )}
          </SafeAreaInsetsContext.Consumer>
        </Modal>
        <Popup
          isVisible={this.state.addFamilyOver14}
          title={"가족회원 동의 요청을 보냈습니다."}
          message={`${this.member.phone}님께서 동의요청을 수락하면 회원님께서 해당 가족회원의 처방내역을 관리하실 수 있습니다.`}
          button1={getPopupButton(() => this.goBack(), "확인")}
        />
        <Popup
          isVisible={this.state.addFamilyUnder14}
          title={"가족회원을 추가하였습니다."}
          message={`${this.member.name}님을 추가하였습니다.`}
          button1={getPopupButton(() => this.goBack(), "확인")}
        />
        <View style={BOTTOM_BUTTONS_VIEW}>
          <Button
            style={BOTTOM_BUTTON}
            onPress={() => {
              this.setState({ submitDisabled: true })
              this._submit(imageUri, imageType)
            }}
            disabled={this.state.submitDisabled}
          >
            <Text style={BUTTON_TEXT}>완료</Text>
          </Button>
        </View>
        <DateTimePicker
          display="spinner"
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDateTimePicked}
          onCancel={this._hideDateTimePicker}
          confirmTextIOS="확인"
          customCancelButtonIOS={() => <View />}
          customConfirmButtonIOS={(buttonProps) => (
            <View style={{ backgroundColor: color.primary, borderRadius: 45 }}>
              <ConfirmButton {...buttonProps} style={customConfirmButtonStyles} />
            </View>
          )}
          customHeaderIOS={() => <View />}
          pickerContainerStyleIOS={DATETIMEPICKER_CONTAINER}
        />
        <RBSheet
          ref={this.bottomSheetRef}
          height={styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * 3 + 14}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            container: {
              backgroundColor: "transparent",
            },
          }}
        >
          <StatusBar backgroundColor={color.dimmedStatusBar} />
          {this.renderBottomSheet()}
        </RBSheet>
      </View>
    )
  }

  renderRadioButtons() {
    const radioButtons = [
      {
        id: 0,
        label: "14세 미만",
      },
      {
        id: 1,
        label: "14세 이상",
      },
    ]
    return (
      <>
        <View style={{ flex: 1, flexDirection: "row" }}>
          {radioButtons.map((button, index) => {
            const idChecked = button.id == this.state.radioSelected
            return (
              <View
                key={index}
                style={{
                  flex: 1,
                  paddingBottom: 10,
                  borderBottomWidth: idChecked ? 2 : 0,
                  borderBottomColor: color.primary,
                }}
              >
                <TouchableOpacity
                  key={button.id}
                  onPress={() => this.onRadioButtonClick(button.id)}
                  style={{
                    flex: 1,
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 17,
                      fontWeight: idChecked ? "600" : "400",
                      color: idChecked ? color.primary : color.palette.gray3,
                    }}
                  >
                    {button.label}
                  </Text>
                </TouchableOpacity>
              </View>
            )
          })}
        </View>
        <View style={{ flex: 1, height: 1, backgroundColor: "rgb(206, 206, 206)" }}></View>
      </>
    )
  }

  onRadioButtonClick(id: number) {
    if (id == 0) {
      this.setState({
        isOver14: false,
        radioSelected: id,
      })
    } else {
      this.setState({
        isOver14: true,
        radioSelected: id,
      })
      firebase.analytics().logEvent("over_14_radio_click")
      amplitude.getInstance().logEvent("over_14_radio_click")
    }
  }

  onCheckboxValueChange() {
    this.setState({
      isOver14: !this.state.isOver14,
    })
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  _showGenderPicker = () =>
    this.setState({ isGenderPickerVisible: !this.state.isGenderPickerVisible })

  rotateProfileImage(): any {
    let imageUrls = this.props.accountStore.user.profile_image + ""
    this.props.accountStore.family.map((member: FamilyMember) => {
      imageUrls = imageUrls + member.img
    })

    for (let i = 11; i < 16; i++) {
      const tempArray = avatarSources[i].imageUrl.split("/")
      const imageFileName = tempArray[tempArray.length - 1]
      if (!imageUrls.includes(imageFileName)) {
        return { imageUrl: avatarSources[i].imageUrl, fileName: imageFileName }
      }
    }

    const index = randomRange(10, 15)
    const tempArray = avatarSources[index].imageUrl.split("/")
    const imageFileName = tempArray[tempArray.length - 1]
    return { imageUrl: avatarSources[index].imageUrl, fileName: imageFileName }
  }

  onItemPress = () => {
    this.bottomSheetRef.current.open()
  }
}
