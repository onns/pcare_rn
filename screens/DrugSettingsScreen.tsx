import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { observer } from "mobx-react-lite"
import React, { useRef, useState } from "react"
import {
  Animated,
  Button,
  Platform,
  ScrollView,
  StatusBar,
  TextInput,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Calendar, LocaleConfig } from "react-native-calendars"
import Svg, { Path } from "react-native-svg"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { withNavigationFocus } from "@react-navigation/compat"
import { CompositeNavigationProp, RouteProp, useFocusEffect } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { HomeStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { MedicationSchedule } from "../stores/drug-to-take/MedicationSchedule"
import { FamilyMember } from "../stores/FamilyMember"
import { color, typography } from "../theme"

type DrugSettingsScreenRouteProp = RouteProp<HomeStackParamList, "DrugSettings">

type DrugSettingsScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "DrugSettings">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface DrugSettingsScreenProps {
  navigation: DrugSettingsScreenNavigationProp
  route: DrugSettingsScreenRouteProp
}

const ROOT: ViewStyle = {
  backgroundColor: color.background,
  paddingBottom: 70,
}
const CONTENT: ViewStyle = {
  paddingBottom: 30,
  paddingHorizontal: 7,
  marginBottom: 44,
  backgroundColor: color.palette.white,
}
const CALENDAR_THEME = {
  "stylesheet.calendar.header": {
    dayHeader: {
      marginTop: 2,
      marginBottom: 7,
      width: 32,
      textAlign: "center",
      fontSize: 13,
      fontFamily: typography.primary,
      color: "#33302f80",
    },
    monthText: {
      fontSize: 20,
      fontFamily: Platform.select({
        ios: "NotoSansKR-Bold",
        android: "NotoSansKR-Bold-Hestia",
      }),
      color: "#272727",
      margin: 10,
    },
  },
}

LocaleConfig.locales.kr = {
  monthNames: [
    "1월",
    "2월",
    "3월",
    "4월",
    "5월",
    "6월",
    "7월",
    "8월",
    "9월",
    "10월",
    "11월",
    "12월",
  ],
  monthNamesShort: [
    "1월",
    "2월",
    "3월",
    "4월",
    "5월",
    "6월",
    "7월",
    "8월",
    "9월",
    "10월",
    "11월",
    "12월",
  ],
  dayNames: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
  dayNamesShort: ["일", "월", "화", "수", "목", "금", "토"],
  today: "오늘",
}
LocaleConfig.defaultLocale = "kr"

const PROPERTY_ROW_NO_BOTTOM_LINE: TextStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  paddingTop: 16,
  paddingBottom: 8,
}
const PROPERTY_ROW: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  paddingVertical: 16,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
}
const PROPERTY_EXTENSIBLE: ViewStyle = {
  paddingVertical: 16,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
}
const PROPERTY_NAME: TextStyle = { ...typography.body }
const PROPERTY_VALUE: TextStyle = { ...typography.body, marginRight: 4 }
const PROPERTY_VALUE_DISABLED: TextStyle = { ...typography.body, opacity: 0.5 }
const DISEASE_BOX: ViewStyle = {
  paddingVertical: 24,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
}
const DISEASE = { ...typography.body, marginBottom: 8 }
const DISEASE_NAME = { ...typography.title2 }
const SECTION1 = {
  backgroundColor: color.palette.white,
  paddingHorizontal: 16,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
  paddingBottom: 8,
  marginBottom: 8,
}
const SECTION2 = {
  backgroundColor: color.palette.white,
  borderTopColor: color.palette.pale,
  borderTopWidth: 1,
  // borderBottomColor: color.palette.pale,
  // borderBottomWidth: 1,
  // marginBottom: 8,
  paddingHorizontal: 16,
  paddingBottom: 8,
  // marginBottom: 24,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
}

const ALARM_BOX: ViewStyle = {
  backgroundColor: color.palette.gray5,
  borderRadius: 1,
  borderWidth: 1,
  borderColor: color.palette.pale,
  paddingHorizontal: 7,
  paddingTop: 2,
  paddingBottom: 3,
  marginLeft: 8,
  marginBottom: 8,
}
const ALARM_TIME: TextStyle = { ...typography.sub2, opacity: 0.7 }
const DRUG_NAME_ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingLeft: 16,
  paddingTop: 16,
  paddingRight: 8,
  marginBottom: 4,
}
const DRUG_LINE = {
  backgroundColor: color.palette.pale,
  height: 1,
  marginTop: 8,
  marginLeft: 16,
  marginRight: 8,
}
const BOTTOM_LINE = { borderBottomWidth: 1, borderBottomColor: color.palette.pale }
const PROPERTY_DESC: ViewStyle = { ...typography.sub2, opacity: 0.5, paddingBottom: 16 }
const ROW: ViewStyle = { flexDirection: "row" }
const ROW_SPACE_BETWEEN: ViewStyle = { flexDirection: "row", justifyContent: "space-between" }

const DrugSettingsScreen: React.FunctionComponent<DrugSettingsScreenProps> = observer((props) => {
  const { accountStore, takingDrugStore } = useStores()
  const { route } = props
  const [viewedCalendarDate, setViewedCalendarDate] = useState(DateTime.local())
  const [prevMonthAvailable, setPrevMonthAvailable] = useState(true)
  const [nextMonthAvailable, setNextMonthAvailable] = useState(true)
  const [stopMedicationPopupVisible, setStopMedicationPopupVisible] = useState(false)
  const [doseVisible, setDoseVisible] = useState(false)
  const [numMedicationVisible, setNumMedicationVisible] = useState(false)
  const [cycleMedicationVisible, setCycleMedicationVisible] = useState(false)
  // const [arrowAngle1, setArrowAngle1] = useState(new Animated.Value(0))

  // fadeAnim will be used as the value for opacity. Initial Value: 0
  const fadeAnim = useRef(new Animated.Value(0)).current

  const fadeIn = () => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 2000,
    }).start()
  }

  const fadeOut = () => {
    // Will change fadeAnim value to 0 in 5 seconds
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 2000,
    }).start()
  }

  let minStartDate: DateTime = DateTime.local()
  let maxEndDate: DateTime = DateTime.local()
  const arrowAngle1 = new Animated.Value(0)
  // let AnimatedPath = Animated.createAnimatedComponent(Path)
  const rotation = arrowAngle1.interpolate({
    inputRange: [0, 180],
    outputRange: ["0deg", "180deg"],
  })

  // set the animation
  // Animated.timing(arrowAngle1, {
  //   toValue: 240, // degrees
  //   duration: 1000,
  //   useNativeDriver: true,
  // }).start()

  useFocusEffect(
    React.useCallback(() => {
      firebase.analytics().logScreenView({
        screen_class: route.name,
        screen_name: route.name,
      })
      amplitude.getInstance().logEvent("screen_view", { screen: route.name })
      Animated.timing(arrowAngle1, { toValue: 1, duration: 2000, easing: () => null }).start()
    }, []),
  )

  React.useEffect(() => {
    const { selectedMemberKey } = accountStore
    if (!selectedMemberKey) return
    if (selectedMemberKey.includes("_u")) {
      takingDrugStore.fetchMedicationSchedule(accountStore.user.id, selectedMemberKey)
    } else if (selectedMemberKey.includes("_c")) {
      takingDrugStore.fetchMedicationSchedule(
        accountStore.user.id,
        selectedMemberKey,
        Number(selectedMemberKey.substr(2)),
      )
    } else {
      takingDrugStore.fetchMedicationSchedule(
        Number(selectedMemberKey.substr(2)),
        selectedMemberKey,
      )
    }
  }, [accountStore.selectedMemberKey])

  const onSelectMember = (memberKey: string, member?: FamilyMember) => {}

  const generatePeriods = (date: DateTime, schedules: MedicationSchedule[]) => {
    const arr = []

    schedules.forEach((schedule) => {
      if (date.equals(schedule.start)) {
        arr.push({
          color: color.palette.greenishTeal,
          startingDay: true,
          diseaseName: schedule.disease.rep_title,
        })
      } else if (schedule.start < date && date < schedule.end) {
        arr.push({ color: color.palette.greenishTeal, diseaseName: schedule.disease.rep_title })
      } else if (date.equals(schedule.end)) {
        arr.push({
          color: color.palette.greenishTeal,
          endingDay: true,
          diseaseName: schedule.disease.rep_title,
        })
      }
    })
    return arr
  }

  const getMarkedDates = (schedules: MedicationSchedule[]) => {
    if (schedules.length == 0) {
      return null
    }

    let arr = []
    // 가장 빠른 시작일 구하기
    for (let i = 0; i < schedules.length; i++) {
      const schedule = schedules[i]
      if (i == 0) {
        minStartDate = schedule.start
        continue
      }
      if (minStartDate > schedule.start) {
        minStartDate = schedule.start
      }
    }

    // 가장 늦는 마감일 구하기
    for (let i = 0; i < schedules.length; i++) {
      const schedule = schedules[i]
      if (i == 0) {
        maxEndDate = schedule.end
        continue
      }
      if (maxEndDate < schedule.end) {
        maxEndDate = schedule.end
      }
    }

    // 빠른 시작일 순서로 스케줄 정렬
    const sortedSchedules = schedules.slice()
    sortedSchedules.sort((a, b) => {
      // 0보다 작은 경우 a를 b보다 낮은 색인으로 정렬 (a가 먼저옴)
      if (a.start < b.start) {
        return -1
      } else if (a.start.equals(b.start)) {
        return 0
      } else {
        return 1
      }
    })

    for (let date = minStartDate; date <= maxEndDate; ) {
      arr.push({
        [date.toSQLDate()]: {
          periods: generatePeriods(date, sortedSchedules),
        },
      })
      date = date.plus({ days: 1 })
    }

    // slicing curly brackets
    arr = arr.map((a) => {
      return JSON.stringify(a).slice(1, -1)
    })

    const arrString = arr.join(",")
    return JSON.parse("{" + arrString + "}")
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
      <Animated.View
        style={{
          flex: 1,
          backgroundColor: color.primary,
          opacity: fadeAnim, // Bind opacity to animated value
        }}
      >
        <Text>Fading View!</Text>
      </Animated.View>
      <View style={ROW}>
        <Button title="Fade In" onPress={fadeIn} />
        <Button title="Fade Out" onPress={fadeOut} />
      </View>
      <View style={SECTION1}>
        <View style={DISEASE_BOX}>
          <Text style={DISEASE}>약제명</Text>
          <Text style={DISEASE_NAME}>코데날정</Text>
        </View>
        <View style={BOTTOM_LINE}>
          <View style={PROPERTY_ROW_NO_BOTTOM_LINE}>
            <Text style={PROPERTY_NAME}>복약 시작일</Text>
            <View style={ROW}>
              <Text style={PROPERTY_VALUE}>2020.03.20</Text>
              <SvgArrowRight width={24} height={24} />
            </View>
          </View>
          <Text style={PROPERTY_DESC}>복약 시작일을 변경하실 경우 복약 기록이 초기화됩니다.</Text>
        </View>
        <View style={PROPERTY_ROW_NO_BOTTOM_LINE}>
          <Text style={PROPERTY_NAME}>종료일</Text>
          <View style={ROW}>
            <Text style={PROPERTY_VALUE_DISABLED}>2020년 3월 15일</Text>
          </View>
        </View>
      </View>
      {/* <View style={{ height: 8, backgroundColor: color.background }} /> */}
      <View style={SECTION2}>
        <View style={PROPERTY_EXTENSIBLE}>
          <View style={ROW_SPACE_BETWEEN}>
            <Text style={PROPERTY_NAME}>1회 복약량</Text>
            <TouchableOpacity style={ROW} onPress={() => setDoseVisible(!doseVisible)}>
              <Text style={PROPERTY_VALUE}>1정</Text>
              <Animated.View
                style={{
                  // opacity: fadeAnim, // Bind opacity to animated value
                  transform: [
                    // { scale: this.state.scale },
                    { rotateY: rotateY },
                    { perspective: 1000 }, // without this line this Animation will not render on Android while working fine on iOS
                  ],
                }}
              >
                <SvgArrowRight width={24} height={24} />
              </Animated.View>

              {/* <Animated.View style={{ transform: [{ rotate: concat(rotation, "deg") }] }}>
                <SvgArrowRight width={24} height={24} />
              </Animated.View> */}
            </TouchableOpacity>
          </View>
          {doseVisible ? (
            <View style={{ marginTop: 24 }}>
              <Text style={{ ...typography.sub2 }}>한번에 얼마나 복약하나요?</Text>
              <View style={{ flexDirection: "row", alignItems: "center", marginTop: 8 }}>
                <TextInput
                  defaultValue="1"
                  // value={"3"}
                  style={{
                    width: 164,
                    height: 48,
                    backgroundColor: color.palette.veryLightPink3,
                    textAlign: "center",
                    fontFamily: typography.bold,
                    fontSize: 16,
                    lineHeight: 19,
                    color: color.text,
                  }}
                  keyboardType="numeric"
                />
                <Text style={{ marginLeft: 12, fontSize: 16, color: "#000000" }}>캡슐</Text>
              </View>
            </View>
          ) : null}
        </View>
        <View style={PROPERTY_ROW}>
          <Text style={PROPERTY_NAME}>1일 복약횟수</Text>
          <View style={ROW}>
            <Text style={PROPERTY_VALUE}>하루 2번 (아침, 저녁)</Text>
            <SvgArrowRight width={24} height={24} />
          </View>
        </View>
        <View style={PROPERTY_ROW}>
          <Text style={PROPERTY_NAME}>복약주기</Text>
          <View style={ROW}>
            <Text style={PROPERTY_VALUE}>매일</Text>
            <SvgArrowRight width={24} height={24} />
          </View>
        </View>
        <View style={PROPERTY_ROW_NO_BOTTOM_LINE}>
          <Text style={PROPERTY_NAME}>총 복약일수</Text>
          <View style={ROW}>
            <Text style={PROPERTY_VALUE}>30일</Text>
            <SvgArrowRight width={24} height={24} />
          </View>
        </View>
      </View>

      <View style={{ alignItems: "flex-end", marginRight: 16, marginTop: 24 }}>
        <TouchableOpacity onPress={() => setStopMedicationPopupVisible(true)}>
          <Text style={{ ...typography.body, color: color.primary }}>복약 중단</Text>
        </TouchableOpacity>
      </View>
      <Popup
        isVisible={stopMedicationPopupVisible}
        title={"복약을 중단하시겠습니까?"}
        message={`복약을 중단하면\n더이상 복약 알림을 받으실 수 없습니다.`}
        button1={getPopupButton(() => null, "네, 중단할게요")}
        button2={getPopupButton(() => setStopMedicationPopupVisible(false), "취소", "cancel")}
      />
    </Screen>
  )
})

export default withNavigationFocus(DrugSettingsScreen)
