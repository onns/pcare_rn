import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content, Icon, ScrollView, View } from "native-base"
import React from "react"
import { Platform, TouchableOpacity, ViewStyle } from "react-native"
import DeviceInfo from "react-native-device-info"
import email from "react-native-email"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgArrowRight from "../assets/images/arrowRight.svg"
import { Text } from "../components/StyledText"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { color } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  // flex: 1, 스크롤 가능하게 주석 처리함.
  backgroundColor: "rgb(239, 239, 239)",
  padding: 20,
}

const SETTING_ITEM: ViewStyle = {
  height: 42,
  flexDirection: "row",
  alignItems: "center",
  backgroundColor: "#fff",
  borderRadius: 20,
  paddingLeft: 26.7,
  paddingRight: 21.5,
  marginBottom: 6,
}

const ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}

type HelpCenterRouteProp = RouteProp<MyPageStackParamList, "HelpCenter">

type HelpCenterNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "HelpCenter">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: HelpCenterNavigationProp
  route: HelpCenterRouteProp
}

@inject("accountStore")
@observer
export default class HelpCenterScreen extends React.Component<Props> {
  static navigationOptions = {
    title: "고객센터",
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  handleEmail = () => {
    const user = this.props.accountStore.user
    const to = ["onions@onns.co.kr"] // string or array of email addresses

    let body = "이곳에 내용을 입력해 주세요.\n\n\n\n\n"
    body =
      body +
      "*오류의 경우, 보다 정확한 내용 확인을 위해서 오류 화면의 스크린샷을 첨부해 주세요.\n\n파프리카케어를 사용해 주셔서 감사합니다.\n\n\n\n"
    body = body + `파프리카케어 ID: ${user.id}, 앱 버전: ${packageJson.version}, `
    body = body + `플랫폼 OS: ${Platform.OS} v${Platform.Version}`
    body = body + `디바이스 정보: ${DeviceInfo.getModel()}`

    email(to, {
      subject: "사용자 의견",
      body: body,
    }).catch(console.error)
  }

  render() {
    return (
      <View style={{ backgroundColor: "rgb(239, 239, 239)" }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <TouchableOpacity
            style={SETTING_ITEM}
            onPress={() => this.props.navigation.navigate("MedicalInfoSource")}
          >
            <View style={ROW}>
              <Text style={{ fontSize: 15 }}>의료정보 출처</Text>
              <View style={{ flex: 1, justifyContent: "center", alignItems: "flex-end" }}>
                <SvgArrowRight width={24} height={24} color={color.primary} />
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={SETTING_ITEM}
            onPress={this.handleEmail}
            // onPress={() => Linking.openURL('http://www.papricacare.com/contact.html')}
          >
            <View style={ROW}>
              <Text style={{ fontSize: 15 }}>사용자 의견 보내기</Text>
              <View style={{ flex: 1, justifyContent: "center", alignItems: "flex-end" }}>
                <SvgArrowRight width={24} height={24} color={color.primary} />
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={SETTING_ITEM}
            onPress={() => this.props.navigation.navigate("DeleteAccount")}
          >
            <View style={ROW}>
              <Text style={{ fontSize: 15 }}>회원 탈퇴</Text>
              <View style={{ flex: 1, justifyContent: "center", alignItems: "flex-end" }}>
                <SvgArrowRight width={24} height={24} color={color.primary} />
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    )
  }
}
