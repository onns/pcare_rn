import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import React, { Component } from "react"
import { WebView } from "react-native-webview"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import {
  AuthStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { color } from "../theme"

type PrivacyPolicyRouteProp = RouteProp<AuthStackParamList, "PrivacyPolicy">

type PrivacyPolicyNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "PrivacyPolicy">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<MyPageStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  navigation: PrivacyPolicyNavigationProp
  route: PrivacyPolicyRouteProp
}

export default class PrivacyPolicyScreen extends Component<Props> {
  static navigationOptions = {
    title: "",
    headerStyle: {
      borderBottomWidth: 0,
      elevation: 0,
      shadowColor: "transparent",
    },
  }

  render() {
    return (
      <WebView
        ref="webview"
        androidLayerType={"hardware"}
        onMessage={(event) => {
          const message = event.nativeEvent.data
          /* event.nativeEvent.data must be string, i.e. window.postMessage
             should send only string.
          */
          // this.props.accountStore.user.setAddress(message)
          this.props.navigation.goBack()
        }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        source={{ uri: "https://web.papricacare.com/agrees/privacy-policy-mobile.html" }}
        containerStyle={{ margin: 0, padding: 0 }}
        style={{ backgroundColor: color.background }}
      />
    )
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  onMessage(data: any) {
    console.tron.log(data)
  }
}
