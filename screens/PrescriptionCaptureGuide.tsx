import React, { useState } from "react"
import { Image, ImageStyle, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Circle, XStack, YStack } from "tamagui"

import SvgGetPoint from "@assets/images/get-point.svg"
import { SvgArrowRight } from "@components/svg/SvgArrowRight"
import remoteConfig from "@react-native-firebase/remote-config"
import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgCheckboxChecked from "../assets/images/checkboxChecked.svg"
import SvgCheckboxDisabled from "../assets/images/checkboxDisabled.svg"
import SvgLock from "../assets/images/lock.svg"
import TouchableItem from "../components/button/TouchableItem"
import { Text } from "../components/StyledText"
import { saveString } from "../lib/storage"
import { DetailsStackParamList } from "../navigation/types"
import { color, typography } from "../theme"

type PrescriptionCaptureGuideScreenRouteProp = RouteProp<
  DetailsStackParamList,
  "PrescriptionCaptureGuide"
>

type PrescriptionCaptureGuideScreenNavigationProp = StackNavigationProp<
  DetailsStackParamList,
  "PrescriptionCaptureGuide"
>

export interface PrescriptionCaptureGuideScreenProps {
  navigation: PrescriptionCaptureGuideScreenNavigationProp
  route: PrescriptionCaptureGuideScreenRouteProp
}

const CAPTURE_GUIDE_BOX: ViewStyle = {
  flex: 1,
  justifyContent: "space-evenly",
  backgroundColor: color.palette.gray1,
  paddingHorizontal: 44,
}

const CAPTURE_GUIDE_NUMBER: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 17,
  lineHeight: 25,
  color: color.palette.white,
}
const CAPTURE_GUIDE_MESSAGE: TextStyle = {
  ...typography.body,
  color: color.palette.white,
  marginLeft: 8,
}
const CAPTURE_GUIDE_MESSAGE_SMALL: TextStyle = {
  ...typography.sub2,
  color: color.palette.white,
}
const CAPTURE_GUIDE_CONFIRM_BUTTON: ViewStyle = {
  height: 42,
  borderRadius: 20,
  borderWidth: 1,
  borderColor: color.palette.white,
  justifyContent: "center",
  alignItems: "center",
}
const CAPTURE_GUIDE_CONFIRM_TEXT = {
  ...typography.body,
  color: color.palette.white,
}
const CAPTURE_GUIDE_CHECKBOX_ROW: ViewStyle = {
  flexDirection: "row",
  marginBottom: 40,
  justifyContent: "flex-end",
  alignItems: "center",
}
const hitSlop10 = {
  top: 10,
  left: 10,
  bottom: 10,
  right: 10,
}
const CAPTURE_GUIDE_IMAGE: ImageStyle = {
  width: 302,
  height: 190,
  resizeMode: "contain",
  marginBottom: 32,
  alignSelf: "center",
}
const CHECKBOX: ImageStyle = {
  marginRight: 8,
}
export function PrescriptionCaptureGuideScreen(props) {
  const { navigation } = props
  const [doNotDisplayCaptureGuide, setDoNotDisplayCaptureGuide] = useState(false)

  const closeCaptureGuide = async () => {
    await saveString("doNotDisplayCaptureGuide", JSON.stringify(doNotDisplayCaptureGuide))
    navigation.navigate("Details", { screen: "PrescriptionScanner" })
  }

  return (
    <View style={CAPTURE_GUIDE_BOX}>
      <View>
        <Image source={require("../assets/images/captureGuide.png")} style={CAPTURE_GUIDE_IMAGE} />
        <View style={{ flexDirection: "row", marginBottom: 8 }}>
          <Text style={CAPTURE_GUIDE_NUMBER} adjustsFontSizeToFit={false}>
            1.
          </Text>
          <Text style={CAPTURE_GUIDE_MESSAGE} adjustsFontSizeToFit={false}>
            처방전 또는 약봉투의 전체가 보이도록 찍어주세요.
          </Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: 16 }}>
          <Text style={CAPTURE_GUIDE_NUMBER} adjustsFontSizeToFit={false}>
            2.
          </Text>
          <Text style={CAPTURE_GUIDE_MESSAGE} adjustsFontSizeToFit={false}>
            한번에 한장만 찍어 주세요.
          </Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: 16 }}>
          <SvgLock width={16} height={16} style={{ marginRight: 4, paddingTop: 4 }} />
          <YStack>
            <Text fow="400" fos={15} lh={18} ls={-0.3} col="white" mb={8}>
              안심하고 찍으세요!
            </Text>
            <Text fow="400" fos={14} lh={20} ls={-0.3} col="rgba(255, 255, 255, 0.7)">
              개인정보는 자동으로 삭제되며 저장하지 않습니다. 안심하고 사용하세요.
            </Text>
          </YStack>
        </View>
        {remoteConfig().getBoolean("points_enabled") && (
          <XStack
            w="100%"
            h={32}
            my={16}
            ai="center"
            onPress={() => {
              navigation.navigate("PointGuide")
            }}
          >
            <XStack f={1} ai="center">
              <Circle size={32}>
                <SvgGetPoint width={24} height={24} />
              </Circle>
              <Text style={CAPTURE_GUIDE_MESSAGE}>처방전 등록 포인트 혜택 안내</Text>
            </XStack>
            <SvgArrowRight />
          </XStack>
        )}
        <View style={CAPTURE_GUIDE_CHECKBOX_ROW}>
          <TouchableOpacity
            onPress={() => {
              setDoNotDisplayCaptureGuide(!doNotDisplayCaptureGuide)
            }}
            hitSlop={hitSlop10}
          >
            {doNotDisplayCaptureGuide ? (
              <SvgCheckboxChecked width={22} height={22} style={CHECKBOX} />
            ) : (
              <SvgCheckboxDisabled width={22} height={22} style={CHECKBOX} />
            )}
          </TouchableOpacity>
          <Text style={CAPTURE_GUIDE_MESSAGE_SMALL} adjustsFontSizeToFit={false}>
            다시 보지 않기
          </Text>
        </View>
        <TouchableItem
          style={CAPTURE_GUIDE_CONFIRM_BUTTON}
          onPress={() => {
            closeCaptureGuide()
          }}
        >
          <Text style={CAPTURE_GUIDE_CONFIRM_TEXT} adjustsFontSizeToFit={false}>
            {remoteConfig().getBoolean("points_enabled") ? "처방전 촬영 등록 50원" : "확인"}
          </Text>
        </TouchableItem>
      </View>
    </View>
  )
}
