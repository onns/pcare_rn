import "@react-native-firebase/analytics"
import "@react-native-firebase/remote-config"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { Image, Linking, Platform, View } from "react-native"
import appsFlyer from "react-native-appsflyer"
// import RNBootSplash from "react-native-bootsplash"
import DeviceInfo from "react-native-device-info"
import VersionCheck from "react-native-version-check"

import { api } from "@api/api"
import { loadString, saveString } from "@lib/storage"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { getPopupButton, Popup } from "../components/Popup"
import { formatDateWithJoinChar } from "../lib/DateUtil"
import { AuthStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { CampaignStore } from "../stores/campaign/CampaignStore"
import { TakingDrugStore } from "../stores/drug-to-take/TakingDrugStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"

type AuthLoadingScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<RootStackParamList, "AuthLoading">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AuthStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

type Props = {
  navigation: AuthLoadingScreenNavigationProp
  accountStore: AccountStore
  campaignStore: CampaignStore
  prescriptionStore: PrescriptionStore
  takingDrugStore: TakingDrugStore
}

interface State {
  needAppUpdate: boolean
  serverMaintenance: boolean
  serverMaintenanceImage: string
}

@inject("accountStore", "campaignStore", "prescriptionStore", "takingDrugStore")
@observer
export default class AuthLoadingScreen extends Component<Props, State> {
  pinCodeEnabled: undefined | string

  constructor(props: Props) {
    super(props)
    this._bootstrapAsync()
    this.state = {
      needAppUpdate: false,
      serverMaintenance: false,
      serverMaintenanceImage: "",
    }
  }

  async componentDidMount() {
    await appsFlyer.initSdk(
      {
        devKey: "nkZNVSwvKU9etrdqi72aiH",
        isDebug: true,
        appId: "1453787406", // IOS id
        // true로 되어 있을 경우 최초 링크에는 문제가 발생하지 않음
        // 하지만 2번째 이후에는 링크로 설정된 페이지로 자동 이동함(DDL이 자동 실행됨, 링크를 클릭하지 않았음에도)
        onInstallConversionDataListener: false,
        onDeepLinkListener: true,
        // timeToWaitForATTUserAuthorization: 10, // for iOS 14.5 ATT 구현후 주석해제 예정
      },
      (result) => {
        console.log("< Home > componentDidMount initSdk result : ", result)
        console.tron.log("< Home > componentDidMount initSdk result : ", result)
      },
      (error) => {
        console.error("< Home > componentDidMount initSdk error : ", error)
        console.tron.error("< Home > componentDidMount initSdk error : ", error)
      },
    )
  }

  fetchRemoteConfig = async (valueToFetch): Promise<string> => {
    try {
      // await firebase.remoteConfig().fetch(1) // ms 단위로 로컬에 캐시로 저장. 즉 1초가 지나지 않으면 데이터 fetch하지 않음.
      // const fetchedRemotely = await firebase.remoteConfig().activate() // ms 단위로 로컬에 캐시로 저장. 즉 1초가 지나지 않으면 데이터 fetch하지 않음.
      const fetchedRemotely = await firebase.remoteConfig().fetchAndActivate() // fetch로 가져온 값을 활성화

      if (fetchedRemotely) {
        console.tron.log("New configs were retrieved from the backend and activated.")
      } else {
        console.tron.log(
          "No new configs were fetched from the backend, and the local configs were already activated",
        )
      }

      const snapshot = await firebase.remoteConfig().getValue(valueToFetch)
      const response = snapshot.asString()

      // 서버 점검 중일 경우 서버 점검 이미지를 가져 옴
      if (response === "stop") {
        const image = await firebase.remoteConfig().getValue("server_maintenance_image")
        this.setState({ serverMaintenance: true, serverMaintenanceImage: image.asString() })
      }

      return response
    } catch (error) {
      console.tron.log(
        "< AuthLoadingScreen > fetchRemoteConfig, Firebase Remote Config error: ",
        error,
      )
      return null
    }
  }

  _bootstrapAsync = async () => {
    // 가져오고자 하는 값의 초기값을 선언해주어야 함
    await firebase.remoteConfig().setDefaults({
      server_status: "", // 서버 가동중 : running, 서버 점검중: stop
      server_maintenance_image: "", // 서버 점검 중 이미지
      search_bar_on_home_enabled: false,
      v4_api_url: "https://dev.papricacare.com/v4/api-www",
    })
    await firebase.remoteConfig().setConfigSettings({
      // eslint-disable-next-line no-undef
      // isDeveloperModeEnabled: __DEV__,
      minimumFetchIntervalMillis: 30000,
    })
    const serverStatus = await this.fetchRemoteConfig("server_status")
    console.tron.logImportant(
      "< AuthLoadingScreen > _bootstrapAsync, Firebase Remote Config server_status: ",
      serverStatus,
    )

    // 데이터를 패치하기 전에 서버점검 중인지 먼저 확인 후 실행
    if (serverStatus !== "stop") {
      const { accountStore, campaignStore, prescriptionStore, takingDrugStore } = this.props
      /** 개발자 모드 체크 */
      await this.fetchDeveloperMode()

      campaignStore.fetchRanking("drug")
      campaignStore.fetchRanking("disease")
      campaignStore.fetchCampaigns()

      const holdFamily = await AsyncStorage.getItem("holdFamily")
      holdFamily ?? (await AsyncStorage.removeItem("holdFamily"))
      const userToken = await AsyncStorage.getItem("userToken")
      const signPath = await AsyncStorage.getItem("signPath")

      console.tron.log(`< AuthLoadingScreen > _bootstrapAsync, accessToken: ${userToken}`)
      if (signPath) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        accountStore.setSignPath(String(signPath))
      }

      if (userToken) {
        const userId = await AsyncStorage.getItem("userId")

        if (userId) {
          api.apisauce.setHeader("Authorization", `Token ${userToken}`)
          prescriptionStore.fetchTimeSlots()

          const numberUserId = Number(userId)
          takingDrugStore.fetchDrugsToTake(numberUserId, `_u${numberUserId}`)

          const result = await api.getMyInfo(numberUserId)

          if (result.kind === "ok") {
            amplitude.getInstance().setUserId(userId)
            firebase.analytics().setUserId(userId)
            // setId 가 setUser 앞에 있어야 함.
            accountStore.user.setId(numberUserId)
            accountStore.setUser(result.user)
            try {
              const currentAppVersion = await VersionCheck.getCurrentVersion()
              // const deviceAppVersion = await DeviceInfo.getVersion()
              const deviceAppVersion = await DeviceInfo.getVersion()
              console.tron.log("deviceAppVersion:", deviceAppVersion)
              console.tron.log("currentAppVersion:", currentAppVersion)
              if (currentAppVersion !== deviceAppVersion) {
                setTimeout(() => {
                  this.setState({
                    needAppUpdate: true,
                  })
                }, 1000)
              }
              // version-check 라이브러리의 .needUpdate 메소드가 응답하지않아(undefined) 위의 방법사용 (21.12.10)  참고: https://github.com/kimxogus/react-native-version-check/issues/141
              // const versionCheck = await VersionCheck.needUpdate({ country: "KR" })
              // console.tron.log(`getCurrentVersion: `, await VersionCheck.getCurrentVersion())
              // console.tron.log(`getLatestVersion: `, await VersionCheck.getLatestVersion())
              // setTimeout(() => {
              //   try {
              //     if (currentAppVersion?.isNeeded === true) {
              //       console.tron.log(`versionCheck.isNeeded === true`, currentAppVersion)
              //       this.setState({
              //         needAppUpdate: true,
              //       })
              //     }
              //   } catch (e) {}
              // }, 1000)
            } catch (e) {}

            const params = new FormData()
            params.append("user", numberUserId)
            params.append("app_ver", packageJson.version)

            let regType
            switch (signPath) {
              case "email":
                regType = "e"
                break
              case "kakao":
                regType = "k"
                break
              case "facebook":
                regType = "f"
                break
              case "naver":
                regType = "n"
                break
              case "apple":
                regType = "apple"
                break
              default:
                regType = "u"
            }
            params.append("reg_type", regType)
            params.append(
              "app_device",
              `${Platform.OS} v${Platform.Version} ${DeviceInfo.getModel()}`,
            )

            if (result.user.newly_agreed) {
              params.append("newly_agreed", result.user.newly_agreed)
            }
            if (result.user.newly_agreed_date) {
              params.append(
                "newly_agreed_date",
                formatDateWithJoinChar(result.user.newly_agreed_date, "-"),
              )
            }

            await api.updateProfile(numberUserId, params)
          }
          // prescriptionStore.fetchPrescriptions(accountStore.user.id, `_u${accountStore.user.id}`)
        }
      }

      if (this.state.needAppUpdate) {
        return
      }
      this.navigate()
    }
  }

  navigate = async () => {
    const { accountStore, navigation } = this.props
    this.setState({ needAppUpdate: false })
    const initialRunning = await AsyncStorage.getItem("initialRunning")
    this.pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")
    const pointsEnabled = firebase.remoteConfig().getBoolean("points_enabled")

    // 로그인 하지 않은 경우
    if (accountStore.user.id === -1) {
      // 앱을 최초 설치한 사용자
      if (initialRunning === null) {
        // Splash를 바로 없애야 Hello 화면을 제대로 보여줄 수 있음
        // RNBootSplash.hide({ fade: true })
        navigation.navigate("Auth", { screen: "Initial" })
      } else {
        // 앱을 최초 설치한 사용자가 아니면 Hello 페이지는 더이상 진입하지 않음
        navigation.navigate("Auth")
      }
    } else if (
      accountStore.user.newly_agreed === "y" ||
      accountStore.user.newly_agreed === "post"
    ) {
      // 잠금 비밀번호를 설정한 사용자
      if (JSON.parse(this.pinCodeEnabled)) {
        navigation.navigate("Auth", {
          screen: "CodeVerification",
          params: { purpose: "verification", preRoute: "AuthLoading" },
        })
      } else if (pointsEnabled) {
        const openedPointGuideScreen = await loadString("openedPointGuideScreen")
        if (openedPointGuideScreen) {
          navigation.navigate("Main")
        } else {
          navigation.navigate("Details", {
            screen: "PointGuide",
          })
          saveString("openedPointGuideScreen", JSON.stringify(true))
        }
      } else {
        navigation.navigate("Main")
      }
    } else {
      navigation.navigate("Auth", {
        screen: "ServiceAgreement",
        params: { isLoginProcess: true },
      })
    }
  }

  openAppStore() {
    if (Platform.OS === "android") {
      Linking.openURL("market://details?id=com.onions.papricacare")
    } else {
      Linking.openURL("itms-apps://apps.apple.com/kr/app/파프리카케어/id1453787406")
    }
  }

  /** 개발자 모드 확인 */
  async fetchDeveloperMode() {
    const developerMode = await AsyncStorage.getItem("developerMode")
    if (!developerMode) {
      /** developerMode가 설정되어 있지 않으면 */
      /** 현재는 아무것도 안함 */
    } else {
      /** developerMode가 설정되어있으면 */
      const mode = Number.parseInt(developerMode)
      this.props.accountStore.changeDeveloperMode(mode)
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.state.serverMaintenance ? (
          <Image
            source={{ uri: this.state.serverMaintenanceImage }}
            style={{ width: "100%", height: "100%" }}
          />
        ) : null}
        <Popup
          isVisible={this.state.needAppUpdate}
          title={"앱을 업데이트 후 이용해 주세요!"}
          message={`더 나아진 파프리카케어의 최신 버전이 출시되었습니다.
              스토어에서 앱을 업데이트한 후 이용해 주세요.`}
          button1={getPopupButton(() => this.openAppStore(), "앱 업데이트하기")}
        />
      </View>
    )
  }
}
