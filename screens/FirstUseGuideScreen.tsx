import { observer } from "mobx-react-lite"
import React, { useRef, useState } from "react"
import {
  Dimensions,
  Platform,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { useSafeArea } from "react-native-safe-area-context"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import Carousel, { Pagination } from "react-native-snap-carousel"

import AsyncStorage from "@react-native-async-storage/async-storage"
import { ParamListBase } from "@react-navigation/native"

import SvgFirstUseGuide1 from "../assets/images/firstUseGuide1.svg"
import SvgFirstUseGuide2 from "../assets/images/firstUseGuide2.svg"
import SvgFirstUseGuide3 from "../assets/images/firstUseGuide3.svg"
import SvgFirstUseGuide4 from "../assets/images/firstUseGuide4.svg"
import SvgAdd from "../assets/images/tabbarAdd.svg"
import SvgDrug from "../assets/images/tabbarDrug.svg"
import SvgHistory from "../assets/images/tabbarHistory.svg"
import SvgHistoryActive from "../assets/images/tabbarHistoryActive.svg"
import SvgHome from "../assets/images/tabbarHome.svg"
import SvgMyPage from "../assets/images/tabbarMyPage.svg"
import CloseButton from "../components/CloseButton"
import { MemberSelectHeader } from "../components/MemberSelectHeader"
import { Text } from "../components/StyledText"
import Triangle from "../components/Triangle"
import { color, typography } from "../theme"

export interface FirstUseGuideScreenProps {
  navigation: NativeStackNavigationProp<ParamListBase>
}

const { width: viewportWidth, height: viewportHeight } = Dimensions.get("window")
const sliderWidth = viewportWidth
const itemWidth = viewportWidth

const ROOT: ViewStyle = {}
const IMAGE_BOX: ViewStyle = {}
const PAGINATION_DOT = {
  width: 8,
  height: 8,
  borderRadius: 4,
  marginHorizontal: -5,
  backgroundColor: color.palette.white,
}
const PAGINATION_INACTIVE_DOT = {
  width: 8,
  height: 8,
  borderRadius: 4,
  backgroundColor: `${color.palette.white}32`,
}
const PAGINATION_CONTAINER = {
  paddingVertical: 0,
}
const HEADER_OVERLAY: ViewStyle = {
  width: viewportWidth,
  height: 54,
  backgroundColor: `${color.palette.black}BF`,
  alignItems: "flex-end",
  paddingHorizontal: 32,
}
const BOTTOM_OVERLAY: ViewStyle = {
  width: viewportWidth,
  height: Platform.OS === "android" ? 78 : 64,
  backgroundColor: Platform.OS === "android" ? `${color.palette.black}BF` : "transparent",
  alignItems: "flex-end",
  paddingHorizontal: 32,
}
const TAB_BAR_LABEL: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 12,
  lineHeight: 18,
  marginTop: 2,
  color: color.palette.grey50,
}
const TAB_BAR_LABEL_FOCUSED: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 12,
  lineHeight: 18,
  marginTop: 2,
  color: color.palette.cordovanBrown,
}
const CONTENT: ViewStyle = {
  flex: 1,
  backgroundColor: `${color.palette.black}BF`,
  alignItems: "center",
  zIndex: 0,
}
const CONTENT_BOTTOM: ViewStyle = {
  ...CONTENT,
  justifyContent: "flex-end",
  paddingBottom: Platform.OS === "android" ? 20 : 8,
}
const CONTENT_TOP: ViewStyle = {
  ...CONTENT,
  justifyContent: "flex-start",
  paddingTop: 8,
}
const TOOLTIP_BOX = {
  backgroundColor: color.palette.orange,
  paddingVertical: 13,
  paddingHorizontal: 20,
  borderRadius: 24,
}
const TOOLTIP_TEXT = { ...typography.body, color: color.palette.white }
const TOOLTIP_TEXT_ACCENT = {
  fontFamily: typography.bold,
  fontSize: 16,
  color: color.palette.white,
}
const TRIANGLE = {
  borderBottomColor: color.palette.orange,
  top: 0,
  marginLeft: 0,
}
const TRIANGLE_FILL = { borderBottomColor: color.palette.orange }

export const FirstUseGuideScreen: React.FunctionComponent<FirstUseGuideScreenProps> = observer(
  (props) => {
    const carouselRef = useRef(null)
    const [entries, setEntries] = useState([
      {
        image: <SvgFirstUseGuide1 width={375} height={206} />,
        title: "복약관리를 시작하세요!",
        description1: "처방전 또는 약봉투를 찍어,",
        description2: "자동으로 복약 일정을 등록하세요.",
      },
      {
        image: <SvgFirstUseGuide2 width={375} height={206} />,
        title: "질병, 약, 병원 분석도 척척.",
        description1: "처방전을 찍어 질병, 약, 병원",
        description2: "분석 내용을 확인하세요!",
      },
      {
        image: <SvgFirstUseGuide3 width={375} height={206} />,
        title: "가족건강 관리도 손쉽게",
        description1: "우리 아이, 부모님의 의료기록 및",
        description2: "복약 관리를 시작하세요.",
      },
      // {
      //   image: <SvgFirstUseGuide4 width={375} height={206} />,
      //   title: "모르는게 약?\n무엇이든 물어보세요!",
      //   description1: "지인에게 듣는 것처럼 쉬운 설명과",
      //   description2: "알찬 정보를 준비했습니다.",
      // },
    ])
    const [activeSlide, setActiveSlide] = useState(0)
    const insets = useSafeArea()
    const { navigation } = props

    const renderItem = ({ item }) => {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            paddingBottom: 24,
            zIndex: 1,
          }}
        >
          <View style={IMAGE_BOX}>{item.image}</View>
          <Text
            style={{
              ...typography.title2,
              color: color.palette.white,
              marginTop: 13,
              marginBottom: 8,
              textAlign: "center",
            }}
          >
            {item.title}
          </Text>
          <Text style={{ ...typography.sub2, opacity: 0.9, color: color.palette.white }}>
            {item.description1}
          </Text>
          <Text style={{ ...typography.sub2, opacity: 0.9, color: color.palette.white }}>
            {item.description2}
          </Text>
        </View>
      )
    }

    const renderTooltip = () => {
      let content
      switch (activeSlide) {
        case 0:
          content = (
            <View style={CONTENT_BOTTOM}>
              <View style={TOOLTIP_BOX}>
                <Text style={TOOLTIP_TEXT}>
                  이곳을 눌러 <Text style={TOOLTIP_TEXT_ACCENT}>처방전</Text>,{" "}
                  <Text style={TOOLTIP_TEXT_ACCENT}>약봉투</Text>를 찍으세요.
                </Text>
              </View>
              <Triangle style={TRIANGLE} fillStyle={TRIANGLE_FILL} isDown />
            </View>
          )
          break

        case 1:
          content = (
            <View style={CONTENT_BOTTOM}>
              <View style={TOOLTIP_BOX}>
                <Text style={TOOLTIP_TEXT}>
                  <Text style={TOOLTIP_TEXT_ACCENT}>의료기록</Text> 분석 내용은 이곳에서 확인!
                </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 1, alignItems: "center" }}>
                  <Triangle style={TRIANGLE} fillStyle={TRIANGLE_FILL} isDown />
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 1 }} />
                <View style={{ flex: 1 }} />
              </View>
            </View>
          )
          break

        case 2:
          content = (
            <View style={CONTENT_TOP}>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 1, alignItems: "center" }}>
                  <Triangle style={TRIANGLE} fillStyle={TRIANGLE_FILL} />
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 1 }} />
                <View style={{ flex: 1 }} />
              </View>
              <View style={TOOLTIP_BOX}>
                <Text style={TOOLTIP_TEXT}>
                  이곳을 눌러 <Text style={TOOLTIP_TEXT_ACCENT}>가족회원</Text>을 추가하세요.
                </Text>
              </View>
            </View>
          )
          break

        default:
          content = <View style={CONTENT_BOTTOM} />
      }
      return (
        <View style={{ height: viewportHeight, paddingBottom: insets.bottom }}>
          <View style={{ backgroundColor: `${color.palette.black}BF`, paddingTop: insets.top }} />
          {Platform.OS === "android" ? (
            <View style={[HEADER_OVERLAY, activeSlide == 2 ? { marginLeft: 196 } : null]}></View>
          ) : (
            <View
              style={[HEADER_OVERLAY, activeSlide == 2 ? { backgroundColor: "transparent" } : null]}
            >
              <CloseButton
                style={{
                  position: "absolute",
                  marginTop: 20,
                  right: 16,
                }}
                imageStyle={{
                  tintColor: activeSlide == 2 ? color.palette.black : color.palette.white,
                }}
                onPress={async () => {
                  await AsyncStorage.setItem("showedFirstUseGuide", JSON.stringify(true))
                  navigation.goBack()
                }}
              />
            </View>
          )}
          {content}
          <View
            style={[
              BOTTOM_OVERLAY,
              activeSlide == 2 ? { backgroundColor: `${color.palette.black}BF` } : null,
            ]}
          >
            {Platform.OS === "android" ? (
              <TouchableOpacity
                style={{ marginBottom: Platform.OS === "android" ? 60 : 0 }}
                // @ts-ignore
                onPress={async () => {
                  await AsyncStorage.setItem("showedFirstUseGuide", JSON.stringify(true))
                  navigation.goBack()
                }}
              >
                <Text style={TOOLTIP_TEXT_ACCENT}>닫기</Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      )
    }

    const renderBottomTab = () => {
      return (
        <View style={{ height: 64, flexDirection: "row", alignItems: "center" }}>
          <View style={{ flex: 1, alignItems: "center" }}>
            <SvgHome />
            <Text allowFontScaling={false} style={TAB_BAR_LABEL}>
              홈
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              height: 64,
              justifyContent: "center",
              alignItems: "center",
              zIndex: activeSlide == 1 ? 10 : 0,
              backgroundColor:
                Platform.OS === "android" && activeSlide == 1 ? color.palette.white : "transparent",
            }}
          >
            {activeSlide == 1 ? <SvgHistoryActive /> : <SvgHistory />}

            <Text
              allowFontScaling={false}
              style={activeSlide == 1 ? TAB_BAR_LABEL_FOCUSED : TAB_BAR_LABEL}
            >
              의료기록
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              height: 64,
              justifyContent: "center",
              alignItems: "center",
              zIndex: activeSlide == 0 ? 1 : 0,
              backgroundColor:
                Platform.OS === "android" && activeSlide == 0 ? color.palette.white : "transparent",
            }}
          >
            <SvgAdd width={50} height={50} />
          </View>
          <View style={{ flex: 1, alignItems: "center" }}>
            <SvgDrug />
            <Text allowFontScaling={false} style={TAB_BAR_LABEL}>
              복약관리
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: "center" }}>
            <SvgMyPage />
            <Text allowFontScaling={false} style={TAB_BAR_LABEL}>
              내정보
            </Text>
          </View>
        </View>
      )
    }

    return (
      <View style={{ flex: 1, paddingTop: insets.top }}>
        <StatusBar
          backgroundColor={`${color.palette.black}BF`}
          barStyle="dark-content"
          hidden={Platform.OS === "ios"}
        />
        <MemberSelectHeader onSelectMember={undefined} hideNotice />
        <View style={{ flex: 2, zIndex: 1, justifyContent: "center" }}>
          <Carousel
            ref={carouselRef}
            data={entries}
            renderItem={renderItem}
            sliderWidth={sliderWidth}
            itemWidth={itemWidth}
            hasParallaxImages={true}
            inactiveSlideScale={1}
            inactiveSlideOpacity={1}
            loop={true}
            loopClonesPerSide={2}
            onSnapToItem={(index) => setActiveSlide(index)}
            useScrollView
          />
          <Pagination
            dotsLength={entries.length}
            activeDotIndex={activeSlide}
            containerStyle={PAGINATION_CONTAINER}
            dotStyle={PAGINATION_DOT}
            inactiveDotStyle={PAGINATION_INACTIVE_DOT}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
            carouselRef={carouselRef.current}
          />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: "flex-end",
          }}
        >
          {renderBottomTab()}
        </View>
        <View
          style={{
            position: "absolute",
            width: viewportWidth,
            height: viewportHeight - insets.top - insets.bottom,
          }}
        >
          {renderTooltip()}
        </View>
        <View
          style={{ backgroundColor: `${color.palette.black}BF`, paddingBottom: insets.bottom }}
        />
      </View>
    )
  },
)
