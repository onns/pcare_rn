import { observer } from "mobx-react-lite"
import { Toast } from "native-base"
import React, { useState } from "react"
import { Image, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { ScrollView } from "react-native-gesture-handler"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import RadioForm, { RadioButton, RadioButtonInput } from "react-native-simple-radio-button"

import { api } from "@api/api"
import { Slider } from "@miblanchard/react-native-slider"
import { useNavigation } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import TouchableItem from "../components/button/TouchableItem"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { DetailsStackParamList } from "../navigation/types"
import { Question } from "../stores/record/question"
import { Survey } from "../stores/record/survey"
import { color, styles, typography } from "../theme"

export interface SurveyQuestionsScreenProps {
  navigation: StackNavigationProp<DetailsStackParamList>
}

const ROOT: ViewStyle = {
  flex: 1,
  backgroundColor: color.background,
}
const ROW: ViewStyle = { flexDirection: "row" }
const ROW_CENTER: ViewStyle = { flexDirection: "row", alignItems: "center" }
const SLIDER: ViewStyle = { flex: 1, height: 4, borderRadius: 1, margin: 0, padding: 0 }
const SLIDER_TRACK: ViewStyle = { height: 4, borderRadius: 1, backgroundColor: color.palette.pale }
const BUTTON_TEXT = { ...typography.title3, color: color.palette.white }

const INDEX_TOTAL = { ...typography.body, color: color.primary, marginBottom: 4 }
const CONTENT: ViewStyle = {
  paddingHorizontal: 24,
  paddingVertical: 16,
  paddingBottom: 60,
  backgroundColor: color.palette.white,
}
const IMAGE = { borderRadius: 5, height: 130 }
const BACK_TEXT = {
  fontFamily: typography.primary,
  fontSize: 17,
  color: color.primary,
  marginRight: 24,
}
const BOTTOM_AREA: ViewStyle = {
  position: "absolute",
  bottom: 0,
  width: "100%",
}
const hitSlop20 = { top: 20, left: 20, right: 20, bottom: 20 }
const HEADER: ViewStyle = {
  height: 56,
  ...ROW,
  marginRight: 20,
  alignItems: "center",
  backgroundColor: color.background,
}
const QUESTION_TEXT = { ...typography.title2, marginBottom: 8, marginTop: 12 }
const RADIO_BUTTON: ViewStyle = {
  borderRadius: 16,
  maxHeight: 80,
  height: 74,
  alignItems: "center",
  paddingRight: 20,
}
const RADIO_BUTTON_INPUT_WRAP = { marginLeft: 19 }
const SCORE_GUIDE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  lineHeight: 21,
  color: color.palette.grey70,
  marginBottom: 32,
}
const POPUP_TITLE = { ...typography.title3, textAlign: "center" }
const POPUP_TITLE_ACCENT = { ...typography.title3, color: color.primary }
const POPUP_TITLE_END_MESSAGE: TextStyle = { ...typography.title3, textDecorationLine: "underline" }

export const SurveyQuestionsScreen: React.FunctionComponent<SurveyQuestionsScreenProps> = observer(
  (props) => {
    const navigation = useNavigation()
    const { recordStore } = useStores()
    const requestedSurvey: Survey = recordStore.requestedSurvey
    const surveyType = requestedSurvey.name
    const { questions } = requestedSurvey
    const [questionIndex, setQuestionIndex] = useState(0)
    const [popupVisible, setPopupVisible] = useState(false)
    const question: Question = questions[questionIndex]
    const [gradationWidth, setGradationWidth] = useState(0)
    const insets = useSafeAreaInsets()

    const getRadioButtonsData = (q: Question) => {
      const radioButtonsData = []
      for (const { description: label, score: value } of q.answers) {
        radioButtonsData.push({ label, value })
      }
      return radioButtonsData
    }
    const [radioButtons, setRadioButtons] = useState(getRadioButtonsData(question))
    const [answerIndex, setAnswerIndex] = useState(0)
    const [totalScore, setTotalScore] = useState(0)
    const [endingMessage, setEndingMessage] = useState("")

    React.useLayoutEffect(() => {
      navigation.setOptions({
        headerShown: false,
      })
    }, [navigation])

    const getEndingMessage = (score: number) => {
      let result = ""
      for (const endingMessage of requestedSurvey.ending_messages) {
        if (endingMessage.range_start <= score && score <= endingMessage.range_end) {
          result = endingMessage.message
          break
        }
      }
      return result
    }

    const submit = async (score: number) => {
      setEndingMessage(getEndingMessage(score))
      const result = await api.postSurvey(requestedSurvey.id, { data: score })
      if (result.kind === "ok") {
        setPopupVisible(true)
      } else {
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
      }
    }

    const notLastQuestion = () => questionIndex + 1 !== questions.length

    function next() {
      setTotalScore(totalScore + question.answers[answerIndex].score)

      if (notLastQuestion()) {
        const q = questions[questionIndex + 1]
        setQuestionIndex(questionIndex + 1)
        setRadioButtons(getRadioButtonsData(q))
        setAnswerIndex(0) // 다음 질문에 선택된 답변 초기화
      } else {
        submit(totalScore + question.answers[answerIndex].score)
      }
    }

    const goBack = () => {
      setPopupVisible(false)
      navigation.goBack() // 이전 화면 SurveyIntroScreen 으로 이동
      navigation.goBack() // SurveyIntroScreen 의 이전화면으로 이동
      navigation.navigate("Main") // 예상치 못한 지점에서 이곳으로 왔을 것을 대비하여 Main 으로 이동
    }

    function onRadioButtonPress(index: number) {
      setAnswerIndex(index)
    }

    const renderPopupTitleMessage = () => {
      if (endingMessage.length === 0) {
        return (
          <Text style={POPUP_TITLE}>
            수고 하셨습니다 !{"\n"}검사 결과는{" "}
            <Text style={POPUP_TITLE_ACCENT}>{totalScore}점</Text>
            입니다.
          </Text>
        )
      } else {
        return (
          <Text style={POPUP_TITLE}>
            수고 하셨습니다 !{"\n"}검사 결과는{" "}
            <Text style={POPUP_TITLE_ACCENT}>{totalScore}점</Text>으로
            {"\n"}
            <Text style={POPUP_TITLE_END_MESSAGE}>{endingMessage}</Text>
          </Text>
        )
      }
    }

    const onGradationViewWidth = (event) => {
      const { width } = event.nativeEvent.layout
      setGradationWidth(width)
    }

    const renderGradation = (index: number) => {
      const STYEL = { top: -10, left: 10, width: gradationWidth / 7.5, height: 7 }
      const firstColor = "#FFE8E1"
      const secondColor = "#FDC0AF"
      const thirdColor = "#F5977F"
      const fourthColor = "#E96E52"
      const fifthColor = "#D83E25"

      const firstView = () => {
        return <View style={[STYEL, { backgroundColor: firstColor, marginRight: 5 }]} />
      }
      const secondView = () => {
        return (
          <>
            {firstView()}
            <View style={[STYEL, { backgroundColor: secondColor, marginRight: 5 }]} />
          </>
        )
      }
      const thirdView = () => {
        return (
          <>
            {secondView()}
            <View style={[STYEL, { backgroundColor: thirdColor, marginRight: 5 }]} />
          </>
        )
      }
      const fourthView = () => {
        return (
          <>
            {thirdView()}
            <View style={[STYEL, { backgroundColor: fourthColor, marginRight: 5 }]} />
          </>
        )
      }
      const fifthView = () => {
        return (
          <>
            {fourthView()}
            <View style={[STYEL, { backgroundColor: fifthColor }]} />
          </>
        )
      }

      switch (index) {
        case 1:
          return firstView()
        case 2:
          return secondView()
        case 3:
          return thirdView()
        case 4:
          return fourthView()
        case 5:
          return fifthView()
        default:
          return null
      }
    }

    return (
      <Screen style={ROOT} preset="fixed">
        <View style={HEADER}>
          <TouchableItem onPress={navigation.goBack}>
            <View style={ROW_CENTER}>
              <CustomHeaderBackImage orange />
              <Text style={BACK_TEXT}>뒤로</Text>
            </View>
          </TouchableItem>
          <Slider
            disabled
            value={questionIndex + 1}
            containerStyle={SLIDER}
            trackStyle={SLIDER_TRACK}
            thumbStyle={{ width: 0 }}
            minimumTrackTintColor={color.primary}
            maximumValue={questions.length}
          />
        </View>
        <ScrollView contentContainerStyle={CONTENT}>
          <Text style={INDEX_TOTAL}>
            {questionIndex + 1}/{questions.length}
          </Text>
          {question.image && (
            <Image source={{ uri: question.image }} style={IMAGE} resizeMode="cover" />
          )}
          <Text style={QUESTION_TEXT}>{question.description}</Text>
          <Text style={SCORE_GUIDE}>
            {requestedSurvey.thtb // 모든 질문지에 보여줄 것인지 확인 필요.
              ? "" // 요구사항에 따라 텍스트 추가할 것.
              : "0점이 가장 상태가 좋음을 5점이 가장 상태가 나쁨을 표현합니다."}
          </Text>

          <RadioForm animation={true}>
            {radioButtons.map((obj, i) => (
              <RadioButton
                labelHorizontal={true}
                key={i}
                style={{
                  ...RADIO_BUTTON,
                  backgroundColor: answerIndex === i ? color.palette.veryLightPink3 : undefined,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                  }}
                >
                  <RadioButtonInput
                    obj={obj}
                    index={i}
                    isSelected={answerIndex === i}
                    onPress={() => onRadioButtonPress(i)}
                    borderWidth={2}
                    buttonInnerColor={color.primary}
                    buttonOuterColor={answerIndex === i ? color.primary : color.palette.grey30}
                    buttonSize={14}
                    buttonOuterSize={22}
                    buttonStyle={{}}
                    buttonWrapStyle={RADIO_BUTTON_INPUT_WRAP}
                  />
                  {surveyType === "CAT" ? (
                    <View
                      style={{
                        width: "100%",
                      }}
                    >
                      {i > 0 ? (
                        <View
                          onLayout={onGradationViewWidth}
                          style={{ flexDirection: "row", height: 0 }}
                        >
                          {renderGradation(i)}
                        </View>
                      ) : null}

                      <TouchableOpacity
                        style={{ paddingLeft: 10, marginTop: 2 }}
                        hitSlop={hitSlop20}
                        onPress={() => onRadioButtonPress(i)}
                      >
                        <Text
                          style={[
                            typography.title3,
                            {
                              lineHeight: 20,
                              width: gradationWidth / 1.3,
                            },
                          ]}
                        >
                          {obj.label}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <TouchableOpacity
                      style={{ paddingLeft: 10, marginTop: 2 }}
                      hitSlop={hitSlop20}
                      onPress={() => onRadioButtonPress(i)}
                    >
                      <Text
                        style={[
                          typography.title3,
                          {
                            lineHeight: 20,
                          },
                        ]}
                      >
                        {obj.label}
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </RadioButton>
            ))}
          </RadioForm>
        </ScrollView>

        <View
          style={{
            ...BOTTOM_AREA,
            paddingBottom: insets.bottom + 12,
            backgroundColor: "white",
          }}
        >
          <Button block primary rounded onPress={next} style={{ marginHorizontal: 22 }}>
            <Text style={BUTTON_TEXT}>{notLastQuestion() ? `다음` : `완료`}</Text>
          </Button>
        </View>

        <View style={{ marginTop: 60 }}></View>

        <Popup
          isVisible={popupVisible}
          title={renderPopupTitleMessage()}
          message={`자세한 내용은 리포트에서 확인할 수 있습니다.`}
          button1={getPopupButton(goBack, "확인")}
        />
      </Screen>
    )
  },
)
