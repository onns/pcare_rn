import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, ScrollView } from "native-base"
import React from "react"
import { TextStyle, View, ViewStyle } from "react-native"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"
import { Button } from "tamagui"

import { api } from "@api/api"
import analytics from "@react-native-firebase/analytics"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { Text } from "../components/StyledText"
import { sendLogScreenView } from "../lib/analytics"
import { AuthStackParamList, RootStackParamList } from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
  justifyContent: "space-between",
}
const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  justifyContent: "space-between",
  paddingHorizontal: 16,
}
const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingBottom: 31,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 32,
  color: color.text,
}
const MESSAGE: TextStyle = {
  ...typography.title2,
  marginBottom: 24,
  textAlign: "center",
}
const MIDDLE_MESSAGE: TextStyle = {
  marginTop: 24,
  fontSize: 13,
  color: color.palette.black,
  alignSelf: "center",
  marginBottom: 40,
  textAlign: "center",
}
const BOTTOM_BUTTONS: ViewStyle = {
  justifyContent: "flex-end",
  padding: 16,
  backgroundColor: color.palette.white,
}
const BOTTOM_MESSAGE: TextStyle = {
  padding: 16,
  fontFamily: typography.primary,
  fontSize: 13,
  color: color.palette.grey50,
  backgroundColor: color.palette.lightGrayishOrange,
  alignSelf: "center",
  marginBottom: 40,
  borderRadius: 1,
}
const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  color: color.palette.white,
}
const DIVIDER: ViewStyle = { width: 16, height: 1, backgroundColor: "#000000", alignSelf: "center" }
const CANCEL_BUTTON: ViewStyle = {
  backgroundColor: color.palette.veryLightPink3,
  marginBottom: 8,
}

type TourGuideScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "TourGuide">,
  StackNavigationProp<RootStackParamList>
>
type TourGuideScreenRouteProp = RouteProp<AuthStackParamList, "TourGuide">

type Props = {
  navigation: TourGuideScreenNavigationProp
  route: TourGuideScreenRouteProp
  accountStore: AccountStore
}

interface State {
  reqAccount: any
}

@inject("accountStore")
@observer
export default class TourGuideScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "",
    headerLeft: null,
  }

  componentDidMount() {
    const { route } = this.props
    sendLogScreenView({ screenClass: route.name, screenName: route.name })
  }

  render() {
    const { accountStore, navigation } = this.props
    return (
      <SafeAreaInsetsContext.Consumer>
        {(insets) => (
          <View style={{ ...BACKGROUND, flex: 1, paddingBottom: insets.bottom }}>
            <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
              <View>
                <View style={TITLE}>
                  <Text style={TITLE_TEXT}>둘러보기</Text>
                </View>
                <Text style={MESSAGE}>회원가입 걱정없이{"\n"}먼저 편하게 둘러보세요</Text>
                <View style={DIVIDER} />
                <Text style={MIDDLE_MESSAGE}>
                  둘러보기 중 언제든지 회원가입을{"\n"}하실수 있습니다.
                </Text>
                <Text style={BOTTOM_MESSAGE}>
                  둘러보기 시 제한적인 기능만 사용이 가능합니다. 처방전 및 약봉투 찍기, 질병 및 약제
                  통계정보 확인, 가족회원관리는 회원가입이 필요한 서비스입니다.{"\n"}해당 기능을
                  사용하실 분들은 회원가입 후 사용을 부탁드립니다.
                </Text>
              </View>
            </ScrollView>
            <View style={BOTTOM_BUTTONS}>
              <Button
                bc={color.palette.veryLightPink3}
                br={45}
                style={CANCEL_BUTTON}
                onPress={() => {
                  analytics().logEvent("tour_cancel_pressed")
                  amplitude.getInstance().logEvent("tour_cancel_pressed")
                  navigation.goBack()
                }}
              >
                <Text style={[BUTTON_TEXT, { color: color.palette.grey50 }]}>취소</Text>
              </Button>
              <Button
                bc={color.palette.orange}
                br={45}
                onPress={() => {
                  const guestName = "방문자"
                  accountStore.user.setNickname(guestName)
                  accountStore.setSelectedMemberName(guestName)
                  accountStore.setSelectedMemberKey(`_u${accountStore.user.id}`)
                  api.apisauce.deleteHeader("Authorization")
                  navigation.navigate("Main", { screen: "HomeStack" })
                }}
              >
                <Text style={BUTTON_TEXT}>둘러보기</Text>
              </Button>
            </View>
          </View>
        )}
      </SafeAreaInsetsContext.Consumer>
    )
  }
}
