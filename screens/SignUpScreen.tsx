import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import { ApiResponse } from "apisauce"
import * as NavigationBar from "expo-navigation-bar"
import { inject, observer } from "mobx-react"
import { Box, Container, ScrollView, Toast } from "native-base"
import React from "react"
import {
  Alert,
  Dimensions,
  Platform,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import appsFlyer from "react-native-appsflyer"
import email from "react-native-email"
import { AccessToken, LoginManager, Settings } from "react-native-fbsdk-next"

import { appleAuth } from "@invertase/react-native-apple-authentication"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { KakaoOAuthToken, login as kakaoLogin } from "@react-native-seoul/kakao-login"
import NaverLogin from "@react-native-seoul/naver-login"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { api, LoginParams } from "../api"
import TouchableItem from "../components/button/TouchableItem"
import CircleButton from "../components/CircleButton"
import CloseButton from "../components/CloseButton"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { color, styles, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  // flex: 1,
  alignItems: "center",
  // width: Dimensions.get("screen").width,
  // width: "100%",
  height: Dimensions.get("screen").height,
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  // flex: 1,

  flexDirection: "column",
  paddingTop: 16,
  paddingHorizontal: 32,
  paddingBottom: 40,
}
const WELCOME: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "center",
  paddingBottom: 48,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 32,
  lineHeight: 37,
  color: color.text,
}
const ROW_CENTER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
}
const CENTER: ViewStyle = {
  alignItems: "center",
}
const CIRCLE_BUTTON = { marginHorizontal: 14 }

const BUTTONS: ViewStyle = {
  borderWidth: 1,
  borderRadius: 45,
  paddingVertical: 16,
  alignItems: "center",
  marginBottom: 16,
}
const SIGN_UP_BUTTON: ViewStyle = {
  ...BUTTONS,
  borderColor: color.palette.grey30,
}
const TOUR_BUTTON: ViewStyle = {
  ...BUTTONS,
  borderColor: color.palette.red50,
}
const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 24,
  color: color.palette.grey50,
}
const TOUR: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 24,
  color: color.primary,
}
const WITH_SNS_TITLE = { paddingBottom: 16 }
const DIVIDER: ViewStyle = { flex: 1, height: 1, backgroundColor: color.palette.grey30 }
const DIVIDER_ROW: ViewStyle = {
  flexDirection: "row",
  marginTop: 32,
  alignItems: "center",
  marginBottom: 32,
}
const OR: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 24,
  color: color.palette.grey50,
  marginHorizontal: 16,
}
const BUTTON: ViewStyle = {
  borderRadius: 5,
  marginRight: 32,
}

const sourceKakao = require("../assets/images/snsKakao.png")
const sourceFacebook = require("../assets/images/snsFacebook.png")
const sourceNaver = require("../assets/images/snsNaver.png")
const hitSlop10 = {
  top: 10,
  left: 10,
  bottom: 10,
  right: 10,
}

const initials = {
  kConsumerKey: "JinS1xIkcrVHYnewU7Dg", // 애플리케이션에서 사용하는 클라이언트 아이디
  kConsumerSecret: "7VrIXWfXil", // 애플리케이션에서 사용하는 클라이언트 시크릿
  kServiceAppName: "파프리카케어", // 애플리케이션 이름
  kServiceAppUrlScheme: "naverloginJinS1xIkcrVHYnewU7Dg", // only for iOS, 콜백을 받을 URL Scheme
}

type SignUpRouteProp = RouteProp<AuthStackParamList, "SignUp">

type SignUpNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "SignUp">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<HomeStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  navigation: SignUpNavigationProp
  route: SignUpRouteProp
}

@inject("accountStore")
@observer
export default class SignUpScreen extends React.Component<Props, {}> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route
    return {
      title: null,
      headerBackTitle: null,
      headerLeft: params?.hideTourButton // https://onions.atlassian.net/browse/PAP-771
        ? () => (
            <CloseButton
              style={{ marginLeft: 24 }}
              imageStyle={{ tintColor: color.palette.gray3 }}
              onPress={() => navigation.navigate("Main", { screen: params?.prevScreen })}
            />
          )
        : () => (
            <TouchableItem
              style={{ marginLeft: 12 }}
              onPress={() => navigation.navigate("Walkthrough")}
            >
              <CustomHeaderBackImage />
            </TouchableItem>
          ),
      headerRight: () => (
        <TouchableItem
          onPress={() =>
            navigation.replace(
              "SignIn",
              params?.hideTourButton
                ? {
                    backRouteName: "Main",
                    params: { screen: params?.prevScreen },
                  }
                : undefined,
            )
          }
          style={BUTTON}
          hitSlop={hitSlop10}
        >
          <Text
            style={{
              ...typography.body,
            }}
          >
            로그인
          </Text>
        </TouchableItem>
      ),
      animationEnabled: false,
    }
  }

  _unsubscribeFocus: any

  constructor(props: Props) {
    super(props)
    const { accountStore } = this.props
    accountStore.createRequestAccount()
    accountStore.reqAccount!.setMode("login")
  }

  async componentDidMount() {
    const { accountStore } = this.props
    const { signPath } = accountStore
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)
    const email = await AsyncStorage.getItem("email")
    if (email && signPath == "email") {
      accountStore.reqAccount!.setEmail(email)
    }
  }

  didFocus = () => {
    const { accountStore, route } = this.props
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })

    NavigationBar.setBackgroundColorAsync("#efefef")
    if (!accountStore.reqAccount) {
      accountStore.createRequestAccount()
      accountStore.reqAccount!.setMode("login")
    }
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
  }

  render() {
    const { route } = this.props
    const isTourButtonVisible = this.isTourButtonVisible(route.params)

    return (
      <View style={BACKGROUND}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView scrollEnabled={false} contentContainerStyle={CONTENT_CONTAINER}>
          <View style={WELCOME}>
            <Text style={TITLE_TEXT}>회원가입</Text>
          </View>

          <View style={CENTER}>
            <Text style={WITH_SNS_TITLE}>간편하게 SNS로 가입하세요.</Text>
            <View style={ROW_CENTER}>
              <CircleButton source={sourceKakao} onPress={this.kakaoLogin} style={CIRCLE_BUTTON} />
              <CircleButton
                source={sourceFacebook}
                onPress={this.facebookLogin}
                style={CIRCLE_BUTTON}
              />
              <CircleButton source={sourceNaver} onPress={this.naverLogin} style={CIRCLE_BUTTON} />
              {Platform.OS === "ios" ? (
                <CircleButton source="apple" onPress={this.signInWithApple} style={CIRCLE_BUTTON} />
              ) : null}
            </View>
          </View>
          <View style={DIVIDER_ROW}>
            <View style={DIVIDER} />
            <Text style={OR}>또는</Text>
            <View style={DIVIDER} />
          </View>
          <TouchableOpacity style={SIGN_UP_BUTTON} onPress={this.signUpByEmail}>
            <Text style={BUTTON_TEXT}>이메일로 가입하기</Text>
          </TouchableOpacity>
          {isTourButtonVisible ? (
            <TouchableOpacity style={TOUR_BUTTON} onPress={this.goTour}>
              <Text style={TOUR}>둘러보기</Text>
            </TouchableOpacity>
          ) : null}
        </ScrollView>
      </View>
    )
  }

  goTour = () => {
    this.props.navigation.navigate("TourGuide")
  }

  signUpByEmail = () => {
    this.props.navigation.navigate("SignUpEmail")
  }

  passwordReset = () => {
    this.props.navigation.navigate("Reset")
  }

  handleEmail = () => {
    const user = this.props.accountStore.user
    const to = ["onions@onns.co.kr"] // string or array of email addresses

    let body = `네이버 계정으로 로그인 시 발생한 오류 입니다. \n증상이 나타나기까지의 과정을 아래에 적어주세요.\n\n\n\n\n`
    body =
      body +
      `\n\n파프리카케어 사용에 불편을 드려 대단히 죄송합니다.\n문제 해결에 최선을 다하겠습니다.\n\n\n`
    body = body + `파프리카케어 ID: ${user.id}\n파프리카케어 앱 버전: ${packageJson.version}\n`
    body = body + `플랫폼 OS: ${Platform.OS} v${Platform.Version}`
    // TODO: add device model?

    email(to, {
      subject: "네이버 로그인 오류",
      body: body,
    }).catch(console.error)
  }

  getSignPathName(path: "kakao" | "naver" | "facebook" | "apple" | "email") {
    switch (path) {
      case "kakao":
        return "카카오"
      case "apple":
        return "애플"
      case "naver":
        return "네이버"
      case "facebook":
        return "페이스북"
      case "email":
        return "이메일"
    }
  }

  async authenticate(path: "kakao" | "naver" | "facebook" | "apple" | "email", params: object) {
    const { accountStore, navigation } = this.props
    api.apisauce.deleteHeader("Authorization")

    let urlPath
    if (path === "email") {
      urlPath = "login"
    } else {
      urlPath = path
    }

    // start making calls
    const response: ApiResponse<any> = await api.apisauce.post(`/rest-auth/${urlPath}/`, params)

    if (!response.ok && !response.data.non_field_errors) {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`일시적으로 서버에 접속할 수 없습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      return
    }

    if (response.data?.key) {
      firebase.analytics().logEvent("login", { signPath: path })
      firebase.analytics().logEvent("login_test", { signPath: `${path}_test` })
      firebase.analytics().setUserId(String(response.data.user))
      amplitude.getInstance().setUserId(response.data.user)
      amplitude.getInstance().logEvent("login", { signPath: path })
      appsFlyer.logEvent("af_login", { signPath: path })
      accountStore.user.setId(response.data.user)
      accountStore.setSignPath(path)

      api.apisauce.setHeader("Authorization", "Token ".concat(response.data.key))
      await AsyncStorage.setItem("userToken", response.data.key)
      await AsyncStorage.setItem("userId", String(response.data.user))
      await AsyncStorage.setItem("signPath", path)

      const { reqAccount } = accountStore
      if (path === "email" && reqAccount) {
        reqAccount.passwordOnChange("")
        await AsyncStorage.setItem("email", reqAccount.email)
      }

      // 기존 사용자 여부 판단하기 위한 사용자 정보 가져오기
      const myInfoResult = await api.getMyInfo(Number(accountStore.user.id), accountStore.signPath)
      if (myInfoResult.kind === "ok") {
        accountStore.setUser(myInfoResult.user)
        const { user } = accountStore

        // profile 이 없는 경우
        if (user.name && user.name.length === 0) {
          console.tron.logImportant("< SignUpScreen > authenticate, Profile 이 존재하지 않음")
        } // profile 있는 경우
        else if (user.newly_agreed == null || user.newly_agreed == "pre") {
          navigation.navigate("ServiceAgreement")
        } else {
          navigation.navigate("Main", { screen: "HomeStack" })
        }
      }
    } else if (
      response.data.non_field_errors &&
      String(response.data.non_field_errors[0]).includes(
        "User is already registered with this e-mail address.",
      )
    ) {
      Alert.alert(
        "",
        this.getExistAccountMessage(path),
        [{ text: "확인", onPress: () => navigation.navigate("SignIn") }],
        { cancelable: false },
      )
    } else if (response.data.detail) {
      Alert.alert("", response.data.detail, [{ text: "확인", onPress: () => {} }], {
        cancelable: false,
      })
    } else if (
      String(response.data.non_field_errors[0]).includes("제공된 인증데이터") &&
      path === "email"
    ) {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`파프리카케어에 등록되지 않은 아이디이거나, 아이디 또는 비밀번호를 잘못 입력하셨습니다.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (response.data.non_field_errors) {
      Alert.alert(
        "",
        `오류가 발생하였습니다. \n` + response.data.non_field_errors[0],
        [
          {
            text: "확인",
            onPress: () =>
              console.tron.log(
                "< SignUpScreen > authenticate, response.data.non_field_errors: ",
                response.data.non_field_errors,
              ),
          },
        ],
        { cancelable: false },
      )
    } else {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
      Alert.alert(
        "",
        `오류가 발생하였습니다. \n나중에 다시 시도해주세요. ` +
          (response.problem ? response.problem : ""),
        [
          {
            text: "확인",
            onPress: () =>
              console.tron.log("< SignUpScreen > authenticate, else error: ", response),
          },
        ],
        { cancelable: false },
      )
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{response.data}</Text>,
        duration: 3500,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  getExistAccountMessage(path: "kakao" | "naver" | "facebook" | "apple" | "email"): string {
    if (path === "email") {
      return "입력하신 이메일 주소는 다른 SNS 계정으로 이미 등록되어 있습니다. 이전에 로그인 하신 방법으로 다시 시도해 주세요."
    } else {
      return `해당 ${this.getSignPathName(
        path,
      )} 계정에서 사용하는 이메일 주소는 다른 SNS 계정 또는 이메일 계정으로 이미 등록되어 있습니다. 이전에 로그인 하신 방법으로 다시 시도해 주세요.`
    }
  }

  signInWithApple = async () => {
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      })
      console.tron.log(
        "< SignInScreen > signInWithApple, appleAuthRequestResponse: ",
        appleAuthRequestResponse,
      )

      const { identityToken } = appleAuthRequestResponse

      if (identityToken) {
        // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
        this.authenticate("apple", { access_token: identityToken })
      } else {
        // no token - failed sign-in?
        Alert.alert("", "no token - failed sign-in?", [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
      }
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.tron.warn("< SignInScreen > signInWithApple, User canceled Apple Sign in.")
        Alert.alert("", "User canceled Apple Sign in.", [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
      } else {
        console.tron.log("< SignInScreen > signInWithApple, error", error)
        Alert.alert("error", error, [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
      }
    }
  }

  naverLogin = async () => {
    const { accountStore } = this.props
    accountStore.setTemporaryPinCodeDisabled(true)
    setTimeout(() => {
      accountStore.setTemporaryPinCodeDisabled(false)
    }, 3000)

    try {
      const { failureResponse, successResponse } = await NaverLogin.login({
        appName: initials.kServiceAppName,
        consumerKey: initials.kConsumerKey,
        consumerSecret: initials.kConsumerSecret,
        serviceUrlScheme: initials.kServiceAppUrlScheme,
      })

      if (successResponse) {
        this.authenticate("naver", { access_token: successResponse.accessToken })
      } else if (failureResponse && !failureResponse.isCancel) {
        console.tron.log(failureResponse.message)
      }
    } catch (err) {
      console.tron.log("< SignInScreen > naverLogin, error: ", err)
    }
  }

  // 카카오 로그인 시작.
  kakaoLogin = async () => {
    const { accountStore } = this.props
    accountStore.setTemporaryPinCodeDisabled(true)
    setTimeout(() => {
      accountStore.setTemporaryPinCodeDisabled(false)
    }, 3000)

    try {
      const token: KakaoOAuthToken = await kakaoLogin()
      if (token) {
        this.authenticate("kakao", {
          access_token: token.accessToken,
        })
      }
    } catch (err) {
      if (err.code != "E_CANCELLED_OPERATION") {
        Alert.alert(
          "",
          `카카오 계정으로 로그인 중 오류가 발생하였습니다. \n나중에 다시 시도해주세요. `,
          [
            {
              text: "확인",
              onPress: () => console.tron.log("< SignInScreen > kakaoLogin, error: ", err),
            },
          ],
          { cancelable: false },
        )
      }
    }
  }

  facebookLogin = async () => {
    await Settings.initializeSDK()
    const { accountStore, navigation } = this.props

    api.apisauce.deleteHeader("Authorization")

    accountStore.setTemporaryPinCodeDisabled(true)
    // Attempt a login using the Facebook login dialog asking for default permissions.
    const result = await LoginManager.logInWithPermissions(["public_profile", "email"])

    setTimeout(() => {
      accountStore.setTemporaryPinCodeDisabled(false)
    }, 3000)

    if (result.isCancelled) {
      console.tron.log("< SignUpScreen > facebookLogin, Login cancelled")
    } else {
      console.tron.log(
        "< SignUpScreen > facebookLogin, Login success with permissions: " +
          result.grantedPermissions.toString(),
      )
      let token: string
      try {
        const data = await AccessToken.getCurrentAccessToken()
        token = data.accessToken.toString()
        this.authenticate("facebook", {
          access_token: token,
        })
      } catch (error) {
        firebase.crashlytics().recordError(500, "Error login from Facebook:" + String(error))
        Toast.show({
          title: <Text style={styles.TOAST_TEXT}>{`페이스북 로그인 실패: ${error}`}</Text>,
          duration: 3000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        console.tron.log("< SignUpScreen > facebookLogin, Login fail with error: " + error)
        navigation.replace("SignIn")
      }
    }
  }

  signInAsync = async () => {
    const reqAccount = this.props.accountStore.reqAccount!
    reqAccount.setMode("login")
    reqAccount.validateForm()

    if (reqAccount.isValid) {
      const params: LoginParams = {
        username: reqAccount.email,
        email: reqAccount.email,
        password: reqAccount.password,
      }
      this.authenticate("email", params)
    } else if (reqAccount.emailError.length > 0) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{reqAccount.emailError}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (reqAccount.passwordError.length > 0) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{reqAccount.passwordError}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`유효한 이메일주소와 비밀번호를 입력해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  isTourButtonVisible(params: any) {
    if (params) {
      return !params.hideTourButton
    } else {
      return true
    }
  }
}
