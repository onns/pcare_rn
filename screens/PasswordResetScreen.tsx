import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { ApiResponse } from "apisauce"
import { inject, observer } from "mobx-react"
import { Container, FormControl, Input, ScrollView, Stack } from "native-base"
import React from "react"
import { Alert, TextStyle, View, ViewStyle } from "react-native"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  DetailsStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  flexDirection: "column",
  justifyContent: "space-between",
  paddingHorizontal: 16,
}

const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 16,
  paddingBottom: 32,
}

const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 32,
  color: color.text,
}
const FORM_ITEM: ViewStyle = {
  backgroundColor: color.input,
  marginBottom: 16,
  marginLeft: 16,
  marginRight: 16,
  paddingLeft: 0,
}
const LABEL: TextStyle = {
  width: 76,
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  paddingRight: 0,
}
const INPUT = {
  fontFamily: typography.bold,
  fontSize: 16,
  color: color.text,
}
const MESSAGE: TextStyle = {
  ...typography.body,
  paddingHorizontal: 16,
  color: color.text,
  alignSelf: "center",
}
const BOTTOM_BUTTON: ViewStyle = {
  flex: 1,
  flexDirection: "column",
  justifyContent: "flex-end",
  paddingVertical: 16,
}
const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
  color: color.palette.white,
}

type PasswordResetRouteProp = RouteProp<AuthStackParamList, "PasswordReset">

type PasswordResetNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "PasswordReset">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  navigation: PasswordResetNavigationProp
  route: PasswordResetRouteProp
}

interface State {
  reqAccount: any
  pending: boolean
}

@inject("accountStore")
@observer
export default class PasswordResetScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "",
  }

  constructor(props: Props) {
    super(props)
    this.props.accountStore.createRequestAccount()
    this.props.accountStore.reqAccount!.setMode("reset")
    this.state = {
      reqAccount: this.props.accountStore.reqAccount,
      pending: false,
    }
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  render() {
    return (
      <SafeAreaInsetsContext.Consumer>
        {(insets) => (
          <View style={{ ...BACKGROUND, flex: 1, paddingBottom: insets.bottom }}>
            <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
              <View>
                <View style={TITLE}>
                  <Text style={TITLE_TEXT}>비밀번호 재설정</Text>
                </View>
                <FormControl>
                  <Stack
                    inlineLabel
                    last
                    style={FORM_ITEM}
                    error={!!this.state.reqAccount.emailError}
                  >
                    <FormControl.Label style={LABEL}>이메일</FormControl.Label>
                    <Input
                      placeholder="이메일 주소"
                      placeholderTextColor={color.placeholder}
                      selectionColor={color.primary}
                      keyboardType={"email-address"}
                      onChangeText={(text) => this.state.reqAccount.emailOnChange(text)}
                      style={INPUT}
                    />
                  </Stack>
                  <Text style={MESSAGE}>
                    괜찮아요! 회원 가입시 사용한 이메일을 입력하면, 해당 이메일로 비밀번호 재설정
                    안내를 해드립니다.
                  </Text>
                </FormControl>
              </View>
              <View style={BOTTOM_BUTTON}>
                <Button block disabled={this.state.pending} onPress={() => this.submit()}>
                  <Text style={BUTTON_TEXT}>비밀번호 재설정</Text>
                </Button>
              </View>
            </ScrollView>
          </View>
        )}
      </SafeAreaInsetsContext.Consumer>
    )
  }

  submit() {
    const newAccount = this.props.accountStore.reqAccount!
    newAccount.validateForm()
    if (newAccount.isValid) {
      const params = {
        email: newAccount.email,
      }
      api.apisauce.deleteHeader("Authorization")
      this.setState({ pending: true })
      // start making calls
      api.apisauce.post(`/rest-auth/password/reset/`, params).then((response: ApiResponse<any>) => {
        this.setState({ pending: false })

        if (response.ok) {
          Alert.alert(
            "재설정 메일이 전송되었습니다.",
            "회원님의 이메일로 비밀번호 재설정 안내 메일을 보냈습니다. 이메일을 확인하셔서 안내에 따라 비밀번호를 재설정해주세요.",
            [{ text: "확인", onPress: () => this.props.navigation.goBack() }],
            { cancelable: false },
          )
        } else {
          firebase
            .crashlytics()
            .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
          console.tron.log("< PasswordResetScreen > submit, error: ", response.data)

          Alert.alert("", response.data.detail, [{ text: "확인", onPress: () => null }], {
            cancelable: false,
          })
        }
      })
    } else {
      Alert.alert(
        "Alert Title",
        "Enter Valid Email & password!",
        [
          { text: "Ask me later", onPress: () => null },
          { text: "Cancel", onPress: () => null, style: "cancel" },
          { text: "확인", onPress: () => null },
        ],
        { cancelable: false },
      )
    }
  }
}
