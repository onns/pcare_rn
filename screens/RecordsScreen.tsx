import { observer } from "mobx-react-lite"
import { Spinner } from "native-base"
import React, { useEffect, useLayoutEffect, useState } from "react"
import {
  Dimensions,
  FlatList,
  Platform,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { TabBar, TabView } from "react-native-tab-view"

import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import ArrowRight from "../assets/images/arrowRight.svg"
import BreathImage from "../assets/images/breath.svg"
import SelfTestImage from "../assets/images/selfTestGray.svg"
import TouchableItem from "../components/button/TouchableItem"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { DrugToTake } from "../stores/drug-to-take/DrugToTake"
import { PrescriptionDetailBanner } from "../stores/drug-to-take/PrescriptionDetailBanner"
import { PrescriptionDetail } from "../stores/PrescriptionStore"
import { Record } from "../stores/record/record"
import { color, typography } from "../theme"
import ReportGraphScreen from "./ReportGraphScreen"

type RecordsScreenRouteProp = RouteProp<DetailsStackParamList, "Records">

type RecordsScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "Records">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface RecordsScreenProps {
  navigation: RecordsScreenNavigationProp
  route: RecordsScreenRouteProp
}

export interface TakingItemProps {
  drugsToTake: DrugToTake
  prescription_detail: PrescriptionDetail
  banners: PrescriptionDetailBanner
}

const ROOT: ViewStyle = { backgroundColor: color.palette.white }
const HEADER_LEFT = { marginLeft: Platform.OS === "android" ? 10 : 0 }
const CENTER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
}
const ACTIVITY_REPORT_BUTTONS_WRAP: ViewStyle = {
  flexDirection: "row",
  height: 40,
  backgroundColor: color.palette.veryLightPink3,
  marginHorizontal: 24,
  marginTop: Platform.OS === "android" ? 24 : 0,
  marginBottom: 24,
  borderRadius: 20,
}
const BUTTON_ACTIVITY: ViewStyle = {
  ...CENTER,
  width: "50%",
  borderRadius: 20,
  backgroundColor: color.palette.brightOrange,
}
const BUTTON_REPORT: ViewStyle = {
  ...BUTTON_ACTIVITY,
  backgroundColor: color.palette.veryLightPink3,
}
const BUTTON_WHITE_TEXT: TextStyle = { ...typography.button, color: color.palette.white }
const BUTTON_GRAY_TEXT: TextStyle = { ...BUTTON_WHITE_TEXT, color: color.palette.grey50 }
const MARGIN_HORIZONTAL_TO_CENTER: ViewStyle = { marginHorizontal: 24 }
const ACTIVITIES_WRAP: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  paddingVertical: 24,
}
const IMAGE_TEXT_WRAP: ViewStyle = { flexDirection: "row", alignItems: "center" }
const MARGIN_RIGHT: ViewStyle = { marginRight: 14 }

const LINE: ViewStyle = { borderTopWidth: 1, borderTopColor: color.palette.gray6 }
const TAB_BAR: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingLeft: 8,
  paddingRight: 8,
  paddingBottom: 24,
  shadowOffset: { height: 0, width: 0 },
  shadowColor: "transparent",
  shadowOpacity: 0,
  elevation: 0,
}
const TAB_BAR_LABEL: ViewStyle = {
  height: 36,
  borderRadius: 18,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  paddingHorizontal: 16,
  marginLeft: 16,
}
const TAB_BAR_LABEL_TEXT: TextStyle = {
  fontSize: 14,
  lineHeight: 22,
}
const TAB: ViewStyle = {
  width: "auto",
  height: 42,
  padding: 0,
  margin: 0,
}
const SCENE = { flex: 1, borderRadius: 20 }
const INDICATOR: ViewStyle = {
  backgroundColor: color.palette.white,
}
const SPINNER: ViewStyle = { flex: 1, alignSelf: "center" }

export const RecordsScreen: React.FunctionComponent<RecordsScreenProps> = observer((props) => {
  const { recordStore } = useStores()
  const { records } = recordStore
  const { navigation } = props
  const [index, setIndex] = React.useState(0)
  const [routes, setRoutes] = React.useState([{ key: "", name: "", title: "" }])
  const [checkButton, setCheckButton] = useState("활동")
  const initialLayout = { width: Dimensions.get("window").width }

  useLayoutEffect(() => {
    /* 22.04.20 노바티스 서비스종료 */
    navigation.setOptions({
      // eslint-disable-next-line react/display-name
      headerLeft: () => (
        <TouchableItem
          onPress={() => {
            recordStore.setSeletedTab("")
            recordStore.setSeletedTabNumber(0)
            recordStore.setGraphDate("daily")
            navigation.goBack()
          }}
          style={HEADER_LEFT}
        >
          <CustomHeaderBackImage />
        </TouchableItem>
      ),
      headerTitle: "호흡기 질환 자가 검사",
      headerStyle: { elevation: 1 },
    })
    /**/
  }, [navigation])

  useEffect(() => {
    setRoutes(recordStore.recordRoutes)
  }, [])

  /**
   * recordRoutes 에 applySnapshot 수행될 때 routes 를 업데이트함.
   * */
  // useEffect(() => {
  //   const snapshotDisposer = onSnapshot(recordStore.recordRoutes, () => {
  //     setRoutes(recordStore.recordRoutes)
  //   })
  //   return () => snapshotDisposer()
  // }, [recordStore.status === "done"])

  const renderTabView = () => {
    if (recordStore.seletedTab === "") {
      const sort = routes.filter((item) => item.name === "CAT")[0]
      recordStore.setSeletedTab(sort.name)
      recordStore.setSeletedTabNumber(sort.name)
    }
    return (
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={(index: number) => setIndex(index)}
        initialLayout={initialLayout}
        swipeEnabled={false}
      />
    )
  }

  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      indicatorStyle={INDICATOR}
      indicatorContainerStyle={{}}
      style={TAB_BAR}
      dynamicWidth
      scrollEnabled
      tabStyle={TAB}
      onTabPress={({ route }) => {
        {
          // @react-navigation/bottom-tabs 모듈 정의에 name이 없어서 발생하는 오류임.
          recordStore.setSeletedTab(route.name)
          recordStore.setSeletedTabNumber(route.name)
        }
      }}
      renderLabel={({ route, focused, color }) => (
        <View
          style={{
            ...TAB_BAR_LABEL,
            backgroundColor: focused ? "#303550" : "#F4F6FA",
          }}
        >
          <Text
            style={{
              ...TAB_BAR_LABEL_TEXT,
              color,
              // fontFamily: focused ? typography.bold : typography.primary,
              fontWeight: focused ? "bold" : "normal",
            }}
          >
            {route.title}
          </Text>
        </View>
      )}
      activeColor={color.palette.white}
      inactiveColor={`${color.text}4D`}
    />
  )

  const renderScene = ({ route }) => {
    return (
      <View style={SCENE}>
        <ReportGraphScreen route={route} navigation={navigation} />
      </View>
    )
  }

  const renderItem = ({ item }: { item: Record }) => {
    return (
      <View>
        <TouchableOpacity
          style={ACTIVITIES_WRAP}
          onPress={async () => {
            switch (item.type) {
              case "SURVEY":
                navigation.navigate("Details", {
                  screen: "SurveyIntro",
                  params: { recordId: item.id },
                })
                break
              case "VIDEO":
                await recordStore.fetchVideo(item.id)
                if (recordStore.status === "done" && recordStore.recordVideo) {
                  navigation.navigate("Details", {
                    screen: "Activity",
                    params: { title: "운동", recordId: item.id },
                  })
                  break
                }

              default:
                break
            }
          }}
        >
          <View style={IMAGE_TEXT_WRAP}>
            {item.type === "VIDEO" ? (
              <BreathImage style={MARGIN_RIGHT} />
            ) : (
              <SelfTestImage style={MARGIN_RIGHT} />
            )}

            <Text>{item.title}</Text>
          </View>
          <ArrowRight />
        </TouchableOpacity>
        <View style={LINE}></View>
      </View>
    )
  }
  return (
    <Screen style={ROOT} preset="fixed">
      <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
      {recordStore.isLoading ? (
        <Spinner color={color.primary} style={SPINNER} />
      ) : (
        <View style={{ flex: 1 }}>
          <View style={ACTIVITY_REPORT_BUTTONS_WRAP}>
            <TouchableOpacity
              style={checkButton === "활동" ? BUTTON_ACTIVITY : BUTTON_REPORT}
              onPress={() => setCheckButton("활동")}
            >
              <Text style={checkButton === "활동" ? BUTTON_WHITE_TEXT : BUTTON_GRAY_TEXT}>
                활동
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={checkButton === "리포트" ? BUTTON_ACTIVITY : BUTTON_REPORT}
              onPress={() => setCheckButton("리포트")}
            >
              <Text style={checkButton === "리포트" ? BUTTON_WHITE_TEXT : BUTTON_GRAY_TEXT}>
                리포트
              </Text>
            </TouchableOpacity>
          </View>
          {checkButton === "활동" ? (
            <View style={MARGIN_HORIZONTAL_TO_CENTER}>
              <FlatList
                data={records}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          ) : (
            renderTabView()
          )}
        </View>
      )}
    </Screen>
  )
})
