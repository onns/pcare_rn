import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import * as NavigationBar from "expo-navigation-bar"
import LottieView from "lottie-react-native"
import { inject, observer } from "mobx-react"
import { ScrollView, View } from "native-base"
import React from "react"
import {
  BackHandler,
  Dimensions,
  Image,
  ImageSourcePropType,
  ImageStyle,
  PermissionsAndroid,
  Platform,
  StatusBar,
  StyleSheet,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import appsFlyer from "react-native-appsflyer"
import { RNCamera, TakePictureOptions } from "react-native-camera"
import DeviceInfo from "react-native-device-info"
import RNFS from "react-native-fs"
import { launchImageLibrary } from "react-native-image-picker"
import Modal from "react-native-modal"
import { check, openSettings, PERMISSIONS, request, RESULTS } from "react-native-permissions"
import { Circle, XStack, YStack } from "tamagui"

import { api } from "@api/api"
import SvgGetPoint from "@assets/images/get-point.svg"
import { SvgArrowRight } from "@components/svg/SvgArrowRight"
import AsyncStorage from "@react-native-async-storage/async-storage"
import { CameraRoll } from "@react-native-camera-roll/camera-roll"
import firebase from "@react-native-firebase/app"
import remoteConfig from "@react-native-firebase/remote-config"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { withNavigationFocus } from "@react-navigation/compat"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgCapture from "../assets/images/capture.svg"
import SvgCheckboxChecked from "../assets/images/checkboxChecked.svg"
import SvgCheckboxDisabled from "../assets/images/checkboxDisabled.svg"
import SvgGallery from "../assets/images/gallery.svg"
import SvgLock from "../assets/images/lock.svg"
import { BackgroundImage, FastImage } from "../components/BackgroundImage"
import TouchableItem from "../components/button/TouchableItem"
import { getPopupButton, Popup } from "../components/Popup"
import { Text } from "../components/StyledText"
import { loadString, saveString } from "../lib/storage"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const ROOT: ViewStyle = {
  backgroundColor: "rgb(52, 52, 52)",
  flex: 1,
}
const CONTENT: ViewStyle = {
  flex: 1,
  backgroundColor: "rgb(52, 52, 52)",
  justifyContent: "center",
  alignItems: "center",
}
const BUTTON_TEXT: TextStyle = {
  ...typography.button,
  color: color.palette.white,
}
const BUTTONS: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "space-around",
  alignItems: "center",
  paddingHorizontal: 8,
}

const productionWebsocketUrl = "wss://prod.onionsapp.com/ws/ocr/"

const options = {
  title: "Select Avatar",
  customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images",
  },
}

const flashModeOrder = {
  off: "on",
  on: "auto",
  auto: "torch",
  torch: "off",
}

const wbOrder = {
  auto: "sunny",
  sunny: "cloudy",
  cloudy: "shadow",
  shadow: "fluorescent",
  fluorescent: "incandescent",
  incandescent: "auto",
}

const deviceWidth = Dimensions.get("window").width
const deviceHeight = Dimensions.get("window").height
const lottieJson = require("../assets/spinner_orange_paprica_animation.json")
const CHECKBOX: ImageStyle = {
  marginRight: 9,
}

const BACKGROUND_IMAGE: ViewStyle = {
  // flex: 1,
  // maxWidth: 234,
  height: deviceHeight > deviceWidth ? deviceWidth * 1.414 : 355,
  width: deviceWidth,
  // maxHeight: (deviceWidth - 10) * 1.414,
  // width: auto,
  // height: auto,
  // height: 151.2,
  // borderRadius: 10,
  // paddingVertical: 10,
  // paddingHorizontal: 25,
  // resizeMode: 'contain'
  // backgroundColor: 'rgb(153, 153, 153)'
}
const CAPTURE_GUIDE_BOX: ViewStyle = {
  backgroundColor: "rgba(0, 0, 0, 0.5)",
  paddingTop: 24,
  paddingHorizontal: 16,
  paddingBottom: 16,
  borderRadius: 8,
}
const ROW: ViewStyle = { flexDirection: "row" }
const BUTTON: ViewStyle = {
  width: 94,
  height: 42,
  alignItems: "center",
  alignSelf: "center",
  justifyContent: "center",
}
const CAPTURE_BUTTON: ViewStyle = {
  width: 94,
  height: 94,
  borderRadius: 47,
  justifyContent: "center",
  paddingTop: 10,
}
const GALLERY: ViewStyle = { marginRight: 8 }

type PrescriptionScannerRouteProp = RouteProp<DetailsStackParamList, "PrescriptionScanner">

type PrescriptionScannerNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "PrescriptionScanner">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

interface Props {
  accountStore: AccountStore
  isFocused: boolean
  navigation: PrescriptionScannerNavigationProp
  route: PrescriptionScannerRouteProp
}

interface State {
  fileName: string | undefined
  uri: string
  base64: string
  ocrProcessing: boolean
  flash: string
  zoom: number
  autoFocus: string
  depth: number
  type: string
  whiteBalance: string
  ratio: string
  ratios: any
  photoId: number
  showGallery: boolean
  photos: any
  faces: any
  recordOptions: any
  isRecording: boolean
  cameraPaused: boolean
  captureDisabled: boolean
  popupVisible: boolean
  popupImageSource: ImageSourcePropType | undefined
  popupTitle: string
  popupMessage: string
  popupButton1: React.ReactElement | undefined
  popupButton2: React.ReactElement | undefined
  popupButton3: React.ReactElement | undefined
  popupButtonGroupStyle: ViewStyle | undefined

  wsBaseUrl: string
  doNotDisplayCaptureGuide: boolean
  captureGuideModalVisible: boolean
}

const CAPTURE_GUIDE_NUMBER: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 17,
  lineHeight: 25,
  color: color.palette.white,
}
const CAPTURE_GUIDE_TITLE: TextStyle = {
  ...typography.title2,
  color: color.palette.white,
  textAlign: "center",
  marginBottom: 16,
}
const CAPTURE_GUIDE_MESSAGE: TextStyle = {
  ...typography.body,
  color: color.palette.white,
  marginLeft: 8,
}
const CAPTURE_GUIDE_MESSAGE_SMALL: TextStyle = {
  ...typography.sub2,
  color: color.palette.white,
}
const CAPTURE_GUIDE_CONFIRM_BUTTON: ViewStyle = {
  height: 42,
  borderRadius: 20,
  borderWidth: 1,
  borderColor: color.palette.white,
  justifyContent: "center",
  alignItems: "center",
}
const CAPTURE_GUIDE_CONFIRM_TEXT = {
  ...typography.body,
  color: color.palette.white,
}
const CAPTURE_GUIDE_CHECKBOX_ROW: ViewStyle = {
  flexDirection: "row",
  marginBottom: 24,
  justifyContent: "flex-end",
  alignItems: "center",
}
const hitSlop10 = {
  top: 10,
  left: 10,
  bottom: 10,
  right: 10,
}
const CAPTURE_GUIDE_IMAGE: ImageStyle = {
  width: 375,
  height: 232,
  resizeMode: "contain",
  marginBottom: 16,
  alignSelf: "center",
}

@inject("accountStore")
@observer
class PrescriptionScannerScreen extends React.Component<Props, State> {
  camera: RNCamera | undefined
  // @ts-ignore
  socket: WebSocket | undefined
  isSocketOpened = false
  timerHandler: any
  _unsubscribeFocus: () => void
  _unsubscribeBlur: () => void

  constructor(props: Props) {
    super(props)
    this.state = {
      fileName: undefined,
      uri: "",
      base64: "",
      ocrProcessing: false,
      flash: "off",
      zoom: 0,
      autoFocus: "on",
      depth: 0,
      type: "back",
      whiteBalance: "auto",
      ratio: "16:9",
      ratios: [],
      photoId: 1,
      showGallery: true,
      photos: [],
      faces: [],
      recordOptions: {
        mute: false,
        maxDuration: 5,
        quality: RNCamera.Constants.VideoQuality["288p"],
      },
      isRecording: false,
      cameraPaused: false,
      captureDisabled: false,
      popupVisible: false,
      popupImageSource: undefined,
      popupTitle: "",
      popupMessage: "",
      popupButton1: undefined,
      popupButton2: undefined,
      popupButton3: undefined,
      popupButtonGroupStyle: undefined,
      wsBaseUrl: productionWebsocketUrl,
      doNotDisplayCaptureGuide: false,
      captureGuideModalVisible: false,
    }
  }

  getRePhotoIndex(params: any): any {
    return params.rePhotoIndex
  }

  getRePhotoId(params: any): any {
    return params.rePhotoId
  }

  // TODO: 처방전, 약봉투 샘플 image 전달받은 후 해제 예정
  async componentWillMount() {}

  componentWillUnmount() {
    if (this.timerHandler) {
      clearTimeout(this.timerHandler)
      this.timerHandler = undefined
    }
    if (this.socket) {
      this.socket.close()
      this.socket = null
    }
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
    this._unsubscribeFocus()
    this._unsubscribeBlur()
  }

  handleBackPress = () => {
    if (this.timerHandler) {
      clearTimeout(this.timerHandler)
      this.timerHandler = undefined
    }
    if (this.socket) {
      this.socket.close()
      this.socket = null
      console.tron.log("< PrescriptionScannerScreen > handleBackPress, closing socket")
    }
    if (this.state.ocrProcessing) {
      this.setState({ ocrProcessing: false })
    }
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
    this.props.navigation.navigate("HomeStack")
    return true
  }

  componentDidMount() {
    const { navigation } = this.props
    navigation.setParams({ handleBackPress: this.handleBackPress })
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)
    this._unsubscribeBlur = navigation.addListener("blur", this.didBlur)
  }

  askPermission = async () => {
    try {
      const iosCheck = await check(PERMISSIONS.IOS.CAMERA)
      const androidCheck = await check(PERMISSIONS.ANDROID.CAMERA)

      if (Platform.OS === "ios") {
        if (iosCheck === RESULTS.GRANTED) {
          await request(PERMISSIONS.IOS.CAMERA)
        } else {
          await openSettings()
        }
      } else {
        if (androidCheck === RESULTS.GRANTED) {
          await request(PERMISSIONS.ANDROID.CAMERA)
        } else {
          await openSettings()
          // 안드로이드의 경우 카메라 권한설정을 하여도
          // 화면에 바로 적용되는 것이 아니라 권한허용 하라는 메시지가 계속 View에 그대로 존재하므로
          // 이전화면으로 되돌아 갔다가 다시 화면에 진입
          this.props.navigation.pop()
          this.props.navigation.navigate("Details", { screen: "PrescriptionScanner" })
        }
      }
    } catch (error) {
      console.tron.log("< PrescriptionScannerScreen > askPermission, error:", error)
    }
  }

  didFocus = async () => {
    const { accountStore, navigation, route } = this.props
    const { user, environment } = accountStore
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })

    this.initiate()

    NavigationBar.setBackgroundColorAsync("#343434")

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress)

    // 둘러보기 이용중
    if (!api.apisauce.headers.Authorization) {
      this.setState({
        popupVisible: true,
        popupImageSource: null,
        popupTitle: `회원가입 및 본인인증이${"\n"}필요한 서비스입니다`,
        popupMessage:
          "회원가입 및 본인인증을 하시면 처방내용 분석 및 사용자 맞춤 의료 정보를 제공합니다. 서비스 사용을 위해 회원가입 및 본인인증을 해주세요.",
        popupButton1: getPopupButton(
          () => {
            navigation.navigate("HomeStack")
          },
          "취소",
          "cancel",
        ),
        popupButton2: getPopupButton(() => {
          this.initiate()
          navigation.navigate("Auth", {
            screen: "SignUp",
            params: { hideTourButton: true, prevScreen: "AddPrescriptionStack" },
          })
        }, "회원가입 및 본인인증"),
        popupButton3: undefined,
        popupButtonGroupStyle: undefined,
      })
    } else if (!user.is_authenticated) {
      this.setState({
        popupVisible: true,
        popupImageSource: null,
        popupTitle: `본인인증이 필요한 서비스입니다`,
        popupMessage: `본인인증을 하시면 처방내용 분석 및 사용자 맞춤 의료 정보를 제공합니다. 서비스 사용을 위해 본인인증을 해주세요.`,
        popupButton1: getPopupButton(
          () => {
            navigation.navigate("HomeStack")
          },
          "취소",
          "cancel",
        ),
        popupButton2: getPopupButton(() => {
          this.initiate()
          navigation.navigate("Certification", {
            previousScreen: "PrescriptionScanner",
          })
        }, "본인인증"),
        popupButton3: undefined,
        popupButtonGroupStyle: undefined,
      })
    } else {
      this.checkCaptureGuide()
      await this.fetchDeveloperMode()
      this.connectToOcrService()
    }
  }

  didBlur = () => {
    NavigationBar.setBackgroundColorAsync("#efefef")
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)

    if (this.timerHandler) {
      clearTimeout(this.timerHandler)
      this.timerHandler = undefined
    }

    if (this.socket) {
      this.socket.close()
      this.socket = null
      console.tron.log("< PrescriptionScannerScreen > didBlur, closing socket..")
    }
  }

  private async checkCaptureGuide() {
    const doNotDisplayCaptureGuide = await loadString("doNotDisplayCaptureGuide")
    if (doNotDisplayCaptureGuide === "true") {
      const captureGuideModalVisible = !JSON.parse(doNotDisplayCaptureGuide)
      this.setState({
        captureGuideModalVisible: captureGuideModalVisible,
      })
    } else {
      this.setState({
        captureGuideModalVisible: true,
      })
    }
  }

  connectToOcrService() {
    if (this.socket == undefined || this.socket == null) {
      this.socket = new WebSocket(this.state.wsBaseUrl)
    }
    this.socket.onopen = () => {
      console.tron.log("< PrescriptionScannerScreen > connectToOcrService, onopen called")
      this.isSocketOpened = true
      // this.socket.send(JSON.stringify({ type: 'greet', payload: 'Hello Mr. Server!' }))
    }
    this.socket.onmessage = ({ data }: { data: any }) => {
      console.tron.log("< PrescriptionScannerScreen > connectToOcrService, onmessage called")
      this.setState({ ocrProcessing: false })
      if (this.timerHandler) {
        clearTimeout(this.timerHandler)
        this.timerHandler = undefined
      }
      const jsonData = JSON.parse(data)

      if (
        jsonData.img_src == "#" ||
        ((jsonData.message.hospital == "-" || jsonData.message.hospital.length == 0) &&
          jsonData.message.diseases.length == 0 &&
          jsonData.message.drugs.length == 0)
      ) {
        this.setState({
          popupVisible: true,
          popupImageSource: require("../assets/images/popupPrescriptionUnrecognizable.png"),
          popupTitle: "처방전을 인식할 수 없습니다",
          popupMessage:
            "처방전 전체를 화면 안에 위치하여 촬영해 주세요. 처방전 외의 사진은 인식하지 않습니다.",
          popupButton1: getPopupButton(() => {
            this.setState({ popupVisible: false })
            this.initiate()
          }, "확인"),
          popupButton2: undefined,
          popupButton3: undefined,
          popupButtonGroupStyle: undefined,
        })
      } else {
        this.previewPrescription(jsonData)
      }
    }
    this.socket.onerror = (e) => {
      console.tron.log("< PrescriptionScannerScreen > connectToOcrService, error: ", e)
      this.isSocketOpened = false
    }
  }

  initiate() {
    if (this.timerHandler) {
      clearTimeout(this.timerHandler)
      this.timerHandler = undefined
    }
    this.setState({
      ocrProcessing: false,
      uri: "",
      popupVisible: false,
    })
  }

  previewPrescription(jsonData: any) {
    const base64Code = jsonData.img_src.split("data:image/png;base64,")
    const finalUri =
      this.state.fileName !== undefined
        ? `file:///${RNFS.CachesDirectoryPath}/${this.state.fileName}`
        : this.state.uri
    RNFS.writeFile(finalUri, base64Code[1], "base64")
      .then(async () => {
        console.tron.log(
          `< PrescriptionScannerScreen > previewPrescription, onSaveEvent success${RNFS.CachesDirectoryPath}`,
        )
        this.props.navigation.navigate("PrescriptionPreview", {
          uri: finalUri,
          type: "image/png",
          ocrMessage: jsonData.message,
          rePhotoIndex: this.getRePhotoIndex(this.props.route.params),
          rePhotoId: this.getRePhotoId(this.props.route.params),
        })
      })
      .catch((error) => {
        console.tron.log(
          "< PrescriptionScannerScreen > previewPrescription, onSaveEvent error",
          error,
        )
        this.props.navigation.navigate("PrescriptionPreview", {
          uri: this.state.uri,
          ocrMessage: jsonData.message,
          rePhotoIndex: this.getRePhotoIndex(this.props.route.params),
          rePhotoId: this.getRePhotoId(this.props.route.params),
        })
      })
  }

  // getRatios = async function() {
  //   const ratios = await this.camera.getSupportedRatios();
  //   return ratios;
  // };

  toggleView() {
    this.setState({
      showGallery: !this.state.showGallery,
    })
  }

  toggleFacing() {
    this.setState({
      type: this.state.type === "back" ? "front" : "back",
    })
  }

  toggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash],
    })
  }

  setRatio(ratio: string) {
    this.setState({
      ratio,
    })
  }

  toggleWB() {
    this.setState({
      whiteBalance: wbOrder[this.state.whiteBalance],
    })
  }

  toggleFocus() {
    this.setState({
      autoFocus: this.state.autoFocus === "on" ? "off" : "on",
    })
  }

  zoomOut() {
    this.setState({
      zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1,
    })
  }

  zoomIn() {
    this.setState({
      zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1,
    })
  }

  setFocusDepth(depth) {
    this.setState({
      depth,
    })
  }

  render() {
    if (this.state.ocrProcessing) {
      return (
        <View style={ROOT}>
          <StatusBar hidden />
          <ScrollView contentContainerStyle={CONTENT}>
            <BackgroundImage
              resizeMode={FastImage.resizeMode.contain}
              image={BACKGROUND_IMAGE}
              source={{ uri: this.state.uri }}
            />
            {this.renderWaitingPopup()}
            <Popup
              isVisible={this.state.popupVisible}
              imageSource={this.state.popupImageSource}
              title={this.state.popupTitle}
              message={this.state.popupMessage}
              button1={this.state.popupButton1}
              button2={this.state.popupButton2}
              button3={this.state.popupButton3}
              buttonGroupStyle={this.state.popupButtonGroupStyle}
            />
          </ScrollView>
        </View>
      )
    } else {
      return this.renderCamera()
    }
  }

  /** 어떤 DeveloperMode인지 확인 후 OCR WS 연결 */
  async fetchDeveloperMode() {
    const developerMode = await AsyncStorage.getItem("developerMode")
    if (developerMode) {
      const mode = Number.parseInt(developerMode)

      const regExp = /^[a-zA-Z]{3,5}\:/
      let wsBaseUrl = api.apisauce.getBaseURL()
      if (mode === 0 || mode === 1) {
        wsBaseUrl = wsBaseUrl.replace(regExp, "wss:")
      } else {
        wsBaseUrl = wsBaseUrl.replace(regExp, "ws:")
      }

      this.setState({
        wsBaseUrl: wsBaseUrl.concat("ws/ocr/"),
      })
    }
  }

  closeCaptureGuide = async () => {
    await saveString(
      "doNotDisplayCaptureGuide",
      JSON.stringify(this.state.doNotDisplayCaptureGuide),
    )
    this.setState({ captureGuideModalVisible: false })
  }

  renderWaitingPopup() {
    return (
      <View
        style={{
          flex: 1,
          width: "100%",
          backgroundColor: "rgba(52, 52, 52, 0.7)",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <LottieView
          style={{ width: 100, marginBottom: 22 }}
          source={lottieJson}
          // source={require("../assets/data.json")}
          // imageAssetsFolder={"lottie/spinner"}
          autoPlay
          loop
        />
        <Text style={{ color: color.palette.offWhite }}>처방전을 분석하고 있습니다.</Text>
        <Text style={{ color: color.palette.offWhite }}>잠시만 기다려주세요.</Text>
      </View>
    )
  }

  renderCamera() {
    const width = Dimensions.get("window").width
    const height = width * 1.414
    const deviceHeight = Dimensions.get("window").height
    const foldableDevice = ["SM-F907N", "SM-F916N", "SM-F926N"] // 좌측부터 갤럭시 폴드1,2,3
    const isFoldable = foldableDevice.includes(DeviceInfo.getModel())

    const {
      captureGuideModalVisible,
      doNotDisplayCaptureGuide,
      captureDisabled,
      popupVisible,
      popupImageSource,
      popupTitle,
      popupMessage,
      popupButton1,
      popupButton2,
      popupButton3,
      popupButtonGroupStyle,
      uri,
    } = this.state

    if (!this.props.isFocused) {
      return <View />
    } else {
      return (
        <View style={styles.container}>
          <StatusBar hidden />
          <RNCamera
            ref={(ref: RNCamera) => {
              this.camera = ref
            }}
            style={[
              styles.preview,
              isFoldable && { flex: 3 },
              { width: width, height: height, justifyContent: "flex-end" },
            ]}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.off}
            captureAudio={false}
            androidCameraPermissionOptions={{
              title: "카메라 권한 요청",
              message: "처방전 사진을 촬영하려면 카메라 접근 권한이 필요합니다.",
              buttonPositive: "확인",
              // buttonNegative: 'Cancel',
            }}
            notAuthorizedView={
              <TouchableOpacity
                style={[
                  styles.preview,
                  {
                    width: width,
                    height: height,
                    justifyContent: "center",
                  },
                ]}
                onPress={() => this.askPermission()}
              >
                <Text>
                  {
                    "카메라 접근 권한이 없습니다.\n\n카메라 설정 권한을 변경하시려면\n\n화면을 터치하세요."
                  }
                </Text>
              </TouchableOpacity>
            }
            playSoundOnCapture={true}
            onMountError={(e) => {
              console.tron.log("< PrescriptionScannerScreen > RNCamera, onMountError: ", e.message)
            }}
            onCameraReady={() => {}}
          />
          {/* <View style={{ backgroundColor: "#000" }}>
          <Text style={{ color: "white" }}>{family[0].name}의 처방전으로 입력 요청합니다.</Text>
        </View> */}

          <View style={BUTTONS}>
            <TouchableItem
              onPress={async () => {
                this.props.navigation.navigate("HomeStack")
                this.checkCaptureGuide()
              }}
              style={BUTTON}
            >
              <Text style={BUTTON_TEXT}>취소</Text>
            </TouchableItem>
            <TouchableItem
              onPress={this.takePicture.bind(this)}
              style={CAPTURE_BUTTON}
              disabled={uri.length > 0 || captureDisabled}
            >
              <SvgCapture width={94} height={94} />
            </TouchableItem>
            <TouchableItem onPress={this.bringImageFromGallery.bind(this)} style={BUTTON}>
              <View style={ROW}>
                <SvgGallery width={24} height={24} style={GALLERY} />
                <Text style={BUTTON_TEXT}>사진첩</Text>
              </View>
            </TouchableItem>
          </View>
          <Popup
            isVisible={popupVisible}
            imageSource={popupImageSource}
            title={popupTitle}
            message={popupMessage}
            button1={popupButton1}
            button2={popupButton2}
            button3={popupButton3}
            buttonGroupStyle={popupButtonGroupStyle}
          />
          {
            // TODO: ios 용 콤포넌트와 함께 리팩토링
            Platform.OS === "android" ? (
              <Modal
                deviceWidth={deviceWidth}
                deviceHeight={deviceHeight}
                isVisible={captureGuideModalVisible}
                animationIn={"slideInUp"}
                animationOut={"fadeOutDown"}
                onBackButtonPress={this.closeCaptureGuide}
              >
                <View style={CAPTURE_GUIDE_BOX}>
                  <Text style={CAPTURE_GUIDE_TITLE} adjustsFontSizeToFit={false}>
                    처방전 촬영 가이드
                  </Text>
                  <Image
                    source={require("../assets/images/captureGuide.png")}
                    style={CAPTURE_GUIDE_IMAGE}
                  />
                  <View style={{ flexDirection: "row", marginBottom: 8 }}>
                    <Text style={CAPTURE_GUIDE_NUMBER} adjustsFontSizeToFit={false}>
                      1.
                    </Text>
                    <Text style={CAPTURE_GUIDE_MESSAGE} adjustsFontSizeToFit={false}>
                      처방전 또는 약봉투의 전체가 보이도록 찍어주세요.
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", marginBottom: 16 }}>
                    <Text style={CAPTURE_GUIDE_NUMBER} adjustsFontSizeToFit={false}>
                      2.
                    </Text>
                    <Text style={CAPTURE_GUIDE_MESSAGE} adjustsFontSizeToFit={false}>
                      한번에 한장만 찍어 주세요.
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", marginBottom: 16 }}>
                    <SvgLock width={16} height={16} style={{ marginRight: 4, paddingTop: 4 }} />
                    <YStack>
                      <Text fow="400" fos={15} lh={18} ls={-0.3} col="white" mb={8}>
                        안심하고 찍으세요!
                      </Text>
                      <Text fow="400" fos={14} lh={20} ls={-0.3} col="rgba(255, 255, 255, 0.7)">
                        개인정보는 자동으로 삭제되며 저장하지 않습니다. 안심하고 사용하세요.
                      </Text>
                    </YStack>
                  </View>
                  {remoteConfig().getBoolean("points_enabled") && (
                    <XStack
                      w="100%"
                      h={32}
                      my={16}
                      ai="center"
                      onPress={() => {
                        this.props.navigation.navigate("PointGuide")
                      }}
                    >
                      <XStack f={1} ai="center">
                        <Circle size={32}>
                          <SvgGetPoint width={24} height={24} />
                        </Circle>
                        <Text style={CAPTURE_GUIDE_MESSAGE}>처방전 등록 포인트 혜택 안내</Text>
                      </XStack>
                      <SvgArrowRight />
                    </XStack>
                  )}
                  <View style={CAPTURE_GUIDE_CHECKBOX_ROW}>
                    <TouchableOpacity
                      onPress={async () =>
                        this.setState({
                          doNotDisplayCaptureGuide: !doNotDisplayCaptureGuide,
                        })
                      }
                      hitSlop={hitSlop10}
                    >
                      {doNotDisplayCaptureGuide ? (
                        <SvgCheckboxChecked width={22} height={22} style={CHECKBOX} />
                      ) : (
                        <SvgCheckboxDisabled width={22} height={22} style={CHECKBOX} />
                      )}
                    </TouchableOpacity>
                    <Text style={CAPTURE_GUIDE_MESSAGE_SMALL} adjustsFontSizeToFit={false}>
                      다시 보지 않기
                    </Text>
                  </View>
                  <TouchableItem
                    style={CAPTURE_GUIDE_CONFIRM_BUTTON}
                    onPress={this.closeCaptureGuide}
                  >
                    <Text style={CAPTURE_GUIDE_CONFIRM_TEXT} adjustsFontSizeToFit={false}>
                      {remoteConfig().getBoolean("points_enabled")
                        ? "처방전 촬영 등록 50원"
                        : "확인"}
                    </Text>
                  </TouchableItem>
                </View>
              </Modal>
            ) : null
          }
        </View>
      )
    }
  }

  bringImageFromGallery = () => {
    this.props.accountStore.setTemporaryPinCodeDisabled(true)
    launchImageLibrary({ mediaType: "photo", quality: 0.8, includeBase64: true }, (response) => {
      firebase.analytics().logEvent("image_gallery_click")
      amplitude.getInstance().logEvent("image_gallery_click")
      appsFlyer.logEvent("af_image_gallery_click", {})

      setTimeout(() => {
        this.props.accountStore.setTemporaryPinCodeDisabled(false)
      }, 3000)

      if (response.didCancel) {
        console.tron.log(
          "< PrescriptionScannerScreen > bringImageFromGallery, User cancelled image picker",
        )
      } else if (response.errorMessage) {
        console.tron.log(
          "< PrescriptionScannerScreen > bringImageFromGallery, ImagePicker errorMessage: ",
          response.errorMessage,
        )
      } else {
        this.setState({ uri: response.assets[0].uri, fileName: response.assets[0].fileName })
        if (this.isSocketOpened) {
          this.processImage(response.assets[0].base64, response.assets[0].type)
        } else {
          const source = {
            uri: response.assets[0].uri,
            type: response.assets[0].type,
            name: response.assets[0].fileName,
          }
          this.props.navigation.navigate("PrescriptionPreview", source)
        }
      }
    })
  }

  takePicture = async () => {
    if (this.camera) {
      this.setState({ captureDisabled: true })

      const options: TakePictureOptions = {
        width: 2480,
        quality: 0.8,
        base64: true,
        fixOrientation: true,
        pauseAfterCapture: true,
        orientation: "portrait",
      }

      const photo = await this.camera.takePictureAsync(options)

      if (Platform.OS === "android") {
        const granted = await PermissionsAndroid.check(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        )
        if (granted) {
          CameraRoll.save(photo.uri)
        } else {
          this.props.accountStore.setTemporaryPinCodeDisabled(true)
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: "사진 저장 권한 요청",
              message:
                "파프리카케어는 촬영한 처방전 사진을 사용자의 폰 갤러리에 저장하기 위해 이와 관련한 권한이 필요합니다.",
              // buttonNeutral: 'Ask Me Later',
              buttonNegative: "취소",
              buttonPositive: "확인",
            },
          )
          this.props.accountStore.setTemporaryPinCodeDisabled(false)
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            CameraRoll.save(photo.uri)
          } else {
            console.tron.log("< PrescriptionScannerScreen > takePicture, Camera permission denied")
          }
        }
      } else {
        this.props.accountStore.setTemporaryPinCodeDisabled(true)
        try {
          CameraRoll.save(photo.uri)
        } catch (e) {
          console.tron.log("< PrescriptionScannerScreen > takePicture, error: ", e)
        }
        this.props.accountStore.setTemporaryPinCodeDisabled(false)
      }

      this.setState({ uri: photo.uri, base64: photo.base64, cameraPaused: true })
      this.camera.setState({ isFocused: true })

      if (photo.base64 && this.isSocketOpened) {
        this.processImage(photo.base64)
      } else {
        this.props.navigation.navigate("PrescriptionPreview", { uri: photo.uri })
      }
      firebase.analytics().logEvent("camera_capture_click")
      amplitude.getInstance().logEvent("camera_capture_click")
      appsFlyer.logEvent("af_camera_capture_click", {})
      this.setState({ captureDisabled: false })
    }
  }

  processImage(base64Image: string, type?: string) {
    this.setState({ ocrProcessing: true })

    const data_url = `data:${type || "image/jpeg"};base64,${base64Image}`
    const attr = {
      is_privacy: true,
      is_num: false,
      is_char: false,
      img_src: data_url,
      is_drug: true,
      is_disease: true,
      is_hosp: true,
      is_test: false,
      user_id: this.props.accountStore.user.id,
    }

    this.socket.send(
      JSON.stringify({
        message: "message",
        attr: attr,
      }),
    )
    this.timerHandler = setTimeout(() => {
      this.setState({
        popupVisible: true,
        popupImageSource: require("../assets/images/popupPrescriptionUnrecognizable.png"),
        popupTitle: "처방전을 인식할 수 없습니다",
        popupMessage:
          "처방전 상단부분이 화면 위로 오도록 처방전 전체를 노란선 안쪽에 위치하여 촬영해 주세요. 처방전 외의 사진은 인식하지 않습니다.",
        popupButton1: getPopupButton(() => {
          this.setState({ popupVisible: false })
          // @ts-ignore
          this.props.navigation.replace("PrescriptionScanner")
        }, "확인"),
        popupButton2: undefined,
        popupButton3: undefined,
        popupButtonGroupStyle: undefined,
      })
      // if (this.state.ocrProcessing) {
      //   this.props.navigation.navigate("PrescriptionPreview", { uri: this.state.uri })
      // }
    }, 60000)
  }
}

const styles = StyleSheet.create({
  // eslint-disable-next-line react-native/no-color-literals
  container: {
    backgroundColor: "rgb(52, 52, 52)",
    flex: 1,
    flexDirection: "column",
  },
  preview: {
    alignItems: "center",
    // flex: 3,
    justifyContent: "flex-end",
    // marginHorizontal: 10,
  },
})

// 다른 페이지 이동 후 다시 카메라 화면 돌아올 시 블랙화면(Android) 현상 발생 -> withNavigationFocus 이용하여 해결함
export default withNavigationFocus(PrescriptionScannerScreen)
