import * as NavigationBar from "expo-navigation-bar"
import { inject, observer } from "mobx-react"
import { View } from "native-base"
import React, { Component } from "react"
import {
  AppState,
  Image,
  NativeEventSubscription,
  StatusBar,
  TextStyle,
  ViewStyle,
} from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import LinearGradient from "react-native-linear-gradient"

import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { BackgroundImage } from "../components/BackgroundImage"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  DetailsStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { typography } from "../theme"

const MESSAGE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 24,
  lineHeight: 32,
  letterSpacing: -0.96,
  color: "#fff",
}
const MESSAGE_BOLD: TextStyle = {
  fontSize: 24,
  fontWeight: "bold",
  lineHeight: 32,
  letterSpacing: -0.96,
  color: "#fff",
  marginTop: -0.5,
}
const LOGO_CONTAINER: ViewStyle = {
  flex: 5,
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  marginTop: 10,
}

type InitialRouteProp = RouteProp<AuthStackParamList, "Initial">

type InitialNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "Initial">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: InitialNavigationProp
  route: InitialRouteProp
}

interface State {
  isModalVisible: boolean
  appState: string
}

@inject("accountStore")
@observer
export default class InitialScreen extends Component<Props, State> {
  static navigationOptions = {
    headerShown: false,
  }

  changeEventListener: NativeEventSubscription

  constructor(props: Props) {
    super(props)
    this.state = {
      isModalVisible: false,
      appState: AppState.currentState,
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate("Walkthrough")
    }, 1500)
  }

  didFocus = () => {
    NavigationBar.setVisibilityAsync("hidden")
    this.changeEventListener = AppState.addEventListener("change", this._handleAppStateChange)
  }

  didBlur = () => {
    NavigationBar.setVisibilityAsync("visible")
    this.changeEventListener.remove()
  }

  _handleAppStateChange = (nextAppState: string) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
      NavigationBar.setVisibilityAsync("hidden")
    }
    this.setState({ appState: nextAppState })
  }

  _toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible })

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#ff841a" }}>
        <StatusBar backgroundColor="#e8382f" />
        <LinearGradient colors={["#e8382f", "#ff841a"]} style={{ flex: 1 }}>
          <View style={LOGO_CONTAINER}>
            <BackgroundImage
              source={require("../assets/images/paprica.png")}
              image={{ width: 309, height: 380 }}
            />
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Walkthrough")
              }}
            >
              <Image
                source={require("../assets/images/hello.png")}
                style={{
                  width: 182,
                  height: 70,
                  marginBottom: 21,
                }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Walkthrough")
              }}
              style={{ alignItems: "center" }}
            >
              <Text style={MESSAGE}>건강해질 수 있다는 믿음</Text>
              <Text style={MESSAGE}>서로를 응원하는 마음</Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={MESSAGE_BOLD}>파프리카케어</Text>
                <Text style={MESSAGE}>가 함께합니다.</Text>
              </View>
            </TouchableOpacity>
          </View>
        </LinearGradient>
      </View>
    )
  }
}
