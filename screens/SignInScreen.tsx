import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import { ApiResponse } from "apisauce"
import * as NavigationBar from "expo-navigation-bar"
import { inject, observer } from "mobx-react"
import { FormControl, Input, ScrollView, Stack, Toast } from "native-base"
import React from "react"
import {
  Alert,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  TextStyle,
  View,
  ViewStyle,
} from "react-native"
import appsFlyer from "react-native-appsflyer"
import email from "react-native-email"
import { AccessToken, LoginManager, Settings } from "react-native-fbsdk-next"
import { TouchableOpacity } from "react-native-gesture-handler"
import { Button } from "tamagui"

import { appleAuth } from "@invertase/react-native-apple-authentication"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { KakaoOAuthToken, login as kakaoLogin } from "@react-native-seoul/kakao-login"
import NaverLogin from "@react-native-seoul/naver-login"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { api, LoginParams } from "../api"
import TouchableItem from "../components/button/TouchableItem"
import CircleButton from "../components/CircleButton"
import CloseButton from "../components/CloseButton"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { Text } from "../components/StyledText"
import Triangle from "../components/Triangle"
import {
  AuthStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  alignItems: "center",
  height: Dimensions.get("screen").height,
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  // flex: 1,
  flexDirection: "column",
  paddingTop: 16,
  paddingHorizontal: 16,
}
const WELCOME: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingBottom: 32,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 34,
  lineHeight: 37,
  color: color.text,
}
const FORM_ITEM: ViewStyle = {
  backgroundColor: color.input,
  marginBottom: 32,
  marginLeft: 16,
  marginRight: 16,
  paddingLeft: 0,
  justifyContent: "center",
  alignSelf: "stretch",
}
const ROW_CENTER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
}
const CENTER: ViewStyle = {
  alignItems: "center",
}
const LABEL: TextStyle = {
  width: 76,
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  paddingRight: 0,
}
const CIRCLE_BORDERED = { borderWidth: 2.5, borderColor: color.primary }
const CIRCLE_BUTTON = { marginHorizontal: 14 }
const ROW: ViewStyle = { flexDirection: "row" }
const TRIANGLE = { borderBottomColor: "#e4e4e4" }
const BUBBLE_BOX = {
  width: 136,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: "#e4e4e4",
}
const BUBBLE_TEXT = { fontSize: 13, paddingVertical: 8, textAlign: "center" }
const RESET_PASSWORD_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  textDecorationLine: "underline",
  color: color.palette.grey50,
}
const LOGIN_BUTTON = {
  marginBottom: 16,
}
const WITH_SNS_TITLE = {
  fontSize: 16,
  paddingBottom: 16,
}
const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
  color: color.palette.white,
}
const INPUT = { fontFamily: typography.bold, fontSize: 16, color: color.text }
const BUTTON: ViewStyle = {
  borderRadius: 5,
  marginRight: 32,
}
const BOTTOM_BUTTONS: ViewStyle = {
  flex: 1,
  justifyContent: "flex-end",
  paddingTop: 20,
}

const sourceKakao = require("../assets/images/snsKakao.png")
const sourceFacebook = require("../assets/images/snsFacebook.png")
const sourceNaver = require("../assets/images/snsNaver.png")
const hitSlop10 = {
  top: 10,
  left: 10,
  bottom: 10,
  right: 10,
}

const initials = {
  kConsumerKey: "JinS1xIkcrVHYnewU7Dg", // 애플리케이션에서 사용하는 클라이언트 아이디
  kConsumerSecret: "7VrIXWfXil", // 애플리케이션에서 사용하는 클라이언트 시크릿
  kServiceAppName: "파프리카케어", // 애플리케이션 이름
  kServiceAppUrlScheme: "naverloginJinS1xIkcrVHYnewU7Dg", // only for iOS, 콜백을 받을 URL Scheme
}

type SignInRouteProp = RouteProp<AuthStackParamList, "SignIn">

type SignInNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "SignIn">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<HomeStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: SignInNavigationProp
  route: SignInRouteProp
}

interface State {
  emailInputFocused: boolean
}

@inject("accountStore", "prescriptionStore")
@observer
export default class SignInScreen extends React.Component<Props, State> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route
    return {
      title: null,
      headerBackTitle: null,
      headerLeft: params?.backRouteName // https://onions.atlassian.net/browse/PAP-771
        ? () => (
            <CloseButton
              style={{ marginLeft: 24 }}
              imageStyle={{ tintColor: color.palette.gray3 }}
              onPress={() => navigation.navigate(params.backRouteName, params.params)}
            />
          )
        : () => (
            <TouchableItem
              style={{ marginLeft: 12 }}
              onPress={() => navigation.navigate("Walkthrough")}
            >
              <CustomHeaderBackImage />
            </TouchableItem>
          ),
      headerRight: () => (
        <TouchableItem
          onPress={() => navigation.replace("SignUp")}
          style={BUTTON}
          hitSlop={hitSlop10}
        >
          <Text
            style={{
              ...typography.body,
            }}
          >
            회원가입
          </Text>
        </TouchableItem>
      ),
      animationEnabled: false,
    }
  }

  _unsubscribeFocus: () => void

  constructor(props: Props) {
    super(props)
    const { accountStore } = this.props
    accountStore.createRequestAccount()
    accountStore.reqAccount!.setMode("login")
    this.state = {
      emailInputFocused: false,
    }
  }

  async UNSAFE_componentWillMount() {
    const { accountStore } = this.props
    const { signPath } = accountStore
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)
    const email = await AsyncStorage.getItem("email")
    if (email && signPath == "email") {
      accountStore.reqAccount!.setEmail(email)
    }
  }

  didFocus = () => {
    const { accountStore, route } = this.props
    console.tron.log("< SignInScreen > didFocus, signPath: " + accountStore.signPath)
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })

    NavigationBar.setBackgroundColorAsync("#efefef")
    if (!accountStore.reqAccount) {
      accountStore.createRequestAccount()
      accountStore.reqAccount!.setMode("login")
    }
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
  }

  render() {
    const { accountStore } = this.props
    const { emailInputFocused } = this.state
    const { reqAccount, signPath } = accountStore
    if (!accountStore.reqAccount) {
      accountStore.createRequestAccount()
    }

    return (
      <View style={BACKGROUND}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={WELCOME}>
            <Text style={TITLE_TEXT}>로그인</Text>
          </View>

          <FormControl>
            <Stack inlineLabel style={FORM_ITEM} error={!!reqAccount?.emailError}>
              <FormControl.Label style={LABEL}>이메일</FormControl.Label>
              <Input
                autoCapitalize="none"
                placeholder="이메일 주소"
                placeholderTextColor={color.placeholder}
                selectionColor={color.primary}
                keyboardType={"email-address"}
                onChangeText={(text) => reqAccount?.emailOnChange(text)}
                onFocus={this.onEmailInputFocus}
                value={reqAccount?.email}
                returnKeyType={"next"}
                style={INPUT}
              />
            </Stack>
            {emailInputFocused ? (
              <View style={{ width: Dimensions.get("window").width - 32 }}>
                <Stack inlineLabel style={{ ...FORM_ITEM }} error={!!reqAccount?.passwordError}>
                  <FormControl.Label style={LABEL}>비밀번호</FormControl.Label>
                  <Input
                    autoCapitalize="none"
                    placeholder="비밀번호"
                    placeholderTextColor={color.placeholder}
                    selectionColor={color.primary}
                    secureTextEntry={true}
                    onBlur={() => reqAccount?.validatePassword()}
                    onChangeText={(text) => reqAccount?.passwordOnChange(text)}
                    value={reqAccount?.password}
                    style={INPUT}
                  />
                </Stack>
                <TouchableOpacity onPress={this.passwordReset} style={{ alignSelf: "center" }}>
                  <Text style={RESET_PASSWORD_TEXT}>비밀번호가 기억나지 않나요?</Text>
                </TouchableOpacity>
              </View>
            ) : null}
            {!emailInputFocused ? (
              <View style={CENTER}>
                <Text style={WITH_SNS_TITLE}>SNS로 로그인 하셨었나요?</Text>
                <View style={ROW_CENTER}>
                  <CircleButton
                    source={sourceKakao}
                    onPress={this.kakaoLogin}
                    style={CIRCLE_BUTTON}
                    circleStyle={signPath === "kakao" ? CIRCLE_BORDERED : undefined}
                  />
                  <CircleButton
                    source={sourceFacebook}
                    onPress={this.facebookLogin}
                    style={CIRCLE_BUTTON}
                    circleStyle={signPath === "facebook" ? CIRCLE_BORDERED : undefined}
                  />
                  <CircleButton
                    source={sourceNaver}
                    onPress={this.naverLogin}
                    style={CIRCLE_BUTTON}
                    circleStyle={signPath === "naver" ? CIRCLE_BORDERED : undefined}
                  />
                  {Platform.OS === "ios" ? (
                    <CircleButton
                      source="apple"
                      onPress={this.signInWithApple}
                      style={CIRCLE_BUTTON}
                      circleStyle={signPath === "apple" ? CIRCLE_BORDERED : undefined}
                    />
                  ) : null}
                </View>
                <View style={ROW}>
                  {signPath !== "email" ? (
                    <View
                      style={{
                        position: "relative",
                        left:
                          Platform.OS === "android"
                            ? signPath === "facebook"
                              ? 0
                              : signPath === "kakao"
                              ? -84
                              : 84
                            : signPath === "facebook"
                            ? -42
                            : signPath === "kakao"
                            ? -126
                            : signPath === "naver"
                            ? 42
                            : 126,
                      }}
                    >
                      <Triangle style={TRIANGLE} isDown={false} />
                      <View style={BUBBLE_BOX}>
                        <Text style={BUBBLE_TEXT}>마지막으로 {"\n"}로그인한 계정입니다.</Text>
                      </View>
                    </View>
                  ) : null}
                </View>
              </View>
            ) : null}
          </FormControl>
          {emailInputFocused ? (
            Platform.OS === "ios" ? (
              <KeyboardAvoidingView
                style={BOTTOM_BUTTONS}
                behavior={"padding"} // Android 의 경우 'padding' 을 주면 로그인 버튼이 이상한 위치에 놓이는 문제 있음. undefined 로 하면 정상적인 위치에 놓이나 버튼이 안 눌리는 문제 있음.
                keyboardVerticalOffset={72} // 가상키보드 위에 로그인 버튼이 보이도록 조정함.
              >
                <Button
                  bc={color.palette.orange}
                  br={45}
                  style={LOGIN_BUTTON}
                  onPress={this.signInAsync}
                >
                  <Text style={BUTTON_TEXT}>로그인</Text>
                </Button>
              </KeyboardAvoidingView>
            ) : (
              <View style={BOTTOM_BUTTONS}>
                <Button
                  bc={color.palette.orange}
                  br={45}
                  style={LOGIN_BUTTON}
                  onPress={this.signInAsync}
                >
                  <Text style={BUTTON_TEXT}>로그인</Text>
                </Button>
              </View>
            )
          ) : null}
        </ScrollView>
      </View>
    )
  }

  signUp = () => {
    this.props.navigation.navigate("SignUp")
  }

  passwordReset = () => {
    this.props.navigation.navigate("Reset")
  }

  handleEmail = () => {
    const user = this.props.accountStore.user
    const to = ["onions@onns.co.kr"] // string or array of email addresses

    let body = `네이버 계정으로 로그인 시 발생한 오류 입니다. \n증상이 나타나기까지의 과정을 아래에 적어주세요.\n\n\n\n\n`
    body =
      body +
      `\n\n파프리카케어 사용에 불편을 드려 대단히 죄송합니다.\n문제 해결에 최선을 다하겠습니다.\n\n\n`
    body = body + `파프리카케어 ID: ${user.id}\n파프리카케어 앱 버전: ${packageJson.version}\n`
    body = body + `플랫폼 OS: ${Platform.OS} v${Platform.Version}`
    // TODO: add device model?

    email(to, {
      subject: "네이버 로그인 오류",
      body: body,
    }).catch(console.error)
  }

  onEmailInputFocus = () => {
    if (!this.state.emailInputFocused) {
      this.setState({ emailInputFocused: true })
    }
  }

  getSignPathName(path: "kakao" | "naver" | "facebook" | "apple" | "email") {
    switch (path) {
      case "kakao":
        return "카카오"
      case "apple":
        return "애플"
      case "naver":
        return "네이버"
      case "facebook":
        return "페이스북"
      case "email":
        return "이메일"
    }
  }

  async authenticate(path: "kakao" | "naver" | "facebook" | "apple" | "email", params: object) {
    const { accountStore, prescriptionStore, navigation } = this.props
    api.apisauce.deleteHeader("Authorization")

    let urlPath
    if (path === "email") {
      urlPath = "login"
    } else {
      urlPath = path
    }

    // start making calls
    const response: ApiResponse<any> = await api.apisauce.post(`/rest-auth/${urlPath}/`, params)

    if (!response.ok && !response.data.non_field_errors) {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`일시적으로 서버에 접속할 수 없습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      return
    }

    if (response.data?.key) {
      firebase.analytics().logEvent("login", { signPath: path })
      firebase.analytics().setUserId(String(response.data.user))
      amplitude.getInstance().setUserId(response.data.user)
      amplitude.getInstance().logEvent("login", { signPath: path })
      appsFlyer.logEvent("af_login", { signPath: path })
      accountStore.user.setId(response.data.user)
      accountStore.setSignPath(path)

      api.apisauce.setHeader("Authorization", "Token ".concat(response.data.key))
      prescriptionStore.fetchTimeSlots()

      await AsyncStorage.setItem("userToken", response.data.key)
      await AsyncStorage.setItem("userId", String(response.data.user))
      await AsyncStorage.setItem("signPath", path)

      const { reqAccount } = accountStore
      if (path === "email" && reqAccount) {
        reqAccount.passwordOnChange("")
        await AsyncStorage.setItem("email", reqAccount.email)
      }

      // 기존 사용자 여부 판단하기 위한 사용자 정보 가져오기
      const myInfoResult = await api.getMyInfo(Number(accountStore.user.id), accountStore.signPath)
      if (myInfoResult.kind === "ok") {
        accountStore.setUser(myInfoResult.user)
        const { user } = accountStore

        // profile 이 없는 경우
        if (user.name && user.name.length === 0) {
          console.tron.logImportant("< SignInScreen > authenticate, Profile 이 존재하지 않음")
        } // profile 있는 경우
        else if (user.newly_agreed == null || user.newly_agreed == "pre") {
          navigation.navigate("ServiceAgreement")
        } else {
          navigation.popToTop()
          navigation.navigate("Main", { screen: "HomeStack" })
        }
      }
    } else if (
      response.data.non_field_errors &&
      String(response.data.non_field_errors[0]).includes(
        "User is already registered with this e-mail address.",
      )
    ) {
      Alert.alert(
        "",
        this.getExistAccountMessage(path),
        [{ text: "확인", onPress: () => navigation.navigate("SignIn") }],
        { cancelable: false },
      )
    } else if (response.data.detail) {
      Alert.alert("", response.data.detail, [{ text: "확인", onPress: () => {} }], {
        cancelable: false,
      })
    } else if (
      String(response.data.non_field_errors[0]).includes("제공된 인증데이터") &&
      path === "email"
    ) {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`파프리카케어에 등록되지 않은 아이디이거나, 아이디 또는 비밀번호를 잘못 입력하셨습니다.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (response.data.non_field_errors) {
      Alert.alert(
        "",
        `오류가 발생하였습니다. \n` + response.data.non_field_errors[0],
        [
          {
            text: "확인",
            onPress: () =>
              console.tron.log(
                "< SignUpEmailScreen > authenticate, response.data.non_field_errors: ",
                response,
              ),
          },
        ],
        { cancelable: false },
      )
    } else {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
      Alert.alert(
        "",
        `오류가 발생하였습니다. \n나중에 다시 시도해주세요. ` +
          (response.problem ? response.problem : ""),
        [
          {
            text: "확인",
            onPress: () =>
              console.tron.log("< SignUpEmailScreen > authenticate, else error: ", response),
          },
        ],
        { cancelable: false },
      )
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{response.data}</Text>,
        duration: 3500,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  getExistAccountMessage(path: "kakao" | "naver" | "facebook" | "apple" | "email"): string {
    if (path === "email") {
      return "입력하신 이메일 주소는 다른 SNS 계정으로 이미 등록되어 있습니다. 이전에 로그인 하신 방법으로 다시 시도해 주세요."
    } else {
      return `해당 ${this.getSignPathName(
        path,
      )} 계정에서 사용하는 이메일 주소는 다른 SNS 계정 또는 이메일 계정으로 이미 등록되어 있습니다. 이전에 로그인 하신 방법으로 다시 시도해 주세요.`
    }
  }

  signInWithApple = async () => {
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      })
      console.tron.log(
        "< SignInScreen > signInWithApple, appleAuthRequestResponse: ",
        appleAuthRequestResponse,
      )

      const { identityToken } = appleAuthRequestResponse

      if (identityToken) {
        // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
        this.authenticate("apple", { access_token: identityToken })
      } else {
        // no token - failed sign-in?
        Alert.alert("", "no token - failed sign-in?", [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
      }
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.tron.warn("< SignInScreen > signInWithApple, User canceled Apple Sign in.")
        Alert.alert("", "User canceled Apple Sign in.", [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
      } else {
        console.tron.log("< SignInScreen > signInWithApple, error", error)
        Alert.alert("error", error, [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
      }
    }
  }

  naverLogin = async () => {
    const { accountStore } = this.props
    accountStore.setTemporaryPinCodeDisabled(true)
    setTimeout(() => {
      accountStore.setTemporaryPinCodeDisabled(false)
    }, 3000)

    try {
      const { failureResponse, successResponse } = await NaverLogin.login({
        appName: initials.kServiceAppName,
        consumerKey: initials.kConsumerKey,
        consumerSecret: initials.kConsumerSecret,
        serviceUrlScheme: initials.kServiceAppUrlScheme,
      })

      if (successResponse) {
        this.authenticate("naver", { access_token: successResponse.accessToken })
      } else if (failureResponse && !failureResponse.isCancel) {
        console.tron.log(failureResponse.message)
      }
    } catch (err) {
      console.tron.log("< SignInScreen > naverLogin, error: ", err)
    }
  }

  kakaoLogin = async () => {
    const { accountStore } = this.props
    accountStore.setTemporaryPinCodeDisabled(true)
    setTimeout(() => {
      accountStore.setTemporaryPinCodeDisabled(false)
    }, 3000)

    try {
      const token: KakaoOAuthToken = await kakaoLogin()
      if (token) {
        this.authenticate("kakao", {
          access_token: token.accessToken,
        })
      }
    } catch (err) {
      if (err.code != "E_CANCELLED_OPERATION") {
        Alert.alert(
          "",
          `카카오 계정으로 로그인 중 오류가 발생하였습니다. \n나중에 다시 시도해주세요. `,
          [
            {
              text: "확인",
              onPress: () => console.tron.log("< SignInScreen > kakaoLogin, error: ", err),
            },
          ],
          { cancelable: false },
        )
      }
    }
  }

  facebookLogin = async () => {
    await Settings.initializeSDK()
    const { accountStore, navigation } = this.props

    api.apisauce.deleteHeader("Authorization")

    accountStore.setTemporaryPinCodeDisabled(true)
    // Attempt a login using the Facebook login dialog asking for default permissions.
    const result = await LoginManager.logInWithPermissions(["public_profile", "email"])

    setTimeout(() => {
      accountStore.setTemporaryPinCodeDisabled(false)
    }, 3000)

    if (result.isCancelled) {
      console.tron.log("< SignInScreen > facebookLogin, Login cancelled")
    } else {
      console.tron.log(
        "< SignInScreen > facebookLogin, Login success with permissions: " +
          result.grantedPermissions.toString(),
      )

      let token: string
      try {
        const data = await AccessToken.getCurrentAccessToken()
        token = data.accessToken.toString()
        this.authenticate("facebook", {
          access_token: token,
        })
      } catch (error) {
        firebase.crashlytics().recordError(500, "Error login from Facebook:" + String(error))
        Toast.show({
          title: <Text style={styles.TOAST_TEXT}>{`페이스북 로그인 실패: ${error}`}</Text>,
          duration: 3000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        console.tron.log("< SignInScreen > facebookLogin, Login fail with error: " + error)
        navigation.replace("SignIn")
      }
    }
  }

  signInAsync = async () => {
    const reqAccount = this.props.accountStore.reqAccount!
    reqAccount.setMode("login")
    reqAccount.validateForm()

    if (reqAccount.isValid) {
      const params: LoginParams = {
        username: reqAccount.email,
        email: reqAccount.email,
        password: reqAccount.password,
      }
      this.authenticate("email", params)
    } else if (reqAccount.emailError.length > 0) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{reqAccount.emailError}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (reqAccount.passwordError.length > 0) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{reqAccount.passwordError}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`유효한 이메일주소와 비밀번호를 입력해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }
}
