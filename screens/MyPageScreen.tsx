import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Avatar, ScrollView, View } from "native-base"
import React from "react"
import {
  Dimensions,
  Linking,
  Platform,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import DeviceInfo from "react-native-device-info"
import email from "react-native-email"
import VersionCheck from "react-native-version-check"
import { Separator, SizableText, Stack, XStack } from "tamagui"

import { api } from "@api/api"
import { supabase } from "@api/supabase"
import SvgArrowRightNew from "@assets/images/arrow-right-new.svg"
import SvgMyPoint from "@assets/images/my-point.svg"
import { setTotalPoints } from "@features/points/stores/total-points"
import { addComma } from "@lib/utils"
import firebase from "@react-native-firebase/app"
import remoteConfig from "@react-native-firebase/remote-config"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import { getPopupButton, Popup } from "../components/Popup"
import { Text } from "../components/StyledText"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const cover = {
  uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png`,
}

const CONTENT_CONTAINER: ViewStyle = {
  backgroundColor: color.palette.gray5,
}
const PROFILE_NAME: ViewStyle = {
  alignItems: "center",
  paddingTop: 24,
  paddingBottom: 16,
  backgroundColor: color.palette.white,
}
const deviceWidth = Dimensions.get("window").width
const NAME: TextStyle = {
  maxWidth: deviceWidth - 180,
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
  color: color.text,
}
const SETTING_ITEM: ViewStyle = {
  height: 56,
  flexDirection: "row",
  alignItems: "center",
  backgroundColor: "#fff",
  paddingHorizontal: 16,
}
const ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const MENU_ITEM_DIVIDER = { height: 1, backgroundColor: color.palette.gray4, marginHorizontal: 16 }
const MENU_ITEM_GROUP = {
  backgroundColor: color.palette.white,
  borderTopColor: color.line,
  borderTopWidth: 1,
  borderBottomColor: color.line,
  borderBottomWidth: 1,
  marginBottom: 8,
}
const ROW_RIGHT: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "flex-end",
  alignItems: "center",
}
const BODY_OPACITY = { ...typography.sub2, opacity: 0.5 }
const UPDATE_TEXT = { fontFamily: typography.primary, fontSize: 15, color: color.primary }

type MyPageRouteProp = RouteProp<MyPageStackParamList, "MyPage">

type MyPageNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "MyPage">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: MyPageNavigationProp
  route: MyPageRouteProp
}

interface State {
  needAppUpdate: boolean
  popup: boolean
  tourMode: boolean
  myPoint: number
}

@inject("accountStore")
@observer
export default class MyPageScreen extends React.Component<Props, State> {
  static navigationOptions = () => {
    return {
      title: "내정보",
      headerLeft: () => null,
    }
  }

  _unsubscribeFocus: () => void

  constructor(props: Props) {
    super(props)

    this.state = {
      needAppUpdate: false,
      popup: false,
      tourMode: false,
      myPoint: -1,
    }
  }

  componentDidMount = async () => {
    await this.props.accountStore.fetchUser()

    // const { data, error } = await supabase.from("point_activities").insert([
    //   {
    //     user_id: accountStore.user.id,
    //     p_id: pid,
    //     point_type: pointType,
    //     desc: description,
    //     point: POINTS_BY_TYPE.get(pointType),
    //   },
    // ])
    // console.tron.log("data", data)
    // console.tron.log("error", error)

    const currentAppVersion = await VersionCheck.getCurrentVersion()
    const deviceAppVersion = await DeviceInfo.getVersion()

    if (currentAppVersion !== deviceAppVersion) {
      this.setState({
        needAppUpdate: true,
      })
    }
    // version-check 라이브러리의 .needUpdate 메소드가 응답하지않아(undefined) 위의 방법사용 (21.12.10)  참고: https://github.com/kimxogus/react-native-version-check/issues/141
    // const versionCheck = await VersionCheck.needUpdate()
    // if (versionCheck.isNeeded) {
    //   this.setState({
    //     needAppUpdate: true,
    //   })
    // }
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)
    this.checkMyPoint()
  }

  checkMyPoint = async () => {
    const { data: totalPoints, error } = await supabase
      .from("total_points")
      .select("*")
      .eq("user_id", this.props.accountStore.user.id)
    console.tron.log("totalPoints", totalPoints)

    if (totalPoints && totalPoints.length > 0) {
      this.setState({ myPoint: totalPoints[0].total_points })
      setTotalPoints(totalPoints[0].total_points)
    } else {
      this.setState({ myPoint: 0 })
      setTotalPoints(0)
    }
  }

  didFocus = async () => {
    const { accountStore, route } = this.props
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })

    // 둘러보기 이용중
    if (!api.apisauce.headers.Authorization) {
      this.setState({ tourMode: true })
    } else {
      this.setState({ tourMode: false })
    }

    this.checkMyPoint()
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
  }

  onNamePress = () => {
    const { navigation } = this.props
    const { tourMode } = this.state
    if (tourMode) {
      navigation.navigate("Auth", {
        screen: "SignUp",
        params: { hideTourButton: true, prevScreen: "MyPageStack" },
      })
    } else {
      navigation.navigate("EditUserProfile")
    }
  }

  handleEmail = () => {
    const user = this.props.accountStore.user
    const to = ["onions@onns.co.kr"] // string or array of email addresses

    let body = "이곳에 내용을 입력해 주세요.\n\n\n\n\n"
    body =
      body +
      "*오류의 경우, 보다 정확한 내용 확인을 위해서 오류 화면의 스크린샷을 첨부해 주세요.\n\n파프리카케어를 사용해 주셔서 감사합니다.\n\n\n\n"
    body = body + `파프리카케어 ID: ${user.id}, 앱 버전: ${packageJson.version}, `
    body = body + `플랫폼 OS: ${Platform.OS} v${Platform.Version}`
    body = body + `디바이스 정보: ${DeviceInfo.getModel()}`

    email(to, {
      subject: "사용자 의견",
      body: body,
    }).catch(console.error)
  }

  render() {
    const { accountStore, navigation } = this.props
    const { user } = accountStore
    const { tourMode } = this.state

    return (
      <View style={{ flex: 1, backgroundColor: color.palette.gray5 }}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={PROFILE_NAME}>
            <Avatar source={user.profile_image ? { uri: user.profile_image } : cover} />
            <TouchableOpacity onPress={this.onNamePress} style={{ paddingTop: 8 }}>
              <View style={{ alignItems: "center" }}>
                <Text style={NAME} numberOfLines={1} ellipsizeMode="tail">
                  {user.nickname}
                </Text>
                {tourMode ? (
                  <Text style={{ ...typography.sub2, color: color.primary }}>회원가입 하기</Text>
                ) : (
                  <Text style={BODY_OPACITY}>개인정보 수정</Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
          <Stack h={12} bc={color.backgrounds.subdued}>
            <Separator boc={color.borders.dividerBetweenUI} />
          </Stack>
          {/* <View style={styles.prescriptionsCountRow}>
          <Text>처방전</Text>
          <Text>{this.state.prescriptionsCount}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
            <Text>최근 방문한 병원</Text>
            <SvgArrowRight width={24} height={24} color={color.palette.gray3} />
        </View> */}
          {/* <Card>
          <CardItem header button onPress={() => this.props.navigation.navigate("HospitalDetail", { hospitalName: "동천이안청소년소아과병원" })}>
            <Text>동천이안청소년소아과의원</Text>
          </CardItem>
          <CardItem button onPress={() => console.log("This is Card Body")}>
            <Body>
              <Text>
                경기도 용인지 수지구 동천동
              </Text>
            </Body>
          </CardItem>
          <CardItem footer button onPress={() => console.log("This is Card Footer")}>
            <Body></Body>
            <Right>
              <Text>평가하기</Text>
            </Right>
          </CardItem>
        </Card> */}
          <View style={{ backgroundColor: color.palette.white }}>
            {!tourMode ? (
              <>
                {remoteConfig().getBoolean("points_enabled") && (
                  <>
                    <XStack
                      jc="space-between"
                      py={16}
                      px={24}
                      ai="center"
                      pressStyle={{ opacity: 0.7 }}
                      onPress={() =>
                        navigation.navigate("Details", {
                          screen: "MyPoint",
                        })
                      }
                    >
                      <XStack ai="center">
                        <SvgMyPoint />
                        <Text style={typography.body} ml={16}>
                          내 포인트
                        </Text>
                      </XStack>
                      <XStack ai="center">
                        <SizableText
                          fow="600"
                          fos={23}
                          lh={30}
                          ls={-0.5}
                          col={color.textColor.default}
                          mr={12}
                        >
                          {this.state.myPoint === -1 ? "" : `${addComma(this.state.myPoint)}원`}
                        </SizableText>
                        <SvgArrowRightNew />
                      </XStack>
                    </XStack>
                    <Stack h={12} bc={color.backgrounds.subdued}>
                      <Separator boc={color.borders.dividerBetweenUI} />
                    </Stack>
                  </>
                )}
                <View>
                  <TouchableOpacity
                    style={SETTING_ITEM}
                    onPress={() => navigation.navigate("FamilyMembers")}
                  >
                    <View style={ROW}>
                      <Text style={typography.body}>가족회원 관리</Text>
                      <View style={ROW_RIGHT}>
                        <SvgArrowRight width={24} height={24} color={color.primary} />
                      </View>
                    </View>
                  </TouchableOpacity>
                  <View style={MENU_ITEM_DIVIDER} />
                </View>
              </>
            ) : null}
            <TouchableOpacity style={SETTING_ITEM} onPress={() => navigation.navigate("Settings")}>
              <View style={ROW}>
                <Text style={typography.body}>앱 설정</Text>
                <View style={ROW_RIGHT}>
                  <SvgArrowRight width={24} height={24} color={color.primary} />
                </View>
              </View>
            </TouchableOpacity>
            <View style={MENU_ITEM_DIVIDER} />
            <TouchableOpacity
              style={SETTING_ITEM}
              onPress={() =>
                navigation.navigate("Details", {
                  screen: "TimeSlotSettings",
                })
              }
            >
              <View style={ROW}>
                <Text style={typography.body}>복약알림 설정</Text>
                <View style={ROW_RIGHT}>
                  <SvgArrowRight width={24} height={24} color={color.primary} />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={MENU_ITEM_GROUP}>
            <TouchableOpacity style={SETTING_ITEM} onPress={this.handleEmail}>
              <View style={ROW}>
                <Text style={typography.body}>사용자 의견 보내기</Text>
                <View style={ROW_RIGHT}>
                  <SvgArrowRight width={24} height={24} color={color.primary} />
                </View>
              </View>
            </TouchableOpacity>
            <View style={MENU_ITEM_DIVIDER} />
            <TouchableOpacity
              style={SETTING_ITEM}
              onPress={() => navigation.navigate("PrivacyPolicyAndTerms")}
            >
              <View style={ROW}>
                <Text style={typography.body}>약관 보기</Text>
                <View style={ROW_RIGHT}>
                  <SvgArrowRight width={24} height={24} color={color.primary} />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={[MENU_ITEM_GROUP, { marginBottom: 70 }]}>
            <TouchableOpacity
              style={SETTING_ITEM}
              onPress={() => navigation.navigate("FirstUseGuide")}
            >
              <View style={ROW}>
                <Text style={typography.body}>앱 사용법 보기</Text>
                <View style={ROW_RIGHT}>
                  <SvgArrowRight width={24} height={24} color={color.primary} />
                </View>
              </View>
            </TouchableOpacity>
            <View style={MENU_ITEM_DIVIDER} />
            <TouchableOpacity
              style={SETTING_ITEM}
              onPress={() => {
                if (this.state.needAppUpdate) {
                  this.openAppStore()
                } else {
                  this.setState({ popup: true })
                }
              }}
            >
              <View style={ROW}>
                <Text style={typography.body}>
                  버전 정보{this.state.needAppUpdate ? `: v${packageJson.version}` : ""}
                </Text>
                <View style={ROW_RIGHT}>
                  {this.state.needAppUpdate ? (
                    <Text style={UPDATE_TEXT}>업데이트</Text>
                  ) : (
                    <Text style={typography.body}>v{packageJson.version}</Text>
                  )}
                </View>
              </View>
            </TouchableOpacity>
          </View>

          <Popup
            isVisible={this.state.popup}
            title={"최신 버전입니다"}
            message={`버전정보: ${packageJson.version} \n사용하고 계신 앱은 최신 버전입니다.`}
            button1={getPopupButton(() => this.setState({ popup: false }), "확인")}
          />
        </ScrollView>
      </View>
    )
  }

  openAppStore() {
    if (Platform.OS === "android") {
      Linking.openURL("market://details?id=com.onions.papricacare")
    } else {
      Linking.openURL("itms-apps://apps.apple.com/kr/app/파프리카케어/id1453787406")
    }
  }
}
