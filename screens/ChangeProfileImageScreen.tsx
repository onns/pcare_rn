import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { ScrollView, Spinner, Toast, View } from "native-base"
import React, { Component } from "react"
import {
  Alert,
  BackHandler,
  FlatList,
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import FastImage from "react-native-fast-image"
import { launchImageLibrary } from "react-native-image-picker"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"
import { YStack } from "tamagui"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { ParamListBase, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import CloseButton from "../components/CloseButton"
import { Button } from "../components/StyledButton"
import { MyPageStackParamList } from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { FamilyMember } from "../stores/FamilyMember"
import { color, styles, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  paddingTop: 16,
  backgroundColor: "#fff",
  alignItems: "center",
}
const BOTTOM_BUTTONS: ViewStyle = {
  height: 140,
  justifyContent: "center",
  paddingVertical: 8,
  paddingHorizontal: 16,
  borderTopColor: color.line,
  borderTopWidth: 1,
  backgroundColor: color.palette.white,
}
const BUTTON_TEXT: TextStyle = {
  flex: 1,
  fontFamily: typography.bold,
  fontSize: 17,
  color: color.text,
  opacity: 0.7,
  fontWeight: "500",
  textAlign: "center",
}
const FROM_GALLERY_BUTTON: ViewStyle = {
  alignItems: "center",
  marginBottom: 8,
  backgroundColor: color.palette.veryLightPink3,
  minHeight: 42,
}

export interface Props {
  accountStore: AccountStore
  navigation: StackNavigationProp<ParamListBase>
  route: RouteProp<MyPageStackParamList, "ChangeProfileImage">
}

interface State {
  selectedAvatar: number
  status: string
}

const avatarImageServerUri = `${DEFAULT_API_CONFIG.publicUrl}avatars/`

export const avatarSources = [
  {
    imageUrl: `${avatarImageServerUri}r2_adult_m.png`,
    key: "0",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_m_2.png`,
    key: "1",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_m_3.png`,
    key: "2",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_m_4.png`,
    key: "3",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_m_5.png`,
    key: "4",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_old_m.png`,
    key: "5",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_f.png`,
    key: "6",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_f_2.png`,
    key: "7",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_f_3.png`,
    key: "8",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_f_4.png`,
    key: "9",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_adult_f_5.png`,
    key: "10",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_old_f.png`,
    key: "11",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_child_m.png`,
    key: "12",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_child_m_2.png`,
    key: "13",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_child_m_3.png`,
    key: "14",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_child_f.png`,
    key: "15",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_child_f_2.png`,
    key: "16",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_child_f_3.png`,
    key: "17",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_young_m.png`,
    key: "18",
  },
  {
    imageUrl: `${avatarImageServerUri}r2_young_f.png`,
    key: "19",
  },
]

@inject("accountStore", "prescriptionStore")
@observer
export default class ChangeProfileImageScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route

    return {
      title: "프로필 이미지",
      headerLeft: () => (
        <CloseButton
          style={{ marginLeft: 24 }}
          imageStyle={{ tintColor: color.palette.gray3 }}
          onPress={() => {
            navigation.pop()
            navigation.navigate(params!.prevRouteName, {
              prevRouteName: params!.prevPrevRouteName,
            })
          }}
        />
      ),
      headerTitleStyle: styles.HEADER_TITLE,
    }
  }

  refAvatarList: FlatList<Object>
  _unsubscribeFocus: () => void

  member: FamilyMember
  memberIndex = -1

  constructor(props: Props) {
    super(props)
    this.state = {
      selectedAvatar: 0,
      status: "idle",
    }
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
    this._unsubscribeFocus()
  }

  componentDidMount() {
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)
  }

  didFocus = () => {
    if (this.refAvatarList) {
      this.refAvatarList.flashScrollIndicators()
    }
  }

  getPrevRouteName(params: any): any {
    return params.prevRouteName
  }

  getPrevPrevRouteName(params: any): any {
    return params.prevPrevRouteName
  }

  getProfileName(params: any): any {
    return params.profileName
  }

  getProfileGender(params: any): any {
    return params.profileGender
  }

  getProfileBirth(params: any): any {
    return params.profileBirth
  }

  getMemberInfo(params: any): any {
    let member
    member = this.props.accountStore.family[params.memberIndex]
    return member
  }

  handleBackPress = () => {
    this.props.navigation.pop()
    return true
  }

  render() {
    const { navigation, route } = this.props
    const prevPrevRouteName = this.getPrevPrevRouteName(route.params)
    const imageUri = avatarSources[this.state.selectedAvatar].imageUrl
    const imageType = "image/png"

    if (this.state.status === "pending")
      return <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
    else
      return (
        <SafeAreaInsetsContext.Consumer>
          {(insets) => (
            <YStack f={1}>
              <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
                <FlatList
                  ref={(ref) => {
                    this.refAvatarList = ref
                  }}
                  data={avatarSources}
                  numColumns={3}
                  renderItem={this._renderItem}
                  extraData={this.state}
                />
                <View style={{ height: 15 }}></View>
              </ScrollView>
              <View style={{ ...BOTTOM_BUTTONS, paddingBottom: insets.bottom }}>
                <Button
                  onPress={() => this.launchImageLibraryOnPress()}
                  style={FROM_GALLERY_BUTTON}
                >
                  <Text style={BUTTON_TEXT}>사진첩에서 선택</Text>
                </Button>
                <Button
                  onPress={() => {
                    const prevRouteName = this.getPrevRouteName(route.params)
                    // const imageUri = avatarSources[this.state.selectedAvatar].imageUrl
                    // const imageType = "image/png"
                    const tempArray = String(imageUri).split("/")
                    const fileName = tempArray[tempArray.length - 1]

                    if (prevRouteName == "EditUserProfile") {
                      this.submit(prevRouteName, imageUri, imageType, fileName)
                    } else {
                      this._submit(prevRouteName, imageUri, imageType, fileName)
                      // navigation.pop()
                      navigation.navigate(this.getPrevRouteName(route.params), {
                        // ...this.props.route.params,
                        prevRouteName: prevPrevRouteName,
                        profileName: this.getProfileName(route.params),
                        profileGender: this.getProfileGender(route.params),
                        profileBirth: this.getProfileBirth(route.params),
                        imageUri: imageUri,
                        imageType: imageType,
                      })
                    }
                  }}
                  style={{ alignItems: "center", minHeight: 42 }}
                >
                  <Text style={[BUTTON_TEXT, { color: "#fff", opacity: 1 }]}>아바타 선택</Text>
                </Button>
              </View>
            </YStack>
          )}
        </SafeAreaInsetsContext.Consumer>
      )
  }

  _renderItem = ({ item, index }: any) => {
    return (
      <TouchableOpacity
        style={{ paddingHorizontal: 20, paddingVertical: 15 }}
        onPress={() => {
          this.setState({ selectedAvatar: index })
        }}
      >
        <FastImage
          style={[
            { width: 80, height: 80 },
            this.state.selectedAvatar == index
              ? { borderWidth: 2, borderRadius: 40, borderColor: color.primary }
              : null,
          ]}
          source={{ uri: item.imageUrl }}
          resizeMode={"contain"}
        />
      </TouchableOpacity>
    )
  }

  launchImageLibraryOnPress() {
    const { navigation, route } = this.props
    this.props.accountStore.setTemporaryPinCodeDisabled(true)
    launchImageLibrary({ mediaType: "photo", quality: 0.8, includeBase64: true }, (response) => {
      setTimeout(() => {
        this.props.accountStore.setTemporaryPinCodeDisabled(false)
      }, 3000)

      if (response.didCancel) {
        console.tron.log(
          "< ChangeProfileImageScreen > launchImageLibrary, User cancelled image picker",
        )
      } else if (response.errorMessage) {
        console.tron.log(
          "< ChangeProfileImageScreen > launchImageLibrary, ImagePicker errorMessage: ",
          response.errorMessage,
        )
      } else {
        const fileNameArray = response.assets[0].fileName?.split(".")
        const fileType = fileNameArray[fileNameArray.length - 1]
        const prevRouteName = this.getPrevRouteName(route.params)
        const imageUri = response.assets[0].uri
        const imageType =
          response.assets[0].type != null ? response.assets[0].type : "image/" + fileType
        const fileName = response.assets[0].fileName

        if (prevRouteName === "EditUserProfile") {
          this.submit(prevRouteName, imageUri, imageType)
        } else {
          this._submit(prevRouteName, imageUri, imageType, fileName)
          navigation.pop()
          navigation.navigate(prevRouteName, {
            // ...this.props.route.params,
            prevRouteName: this.getPrevPrevRouteName(route.params),
            profileName: this.getProfileName(route.params),
            profileGender: this.getProfileGender(route.params),
            profileBirth: this.getProfileBirth(route.params),
            imageUri: imageUri,
            imageType: imageType,
          })
        }
      }
    })
  }

  async submit(prevRouteName: string, imageUri: string, imageType: string, fileName?: string) {
    this.setState({ status: "pending" })
    const { route } = this.props
    const user = this.props.accountStore.user

    const reqParams = new FormData()
    const now = new Date()

    reqParams.append("user", user.id)
    reqParams.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
    if (imageUri.length > 0) {
      reqParams.append("img", {
        uri: imageUri,
        type: imageType,
        name: fileName || this.props.accountStore.user.id + "_" + now.toISOString() + ".jpg",
      })
    }

    const result = await api.updateProfile(this.props.accountStore.user.id, reqParams)

    if (result.kind === "ok") {
      this.props.accountStore.user.setProfileImage(result.data.img)
      this.props.navigation.pop()
      this.props.navigation.navigate(prevRouteName, {
        prevRouteName: this.getPrevPrevRouteName(route.params),
        profileName: this.getProfileName(route.params),
        profileGender: this.getProfileGender(route.params),
        profileBirth: this.getProfileBirth(route.params),
        imageUri: imageUri,
        imageType: imageType,
      })
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와의 연결이 원활하지 않아 프로필 이미지를 저장하지 못 하였습니다. \n잠시 후 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.pop()
        this.props.navigation.navigate(prevRouteName, {
          // ...this.props.route.params,
          prevRouteName: this.getPrevPrevRouteName(route.params),
          profileName: this.getProfileName(route.params),
          profileGender: this.getProfileGender(route.params),
          profileBirth: this.getProfileBirth(route.params),
          imageUri: imageUri,
          imageType: imageType,
        })
      }, 2000)
    }
  }

  async _submit(prevRouteName: string, imageUri: string, imageType: string, fileName?: string) {
    this.member = this.getMemberInfo(this.props.route.params)

    this.setState({ status: "pending" })
    const store = this.props.accountStore

    const now = new Date()
    const reqParams = new FormData()

    reqParams.append("id", this.member.id)
    reqParams.append("user", this.props.accountStore.user.id)

    // reqParams.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
    if (imageType && imageUri.length > 0) {
      const tempArray = String(imageUri).split("/")
      const fileName = tempArray[tempArray.length - 1]

      reqParams.append("img", {
        uri: imageUri,
        type: imageType,
        name: tempArray[0].includes("content:")
          ? fileName
          : this.props.accountStore.user.id + "_" + now.toISOString() + ".jpg",
      })
    }

    const result = await api.uploadChildProfileImage(reqParams, this.member.id)

    if (result.kind === "ok") {
      this.member.setProfileImage(result.data.img)
      store.changeFamilyMember(this.props.route.params.memberIndex, this.member)
      // this.props.navigation.pop()
      // this.props.navigation.navigate(prevRouteName)
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      Alert.alert(
        "",
        `서버에 저장하지 못 하였습니다. \n나중에 다시 시도해주세요.`,
        [{ text: "확인", onPress: () => null }],
        { cancelable: false },
      )
      console.tron.log("< ChangeProfileImageScreen > _submit, update error")
    }
  }
}
