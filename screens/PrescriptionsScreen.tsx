import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { onAction } from "mobx-state-tree"
import { Spinner, Toast, View } from "native-base"
import React, { Component } from "react"
import {
  Alert,
  AppState,
  BackHandler,
  Dimensions,
  Image,
  ImageStyle,
  Linking,
  NativeEventSubscription,
  Platform,
  RefreshControl,
  ScrollView,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import FastImage from "react-native-fast-image"
import { Notification } from "react-native-firebase/notifications"
import Modal from "react-native-modal"
import RBSheet from "react-native-raw-bottom-sheet"
import { SafeAreaView } from "react-native-safe-area-context"
import { SwipeListView } from "react-native-swipe-list-view"

import { api } from "@api/api"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import remoteConfig from "@react-native-firebase/remote-config"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import SvgDelete from "../assets/images/delete.svg"
import SvgSortChecked from "../assets/images/sortChecked.svg"
import SvgTransfer from "../assets/images/transfer.svg"
import CloseButton from "../components/CloseButton"
import EmptyPrescriptionList from "../components/EmptyPrescriptionList"
import { MemberSelectHeader } from "../components/MemberSelectHeader"
import { getPopupButton, Popup } from "../components/Popup"
import PrescriptionItem from "../components/PrescriptionItem"
import { Screen } from "../components/screen/screen"
import SendingUserQuestion from "../components/SendingUserQuestion"
import { Text } from "../components/StyledText"
import {
  HomeStackParamList,
  MainTabParamList,
  PrescriptionsStackParamList,
  RootStackParamList,
} from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { FamilyMember } from "../stores/FamilyMember"
import { Prescription, PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"
import { FamilyMemberType } from "./PrescriptionPreviewScreen"

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  backgroundColor: "#fcfbf9",
}
const TITLE_ROW: ViewStyle = {
  paddingTop: 22,
  paddingBottom: 16,
  paddingLeft: 16,
  paddingRight: 27,
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  backgroundColor: "#fcfbf9",
}
const CIRCLE: ImageStyle = {
  width: 40,
  height: 40,
  borderRadius: 20,
  borderWidth: 2,
  borderColor: color.palette.white,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: color.background,
}
const FULL: ViewStyle = { flex: 1 }
const SAMPLE_DESCRIPTION: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 22,
  letterSpacing: -0.3,
  color: color.palette.grey1,
  textAlign: "center",
}
const SAMPLE_DESCRIPTION_ACCENT: TextStyle = {
  ...SAMPLE_DESCRIPTION,
  color: color.primary,
}
const SAMPLE_DESCRIPTION_BOX: ViewStyle = {
  position: "relative",
  paddingHorizontal: 17,
  justifyContent: "center",
  paddingTop: 11,
  paddingBottom: 9,
}
const ROW_BACK: ViewStyle = {
  alignItems: "center",
  backgroundColor: color.background,
  flex: 1,
  flexDirection: "row",
  justifyContent: "flex-end",
  marginRight: 16,
}
const BACK_RIGHT_BUTTON: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
  width: 64,
  height: 64,
  backgroundColor: color.palette.salmon2,
  borderRadius: 5,
}
const BACK_RIGHT_TRANSFER_BUTTON: ViewStyle = {
  ...BACK_RIGHT_BUTTON,
  marginRight: 8,
  backgroundColor: color.palette.macaroniAndCheese,
}
const HEADER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  borderBottomWidth: 0,
  height: 56, // default is 64
  elevation: 0,
  shadowColor: "transparent",
  backgroundColor: "#fff",
  paddingHorizontal: 16,
}
const DUR_DESC_TITLE = {
  fontFamily: typography.primary,
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
  paddingBottom: 15,
}
const DUR_DESCRIPTION = { lineHeight: 22.5, letterSpacing: -0.3, color: color.palette.lightBlack }
const DUR_DESC_CONTAINER = { borderRadius: 10, backgroundColor: "rgb(239, 239, 239)", padding: 20 }
const ORANGE_DOT: ViewStyle = {
  position: "absolute",
  width: 14,
  height: 14,
  borderRadius: 7,
  borderWidth: 1,
  borderColor: color.palette.white,
  marginLeft: 11,
  backgroundColor: color.palette.orange,
}
const ROOT: ViewStyle = { backgroundColor: color.backgrounds.lightmode }
const ROW: ViewStyle = { flexDirection: "row" }
const SELECTED_MEMBER_NAME: TextStyle = {
  ...typography.title3,
  lineHeight: 25,
  paddingLeft: 8,
  marginRight: 8,
}
const TRIANGLE: ViewStyle = {
  height: 0,
  width: 0,
  borderStyle: "solid",
  borderLeftWidth: 5,
  borderRightWidth: 5,
  borderBottomWidth: 6,
  borderLeftColor: "transparent",
  borderRightColor: "transparent",
  borderBottomColor: "#7f7f7f",
  left: 0,
  zIndex: 1,
  marginLeft: 7,
  alignSelf: "center",
}
const DOWN = { transform: [{ rotate: "180deg" }] }
const HIDDEN_BUTTON_TEXT: TextStyle = {
  ...typography.sub1,
  color: color.palette.white,
}
const TITLE: TextStyle = { ...typography.title2, color: color.palette.charcoalGrey }
const CLOSE_BUTTON: ViewStyle = {
  width: 15.7,
  height: 15.7,
  marginRight: 0,
  paddingTop: 0,
  marginBottom: 12.4,
}
const SORT_CHECKED: ViewStyle = { marginLeft: 8.5, marginBottom: 4 }
const SORT_CHECKED_ROW_CENTER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingLeft: 8.5 + 14.8,
}
const BOLD_PRIMARY_TEXT: TextStyle = { fontFamily: typography.bold, color: color.primary }

function getImageSource(img: any) {
  if (img == null || img == "") {
    return {
      uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png`,
      priority: FastImage.priority.high,
    }
  } else {
    return {
      uri: img,
      priority: FastImage.priority.high,
    }
  }
}

enum SortMethod {
  IssueDate,
  TakingDrug,
  Disease,
  Hospital,
}

type PrescriptionsRouteProp = RouteProp<PrescriptionsStackParamList, "Prescriptions">

type PrescriptionsNavigationProp = CompositeNavigationProp<
  StackNavigationProp<PrescriptionsStackParamList, "Prescriptions">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<HomeStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: PrescriptionsNavigationProp
  route: PrescriptionsRouteProp
}

interface State {
  sortMethod: SortMethod
  appState: string
  isModalVisible: boolean
  isDurDescPopup: boolean
  requestedReviewDate: number
  todayDate: number
  requestedReviewResponse: string
  showedDurGuidance: boolean
  isDurGuidancePopup: boolean
  withinDurNotiPeriod: boolean
  previewSwipeableRowEnabled: boolean
  isAlarmNew: boolean
  expandFamily: boolean
  bottomSheetMode: "sort" | "transfer"
}

@inject("accountStore", "prescriptionStore")
@observer
export default class PrescriptionsScreen extends Component<Props, State> {
  static navigationOptions = {
    headerShown: false,
  }

  bottomSheetRef = React.createRef<RBSheet>()
  notificationListener: any
  changeEventListener: NativeEventSubscription
  onActionDisposer: any
  onTokenRefreshListener: any
  pinCodeEnabled: undefined | string
  selectedPrescriptionId: number
  _unsubscribeFocus: () => void

  constructor(props: Props) {
    super(props)
    this.state = {
      sortMethod: SortMethod.IssueDate,
      appState: AppState.currentState,
      isModalVisible: false,
      isDurDescPopup: false,
      requestedReviewDate: 0,
      todayDate: new Date().getMilliseconds(),
      requestedReviewResponse: "",
      showedDurGuidance: false,
      isDurGuidancePopup: false,
      withinDurNotiPeriod: false,
      previewSwipeableRowEnabled: false,
      isAlarmNew: false,
      expandFamily: false,
      bottomSheetMode: "sort",
    }
    this.props.prescriptionStore.setSelectedFamilyMemberKey("_u" + this.props.accountStore.user.id)

    this.onActionDisposer = onAction(this.props.prescriptionStore, (call) => {
      if (call.name === "addPrescription") {
        this.refresh()
      }
    })
  }

  async UNSAFE_componentWillMount() {
    const accountStore = this.props.accountStore
    const pStore = this.props.prescriptionStore
    const { user } = accountStore

    // AuthLoading 을 거치지 않은 경우(로그아웃 후 다른 user로 로그인 했을 때) 본인 처방전 목록 갱신 필요할 수 있음.
    const myPrescriptions = pStore.prescriptions.get(`_u${user.id}`)
    if (!myPrescriptions || (myPrescriptions.length > 0 && myPrescriptions[0].user != user.id)) {
      pStore.fetchPrescriptions(user.id, `_u${user.id}`)
    }

    if (accountStore.family) {
      setTimeout(() => {
        // 모든 가족회원 정보(14세 이상 가족회원) 가지고 오기까지 시간 걸림
        for (let i = 0; i < accountStore.family.length; i++) {
          const member = accountStore.family[i]

          if (member.family_user) {
            pStore.fetchPrescriptions(member.family_user, `_f${member.family_user}`, undefined)
          } else {
            pStore.fetchPrescriptions(accountStore.user.id, `_c${member.id}`, member.id)
          }
        }
      }, 800)
    }

    BackHandler.addEventListener("hardwareBackPress", this.backPressed)

    const showedDurGuidance = await AsyncStorage.getItem("showedDurGuidance")
    const requestedReviewResponse = await AsyncStorage.getItem("requestedReviewResponse")
    const requestedReviewDate = await AsyncStorage.getItem("requestedReviewDate")
    const previewSwipeableRowEnabled = await AsyncStorage.getItem("previewSwipeableRowEnabled")
    this.pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")

    if (requestedReviewDate) {
      this.setState({
        requestedReviewDate: Number(requestedReviewDate),
        requestedReviewResponse: requestedReviewResponse || "",
        todayDate: new Date().getMilliseconds(),
        showedDurGuidance: showedDurGuidance ? JSON.parse(showedDurGuidance) : false,
        previewSwipeableRowEnabled: previewSwipeableRowEnabled
          ? JSON.parse(previewSwipeableRowEnabled)
          : true,
      })
    } else {
      const todayTime = new Date().getMilliseconds()
      this.setState({
        requestedReviewDate: todayTime,
        requestedReviewResponse: requestedReviewResponse || "",
        todayDate: todayTime,
        showedDurGuidance: showedDurGuidance ? JSON.parse(showedDurGuidance) : false,
        previewSwipeableRowEnabled: previewSwipeableRowEnabled
          ? JSON.parse(previewSwipeableRowEnabled)
          : true,
      })
    }
  }

  componentDidMount() {
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)

    this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(async (fcmToken: string) => {
      console.tron.logImportant(
        "< PrescriptionsScreen > onTokenRefresh, fcmToken updated: " + fcmToken,
      )
      const user = this.props.accountStore.user

      const params = new FormData()
      params.append("user", user.id)
      params.append("firebase_token0", fcmToken)
      params.append("app_ver", packageJson.version)
      let regType
      switch (this.props.accountStore.signPath) {
        case "email":
          regType = "e"
          break
        case "kakao":
          regType = "k"
          break
        case "facebook":
          regType = "f"
          break
        case "naver":
          regType = "n"
          break
        case "apple":
          regType = "apple"
          break
        default:
          regType = "u"
      }
      params.append("reg_type", regType)
      params.append("app_device", Platform.OS + " v" + Platform.Version)

      api.updateProfile(user.id, params)
      user.setFirebaseToken0(fcmToken)
    })

    // Foreground 상태에서 notification 을 받았을 경우
    this.notificationListener = firebase.messaging().onMessage((message) => {
      // Process your notification as required
      if (this.props.prescriptionStore) {
        this.props.prescriptionStore.setStatus("pending")
      }
      // Toast.show({
      //   text: notification.title + (notification.body.length > 0 ? "\n\n" + notification.body : null),
      //   duration: 3500,
      //   position: "center",
      //   textStyle: styles.TOAST_TEXT,
      //   style: styles.TOAST_VIEW
      // })
      if (this) {
        this.refresh()
        setTimeout(() => {
          this.props.navigation.setParams({ prescriptionStore: this.props.prescriptionStore }) // TODO: remove?
          // this.forceUpdate()
        }, 1000)
      }
      this.props.prescriptionStore.setStatus("done")
    })
    this.changeEventListener = AppState.addEventListener("change", this._handleAppStateChange)

    // TODO: 복약순응도 기록 관련 코드 삭제
    // 복약종료된 처방전이 있으면 Rating screen 으로 이동
    // 본인처방전만 해당됨 - componentDidMount 안에 있으면 본인 처방전만 해당될 것임
    // const pStore = this.props.prescriptionStore
    // if (pStore.dosingEnded.length > 0) {
    //   this.props.navigation.navigate("DrugAdherenceRating", {
    //     prescriptionId: pStore.dosingEnded[0].id,
    //     issueDate: pStore.dosingEnded[0].issue_date,
    //     repTitle:
    //       pStore.dosingEnded[0].disease.length > 0
    //         ? pStore.dosingEnded[0].disease[0].one_liner.length > 0
    //           ? pStore.dosingEnded[0].disease[0].one_liner
    //           : pStore.dosingEnded[0].disease[0].rep_title
    //           ? pStore.dosingEnded[0].disease[0].rep_title
    //           : "질병정보 없음"
    //         : "질병정보 없음",
    //     neededSymptomImprovementRating: true,
    //   })
    //   pStore.removeDosingEndedPrescription(pStore.dosingEnded[0].id)
    // }
  }

  didFocus = () => {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })

    this.refresh()
  }

  componentWillUnmount() {
    this.onActionDisposer()
    this.onTokenRefreshListener()
    this.notificationListener()
    this._unsubscribeFocus()
    this.changeEventListener.remove()
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed)
  }

  showDurGuidance(): boolean {
    if (this.state.showedDurGuidance) {
      return true
    } else {
      this.setState({
        isDurDescPopup: false,
        isDurGuidancePopup: true,
        isModalVisible: true,
        withinDurNotiPeriod: false,
      })
      return false
    }
  }

  backPressed = () => {
    if (this.state.isModalVisible) {
      this.setState({
        isModalVisible: false,
      })
      return true
    }
  }

  _handleAppStateChange = (nextAppState: string) => {
    const { accountStore, prescriptionStore } = this.props
    if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
      if (this.pinCodeEnabled && JSON.parse(this.pinCodeEnabled)) {
        // App.tsx 에서 라우팅
        // this.props.navigation.navigate("CodeVerification", { purpose: "verification", preRoute: "Home" })
      } else {
        prescriptionStore.setStatus("pending")
        this.refresh()
        setTimeout(() => {
          // this.forceUpdate()
        }, 1000)
        prescriptionStore.setStatus("done")
      }
    }
    this.setState({ appState: nextAppState })
  }

  refresh(afterTransfer?: boolean) {
    const accountStore = this.props.accountStore
    const pStore = this.props.prescriptionStore

    // 현재 선택된 멤버의 처방전 목록을 먼저 갱신
    if (accountStore.selectedMemberKey === `_u${accountStore.user.id}`) {
      const myPrescriptions = pStore.prescriptions.get(accountStore.selectedMemberKey)
      if (
        !afterTransfer &&
        myPrescriptions &&
        myPrescriptions.length > 0 &&
        myPrescriptions[0].disease.length > 0 &&
        (myPrescriptions[0].disease[0].name_kr.includes("샘플 처방전") ||
          myPrescriptions[0].disease[0].name_kr.includes("처방전 예시"))
      ) {
        // 예시 처방전 있는 경우 fetch 하지 않음
      } else {
        pStore.fetchPrescriptions(accountStore.user.id, `_u${accountStore.user.id}`)
      }
    } else {
      for (let i = 0; i < accountStore.family.length; i++) {
        const member = accountStore.family[i]

        if (accountStore.selectedMemberKey === member.familyMemberKey) {
          if (member.family_user) {
            pStore.fetchPrescriptions(member.family_user, `_f${member.family_user}`)
          } else {
            pStore.fetchPrescriptions(accountStore.user.id, `_c${member.id}`, member.id)
          }
        }
      }
    }

    // // 나머지 멤버들의 처방전 목록 갱신
    if (accountStore.selectedMemberKey != `_u${accountStore.user.id}`) {
      const myPrescriptions = pStore.prescriptions.get(`_u${accountStore.user.id}`)
      if (
        !afterTransfer &&
        myPrescriptions?.length > 0 &&
        myPrescriptions[0].disease.length > 0 &&
        (myPrescriptions[0].disease[0].name_kr.includes("샘플 처방전") ||
          myPrescriptions[0].disease[0].name_kr.includes("처방전 예시"))
      ) {
        // 예시 처방전 있는 경우 fetch 하지 않음
      } else {
        pStore.fetchPrescriptions(accountStore.user.id, `_u${accountStore.user.id}`)
      }
    }
    for (let i = 0; i < accountStore.family.length; i++) {
      const member = accountStore.family[i]

      if (accountStore.selectedMemberKey != member.familyMemberKey) {
        if (member.family_user) {
          pStore.fetchPrescriptions(member.family_user, `_f${member.family_user}`)
        } else {
          pStore.fetchPrescriptions(accountStore.user.id, `_c${member.id}`, member.id)
        }
      }
    }

    accountStore.fetchFamily()
    accountStore.fetchFamilyToAgree()
  }

  async askToAcceptFamily() {
    const holdingMemberId = await AsyncStorage.getItem("holdFamily")
    this.props.accountStore.familyToAgree.forEach((member) => {
      if (
        member.status === "RJ" ||
        (holdingMemberId && member.id == Number.parseInt(holdingMemberId))
      ) {
        return
      }

      Alert.alert(
        "가족회원 동의 요청",
        member.name + `님이 당신을 가족회원으로 추가하였습니다. 동의하시겠습니까?`,
        [
          { text: "나중에", onPress: () => this.holdFamily(member), style: "cancel" },
          { text: "거절", onPress: () => this.rejectFamily(member) },
          { text: "동의", onPress: () => this.acceptFamily(member) },
        ],
        { cancelable: false },
      )
    })
  }

  async holdFamily(member: FamilyMember) {
    // start making calls
    const response = await api.apisauce.patch(`/user/family/${member.id}/`, {
      is_agree: false,
      status: "HD",
      user: member.user,
      family_user: member.family_user,
    })
    if (response.ok) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`가족회원 승인 요청을 보류하였습니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    }
    // 보류 시, 승인 응답자가 앱을 실행시킬 때에만 동일한 팝업을 띄어줌
    AsyncStorage.setItem("holdFamily", String(member.id))
  }

  async acceptFamily(member: FamilyMember) {
    // start making calls
    const response = await api.apisauce.patch(`/user/family/${member.id}/`, {
      is_agree: true,
      status: "AC",
      user: member.user,
      family_user: member.family_user,
    })
    if (response.ok) {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`가족회원으로 추가되는 것에 동의하였습니다.`}</Text>
        ),
        duration: 3000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    }
  }

  async rejectFamily(member: FamilyMember) {
    // start making calls
    const response = await api.apisauce.patch(`/user/family/${member.id}/`, {
      is_agree: false,
      status: "RJ",
      user: member.user,
      family_user: member.family_user,
    })
    if (response.ok) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`가족회원 승인 요청을 거절하였습니다.`}</Text>,
        duration: 3000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    }
  }

  setSortMethod(method: SortMethod) {
    this.setState({ sortMethod: method })
  }

  // TODO: 페이징 처리를 위하여 나중엔 백엔드에서 유저 처방전에 자녀 처방전은 빼고 줘야 함.
  removePrescriptionsOfChildren(prescriptions: Array<Prescription> | undefined): any {
    const filteredData = new Array<Prescription>()
    if (prescriptions) {
      for (let i = 0; i < prescriptions.length; i++) {
        const prescription = prescriptions[i]
        if (
          !(
            prescription.children &&
            this.props.accountStore.selectedMemberKey.includes(String(prescription.children))
          )
        ) {
          filteredData.push(prescription)
        }
      }
      return filteredData
    } else {
      return filteredData
    }
  }

  onSortPress = () => {
    this.setState({ bottomSheetMode: "sort" })
    this.bottomSheetRef.current.open()
  }

  render() {
    const { accountStore } = this.props
    const pStore = this.props.prescriptionStore
    const { family, selectedMemberKey, user } = accountStore
    const { bottomSheetMode, expandFamily, previewSwipeableRowEnabled, sortMethod } = this.state

    // const filteredPrescriptions = this.filterPrescriptions(
    //   this.removePrescriptionsOfChildren(
    //     this.props.prescriptionStore.prescriptions.get(selectedMemberKey),
    //   ),
    // )
    const filteredPrescriptions = this.filterPrescriptions(
      pStore.prescriptions.get(selectedMemberKey),
    )

    if (pStore.status == "error") {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (pStore.status == "unauthorized") {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.navigate("Auth")
      }, 2000)
    }

    const deviceWidth = Dimensions.get("window").width
    const deviceHeight =
      Platform.OS === "ios"
        ? Dimensions.get("window").height
        : require("react-native-extra-dimensions-android").get("REAL_WINDOW_HEIGHT")

    return (
      <Screen style={ROOT} preset="fixed">
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <MemberSelectHeader onSelectMember={this.onSelectMember} />
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={pStore.arePrescriptionsLoading}
              onRefresh={() => this.refresh()}
              tintColor={color.primary}
              titleColor={color.primary}
              colors={[color.primary]}
            />
          }
        >
          {remoteConfig().getBoolean("sending_user_question_enabled") ? (
            <SendingUserQuestion />
          ) : null}
          <View style={TITLE_ROW}>
            <Text style={TITLE}>처방 목록</Text>
            <TouchableOpacity
              onPress={this.onSortPress}
              hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
            >
              <View style={ROW}>
                <Text style={typography.sub2}>
                  {sortMethod === SortMethod.IssueDate
                    ? "진료일순"
                    : sortMethod === SortMethod.TakingDrug
                    ? "복용중"
                    : sortMethod === SortMethod.Disease
                    ? "질병별"
                    : "병원별"}
                </Text>
                <View style={[TRIANGLE, DOWN]} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={CONTENT_CONTAINER}>
            {Platform.OS === "ios" &&
            pStore.arePrescriptionsLoading &&
            pStore.prescriptions.get(selectedMemberKey) === undefined ? (
              <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
            ) : (
              <View>
                <SwipeListView
                  useNativeDriver
                  data={filteredPrescriptions}
                  renderItem={({ item, index }: { item: Prescription; index: number }) => {
                    return (
                      <PrescriptionItem
                        key={item.id}
                        index={index}
                        prescription={item}
                        onPress={(dosingDays) => this.onItemPress(item, dosingDays)}
                        onWithinDurNotiPeriod={() => {
                          this.setState({ withinDurNotiPeriod: true })
                        }}
                      />
                    )
                  }}
                  ItemSeparatorComponent={() => <View style={{ height: 8 }} />}
                  // onRefresh={() => this.refreshPrescriptions()}
                  // refreshing={pStore.isLoading}
                  keyExtractor={(item: Prescription) => item.id.toString()}
                  // extraData={{ extra: this.props.prescriptionStore.prescriptions }}
                  ListEmptyComponent={
                    <EmptyPrescriptionList
                      onLoadSamplePress={this.onLoadSamplePress}
                      showSampleButton={
                        (selectedMemberKey && selectedMemberKey.includes("_u")) || user.id === -1
                      }
                      userId={user.id}
                    />
                  }
                  renderHiddenItem={(
                    { item, index }: { item: Prescription; index: number },
                    rowMap: any,
                  ) => (
                    <View style={ROW_BACK}>
                      <TouchableOpacity
                        style={BACK_RIGHT_TRANSFER_BUTTON}
                        onPress={() => {
                          if (
                            item.disease.length > 0 &&
                            (item.disease[0].name_kr.includes("샘플 처방전") ||
                              item.disease[0].name_kr.includes("처방전 예시"))
                          ) {
                            Toast.show({
                              title: (
                                <Text
                                  style={styles.TOAST_TEXT}
                                >{`예시 처방전은 이동할 수 없습니다.`}</Text>
                              ),
                              duration: 2000,
                              placement: "top",
                              style: styles.TOAST_VIEW,
                            })
                            return
                          } else if (pStore.selectedFamilyMemberKey.includes("f")) {
                            Toast.show({
                              title: (
                                <Text
                                  style={styles.TOAST_TEXT}
                                >{`14세 이상 가족회원의 처방전 이동은 추후 지원 예정입니다.`}</Text>
                              ),
                              duration: 2500,
                              placement: "top",
                              style: styles.TOAST_VIEW,
                            })
                            return
                          }
                          this.selectedPrescriptionId = item.id
                          this.setState({ bottomSheetMode: "transfer" })
                          this.bottomSheetRef.current.open()
                        }}
                      >
                        <SvgTransfer width={16} height={16} />
                        <Text style={HIDDEN_BUTTON_TEXT}>이동</Text>
                      </TouchableOpacity>
                      {item.status === "IP" || item.status === "UL" ? null : (
                        <TouchableOpacity
                          style={BACK_RIGHT_BUTTON}
                          onPress={() => {
                            if (
                              item.disease.length > 0 &&
                              (item.disease[0].name_kr.includes("샘플 처방전") ||
                                item.disease[0].name_kr.includes("처방전 예시"))
                            ) {
                              this.props.prescriptionStore.removePrescription(
                                item,
                                selectedMemberKey,
                              )
                              return
                            } else if (
                              pStore.selectedFamilyMemberKey.includes("f") &&
                              item.status === "CF"
                            ) {
                              Toast.show({
                                title: (
                                  <Text
                                    style={styles.TOAST_TEXT}
                                  >{`14세 이상 가족회원의 처방전 삭제는 추후 지원 예정입니다.`}</Text>
                                ),
                                duration: 2500,
                                placement: "top",
                                style: styles.TOAST_VIEW,
                              })
                              return
                            }

                            Alert.alert(
                              "처방전 삭제",
                              `정말로 삭제하시겠습니까?\n삭제된 처방전 내역은 복구할 수 없습니다.`,
                              [
                                {
                                  text: "삭제",
                                  onPress: () => this.deletePrescription(item, selectedMemberKey),
                                  style: "cancel",
                                },
                                { text: "취소", onPress: () => null },
                              ],
                              { cancelable: false },
                            )
                          }}
                        >
                          <SvgDelete width={16} height={16} />
                          <Text style={HIDDEN_BUTTON_TEXT}>삭제</Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  )}
                  rightOpenValue={-152}
                  stopRightSwipe={-180}
                  disableRightSwipe
                  swipeToOpenVelocityContribution={100}
                  directionalDistanceChangeThreshold={Platform.OS === "ios" ? 2 : 10} // https://github.com/jemise111/react-native-swipe-list-view/issues/97
                  previewRowKey={
                    previewSwipeableRowEnabled && filteredPrescriptions.length > 0
                      ? filteredPrescriptions[0].id.toString()
                      : ""
                  }
                  previewDuration={500}
                  previewOpenValue={-144}
                  previewOpenDelay={1500}
                  onSwipeValueChange={this.onSwipeValueChange}
                  style={{ paddingBottom: 24 }}
                />
              </View>
            )}
            {(filteredPrescriptions.length === 1 || filteredPrescriptions.length === 2) &&
            filteredPrescriptions[0].disease.length > 0 &&
            (filteredPrescriptions[0].disease[0].name_kr.includes("처방전 예시") ||
              filteredPrescriptions[0].disease[0].name_kr.includes("샘플 처방전")) ? (
              <View style={{ paddingTop: 75, paddingBottom: 32 }}>
                <View style={SAMPLE_DESCRIPTION_BOX}>
                  <Text style={SAMPLE_DESCRIPTION}>
                    위 <Text style={SAMPLE_DESCRIPTION_ACCENT}>예시 처방</Text>은 처방전, 약봉투
                    등록 시
                  </Text>
                  <Text style={SAMPLE_DESCRIPTION}>자동적으로 사라집니다.</Text>
                </View>
              </View>
            ) : null}
          </View>
        </ScrollView>
        <Modal
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
          isVisible={this.state.isModalVisible}
          animationOut={"fadeOutDown"}
          // animationOutTiming={300}
          onBackdropPress={this.toggleModal}
        >
          {this.state.isDurDescPopup ? (
            <ScrollView contentContainerStyle={{ paddingTop: 40 }}>
              <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
              <View style={{ alignItems: "flex-end" }}>
                <CloseButton
                  imageStyle={{ tintColor: "rgb(255, 255, 255)" }}
                  onPress={() => this.toggleModal()}
                  style={CLOSE_BUTTON}
                />
              </View>
              <View style={DUR_DESC_CONTAINER}>
                <Image
                  source={require("../assets/images/popupDur.png")}
                  style={{ width: 295, height: 109, marginBottom: 20, alignSelf: "center" }}
                />
                <Text style={DUR_DESC_TITLE}>의약품 안전 점검(DUR) 이란?</Text>
                <Text style={DUR_DESCRIPTION}>
                  환자가 여러 의사에게 진료받을 경우 의사와 약사는 환자가 복용하고 있는 약을 알지
                  못하고 처방·조제하여 환자가 약물 부작용에 노출될 가능성이 있습니다. 의약품
                  처방·조제 시 병용금기 등 부적절한 약물사용을 사전에 점검할 수 있도록 의약품 안전
                  정보를 제공하는 것을 “DUR (Drug Utilization Review)” 또는 “의약품 안전 사용
                  서비스”라고 하며 저희 파프리카케어에서는 이해가 쉽도록 “의약품 안전 점검”이라고z
                  약칭하고 있습니다.{"\n\n"}
                  <Text style={{ fontFamily: typography.primary, color: color.palette.lightBlack }}>
                    *비교분석 기간:
                  </Text>{" "}
                  <Text style={{ color: color.palette.grey1 }}>
                    처방전 교부시점 3개월 이내에 등록 된 모든 약제의 의약품 안전성을 비교・분석
                    합니다.
                  </Text>
                  {"\n"}
                  <Text style={{ fontFamily: typography.primary, color: color.palette.lightBlack }}>
                    *알림 설정:
                  </Text>{" "}
                  <Text style={{ color: color.palette.grey1 }}>
                    내정보 {">"} 앱설정에서 분석 항목별로 알림을 활성화, 비활성화 할 수 있습니다.
                  </Text>
                </Text>
              </View>
            </ScrollView>
          ) : null}
          {this.state.isDurGuidancePopup
            ? Popup.renderAnimationPopupContent(
                require("../assets/animation_dur_guidance.json"),
                "의약품 안전 점검 알림 서비스 개시",
                "이제 처방전을 올리면 등록된 약제를 비교분석하여 임부금기, 병용금기, 동일성분 중복, 동일효능군 중복 약제가 있을 경우 해당 약제에 대한 알림과 설명을 제공합니다.",
                getPopupButton(() => {
                  AsyncStorage.setItem("showedDurGuidance", JSON.stringify(true))
                  const time = new Date().getMilliseconds()
                  this.setState({
                    requestedReviewDate: time,
                    requestedReviewResponse: "refused",
                    isModalVisible: !this.state.isModalVisible,
                    isDurGuidancePopup: false,
                    isDurDescPopup: true,
                    showedDurGuidance: true,
                  })
                }, "확인"),
              )
            : null}
          {!this.state.isDurGuidancePopup && !this.state.isDurDescPopup
            ? Popup.renderPopupContent(
                require("../assets/images/popupReview.png"),
                "평가해주세요",
                "따뜻한 응원도 따끔한 질책도 더 좋은 파프리카케어를 만드는데 도움이 됩니다. 소중한 별점과 후기를 남겨주세요.",
                getPopupButton(
                  () => {
                    const time = new Date().getMilliseconds()
                    AsyncStorage.multiSet([
                      ["requestedReviewDate", String(time)],
                      ["requestedReviewResponse", "refused"],
                    ])
                    this.setState({
                      requestedReviewDate: time,
                      requestedReviewResponse: "refused",
                      isModalVisible: !this.state.isModalVisible,
                    })
                  },
                  "다음에 하기",
                  "cancel",
                ),
                getPopupButton(this.writeReview, "평가하기"),
                getPopupButton(
                  () => {
                    const time = new Date().getMilliseconds()
                    AsyncStorage.multiSet([
                      ["requestedReviewDate", String(time)],
                      ["requestedReviewResponse", "doNotAskAgain"],
                    ])
                    this.setState({
                      requestedReviewDate: time,
                      requestedReviewResponse: "doNotAskAgain",
                      isModalVisible: !this.state.isModalVisible,
                    })
                  },
                  "다시 묻지 않기",
                  "transparent",
                ),
              )
            : null}
        </Modal>
        <RBSheet
          ref={this.bottomSheetRef}
          height={
            bottomSheetMode === "sort"
              ? styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * 4 + 72
              : styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * family.length + 72
          }
          openDuration={250}
          closeDuration={250}
          customStyles={{
            container: {
              backgroundColor: "transparent",
            },
          }}
        >
          <StatusBar backgroundColor={color.dimmedStatusBar} />
          {bottomSheetMode === "sort" ? this.renderSortingSheet() : this.renderTransferSheet()}
        </RBSheet>
      </Screen>
    )
  }

  renderSortingSheet() {
    const { sortMethod } = this.state
    return (
      <View style={{ marginBottom: 0 }}>
        <View style={styles.BOTTOM_SHEET_HEADER}>
          <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>정렬 및 필터</Text>
        </View>
        <View style={{ backgroundColor: color.palette.white }}>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              this.setState({ sortMethod: SortMethod.IssueDate })
            }}
          >
            {sortMethod == SortMethod.IssueDate ? (
              <View style={SORT_CHECKED_ROW_CENTER}>
                <Text style={[styles.BOTTOM_SHEET_BUTTON_TEXT, BOLD_PRIMARY_TEXT]}>진료일순</Text>
                <SvgSortChecked width={14.8} height={11} style={SORT_CHECKED} />
              </View>
            ) : (
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>진료일순</Text>
            )}
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              this.setState({ sortMethod: SortMethod.TakingDrug })
            }}
          >
            {sortMethod == SortMethod.TakingDrug ? (
              <View style={SORT_CHECKED_ROW_CENTER}>
                <Text style={[styles.BOTTOM_SHEET_BUTTON_TEXT, BOLD_PRIMARY_TEXT]}>복용중</Text>
                <SvgSortChecked width={14.8} height={11} style={SORT_CHECKED} />
              </View>
            ) : (
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>복용중</Text>
            )}
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              this.setState({ sortMethod: SortMethod.Hospital })
            }}
          >
            {sortMethod == SortMethod.Hospital ? (
              <View style={SORT_CHECKED_ROW_CENTER}>
                <Text style={[styles.BOTTOM_SHEET_BUTTON_TEXT, BOLD_PRIMARY_TEXT]}>병원별</Text>
                <SvgSortChecked width={14.8} height={11} style={SORT_CHECKED} />
              </View>
            ) : (
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>병원별</Text>
            )}
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              this.setState({ sortMethod: SortMethod.Disease })
            }}
          >
            {sortMethod == SortMethod.Disease ? (
              <View style={SORT_CHECKED_ROW_CENTER}>
                <Text style={[styles.BOTTOM_SHEET_BUTTON_TEXT, BOLD_PRIMARY_TEXT]}>질병별</Text>
                <SvgSortChecked width={14.8} height={11} style={SORT_CHECKED} />
              </View>
            ) : (
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>질병별</Text>
            )}
          </TouchableHighlight>
          <SafeAreaView>
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_CANCEL_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={() => this.bottomSheetRef.current.close()}
            >
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>취소</Text>
            </TouchableHighlight>
          </SafeAreaView>
        </View>
      </View>
    )
  }

  renderTransferSheet() {
    const { accountStore, prescriptionStore } = this.props
    const { selectedFamilyMemberKey } = prescriptionStore
    const { family, user } = accountStore
    return (
      <View style={{ marginBottom: 0 }}>
        <View style={styles.BOTTOM_SHEET_HEADER}>
          <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>
            누구의 처방전 목록으로 이동하시겠습니까?
          </Text>
        </View>
        <View style={{ backgroundColor: color.palette.white }}>
          {selectedFamilyMemberKey.includes("u") ? null : (
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={() => {
                this.bottomSheetRef.current.close()
                this.transferPrescription(
                  this.selectedPrescriptionId,
                  FamilyMemberType.Self,
                  accountStore.user.id,
                  accountStore.user.nickname,
                )
              }}
            >
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>{user.nickname}</Text>
            </TouchableHighlight>
          )}
          {family.map((member, index) => {
            if (
              (selectedFamilyMemberKey.includes("f") &&
                selectedFamilyMemberKey.includes(String(member.family_user))) ||
              (selectedFamilyMemberKey.includes("c") &&
                selectedFamilyMemberKey.includes(String(member.id)))
            ) {
              return null
            }
            return (
              <TouchableHighlight
                key={member.familyMemberKey}
                style={
                  index == family.length - 1
                    ? styles.BOTTOM_SHEET_LAST_BUTTON
                    : styles.BOTTOM_SHEET_BUTTON
                }
                underlayColor={color.palette.grey2}
                onPress={() => {
                  this.bottomSheetRef.current.close()
                  this.transferPrescription(
                    this.selectedPrescriptionId,
                    member.family_user ? FamilyMemberType.FamilyUser : FamilyMemberType.Child,
                    member.family_user ? member.family_user : member.id,
                    member.name,
                  )
                }}
              >
                <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>{member.name}</Text>
              </TouchableHighlight>
            )
          })}
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_CANCEL_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => this.bottomSheetRef.current.close()}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>취소</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  async transferPrescription(
    prescriptionId: number,
    memberType: FamilyMemberType,
    memberId: number,
    memberName: string,
  ) {
    const { accountStore } = this.props

    let params
    switch (memberType) {
      case FamilyMemberType.Self:
        params = {
          user: memberId,
          children: null,
        }
        break

      case FamilyMemberType.FamilyUser:
        params = {
          user: memberId,
          children: null,
        }
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`14세 이상 가족회원 처방전 목록으로의 이동은 추후 지원 예정입니다.`}</Text>
          ),
          duration: 2500,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        return

      case FamilyMemberType.Child:
        params = {
          user: accountStore.user.id,
          children: memberId,
        }
        break
    }

    const result = await api.updatePrescription(prescriptionId, params)
    if (result.kind === "ok") {
      this.setState({})
      this.refresh(true)
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`처방전이 ${memberName}님의 처방전 목록으로 이동하였습니다.`}</Text>
        ),
        duration: 2500,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`처방전 이동 실패. 나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  onSwipeValueChange = () => {
    const { previewSwipeableRowEnabled } = this.state
    if (previewSwipeableRowEnabled) {
      AsyncStorage.setItem("previewSwipeableRowEnabled", JSON.stringify(false))
      this.setState({ previewSwipeableRowEnabled: false })
    }
  }

  onLoadSamplePress = () => {
    const { accountStore, prescriptionStore } = this.props
    if (
      accountStore.user.id == -1 ||
      prescriptionStore.prescriptions.get(prescriptionStore.selectedFamilyMemberKey).length == 0
    ) {
      prescriptionStore.fetchSamplePrescription(
        2394,
        accountStore.user.id,
        prescriptionStore.selectedFamilyMemberKey,
      )
      prescriptionStore.fetchSamplePrescription(
        2395,
        accountStore.user.id,
        prescriptionStore.selectedFamilyMemberKey,
      )
    }
  }

  writeReview = async () => {
    await AsyncStorage.setItem("requestedReviewResponse", "reviewed")
    this.setState({
      requestedReviewResponse: "reviewed",
      isModalVisible: !this.state.isModalVisible,
    })
    if (Platform.OS === "android") {
      Linking.openURL("market://details?id=com.onions.papricacare")
    } else {
      Linking.openURL("itms://apps.apple.com/kr/app/파프리카케어/id1453787406")
    }
  }

  toggleModal = () => {
    // DUR 설명 팝업만 작동
    if (this.state.isDurDescPopup) {
      this.setState({ isModalVisible: !this.state.isModalVisible })
    }
  }

  // renderModalContent = () => (
  //   <View style={{ alignItems: "flex-end" }}>
  //     <StatusBar backgroundColor="#505050" barStyle="light-content" />
  //     <TouchableOpacity onPressOut={this._toggleModal}
  //       style={{ width: 24, height: 24, justifyContent: 'flex-start', alignItems: 'flex-end', marginBottom: 3 }}>
  //       <Image
  //         source={require('../assets/images/close2.png')}
  //         style={{
  //           width: 15.7,
  //           height: 15.7
  //         }}
  //       />
  //     </TouchableOpacity>
  //     <View style={MODAL_CONTENT}>
  //       <Text style={MODAL_TITLE}>의약품안전사용서비스(DUR) 란?</Text>
  //       <Text style={MODAL_TEXT}>환자가 여러 의사에게 진료 받을 경우 의사와 약사는 환자가 복용하고 있는 약을 알지 못하고 처방·조제하여 환자가 약물 부작용에 노출될 가능성 있습니다. 의약품 처방·조제 시 병용금기 등 의약품 안전성 관련 정보를 제공하여 부적절한 약물사용을 사전에 점검할 수 있도록 의약품 안전 정보를 제공하는 것을 “DUR (Drug Utilization Review) “ 또는 “의약품안전사용서비스”라고 합니다.</Text>
  //     </View>
  //   </View>
  // )

  async deletePrescription(prescription: Prescription, memberIndex: string) {
    const result = await api.deletePrescription(prescription.id)
    if (result.kind === "ok") {
      const { prescriptionStore } = this.props
      // 처방전 삭제할 때 처방전 목록을 새로 fetch 하기 위해 setStatus 에서 setStatusOfPrescriptions 으로 전환함.
      prescriptionStore.setStatusOfPrescriptions(memberIndex, "pending")
      prescriptionStore.removePrescription(prescription, memberIndex)
      prescriptionStore.setStatusOfPrescriptions(memberIndex, "done")
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`처방전을 삭제하였습니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.navigate("Auth")
      }, 2000)
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`처방전 삭제에 실패하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 3000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  filterPrescriptions(data: Array<Prescription> | undefined): any {
    let filteredData
    if (data) {
      filteredData = new Array<Prescription>(...data)
    } else {
      filteredData = new Array<Prescription>()
    }

    if (this.state.sortMethod == SortMethod.IssueDate) {
      filteredData
        .slice()
        .sort(
          (a, b) =>
            Number(b.issue_date!.replace(/-/g, "")) - Number(a.issue_date!.replace(/-/g, "")),
        )
    } else if (this.state.sortMethod == SortMethod.TakingDrug) {
      filteredData.slice().sort((a, b) => {
        if (b.remainingDosingDays > 0 && a.remainingDosingDays > 0) {
          return 0
        } else if (b.remainingDosingDays > 0 && a.remainingDosingDays <= 0) {
          return 1
        } else if (a.remainingDosingDays > 0 && b.remainingDosingDays <= 0) {
          return -1
        } else {
          return 0
        }
      })
    } else if (this.state.sortMethod == SortMethod.Disease) {
      filteredData.slice().sort((a, b) => {
        // 0보다 작은 경우 a를 b보다 낮은 색인으로 정렬 (a가 먼저옴)
        if (a.disease.length == 0) {
          return 1
        }
        if (b.disease.length == 0) {
          return -1
        }
        if (a.disease[0].one_liner.length > 0 && b.disease[0].one_liner.length > 0) {
          if (a.disease[0].one_liner < b.disease[0].one_liner) {
            return -1
          }
          if (a.disease[0].one_liner > b.disease[0].one_liner) {
            return 1
          }
        }
        if (a.disease[0].one_liner.length > 0) {
          if (a.disease[0].one_liner < b.disease[0].name_kr) {
            return -1
          }
          if (a.disease[0].one_liner > b.disease[0].name_kr) {
            return 1
          }
        }
        if (b.disease[0].one_liner.length > 0) {
          if (a.disease[0].name_kr < b.disease[0].one_liner) {
            return -1
          }
          if (a.disease[0].name_kr > b.disease[0].one_liner) {
            return 1
          }
        } else {
          if (a.disease[0].name_kr < b.disease[0].name_kr) {
            return -1
          }
          if (a.disease[0].name_kr > b.disease[0].name_kr) {
            return 1
          }
        }
        return 0
      })
    } else if (this.state.sortMethod == SortMethod.Hospital) {
      filteredData.slice().sort((a, b) => {
        if (a.hospital == null) {
          return 1
        }
        if (b.hospital == null) {
          return -1
        }
        if (a.hospital!.name < b.hospital!.name) {
          return -1
        }
        if (a.hospital!.name > b.hospital!.name) {
          return 1
        }
        return 0
      })
    }

    // 처리중인 처방전을 목록 상단으로 올림
    filteredData.slice().sort((a, b) => {
      if (a.status != "CF") {
        return -1
      } else {
        return 0
      }
    })

    return filteredData
  }

  onItemPress(item: Prescription, dosingDays: number) {
    const { navigation, route } = this.props
    if (item.status === "RJ" || item.status === "UL" || item.status === "IP") {
      // @ts-ignore
      navigation.navigate("PendingPrescriptionDetail", {
        item: item,
        index: undefined,
        prevRouteName: route.name,
      })
    } else {
      // @ts-ignore
      navigation.navigate("PrescriptionDetail", {
        prescriptionId: item.id,
        startDate: item.start_at,
        endDate: item.end_at,
      })
    }
  }

  _search = () => {
    this.props.navigation.navigate("Search")
  }

  onSelectMember = (memberKey: string, member?: FamilyMember) => {
    const { accountStore, prescriptionStore } = this.props
    prescriptionStore.setSelectedFamilyMemberKey(memberKey)

    if (accountStore.status === "error" || prescriptionStore.status === "error") {
      this.refresh()
    } else if (memberKey && !prescriptionStore.prescriptions.has(memberKey)) {
      if (memberKey.includes("_u")) {
        prescriptionStore.fetchPrescriptions(accountStore.user.id, memberKey)
      } else if (memberKey.includes("_c")) {
        prescriptionStore.fetchPrescriptions(
          accountStore.user.id,
          memberKey,
          Number(memberKey.substr(2)),
        )
      } else {
        prescriptionStore.fetchPrescriptions(member.family_user, memberKey)
      }
    }
  }

  getFamilyMemberMapKey(memberId: number): string {
    const accountStore = this.props.accountStore
    if (accountStore.user.id == memberId) {
      return "_u" + accountStore.user.id
    } else {
      for (let i = 0; i < accountStore.family.length; i++) {
        const member = accountStore.family[i]
        if (member.id == memberId) {
          return member.familyMemberKey
        }
      }
    }
  }
}
