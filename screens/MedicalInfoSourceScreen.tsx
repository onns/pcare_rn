import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content, ScrollView } from "native-base"
import React from "react"
import { TextStyle, View, ViewStyle } from "react-native"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { Text } from "../components/StyledText"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  backgroundColor: "#fff",
  marginTop: 12,
  padding: 20,
}
const TITLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  color: color.palette.grey1,
  marginTop: 6,
  marginBottom: 14,
}
const LABEL: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  color: color.text,
  lineHeight: 24,
}
const DESCRIPTION: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  color: color.palette.brownGrey,
  lineHeight: 24,
}

type MedicalInfoSourceRouteProp = RouteProp<MyPageStackParamList, "MedicalInfoSource">

type MedicalInfoSourceNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "MedicalInfoSource">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: MedicalInfoSourceNavigationProp
  route: MedicalInfoSourceRouteProp
}

@inject("accountStore")
@observer
export default class MedicalInfoSourceScreen extends React.Component<Props> {
  static navigationOptions = {
    title: "의료정보 출처",
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  render() {
    return (
      <View style={{ backgroundColor: "rgb(239, 239, 239)" }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <Text style={TITLE}>출처 정보</Text>
          <Text style={LABEL}>
            약제설명: <Text style={DESCRIPTION}>파프리카케어 자체 제작</Text>
          </Text>
          <Text style={LABEL}>
            질병간단설명: <Text style={DESCRIPTION}>파프리카케어 자체 제작</Text>
          </Text>
          <Text style={LABEL}>
            관련질병 상세보기: <Text style={DESCRIPTION}>질병관리본부, 대한의학회</Text>
          </Text>
          <Text style={LABEL}>
            의약품안전점검(DUR): <Text style={DESCRIPTION}>건강보험심사평가원</Text>
          </Text>
        </ScrollView>
      </View>
    )
  }
}
