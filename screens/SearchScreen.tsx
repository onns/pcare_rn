import React, { FC } from "react"
import { useWindowDimensions } from "react-native"
import { SceneMap, TabBar, TabView } from "react-native-tab-view"

import { SearchDrugMode, SearchDrugScreen } from "@features/drug-v4/SearchDrugScreen"
import {
  SearchHospitalMode,
  SearchHospitalScreen,
} from "@features/hospital-v4/SearchHospitalScreen"

/* eslint-disable react/display-name */
import { sendLogEvent } from "../lib/analytics"
import { typography } from "../theme"
import { SearchDiseaseMode, SearchDiseaseScreen } from "./SearchDiseaseScreen"

const renderScene = SceneMap({
  disease: () => <SearchDiseaseScreen mode={SearchDiseaseMode.Search} />,
  drug: () => <SearchDrugScreen mode={SearchDrugMode.Search} />,
  hospital: () => <SearchHospitalScreen mode={SearchHospitalMode.Search} />,
})

export const SearchScreen: FC = () => {
  const layout = useWindowDimensions()

  const [index, setIndex] = React.useState(0)
  const [routes] = React.useState([
    { key: "disease", title: "질병" },
    { key: "drug", title: "약품" },
    { key: "hospital", title: "병원" },
  ])

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{
        borderBottomWidth: 2,
        // borderBottomColor: config.tokens.color.iconSelected,
        borderBottomColor: "#383D46",
      }}
      style={{
        // height: 34,
        backgroundColor: "#fff",
        borderColor: "#E6E8ED",
        borderBottomWidth: 1,
        padding: 0,
        elevation: 0,
      }}
      tabStyle={{
        // width: "auto",
        // height: 34,
        paddingVertical: 0,
        paddingHorizontal: 16,
      }}
      labelStyle={{
        fontFamily: typography.primary,
        fontWeight: "600",
        fontSize: 16,
        lineHeight: 25,
        letterSpacing: -0.5,
        paddingTop: 4,
        paddingBottom: 10,
      }}
      // activeColor={config.tokens.color.action}
      activeColor="#565E6D"
      inactiveColor="#7F8694"
    />
  )

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={(index) => {
        setIndex(index)
        sendLogEvent("search_tab_category", { tab_category: routes[index].key })
      }}
      initialLayout={{ width: layout.width }}
      sceneContainerStyle={{ paddingTop: 12, backgroundColor: "#fff" }}
    />
  )
}

export default SearchScreen
