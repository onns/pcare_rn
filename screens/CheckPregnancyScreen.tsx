import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content, Toast } from "native-base"
import React from "react"
import { Dimensions, ScrollView, StatusBar, TextStyle, View, ViewStyle } from "react-native"
import Modal from "react-native-modal"

import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { ParamListBase, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { api, GeneralResult } from "../api"
import CloseButton from "../components/CloseButton"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { AuthStackParamList, RootStackParamList } from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, styles, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  flexDirection: "column",
  // justifyContent: 'space-between',
  paddingHorizontal: 20,
}
const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 34,
  paddingHorizontal: 6,
  paddingBottom: 30,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 34,
  lineHeight: 44,
  letterSpacing: -1.36,
  color: color.text,
  textAlign: "center",
}
const TITLE_DOT: TextStyle = {
  fontSize: 34,
  color: "rgb(255, 132, 26)",
}
const DESCRIPTION: TextStyle = {
  fontSize: 15,
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.text,
  paddingBottom: 12,
  textAlign: "center",
  marginBottom: 60,
}
const DESC_ACCENT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.palette.waterBlue,
  textDecorationLine: "underline",
}
const ANSWER_BUTTON: ViewStyle = {
  borderColor: color.palette.lightGrey2,
  marginBottom: 10,
}
const ANSWER_BUTTON_SELECTED: ViewStyle = {
  ...ANSWER_BUTTON,
  backgroundColor: color.palette.lightGrey2,
}
const ANSWER_TEXT: TextStyle = {
  fontSize: 17,
  color: color.text,
}
const ANSWER_TEXT_BOLD: TextStyle = {
  fontSize: 17,
  fontWeight: "bold",
  color: color.text,
}
const BOTTOM_BUTTON: ViewStyle = {
  flex: 1,
  flexDirection: "column",
  justifyContent: "flex-end",
  // alignItems: 'stretch',
  // paddingTop: 20,
  marginBottom: 20,
}
const CLOSE_BUTTON: ViewStyle = {
  width: 15.7,
  height: 15.7,
  marginRight: 0,
  paddingTop: 0,
  marginBottom: 12.4,
}
const POPUP_TITLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
  paddingBottom: 15,
  color: color.palette.lightBlack,
}
const POPUP_CONTENT: TextStyle = {
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.palette.lightBlack,
}
const deviceWidth = Dimensions.get("window").width
const deviceHeight = Dimensions.get("window").height

const modalContents = {
  preg: {
    title: "임부금기 약제",
    content:
      "임신 또는 임신하고 있을 가능성이 있는 환자가 복용하면 태아기형 및 태아독성 등 태아에 대한 위험성이 있는 약물입니다.",
  },
}

type CheckPregnancyScreenRouteProp = RouteProp<AuthStackParamList, "CheckPregnancy">

interface Props {
  accountStore: AccountStore
  navigation: StackNavigationProp<ParamListBase>
  route: CheckPregnancyScreenRouteProp
}

interface State {
  isPregnant: boolean | undefined
  isModalVisible: boolean
}

@inject("accountStore")
@observer
export default class CheckPregnancyScreen extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      isPregnant: undefined,
      isModalVisible: false,
    }
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  openModal() {
    this.setState({
      isModalVisible: true,
    })
  }

  render() {
    const { isPregnant } = this.state
    return (
      <View style={BACKGROUND}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={TITLE}>
            <Text style={TITLE_TEXT}>
              임신 중 혹은 임신을 예정하고 계신가요
              <Text style={TITLE_DOT}>?</Text>
            </Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Text style={DESCRIPTION}>
              임신 여부를 선택하시면{" "}
              <Text style={DESC_ACCENT} onPress={() => this.openModal()}>
                임부금기
              </Text>{" "}
              약물에 대한 의약품 안전성 알림 및 상세 설명을 제공해 드립니다.
            </Text>

            <Button
              bordered
              block
              rounded
              onPress={() => this.setPregnant(false)}
              style={isPregnant == undefined || isPregnant ? ANSWER_BUTTON : ANSWER_BUTTON_SELECTED}
            >
              <Text style={isPregnant == undefined || isPregnant ? ANSWER_TEXT : ANSWER_TEXT_BOLD}>
                아니오.
              </Text>
            </Button>
            <Button
              bordered
              block
              rounded
              onPress={() => this.setPregnant(true)}
              style={
                isPregnant == undefined || !isPregnant ? ANSWER_BUTTON : ANSWER_BUTTON_SELECTED
              }
            >
              <Text style={isPregnant == undefined || !isPregnant ? ANSWER_TEXT : ANSWER_TEXT_BOLD}>
                네, 임신 중 또는 임신 예정 입니다.
              </Text>
            </Button>
          </View>

          <View style={BOTTOM_BUTTON}>
            <Button disabled={isPregnant == undefined} block rounded onPress={() => this.next()}>
              <Text style={{ fontWeight: "bold" }}>다음</Text>
            </Button>
          </View>

          <Modal
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            isVisible={this.state.isModalVisible}
            animationOut={"fadeOutDown"}
            // animationOutTiming={300}
            onBackdropPress={this.toggleModal}
          >
            <ScrollView contentContainerStyle={{ flex: 1, justifyContent: "center" }}>
              <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
              <View style={{ alignItems: "flex-end" }}>
                <CloseButton
                  imageStyle={{ tintColor: "rgb(255, 255, 255)" }}
                  onPress={() => this.toggleModal()}
                  style={CLOSE_BUTTON}
                />
              </View>
              <View
                style={{
                  borderRadius: 10,
                  backgroundColor: "rgb(239, 239, 239)",
                  padding: 20,
                }}
              >
                <Text style={POPUP_TITLE}>{modalContents.preg.title}</Text>
                <Text style={POPUP_CONTENT}>{modalContents.preg.content}</Text>
              </View>
            </ScrollView>
          </Modal>
        </ScrollView>
      </View>
    )
  }

  setPregnant(isPregnant: boolean) {
    this.setState({
      isPregnant: isPregnant,
    })
  }

  async next() {
    if (!this.state.isPregnant) {
      this.props.navigation.navigate("Welcome")
      return
    }

    const userId = this.props.accountStore.user.id

    const params = new FormData()
    params.append("user", userId)
    params.append("is_agree", true)
    params.append("dur_preg", "y")

    // start making calls
    const result: GeneralResult = await api.updateProfile(userId, params)

    if (result.kind == "ok") {
      const pushNotiEnabled = await AsyncStorage.getItem("pushNotiEnabled")
      if (!pushNotiEnabled) {
        await AsyncStorage.setItem("pushNotiEnabled", JSON.stringify(true))
      }
      this.props.navigation.navigate("Welcome")
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Welcome"), 2000)
    }
  }

  closeModal = () => {
    if (this.state.isModalVisible) {
      this.setState({ isModalVisible: false })
      return true
    }
    return false
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }
}
