import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { ScrollView, TextArea, Toast, View } from "native-base"
import React, { Component } from "react"
import { TextStyle, TouchableOpacity, ViewStyle } from "react-native"

import { api } from "@api/api"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { getPopupButton, Popup } from "../components/Popup"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, styles, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  paddingTop: 10,
  backgroundColor: "#fff",
  justifyContent: "center",
  paddingHorizontal: 20,
}

type DeleteAccountWhyRouteProp = RouteProp<MyPageStackParamList, "DeleteAccountWhy">

type DeleteAccountWhyNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "DeleteAccountWhy">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AuthStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: DeleteAccountWhyNavigationProp
  route: DeleteAccountWhyRouteProp
}

interface State {
  reason: string
  memo: string
  isPopupVisible: boolean
  popupTitle: string
  popupMessage: string
  popupButton1: React.ReactElement | undefined
  popupButton2: React.ReactElement | undefined
  popupButton3: React.ReactElement | undefined
  popupButtonGroupStyle: ViewStyle | undefined
}

const OPTION_TEXT: TextStyle = {
  fontSize: 16,
  lineHeight: 26,
  letterSpacing: -0.64,
  textAlign: "center",
}
const OPTION_TEXT_SELECTED: TextStyle = {
  ...OPTION_TEXT,
  fontFamily: typography.primary,
}
const OPTION_BOX: ViewStyle = {
  height: 42,
  borderRadius: 5,
  borderColor: color.palette.lightGrey2,
  borderWidth: 1,
  marginBottom: 10,
  justifyContent: "center",
}
const OPTION_BOX_SELECTED: ViewStyle = {
  ...OPTION_BOX,
  backgroundColor: color.palette.lightGrey2,
}
const TITLE: TextStyle = {
  fontSize: 22,
  fontWeight: "bold",
  letterSpacing: -0.5,
  lineHeight: 30,
  color: color.text,
  textAlign: "center",
  marginBottom: 30,
}

@inject("accountStore")
@observer
export default class DeleteAccountWhyScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "회원 탈퇴",
    }
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      reason: "",
      memo: "",
      isPopupVisible: false,
      popupTitle: "",
      popupMessage: "",
      popupButton1: undefined,
      popupButton2: undefined,
      popupButton3: undefined,
      popupButtonGroupStyle: undefined,
    }
  }

  componentDidMount() {
    const { route } = this.props
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })
  }

  async _submit() {
    this.setState({
      isPopupVisible: true,
      popupTitle: "정말로 탈퇴하시겠습니까?",
      popupMessage: `탈퇴 시 회원님의 개인 정보 및 이용 기록은 모두 삭제되며, 이후 복구할 수 없습니다.`,
      popupButton1: getPopupButton(
        () => {
          this.setState({ isPopupVisible: false })
          this.deleteAccount()
        },
        "정말로 탈퇴",
        "cancel",
      ),
      popupButton2: getPopupButton(() => {
        this.setState({ isPopupVisible: false })
        this.props.navigation.popToTop()
      }, "취소"),
      popupButton3: undefined,
      popupButtonGroupStyle: undefined,
    })
  }

  async deleteAccount() {
    const userId = this.props.accountStore.user.id

    const params = new FormData()
    params.append("user", userId)
    params.append("reason", this.state.reason)
    params.append("memo", this.state.memo)

    const result = await api.updateProfile(userId, params)
    if (result.kind === "ok") {
      const result2 = await api.deleteAccount(userId)
      if (result2.kind === "ok") {
        this.setState({
          isPopupVisible: true,
          popupTitle: "회원 탈퇴 성공",
          popupMessage: `그동안 저희 파프리카케어 서비스를 이용해 주셔서 감사합니다. 보다 나은 서비스로 다시 찾아 뵐 수 있기를 바라며 언제나 고객님이 건강하시기를 바랍니다.`,
          popupButton1: getPopupButton(() => {
            this.setState({ isPopupVisible: false })
            this.initiateApp()
          }, "확인"),
          popupButton2: undefined,
          popupButton3: undefined,
          popupButtonGroupStyle: undefined,
        })
      } else if (result2.kind === "unauthorized") {
        this.showToastUnauthorized()
      } else {
        this.showToastConnectionError()
      }
    } else if (result.kind === "unauthorized") {
      this.showToastUnauthorized()
    } else {
      this.showToastConnectionError()
    }
  }

  private showToastConnectionError() {
    Toast.show({
      title: (
        <Text
          style={styles.TOAST_TEXT}
        >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
      ),
      duration: 2000,
      placement: "top",
      style: styles.TOAST_VIEW,
    })
    setTimeout(() => this.props.navigation.pop(), 2000)
  }

  private showToastUnauthorized() {
    Toast.show({
      title: (
        <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
      ),
      duration: 2000,
      placement: "top",
      style: styles.TOAST_VIEW,
    })
    setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
  }

  async initiateApp() {
    const { accountStore, navigation } = this.props

    api.apisauce.deleteHeader("Authorization")
    accountStore.initiate()

    await AsyncStorage.clear()
    navigation.popToTop()
    navigation.navigate("Auth")
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: color.background }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={{ paddingHorizontal: 10 }}>
            <Text style={TITLE}>탈퇴 이유가 무엇인가요?</Text>
            <TouchableOpacity
              style={this.state.reason == "0" ? OPTION_BOX_SELECTED : OPTION_BOX}
              onPress={() => this.setState({ reason: "0" })}
            >
              <Text style={this.state.reason == "0" ? OPTION_TEXT_SELECTED : OPTION_TEXT}>
                기능이 유용하지 않음
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={this.state.reason == "1" ? OPTION_BOX_SELECTED : OPTION_BOX}
              onPress={() => this.setState({ reason: "1" })}
            >
              <Text style={this.state.reason == "1" ? OPTION_TEXT_SELECTED : OPTION_TEXT}>
                사용 불편
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={this.state.reason == "2" ? OPTION_BOX_SELECTED : OPTION_BOX}
              onPress={() => this.setState({ reason: "2" })}
            >
              <Text style={this.state.reason == "2" ? OPTION_TEXT_SELECTED : OPTION_TEXT}>
                개인정보 보안 우려
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={this.state.reason == "3" ? OPTION_BOX_SELECTED : OPTION_BOX}
              onPress={() => this.setState({ reason: "3" })}
            >
              <Text style={this.state.reason == "3" ? OPTION_TEXT_SELECTED : OPTION_TEXT}>
                콘텐츠 미흡
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={this.state.reason == "4" ? OPTION_BOX_SELECTED : OPTION_BOX}
              onPress={() => this.setState({ reason: "4" })}
            >
              <Text style={this.state.reason == "4" ? OPTION_TEXT_SELECTED : OPTION_TEXT}>
                이유 없음
              </Text>
            </TouchableOpacity>
            <TextArea
              rowSpan={4}
              bordered
              underline
              placeholder="기타 의견은 이곳에 적어주세요."
              placeholderTextColor="rgb(168, 168, 168)"
              style={{
                borderRadius: 5,
                borderWidth: 1,
                borderColor: color.palette.lightGrey2,
                fontFamily: typography.primary,
                fontSize: 16,
                lineHeight: 26,
                letterSpacing: -0.64,
                paddingHorizontal: 13,
              }}
              value={this.state.memo}
              onChangeText={(text) => {
                this.setState({
                  memo: text.substr(0, 1000),
                })
              }}
            />
          </View>
          <View style={{ height: 42, marginTop: 40, marginBottom: 20 }}>
            <Button
              primary
              block
              style={{ height: 42, flex: 1 }}
              onPress={() => this._submit()}
              disabled={this.state.reason.length == 0 && this.state.memo.length == 0}
            >
              <Text style={{ fontSize: 17, fontWeight: "bold", color: "#fff" }}>회원 탈퇴</Text>
            </Button>
          </View>
        </ScrollView>

        <Popup
          isVisible={this.state.isPopupVisible}
          title={this.state.popupTitle}
          message={this.state.popupMessage}
          button1={this.state.popupButton1}
          button2={this.state.popupButton2}
          button3={this.state.popupButton3}
          buttonGroupStyle={this.state.popupButtonGroupStyle}
        />
      </View>
    )
  }
}
