import { DateTime } from "luxon"
import { values } from "mobx"
import { inject, observer } from "mobx-react"
import { Spinner, Toast, View } from "native-base"
import React, { Component } from "react"
import {
  AppState,
  FlatList,
  Platform,
  RefreshControl,
  ScrollView,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  ViewStyle,
} from "react-native"
import FastImage from "react-native-fast-image"
import RBSheet from "react-native-raw-bottom-sheet"

import { api } from "@api/api"
import { POINT_TYPES } from "@features/points/constants"
import { validatePoints } from "@features/points/validator"
import { getPrescriptionTitle } from "@lib/prescription"
import remoteConfig from "@react-native-firebase/remote-config"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import CloseButton from "../components/CloseButton"
import { EmptyDrugsToTakeList } from "../components/EmptyDrugsToTakeList"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { TakingPrescription } from "../components/TakingPrescription"
import { getNow as getNowTimeSlotName, TimeSlotRow } from "../components/TimeSlotRow"
import {
  DetailsStackParamList,
  MainTabParamList,
  RootStackParamList,
  TakingDrugStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { DrugToTake } from "../stores/drug-to-take/DrugToTake"
import { DrugToTakeGroup } from "../stores/drug-to-take/DrugToTakeGroup"
import { MedicationSchedule } from "../stores/drug-to-take/MedicationSchedule"
import { TakingDrugStore } from "../stores/drug-to-take/TakingDrugStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

type MedicDetailRouteProp = RouteProp<TakingDrugStackParamList, "MedicationScheduleDetail">

type MedicDetailNavigationProp = CompositeNavigationProp<
  StackNavigationProp<TakingDrugStackParamList, "MedicationScheduleDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  takingDrugStore: TakingDrugStore
  navigation: MedicDetailNavigationProp
  route: MedicDetailRouteProp
}

interface State {
  appState: string
  isModalVisible: boolean
  isDurDescPopup: boolean
  requestedReviewDate: number
  todayDate: number
  requestedReviewResponse: string
  isDurGuidancePopup: boolean
  previewSwipeableRowEnabled: boolean
  showedDurGuidance: boolean
  timeSlot: string | undefined
  drugsTodayUnderlineWidth: number
  drugsTodayUnderlineTop: number
  drugsOnceUnderlineWidth: number
  drugsOnceUnderlineTop: number
}

const ROOT: ViewStyle = {
  backgroundColor: color.background,
}
const XBTN: ViewStyle = {
  height: 48,
  paddingLeft: 20,
  marginRight: 0,
  justifyContent: "center",
  alignItems: "flex-start",
}
const CONTENT: ViewStyle = {
  paddingHorizontal: 7,
  overflow: "hidden",
  backgroundColor: color.background,
  justifyContent: "center",
  alignItems: "center",
}

const HEADER_DATE: ViewStyle = {
  height: 106,
  borderBottomWidth: 1,
  borderBottomColor: color.line,
  backgroundColor: color.background,
}

const DATETIME = {
  fontSize: 20,
  fontFamily: Platform.select({
    ios: "NotoSansKR-Bold",
    android: "NotoSansKR-Bold-Hestia",
  }),
  color: "#272727",
  margin: 10,
}
const TAKE_DRUG_TOAST_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
  letterSpacing: -0.68,
  color: color.palette.white,
  paddingHorizontal: 16,
  textAlign: "center",
}
const TAKE_DRUG_TOAST_VIEW: ViewStyle = {
  backgroundColor: "rgba(47, 50, 51, 0.3)",
  marginTop: 44,
  marginHorizontal: 16,
  marginBottom: 20,
  borderRadius: 10,
  paddingTop: 12,
  paddingBottom: 11,
  shadowColor: "#29453b30",
  shadowOffset: {
    width: 0,
    height: 10,
  },
  shadowOpacity: 0.3,
  shadowRadius: 20,
  top: 280,
}
const TAKE_DRUG_TOAST_TEXT_CANCEL: TextStyle = {
  ...TAKE_DRUG_TOAST_TEXT,
  color: color.palette.white,
}
const TAKE_DRUG_TOAST_VIEW_CANCEL: ViewStyle = {
  ...TAKE_DRUG_TOAST_VIEW,
  backgroundColor: "rgba(47, 50, 51, 0.3)",
  borderColor: "rgba(47, 50, 51, 0.3)",
}
const COLOR_UNDERLINE_BOX: ViewStyle = {
  position: "absolute",
  left: 24,
}
const COLOR_UNDERLINE_PAINT: ViewStyle = {
  height: 8,
  opacity: 0.2,
  backgroundColor: color.palette.darkMint,
}
const SECTION_HEADER: ViewStyle = {
  flexDirection: "row",
  marginTop: 48,
  paddingHorizontal: 24,
}
const TAKING_ITEM_LIST = { marginTop: 16 }
const TAKE_DRUG_ONCE_TEXT: TextStyle = {
  ...typography.title2,
  fontSize: 22,
}

@inject("accountStore", "prescriptionStore", "takingDrugStore")
@observer
export default class MedicationScheduleDetailScreen extends Component<Props, State> {
  static navigationOptions = {
    headerShown: false,
  }

  bottomSheetRef = React.createRef<RBSheet>()
  date: any
  pinCodeEnabled: undefined | string
  selectedDrugToTakeGroup: DrugToTakeGroup | undefined

  constructor(props: Props) {
    super(props)

    this.state = {
      appState: AppState.currentState,
      isModalVisible: false,
      isDurDescPopup: false,
      requestedReviewDate: 0,
      todayDate: new Date().getMilliseconds(),
      requestedReviewResponse: "",
      showedDurGuidance: false,
      isDurGuidancePopup: false,
      previewSwipeableRowEnabled: false,
      timeSlot: undefined,
      drugsTodayUnderlineWidth: 120,
      drugsTodayUnderlineTop: 20,
      drugsOnceUnderlineWidth: 120,
      drugsOnceUnderlineTop: 120,
    }
    this.date = this.props.route.params.date
  }

  async componentDidMount() {
    this.fetchDrugsToTake()
  }

  _handleAppStateChange = (nextAppState: string) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
      this.props.prescriptionStore.setStatus("pending")
      this.fetchDrugsToTake()
      this.props.prescriptionStore.setStatus("done")
    }
    this.setState({ appState: nextAppState })
  }

  fetchDrugsToTake() {
    const { accountStore, takingDrugStore } = this.props
    const { selectedMemberKey } = accountStore

    if (selectedMemberKey) {
      if (selectedMemberKey.includes("_u")) {
        takingDrugStore.fetchDrugsToTake(
          accountStore.user.id,
          selectedMemberKey,
          undefined,
          false,
          this.date.dateString,
        )
      } else if (selectedMemberKey.includes("_c")) {
        takingDrugStore.fetchDrugsToTake(
          accountStore.user.id,
          selectedMemberKey,
          Number(selectedMemberKey.substr(2)),
          false,
          this.date.dateString,
        )
      } else {
        takingDrugStore.fetchDrugsToTake(
          Number(accountStore.selectedMemberKey.substr(2)),
          selectedMemberKey,
          undefined,
          false,
          this.date.dateString,
        )
      }
    }
  }

  setTimeSlot = (timeSlot: string) => {
    this.setState({
      timeSlot: timeSlot,
    })
  }

  renderBottomSheet() {
    const { navigation } = this.props
    return (
      <View style={{ marginBottom: 0 }}>
        <View style={styles.BOTTOM_SHEET_HEADER}>
          <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>원하는 항목을 선택해 주세요.</Text>
        </View>
        <View style={{ backgroundColor: color.palette.white }}>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              const pid = this.selectedDrugToTakeGroup?.id.substring(
                0,
                this.selectedDrugToTakeGroup.id.length - 5,
              )
              navigation.navigate("PrescriptionDetail", {
                prescriptionId: pid,
                issueDate: this.selectedDrugToTakeGroup?.drugsToTake[0].prescription.issue_date,
              })
            }}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>처방 내역 보기</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              const pid = this.selectedDrugToTakeGroup?.id.substring(
                0,
                this.selectedDrugToTakeGroup.id.length - 5,
              )
              navigation.navigate("Details", {
                screen: "MedicationSettings",
                params: {
                  prescriptionId: pid,
                  issueDate: this.selectedDrugToTakeGroup?.drugsToTake[0].prescription.issue_date,
                },
              })
            }}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>처방 내역 수정</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_CANCEL_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => this.bottomSheetRef.current.close()}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>취소</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  render() {
    const { navigation, takingDrugStore } = this.props
    const pStore = this.props.prescriptionStore
    const { timeSlot } = this.state
    const { drugsOnce } = takingDrugStore

    const filteredDrugsToTake = this.filterDrugsToTake(
      values(takingDrugStore.drugToTakeGroups),
      timeSlot,
    )
    const listLength = filteredDrugsToTake.length

    if (pStore.status == "error") {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (pStore.status == "unauthorized") {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => navigation.navigate("Auth"), 2000)
    }

    return (
      <Screen style={ROOT} preset="fixed">
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <View style={HEADER_DATE}>
          <CloseButton onPress={() => navigation.goBack()} style={XBTN} />
          <View style={CONTENT}>
            <Text style={DATETIME}>{`${this.date.month}월 ${this.date.day}일`}</Text>
          </View>
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={() => this.fetchDrugsToTake()}
              tintColor={color.primary}
              titleColor={color.primary}
              colors={[color.primary]}
            />
          }
        >
          <View>
            <TimeSlotRow
              slots={takingDrugStore.visibleDefaultTimeSlots}
              onPress={this.setTimeSlot}
            />
            {takingDrugStore.isLoading ? (
              <Spinner color={color.primary} style={{ marginTop: 20, alignSelf: "center" }} />
            ) : (
              <View>
                <FlatList<DrugToTakeGroup>
                  style={TAKING_ITEM_LIST}
                  data={filteredDrugsToTake}
                  renderItem={({ item, index }: { item: DrugToTakeGroup; index: number }) => (
                    <View key={item.id}>
                      <TakingPrescription
                        key={item.id}
                        drugToTakeGroup={item}
                        index={index}
                        listLength={listLength}
                        onPress={() => this.onItemPress(item)}
                        onTakePress={this.onTakePress}
                        date={this.date.timestamp}
                      />
                    </View>
                  )}
                  ItemSeparatorComponent={() => <View style={{ height: 8 }} />}
                  keyExtractor={(item) => item.id.toString()}
                  // extraData={{ extra: this.props.prescriptionStore.prescriptions }}
                  ListEmptyComponent={<EmptyDrugsToTakeList timeSlot={timeSlot} />}
                />
                {drugsOnce.size > 0 ? this.renderDrugsOnce() : null}
              </View>
            )}
          </View>
          <View style={{ height: 80 }} />
        </ScrollView>
        <RBSheet
          ref={this.bottomSheetRef}
          height={styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * 3 + 14}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            container: {
              backgroundColor: "transparent",
            },
          }}
        >
          <StatusBar backgroundColor={color.dimmedStatusBar} />
          {this.renderBottomSheet()}
        </RBSheet>
      </Screen>
    )
  }

  renderDrugsOnce() {
    const { takingDrugStore } = this.props
    const { drugsOnceUnderlineTop, drugsOnceUnderlineWidth } = this.state
    const data = takingDrugStore.onceDataForList

    return (
      <View>
        <View
          style={[SECTION_HEADER, { marginTop: 18, marginBottom: 32, justifyContent: "center" }]}
        >
          <View style={{ ...COLOR_UNDERLINE_BOX, top: drugsOnceUnderlineTop }}>
            <View style={{ ...COLOR_UNDERLINE_PAINT, width: drugsOnceUnderlineWidth }} />
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ ...TAKE_DRUG_ONCE_TEXT, color: color.primary }}>하루에 한번 </Text>
            <Text style={TAKE_DRUG_ONCE_TEXT}>잊지마세요!</Text>
          </View>
        </View>
        <FlatList<DrugToTakeGroup>
          style={[TAKING_ITEM_LIST, { marginTop: 0 }]}
          data={values(takingDrugStore.drugsOnce)}
          renderItem={({ item, index }: { item: DrugToTakeGroup; index: number }) => (
            <TakingPrescription
              key={item.id}
              type="once"
              drugToTakeGroup={item}
              index={index}
              listLength={data.length}
              onPress={() => this.onItemPress(item)}
              onTakePress={this.onTakePress}
              date={this.date.timestamp}
            />
          )}
          ItemSeparatorComponent={() => <View style={{ height: 8 }} />}
          refreshControl={
            <RefreshControl
              refreshing={takingDrugStore.isLoading}
              onRefresh={() => this.fetchDrugsToTake()}
              tintColor={color.primary}
              titleColor={color.primary}
              colors={[color.primary]}
            />
          }
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
    )
  }

  getImageSource(img: any): any {
    if (img == null || img == "") {
      return {
        uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png`,
        priority: FastImage.priority.high,
      }
    } else {
      return {
        uri: img,
        priority: FastImage.priority.high,
      }
    }
  }

  toggleModal = () => {
    // DUR 설명 팝업만 작동
    if (this.state.isDurDescPopup) {
      this.setState({ isModalVisible: !this.state.isModalVisible })
    }
  }

  filterDrugsToTake(
    data: ReadonlyArray<DrugToTakeGroup> | undefined,
    timeSlotName?: string,
  ): ReadonlyArray<DrugToTakeGroup> {
    if (!data) return new Array<DrugToTakeGroup>()
    if (data.length === 0) return data

    if (timeSlotName) {
      return data.filter((value) => value.belongsTo(timeSlotName))
    } else {
      const nowSlot = getNowTimeSlotName()
      return data.filter((value) => value.belongsTo(nowSlot))
    }
  }

  // 질병명 클릭 시 바텀시트 오픈
  onItemPress(item: DrugToTakeGroup) {
    this.bottomSheetRef.current.open()
    this.selectedDrugToTakeGroup = item
  }

  onTakePress = async (
    item: DrugToTake,
    takeAllPressed?: boolean,
    allTaken?: boolean,
    groupType?: "once" | undefined,
  ) => {
    const { accountStore, takingDrugStore } = this.props
    const { drugsOnce, drugToTakeGroups } = takingDrugStore
    const statusToChange = takeAllPressed
      ? allTaken
        ? "taken"
        : "ready"
      : item.status === "ready"
      ? "taken"
      : "ready"

    const drugTakeGroup = takeAllPressed
      ? !groupType
        ? drugToTakeGroups.get(`${item.prescription.id}${item.when}`)
        : drugsOnce.get(String(item.prescription.id))
      : undefined

    const result = await api.takeDrug(item.id, statusToChange, drugTakeGroup)
    if (result.kind === "ok") {
      if (takeAllPressed) {
        takingDrugStore.setMedicationStatus(
          `${item.prescription.id}${item.when}`,
          statusToChange,
          groupType,
        )
        if (statusToChange === "taken") {
          Toast.show({
            title: <Text style={TAKE_DRUG_TOAST_TEXT}>{"✔︎ 복약 완료"}</Text>,
            duration: 2000,
            placement: "top",
            style: TAKE_DRUG_TOAST_VIEW,
          })
          // 오늘 먹을 약에만 포인트 지급
          if (
            remoteConfig().getBoolean("points_enabled") &&
            DateTime.now().hasSame(this.date, "day")
          ) {
            const drugTakeIds = drugTakeGroup.drugsToTake.map((item) => item.id)
            const result = validatePoints(POINT_TYPES.drugTaken, drugTakeIds)
            if (result.validPoints > 0) {
              this.props.navigation.navigate("Details", {
                screen: "GetPoint",
                params: {
                  pid: item.prescription.id,
                  description: getPrescriptionTitle(item.prescription),
                  pointType: POINT_TYPES.drugTaken,
                  drugTakeIds: result.validDrugTakeIds,
                  alarmName: item.alarm_name,
                  point: result.validPoints,
                },
              })
            }
          }
        } else {
          Toast.show({
            title: <Text style={TAKE_DRUG_TOAST_TEXT_CANCEL}>{"✘ 복약 취소"}</Text>,
            duration: 2000,
            placement: "top",
            style: TAKE_DRUG_TOAST_VIEW_CANCEL,
          })
        }
      } else {
        item.setStatus(statusToChange)
        if (
          remoteConfig().getBoolean("points_enabled") &&
          statusToChange === "taken" &&
          DateTime.now().hasSame(this.date, "day")
        ) {
          const result = validatePoints(POINT_TYPES.drugTaken, [item.id])
          if (result.validPoints > 0) {
            this.props.navigation.navigate("Details", {
              screen: "GetPoint",
              params: {
                pid: item.prescription.id,
                description: getPrescriptionTitle(item.prescription),
                pointType: POINT_TYPES.drugTaken,
                drugTakeIds: result.validDrugTakeIds,
                alarmName: item.alarm_name,
                point: result.validPoints,
              },
            })
          }
        }
      }
    } else {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>
            {"서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요."}
          </Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }
}
