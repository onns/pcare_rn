import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import React, { Component } from "react"
import { Platform } from "react-native"
import OrientationLocker from "react-native-orientation-locker"
import { WebView } from "react-native-webview"

import { sendLogEvent } from "@lib/analytics"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import CloseButton from "../components/CloseButton"
import {
  AuthStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { color } from "../theme"

type WebViewRouteProp = RouteProp<AuthStackParamList, "WebView">

type WebViewNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "WebView">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<HomeStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  navigation: WebViewNavigationProp
  route: WebViewRouteProp
}

export default class WebViewScreen extends Component<Props> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route
    return {
      title: params?.title ?? "",
      headerLeft: () => (
        <CloseButton
          style={{ marginLeft: 20 }}
          imageStyle={{ tintColor: color.palette.gray3 }}
          onPress={() => {
            navigation.pop()
            navigation.navigate("Main")
          }}
        />
      ),
    }
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
    // 세로, 가로모드 모두 가능하도록 설정
    OrientationLocker.unlockAllOrientations()
  }

  componentWillUnmount() {
    // unmount될 때 세로모드 고정
    OrientationLocker.lockToPortrait()
  }

  render() {
    const { params } = this.props.route
    const { navigation } = this.props
    let VideoTagOnlyAndroidUri
    if (params.content !== undefined && params.content.includes("_usage_html5_640x480.mp4")) {
      VideoTagOnlyAndroidUri = params.content.match(/(.src=".+(\.mp4))/g)[0].replace(`src="`, "")
    }

    return (
      <WebView
        ref="webview"
        onMessage={(event) => {
          const message = event.nativeEvent.data
          /* event.nativeEvent.data must be string, i.e. window.postMessage
            should send only string.
          */
          const obj = JSON.parse(message)
          if (obj.screen && obj.id) {
            sendLogEvent(`press_${obj.screen}_from_webview`, { id: Number(obj.id) })
            navigation.navigate(
              obj.screen,
              obj.code ? { id: Number(obj.id), code: obj.code } : { id: Number(obj.id) },
            )
          }
          // this.props.accountStore.user.setAddress(message)
          // this.props.navigation.goBack()
        }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        androidLayerType={"hardware"}
        allowsFullscreenVideo={true}
        source={
          Platform.OS === "android"
            ? params.url
              ? { uri: params.url }
              : VideoTagOnlyAndroidUri
              ? { uri: VideoTagOnlyAndroidUri }
              : { html: params.content }
            : params.url
            ? { uri: params.url }
            : { html: params.content }
        }
        // source={params.url ? { uri: params.url } : { html: params.content }}
        injectedJavaScript={params.javaScript ? params.javaScript : undefined}
      />
    )
  }
}
