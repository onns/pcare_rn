import { observer } from "mobx-react-lite"
import { FormControl, Input, Stack, Toast } from "native-base"
import React, { useState } from "react"
import { Alert, SafeAreaView, TextStyle, View, ViewStyle } from "react-native"

import { api } from "@api/api"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { FamilyMember, FamilyMemberModel } from "../stores/FamilyMember"
import { color, styles, typography } from "../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingTop: 16,
  paddingBottom: 12,
  paddingHorizontal: 16,
}

const FORM_ITEM: ViewStyle = {
  marginTop: 32,
  marginLeft: 16,
  marginRight: 16,
}

const LABEL: TextStyle = {
  width: 76,
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  paddingRight: 0,
}

const INPUT = { fontFamily: typography.bold, fontSize: 16, color: color.text }

const TITLE = {
  fontFamily: typography.bold,
  fontSize: 20,
  lineHeight: 30,
  color: color.text,
  marginLeft: 16,
}
const BUTTON_TEXT = { fontFamily: typography.bold, fontSize: 17, color: color.palette.white }

const BOTTOM_BUTTONS: ViewStyle = { flex: 1, justifyContent: "flex-end", borderRadius: 28 }

type EditMemberNameRouteProp = RouteProp<DetailsStackParamList, "EditMemberName">

type EditMemberNameNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "EditMemberName">,
  CompositeNavigationProp<
    StackNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface EditMemberNameScreenProps {
  navigation: EditMemberNameNavigationProp
  route: EditMemberNameRouteProp
}

export interface Props {
  accountStore: AccountStore
  navigation: EditMemberNameNavigationProp
  route: EditMemberNameRouteProp
}

interface State {
  isDateTimePickerVisible: boolean
  isOver14: boolean
  isMale: boolean
  imageType: any
  imageUri: any
  fileName: any
  memberIndex: number
}

export const EditMemberNameScreen: React.FunctionComponent<EditMemberNameScreenProps> = observer(
  (props) => {
    const { accountStore } = useStores()
    const { navigation } = props
    const { params } = props.route
    const [isChangeNameVisible, setIsChangeNameVisible] = useState(false)

    const getMemberInfo = (params: any): any => {
      const member = accountStore.family[params.memberIndex]
      return member
    }
    const member = getMemberInfo(props.route.params)
    const [name, setName] = useState(member.name)

    const submit = async () => {
      if (!name || name.trim().length == 0) {
        Alert.alert("", `이름을 입력하세요.`, [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
        return
      } else if (accountStore.hasName(name.trim(), member.id)) {
        Alert.alert(
          "",
          `해당 가족회원 이름은 현재 사용 중 입니다. \n다른 이름으로 등록해 주세요.`,
          [{ text: "확인", onPress: () => null }],
          { cancelable: false },
        )
        return
      } else {
        member.setName(name)
        setIsChangeNameVisible(true)
      }
      const store = accountStore
      const reqParams = new FormData()

      reqParams.append("name", member.name)

      const result = await api.uploadChildProfileImage(reqParams, member.id)

      if (result.kind === "ok") {
        store.changeFamilyMember(params.memberIndex, member)
      } else if (result.kind === "unauthorized") {
        Toast.show({
          title: (
            <Text style={styles.TOAST_TEXT}>
              {`세션이 만료되었습니다. \n다시 로그인 해주세요.`}
            </Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => props.navigation.navigate("Auth"), 2000)
      } else {
        Alert.alert(
          "",
          `서버에 저장하지 못 하였습니다. \n나중에 다시 시도해주세요.`,
          [{ text: "확인", onPress: () => null }],
          { cancelable: false },
        )
        console.tron.log("< EditMemberNameScreen > submit, update error")
      }
    }

    interface State {
      isChangeNameVisible: boolean
    }

    const goBack = () => {
      navigation.pop()
    }

    return (
      <>
        <Screen style={ROOT} preset="fixed" keyboardOffset="bottomButton">
          <Text style={TITLE}>이름 수정</Text>
          <Stack inlineLabel last style={FORM_ITEM}>
            <FormControl.Label style={LABEL}>이름</FormControl.Label>
            <Input
              autoCapitalize="none"
              selectionColor={color.text}
              onChangeText={(text) => setName(text)}
              value={name}
              style={INPUT}
            />
          </Stack>
          {member.name != name ? (
            <SafeAreaView style={BOTTOM_BUTTONS}>
              <Button block onPress={submit}>
                <Text style={BUTTON_TEXT}>확인</Text>
              </Button>
            </SafeAreaView>
          ) : null}
        </Screen>
        <Popup
          isVisible={isChangeNameVisible}
          title={"이름이 수정되었습니다"}
          button1={getPopupButton(() => goBack(), "확인")}
        />
      </>
    )
  },
)
