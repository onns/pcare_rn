import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"

import amplitude from "amplitude-js"
import * as NavigationBar from "expo-navigation-bar"
import { inject, observer } from "mobx-react"
import { Spinner, Toast, View } from "native-base"
import React, { Component } from "react"
import {
  Alert,
  Animated,
  BackHandler,
  Dimensions,
  Image,
  ImageSourcePropType,
  Linking,
  Platform,
  ScrollView,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import AndroidOpenSettings from "react-native-android-open-settings"
import appsFlyer from "react-native-appsflyer"
import {
  PanGestureHandler,
  PinchGestureHandler,
  State as GestureState,
} from "react-native-gesture-handler"
import Modal from "react-native-modal"
import { requestNotifications } from "react-native-permissions"

import { api } from "@api/api"
import { supabase } from "@api/supabase"
import { POINT_TYPES } from "@features/points/constants"
import { validatePoints } from "@features/points/validator"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import remoteConfig from "@react-native-firebase/remote-config"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import CloseButton from "../components/CloseButton"
import { getPopupButton, Popup } from "../components/Popup"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { FamilyMember } from "../stores/FamilyMember"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles } from "../theme"

export enum FamilyMemberType {
  Self,
  FamilyUser,
  Child,
}

const HEADER = {
  backgroundColor: "rgb(52, 52, 52)",
  borderBottomWidth: 0,
  borderBottomColor: "rgb(52, 52, 52)",
  elevation: 0,
  shadowColor: "transparent",
}
const RECAPTURE_BUTTON_TEXT: TextStyle = {
  color: color.primary,
  fontWeight: "bold",
  alignSelf: "center",
}
const SUBMIT_BUTTON_TEXT: TextStyle = {
  color: "#fff",
  fontWeight: "bold",
  alignSelf: "center",
}
const BUTTONS: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-around",
  marginVertical: 30,
}
const MODAL_CONTENT: ViewStyle = {
  backgroundColor: "white",
  margin: 42.5,
  padding: 16.9,
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 20,
  borderColor: "rgba(0, 0, 0, 0.1)",
}
const ROW: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  height: 20,
}
const WHOSE_PRESCRIPTION_TEXT: TextStyle = {
  fontSize: 15,
  letterSpacing: -0.6,
  marginBottom: 50,
}
const MEMBER_BUTTON: ViewStyle = {
  height: 50,
  alignItems: "center",
}
const NAME: TextStyle = {
  fontSize: 20,
  letterSpacing: -0.8,
}

type PrescriptionPreviewRouteProp = RouteProp<DetailsStackParamList, "PrescriptionPreview">

type PrescriptionPreviewNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "PrescriptionPreview">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: PrescriptionPreviewNavigationProp
  route: PrescriptionPreviewRouteProp
}

interface State {
  uri: string
  type: string
  name: string
  isModalVisible: boolean
  ocrMessage: any
  status: string
  popupVisible: boolean
  popupImageSource: ImageSourcePropType | undefined
  popupTitle: string
  popupMessage: string
  popupButton1: React.ReactElement | undefined
  popupButton2: React.ReactElement | undefined
  popupButton3: React.ReactElement | undefined
  popupButtonGroupStyle: ViewStyle | undefined
}

@inject("accountStore", "prescriptionStore")
@observer
export default class PrescriptionPreviewScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "",
      headerStyle: HEADER,
      headerLeft: null,
      headerRight: ({ tintColor }) => (
        <CloseButton
          imageStyle={{ tintColor: tintColor }}
          onPress={() => {
            navigation.popToTop({ immediate: true })
            navigation.navigate("HomeStack")
          }}
        />
      ),
    }
  }

  _baseScale = new Animated.Value(1)
  _pinchScale = new Animated.Value(1)
  _scale = Animated.multiply(this._baseScale, this._pinchScale)
  _lastScale = 1
  _onPinchGestureEvent = Animated.event([{ nativeEvent: { scale: this._pinchScale } }], {
    useNativeDriver: true,
  })

  isFirstPoints = false
  pid = null

  // rome-ignore lint/suspicious/noExplicitAny: <explanation>
  _onPinchHandlerStateChange = (event: { nativeEvent: { oldState: any; scale: number } }) => {
    if (event.nativeEvent.oldState === GestureState.ACTIVE) {
      this._lastScale *= event.nativeEvent.scale
      this._baseScale.setValue(this._lastScale)
      this._pinchScale.setValue(1)
    }
  }

  _translateX = new Animated.Value(0)
  _translateY = new Animated.Value(0)
  _lastOffset = { x: 0, y: 0 }
  _onGestureEvent = Animated.event(
    [
      {
        nativeEvent: {
          translationX: this._translateX,
          translationY: this._translateY,
        },
      },
    ],
    { useNativeDriver: true },
  )

  _unsubscribeFocus: () => void
  _unsubscribeBlur: () => void

  _onHandlerStateChange = (event: {
    // rome-ignore lint/suspicious/noExplicitAny: <explanation>
    nativeEvent: { oldState: any; translationX: number; translationY: number }
  }) => {
    if (event.nativeEvent.oldState === GestureState.ACTIVE) {
      this._lastOffset.x += event.nativeEvent.translationX
      this._lastOffset.y += event.nativeEvent.translationY
      this._translateX.setOffset(this._lastOffset.x)
      this._translateX.setValue(0)
      this._translateY.setOffset(this._lastOffset.y)
      this._translateY.setValue(0)
    }
  }

  constructor(props: Props) {
    super(props)

    const params = this.props.route.params!

    const now = new Date()

    this.state = {
      uri: params.uri,
      type: params.type ? params.type : "image/jpeg",
      name: params.type
        ? `${this.props.accountStore.user.id}_${now.toISOString()}.png`
        : `${this.props.accountStore.user.id}_${now.toISOString()}.jpg`,
      isModalVisible: false,
      ocrMessage: params.ocrMessage ? params.ocrMessage : undefined,
      status: "idle",
      popupVisible: false,
      popupImageSource: undefined,
      popupTitle: "",
      popupMessage: "",
      popupButton1: undefined,
      popupButton2: undefined,
      popupButton3: undefined,
      popupButtonGroupStyle: undefined,
    }
  }

  getRePhotoIndex(params) {
    return params.rePhotoIndex
  }

  getRePhotoId(params) {
    return params.rePhotoId
  }

  async getIsFirstPoints() {
    const { data: pointActivities, error } = await supabase
      .from("point_activities")
      .select("*")
      .eq("user_id", this.props.accountStore.user.id)
    console.tron.log(pointActivities)

    if (error) return false
    return pointActivities && pointActivities.length === 0
  }

  async componentDidMount() {
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)
    this._unsubscribeBlur = navigation.addListener("blur", this.didBlur)
    this.isFirstPoints = await this.getIsFirstPoints()
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
    this._unsubscribeBlur()
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
  }

  didFocus = () => {
    NavigationBar.setBackgroundColorAsync("#343434")
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress)
  }

  didBlur = () => {
    NavigationBar.setBackgroundColorAsync("#efefef")
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
  }

  handleBackPress = () => {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.props.navigation.replace("PrescriptionScanner")
    return true
  }

  _toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible })

  handleNavigate = () => {
    this.props.navigation.popToTop()
    if (remoteConfig().getBoolean("points_enabled")) {
      const pointType = this.isFirstPoints
        ? POINT_TYPES.firstPrescriptionRegistered
        : POINT_TYPES.prescriptionRegistered
      const result = validatePoints(pointType)
      if (result.validPoints > 0) {
        this.props.navigation.navigate("Details", {
          screen: "GetPoint",
          params: {
            pointType: pointType,
            description: "처방 등록",
            pid: this.pid,
            point: result.validPoints,
          },
        })
      } else {
        this.props.navigation.navigate("HomeStack")
      }
    } else {
      this.props.navigation.navigate("HomeStack")
    }
  }

  render() {
    const deviceWidth = Dimensions.get("window").width
    const deviceHeight =
      Platform.OS === "ios"
        ? Dimensions.get("window").height
        : require("react-native-extra-dimensions-android").get("REAL_WINDOW_HEIGHT")

    const imageWidth = Dimensions.get("window").width - 30
    const imageHeight = imageWidth * 1.414

    if (this.state.status === "pending") {
      return (
        // eslint-disable-next-line react-native/no-color-literals
        <View style={{ flex: 1, backgroundColor: "rgb(52, 52, 52)" }}>
          <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
          {this.renderPopup()}
        </View>
      )
    }

    return (
      // eslint-disable-next-line react-native/no-color-literals
      <View style={{ flex: 1, backgroundColor: "rgb(52, 52, 52)" }}>
        <StatusBar backgroundColor="rgb(52, 52, 52)" barStyle="light-content" />
        <ScrollView contentContainerStyle={{ flex: 1, alignItems: "center" }}>
          <PanGestureHandler
            onGestureEvent={this._onGestureEvent}
            onHandlerStateChange={this._onHandlerStateChange}
          >
            <Animated.View style={{ flex: 1 }}>
              <PinchGestureHandler
                onGestureEvent={this._onPinchGestureEvent}
                onHandlerStateChange={this._onPinchHandlerStateChange}
              >
                <Animated.Image
                  source={{ uri: this.state.uri }}
                  style={{
                    width: imageWidth,
                    height: imageHeight,
                    resizeMode: "contain",
                    transform: [
                      { perspective: 200 },
                      { scale: this._scale },
                      { translateX: this._translateX },
                      { translateY: this._translateY },
                    ],
                  }}
                />
              </PinchGestureHandler>
            </Animated.View>
          </PanGestureHandler>

          <View style={BUTTONS}>
            <Button
              light
              rounded
              onPress={this.photo}
              style={{
                width: 163,
                marginRight: 10.5,
                justifyContent: "center",
                alignSelf: "center",
              }}
            >
              <Text style={RECAPTURE_BUTTON_TEXT}>재촬영하기</Text>
            </Button>
            <Button
              rounded
              onPress={this._toggleModal}
              style={{ width: 163, justifyContent: "center", alignSelf: "center" }}
            >
              <Text style={SUBMIT_BUTTON_TEXT}>사용하기</Text>
            </Button>
          </View>

          <Modal
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            isVisible={this.state.isModalVisible}
            animationOut={"fadeOutDown"}
            // animationOutTiming={300}
            // onBackdropPress={this._toggleModal}
          >
            {this.renderSelectMemberModalContent()}
          </Modal>
          {this.state.status === "pending" ? (
            <Spinner
              color={color.primary}
              style={{
                position: "absolute",
                // top: 0,
                // left: 0,
                width: "100%",
                height: "100%",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            />
          ) : null}
        </ScrollView>
        {this.renderPopup()}
      </View>
    )
  }

  renderPopup = () => (
    <Popup
      isVisible={this.state.popupVisible}
      imageSource={this.state.popupImageSource}
      title={this.state.popupTitle}
      message={this.state.popupMessage}
      button1={this.state.popupButton1}
      button2={this.state.popupButton2}
      button3={this.state.popupButton3}
      buttonGroupStyle={this.state.popupButtonGroupStyle}
    />
  )

  renderSelectMemberModalContent = () => (
    <View style={MODAL_CONTENT}>
      <View style={ROW}>
        <View style={{ flex: 1 }} />
        <TouchableOpacity
          onPress={this.cancel}
          style={{ width: 24, height: 24, justifyContent: "flex-start", alignItems: "flex-end" }}
          hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}
        >
          <Image
            source={require("../assets/images/close.png")}
            style={{
              width: 15.7,
              height: 15.7,
              // resizeMode: 'contain',
            }}
          />
        </TouchableOpacity>
      </View>
      <View style={{ padding: 5, marginTop: 42 }}>
        <Text style={WHOSE_PRESCRIPTION_TEXT}>누구의 처방전인가요?</Text>

        <ScrollView style={{ maxHeight: 250 }}>
          <TouchableOpacity
            onPress={() => {
              this.requestToEnterPrescData(this.props.accountStore.user.id, FamilyMemberType.Self)
            }}
            // {...rest}
            style={MEMBER_BUTTON}
          >
            <Text style={NAME}>{this.props.accountStore.user.nickname}</Text>
          </TouchableOpacity>
          {this.props.accountStore.family.map((member: FamilyMember) => {
            return (
              <TouchableOpacity
                key={member.id}
                onPress={() =>
                  this.requestToEnterPrescData(
                    member.isOver14 ? member.family_user! : member.id,
                    member.isOver14 ? FamilyMemberType.FamilyUser : FamilyMemberType.Child,
                  )
                }
                // {...rest}
                style={MEMBER_BUTTON}
              >
                <Text style={NAME}>{member.name}</Text>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
      <View style={{ height: 40 }} />
    </View>
  )

  cancel = () => {
    if (Platform.OS === "ios") {
      this.setState({
        isModalVisible: false,
      })
    } else {
      this.setState({
        popupVisible: true,
        // popupImageSource: require("../assets/images/popupUploadCompleted.png"),
        popupTitle: "처방전 입력 취소",
        popupMessage: "처방전 입력을 취소하시겠습니까?",
        popupButton1: getPopupButton(
          () => {
            this.setState({ popupVisible: false })
          },
          "아니오",
          "cancel",
        ),
        popupButton2: getPopupButton(() => {
          this.setState({ popupVisible: false })
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.props.navigation.replace("PrescriptionScanner")
        }, "네"),
        popupButton3: undefined,
        popupButtonGroupStyle: undefined,
      })
    }
  }

  photo = () => {
    amplitude.getInstance().logEvent("camera_capture_again")
    this.props.navigation.replace("PrescriptionScanner")
  }

  async requestToEnterPrescData(id: number, memberType: FamilyMemberType) {
    this.setState({ isModalVisible: !this.state.isModalVisible, status: "pending" })

    // upload image

    const params = new FormData()
    params.append("image", {
      uri: this.state.uri,
      type: this.state.type,
      name: this.state.name,
    })
    if (memberType === FamilyMemberType.FamilyUser) {
      params.append("user", id)
    } else if (memberType === FamilyMemberType.Child) {
      params.append("children", id)
    }
    const naviParams = this.props.route.params!
    if (naviParams.ocrMessage) {
      // user_id, ver_no 추가하기
      const ocrRes = {
        user_id: this.props.accountStore.user.id,
        ...naviParams.ocrMessage,
      }
      params.append("ocr_res", JSON.stringify(ocrRes))
    }

    const result = await api.uploadPrescriptionImage(params)
    if (result.kind === "ok") {
      firebase.analytics().logEvent("prescription_uploaded")
      amplitude.getInstance().logEvent("prescription_uploaded")
      appsFlyer.logEvent("af_prescription_uploaded", {})

      this.pid = result.data.id

      this.checkRephotograph()
      this.showPopup()
      this.fetchMemberPrescriptions(id, memberType)
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{"세션이 만료되었습니다. \n다시 로그인 해주세요."}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.navigate("Auth")
      }, 2000)
    } else {
      this.setState({ status: "idle" })
      console.tron.log(
        "< PrescriptionPreviewScreen > requestToEnterPrescData, uploadPrescriptionImage error",
      )
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>
            {"처방전 입력 요청을 하지 못 하였습니다. \n나중에 다시 시도해 주세요."}
          </Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  async showPopup() {
    const pushNotiEnabled = await AsyncStorage.getItem("pushNotiEnabled")
    if (!pushNotiEnabled || (pushNotiEnabled && !JSON.parse(pushNotiEnabled))) {
      this.setState({
        popupVisible: true,
        popupImageSource: require("../assets/images/popupPrescNoti.png"),
        popupTitle: "처방전 분석 결과 알림 받기",
        popupMessage:
          "처방전 분석이 완료되면 알림을 보내드립니다. 알림 받기를 수락하시면 분석 결과외에도 의약품 안전 점검 알림, 복약일 완료 알림을 보내드립니다.",
        popupButton1: getPopupButton(
          () => {
            amplitude.getInstance().logEvent("presc_noti_popup_cancel")
            this.setState({ popupVisible: false })
            this.handleNavigate()
          },
          "알림 받지 않기",
          "cancel",
        ),
        popupButton2: getPopupButton(() => {
          amplitude.getInstance().logEvent("presc_noti_popup_ok")
          this.setState({ popupVisible: false })
          this.enablePushNotifications()
          this.handleNavigate()
        }, "알림 받기"),
        popupButton3: undefined,
        popupButtonGroupStyle: undefined,
      })
    } else {
      this.setState({
        popupVisible: true,
        popupImageSource: require("../assets/images/popupUploadCompleted.png"),
        popupTitle: "처방전 등록 성공 !",
        popupMessage:
          "처방전 내용을 정확하게 입력하기 위해 다소 시간이 걸릴 수 있습니다. 조금만 기다려 주세요.",
        popupButton1: getPopupButton(() => {
          this.setState({ popupVisible: false })
          this.onCompleteToastClose()
        }, "확인"),
        popupButton2: undefined,
        popupButton3: undefined,
        popupButtonGroupStyle: undefined,
      })
    }
  }

  fetchMemberPrescriptions(id: number, memberType: FamilyMemberType) {
    let familyMemberKey
    if (memberType === FamilyMemberType.Self) {
      familyMemberKey = `_u${id}`
    } else if (memberType === FamilyMemberType.Child) {
      familyMemberKey = `_c${id}`
    } else if (memberType === FamilyMemberType.FamilyUser) {
      familyMemberKey = `_f${id}`
    }
    this.props.prescriptionStore.fetchPrescriptions(
      memberType === FamilyMemberType.FamilyUser ? id : this.props.accountStore.user.id,
      familyMemberKey,
      memberType === FamilyMemberType.Child ? id : undefined,
    )
  }

  async checkRephotograph() {
    const rePhotoIndex = this.getRePhotoIndex(this.props.route.params)
    const rePhotoId = this.getRePhotoId(this.props.route.params)
    if (rePhotoIndex !== undefined && rePhotoId !== undefined) {
      api.deletePrescription(rePhotoId)
    }
  }

  async onCompleteToastClose() {
    const pushNotiEnabled = await AsyncStorage.getItem("pushNotiEnabled")
    if (!pushNotiEnabled || (pushNotiEnabled && !JSON.parse(pushNotiEnabled))) {
      this.setState({
        popupVisible: true,
        popupImageSource: require("../assets/images/popupPrescNoti.png"),
        popupTitle: "처방전 분석 결과 알림 받기",
        popupMessage:
          "처방전 분석이 완료되면 알림을 보내드립니다. 알림 받기를 수락하시면 분석 결과외에도 의약품 안전 점검 알림, 복약일 완료 알림을 보내드립니다.",
        popupButton1: getPopupButton(
          () => {
            this.setState({ popupVisible: false })
            this.handleNavigate()
          },
          "알림 받지 않기",
          "cancel",
        ),
        popupButton2: getPopupButton(() => {
          this.setState({ popupVisible: false })
          this.enablePushNotifications()
          this.handleNavigate()
        }, "알림 받기"),
        popupButton3: undefined,
        popupButtonGroupStyle: undefined,
      })
    } else {
      this.handleNavigate()
    }
  }

  async enablePushNotifications() {
    const user = this.props.accountStore.user

    const authStatus = await firebase.messaging().hasPermission()

    if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
      console.tron.log("Push Notifications AUTHORIZED")
    } else {
      this.props.accountStore.setTemporaryPinCodeDisabled(true)
      try {
        if (Platform.OS === "ios") {
          const authStatus = await firebase.messaging().requestPermission({
            announcement: true,
          })
          if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
            console.tron.log("User has notification permissions enabled.")
          } else if (authStatus === firebase.messaging.AuthorizationStatus.PROVISIONAL) {
            console.tron.log("User has provisional notification permissions.")
          } else {
            console.tron.log("User has notification permissions disabled")
          }
        } else {
          const { status, settings } = await requestNotifications(["alert", "sound"])
          console.tron.log(status)
          console.tron.log(settings)
        }
      } catch (error) {
        console.tron.log("< SettingsScreen > onDurNotiValueChange, rejected permissions", error)
        if (Platform.OS === "ios") {
          Alert.alert(
            "",
            `알림 설정을 변경하려면, 다음 페이지에서 "알림"을 누르고 "알림 허용"을 버튼을 다시 한번 눌러 주세요.`,
            [
              {
                text: "확인",
                onPress: () => {
                  Linking.openURL("app-settings://notification/com.onns.papricacare")
                  // AndroidOpenSettings.appNotificationSettings()
                },
              },
            ],
            { cancelable: false },
          )
        } else {
          Alert.alert(
            "",
            `알림 설정을 변경하려면, 다음 페이지에서 "알림" 버튼을 눌러 주세요.`,
            [
              {
                text: "확인",
                onPress: () => {
                  // Linking.openURL('app-settings://notification/com.onns.papricacare')
                  AndroidOpenSettings.appNotificationSettings()
                },
              },
            ],
            { cancelable: false },
          )
        }
      }
      this.props.accountStore.setTemporaryPinCodeDisabled(false)
    }

    const fcmToken = await firebase.messaging().getToken()

    const params = new FormData()
    params.append("user", user.id)
    params.append("firebase_token0", fcmToken)
    params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
    const result = await api.updateProfile(user.id, params)
    if (result.kind === "ok") {
      user.setFirebaseToken0(fcmToken)
      await AsyncStorage.setItem("pushNotiEnabled", JSON.stringify(true))
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{"세션이 만료되었습니다. \n다시 로그인 해주세요."}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.navigate("Auth")
      }, 2000)
    }
  }
}
