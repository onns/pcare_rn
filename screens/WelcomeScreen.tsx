import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { TextStyle, View, ViewStyle } from "react-native"

import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgLogo from "../assets/images/logo.svg"
import SvgWelcome from "../assets/images/welcome.svg"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import {
    AuthStackParamList, HomeStackParamList, MainTabParamList, RootStackParamList
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"

// static styles
const MESSAGE1: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 20,
  lineHeight: 29,
  letterSpacing: -0.4,
  color: color.text,
  marginTop: 24,
  marginBottom: 24,
}
const MESSAGE2: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 17,
  lineHeight: 25,
  letterSpacing: -0.34,
  color: color.text,
}
const LOGO_CONTAINER: ViewStyle = {
  flex: 5,
  flexDirection: "column",
  alignItems: "center",
  marginTop: 72,
}
const BOTTOM_BUTTONS: ViewStyle = {
  flex: 0.5,
  flexDirection: "row",
  justifyContent: "center",
  paddingBottom: 20,
}
const TITLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 32,
  lineHeight: 47,
  letterSpacing: -0.64,
  color: color.text,
  marginBottom: 32,
}
const POPUP_MESSAGE: TextStyle = {
  fontSize: 16,
  lineHeight: 30,
  letterSpacing: -0.64,
  paddingHorizontal: 5,
  textAlign: "center",
}
const POPUP_MESSAGE2: TextStyle = {
  fontSize: 13,
  lineHeight: 19.5,
  letterSpacing: -0.52,
  color: "rgb(132, 132, 132)",
}

type WelcomeRouteProp = RouteProp<AuthStackParamList, "Welcome">

type WelcomeNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "Welcome">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<HomeStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  prescriptionStore: PrescriptionStore
  accountStore: AccountStore
  navigation: WelcomeNavigationProp
  route: WelcomeRouteProp
}

@inject("accountStore", "prescriptionStore")
@observer
export default class WelcomeScreen extends Component<Props> {
  static navigationOptions = {
    headerShown: false,
  }
  _unsubscribeFocus: () => void
  _unsubscribeBlur: () => void

  state = {
    isModalVisible: false,
  }

  UNSAFE_componentWillMount() {
    const accountStore = this.props.accountStore
    this.props.prescriptionStore.fetchPrescriptions(
      accountStore.user.id,
      "_u" + accountStore.user.id,
    )
  }

  componentDidMount() {
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)
    this._unsubscribeBlur = navigation.addListener("blur", this.didBlur)
  }

  didFocus = () => {
    // changeNavigationBarColor("#ff841a", true, true)
    // hideNavigationBar()
    setTimeout(() => {
      this.setState({
        isModalVisible: true,
      })
    }, 2000)
  }

  didBlur = () => {
    // changeNavigationBarColor("#efefef", true, true)
    // showNavigationBar()
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
    this._unsubscribeBlur()
  }

  _toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible })

  onCancelPress = () => {
    this.setState({ isModalVisible: false })
    this.props.navigation.navigate("Main", { screen: "HomeStack" })
  }

  onConfirmPress = () => {
    this.setState({ isModalVisible: false })
    this.props.navigation.navigate("CodeVerification", {
      purpose: "initial",
      preRoute: "Welcome",
    })
  }

  render() {
    return (
      <Screen preset="fixed">
        {/* <LinearGradient colors={["#e8382f", "#ff841a"]} style={LINEAR_GRADIENT}> */}
        <View style={LOGO_CONTAINER}>
          <Text style={TITLE}>환영합니다!</Text>
          <SvgWelcome width={261} height={185} />
          <Text style={MESSAGE1}>회원가입을 축하합니다.</Text>
          <Text style={[MESSAGE2, { fontFamily: typography.bold, color: color.primary }]}>
            파프리카케어<Text>는</Text>
          </Text>
          <Text style={MESSAGE2}>당신의 건강을 응원합니다!</Text>
        </View>

        <View style={BOTTOM_BUTTONS}>
          <SvgLogo width={149} height={28} />
        </View>
        {/* </LinearGradient> */}
        <Popup
          isVisible={this.state.isModalVisible}
          title={"잠금 비밀번호 설정"}
          message={
            <Text style={POPUP_MESSAGE}>
              잠금 비밀번호를 사용하시겠습니까?{"\n"}
              <Text style={POPUP_MESSAGE2}>
                잠금 비밀번호는 “마이페이지 > 앱설정” 에서 언제든지 변경 가능합니다.
              </Text>
            </Text>
          }
          button1={getPopupButton(this.onCancelPress, "나중에", "cancel")}
          button2={getPopupButton(this.onConfirmPress, "네")}
        />
      </Screen>
    )
  }
}
