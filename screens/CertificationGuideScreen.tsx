import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, ScrollView } from "native-base"
import React from "react"
import { TextStyle, View, ViewStyle } from "react-native"

import firebase from "@react-native-firebase/app"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { AuthStackParamList, RootStackParamList } from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  justifyContent: "space-between",
  paddingHorizontal: 16,
}
const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 16,
  paddingBottom: 48,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 32,
  color: color.text,
}
const MESSAGE: TextStyle = {
  paddingHorizontal: 4,
  fontFamily: typography.bold,
  fontSize: 20,
  lineHeight: 30,
  color: color.text,
  textAlign: "center",
}
const SUB_MESSAGE: TextStyle = {
  fontSize: 14,
  lineHeight: 23,
  color: color.text,
  alignSelf: "center",
}
const DIVIDER: ViewStyle = {
  width: 16,
  height: 1,
  backgroundColor: "#000000",
  alignSelf: "center",
  marginVertical: 24,
}
const BOTTOM_MESSAGE: TextStyle = {
  padding: 16,
  fontFamily: typography.primary,
  fontSize: 13,
  color: color.palette.grey50,
  backgroundColor: color.palette.lightGrayishOrange,
  alignSelf: "center",
  marginTop: 24,
  marginBottom: 30,
  borderRadius: 1,
}
const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  color: color.palette.white,
}
const CANCEL_BUTTON: ViewStyle = {
  backgroundColor: color.palette.veryLightPink3,
  marginBottom: 8,
}

type CertificationGuideScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "CertificationGuide">,
  StackNavigationProp<RootStackParamList>
>
type CertificationGuideScreenRouteProp = RouteProp<AuthStackParamList, "CertificationGuide">

type Props = {
  navigation: CertificationGuideScreenNavigationProp
  route: CertificationGuideScreenRouteProp
  accountStore: AccountStore
}

interface State {
  reqAccount: any
}

@inject("accountStore")
@observer
export default class CertificationGuideScreen extends React.Component<Props, State> {
  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  render() {
    const { accountStore, navigation } = this.props
    return (
      <View style={BACKGROUND}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View>
            <View style={TITLE}>
              <Text style={TITLE_TEXT}>본인 인증</Text>
            </View>
            <Text style={MESSAGE}>아래 서비스 이용을 위해{"\n"}본인 인증을 해주세요.</Text>
            <View style={DIVIDER} />
            <View style={{ alignSelf: "center" }}>
              <Text style={SUB_MESSAGE}>• 처방내용 분석</Text>
              <Text style={SUB_MESSAGE}>• 사용자 맞춤 의료 정보 (질병, 약제)</Text>
              <Text style={SUB_MESSAGE}>• 14세 이상 가족회원 추가</Text>
            </View>
            <Text style={BOTTOM_MESSAGE}>
              * 타인의 처방전 도용에 대한 사후 법적 책임은 파프리카케어가 아닌 도용자에게 있음을
              알려드립니다.{"\n\n"}* 파프리카케어는 만 14세 미만의 아동은 회원가입이 불가합니다. 만
              14세 미만의 회원은 본인인증이 가능한 만 14세 이상의 계정에서 가족회원으로 관리하세요.
            </Text>
            <Button
              block
              style={CANCEL_BUTTON}
              onPress={() => {
                firebase.analytics().logEvent("certification_skip_click")
                amplitude.getInstance().logEvent("certification_skip_click")

                const user = accountStore.user
                if (user.gender === "female") {
                  navigation.replace("CheckPregnancy")
                } else {
                  navigation.navigate("Welcome")
                }
              }}
            >
              <Text style={[BUTTON_TEXT, { color: color.palette.grey50 }]}>나중에</Text>
            </Button>
            <Button
              block
              style={{ marginBottom: 16 }}
              onPress={() => {
                navigation.navigate("Certification", {
                  previousScreen: "CertificationGuide",
                })
              }}
            >
              <Text style={BUTTON_TEXT}>인증하기</Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    )
  }
}
