import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container } from "native-base"
import React from "react"
import { ScrollView, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgArrowRight from "../assets/images/arrowRight.svg"
import { Text } from "../components/StyledText"
import { formatDateWithJoinChar } from "../lib/DateUtil"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  backgroundColor: "#fff",
  marginTop: 9,
  paddingLeft: 20,
  paddingRight: 20,
}
const ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const SETTING_ROW: ViewStyle = {
  ...ROW,
  paddingTop: 8,
  paddingBottom: 8,
}
const AGREED_DATE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 13,
  lineHeight: 16,
  letterSpacing: -0.52,
  color: "rgb(153, 153, 153)",
  textAlign: "right",
  marginLeft: 1,
}
const SECTION_TITLE = {
  fontFamily: typography.primary,
  fontSize: 15,
  color: color.palette.grey1,
  marginTop: 15,
}
const PROPERTY_NAME = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 19,
  letterSpacing: -0.6,
}
const FLEX_END: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "flex-end",
  alignItems: "center",
}
const SECTION_HEADER = {
  borderBottomWidth: 1,
  borderBottomColor: color.palette.lightGrey2,
  paddingBottom: 8,
  marginBottom: 5,
}

type PrivacyPolicyAndTermsRouteProp = RouteProp<MyPageStackParamList, "PrivacyPolicyAndTerms">

type PrivacyPolicyAndTermsNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "PrivacyPolicyAndTerms">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: PrivacyPolicyAndTermsNavigationProp
  route: PrivacyPolicyAndTermsRouteProp
}

interface State {
  pinCode: undefined | string
  pinCodeEnabled: boolean
  pushNotiEnabled: boolean
  durNotiEnabled: boolean
  isModalVisible: boolean
  modalContentType: "preg" | "byg" | "hng" | "dis" | "nosale"
}

@inject("accountStore")
@observer
export default class PrivacyPolicyAndTermsScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "약관 및 개인정보 처리방침",
    headerRight: null,
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  render() {
    const user = this.props.accountStore.user
    const newly_agreed_date = formatDateWithJoinChar(user.newly_agreed_date, "-")
    const ad_noti_date = formatDateWithJoinChar(user.ad_noti_date, "-")
    return (
      <View style={{ flex: 1, backgroundColor: "rgb(239, 239, 239)" }}>
        <ScrollView>
          <View style={CONTENT_CONTAINER}>
            <View style={SECTION_HEADER}>
              <Text style={SECTION_TITLE}>필수 항목</Text>
            </View>
            <TouchableOpacity
              style={SETTING_ROW}
              onPress={() => this.props.navigation.navigate("TermsOfService")}
            >
              <Text style={PROPERTY_NAME}>서비스 이용약관</Text>
              <View style={FLEX_END}>
                <Text ellipsizeMode={"tail"} numberOfLines={1} style={AGREED_DATE_TEXT}>
                  {user.newly_agreed_date ? newly_agreed_date + " 동의" : null}
                </Text>
                <SvgArrowRight width={24} height={24} color={color.primary} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={SETTING_ROW}
              onPress={() =>
                this.props.navigation.navigate("WebView", {
                  url: "https://web.papricacare.com/agrees/sensitive-info-agreement.html",
                })
              }
            >
              <Text style={PROPERTY_NAME}>민감정보 수집이용 동의</Text>
              <View style={FLEX_END}>
                {user.is_agreed}
                <Text ellipsizeMode={"tail"} numberOfLines={1} style={AGREED_DATE_TEXT}>
                  {user.newly_agreed_date ? newly_agreed_date + " 동의" : null}
                </Text>
                <SvgArrowRight width={24} height={24} color={color.primary} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[SETTING_ROW, { marginBottom: 5 }]}
              onPress={() =>
                this.props.navigation.navigate("WebView", {
                  url: "https://web.papricacare.com/agrees/personal-data-agreement.html",
                })
              }
            >
              <Text style={PROPERTY_NAME}>개인정보 수집이용 동의</Text>
              <View style={FLEX_END}>
                <Text ellipsizeMode={"tail"} numberOfLines={1} style={AGREED_DATE_TEXT}>
                  {user.newly_agreed_date ? newly_agreed_date + " 동의" : null}
                </Text>
                <SvgArrowRight width={24} height={24} color={color.primary} />
              </View>
            </TouchableOpacity>
          </View>
          {user.id != -1 ? (
            <View style={CONTENT_CONTAINER}>
              <View style={SECTION_HEADER}>
                <Text style={SECTION_TITLE}>선택 항목</Text>
              </View>
              <TouchableOpacity
                style={[SETTING_ROW, { marginBottom: 5 }]}
                onPress={() => this.props.navigation.navigate("MarketingAgreement")}
              >
                <Text style={PROPERTY_NAME}>마케팅 정보 수신 및 이용 동의</Text>
                <View style={FLEX_END}>
                  {user.ad_noti === "y" ? (
                    <Text ellipsizeMode={"tail"} numberOfLines={1} style={AGREED_DATE_TEXT}>
                      {ad_noti_date} 동의
                    </Text>
                  ) : null}
                  <SvgArrowRight width={24} height={24} color={color.primary} />
                </View>
              </TouchableOpacity>
            </View>
          ) : null}
          <View style={CONTENT_CONTAINER}>
            <TouchableOpacity
              style={[SETTING_ROW, { marginVertical: 5 }]}
              onPress={() => this.props.navigation.navigate("PrivacyPolicy")}
            >
              <Text style={PROPERTY_NAME}>개인정보 처리방침</Text>
              <View style={FLEX_END}>
                <SvgArrowRight width={24} height={24} color={color.primary} />
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }
}
