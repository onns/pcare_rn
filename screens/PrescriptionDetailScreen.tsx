import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { inject, observer } from "mobx-react"
import { onAction } from "mobx-state-tree"
import { Icon, ScrollView, Spinner, TextArea, Toast, View } from "native-base"
import React, { Component } from "react"
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  ImageStyle,
  Platform,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import Modal from "react-native-modal"
import RBSheet from "react-native-raw-bottom-sheet"
import { SafeAreaInsetsContext, SafeAreaView } from "react-native-safe-area-context"
import { Route, TabBar, TabView } from "react-native-tab-view"

import { api } from "@api/api"
import { Slider } from "@miblanchard/react-native-slider"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgArrowRight from "../assets/images/arrowRight.svg"
import SvgDisease from "../assets/images/disease.svg"
import SvgHospital from "../assets/images/hospital.svg"
import SvgInfo from "../assets/images/info.svg"
import SvgMoreHorizontal from "../assets/images/moreHorizontal.svg"
import TouchableItem from "../components/button/TouchableItem"
import CloseButton from "../components/CloseButton"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { deviceHeight, deviceWidth } from "../components/Popup"
import PrescriptionDetailItem from "../components/PrescriptionDetailItem"
import { Text } from "../components/StyledText"
import { formatDateFromString, getDayBefore } from "../lib/DateUtil"
import {
  DetailsStackParamList,
  MainTabParamList,
  PrescriptionsStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { Prescription, PrescriptionDetail, PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"
import {
  rating1Text,
  rating2Text,
  rating3Text,
  rating4Text,
  rating5Text,
} from "./DrugAdherenceRatingScreen"
import { FamilyMemberType } from "./PrescriptionPreviewScreen"

const infoSource = require("../assets/images/info.png")
const saveSource = require("../assets/images/iconSave.png")

const iconEmojiArray = [
  require("../assets/images/iconEmojiSoBad.png"),
  require("../assets/images/iconEmojiBad.png"),
  require("../assets/images/iconEmojiNormal.png"),
  require("../assets/images/iconEmojiNice.png"),
  require("../assets/images/iconEmojiSoNice.png"),
]
const iconCheckedEmojiArray = [
  require("../assets/images/iconEmojiSoBadChecked.png"),
  require("../assets/images/iconEmojiBadChecked.png"),
  require("../assets/images/iconEmojiNormalChecked.png"),
  require("../assets/images/iconEmojiNiceChecked.png"),
  require("../assets/images/iconEmojiSoNiceChecked.png"),
]

const CONTENT_CONTAINER: ViewStyle = {}
const DISEASE_DETAIL_NAME: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const RIGHT_ARROW: ViewStyle = {
  justifyContent: "center",
  alignItems: "flex-end",
}
const TOUCHABLE_ITEM: ViewStyle = {
  backgroundColor: "#fff",
  paddingTop: 16,
  paddingLeft: 16,
  paddingRight: 16,
  paddingBottom: 16,
}
const TOUCHABLE_MIDDLE_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
}
const TOUCHABLE_FIRST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
}
const TOUCHABLE_LAST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
  paddingBottom: 24,
}
const TOUCHABLE_FIRST_LAST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
  paddingVertical: 24,
}
const DRUG_SECTION_1: ViewStyle = {
  backgroundColor: "#fff",
  paddingTop: 16,
  paddingBottom: 15,
  paddingLeft: 20,
  paddingRight: 20,
  borderBottomColor: color.line,
  borderBottomWidth: 1,
}
const DRUG_SECTION_2: ViewStyle = {
  flexDirection: "row",
  backgroundColor: "#fff",
  paddingTop: 6,
  paddingLeft: 20,
  paddingBottom: 3,
  paddingRight: 15,
  borderBottomColor: color.line,
  borderBottomWidth: 1,
}
const PROGRESS_BAR: ViewStyle = {
  paddingTop: 10,
}
const TITLE = {
  ...typography.title3,
}
const TITLE_AREA: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingTop: 24,
  paddingLeft: 24,
}

const hitSlop20 = { top: 20, left: 20, right: 20, bottom: 20 }

const RX_DETAIL_DIVIDER = {
  marginLeft: 16,
  marginRight: 16,
  height: 1,
  backgroundColor: color.palette.gray5,
}
const DISEASE_DIVIDER = {
  marginLeft: 16,
  marginRight: 16,
  height: 1,
  backgroundColor: color.palette.gray5,
}
const HEADER_SUBTITLE = {
  ...typography.sub2,
  opacity: 0.5,
  marginTop: 5,
}
const DOCTOR_NAME = {
  ...typography.sub2,
  marginTop: 4,
  opacity: 1,
}
const DOCTOR = {
  ...typography.sub2,
  marginTop: 4,
  opacity: 0.5,
}
const INFO_MARK: ImageStyle = {
  width: 14,
  height: 14,
  resizeMode: "contain",
}
const EDIT_IMAGE: ImageStyle = {
  width: 16,
  height: 16,
  resizeMode: "contain",
}
const ROW_VERT_CENTER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const RATING_BOX: ViewStyle = {
  alignItems: "center",
  paddingTop: 20,
  paddingBottom: 15,
  backgroundColor: "white",
}
const RATING_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontWeight: "bold",
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
}
const RATING_AVAILABLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 24,
  letterSpacing: -0.8,
  color: "rgb(153, 153, 153)",
}
const RATING_UNAVAILABLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 24,
  letterSpacing: -0.6,
  color: color.palette.grey3,
}
const EDIT_TEXT: TextStyle = {
  fontWeight: "bold",
  lineHeight: 24,
  letterSpacing: -0.6,
  color: color.palette.darkSkyBlue,
}
const SAVE_TEXT: TextStyle = {
  ...EDIT_TEXT,
  color: color.primary,
}
const EDIT_BUTTON: ViewStyle = {
  ...ROW_VERT_CENTER,
  marginBottom: 3,
  justifyContent: "flex-end",
  marginRight: 20,
}
const CLOSE_BUTTON: ViewStyle = {
  marginRight: 0,
}
const FULL: ViewStyle = { flex: 1 }
const POPUP: ViewStyle = {
  borderBottomLeftRadius: 10,
  borderBottomRightRadius: 10,
  backgroundColor: "rgb(239, 239, 239)",
  paddingHorizontal: 24,
  paddingBottom: 32,
}
const POPUP_TITLE: TextStyle = {
  ...typography.title3,
}
const POPUP_TITLE_RIGHT: ViewStyle = {
  flex: 1,
  justifyContent: "flex-end",
  alignItems: "flex-end",
  paddingRight: 4,
}
const POPUP_CONTENT: TextStyle = {
  ...typography.body,
}
const POPUP_DESCRIPTION: TextStyle = {
  ...typography.sub2,
  opacity: 0.7,
  marginTop: 16,
}
const RATING_TEXT_GRAY_BOX: ViewStyle = {
  width: "90%",
  height: 40,
  alignItems: "center",
  justifyContent: "center",
  borderColor: "rgb(217,217,217)",
  borderWidth: 1,
  borderRadius: 20,
  marginTop: 15,
}
const HEADER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  paddingLeft: Platform.OS === "android" ? 10 : 0,
  backgroundColor: color.palette.white,
  borderBottomWidth: 0,
  borderBottomColor: color.palette.white,
  elevation: 0,
  shadowColor: "transparent",
}
const POPUP_HEADER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingTop: 24,
  paddingBottom: 24,
  paddingHorizontal: 16,
  borderTopLeftRadius: 10,
  borderTopRightRadius: 10,
  backgroundColor: "rgb(239, 239, 239)",
}
const HEADER_TITLE_BOX: ViewStyle = {
  alignItems: "center",
  paddingVertical: 4,
  justifyContent: "center",
}
const CHEVRON_RIGHT: TextStyle = { fontSize: 24, color: color.palette.gray3 }
const DRUG_SECTION_HEADER: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingTop: 24,
  paddingBottom: 8,
  paddingHorizontal: 16,
}
const TITLE2_ORANGE: TextStyle = { ...typography.title2, color: color.primary }
const TEXT_BUTTON: TextStyle = {
  ...typography.buttons.scaledUp,
  color: color.textColor.action,
}
const TEXT_BUTTON_CONTAINER: ViewStyle = {
  // paddingHorizontal: 16,
  width: 60,
  height: 40,
  justifyContent: "center",
  alignItems: "center",
}

type PrescriptionDetailRouteProp = RouteProp<PrescriptionsStackParamList, "PrescriptionDetail">

type PrescriptionDetailNavigationProp = CompositeNavigationProp<
  StackNavigationProp<PrescriptionsStackParamList, "PrescriptionDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  prescriptionStore: PrescriptionStore
  accountStore: AccountStore
  navigation: PrescriptionDetailNavigationProp
  route: PrescriptionDetailRouteProp
}

interface State {
  bottomSheetMode: "more" | "transfer"
  drugsBeingTaken: Array<PrescriptionDetail>
  drugsTaken: Array<PrescriptionDetail>
  dosingDays: number
  index: number
  routes: Array<Route>
  drugAdherenceRating: number
  symptomImprovementRating: number
  memo: string
  memoEdited: boolean
  isModalVisible: boolean
  popupTitle: string
  popupContent: string
  popupDescription: string
}

@inject("prescriptionStore", "accountStore")
@observer
export default class PrescriptionDetailScreen extends Component<Props, State> {
  /**
   * headerStyle 이 iOS 에 적용되지 않는 문제로 header 전체를 구현함.
   * TODO: back action interpolation 조정 필요
   */
  static navigationOptions = {
    header: ({ route, progress, navigation }) => {
      const { params } = route
      const { previous } = progress

      let startDate
      let endDate
      let periodText = ""

      if (params?.startDate) {
        startDate = params.startDate
        endDate = params.endDate
        if (!endDate) {
          periodText = formatDateFromString(startDate)
        } else {
          periodText = `${formatDateFromString(startDate)} ~ ${formatDateFromString(endDate)}`
        }
      }
      return (
        <SafeAreaInsetsContext.Consumer>
          {(insets) => (
            <View
              style={[
                HEADER,
                {
                  paddingTop: insets.top,
                  height: Platform.OS === "android" ? 64 + insets.top : 76 + insets.top,
                },
              ]}
            >
              {previous ? (
                <TouchableItem onPress={navigation.goBack}>
                  <CustomHeaderBackImage />
                </TouchableItem>
              ) : (
                <View style={{ width: 60, height: 40 }} />
              )}
              <View style={HEADER_TITLE_BOX}>
                <Text style={typography.title3}>처방내역</Text>
                {/* <Text style={HEADER_SUBTITLE}>{periodText}</Text> */}
              </View>
              <TouchableOpacity
                // hitSlop={hitSlop10}
                onPress={params?.handleMorePress ?? null}
                style={TEXT_BUTTON_CONTAINER}
              >
                {/* <SvgMoreHorizontal width={24} height={24} /> */}
                <Text style={TEXT_BUTTON}>편집</Text>
              </TouchableOpacity>
            </View>
          )}
        </SafeAreaInsetsContext.Consumer>
      )
    },
  }

  bottomSheetRef = React.createRef<RBSheet>()
  prescriptionId = -1
  data: any
  onActionDisposer: any
  transferFromMemberId: number
  transferFromMemberKey: string

  constructor(props: Props) {
    super(props)

    this.state = {
      bottomSheetMode: "more",
      drugsBeingTaken: [],
      drugsTaken: [],
      dosingDays: 0,
      index: 0,
      routes: [
        { key: "prescriptionDetail", title: "상세" },
        { key: "evaluation", title: "평가 및 메모" },
      ],
      drugAdherenceRating: 0,
      symptomImprovementRating: 0,
      memo: "",
      memoEdited: false,
      isModalVisible: false,
      popupTitle: "",
      popupContent: "",
      popupDescription: undefined,
    }
    this.prescriptionId = this.getPrescriptionId(this.props.route.params)

    this.onActionDisposer = onAction(this.props.prescriptionStore, (call) => {
      if (call.name === "setPrescription" && call.args) {
        const f_value = call.args[0].f_value
        const f_effect = call.args[0].f_effect
        const f_memo = call.args[0].f_memo
        this.setState({
          drugAdherenceRating: f_value || 0,
          symptomImprovementRating: f_effect || 0,
          memo: f_memo || "",
        })
      } else if (call.name === "setDrugAdherenceRating" && call.args) {
        this.setState({
          drugAdherenceRating: call.args[0],
        })
      } else if (call.name === "setSymptomImprovementRating" && call.args) {
        this.setState({
          symptomImprovementRating: call.args[0],
        })
      } else if (
        call.name === "removeDisease" ||
        call.name === "removePrescriptionDetail" ||
        call.name === "removeHospital"
      ) {
        this.setState({}) // 질병, 약제가 삭제된 경우, re-render 시킴
      }
    })
  }

  async componentDidMount() {
    await this.props.prescriptionStore.fetchPrescriptionDetail(
      this.prescriptionId,
      this.props.accountStore.user.id,
    )
    await this.setState({ memo: this.props.prescriptionStore.prescriptionDetail.f_memo })
    const { route } = this.props
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })
    await this.props.navigation.setParams({ handleMorePress: this.handleMorePress })
  }

  componentWillUnmount() {
    this.onActionDisposer()
  }

  getPrescriptionId(params: any): any {
    return params.prescriptionId
  }

  renderDiseaseDivider() {
    return <View style={DISEASE_DIVIDER}></View>
  }

  isSamplePrescription(): boolean {
    const store = this.props.prescriptionStore
    return (
      store.prescriptionDetail &&
      store.prescriptionDetail.disease.length > 0 &&
      (store.prescriptionDetail.disease[0].name_kr.includes("샘플 처방전") ||
        store.prescriptionDetail.disease[0].name_kr.includes("처방전 예시"))
    )
  }

  render() {
    const store = this.props.prescriptionStore
    if (this.isSamplePrescription()) {
      const today = new Date()
      store.prescriptionDetail.setIssueDate(getDayBefore(today, 2, "-"))
    }

    const adherenceEmojiList: Array<any> = []
    const symptomEmojiList: Array<any> = []

    for (let index = 1; index <= 5; index++) {
      adherenceEmojiList.push(this.renderEmojiByType(index, "adherence"))
    }
    for (let index = 1; index <= 5; index++) {
      symptomEmojiList.push(this.renderEmojiByType(index, "symptom"))
    }

    if (store.status == "error") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`서버로부터 데이터를 가져오지 못 하였습니다.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (store.status == "pending") {
      return <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
    }

    const prescription = this.props.prescriptionStore.prescriptionDetail
    const family = this.props.accountStore.family

    return (
      <View style={{ flex: 1, backgroundColor: "#f9f7f4", borderTopWidth: 0 }}>
        <TabView
          navigationState={this.state}
          renderScene={({ route, jumpTo }) => {
            switch (route.key) {
              case "prescriptionDetail":
                return this.renderPrescriptionDetail()
              case "evaluation":
                return (
                  <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
                    <View style={[TITLE_AREA, ROW_VERT_CENTER, { paddingTop: 15 }]}>
                      <Text style={TITLE}>증상개선도</Text>
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({
                            popupTitle: "증상개선도 기록은 왜 필요한가요?",
                            popupContent:
                              "대부분의 의료진은 질병에 따른 경험적이고 보편적인 치료법으로 환자의 치료를 접근하고 있습니다. 치료 성공률이 가장 높다고 판단되는 치료법을 의료진 개별 근거에 따라 선택하여 여러 환자들에게 적용을 합니다. 하지만 같은 질병을 가지고 있다고 하더라도 환자 개개인의 차이로 치료효과는 달라질 수 있습니다. 현재 복용 중인 약제로 인한 증상개선정도 기록은 나에게 맞는 치료법을 찾는데 큰 도움이 될 수 있습니다.",
                            isModalVisible: true,
                          })
                        }
                        hitSlop={hitSlop20}
                      >
                        <Image
                          source={infoSource}
                          style={[INFO_MARK, { marginLeft: 4, marginBottom: 1 }]}
                        />
                      </TouchableOpacity>
                      <View style={{ flex: 1 }} />
                      {/* {prescription.drugsBeingTaken.length == 0 ? (
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate(
                              "SymptomImprovementRating",
                              {
                                prescriptionId: prescription.id,
                                rating: prescription.f_value,
                                issueDate: prescription.issue_date,
                                repTitle:
                                  prescription.disease.length > 0
                                    ? prescription.disease[0].rep_title
                                    : "질병정보 없음"
                              }
                            )
                          }
                          style={EDIT_BUTTON}
                        >
                          <Image source={editSource} style={EDIT_IMAGE} />
                          <Text style={EDIT_TEXT}>
                            {prescription.f_effect == null ? "기록" : "수정"}
                          </Text>
                        </TouchableOpacity>
                      ) : null} */}
                    </View>
                    <View style={RATING_BOX}>
                      <Text
                        style={{
                          ...typography.body,
                          marginBottom: 20,
                        }}
                      >
                        증상은 좀 나아지셨나요?
                      </Text>
                      <View style={{ flexDirection: "row", alignItems: "center" }}>
                        {symptomEmojiList}
                      </View>

                      <View style={RATING_TEXT_GRAY_BOX}>
                        <Text
                          style={
                            prescription && prescription.drugsBeingTaken.length > 0
                              ? RATING_UNAVAILABLE_TEXT
                              : this.state.symptomImprovementRating == 0
                              ? RATING_AVAILABLE_TEXT
                              : RATING_TEXT
                          }
                        >
                          {this.getSymptomImprovementRatingLabel(
                            this.state.symptomImprovementRating,
                          )}
                        </Text>
                      </View>
                    </View>
                    <View style={[TITLE_AREA, ROW_VERT_CENTER, { paddingTop: 15 }]}>
                      <Text style={TITLE}>메모</Text>
                      <View style={{ flex: 1 }} />
                      {this.state.memoEdited ? (
                        <TouchableOpacity onPress={() => this.saveMemo()} style={EDIT_BUTTON}>
                          <Image source={saveSource} style={EDIT_IMAGE} />
                          <Text style={SAVE_TEXT}>저장</Text>
                        </TouchableOpacity>
                      ) : null}
                    </View>
                    <View
                      style={{
                        backgroundColor: "white",
                        padding: 20,
                        borderBottomColor: color.line,
                        borderBottomWidth: 1,
                      }}
                    >
                      <TextArea
                        maxFontSizeMultiplier={1.3}
                        rowSpan={5}
                        bordered
                        underline
                        placeholder="처방약과 관련해서 느끼신 점이나 특이사항을 미래의 나를 위해 남겨주세요.(1000자 이내)"
                        placeholderTextColor={`${color.text}48`}
                        style={{
                          borderRadius: 4,
                          borderWidth: 1,
                          borderColor: color.palette.pale,
                          ...typography.body,
                          paddingHorizontal: 15,
                        }}
                        value={this.state.memo}
                        onChangeText={(text) => {
                          this.setState({
                            memo: text.substr(0, 1000),
                            memoEdited: true,
                          })
                        }}
                      />
                    </View>
                  </ScrollView>
                )
            }
          }}
          // renderScene={SceneMap({
          //   prescriptionDetail: this.renderPrescriptionDetail,
          //   evaluation: this.renderEvaluation
          // })}
          renderTabBar={this.renderTabBar}
          onIndexChange={(index) => this.setState({ index: index })}
          initialLayout={{ width: Dimensions.get("window").width }}
        />
        <Modal
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
          isVisible={this.state.isModalVisible}
          animationOut={"fadeOutDown"}
          // animationOutTiming={300}
          onBackdropPress={this.toggleModal}
        >
          <View>
            <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
            <View style={POPUP_HEADER}>
              <View style={FULL} />
              <Text style={POPUP_TITLE}>{this.state.popupTitle}</Text>
              <View style={POPUP_TITLE_RIGHT}>
                <CloseButton
                  onPress={() => this.toggleModal()}
                  style={CLOSE_BUTTON}
                  imageStyle={{ tintColor: color.palette.gray3 }}
                />
              </View>
            </View>
            <View style={POPUP}>
              <Text style={POPUP_CONTENT}>{this.state.popupContent}</Text>
              {this.state.popupDescription ? (
                <Text style={POPUP_DESCRIPTION}>{this.state.popupDescription}</Text>
              ) : null}
            </View>
          </View>
        </Modal>
        <RBSheet
          ref={this.bottomSheetRef}
          height={
            this.state.bottomSheetMode === "more"
              ? styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * 4 + 14
              : styles.bottomSheetHeaderHeight +
                styles.bottomSheetButtonHeight * (family.length + 1) +
                14
          }
          openDuration={250}
          closeDuration={250}
          customStyles={{
            container: {
              backgroundColor: "transparent",
            },
          }}
        >
          <StatusBar backgroundColor={color.dimmedStatusBar} />
          {this.state.bottomSheetMode == "more"
            ? this.renderMoreBottomSheet()
            : this.renderTransferBottomSheet()}
        </RBSheet>
      </View>
    )
  }

  renderTabBar = (props: any) => (
    <TabBar
      {...props}
      indicatorStyle={{
        borderBottomWidth: 2,
        borderBottomColor: color.primary,
      }}
      style={{
        height: 34,
        backgroundColor: "#fff",
        borderColor: color.palette.pale,
        borderBottomWidth: 1,
        padding: 0,
        elevation: 0,
      }}
      // dynamicWidth
      // scrollEnabled
      tabStyle={{
        // width: "auto",
        height: 34,
        paddingVertical: 0,
        paddingHorizontal: 16,
      }}
      labelStyle={{
        fontFamily: typography.primary,
        fontSize: 15,
        lineHeight: 19,
        letterSpacing: -0.6,
        paddingBottom: 10,
      }}
      activeColor={color.primary}
      inactiveColor="rgb(153, 153, 153)"
    />
  )

  renderMoreBottomSheet() {
    const { prescriptionStore } = this.props
    const { prescriptionDetail } = prescriptionStore
    return (
      <View>
        <View style={styles.BOTTOM_SHEET_HEADER}>
          <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>원하시는 항목을 선택해 주세요.</Text>
        </View>
        <View>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              if (
                prescriptionDetail.disease.length > 0 &&
                (prescriptionDetail.disease[0].name_kr.includes("샘플 처방전") ||
                  prescriptionDetail.disease[0].name_kr.includes("처방전 예시"))
              ) {
                Toast.show({
                  title: (
                    <Text style={styles.TOAST_TEXT}>{`예시 처방전은 이동할 수 없습니다.`}</Text>
                  ),
                  duration: 2000,
                  placement: "top",
                  style: styles.TOAST_VIEW,
                })
                return
              } else if (prescriptionStore.selectedFamilyMemberKey.includes("f")) {
                Toast.show({
                  title: (
                    <Text
                      style={styles.TOAST_TEXT}
                    >{`14세 이상 가족회원의 처방전 이동은 추후 지원 예정입니다.`}</Text>
                  ),
                  duration: 2500,
                  placement: "top",
                  style: styles.TOAST_VIEW,
                })
                return
              }
              this.setState({ bottomSheetMode: "transfer" })
              setTimeout(() => this.bottomSheetRef.current.open(), 300)
            }}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>이동</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_LAST_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={this.handleEditPress}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>수정</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_LAST_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={this.handleDeletePress}
          >
            <Text style={[styles.BOTTOM_SHEET_BUTTON_TEXT, { color: color.primary }]}>삭제</Text>
          </TouchableHighlight>
        </View>
        <SafeAreaView style={{ backgroundColor: color.palette.white }}>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_CANCEL_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => this.bottomSheetRef.current.close()}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>취소</Text>
          </TouchableHighlight>
        </SafeAreaView>
      </View>
    )
  }

  renderTransferBottomSheet() {
    const pStore = this.props.prescriptionStore
    const { accountStore } = this.props
    const family = accountStore.family
    return (
      <View>
        <View style={styles.BOTTOM_SHEET_HEADER}>
          <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>
            누구의 처방전 목록으로 이동하시겠습니까?
          </Text>
        </View>
        <View style={{ backgroundColor: color.palette.white }}>
          {pStore.selectedFamilyMemberKey.includes("u") ? (
            this.setTransferFromMemberInfo(accountStore.user.id, pStore.selectedFamilyMemberKey)
          ) : (
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={() => {
                this.bottomSheetRef.current.close()
                this.transferPrescription(
                  pStore.prescriptionDetail.id,
                  FamilyMemberType.Self,
                  accountStore.user.id,
                  accountStore.user.nickname,
                )
              }}
            >
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>{accountStore.user.nickname}</Text>
            </TouchableHighlight>
          )}
          {family.map((member, index) => {
            if (
              pStore.selectedFamilyMemberKey.includes("f") &&
              pStore.selectedFamilyMemberKey.includes(String(member.family_user))
            ) {
              return this.setTransferFromMemberInfo(
                member.family_user,
                pStore.selectedFamilyMemberKey,
              )
            } else if (
              pStore.selectedFamilyMemberKey.includes("c") &&
              pStore.selectedFamilyMemberKey.includes(String(member.id))
            ) {
              return this.setTransferFromMemberInfo(member.id, pStore.selectedFamilyMemberKey)
            } else {
              return (
                <TouchableHighlight
                  key={index}
                  style={
                    index == family.length - 1
                      ? styles.BOTTOM_SHEET_LAST_BUTTON
                      : styles.BOTTOM_SHEET_BUTTON
                  }
                  underlayColor={color.palette.grey2}
                  onPress={() => {
                    this.bottomSheetRef.current.close()
                    this.transferPrescription(
                      pStore.prescriptionDetail.id,
                      member.family_user ? FamilyMemberType.FamilyUser : FamilyMemberType.Child,
                      member.family_user ? member.family_user : member.id,
                      member.name,
                    )
                  }}
                >
                  <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>{member.name}</Text>
                </TouchableHighlight>
              )
            }
          })}
          <SafeAreaView>
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_CANCEL_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={() => {
                this.bottomSheetRef.current.close()
                this.setState({ bottomSheetMode: "more" })
              }}
            >
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>취소</Text>
            </TouchableHighlight>
          </SafeAreaView>
        </View>
      </View>
    )
  }

  setTransferFromMemberInfo(memberId: number, memberKey: string): any {
    this.transferFromMemberId = memberId
    this.transferFromMemberKey = memberKey
    return null
  }

  renderPrescriptionDetail() {
    const prescriptionDetail = this.props.prescriptionStore.prescriptionDetail
    return (
      <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
        {prescriptionDetail &&
          prescriptionDetail.disease.map((disease, index) => (
            <View key={disease.id}>
              <TouchableOpacity
                // style={prescriptionDetail!.disease.length == (index + 1) && prescriptionDetail!.subdiseases.length == 0 ? TOUCHABLE_ITEM : TOUCHABLE_FIRST_ITEM}
                style={
                  index !== 0
                    ? prescriptionDetail!.disease.length === index + 1
                      ? prescriptionDetail.subdiseases.length === 0
                        ? TOUCHABLE_LAST_ITEM
                        : TOUCHABLE_MIDDLE_ITEM
                      : TOUCHABLE_MIDDLE_ITEM
                    : prescriptionDetail.subdiseases.length === 0
                    ? TOUCHABLE_FIRST_LAST_ITEM
                    : TOUCHABLE_FIRST_ITEM
                }
                onPress={() => {
                  this.props.navigation.navigate("DiseaseSummary", {
                    id: disease.id,
                    code: disease.code,
                    index: index,
                    guideTabVisible: !!disease.guide,
                  })
                }}
              >
                <View style={ROW}>
                  <SvgDisease width={40} height={40} style={{ marginLeft: -4, marginRight: 8 }} />
                  <View style={{ flex: 7 }}>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                      {disease.rep_title}
                    </Text>
                    <Text style={DISEASE_DETAIL_NAME}>
                      {this.isSamplePrescription()
                        ? disease.code.substring(1) +
                          " " +
                          disease.name_kr.replace("(샘플 처방전)", "").replace("(처방전 예시)", "")
                        : disease.code + " " + disease.name_kr}
                    </Text>
                  </View>
                  <View style={RIGHT_ARROW}>
                    <SvgArrowRight width={24} height={24} color={color.palette.grey30} />
                  </View>
                </View>
              </TouchableOpacity>
              {prescriptionDetail!.disease.length === index + 1
                ? prescriptionDetail.subdiseases.length === 0
                  ? null
                  : this.renderDiseaseDivider()
                : this.renderDiseaseDivider()}
            </View>
          ))}
        {prescriptionDetail &&
          prescriptionDetail.subdiseases.map((disease, index) => (
            <View key={disease.id}>
              <TouchableOpacity
                style={
                  prescriptionDetail!.subdiseases.length === index + 1
                    ? TOUCHABLE_LAST_ITEM
                    : TOUCHABLE_MIDDLE_ITEM
                }
                onPress={() => {
                  this.props.navigation.navigate("DiseaseSummary", {
                    id: disease.id,
                    code: disease.code,
                    index: index,
                    isSubDisease: true,
                    guideTabVisible: !!disease.guide,
                  })
                }}
              >
                <View style={ROW}>
                  <SvgDisease width={40} height={40} style={{ marginLeft: -4, marginRight: 8 }} />
                  <View style={{ flex: 7 }}>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                      {disease.rep_title}
                    </Text>
                    <Text style={DISEASE_DETAIL_NAME}>
                      {disease.code} {disease.name_kr}
                    </Text>
                    {/* <Text style={DISEASE_ONE_LINER}>
                    {disease.one_liner.length > 0
                      ? disease.one_liner
                      : "상세 설명을 준비 중입니다."}
                  </Text> */}
                    {/* <Text style={PRESCRIPTION_ISSUE_DATE}>{prescriptionDetail != undefined ? prescriptionDetail.issue_date : ""}</Text> */}
                  </View>
                  <View style={RIGHT_ARROW}>
                    <SvgArrowRight width={24} height={24} color={color.palette.grey30} />
                  </View>
                </View>
              </TouchableOpacity>
              {prescriptionDetail!.subdiseases.length === index + 1
                ? null
                : this.renderDiseaseDivider()}
            </View>
          ))}
        {prescriptionDetail &&
        prescriptionDetail.disease.length === 0 &&
        prescriptionDetail.subdiseases.length === 0 ? (
          <View style={TOUCHABLE_FIRST_LAST_ITEM}>
            <View style={ROW}>
              <SvgDisease width={40} height={40} style={{ marginLeft: -4, marginRight: 8 }} />
              <View style={{ flex: 7 }}>
                <Text style={typography.title3}>질병명 없음</Text>
                <View style={{ marginTop: 5 }}>
                  <Text
                    style={{
                      ...typography.sub2,
                      opacity: 0.5,
                    }}
                  >
                    질병분류기호가 없습니다.
                  </Text>
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  popupTitle: "왜 질병명이 없나요?",
                  popupContent:
                    "질병분류기호가 기재되지 않은 처방전 또는 약봉투의 경우 처방받은 질병명을 확인할 수 없습니다.",
                  popupDescription:
                    "*정확한 의료 정보 관리를 위해서 처방전을 받으셨을 때, 질병분류기호 기재 여부를 확인하세요. 다음번 병원 방문시 질병분류기호가 없으면 기재를 요청해보세요.",
                  isModalVisible: true,
                })
              }
              hitSlop={hitSlop20}
              style={{
                backgroundColor: "#f6f3f2",
                borderRadius: 1,
                marginTop: 16,
                paddingLeft: 8,
                paddingRight: 16,
                paddingVertical: 8,
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <SvgInfo
                width={16}
                height={16}
                style={{ marginTop: Platform.OS === "ios" ? 1 : 0, marginRight: 4 }}
              />
              <Text style={{ ...typography.sub2, flex: 1 }}>왜 질병명이 없나요?</Text>
              <Text
                style={{
                  fontFamily: typography.primary,
                  fontSize: 14,
                  lineHeight: 20,
                  color: color.text,
                  opacity: 0.7,
                }}
              >
                보기
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
        <View
          style={{
            height: 8,
            backgroundColor: "#f9f7f4",
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderTopColor: color.palette.pale,
            borderBottomColor: color.palette.pale,
          }}
        />
        {prescriptionDetail != undefined && prescriptionDetail.hospital != null ? (
          <TouchableOpacity
            style={TOUCHABLE_FIRST_LAST_ITEM}
            onPress={() =>
              this.props.navigation.navigate("HospitalDetail", {
                id: prescriptionDetail ? prescriptionDetail.hospital!.id : -1,
              })
            }
          >
            <View style={ROW}>
              <SvgHospital width={40} height={40} style={{ marginLeft: -4, marginRight: 8 }} />
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                    {prescriptionDetail ? prescriptionDetail.hospital!.name : ""}
                  </Text>
                </View>
                {prescriptionDetail &&
                prescriptionDetail.doctor &&
                prescriptionDetail.doctor.name ? (
                  <View style={ROW}>
                    <Text style={DOCTOR}>담당의 </Text>
                    <Text style={DOCTOR_NAME}>
                      {prescriptionDetail.doctor.name}
                      {prescriptionDetail.doctor.narrow_major
                        ? `(${prescriptionDetail.doctor.narrow_major})`
                        : ""}
                    </Text>
                  </View>
                ) : (
                  <Text style={DOCTOR}>의료진 정보가 없습니다.</Text>
                )}
              </View>
              <View style={RIGHT_ARROW}>
                <SvgArrowRight width={24} height={24} color={color.palette.grey30} />
              </View>
            </View>
          </TouchableOpacity>
        ) : (
          <View style={TOUCHABLE_FIRST_LAST_ITEM}>
            <View style={ROW}>
              <SvgHospital width={40} height={40} style={{ marginLeft: -4, marginRight: 8 }} />
              <View style={{ flex: 1, paddingBottom: 5 }}>
                <Text style={typography.title3}>병원정보 없음</Text>
                <Text style={DOCTOR}>의료진 정보가 없습니다.</Text>
              </View>
            </View>
          </View>
        )}
        <View
          style={{
            height: 8,
            backgroundColor: "#f9f7f4",
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderTopColor: color.palette.pale,
            borderBottomColor: color.palette.pale,
          }}
        />
        {/* {prescriptionDetail != undefined && prescriptionDetail.drugsBeingTaken.length > 0
          ? this.renderDrugSection1(true)
          : this.renderDrugSection1(false)} */}
        <View style={DRUG_SECTION_HEADER}>
          <Text style={typography.title2}>
            처방받은 약{" "}
            <Text style={TITLE2_ORANGE}>{prescriptionDetail?.prescription_details?.length}</Text>
          </Text>
        </View>
        {prescriptionDetail && prescriptionDetail.drugsBeingTaken.length > 0 ? (
          <FlatList
            data={prescriptionDetail.drugsBeingTaken}
            renderItem={({ item, index }: { item: PrescriptionDetail; index: number }) => (
              <PrescriptionDetailItem
                key={item.id}
                enableDur={prescriptionDetail.dosingDays - prescriptionDetail.diffDays > 0}
                diffDays={prescriptionDetail.diffDays}
                prescriptionDetail={item}
                durRes={prescriptionDetail.durRes}
                onPress={() =>
                  this.props.navigation.navigate("DrugDetail", {
                    id: item.drug.id,
                  })
                }
                // style={prescriptionDetail.drugsBeingTaken.length == index + 1 ?
                //   { borderBottomLeftRadius: 4, borderBottomRightRadius: 4 } : null }
              />
            )}
            ItemSeparatorComponent={() => (
              <View style={{ backgroundColor: "#fff" }}>
                <View style={RX_DETAIL_DIVIDER}></View>
              </View>
            )}
            // onRefresh={pStore.fetchPrescriptions}
            // refreshing={pStore.isLoading}
            keyExtractor={(item) => item.id.toString()}
            // extraData={{ extra: store.prescriptions }}
            // ListEmptyComponent={<EmptyPrescriptionList />}
          />
        ) : null}
        {/* {
          prescriptionDetail != undefined && prescriptionDetail.drugsTaken.length > 0 ?
          this.renderDrugSection1(false)
          : null
        } */}
        {prescriptionDetail && prescriptionDetail.drugsTaken.length > 0 ? (
          <FlatList
            data={prescriptionDetail.drugsTaken}
            renderItem={({ item, index }: { item: PrescriptionDetail; index: number }) => (
              <PrescriptionDetailItem
                key={item.id}
                diffDays={prescriptionDetail.diffDays}
                prescriptionDetail={item}
                durRes={prescriptionDetail.durRes}
                onPress={() =>
                  this.props.navigation.navigate("DrugDetail", {
                    id: item.drug.id,
                  })
                }
                // style={prescriptionDetail.drugsTaken.length == index + 1 ?
                //   { borderBottomLeftRadius: 4, borderBottomRightRadius: 4 } : null}
              />
            )}
            ItemSeparatorComponent={() => (
              <View style={{ backgroundColor: "#fff" }}>
                <View style={RX_DETAIL_DIVIDER}></View>
              </View>
            )}
            // onRefresh={pStore.fetchPrescriptions}
            // refreshing={pStore.isLoading}
            keyExtractor={(item) => item.id.toString()}
            // extraData={{ extra: store.prescriptions }}
            // ListEmptyComponent={<EmptyPrescriptionList />}
          />
        ) : null}

        <View style={{ height: 50, borderTopWidth: 1, borderTopColor: color.line }}></View>
      </ScrollView>
    )
  }

  handleEditPress = () => {
    this.bottomSheetRef.current.close()
    const { prescriptionDetail } = this.props.prescriptionStore
    if (this.isSamplePrescription()) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`예시 처방전은 수정이 불가합니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      return
    }
    this.props.navigation.navigate("Details", {
      screen: "MedicationSettings",
      params: {
        prescriptionId: prescriptionDetail.id,
        issueDate: prescriptionDetail.issue_date,
      },
    })
  }

  handleDeletePress = () => {
    this.bottomSheetRef.current.close()
    const pStore = this.props.prescriptionStore
    const { prescriptionDetail } = pStore
    if (pStore.selectedFamilyMemberKey.includes("f") && prescriptionDetail.status === "CF") {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`14세 이상 가족회원의 처방전 삭제는 추후 지원 예정입니다.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      return
    }
    if (
      prescriptionDetail.disease.length > 0 &&
      (prescriptionDetail.disease[0].name_kr.includes("샘플 처방전") ||
        prescriptionDetail.disease[0].name_kr.includes("처방전 예시"))
    ) {
      this.deletePrescription(prescriptionDetail, pStore.selectedFamilyMemberKey)
    } else {
      setTimeout(() => {
        Alert.alert(
          "처방전 삭제",
          `정말로 삭제하시겠습니까?\n삭제된 처방전 내역은 복구할 수 없습니다.`,
          [
            {
              text: "삭제",
              onPress: () =>
                this.deletePrescription(prescriptionDetail, pStore.selectedFamilyMemberKey),
              style: "cancel",
            },
            { text: "취소", onPress: () => null },
          ],
          { cancelable: false },
        )
      }, 300)
    }
  }

  async deletePrescription(prescription: Prescription, memberIndex: string) {
    const result = await api.deletePrescription(prescription.id)
    if (result.kind === "ok") {
      this.props.prescriptionStore.setStatus("pending")
      this.props.prescriptionStore.removePrescription(prescription, memberIndex)
      this.props.prescriptionStore.setStatus("done")
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`처방전을 삭제하였습니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.goBack()
      }, 2000)
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.navigate("Auth")
      }, 2000)
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`처방전 삭제에 실패하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  async transferPrescription(
    prescriptionId: number,
    memberType: FamilyMemberType,
    memberId: number,
    memberName: string,
  ) {
    const { accountStore, prescriptionStore } = this.props

    let params
    let toMemberId
    let toMemberKey
    switch (memberType) {
      case FamilyMemberType.Self:
        params = {
          user: memberId,
          children: null,
        }
        toMemberId = memberId
        toMemberKey = `_u${memberId}`
        break

      case FamilyMemberType.FamilyUser:
        params = {
          user: memberId,
          children: null,
        }
        toMemberId = memberId
        toMemberKey = `_f${memberId}`
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`14세 이상 가족회원 처방전 목록으로의 이동은 추후 지원 예정입니다.`}</Text>
          ),
          duration: 2500,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        return

      case FamilyMemberType.Child:
        params = {
          user: accountStore.user.id,
          children: memberId,
        }
        toMemberId = accountStore.user.id
        toMemberKey = `_c${memberId}`
        break
    }

    const result = await api.updatePrescription(prescriptionId, params)
    if (result.kind === "ok") {
      // 원 소유자의 처방전 목록 갱신
      prescriptionStore.fetchPrescriptions(
        this.transferFromMemberId,
        this.transferFromMemberKey,
        this.transferFromMemberKey.includes("c") ? memberId : undefined,
        true,
      )

      // 새로운 소유자의 처방전 목록 갱신
      prescriptionStore.fetchPrescriptions(
        toMemberId,
        toMemberKey,
        toMemberKey.includes("c") ? memberId : undefined,
        true,
      )
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`처방전이 ${memberName}님의 처방전 목록으로 이동하였습니다.`}</Text>
        ),
        duration: 2500,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`처방전 이동 실패. 나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  ratingDrugAdherenceCompleted(rating: number) {
    this.setState({ drugAdherenceRating: rating })
  }

  getDrugAdherenceRatingLabel(rating: number): string {
    switch (rating) {
      case 1:
        return rating1Text

      case 2:
        return rating2Text

      case 3:
        return rating3Text

      case 4:
        return rating4Text

      case 5:
        return rating5Text

      default:
        return "얼굴 표정을 탭하여 기록하기"
    }
  }

  getSymptomImprovementRatingLabel(rating: number): string {
    switch (rating) {
      case 1:
        return "악화됨"

      case 2:
        return "약간 악화됨"

      case 3:
        return "개선 되지 않음"

      case 4:
        return "약간 개선됨"

      case 5:
        return "개선됨"

      default:
        return "얼굴 표정을 탭하여 기록하기"
    }
  }

  renderDrugSection1(beingTaken: boolean) {
    const store = this.props.prescriptionStore
    let drugSection1
    if (beingTaken) {
      drugSection1 = (
        <View style={DRUG_SECTION_1}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                fontSize: 15,
                fontWeight: "bold",
                color: color.text,
                marginRight: 23,
              }}
            >
              총투약일수 {store.prescriptionDetail ? store.prescriptionDetail.dosingDays : ""}일
            </Text>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-end",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: "bold",
                  letterSpacing: -0.3,
                  color: color.palette.brownGrey,
                }}
              >
                {store.prescriptionDetail
                  ? store.prescriptionDetail.dosingDays -
                    store.prescriptionDetail.diffDays +
                    "일 남음"
                  : null}
              </Text>
            </View>
          </View>
          <View style={PROGRESS_BAR}>
            <Slider
              disabled
              value={
                store.prescriptionDetail
                  ? store.prescriptionDetail.diffDays / store.prescriptionDetail.dosingDays
                  : null
              }
              containerStyle={{ flex: 1, height: 6, margin: 0, padding: 0 }}
              trackStyle={{
                height: 6,
                borderRadius: 10,
                backgroundColor: "rgb(228, 228, 228)",
              }}
              thumbStyle={{ width: 0 }}
              minimumTrackTintColor={color.palette.brightCyan}
            />
          </View>
        </View>
      )
    } else {
      drugSection1 = (
        <View style={DRUG_SECTION_1}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                fontSize: 15,
                fontWeight: "bold",
                color: color.text,
                marginRight: 23,
                letterSpacing: -0.3,
              }}
            >
              총투약일수 {store.prescriptionDetail ? store.prescriptionDetail.dosingDays : null}일
            </Text>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-end",
                alignItems: "center",
              }}
            >
              <Image
                source={require("../assets/images/drugTaken.png")}
                style={{ height: 16, width: 16, marginRight: 4 }}
              />
              <Text
                style={{
                  fontSize: 13,
                  fontWeight: "bold",
                  color: "rgb(168, 168, 168)",
                  letterSpacing: -0.52,
                }}
              >
                복용완료
              </Text>
            </View>
          </View>
          <View style={PROGRESS_BAR}>
            <Slider
              disabled
              value={1}
              style={{ flex: 1, height: 6, margin: 0, padding: 0 }}
              trackStyle={{
                height: 6,
                borderRadius: 10,
                backgroundColor: "rgb(228, 228, 228)",
              }}
              thumbStyle={{ width: 0 }}
              minimumTrackTintColor={"#A8A8A8"}
            />
          </View>
        </View>
      )
    }

    // totalSection = drugSection1 drugSection2
    return drugSection1
  }

  renderDrugSection2() {
    return (
      <View style={DRUG_SECTION_2}>
        <View style={{ flex: 1.3, flexDirection: "row" }}>
          <Text
            style={{
              fontSize: 13,
              fontWeight: "bold",
              color: "rgb(153, 153, 153)",
            }}
          >
            처방약
          </Text>
        </View>
        <View
          style={{
            flex: 3,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View style={{ width: 50 }} />
          <Text
            style={{
              fontSize: 13,
              fontWeight: "bold",
              color: "rgb(153, 153, 153)",
            }}
          >
            1회 투여량
          </Text>
          {/* <Text style={{ fontSize: 13, fontWeight: "bold", color: 'rgb(153, 153, 153)' }}>복용시기</Text> */}
          <Text
            style={{
              fontSize: 13,
              fontWeight: "bold",
              color: "rgb(153, 153, 153)",
            }}
          >
            투여횟수
          </Text>
          <Text
            style={{
              fontSize: 13,
              fontWeight: "bold",
              color: "rgb(153, 153, 153)",
            }}
          >
            총투약일
          </Text>
        </View>
      </View>
    )
  }

  handleMorePress = () => {
    this.bottomSheetRef.current.open()
  }

  async saveMemo() {
    const prescription = this.props.prescriptionStore.prescriptionDetail
    const memo = this.state.memo.trim()
    const params = {
      f_memo: memo,
    }

    const result = await api.updatePrescription(prescription.id, params)
    if (result.kind === "ok") {
      prescription.setMemo(memo)
      this.setState({
        memo: memo,
        memoEdited: false,
      })
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`저장하였습니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (result.kind === "forbidden") {
      if (this.isSamplePrescription()) {
        Toast.show({
          title: <Text style={styles.TOAST_TEXT}>{`예시 처방전은 평가가 불가합니다.`}</Text>,
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
      } else {
        Toast.show({
          title: <Text style={styles.TOAST_TEXT}>{`14세 이상 가족회원은 평가가 불가합니다.`}</Text>,
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
      }
      // TODO: 서버 업데이트 실패 메시지
    }
  }

  async saveRatings(
    drugAdherenceRating: number,
    symptomImprovementRating: number,
    previousDrugAdherenceRating: number,
    previousSymptomImprovementRating: number,
  ) {
    const prescription = this.props.prescriptionStore.prescriptionDetail
    const params = {
      f_value: drugAdherenceRating,
      f_effect: symptomImprovementRating,
    }

    const result = await api.updatePrescription(prescription.id, params)
    if (result.kind === "ok") {
    } else if (
      drugAdherenceRating !== previousDrugAdherenceRating ||
      symptomImprovementRating !== previousSymptomImprovementRating
    ) {
      this.setState({
        drugAdherenceRating: previousDrugAdherenceRating,
        symptomImprovementRating: previousSymptomImprovementRating,
      })
      prescription.setDrugAdherenceRating(previousDrugAdherenceRating)
      prescription.setSymptomImprovementRating(previousSymptomImprovementRating)

      if (result.kind === "forbidden") {
        if (this.isSamplePrescription()) {
          Toast.show({
            title: <Text style={styles.TOAST_TEXT}>{`예시 처방전은 평가가 불가합니다.`}</Text>,
            duration: 2000,
            placement: "top",
            style: styles.TOAST_VIEW,
          })
        } else {
          Toast.show({
            title: (
              <Text style={styles.TOAST_TEXT}>{`14세 이상 가족회원은 평가가 불가합니다.`}</Text>
            ),
            duration: 2000,
            placement: "top",
            style: styles.TOAST_VIEW,
          })
        }
      } else {
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`서버와의 연결이 원활하지 않습니다. 잠시 후 다시 시도해 주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
      }

      // TODO: 서버 업데이트 실패 메시지
    }
  }

  toggleModal = () => {
    this.setState({
      isModalVisible: false,
    })
  }

  renderEmojiByType(rating: number, type: string) {
    const prescription = this.props.prescriptionStore.prescriptionDetail
    const preRating =
      type === "symptom" ? this.state.symptomImprovementRating : this.state.drugAdherenceRating

    return (
      <TouchableOpacity
        style={{
          marginLeft: 11,
          marginRight: 11,
        }}
        onPress={() => {
          /**
           * 증상 나아짐 평가
           */
          if (type === "symptom") {
            if (preRating !== rating) {
              /**
               * 같은 거 한번 더 누르면 취소됨
               */
              this.setState({ symptomImprovementRating: rating })
              prescription.setSymptomImprovementRating(rating)
            } else {
              rating = 0
              this.setState({ symptomImprovementRating: 0 })
              prescription.setSymptomImprovementRating(0)
            }
            this.saveRatings(
              this.state.drugAdherenceRating,
              rating,
              this.state.drugAdherenceRating,
              preRating,
            )
            /** 약은 다 먹었는지 평가 */
          } else {
            if (preRating !== rating) {
              this.setState({ drugAdherenceRating: rating })
              prescription.setDrugAdherenceRating(rating)
            } else {
              rating = 0
              this.setState({ drugAdherenceRating: 0 })
              prescription.setDrugAdherenceRating(0)
            }
            this.saveRatings(
              rating,
              this.state.symptomImprovementRating,
              preRating,
              this.state.symptomImprovementRating,
            )
          }
        }}
      >
        {preRating === rating ? (
          <Image source={iconCheckedEmojiArray[rating - 1]} style={{ height: 42, width: 42 }} />
        ) : (
          <Image source={iconEmojiArray[rating - 1]} style={{ height: 42, width: 42 }} />
        )}
      </TouchableOpacity>
    )
  }
}
