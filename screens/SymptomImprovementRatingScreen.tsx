import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content, ScrollView, Toast } from "native-base"
import React from "react"
import {
  Dimensions,
  Image,
  ImageStyle,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import Modal from "react-native-modal"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import CloseButton from "../components/CloseButton"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { formatDateFromString } from "../lib/DateUtil"
import {
  DetailsStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  paddingHorizontal: 20,
  paddingTop: 20,
}
const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 34,
  paddingHorizontal: 6,
  paddingBottom: 24,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 34,
  lineHeight: 40,
  letterSpacing: -1.36,
  color: color.text,
  textAlign: "center",
}
const TITLE_DOT: TextStyle = {
  fontFamily: typography.primary,
  fontWeight: "bold",
  fontSize: 34,
  lineHeight: 40,
  letterSpacing: -1.36,
  color: "rgb(255, 132, 26)",
}
const BOTTOM_BUTTON: ViewStyle = {
  flex: 1,
  flexDirection: "column",
  justifyContent: "flex-end",
  marginBottom: 30,
}
const MODAL_TITLE = {
  fontFamily: typography.primary,
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
  paddingBottom: 15,
}
const MODAL_DESCRIPTION = {
  fontSize: 15,
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.palette.black,
}
const MODAL_CONTAINER = {
  borderRadius: 10,
  backgroundColor: "rgb(239, 239, 239)",
  padding: 20,
}

const starImageSource = require("../assets/images/star.png")
const starGreyImageSource = require("../assets/images/starGray.png")

const INFO_MARK: ImageStyle = {
  width: 14,
  height: 14,
  marginLeft: 5,
  resizeMode: "contain",
}
const infoSource = require("../assets/images/info.png")
const hitSlop20 = { top: 20, left: 20, right: 20, bottom: 20 }

const deviceWidth = Dimensions.get("window").width
const deviceHeight = Dimensions.get("window").height
const rating5Text = "개선됨"
const rating4Text = "약간 개선됨"
const rating3Text = "개선 되지 않음"
const rating2Text = "약간 악화됨"
const rating1Text = "악화됨"

const RATING_TEXT_GRAY_BOX: ViewStyle = {
  alignSelf: "stretch",
  height: 40,
  alignItems: "center",
  justifyContent: "center",
  borderColor: "rgb(217,217,217)",
  borderWidth: 1,
  borderRadius: 20,
  marginTop: 15,
}

const RATING_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontWeight: "bold",
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
}
const RATING_AVAILABLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 24,
  letterSpacing: -0.8,
  color: "rgb(153, 153, 153)",
}

const iconEmojiArray = [
  require("../assets/images/iconEmojiSoBad.png"),
  require("../assets/images/iconEmojiBad.png"),
  require("../assets/images/iconEmojiNormal.png"),
  require("../assets/images/iconEmojiNice.png"),
  require("../assets/images/iconEmojiSoNice.png"),
]
const iconCheckedEmojiArray = [
  require("../assets/images/iconEmojiSoBadChecked.png"),
  require("../assets/images/iconEmojiBadChecked.png"),
  require("../assets/images/iconEmojiNormalChecked.png"),
  require("../assets/images/iconEmojiNiceChecked.png"),
  require("../assets/images/iconEmojiSoNiceChecked.png"),
]

type SymptomImprovementRatingRouteProp = RouteProp<HomeStackParamList, "SymptomImprovementRating">

type SymptomImprovementRatingNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "SymptomImprovementRating">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: SymptomImprovementRatingNavigationProp
  route: SymptomImprovementRatingRouteProp
}

interface State {
  starCount: number
  isModalVisible: boolean
}

const SMALL_TEXT = {
  fontFamily: typography.primary,
  fontSize: 13,
  lineHeight: 24,
  letterSpacing: -0.26,
  color: color.palette.brownGrey,
}
@inject("accountStore", "prescriptionStore")
@observer
export default class SymptomImprovementRatingScreen extends React.Component<Props, State> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route
    return {
      title: "[" + params.repTitle + ", " + formatDateFromString(params.issueDate) + "]",
      headerLeft: null,
    }
  }

  constructor(props: Props) {
    super(props)
    const rating = this.getRating(this.props.route.params)
    this.state = {
      starCount: rating || 2,
      isModalVisible: false,
    }
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  getPrescriptionId(params: any) {
    return params.prescriptionId
  }

  getRating(params: any) {
    return params.rating
  }

  renderEmojiByType(rating: number) {
    return (
      <TouchableOpacity
        style={{
          marginLeft: 11,
          marginRight: 11,
        }}
        onPress={() => {
          this.ratingCompleted(rating)
        }}
      >
        {this.state.starCount == rating ? (
          <Image source={iconCheckedEmojiArray[rating - 1]} style={{ height: 42, width: 42 }} />
        ) : (
          <Image source={iconEmojiArray[rating - 1]} style={{ height: 42, width: 42 }} />
        )}
      </TouchableOpacity>
    )
  }

  getDrugSymptomRatingLabel(rating: number): string {
    switch (rating) {
      case 1:
        return rating1Text

      case 2:
        return rating2Text

      case 3:
        return rating3Text

      case 4:
        return rating4Text

      case 5:
        return rating5Text

      default:
        return "얼굴 표정을 탭하여 기록하기"
    }
  }

  render() {
    /** 이모티콘  */
    const symptomEmojiList: Array<any> = []

    for (let index = 1; index <= 5; index++) {
      symptomEmojiList.push(this.renderEmojiByType(index))
    }

    const rating = this.getRating(this.props.route.params)
    return (
      <View style={BACKGROUND}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={TITLE}>
            <Text style={TITLE_TEXT}>
              증상은 좀 나아지셨나요
              <Text style={TITLE_DOT}>?</Text>
            </Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>{symptomEmojiList}</View>
            <View style={RATING_TEXT_GRAY_BOX}>
              <Text style={this.state.starCount == 0 ? RATING_AVAILABLE_TEXT : RATING_TEXT}>
                {this.getDrugSymptomRatingLabel(this.state.starCount)}
              </Text>
            </View>
            <View
              style={{
                alignSelf: "stretch",
                borderWidth: 1,
                borderRadius: 10,
                borderColor: color.palette.grey4,
                marginTop: 20,
                marginBottom: 23,
                paddingTop: 15,
                paddingBottom: 18,
              }}
            >
              {this.renderRatingInfo(rating5Text, 5)}
              {this.renderRatingInfo(rating4Text, 4)}
              {this.renderRatingInfo(rating3Text, 3)}
              {this.renderRatingInfo(rating2Text, 2)}
              {this.renderRatingInfo(rating1Text, 1)}
            </View>
          </View>
          <View
            style={{
              height: 22,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 18,
            }}
          >
            <Text style={SMALL_TEXT}>* 증상개선정도 기록은 왜 남기나요?</Text>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  isModalVisible: true,
                })
              }
              hitSlop={hitSlop20}
            >
              <Image source={infoSource} style={INFO_MARK} />
            </TouchableOpacity>
          </View>

          <View style={BOTTOM_BUTTON}>
            <Button
              block
              rounded
              onPress={() => this.cancel()}
              style={{ backgroundColor: "rgb(217, 217, 217)", marginBottom: 8 }}
            >
              <Text style={{ fontWeight: "bold", fontSize: 17, color: color.text }}>
                {rating ? "취소" : "다음에 기록"}
              </Text>
            </Button>
            <Button block rounded onPress={() => this.complete()}>
              <Text style={{ fontWeight: "bold", fontSize: 17 }}>완료</Text>
            </Button>
          </View>

          <Modal
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            isVisible={this.state.isModalVisible}
            animationOut={"fadeOutDown"}
            onBackdropPress={this.toggleModal}
          >
            <View>
              <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
              <View style={{ alignItems: "flex-end" }}>
                <CloseButton
                  imageStyle={{ tintColor: "rgb(255, 255, 255)" }}
                  onPress={() => this.toggleModal()}
                  style={{
                    width: 15.7,
                    height: 15.7,
                    marginRight: 0,
                    paddingTop: 0,
                    marginBottom: 12.4,
                  }}
                />
              </View>
              <View style={MODAL_CONTAINER}>
                <Text style={MODAL_TITLE}>증상개선정도 기록은 왜 필요한가요?</Text>
                <Text style={MODAL_DESCRIPTION}>
                  대부분의 의료진은 질병에 따른 경험적이고 보편적인 치료법으로 환자의 치료를
                  접근하고 있습니다. 치료 성공률이 가장 높다고 판단되는 치료법을 의료진 개별 근거에
                  따라 선택하여 여러 환자들에게 적용을 합니다. 하지만 같은 질병을 가지고 있다고
                  하더라도 환자 개개인의 차이로 치료효과는 달라질 수 있습니다. 현재 복용 중인 약제로
                  인한 증상개선정도 기록은 나에게 맞는 치료법을 찾는데 큰 도움이 될 수 있습니다.
                </Text>
                <View
                  style={{
                    borderWidth: 1,
                    borderRadius: 10,
                    borderColor: color.palette.grey2,
                    marginTop: 20,
                    marginBottom: 20,
                  }}
                >
                  {this.renderRatingInfo(rating5Text, 5, color.palette.grey6)}
                  {this.renderRatingInfo(rating4Text, 4, color.palette.grey6)}
                  {this.renderRatingInfo(rating3Text, 3, color.palette.grey6)}
                  {this.renderRatingInfo(rating2Text, 2, color.palette.grey6)}
                  {this.renderRatingInfo(rating1Text, 1, color.palette.grey6)}
                </View>
              </View>
            </View>
          </Modal>
        </ScrollView>
      </View>
    )
  }

  ratingCompleted(rating: number) {
    this.setState({
      starCount: rating,
    })
  }

  renderRatingInfo(text: string, ratingCount: number, emptyStarColor?: string) {
    return (
      <View
        style={{
          height: 34,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start",
          paddingLeft: 109,
        }}
      >
        <Image source={iconCheckedEmojiArray[ratingCount - 1]} style={{ height: 26, width: 26 }} />
        <Text
          numberOfLines={2}
          style={{
            fontFamily: typography.primary,
            fontSize: 16,
            lineHeight: 20,
            letterSpacing: -0.64,
            textAlign: "left",
            marginLeft: 10,
          }}
        >
          {text}
        </Text>
      </View>
    )
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  cancel() {
    this.props.navigation.pop()
  }

  async complete() {
    const { navigation, route } = this.props
    const prescription = this.props.prescriptionStore.prescriptionDetail
    const pStore = this.props.prescriptionStore
    const params = {
      f_effect: this.state.starCount,
    }

    const prescriptionId = this.getPrescriptionId(route.params)
    if (!prescriptionId) {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        navigation.popToTop()
      }, 2000)
      return
    }

    const result = await api.updatePrescription(prescriptionId, params)
    if (result.kind === "ok") {
      if (prescription && prescriptionId === prescription.id) {
        prescription.setSymptomImprovementRating(this.state.starCount)
      } else if (prescriptionId) {
        const found = pStore.findPrescription(prescriptionId)
        if (found) {
          found.setSymptomImprovementRating(this.state.starCount)
        }
      }
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`저장하였습니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })

      // pStore.removeDoingEndedPrescription(prescriptionId)
      navigation.popToTop()
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        navigation.popToTop()
      }, 2000)
    }
  }
}
