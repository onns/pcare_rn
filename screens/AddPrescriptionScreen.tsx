import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { observer } from "mobx-react-lite"
import { Spinner, Toast } from "native-base"
import React, { useEffect, useLayoutEffect, useRef, useState } from "react"
import {
  Dimensions,
  FlatList,
  Image,
  Platform,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import appsFlyer from "react-native-appsflyer"
import FastImage from "react-native-fast-image"
import { ScrollView } from "react-native-gesture-handler"
import Modal from "react-native-modal"
import RBSheet from "react-native-raw-bottom-sheet"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { withNavigationFocus } from "@react-navigation/compat"
import { CompositeNavigationProp, RouteProp, useFocusEffect } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import SvgCalendar from "../assets/images/calendar.svg"
import SvgDisease from "../assets/images/disease.svg"
import SvgDrugEmptyImage from "../assets/images/drugEmptyImage.svg"
import SvgHospital from "../assets/images/hospital.svg"
import SvgMedicine from "../assets/images/medicine.svg"
import TouchableItem from "../components/button/TouchableItem"
import CloseButton from "../components/CloseButton"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { getText } from "../components/TimeSlotRow"
import { formatTime } from "../lib/DateUtil"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { env } from "../stores"
import { Disease } from "../stores/Disease"
import { FamilyMember } from "../stores/FamilyMember"
import { PrescriptionModel } from "../stores/Prescription"
import { Prescription, PrescriptionDetail } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"
import { SearchDrugMode } from "./SearchDrugScreen"

type MedicationSettingsScreenRouteProp = RouteProp<DetailsStackParamList, "AddPrescription">

type MedicationSettingsScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "AddPrescription">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface AddPrescriptionScreenProps {
  navigation: MedicationSettingsScreenNavigationProp
  route: MedicationSettingsScreenRouteProp
}

export enum FamilyMemberType {
  Self,
  FamilyUser,
  Child,
}

const ROOT: ViewStyle = {
  backgroundColor: color.background,
}
const TOUCHABLE_ITEM: ViewStyle = {
  backgroundColor: "#fff",
  paddingTop: 24,
  paddingLeft: 16,
  paddingRight: 16,
  paddingBottom: 24,
}
const TOUCHABLE_MIDDLE_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
}
const TOUCHABLE_LAST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
  paddingBottom: 24,
}
const TOUCHABLE_FIRST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
}
const TOUCHABLE_FIRST_LAST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
  paddingVertical: 24,
}
const DISEASE_DETAIL_NAME: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
  marginTop: 6,
}
const RIGHT_ARROW: ViewStyle = {
  paddingRight: 8,
  paddingBottom: 0,
  alignItems: "flex-end",
  justifyContent: "center",
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const DRUG_SECTION_HEADER: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingTop: 24,
  paddingBottom: 8,
  paddingHorizontal: 16,
}
const TITLE2_ORANGE: TextStyle = { ...typography.title2, color: color.primary }
const SECTION1 = {
  backgroundColor: color.palette.white,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
  marginBottom: 8,
  paddingBottom: 24,
}
const SECTION2 = {
  backgroundColor: color.palette.white,
  borderBottomColor: color.palette.pale,
  borderTopColor: color.palette.pale,
  borderBottomWidth: 1,
  borderTopWidth: 1,
  marginBottom: 8,
  paddingBottom: 24,
}
const SECTION3 = {
  backgroundColor: color.palette.white,
  paddingRight: 8,
  borderTopColor: color.palette.pale,
  borderTopWidth: 1,
  paddingBottom: 24,
  marginBottom: 41,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
}
const ALARM_BOX: ViewStyle = {
  backgroundColor: color.palette.gray5,
  borderRadius: 1,
  borderWidth: 1,
  borderColor: color.palette.pale,
  paddingHorizontal: 7,
  paddingTop: 2,
  paddingBottom: 3,
  marginLeft: 8,
  marginBottom: 8,
}
const ALARM_TIME: TextStyle = { ...typography.sub2, opacity: 0.7 }
const DRUG_BUTTON: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingLeft: 16,
  paddingTop: 24,
  paddingRight: 8,
  marginBottom: 4,
}
const DRUG_LINE = {
  backgroundColor: color.palette.pale,
  height: 1,
  marginTop: 16,
  marginLeft: 16,
  marginRight: 16,
}
const DISEASE_DIVIDER = {
  marginLeft: 16,
  marginRight: 16,
  height: 1,
  backgroundColor: color.palette.gray5,
}
const DRUG_IMAGE: ViewStyle = {
  width: 84,
  height: 48,
  borderRadius: 5,
}
const REMOVE_BUTTON: ViewStyle = {
  height: 32,
  borderRadius: 16,
  backgroundColor: color.palette.gray5,
  paddingVertical: 6,
  paddingHorizontal: 16,
  justifyContent: "center",
  alignItems: "center",
}
const REMOVE_BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.copper,
}
const DOCTOR_NAME = {
  ...typography.sub2,
  marginTop: 4,
  opacity: 1,
}
const DOCTOR = {
  ...typography.sub2,
  marginTop: 4,
  opacity: 0.5,
}
const HOSPITAL_DESC = {
  ...typography.body,
  marginTop: 8,
  color: color.palette.grey70,
}
const HOSPITAL_ITEM = { paddingTop: 24, paddingHorizontal: 16 }

const DRUG_NAME = { flex: 1, ...typography.title3, paddingRight: 18 }
const DRUG_DESC = {
  ...HOSPITAL_DESC,
  marginBottom: -16,
}
const DRUG_BUTTON_RIGHT = {
  flex: 1,
  marginLeft: 8,
  marginBottom: 4,
}
const MEDICATION_PERIOD = {
  ...typography.sub2,
  color: color.palette.grey50,
  marginLeft: 4,
  marginBottom: 4,
}
const DOSING_DAYS = {
  ...typography.sub2,
  color: color.palette.black,
}
const DOSE_INFO_TEXT = { ...typography.sub2, paddingLeft: 8, marginBottom: 8 }
const FULL = { flex: 1 }

const SAVE = { ...typography.button, fontFamily: typography.primary, color: "#2F3233" }

const MEMBER_BUTTON: ViewStyle = {
  height: 50,
  alignItems: "center",
}
const NAME: TextStyle = {
  fontSize: 20,
  letterSpacing: -0.8,
}

const MODAL_CONTENT: ViewStyle = {
  backgroundColor: "white",
  margin: 42.5,
  padding: 16.9,
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 20,
  borderColor: "rgba(0, 0, 0, 0.1)",
}
const WHOSE_PRESCRIPTION_TEXT: TextStyle = {
  fontSize: 15,
  letterSpacing: -0.6,
  marginBottom: 50,
}

/** [처방전 찍기]에서 [직접 입력]을 선택했을 때의 화면 */
const AddPrescriptionScreen: React.FunctionComponent<AddPrescriptionScreenProps> = observer(
  (props) => {
    const { accountStore, prescriptionStore, searchDiseaseStore, searchHospitalStore } = useStores()
    const { navigation, route } = props
    // const prescriptionId = route.params.prescriptionId
    // const issueDate = route.params.issueDate
    // const prescription: Prescription = prescriptionStore.prescriptionDetail

    const [prescription] = useState<Prescription>(
      PrescriptionModel.create(
        {
          id: -1,
          status: "CF",
        },
        env,
      ),
    )

    const deviceWidth = Dimensions.get("window").width
    const deviceHeight =
      Platform.OS === "ios"
        ? Dimensions.get("window").height
        : require("react-native-extra-dimensions-android").get("REAL_WINDOW_HEIGHT")

    const [popupVisible, setPopupVisible] = useState(false)
    const [popupTitle, setPopupTitle] = useState("")
    const [popupMessage, setPopupMessage] = useState("")
    const [popupButton1, setPopupButton1] = useState(<View />)
    const [popupButton2, setPopupButton2] = useState(<View />)
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [
      selectedPrescriptionDetail,
      setSelectedPrescriptionDetail,
    ] = useState<PrescriptionDetail>()
    const [selectedPrescriptionDetailIdx, setSelectedPrescriptionDetailIdx] = useState(-1)
    const bottomSheetRef = useRef(null)

    const savePrescriptionDetails = async (pid: number) => {
      for (const prescriptionDetail of prescription.prescription_details) {
        /**
         * 요청할 땐 dosing_days_num 와 end_at 값 중 하나만 전달하면 되나, 캘린더에서 종료일을 선택한 경우 dosing_days_num 값에 반영되지 않음.
         * 따라서 end_at 값을 쓸 것.
         */
        const params = {
          drug_id: prescriptionDetail.drug.id,
          prescription: pid,
          dose: prescriptionDetail.dose,
          doses_num: prescriptionDetail.doses_num,
          per_days: prescriptionDetail.per_days,
          start_at: prescriptionDetail.start_at.toJSON(),
          end_at: prescriptionDetail.end_at.toJSON(),
          timeslots: prescriptionDetail.timeSlotIds,
        }
        const result = await api.postPrescriptionDetail(params)
        if (result.kind !== "ok") {
          console.tron.log("< AddPrescriptionScreen > savePrescriptionDetails, error: ", result)
          Toast.show({
            title: (
              <Text
                style={styles.TOAST_TEXT}
              >{`서버와 통신 중 오류가 발생하였습니다. \n저장되지 않은 약은 '처방 내역 수정'에서 다시 추가해주세요.`}</Text>
            ),
            duration: 2000,
            placement: "top",
            style: styles.TOAST_VIEW,
          })
        }
      }
    }

    const renderSelectMemberModalContent = () => (
      <View style={MODAL_CONTENT}>
        <View style={{ flex: 1, flexDirection: "row", height: 20 }}>
          <View style={{ flex: 1 }}></View>
          <TouchableOpacity
            onPress={cancel}
            style={{ width: 24, height: 24, justifyContent: "flex-start", alignItems: "flex-end" }}
            hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}
          >
            <Image
              source={require("../assets/images/close.png")}
              style={{
                width: 15.7,
                height: 15.7,
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={{ padding: 5, marginTop: 42 }}>
          <Text style={WHOSE_PRESCRIPTION_TEXT}>누구의 처방전인가요?</Text>

          <ScrollView style={{ maxHeight: 250 }}>
            <TouchableOpacity
              onPress={() => {
                requestToEnterPrescData(accountStore.user.id, FamilyMemberType.Self)
              }}
              // {...rest}
              style={MEMBER_BUTTON}
            >
              <Text style={NAME}>{accountStore.user.nickname}</Text>
            </TouchableOpacity>
            {accountStore.family.map((member: FamilyMember) => {
              return (
                <TouchableOpacity
                  key={member.id}
                  onPress={() =>
                    requestToEnterPrescData(
                      member.isOver14 ? member.family_user! : member.id,
                      member.isOver14 ? FamilyMemberType.FamilyUser : FamilyMemberType.Child,
                    )
                  }
                  style={MEMBER_BUTTON}
                >
                  <Text style={NAME}>{member.name}</Text>
                </TouchableOpacity>
              )
            })}
          </ScrollView>
        </View>
        <View style={{ height: 40 }}></View>
      </View>
    )

    const cancel = () => {
      if (Platform.OS === "ios") {
        setIsModalVisible(false)
      } else {
        setPopupVisible(true)
        setPopupTitle("처방전 입력 취소")
        setPopupMessage("처방전 입력을 취소하시겠습니까?")
        setPopupButton1(
          getPopupButton(
            () => {
              setPopupVisible(false)
            },
            "아니오",
            "cancel",
          ),
        )
        setPopupButton2(
          getPopupButton(() => {
            setPopupVisible(false)
            _toggleModal()
          }, "네"),
        )
      }
    }

    const _toggleModal = () => setIsModalVisible(!isModalVisible)

    const requestToEnterPrescData = async (id: number, memberType: FamilyMemberType) => {
      try {
        fetchMemberPrescriptions(id, memberType)
        savePrescription(id, memberType)
      } catch (err) {
        console.tron.log("< AddPrescriptionScreen > requestToEnterPrescData, error: ", err)
      }
    }

    const fetchMemberPrescriptions = (id: number, memberType: FamilyMemberType) => {
      let familyMemberKey
      if (memberType === FamilyMemberType.Self) {
        familyMemberKey = "_u" + id
      } else if (memberType === FamilyMemberType.Child) {
        familyMemberKey = "_c" + id
      } else if (memberType === FamilyMemberType.FamilyUser) {
        familyMemberKey = "_f" + id
      }
      prescriptionStore.fetchPrescriptions(
        memberType === FamilyMemberType.FamilyUser ? id : accountStore.user.id,
        familyMemberKey,
        memberType === FamilyMemberType.Child ? id : undefined,
      )
    }

    const newPrescriptionPopup = () => {
      setTimeout(() => {
        setPopupVisible(true)
        setPopupTitle("새로운 처방이 등록되었습니다.")
        setPopupMessage("등록하신 처방 내역은 의료기록에서 확인하실 수 있습니다.")
        setPopupButton1(getPopupButton(navigation.goBack, "확인"))
        setPopupButton2(null)
      }, 500)
    }

    const savePrescription = async (id: number, memberType: FamilyMemberType) => {
      try {
        const { environment } = prescriptionStore

        const params = {
          disease: prescription.disease.map((item) => item.id),
          subdiseases: prescription.subdiseases.map((item) => item.id),
          // TODO: 가족회원의 처방전을 추가할 때 아래 두 필드를 이용하여 저장함.(14세미만은 children, 14세이상은 user)
          // user: prescipription.user,
          // children: types.maybeNull(types.number),

          user: null,
          children: null,
          issue_date: prescription.minStartDate,
          confirm_date: DateTime.local().toJSON(),
          status: "CF",
          cf_user: "직접입력",
        }
        if (prescription.hospital) {
          params.hospital = prescription.hospital.id
        }

        if (memberType == FamilyMemberType.FamilyUser) {
          params.user = id
          delete params.children
        } else if (memberType == FamilyMemberType.Child) {
          params.children = id
          delete params.user
        } else {
          delete params.children
          delete params.user
        }

        const result = await api.postPrescription(params)
        _toggleModal()

        if (result.kind === "ok") {
          firebase.analytics().logEvent("add_self_prescription")
          appsFlyer.logEvent("af_add_self_prescription", {})
          newPrescriptionPopup()
          savePrescriptionDetails(result.data.id) // 약제 추가는 별도의 api 로 처리해달라고 안내받음.

          amplitude
            .getInstance()
            .logEvent("prescription_added_by_self_input", { ...prescription, id: result.data.id })

          setTimeout(() => {
            // addPrescription 호출하면 listener 를 통해 fetchDrugsToTake, fetchPrescriptions 호출 됨.
            // 서버에서 taken 및 end_at 데이터 처리 시까지 시간 필요하여 호출을 지연시킴.
            const { selectedMemberKey } = accountStore
            prescriptionStore.addPrescription(result.data, selectedMemberKey)
          }, 500)
        } else {
          Toast.show({
            title: (
              <Text
                style={styles.TOAST_TEXT}
              >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
            ),
            duration: 2000,
            placement: "top",
            style: styles.TOAST_VIEW,
          })
        }
      } catch (err) {
        console.tron.log("< AddPrescriptionScreen > savePrescription, error: ", err)
      }
    }

    useLayoutEffect(() => {
      navigation.setOptions({
        headerLeft: () => <CloseButton onPress={navigation.goBack} style={{ marginLeft: 24 }} />,
        // eslint-disable-next-line react/display-name
        headerRight: () => (
          <TouchableOpacity
            onPress={() => {
              prescription?.prescription_details.length !== 0
                ? _toggleModal()
                : Toast.show({
                    title: <Text style={styles.TOAST_TEXT}>{`복용하실 약을 추가 해주세요.`}</Text>,
                    duration: 2000,
                    placement: "top",
                    style: styles.TOAST_VIEW,
                  })
            }}
            style={{ marginRight: 24 }}
          >
            <Text style={SAVE}>저장</Text>
          </TouchableOpacity>
        ),
        headerTitle: "직접 입력",
        headerStyle: { elevation: 1 },
      })
    }, [navigation])

    useFocusEffect(
      React.useCallback(() => {
        firebase.analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        })
        amplitude.getInstance().logEvent("screen_view", { screen: route.name })

        // selectedDisease 가 존재하면 해당 disease 추가
        const { environment, selectedDisease } = searchDiseaseStore
        const { selectedHospital } = searchHospitalStore
        const { newPrescriptionDetail } = prescriptionStore
        if (selectedDisease) {
          prescription.addDisease(selectedDisease)
          searchDiseaseStore.initiate()
        } else if (selectedHospital) {
          prescription.setHospital(selectedHospital)
          searchHospitalStore.initiate()
        } else if (newPrescriptionDetail) {
          prescription.addPrescriptionDetail(newPrescriptionDetail)
          prescriptionStore.setNewPrescriptionDetail(undefined)
        }
        // else if (selectedDrug) {
        //   prescription.addPres(selectedHospital)
        // }
      }, []),
    )

    // const editMedicationDetail = () => {
    //   bottomSheetRef.current.close()
    //   navigation.navigate("EditMedicationDetail", {
    //     mode: EditMedicationDetailMode.
    //     // prescriptionDetail: selectedPrescriptionDetail,
    //     prescriptionDetailIdx: selectedPrescriptionDetailIdx,
    //   })
    // }

    const removeDisease = async (selectedDisease: Disease, isSubDisease?: boolean) => {
      prescription.removeDisease(selectedDisease, isSubDisease)
    }

    const removeHospital = async () => {
      prescription.setHospital(null)
    }
    const removePrescriptionDetail = async () => {
      prescription.removePrescriptionDetail(selectedPrescriptionDetail)
    }

    const renderDiseaseDivider = () => <View style={DISEASE_DIVIDER} />

    const renderBottomSheet = () => {
      return (
        <View>
          <View style={styles.BOTTOM_SHEET_HEADER}>
            <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>원하시는 항목을 선택해 주세요.</Text>
          </View>
          <View>
            {/* <TouchableHighlight
              style={styles.BOTTOM_SHEET_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={editMedicationDetail}
            >
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>수정</Text>
            </TouchableHighlight> */}
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_LAST_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={removePrescriptionDetail}
            >
              <Text style={[styles.BOTTOM_SHEET_BUTTON_TEXT, { color: color.palette.copper }]}>
                삭제
              </Text>
            </TouchableHighlight>
          </View>
          <View style={{ backgroundColor: color.palette.white }}>
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_CANCEL_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={() => bottomSheetRef.current.close()}
            >
              <Text style={styles.BOTTOM_SHEET_CANCEL_BUTTON_TEXT}>취소</Text>
            </TouchableHighlight>
          </View>
        </View>
      )
    }

    if (prescriptionStore.status == "error") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`서버로부터 데이터를 가져오지 못 하였습니다.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (prescriptionStore.status == "pending") {
      return <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
    }

    return (
      <Screen style={ROOT} preset="fixed" unsafe>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView>
          <View style={SECTION1}>
            {prescription &&
              prescription.disease.map((disease, index) => (
                <View key={disease.id}>
                  <View
                    // style={prescriptionDetail!.disease.length == (index + 1) && prescriptionDetail!.subdiseases.length == 0 ? TOUCHABLE_ITEM : TOUCHABLE_FIRST_ITEM}
                    style={
                      index != 0
                        ? prescription!.disease.length == index + 1
                          ? prescription.subdiseases.length == 0
                            ? TOUCHABLE_LAST_ITEM
                            : TOUCHABLE_MIDDLE_ITEM
                          : TOUCHABLE_MIDDLE_ITEM
                        : prescription.subdiseases.length == 0
                        ? TOUCHABLE_FIRST_LAST_ITEM
                        : TOUCHABLE_FIRST_ITEM
                    }
                  >
                    <View style={ROW}>
                      <SvgDisease
                        width={40}
                        height={40}
                        style={{ marginLeft: -4, marginRight: 8 }}
                      />
                      <View style={{ flex: 7 }}>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                          {disease.feature || disease.rep_title}
                        </Text>
                        <Text style={DISEASE_DETAIL_NAME}>{disease.code + " " + disease.name}</Text>
                      </View>
                      <View style={RIGHT_ARROW}>
                        <TouchableItem style={REMOVE_BUTTON} onPress={() => removeDisease(disease)}>
                          <Text style={REMOVE_BUTTON_TEXT}>삭제</Text>
                        </TouchableItem>
                      </View>
                    </View>
                  </View>
                  {prescription!.disease.length == index + 1
                    ? prescription.subdiseases.length == 0
                      ? null
                      : renderDiseaseDivider()
                    : renderDiseaseDivider()}
                </View>
              ))}
            {prescription &&
              prescription.subdiseases.map((disease: Disease, index: number) => (
                <View key={disease.id}>
                  <View
                    style={TOUCHABLE_MIDDLE_ITEM}
                    // onPress={() => {}}
                  >
                    <View style={ROW}>
                      <SvgDisease
                        width={40}
                        height={40}
                        style={{ marginLeft: -4, marginRight: 8 }}
                      />
                      <View style={{ flex: 7 }}>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                          {disease.feature || disease.rep_title}
                        </Text>
                        <Text style={DISEASE_DETAIL_NAME}>
                          {disease.code} {disease.name}
                        </Text>
                      </View>
                      <View style={RIGHT_ARROW}>
                        <TouchableItem
                          style={REMOVE_BUTTON}
                          onPress={() => removeDisease(disease, true)}
                        >
                          <Text style={REMOVE_BUTTON_TEXT}>삭제</Text>
                        </TouchableItem>
                      </View>
                    </View>
                  </View>
                  {prescription!.subdiseases.length == index + 1 ? null : renderDiseaseDivider()}
                </View>
              ))}
            {prescription?.disease.length == 0 && prescription?.subdiseases.length == 0 ? (
              <View>
                <View style={{ ...HOSPITAL_ITEM, paddingBottom: 8 }}>
                  <Text style={typography.title2}>질병</Text>
                  <Text style={HOSPITAL_DESC}>어떤 질병으로 약을 복용하시나요?</Text>
                </View>
              </View>
            ) : null}
            <TouchableItem
              style={styles.LARGE_RADIUS_BUTTON}
              onPress={() => navigation.navigate("SearchDisease")}
            >
              <Text style={styles.LARGE_RADIUS_BUTTON_TEXT}>
                {prescription?.disease.length == 0 && prescription?.subdiseases.length == 0
                  ? `질병 입력`
                  : `질병 추가`}
              </Text>
            </TouchableItem>
          </View>
          {prescription?.hospital ? (
            <View style={SECTION2}>
              <View style={HOSPITAL_ITEM}>
                <View style={ROW}>
                  <SvgHospital width={40} height={40} style={{ marginLeft: -4, marginRight: 8 }} />
                  <View style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                      <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                        {prescription?.hospital?.name ?? ""}
                      </Text>
                    </View>
                    {prescription?.doctor?.name ? (
                      <View style={ROW}>
                        <Text style={DOCTOR}>담당의 </Text>
                        <Text style={DOCTOR_NAME}>
                          {prescription.doctor.name}
                          {prescription.doctor.narrow_major
                            ? `(${prescription.doctor.narrow_major ?? ""})`
                            : ""}
                        </Text>
                      </View>
                    ) : (
                      <Text style={DOCTOR}>의료진 정보가 없습니다.</Text>
                    )}
                  </View>
                  <View style={RIGHT_ARROW}>
                    <TouchableItem style={REMOVE_BUTTON} onPress={removeHospital}>
                      <Text style={REMOVE_BUTTON_TEXT}>삭제</Text>
                    </TouchableItem>
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={SECTION2}>
              <View style={{ ...HOSPITAL_ITEM, paddingBottom: 8 }}>
                <Text style={typography.title2}>병원</Text>
                <Text style={HOSPITAL_DESC}>처방 받으신 병원을 입력해 주세요.</Text>
              </View>
              <TouchableItem
                style={styles.LARGE_RADIUS_BUTTON}
                onPress={() => navigation.navigate("SearchHospital")}
              >
                <Text style={styles.LARGE_RADIUS_BUTTON_TEXT}>병원 입력</Text>
              </TouchableItem>
            </View>
          )}

          <View style={SECTION3}>
            <View style={DRUG_SECTION_HEADER}>
              <Text style={typography.title2}>
                약{" "}
                {prescription.prescription_details.length > 0 && (
                  <Text style={TITLE2_ORANGE}>{prescription.prescription_details.length}</Text>
                )}
              </Text>
              {prescription.prescription_details.length == 0 && (
                <Text style={DRUG_DESC}>복용하실 약을 입력해 주세요.</Text>
              )}
            </View>
            {prescription.prescription_details.length > 0 ? (
              <FlatList
                data={prescription.prescription_details.slice()}
                renderItem={({ item, index }: { item: PrescriptionDetail; index: number }) => (
                  <View>
                    <TouchableOpacity
                      onPress={() => {
                        setSelectedPrescriptionDetail(item)
                        setSelectedPrescriptionDetailIdx(index)
                        bottomSheetRef.current.open()
                      }}
                      style={DRUG_BUTTON}
                    >
                      {item.drug.image_url ? (
                        <FastImage source={{ uri: item.drug.image_url }} style={DRUG_IMAGE} />
                      ) : (
                        <SvgDrugEmptyImage
                          width={84}
                          height={48}
                          style={{ alignSelf: "flex-start" }}
                        />
                      )}
                      <View style={DRUG_BUTTON_RIGHT}>
                        <View style={ROW}>
                          <Text style={DRUG_NAME} numberOfLines={1} ellipsizeMode="tail">
                            {item.drug.shortenedName}
                          </Text>
                          <SvgArrowRight width={24} height={24} />
                        </View>
                        <View style={ROW}>
                          <SvgCalendar width={16} height={16} style={{ marginTop: 1 }} />
                          <Text style={MEDICATION_PERIOD}>
                            {item.start_at.toFormat("yyyy.MM.dd")} ~{" "}
                            {item.end_at.toFormat("yyyy.MM.dd")} |{" "}
                            <Text style={DOSING_DAYS}>총{item.dosing_days_num}일</Text>
                          </Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <View style={{ ...ROW, paddingLeft: 16 }}>
                      <SvgMedicine width={84} height={24} />
                      <View style={FULL}>
                        <Text style={DOSE_INFO_TEXT}>
                          {item.dose}
                          {item.drug.unitConverted} | 하루 {item.doses_num}번 |{" "}
                          {item.per_days == 1 ? "매일" : `${item.per_days}일간격`}
                        </Text>
                        <View style={{ ...ROW, flexWrap: "wrap" }}>
                          {item.timeslots.map((timeSlot, index) => (
                            <View key={index} style={ALARM_BOX}>
                              <Text style={ALARM_TIME}>
                                {timeSlot.name == "custom"
                                  ? formatTime(timeSlot.when)
                                  : getText(timeSlot.name)}
                              </Text>
                            </View>
                          ))}
                        </View>
                      </View>
                    </View>
                  </View>
                )}
                ItemSeparatorComponent={() => <View style={DRUG_LINE} />}
                // onRefresh={pStore.fetchPrescriptions}
                // refreshing={pStore.isLoading}
                keyExtractor={(item) => item.id.toString()}
                // extraData={{ extra: store.prescriptions }}
                // ListEmptyComponent={<EmptyPrescriptionList />}
              />
            ) : null}
            <View style={{ marginTop: 16 }}>
              <TouchableItem
                style={styles.LARGE_RADIUS_BUTTON}
                onPress={() =>
                  navigation.navigate("SearchDrug", { mode: SearchDrugMode.NewPrescription })
                }
              >
                <Text style={styles.LARGE_RADIUS_BUTTON_TEXT}>
                  {prescription?.prescription_details.length == 0 ? `약 입력` : `약 추가`}
                </Text>
              </TouchableItem>
            </View>
          </View>
        </ScrollView>

        <Modal
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
          isVisible={isModalVisible}
          animationOut={"fadeOutDown"}
        >
          {renderSelectMemberModalContent()}
        </Modal>

        <Popup
          isVisible={popupVisible}
          title={popupTitle}
          message={popupMessage}
          button1={popupButton1}
          button2={popupButton2}
        />
        <RBSheet
          ref={bottomSheetRef}
          height={styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * 2 + 14}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            container: {
              backgroundColor: "transparent",
            },
          }}
        >
          <StatusBar backgroundColor={color.dimmedStatusBar} />
          {renderBottomSheet()}
        </RBSheet>
      </Screen>
    )
  },
)

export default withNavigationFocus(AddPrescriptionScreen)
