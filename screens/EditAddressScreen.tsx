import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { Platform, View } from "react-native"
import { WebView } from "react-native-webview"

import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color } from "../theme"

const daumPostcode = require("../assets/daum-postcode.html")

type EditAddressRouteProp = RouteProp<DetailsStackParamList, "EditAddress">

type EditAddressNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "EditAddress">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<MyPageStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: EditAddressNavigationProp
  route: EditAddressRouteProp
}

@inject("accountStore")
@observer
export default class EditAddressScreen extends Component<Props> {
  static navigationOptions = {
    title: "주소 수정",
  }

  render() {
    const html = `
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width">
      <link href="//t1.daumcdn.net/localimg/localimages/07/postcode/2015/favicon.ico" rel="shortcut icon">       
      <title>Daum 우편번호 서비스 사용 가이드</title>
    </head>
    <body style="padding: 0; margin: 0">
    
    <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js?autoload=false"></script>
    <script>
      daum.postcode.load(function() {
        new daum.Postcode({
          oncomplete: function(data) {
              // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
      
              
              // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
              // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
              var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
              var extraRoadAddr = ''; // 도로명 조합형 주소 변수
      
              // 법정동명이 있을 경우 추가한다. (법정리는 제외)
              // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
              if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                  extraRoadAddr += data.bname;
              }
              // 건물명이 있고, 공동주택일 경우 추가한다.
              if(data.buildingName !== '' && data.apartment === 'Y'){
                extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
              }
              // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
              if(extraRoadAddr !== ''){
                  extraRoadAddr = ' (' + extraRoadAddr + ')';
              }
              // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
              if(fullRoadAddr !== ''){
                  fullRoadAddr += extraRoadAddr;
              }
      
              // 우편번호와 주소 정보를 해당 필드에 넣는다.
              // document.getElementById('sample4_postcode').value = data.zonecode; //5자리 새우편번호 사용
              // document.getElementById('sample4_roadAddress').value = fullRoadAddr;
              window.ReactNativeWebView.postMessage(fullRoadAddr);
              // alert(fullRoadAddr);
      
              // document.getElementById('sample4_jibunAddress').value = data.jibunAddress;
      
              // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
              // if(data.autoRoadAddress) {
              //     //예상되는 도로명 주소에 조합형 주소를 추가한다.
              //     var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
              //     document.getElementById('guide').innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';
      
              // } else if(data.autoJibunAddress) {
              //     var expJibunAddr = data.autoJibunAddress;
              //     document.getElementById('guide').innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';
      
              // } else {
              //     document.getElementById('guide').innerHTML = '';
              // }
          },
          width : '100%',
          height : '100%',
          maxSuggestItems : 5
        }).embed();
      });
    
    </script>
    </body>
    </html>
    `

    return (
      <View style={{ flex: 1 }}>
        <WebView
          ref="webview"
          androidLayerType={"hardware"}
          originWhitelist={["*"]}
          onMessage={(event) => {
            let message = event.nativeEvent.data
            // this.props.accountStore. user.setAddress(message)
            // @ts-ignore
            this.props.navigation.replace("EditUserProfile", { address: message })
          }}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          source={{ uri: "http://onenewman.co.kr/daum-postcode.html" }}
          // source={ Platform.OS == 'android' ? { uri: 'file:///android_asset/daum-postcode.html' } : { html } }
          // source={daumPostcode}
          style={{ marginTop: 10, backgroundColor: color.background }}
          allowFileAccess={true}
        />
      </View>
    )
  }
}
