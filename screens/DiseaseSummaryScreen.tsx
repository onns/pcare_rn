import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import * as scale from "d3-scale"
import { isEmpty } from "lodash"
import { inject, observer } from "mobx-react"
import { onAction, onSnapshot } from "mobx-state-tree"
import { Spinner, View } from "native-base"
import React, { Component, useRef } from "react"
import {
  BackHandler,
  Dimensions,
  Image,
  Linking,
  Platform,
  ScrollView,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import { BarChart, Grid, PieChart, XAxis, YAxis } from "react-native-svg-charts"
import { TabBar, TabView } from "react-native-tab-view"
import { WebView } from "react-native-webview"

import { RestrictedContentAlert } from "@components/RestrictedContentAlert"
import { sendLogEvent } from "@lib/analytics"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { goBack } from "@react-navigation/compat/lib/typescript/src/helpers"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgGraph from "../assets/images/graph.svg"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  MainTabParamList,
  PrescriptionsStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { Disease, DiseaseDetail } from "../stores/Disease"
import { DiseasePrevalence } from "../stores/nps/DiseasePrevalence"
import { NpsStore } from "../stores/nps/NpsStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"
import { palette } from "../theme/palette"

const DISEASE_TITLE: TextStyle = {
  ...typography.title2,
  marginBottom: 3,
}
const DISEASE_DETAIL_NAME: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
}
const DISEASE_ONE_LINER: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 16,
  lineHeight: 24,
  color: color.text,
  textDecorationLine: "underline",
  marginBottom: 24,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const TOUCHABLE_ITEM: ViewStyle = {
  backgroundColor: "#fff",
  // borderBottomWidth: 1,
  borderBottomColor: color.palette.pale,
}
const DISEASE_SUMMARY = {
  fontSize: 15,
  lineHeight: 24,
  letterSpacing: -0.3,
  paddingBottom: 42,
}
const PREVALENCE_DESC: TextStyle = {
  ...typography.paragraph.default,
  color: color.textColor.action,
}
const PREVALENCE_DESC_ACCENT = {
  ...PREVALENCE_DESC,
  color: color.textColor.primary,
}

const HORIZONTAL_BAR_CHART_BOX: ViewStyle = {
  flexDirection: "row",
  height: 302.5,
  paddingVertical: 16,
}
const BAR_CHART: ViewStyle = { flex: 1, marginLeft: 8, marginRight: 8 }
const SCROLL_VIEW_CONTENT_CONTAINER = {
  backgroundColor: color.palette.white,
}
const SCROLL_VIEW_CONTENT_BLOCK = {
  paddingHorizontal: 20,
}
const SCROLL_VIEW_HEADROOM = {
  height: 32,
}
const X_Y_AXIS_TEXT = {
  fill: color.text,
  fontSize: 13,
  fontWeight: "600",
  letterSpacing: -0.26,
}
const GRAPH_COLOR_INFO_BOX: ViewStyle = {
  flexDirection: "row",
  justifyContent: "flex-end",
  alignItems: "center",
  marginTop: 8,
}
const COLOR_INFO_RECT: ViewStyle = {
  width: 10,
  height: 10,
  backgroundColor: color.palette.brightCyan3,
  marginRight: 8,
}
const COLOR_INFO_RECT2: ViewStyle = {
  ...COLOR_INFO_RECT,
  backgroundColor: color.palette.salmon,
  marginLeft: 16,
}
const GRAPH_COLOR_INFO_TEXT: TextStyle = { fontFamily: typography.primary, fontSize: 15 }
const BOTTOM_CONTENT = {
  flex: 1,
}
const BOTTOM_CONTENT_DESC: TextStyle = {
  ...typography.captions.default,
  opacity: 0.7,
}
const BOTTOM_CONTENT_DESC_BOLD: TextStyle = {
  ...BOTTOM_CONTENT_DESC,
  fontFamily: typography.bold,
}
const CIRCLE_DEPARTMENT: ViewStyle = { width: 10, height: 10, marginRight: 8 }
const CIRCLE_DEPARTMENT_ROW: ViewStyle = {
  flexDirection: "row",
  marginBottom: 8,
  alignItems: "center",
}
const PIE_CHART: ViewStyle = { width: 160, height: 160, marginTop: 8 }
const RANK_LIST_BOX: ViewStyle = {
  borderWidth: 1,
  borderColor: color.palette.pale,
  borderRadius: 0.5,
  paddingTop: 24,
  paddingHorizontal: 24,
  paddingBottom: 8,
}
const ROW_ALIGN_CENTER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const RANK_BOX: ViewStyle = {
  width: 32,
  height: 32,
  borderRadius: 8,
  backgroundColor: color.decorative.surfaceSolidNeutral,
  marginRight: 16,
  justifyContent: "center",
  alignItems: "center",
}
const RANK_BOX_ORANGE: ViewStyle = {
  ...RANK_BOX,
  backgroundColor: color.decorative.surfaceSolidPrimary,
}
const RANK_TEXT: TextStyle = {
  ...typography.heading.subheading,
  color: color.textOn.primary,
}
const RANK_CONTENT: TextStyle = {
  ...typography.ui.defaultEmphasized,
  flex: 1,
}
const RANK_CONTENT_ACCENT: TextStyle = {
  ...RANK_CONTENT,
  color: color.textColor.primary,
}
const RANK_PERCENT: TextStyle = {
  ...typography.ui.default,
}
const RANK_PERCENT_SUB: TextStyle = {
  ...RANK_PERCENT,
  color: color.textColor.primary,
}

const RANK_ETC_DESC = { ...typography.paragraph.default }
const RANK_ROW_LINE: ViewStyle = {
  flex: 1,
  height: 1,
  backgroundColor: color.palette.veryLightPink3,
  marginVertical: 16,
}
const EMPTY_DATA_BOX: ViewStyle = {
  flex: 1,
  height: 112,
  borderRadius: 2,
  backgroundColor: color.palette.grey6,
  justifyContent: "center",
  alignItems: "center",
  marginTop: 20,
}
const EMPTY_DATA_TEXT: TextStyle = {
  fontSize: 14,
  lineHeight: 24,
  color: color.palette.grey3,
  marginTop: 8,
}
const PIE_CHART_BOX: ViewStyle = {
  flexDirection: "row",
  paddingHorizontal: 10,
  paddingVertical: 20,
  justifyContent: "center",
}
const SUMMARY_CONTENT_BOX: ViewStyle = {
  // flex: 1,
  backgroundColor: "#fff",
  paddingLeft: 20,
  paddingRight: 26,
  paddingTop: 32,
  paddingBottom: 40,
}
const VIEW_RELATED_DISEASES_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 16,
  lineHeight: 24,
  color: color.text,
}
const DATA_FROM: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
  marginTop: 4,
  marginBottom: 16,
}
const RELATED_DISEASE_BUTTON: ViewStyle = {
  paddingVertical: 4,
  paddingHorizontal: 8,
  justifyContent: "center",
  borderWidth: 1,
  borderColor: color.primary,
  borderRadius: 1,
  marginRight: 8,
  marginBottom: 8,
}
const RELATED_DISEASE_TEXT: TextStyle = {
  ...typography.body,
  color: color.primary,
}
const PREVALENCE_DESC_APPENDIX: TextStyle = {
  ...typography.captions.small,
  color: color.textColor.subdued,
  textAlign: "right",
}
const TAB_BAR: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingBottom: 12,
  shadowOffset: { height: 0, width: 0 },
  shadowColor: "transparent",
  shadowOpacity: 0,
  elevation: 0,
  borderBottomWidth: 1,
  borderBottomColor: color.borders.neutralSubded,
}
const DATA_FROM_BOX: ViewStyle = {
  // borderRadius: 1,
  backgroundColor: color.surface.neutralDefault,
  marginTop: 48,
  paddingHorizontal: 20,
  paddingVertical: 24,
  paddingBottom: 56,
  flex: 1,
}
const DISEASE_BOX: ViewStyle = {
  paddingTop: 8,
  paddingLeft: 16,
  paddingRight: 10,
  paddingBottom: 22,
}
const ROW_WRAP: ViewStyle = { ...ROW, flexWrap: "wrap" }
const FULL_WHITE: ViewStyle = { backgroundColor: color.palette.white }
const SECTION_TITLE: TextStyle = { ...typography.heading.section, marginBottom: 16 }
const LINE: ViewStyle = { height: 1, backgroundColor: color.line, marginVertical: 24 }
const SECTION_DESC_BOX: ViewStyle = {
  backgroundColor: color.surface.neutralDefault,
  paddingVertical: 16,
  paddingHorizontal: 24,
  borderRadius: 8,
}

const data = [
  {
    percent: 0,
    label: "70대+",
  },
  {
    percent: 0,
    label: "60대",
  },
  {
    percent: 0,
    label: "50대",
  },
  {
    percent: 0,
    label: "40대",
  },
  {
    percent: 0,
    label: "30대",
  },
  {
    percent: 0,
    label: "20대",
  },
  {
    percent: 0,
    label: "10대",
  },
  {
    percent: 0,
    label: "아동",
  },
]
const circleColors = [
  color.palette.orange,
  color.palette.paleSalmon,
  color.palette.veryPaleRed,
  color.palette.gray2,
  color.palette.gray3,
  color.palette.grey4,
]

type DiseaseSummaryRouteProp = RouteProp<PrescriptionsStackParamList, "DiseaseSummary">

type DiseaseSummaryNavigationProp = CompositeNavigationProp<
  StackNavigationProp<PrescriptionsStackParamList, "DiseaseSummary">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AuthStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  prescriptionStore: PrescriptionStore
  npsStore: NpsStore
  accountStore: AccountStore
  navigation: DiseaseSummaryNavigationProp
  route: DiseaseSummaryRouteProp
}

interface State {
  index: number
  routes: Array
  includesCdc: boolean
  prevalenceData: any
  prevalenceXAxis: any
  prevalencePercent: number
  webViewHeight: number | null
  contentHeight: number
  count: number
}

@inject("prescriptionStore", "npsStore", "accountStore")
@observer
export default class DiseaseSummaryScreen extends Component<Props, State> {
  static navigationOptions = {
    title: "",
    headerStyle: {
      height: Platform.OS === "ios" ? 76 : undefined,
      borderBottomWidth: 0,
      elevation: 0,
      shadowColor: "transparent",
    },
  }

  diseaseId = -1
  diseaseCode = ""
  index = -1
  isSubDisease: boolean | undefined
  guideTabVisible: boolean | undefined
  tabIndex: number | undefined
  onActionDisposer: any
  onSnapshotDisposer: any
  age: number
  ageRange: string

  constructor(props: Props) {
    super(props)
    const { npsStore, accountStore, prescriptionStore, route } = this.props
    this.guideTabVisible = this.getGuideTabVisible(route.params)
    this.tabIndex = this.getTabIndex(route.params)

    this.state = {
      index: this.tabIndex ? this.tabIndex : 0,
      routes: this.guideTabVisible
        ? [
            { key: "질병개요", title: "질병개요" },
            { key: "질병 및 건강관리정보", title: "질병 및 건강관리정보" },
            { key: "진단현황", title: "진단현황" },
            { key: "진료과", title: "진료과" },
            { key: "동반질병", title: "동반질병" },
          ]
        : [
            { key: "질병개요", title: "질병개요" },
            { key: "진단현황", title: "진단현황" },
            { key: "진료과", title: "진료과" },
            { key: "동반질병", title: "동반질병" },
          ],
      includesCdc: false,
      prevalenceData: [
        {
          data: data,
          svg: {
            fill: color.palette.brightCyan3,
          },
        },
        {
          data: data,
        },
      ],
      prevalenceXAxis: [0, 100],
      prevalencePercent: 0,
      webViewHeight: 2000,
      contentHeight: -1,
      count: 0,
    }

    this.diseaseId = this.getDiseaseId(route.params)
    this.diseaseCode = this.getDiseaseCode(route.params)
    this.index = this.getIndex(route.params)
    this.isSubDisease = this.getIsSubDisease(route.params)
    this.age = this.getAge()
    this.ageRange = this.getAgeRange()

    this.onSnapshotDisposer = onSnapshot(npsStore.diseasePrevalence, (call) => {
      const diseasePrevalence = npsStore.diseasePrevalence
      const sortAgeDiseasePrevalence = diseasePrevalence.slice().sort((a, b) => {
        if (a.age < b.age) {
          return 1
        } else if (a.age > b.age) {
          return -1
        }
      })
      // TODO: 2017 -> Remote Config 에 올리기
      const dataM = sortAgeDiseasePrevalence
        .filter((value) => value.gender === 1 && value.year === 2017)
        .map((data) => data)
      const dataF = sortAgeDiseasePrevalence
        .filter((value) => value.gender === 2 && value.year === 2017)
        .map((data) => data)
      let prevalenceByAgeDataXAxis = sortAgeDiseasePrevalence
        .filter((value) => value.year === 2017)
        .map((data) => data.percent)

      let maxPercent = 0
      for (let i = 0; i < prevalenceByAgeDataXAxis.length; i++) {
        const percent = prevalenceByAgeDataXAxis[i]
        if (maxPercent < percent) {
          maxPercent = percent
        }
      }

      prevalenceByAgeDataXAxis = [0, ...prevalenceByAgeDataXAxis]

      let myData
      if (this.age !== -1) {
        myData = (accountStore.user.gender === "male" ? dataM : dataF).filter((value) =>
          value.age.startsWith(`${this.age}`),
        )[0]
      }

      this.setState({
        prevalenceData: [
          {
            data: dataM,
            svg: {
              fill: color.palette.brightCyan3,
            },
          },
          {
            data: dataF,
          },
        ],
        prevalenceXAxis: this.getTicks(6, [0, maxPercent]),
        prevalencePercent: myData ? myData.percent : 0,
      })
    })

    // PrescriptionStore 의 fetchDisease 함수 안에서 NPS 데이터 조회하도록 하여 주석처리함.
    // 추후 삭제 예정
    // this.onActionDisposer = onAction(prescriptionStore, (call) => {
    //   console.tron.log("onAction prescriptionStore ")
    //   if (call.name == "setDisease" && call.args) {
    //     console.tron.log("onAction setDisease")
    //     if (call.args[0] && call.args[0].rep_code) {
    //       if (call.args[0].diseaseDetail) {
    //         this.checkCdc(call.args[0].diseaseDetail)
    //       }
    //       let repDiseaseCode = call.args[0].rep_code
    //       if (repDiseaseCode.length == 0) {
    //         repDiseaseCode = call.args[0].code.split(".")[0]
    //       }
    //       npsStore.fetchDiseasePrevalence(repDiseaseCode)
    //       npsStore.fetchDiseaseDepartment(repDiseaseCode)
    //       npsStore.fetchDiseaseComorb(repDiseaseCode)
    //     }
    //   }
    // })
  }

  UNSAFE_componentWillMount() {
    // this.props.prescriptionStore.fetchDiseaseSummary(this.diseaseId, this.diseaseCode) // test-id: 7856
    const { npsStore, prescriptionStore } = this.props
    const { prescriptionDetail } = prescriptionStore

    if (this.index !== -1) {
      if (this.isSubDisease) {
        this.checkCdc(prescriptionDetail.subdiseases[this.index].description)
      } else {
        this.checkCdc(prescriptionDetail.disease[this.index].description)
      }
    } else {
      prescriptionStore.fetchDisease(this.diseaseId, this.diseaseCode, npsStore)
    }
  }

  componentDidMount() {
    const { npsStore, prescriptionStore, route } = this.props
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    firebase.analytics().logEvent("disease_summary_screen")
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })

    const { prescriptionDetail } = prescriptionStore

    let repDiseaseCode = ""
    if (this.index !== -1) {
      if (this.isSubDisease) {
        repDiseaseCode = prescriptionDetail!.subdiseases[this.index].rep_code
        if (repDiseaseCode.length === 0) {
          repDiseaseCode = prescriptionDetail!.subdiseases[this.index].code.split(".")[0]
        }
      } else {
        repDiseaseCode = prescriptionDetail!.disease[this.index].rep_code
        if (repDiseaseCode.length === 0) {
          repDiseaseCode = prescriptionDetail!.disease[this.index].code.split(".")[0]
        }
      }

      npsStore.fetchDiseasePrevalence(repDiseaseCode)
      npsStore.fetchDiseaseDepartment(repDiseaseCode)
      npsStore.fetchDiseaseComorb(repDiseaseCode)
    } else if (!isEmpty(this.diseaseCode)) {
      const repDiseaseCode = this.diseaseCode.split(".")[0]
      npsStore.fetchDiseasePrevalence(repDiseaseCode)
      npsStore.fetchDiseaseDepartment(repDiseaseCode)
      npsStore.fetchDiseaseComorb(repDiseaseCode)
    }
  }

  componentWillUnmount() {
    this.onSnapshotDisposer()
    // this.onActionDisposer()
  }

  getDiseaseId(params) {
    return params.id
  }

  getDiseaseCode(params) {
    return params.code
  }

  getIndex(params) {
    return params.index !== undefined ? params.index : -1
  }

  getIsSubDisease(params) {
    return params.isSubDisease
  }

  getGuideTabVisible(params) {
    return params.guideTabVisible
  }

  getTabIndex(params) {
    return params.tabIndex
  }

  getTicks(ticks: number, domainData: (number | { valueOf(): number })[]) {
    const x = scale.scaleLinear().domain(domainData).range([0, 100])

    return x.ticks(ticks)
  }

  getAge(): number {
    const { accountStore, prescriptionStore } = this.props
    let birth
    if (prescriptionStore.selectedFamilyMemberKey.includes("_u")) {
      birth = accountStore.user.birth
    } else {
      for (let i = 0; i < accountStore.family.length; i++) {
        const member = accountStore.family[i]
        if (accountStore.selectedMemberKey.includes(String(member.id))) {
          birth = member.birth_date
          break
        }
      }
    }

    if (birth) {
      const date = new Date()
      const year = date.getFullYear()
      const month = date.getMonth() + 1
      const day = date.getDate()

      const birthYear = birth.getFullYear()
      const birthMonth = birth.getMonth() + 1
      const birthDay = birth.getDate()

      const age =
        month < birthMonth || (month === birthMonth && day < birthDay)
          ? year - birthYear - 1
          : year - birthYear

      if (age < 10) {
        return 0
      } else if (age < 20) {
        return 10
      } else if (age < 30) {
        return 20
      } else if (age < 40) {
        return 30
      } else if (age < 50) {
        return 40
      } else if (age < 60) {
        return 50
      } else if (age < 70) {
        return 60
      }
    }
    return -1
  }

  getAgeRange(): string {
    switch (this.age) {
      case 0:
        return "0-9"
      case 10:
        return "10-19"
      case 20:
        return "20-29"
      case 30:
        return "30-39"
      case 40:
        return "40-49"
      case 50:
        return "50-59"
      case 60:
        return "60-69"
      case 70:
        return "70 +"
    }
  }

  isSamplePrescription(): boolean {
    const store = this.props.prescriptionStore
    return (
      store.prescriptionDetail &&
      store.prescriptionDetail.disease.length > 0 &&
      store.prescriptionDetail.disease[0].name.includes("샘플 처방전")
    )
  }

  render() {
    const { prescriptionStore } = this.props
    const { prescriptionDetail, disease, status } = prescriptionStore
    let data: Disease

    if (this.index !== -1) {
      if (this.isSubDisease) {
        data = prescriptionDetail.subdiseases[this.index]
      } else {
        data = prescriptionDetail.disease[this.index]
      }
    } else {
      data = disease
    }

    if (!data || data.id !== this.diseaseId) {
      return <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
    }

    return (
      <View style={{ flex: 1, backgroundColor: color.background }}>
        {/* 질병명 헤더 */}
        <View style={TOUCHABLE_ITEM}>
          <View style={DISEASE_BOX}>
            <Text style={DISEASE_TITLE}>{data.feature}</Text>
            <Text style={DISEASE_DETAIL_NAME}>
              {this.isSamplePrescription()
                ? `${data.code.substring(1)} ${data.name.replace("(샘플 처방전)", "")}`
                : `${data.code} ${data.name}`}
            </Text>
          </View>
        </View>

        <TabView
          navigationState={this.state}
          renderScene={({ route }) => {
            switch (route.key) {
              case "질병개요":
                return (
                  <View style={FULL_WHITE}>
                    <ScrollView>
                      <View style={SUMMARY_CONTENT_BOX}>
                        {data.description.length > 0 ? (
                          <>
                            <Text style={DISEASE_ONE_LINER}>{data.description[0].feature}</Text>
                            <Text style={DISEASE_SUMMARY}>{data.description[0].desc}</Text>
                          </>
                        ) : (
                          <View style={[DATA_FROM_BOX, { marginTop: 24 }]}>
                            <Text style={BOTTOM_CONTENT_DESC_BOLD}>
                              현재 파프리카케어는 국내 다빈도 질병 순위 상위 60% 에 대하여 질병
                              정보를 제공하고 있습니다. 해당 질병에 대한 질병 정보를 추후 업데이트
                              예정입니다.
                            </Text>
                          </View>
                        )}
                        {this.state.includesCdc ? (
                          <View>
                            <Text style={VIEW_RELATED_DISEASES_TEXT}>관련 질병</Text>
                            <Text style={DATA_FROM}>(출처: 질병관리본부, 대한의학회)</Text>
                          </View>
                        ) : null}

                        <View style={ROW_WRAP}>
                          {data.description?.length > 1 ? (
                            <TouchableOpacity
                              key="data.description[1]"
                              style={RELATED_DISEASE_BUTTON}
                              onPress={() => {
                                console.log("류마티스관절염 ")
                                this.props.navigation.navigate("DiseaseDetail", {
                                  id: data.description[1].id,
                                  diseaseIdx: this.index,
                                  diseaseDetailIdx: 1,
                                  isSubDisease: this.isSubDisease,
                                })
                              }}
                            >
                              <Text style={RELATED_DISEASE_TEXT}>{data.description[1].title}</Text>
                            </TouchableOpacity>
                          ) : null}
                        </View>
                        {data.description.length > 0 ? (
                          <View style={DATA_FROM_BOX}>
                            <Text style={BOTTOM_CONTENT_DESC_BOLD}>
                              <Text style={BOTTOM_CONTENT_DESC}>출처: </Text>
                              파프리카케어 자체 제작.{"\n"}* 질병에 대한 간략한 설명으로 의료
                              전문가의 소견을 대신할 수 없습니다.
                            </Text>
                          </View>
                        ) : null}
                      </View>
                    </ScrollView>
                  </View>
                )

              case "진단현황":
                return this.renderPrevalence()

              case "진료과":
                return this.renderDepartmentAnalysis()

              case "동반질병":
                return this.renderComorbAnalysis()

              case "질병 및 건강관리정보":
                return this.renderGuide()
            }

            return (
              <View style={FULL_WHITE}>
                <ScrollView>
                  <Text>암 것도 없을 때</Text>
                </ScrollView>
              </View>
            )
          }}
          renderTabBar={this.renderTabBar}
          onIndexChange={(index: number) => this.setState({ index })}
          initialLayout={{
            // width: Dimensions.get("window").width,
            // height: Dimensions.get("window").height,
            height: 0,
          }}
          swipeEnabled={Platform.OS === "ios"}
        />
      </View>
    )
  }

  renderPrevalence() {
    const contentInset = { top: 10, left: 0, right: 0, bottom: 10 }
    const gender = this.props.accountStore.user.gender
    const { npsStore } = this.props
    const diseasePrevalence = this.props.npsStore.diseasePrevalence
    const { prevalencePercent } = this.state
    return (
      <View style={SCROLL_VIEW_CONTENT_CONTAINER}>
        <ScrollView>
          <View style={SCROLL_VIEW_HEADROOM} />
          {diseasePrevalence.length > 0 ? (
            <View>
              {this.age !== -1 ? (
                <View style={SCROLL_VIEW_CONTENT_BLOCK}>
                  <Text style={SECTION_TITLE}>
                    {this.age !== 0 ? `${this.age}대` : "0-9세"}{" "}
                    {gender === "male" ? "남성" : "여성"}의 경우
                  </Text>
                  <Text style={PREVALENCE_DESC}>
                    {prevalencePercent === 0 ? (
                      "해당 질병으로 진단 받은 사람이 없습니다."
                    ) : (
                      <Text style={PREVALENCE_DESC}>
                        <Text style={PREVALENCE_DESC_ACCENT}>
                          {this.displayRate(prevalencePercent)}
                        </Text>
                        이 진단 받았습니다.
                      </Text>
                    )}
                  </Text>
                  <View style={LINE} />
                </View>
              ) : null}
              <View style={SCROLL_VIEW_CONTENT_BLOCK}>
                <Text style={SECTION_TITLE}>
                  연령별, 성별<Text style={PREVALENCE_DESC_APPENDIX}>(2017년 기준)</Text>
                </Text>

                <Text style={PREVALENCE_DESC}>
                  해당 질병은 모든 연령대, 성별에서{" "}
                  <Text style={PREVALENCE_DESC_ACCENT}>
                    {this.displayRate(npsStore.averagePercentOfPrevalence)}
                  </Text>
                  이 진단 받았습니다.
                </Text>
              </View>
            </View>
          ) : null}
          {diseasePrevalence.length > 0 ? (
            <View style={SCROLL_VIEW_CONTENT_BLOCK}>
              <View style={GRAPH_COLOR_INFO_BOX}>
                <View style={COLOR_INFO_RECT} />
                <Text style={GRAPH_COLOR_INFO_TEXT}>남</Text>
                <View style={COLOR_INFO_RECT2} />
                <Text style={GRAPH_COLOR_INFO_TEXT}>여</Text>
              </View>
              <View style={HORIZONTAL_BAR_CHART_BOX}>
                <YAxis
                  data={data}
                  yAccessor={({ index }) => index}
                  scale={scale.scaleBand}
                  contentInset={contentInset}
                  // @ts-ignore
                  svg={X_Y_AXIS_TEXT}
                  spacingInner={0.6}
                  formatLabel={(_, index) => data[index].label}
                  style={{ width: 40 }}
                />
                <BarChart
                  style={BAR_CHART}
                  data={this.state.prevalenceData}
                  // @ts-ignore
                  horizontal={true}
                  yAccessor={({ item }: { item: DiseasePrevalence }) => item.percent}
                  svg={{ fill: color.palette.salmon }}
                  spacingInner={0.6}
                  contentInset={contentInset}
                  numberOfTicks={6}
                  gridMin={0}
                >
                  <Grid svg={{}} direction={Grid.Direction.VERTICAL} />
                </BarChart>
              </View>
              <XAxis
                style={{ marginLeft: 22 }}
                data={this.state.prevalenceXAxis}
                scale={scale.scaleBand}
                svg={X_Y_AXIS_TEXT}
                // spacingInner={0.7}
                formatLabel={(_, index) => `${this.state.prevalenceXAxis[index]}%`}
              />
            </View>
          ) : (
            this.renderEmptyData()
          )}

          <View style={BOTTOM_CONTENT}>{this.renderDataFromBox()}</View>
        </ScrollView>
      </View>
    )
  }

  renderDataFromBox() {
    return (
      <View style={DATA_FROM_BOX}>
        <Text style={BOTTOM_CONTENT_DESC}>
          <Text style={BOTTOM_CONTENT_DESC_BOLD}>출처: </Text>
          파프리카케어에서 제공하는 질병 및 약제 통계정보는 건강보험심사평가원에서 매년 제공하는
          145만명의 환자 표본데이터 중, 가장 최신의 2017년 자료를 의료데이터 솔루션 전문회사인
          Core-Zeta가 수행한 분석자료입니다.
        </Text>
      </View>
    )
  }

  renderDepartmentAnalysis() {
    const diseaseDepartment = this.props.npsStore.diseaseDepartment
    const sortDiseaseDepartment = diseaseDepartment.slice().sort((a, b) => {
      if (a.rank < b.rank) {
        return -1
      } else if (a.rank > b.rank) {
        return 1
      }
    })
    const pieData = sortDiseaseDepartment.map((value, index) => ({
      value: value.percent,
      svg: {
        fill: circleColors[index],
        onPress: () => null,
      },
      key: value.rank,
    }))

    return (
      <View style={SCROLL_VIEW_CONTENT_CONTAINER}>
        <ScrollView>
          <View style={SCROLL_VIEW_HEADROOM} />
          {sortDiseaseDepartment.length > 0 ? (
            <View style={SCROLL_VIEW_CONTENT_BLOCK}>
              <Text style={SECTION_TITLE}>진료는 어디에서?</Text>
              <Text style={PREVALENCE_DESC}>
                해당 질병의 진료가 가장 많이 이뤄지고 있는 진료과는{" "}
                <Text style={PREVALENCE_DESC_ACCENT}>
                  {sortDiseaseDepartment.length > 0 ? sortDiseaseDepartment[0].department : ""}
                </Text>{" "}
                입니다.
              </Text>
            </View>
          ) : null}
          {sortDiseaseDepartment.length > 0 ? (
            <View>
              <View style={PIE_CHART_BOX}>
                <PieChart
                  style={PIE_CHART}
                  data={pieData}
                  innerRadius="60%"
                  sort={(a, b) => parseInt(a.key) - parseInt(b.key)}
                  padAngle={0}
                />
                <View
                  style={{ height: 160, marginLeft: 30, marginTop: 8, justifyContent: "center" }}
                >
                  {sortDiseaseDepartment.map((item, index) => (
                    <View key={index} style={CIRCLE_DEPARTMENT_ROW}>
                      <View style={[CIRCLE_DEPARTMENT, { backgroundColor: circleColors[index] }]} />
                      <Text style={typography.sub2}>{item.department}</Text>
                    </View>
                  ))}
                </View>
              </View>
              <View style={SCROLL_VIEW_CONTENT_BLOCK}>
                <Text style={PREVALENCE_DESC_APPENDIX}>*상급종합병원 기준</Text>
              </View>
              <View style={SCROLL_VIEW_HEADROOM} />
              <View style={SCROLL_VIEW_CONTENT_BLOCK}>
                {sortDiseaseDepartment.map((item, index) => (
                  <View
                    key={index}
                    style={{ ...ROW_ALIGN_CENTER, paddingHorizontal: 4, paddingVertical: 12 }}
                  >
                    <View
                      style={item.rank === "1" ? { ...RANK_BOX_ORANGE, marginRight: 16 } : RANK_BOX}
                    >
                      <Text style={RANK_TEXT}>{item.rank}</Text>
                    </View>
                    <Text style={item.rank === "1" ? RANK_CONTENT_ACCENT : { ...RANK_CONTENT }}>
                      {item.department}
                    </Text>
                    <Text style={item.rank === "1" ? RANK_PERCENT_SUB : RANK_PERCENT}>
                      {item.percent < 0.5 ? item.percent.toFixed(1) : Math.round(item.percent)}%
                    </Text>
                  </View>
                ))}
              </View>
            </View>
          ) : (
            this.renderEmptyData()
          )}
          <View style={BOTTOM_CONTENT}>{this.renderDataFromBox()}</View>
        </ScrollView>
      </View>
    )
  }

  renderComorbAnalysis() {
    const diseaseComorb = this.props.npsStore.diseaseComorb
    const pieData = diseaseComorb.map((value, index) => ({
      value: value.percent,
      svg: {
        fill: circleColors[index],
        onPress: () => null,
      },
      key: value.rank,
    }))
    const { prescriptionStore } = this.props
    const { prescriptionDetail, disease } = prescriptionStore
    let data

    if (this.index !== -1) {
      if (this.isSubDisease) {
        data = prescriptionDetail.subdiseases[this.index]
      } else {
        data = prescriptionDetail.disease[this.index]
      }
    } else {
      data = disease
    }

    return (
      <View style={SCROLL_VIEW_CONTENT_CONTAINER}>
        <ScrollView>
          <View style={SCROLL_VIEW_HEADROOM} />
          {diseaseComorb.length > 0 ? (
            <View style={SCROLL_VIEW_CONTENT_BLOCK}>
              <Text style={SECTION_TITLE}>함께 진단되었던 다른 질병은?</Text>
              <View style={SECTION_DESC_BOX}>
                <Text style={BOTTOM_CONTENT_DESC}>
                  동반질병이란 해당 질병을 진단받은 사람들이 진단받은 다른 질병입니다.
                </Text>
              </View>
            </View>
          ) : null}
          <View style={SCROLL_VIEW_HEADROOM} />
          {diseaseComorb.length > 0
            ? diseaseComorb.map((item, index) => (
                <View key={`diseaseComorb${index}`} style={SCROLL_VIEW_CONTENT_BLOCK}>
                  <View>
                    {item.rank !== "etc" ? (
                      <View style={ROW_ALIGN_CENTER}>
                        <View style={RANK_BOX_ORANGE}>
                          <Text style={RANK_TEXT}>{item.rank}</Text>
                        </View>
                        <View style={{ flexDirection: "column" }}>
                          <Text style={RANK_CONTENT}>
                            {item.comorb_name_kr}
                            {"  "}
                            <Text style={RANK_PERCENT_SUB}>
                              {item.percent < 0.5
                                ? item.percent.toFixed(1)
                                : Math.round(item.percent)}
                              %
                            </Text>
                          </Text>
                          <Text style={[RANK_ETC_DESC, { color: color.textColor.subdued }]}>
                            {item.comorb_code}.
                          </Text>
                        </View>
                      </View>
                    ) : (
                      <View style={{ marginTop: 8 }}>
                        <Text style={RANK_ETC_DESC}>
                          그 외에도{" "}
                          {this.isSamplePrescription()
                            ? data.name.replace("(샘플 처방전)", "")
                            : data.name}
                          을 진단받은 사람들의{" "}
                          <Text style={typography.sub1}>
                            {item.percent < 0.5
                              ? item.percent.toFixed(1)
                              : Math.round(item.percent)}
                          </Text>
                          %가 기타 다른 질병을 진단받았습니다.
                        </Text>
                      </View>
                    )}
                    {index + 1 < diseaseComorb.length ? <View style={RANK_ROW_LINE} /> : null}
                  </View>
                </View>
              ))
            : this.renderEmptyData()}
          <View style={BOTTOM_CONTENT}>{this.renderDataFromBox()}</View>
        </ScrollView>
      </View>
    )
  }

  renderGuide() {
    const { prescriptionDetail, disease } = this.props.prescriptionStore

    const webViewRef = useRef(null)

    const goBack = () => {
      if (webViewRef.current) {
        webViewRef.current.goBack()
      }
    }

    // const handleSetRef = (_ref) => {
    //   webviewRef = _ref
    // }
    let data

    if (this.index !== -1) {
      if (this.isSubDisease) {
        data = prescriptionDetail.subdiseases[this.index]
      } else {
        data = prescriptionDetail.disease[this.index]
      }
    } else {
      data = disease
    }

    return (
      <View style={{ flex: 1 }}>
        <WebView
          ref={webViewRef}
          setSupportMultipleWindows={false}
          androidLayerType={"hardware"}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          allowFileAccess={true}
          originWhitelist={["*"]}
          source={{
            html: `
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"} />
          <style>
            body {
              white-space: inherit !important;
              max-width: none !important;
              marin: none !important;
              width: 100% !important;
              word-break: keep-all !important; 
              padding: 0 !important;
            }
            
            div {
              padding: 10;
              font-size: 16;
              color: rgb(85, 85, 85);
              line-height: 19pt;
              list-style: none;
              padding: 0 !important;
            }
            header h1 {
              font-size: 32 !important;
              font-weight: 'bold';
              color: rgb(85, 85, 85);
              padding: 0 18px !important;
            }
            h1 {
              font-size: 26 !important;
              font-weight: 'bold';
              color: rgb(85, 85, 85);
              padding: 0 18px !important;
            }
            h2 {
              font-size: 19 !important;
              font-weight: 'bold';
              color: rgb(85, 85, 85);
              padding: 0 18px !important;
            }

            h3 {
              font-size: 18;
              font-weight: 'bold';
              color: rgb(85, 85, 85);
              padding: 0 18px !important;
            }

            h4 {
              font-size: 17;
              font-weight: 'bold';
              color: rgb(255, 132, 26);
            }

            h5 {
              font-size: 16;
              font-weight: 'bold';
              color: rgb(85, 85, 85);
            }

            p, ul, li {
              font-size: 18 !important;
              color: rgb(85, 85, 85);
              line-height: 1.5;
              list-style: none;
              padding: 0 18px !important;
              word-break: break-word !important;
              opacity: 1 !important;
            }
            ul > li {
              padding-left: 4px !important;
            }
            .bulleted-list {
              padding-inline-start: 8px !important;
            }
            .bulleted-list > li {
              font-size: 16px !important;
              white-space: pre-wrap !important;
              
            }

            a {
              color: rgb(242, 74, 16);
              text-decoration: none !important;
            }
            .image {
              margin: 0 !important;
              padding: 0 16px !important;
            }
            img {
              width: 100%;
              height: 12em !important;
              border-radius: 24px;
              object-fit: cover;
              margin: 16px 0 !important;
            }

            /* 날짜 나오는 테이블 */
            header table {
              background-color: #F0F2F4;
              border-radius: 8px;
              margin: 0 18px 32px 18px !important;
              color: #7F8694;
              opacity: 1 !important;
            }
            td > time {
              color: #7F8694 !important;
              opacity: 1 !important;
            }
            header table, th, td {
              border: none !important;
              border-sapcing: 0 !important;
            }
            .properties {
              border: none !important;
              padding: 0 16px !important;
            }
            
            .property-row-multi_select {
              border: none !important;
              padding: 0 16px !important;
            }
            /* ---- 콜아웃 ---- */
            .callout { 
              background: rgba(241, 241, 239, 1) !important;
              font-size: 16px !important;
              white-space: inherit !important;
            }
            
            /* ---- 판매 제품 ---- */
            .goods {
              padding: 0 16px !important;
            }
            .goods:hover {
              background-color: #F0F2F4;
            }
            .goods-image {
              width: 80px !important;
              height: 80px !important;
              border-radius: 8px;
            }
            .goods-title {
              font-size: 16px !important;
              font-weight: bold !important;
              line-height: 1.3;
            }
            .goods-price {
              font-size: 16px !important;
              font-weight: bold !important;
              color: rgb(242, 74, 16);
            }
            .goods-container {
              display: flex;
              flex-direction: row;
            }
            .goods-meta {
              padding-top: 4px !important;
              display: flex;
              flex-direction: column;
            }
            </style>${data.guide.content}`,
          }}
          bounces={false}
          allowsFullscreenVideo={true}
          scrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          injectedJavaScript={`
          // const a = document.querySelector('a');
          // a.addEventListener('click', showConsole());
          
          // function showConsole() {
  	      //   window.history.go(-2);
          function openGoods(){
            window.open("https://smartstore.naver.com/papricacare/products/8130808769")
          }
            `}
          onMessage={(event) => {
            const message = event.nativeEvent.data
            /* event.nativeEvent.data must be string, i.e. window.postMessage
              should send only string.
            */
            // if (message !== undefined && message !== null) {
            //   Linking.openURL(message)
            // }
            const obj = JSON.parse(message)
            if (obj.linkAddress) {
              Linking.openURL(obj.linkAddress)
              sendLogEvent("press_purchase_link", {
                link_address: obj.linkAddress,
                purchase_screen: "DiseaseSummary",
              })
            }
          }}
        />
      </View>
    )
  }

  renderEmptyData() {
    const { npsStore } = this.props

    if (npsStore.status === "error") {
      return <RestrictedContentAlert />
    } else {
      return (
        <View style={EMPTY_DATA_BOX}>
          <SvgGraph width={29} height={28} />
          <Text style={EMPTY_DATA_TEXT}>데이터가 없습니다.</Text>
        </View>
      )
    }
  }

  renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{
        backgroundColor: color.palette.white,
      }}
      indicatorContainerStyle={{}}
      style={TAB_BAR}
      dynamicWidth
      scrollEnabled
      tabStyle={{
        width: "auto",
        // height: 42,
        padding: 0,
        margin: 0,
      }}
      renderLabel={({ route, focused, color }) => (
        <View
          style={{
            height: 36,
            borderRadius: 18,
            flexDirection: "row",
            backgroundColor: focused ? palette.orange : palette.veryLightPink3,
            justifyContent: "center",
            alignItems: "center",
            paddingHorizontal: 16,
            marginLeft: 8,
          }}
        >
          <Text
            style={{
              color,
              fontFamily: typography.bold,
              fontSize: 16,
              lineHeight: 24,
            }}
          >
            {route.title}
          </Text>
        </View>
      )}
      // labelStyle={{}}
      activeColor={color.palette.white}
      inactiveColor={`${color.text}4D`}
    />
  )

  displayRate(percent: number) {
    let text
    if (percent > 1) {
      text = `100명 중 ${Math.round(percent)}`
    } else if (percent > 0.1) {
      text = `1,000명 중 ${Math.round(percent * 10)}`
    } else if (percent > 0.01) {
      text = `10,000명 중 ${Math.round(percent * 100)}`
    } else if (percent > 0.001) {
      text = `100,000명 중 ${Math.round(percent * 1000)}`
    } else if (percent > 0.0001) {
      text = `1,000,000명 중 ${Math.round(percent * 10000)}`
    } else if (percent > 0.00001) {
      text = `10,000,000명 중 ${Math.round(percent * 100000)}`
    } else {
      text = `10,000,000명 중 ${Math.round(percent * 100000)}`
    }
    return `${text}명`
  }

  checkCdc(diseaseDetails) {
    // TODO: refactor checkCdc
    if (diseaseDetails?.length > 1) {
      this.setState({ includesCdc: true })
    }
    // diseaseDetails.map((diseaseDetail: DiseaseDetail) => {
    //   if (diseaseDetail.source === "cdc") {
    //     this.setState({ includesCdc: true })
    //   }
    // })
  }
}
