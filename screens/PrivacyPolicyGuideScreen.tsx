import LottieView from "lottie-react-native"
import { inject, observer } from "mobx-react"
import { ScrollView } from "native-base"
import React from "react"
import { Animated, Easing, TextStyle, View, ViewStyle } from "react-native"

import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  DetailsStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const lottieJson = require("../assets/lock_animation.json")

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}

const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  // justifyContent: 'space-between',
  paddingHorizontal: 20,
}

const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 30,
  paddingBottom: 30,
}

const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 34,
  letterSpacing: -2,
  color: color.text,
}

const TITLE_DOT: TextStyle = {
  fontSize: 34,
  color: "rgb(255, 132, 26)",
}

const BOTTOM_BUTTON: ViewStyle = {
  flexDirection: "column",
  justifyContent: "flex-end",
  // alignItems: 'stretch',
  // paddingTop: 20,
  marginTop: 20,
  marginBottom: 21,
}

type PrivacyPolicyGuideRouteProp = RouteProp<AuthStackParamList, "PrivacyPolicyGuide">

type PrivacyPolicyGuideNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "PrivacyPolicyGuide">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  navigation: PrivacyPolicyGuideNavigationProp
  route: PrivacyPolicyGuideRouteProp
}

interface State {
  progress: any
}

@inject("accountStore")
@observer
export default class PrivacyPolicyGuideScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "",
    headerBackTitle: null,
    headerLeft: null,
    headerRight: null,
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      progress: new Animated.Value(0),
    }
  }

  componentDidMount() {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 4500,
      easing: Easing.linear,
    }).start()
  }

  render() {
    return (
      <View style={BACKGROUND}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={TITLE}>
            <Text style={TITLE_TEXT}>개인정보 보안 방침</Text>
            <Text style={TITLE_DOT}>.</Text>
          </View>
          <Text
            style={{
              fontSize: 17,
              lineHeight: 22.1,
              letterSpacing: -0.34,
              color: color.palette.lightBlack,
              textAlign: "center",
              marginBottom: 24,
            }}
          >
            파프리카케어 운영진 누구도 회원님의{" "}
            <Text
              style={{
                fontFamily: typography.medium,
                fontSize: 17,
                lineHeight: 22.1,
                letterSpacing: -0.34,
                color: color.primary,
              }}
            >
              의료정보를 식별할 수 없습니다.
            </Text>
            {"\n"}
            안심하고 사용하세요.
          </Text>
          <LottieView
            style={{ height: 80, alignSelf: "center", marginBottom: 36 }}
            source={lottieJson}
            progress={this.state.progress}
          />
          <View style={{ paddingHorizontal: 8 }}>
            <Text style={{ fontSize: 14.6, lineHeight: 20, letterSpacing: -0.6, marginBottom: 15 }}>
              *파프리카케어에 올려주시는 처방전 사진의 개인정보는 두 단계의 보안 정책에 따라
              처리됩니다.
            </Text>
            <View
              style={{
                // height: 72,
                borderWidth: 1,
                borderColor: color.palette.grey2,
                borderRadius: 4,
                justifyContent: "center",
                alignItems: "center",
                paddingVertical: 15,
                paddingLeft: 4,
              }}
            >
              <Text
                style={{
                  fontSize: 14.6,
                  lineHeight: 20,
                  letterSpacing: -0.6,
                }}
              >
                •개인정보 삭제 후 의료 정보만 분리{"\n"}•의료정보 전자문서화 이후 처방전 사진 자동
                삭제
              </Text>
            </View>
          </View>

          <View style={BOTTOM_BUTTON}>
            <Button block rounded onPress={() => this.next()}>
              <Text>다음</Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    )
  }

  async next() {
    this.props.navigation.navigate("ServiceAgreement")
  }
}
