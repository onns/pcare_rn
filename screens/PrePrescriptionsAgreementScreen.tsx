import "@react-native-firebase/analytics"

import { inject, observer } from "mobx-react"
import { Input, ScrollView, Stack } from "native-base"
import React from "react"
import { Dimensions, Image, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import Modal from "react-native-modal"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { getPopupButton } from "../components/Popup"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import {
  DetailsStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  justifyContent: "space-between",
  paddingHorizontal: 20,
}
const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 30,
  paddingBottom: 31,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 34,
  lineHeight: 40,
  letterSpacing: -2,
  color: color.text,
  textAlign: "center",
}
const TITLE_DOT: TextStyle = {
  fontSize: 34,
  color: "rgb(255, 132, 26)",
}
const MESSAGE: TextStyle = {
  paddingHorizontal: 4,
  fontSize: 15,
  letterSpacing: -0.6,
  color: color.text,
  alignSelf: "center",
}
const MESSAGE_ACCENT: TextStyle = {
  ...MESSAGE,
  fontWeight: "bold",
  color: color.primary,
}
const SUB_MESSAGE: TextStyle = {
  paddingTop: 16,
  paddingLeft: 15,
  paddingRight: 30,
  fontSize: 13,
  lineHeight: 16,
  letterSpacing: -0.26,
  color: "rgb(153, 153, 153)",
  textAlign: "center",
}
const DESC_ACCENT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 22.5,
  letterSpacing: -0.3,
  color: color.palette.waterBlue,
  textDecorationLine: "underline",
  textAlign: "center",
}
const BOTTOM: ViewStyle = {
  flexDirection: "column",
  justifyContent: "flex-end",
  alignItems: "center",
  marginBottom: 30,
}
const POPUP_CONTAINER: ViewStyle = {
  paddingTop: 30,
  paddingHorizontal: 15,
  paddingBottom: 0,
  borderRadius: 10,
  backgroundColor: "#fff",
  marginHorizontal: 10,
}
const POPUP_TITLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 27,
  lineHeight: 32,
  letterSpacing: -1.08,
  marginBottom: 10,
  textAlign: "center",
}
const POPUP_MESSAGE: TextStyle = {
  fontSize: 16,
  lineHeight: 24,
  letterSpacing: -0.64,
  paddingHorizontal: 5,
  textAlign: "center",
}
const POPUP_MESSAGE_ACCENT: TextStyle = {
  ...POPUP_MESSAGE,
  fontFamily: typography.primary,
  color: color.primary,
}
const INPUT_BOX: ViewStyle = {
  backgroundColor: color.palette.white,
  height: 40,
  marginBottom: 20,
  marginLeft: 0,
  borderColor: color.palette.lightGrey2,
}
const INPUT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  fontWeight: "bold",
  lineHeight: 24,
  color: color.text,
  paddingLeft: 15.5,
  paddingTop: 0,
  paddingBottom: 0,
}
const ROW: ViewStyle = {
  flexDirection: "row",
  height: 20,
  marginBottom: 13.4,
  marginHorizontal: 10,
}

type PrePrescriptionsAgreementRouteProp = RouteProp<HomeStackParamList, "PrePrescriptionsAgreement">

type PrePrescriptionsAgreementNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "PrePrescriptionsAgreement">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: PrePrescriptionsAgreementNavigationProp
  route: PrePrescriptionsAgreementRouteProp
}

interface State {
  isModalVisible: boolean
  input: string
  buttonEnabled: boolean
}

@inject("accountStore", "prescriptionStore")
@observer
export default class PrePrescriptionsAgreementScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "",
    headerStyle: {
      borderBottomWidth: 0,
      height: 40, // default is 64
      elevation: 0,
      shadowColor: "transparent",
    },
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      isModalVisible: false,
      input: "",
      buttonEnabled: false,
    }
  }

  render() {
    const { navigation } = this.props
    return (
      <View style={BACKGROUND}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={TITLE}>
            <Text style={TITLE_TEXT}>
              등록된 처방전{"\n"}정보 수집 • 이용 동의<Text style={TITLE_DOT}>.</Text>
            </Text>
          </View>
          <Text style={MESSAGE}>
            <Text>
              파프리카케어의 의료정보 기록 및 관리 서비스를 계속 사용하기 위해서는 아래 민감정보
              수집 • 이용 동의가 필요합니다.{" "}
              <Text style={MESSAGE_ACCENT}>
                동의를 하지 않으시면 지금까지 등록하신 의료정보가 모두 삭제되어 복구가 불가합니다.
              </Text>
            </Text>
          </Text>
          <View style={{ marginTop: 15, marginBottom: 40 }}>
            <Text
              style={DESC_ACCENT}
              onPress={() =>
                navigation.navigate("WebView", {
                  url: "http://www.papricacare.com/sensitive-info-agreement-previous.html",
                })
              }
            >
              등록된 처방전 정보 수집 • 이용 동의서
            </Text>
          </View>
          <Button
            block
            rounded
            style={{
              backgroundColor: "rgb(217, 217, 217)",
              marginBottom: 8,
            }}
            onPress={() => this.toggleModal()}
          >
            <Text style={{ fontWeight: "bold", color: color.text }}>
              동의하지 않음 (등록된 의료정보 삭제)
            </Text>
          </Button>
          <Button
            block
            rounded
            style={{ marginBottom: 20 }}
            onPress={() => this.updateAgreement(true)}
          >
            <Text style={{ fontWeight: "bold" }}>동의합니다.</Text>
          </Button>
          <View style={BOTTOM}>
            <Text style={SUB_MESSAGE}>
              *위 선택 약관에 동의하지 않아도 서비스 사용이 가능합니다.{" "}
              <Text style={[SUB_MESSAGE, { color: color.primary }]}>
                다만 기존에 등록하신 모든 처방전 정보 및 의료정보는 삭제되며 복구가 불가합니다.
              </Text>
            </Text>
          </View>
          <Modal
            deviceWidth={Dimensions.get("window").width}
            deviceHeight={Dimensions.get("window").height}
            isVisible={this.state.isModalVisible}
          >
            <View style={ROW}>
              <View style={{ flex: 1 }}></View>
              <TouchableOpacity
                onPress={() => this.toggleModal()}
                style={{
                  width: 24,
                  height: 24,
                  justifyContent: "flex-start",
                  alignItems: "flex-end",
                }}
                hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}
              >
                <Image
                  source={require("../assets/images/close.png")}
                  style={{
                    width: 15.7,
                    height: 15.7,
                    tintColor: color.palette.white,
                    // resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
            </View>
            <View style={POPUP_CONTAINER}>
              <View style={{ alignItems: "center", marginBottom: 20 }}>
                <Text style={POPUP_TITLE}>동의하지 않음</Text>
                <Text style={POPUP_MESSAGE}>
                  지금까지 등록하신{" "}
                  <Text style={POPUP_MESSAGE_ACCENT}>모든 의료정보가 삭제되어 복구가 불가</Text>
                  합니다. 이대로 진행을 원하시면 아래 입력창에 “동의하지 않습니다”를 입력 후
                  “동의하지 않음 (의료정보 삭제)” 버튼을 눌러주세요.
                </Text>
              </View>
              <View style={{ marginBottom: 15 }}>
                <Stack inlineLabel rounded last style={INPUT_BOX}>
                  <Input
                    onChangeText={(value) => this.onChangeInputText(value)}
                    value={this.state.input}
                    placeholder="입력창"
                    placeholderTextColor={color.palette.lightGrey2}
                    style={INPUT}
                  />
                </Stack>
                <View
                  style={{
                    height: 1,
                    backgroundColor: color.palette.lightGrey2,
                    marginHorizontal: 10,
                    marginBottom: 20,
                  }}
                ></View>
                <Button
                  block
                  rounded
                  disabled={!this.state.buttonEnabled}
                  onPress={() => this.updateAgreement(false)}
                  style={{ backgroundColor: "rgb(217, 217, 217)" }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: this.state.buttonEnabled ? color.text : color.palette.white,
                    }}
                  >
                    동의하지 않음 (의료정보 삭제)
                  </Text>
                </Button>
                <View style={{ height: 10 }}></View>
                {getPopupButton(() => this.toggleModal(), "취소")}
              </View>
            </View>
          </Modal>
        </ScrollView>
      </View>
    )
  }

  onChangeInputText(value: string) {
    this.setState({
      input: value,
      buttonEnabled: value === "동의하지 않습니다",
    })
  }

  toggleModal() {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      input: "",
    })
  }

  async updateAgreement(agreed: boolean) {
    const { accountStore, navigation, prescriptionStore } = this.props
    const userId = accountStore.user.id

    // start making calls
    const response = await api.apisauce.get(
      `/user/confirm/privacy-imediate/${userId}/${agreed ? "y" : "post"}/`,
    )
    if (!response.ok) {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    } else {
      accountStore.user.setNewlyAgreed(agreed ? "y" : "post")
      accountStore.user.setNewlyAgreedDate(new Date())
      prescriptionStore.fetchPrescriptions(accountStore.user.id, "_u" + accountStore.user.id)
      for (let i = 0; i < accountStore.family.length; i++) {
        const member = accountStore.family[i]
        if (!member.family_user) {
          prescriptionStore.fetchPrescriptions(accountStore.user.id, "_c" + member.id, member.id)
        }
      }
    }
    navigation.navigate("Home")
  }
}
