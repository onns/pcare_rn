import "@react-native-firebase/analytics"

import { ApiResponse } from "apisauce"
import { observer } from "mobx-react-lite"
import { FormControl, Input, Stack, Toast } from "native-base"
import React, { useState } from "react"
import { Alert, TextStyle, View, ViewStyle } from "react-native"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import { Screen } from "../components/screen/screen"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { MainTabParamList, MyPageStackParamList, RootStackParamList } from "../navigation/types"
import { color, styles, typography } from "../theme"

type EditPasswordRouteProp = RouteProp<MyPageStackParamList, "EditPassword">

type EditPasswordNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "EditPassword">,
  CompositeNavigationProp<
    StackNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface EditPasswordScreenProps {
  navigation: EditPasswordNavigationProp
  route: EditPasswordRouteProp
}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingTop: 16,
  paddingBottom: 12,
  paddingHorizontal: 16,
}
const FORM_ITEM: ViewStyle = {
  marginTop: 24,
  marginLeft: 16,
  marginRight: 16,
}
const LABEL: TextStyle = {
  width: 110,
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  paddingRight: 0,
}
const INPUT = { fontFamily: typography.bold, fontSize: 16, color: color.text }
const TITLE = {
  fontFamily: typography.bold,
  fontSize: 20,
  lineHeight: 30,
  color: color.text,
  marginLeft: 16,
}
const BUTTON_TEXT = { fontFamily: typography.bold, fontSize: 17, color: color.palette.white }
const BOTTOM_BUTTONS: ViewStyle = { flex: 1, justifyContent: "flex-end" }

export const EditPasswordScreen: React.FunctionComponent<EditPasswordScreenProps> = observer(
  (props) => {
    const { accountStore } = useStores()
    const { navigation } = props

    const [prePassword, setPrePassword] = useState("")
    const [newPassword1, setNewPassword1] = useState("")
    const [newPassword2, setNewPassword2] = useState("")

    const changePassword = async () => {
      if (newPassword1 != newPassword2) {
        Alert.alert(
          "",
          "입력한 비밀번호가 서로 다릅니다.",
          [{ text: "확인", onPress: () => null }],
          { cancelable: false },
        )
        return
      }

      const params = {
        new_password1: newPassword1.trim(),
        new_password2: newPassword2.trim(),
        old_password: prePassword.trim(),
      }
      // start making calls
      const response: ApiResponse<any> = await api.apisauce.post(
        `/rest-auth/password/change/`,
        params,
      )
      if (response.ok) {
        Alert.alert(
          "",
          "비밀번호를 변경하였습니다.",
          [
            {
              text: "확인",
              onPress: () => {
                navigation.pop()
              },
            },
          ],
          { cancelable: false },
        )
      } else if (response.data.new_password1) {
        Alert.alert(
          "",
          response.data.new_password1[0],
          [
            {
              text: "확인",
              onPress: () => {
                setNewPassword1("")
                setNewPassword2("")
              },
            },
          ],
          { cancelable: false },
        )
      } else if (response.data.new_password2) {
        Alert.alert(
          "",
          response.data.new_password2[0],
          [
            {
              text: "확인",
              onPress: () => {
                setNewPassword1("")
                setNewPassword2("")
              },
            },
          ],
          { cancelable: false },
        )
      } else if (response.data.old_password) {
        Alert.alert(
          "",
          "입력하신 기존 비밀번호가 유효하지 않습니다.",
          [
            {
              text: "확인",
              onPress: () => {
                setPrePassword("")
                setNewPassword1("")
                setNewPassword2("")
              },
            },
          ],
          { cancelable: false },
        )
      } else {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
          onCloseComplete: () => navigation.pop(),
        })
      }
    }

    return (
      <Screen style={ROOT} preset="fixed" keyboardOffset="bottomButton" statusBar="dark-content">
        <Text style={TITLE}>비밀번호 변경</Text>
        <Stack inlineLabel last style={FORM_ITEM}>
          <FormControl.Label style={LABEL}>기존 비밀번호</FormControl.Label>
          <Input
            autoCapitalize="none"
            placeholder={"기존 비밀번호"}
            placeholderTextColor={color.placeholder}
            selectionColor={color.primary}
            onChangeText={(text) => setPrePassword(text)}
            secureTextEntry={true}
            style={INPUT}
          />
        </Stack>
        <Stack inlineLabel last style={FORM_ITEM}>
          <FormControl.Label style={LABEL}>새 비밀번호</FormControl.Label>
          <Input
            autoCapitalize="none"
            placeholder={"새 비밀번호"}
            placeholderTextColor={color.placeholder}
            selectionColor={color.primary}
            value={newPassword1}
            onChangeText={(text) => setNewPassword1(text)}
            secureTextEntry={true}
            style={INPUT}
          />
        </Stack>
        <Stack inlineLabel last style={FORM_ITEM}>
          <FormControl.Label style={LABEL}>다시 입력</FormControl.Label>
          <Input
            autoCapitalize="none"
            placeholder={"다시 입력"}
            placeholderTextColor={color.placeholder}
            selectionColor={color.primary}
            value={newPassword2}
            onChangeText={(text) => setNewPassword2(text)}
            secureTextEntry={true}
            style={INPUT}
          />
        </Stack>
        {prePassword.length > 0 && newPassword1.length > 0 && newPassword2.length > 0 ? (
          <View style={BOTTOM_BUTTONS}>
            <Button block onPress={changePassword}>
              <Text style={BUTTON_TEXT}>확인</Text>
            </Button>
          </View>
        ) : null}
      </Screen>
    )
  },
)
