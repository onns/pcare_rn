import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Toast, View } from "native-base"
import React from "react"
import { ScrollView, TextStyle, TouchableOpacity, ViewStyle } from "react-native"

import { api } from "@api/api"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgArrowRight from "../assets/images/arrowRight.svg"
import { getPopupButton, Popup } from "../components/Popup"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { formatDateWithJoinChar } from "../lib/DateUtil"
import {
  AuthStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, styles, typography } from "../theme"

const CONTENT_ROW1: ViewStyle = {
  flex: 1,
  backgroundColor: "#fff",
  paddingLeft: 16,
  paddingRight: 16,
  borderBottomWidth: 1,
  borderBottomColor: color.palette.pale,
}
const CONTENT_ROW2: ViewStyle = {
  flex: 1,
  backgroundColor: "#fff",
  marginTop: 9,
  paddingLeft: 16,
  paddingRight: 16,
  borderTopWidth: 1,
  borderTopColor: color.palette.pale,
  borderBottomWidth: 1,
  borderBottomColor: color.palette.pale,
}
const ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const SETTING_ROW: ViewStyle = {
  ...ROW,
  paddingTop: 8,
  paddingBottom: 8,
}
const ANSWER_BUTTON: ViewStyle = {
  borderColor: color.palette.veryLightPink3,
  marginBottom: 10,
}
const BUTTON: ViewStyle = {
  ...ANSWER_BUTTON,
  backgroundColor: color.palette.veryLightPink3,
}
const WITHDRAW_BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  color: color.text,
  opacity: 0.5,
}
const AGREE_BUTTON_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 17,
  color: color.palette.white,
}
const PROPERTY_NAME = {
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 19,
  color: color.text,
}
const FLEX_END: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "flex-end",
  alignItems: "center",
}

type MarketingAgreementRouteProp = RouteProp<MyPageStackParamList, "MarketingAgreement">

type MarketingAgreementNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "MarketingAgreement">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AuthStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: MarketingAgreementNavigationProp
  route: MarketingAgreementRouteProp
}

interface State {
  pinCode: undefined | string
  pinCodeEnabled: boolean
  pushNotiEnabled: boolean
  durNotiEnabled: boolean
  isModalVisible: boolean
  modalContentType: "preg" | "byg" | "hng" | "dis" | "nosale"
}

@inject("accountStore")
@observer
export default class MarketingAgreementScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "마케팅 정보 수신 및 이용 동의",
  }

  _unsubscribeFocus: () => void

  constructor(props: Props) {
    super(props)

    this.state = {
      pinCode: undefined,
      pinCodeEnabled: false,
      pushNotiEnabled: false,
      durNotiEnabled: false,
      isModalVisible: false,
      modalContentType: "preg",
    }
  }

  componentDidMount() {
    this._unsubscribeFocus = this.props.navigation.addListener("focus", this.didFocus)
  }

  didFocus = async () => {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })

    const pinCode = await AsyncStorage.getItem("pinCode")
    const pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")
    const pushNotiEnabled = this.props.accountStore.user.presc_noti === "y"
    const durNotiEnabled = this.props.accountStore.user.dur_noti === "y"
    this.setState({
      pinCode: pinCode,
      pinCodeEnabled: JSON.parse(pinCodeEnabled),
      pushNotiEnabled: pushNotiEnabled,
      durNotiEnabled: durNotiEnabled,
    })
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
  }

  async updateMarketingAgreement(agreed: boolean) {
    const user = this.props.accountStore.user
    const { navigation } = this.props

    const params = new FormData()
    params.append("user", user.id)
    params.append("ad_noti", agreed ? "y" : "n")
    params.append("ad_noti_date", formatDateWithJoinChar(new Date(), "-"))
    params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
    const result = await api.updateProfile(user.id, params)
    if (result.kind == "ok") {
      if (!agreed) {
        this.closeModal()
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`마케팅 정보 수신 및 이용 동의를 철회하였습니다.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => {
          user.setAdNoti(agreed ? "y" : "n")
          user.setAdNotiDate(new Date())
        }, 200)
        navigation.goBack()
      } else {
        this.openModal()
      }
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => navigation.navigate("Auth"), 2000)
    }
  }

  render() {
    const user = this.props.accountStore.user
    return (
      <View style={{ flex: 1, backgroundColor: color.background }}>
        <ScrollView>
          <View style={CONTENT_ROW1}>
            <TouchableOpacity
              style={[SETTING_ROW, { marginVertical: 5 }]}
              onPress={() =>
                this.props.navigation.navigate("WebView", {
                  url: "https://web.papricacare.com/agrees/marketing-info-agreement.html",
                })
              }
            >
              <Text style={PROPERTY_NAME}>마케팅 정보 수신 및 이용 동의</Text>
              <View style={FLEX_END}>
                <SvgArrowRight width={24} height={24} color={color.palette.grey30} />
              </View>
            </TouchableOpacity>
          </View>
          {user.ad_noti === "y" ? this.renderWithdrawAgreement() : this.renderAgreement()}
        </ScrollView>
      </View>
    )
  }

  renderAgreement() {
    return (
      <View style={CONTENT_ROW2}>
        <View style={{ marginTop: 19, marginBottom: 16 }}>
          <Text style={PROPERTY_NAME}>동의를 하시면</Text>
          <Text
            style={{
              fontFamily: typography.primary,
              fontSize: 13,
              lineHeight: 19,
              color: color.text,
              marginTop: 12,
              marginBottom: 24,
            }}
          >
            파프리카케어에서 제공하는 특별한 혜택 및 이벤트, 사은품 등에 대한 안내와 함께 차별화된
            서비스로 회원님께 다가갑니다.
          </Text>
          <Button block onPress={() => this.updateMarketingAgreement(true)}>
            <Text style={AGREE_BUTTON_TEXT}>동의</Text>
          </Button>
        </View>
        <Popup
          isVisible={this.state.isModalVisible}
          title={"마케팅 정보 수신에 동의하셨습니다."}
          message={`소중한 기회를 주신 회원님께 감사합니다. 특별한 혜택과 서비스로 회원님께 다가가겠습니다.`}
          button1={getPopupButton(() => {
            this.closeModal()
            const { accountStore, navigation } = this.props
            const { user } = accountStore
            user.setAdNoti("y")
            user.setAdNotiDate(new Date())
            navigation.goBack()
          }, "확인")}
        />
      </View>
    )
  }

  renderWithdrawAgreement() {
    return (
      <View style={CONTENT_ROW2}>
        <View style={{ marginTop: 19, marginBottom: 16 }}>
          <Text style={PROPERTY_NAME}>동의 철회를 원하시나요?</Text>
          <Text
            style={{
              fontFamily: typography.primary,
              fontSize: 13,
              lineHeight: 19,
              color: color.text,
              marginTop: 12,
              marginBottom: 24,
            }}
          >
            동의 철회 시 파프리카케어에서 제공하는 특별한 해택 및 이벤트, 사은품등에 대한 안내를
            더이상 받을 수 없습니다.
          </Text>
          <Button bordered block onPress={() => this.openModal()} style={BUTTON}>
            <Text style={WITHDRAW_BUTTON_TEXT}>동의 철회</Text>
          </Button>
        </View>
        <Popup
          isVisible={this.state.isModalVisible}
          title={"정말로 동의를 철회하시겠습니까?"}
          message={`마케팅 정보 수신 및 이용 동의 철회 시 더이상 파프리카케어에서 제공하는 혜택 및 이벤트에 대한 안내를 받으실 수 없습니다.`}
          button1={getPopupButton(() => this.updateMarketingAgreement(false), "철회하기", "cancel")}
          button2={getPopupButton(() => this.closeModal(), "취소")}
        />
      </View>
    )
  }

  openModal() {
    this.setState({ isModalVisible: true })
  }

  closeModal() {
    this.setState({ isModalVisible: false })
  }

  onDurHngChange(): void {
    const user = this.props.accountStore.user
    user.setDurHng(user.dur_hng == "y" ? "n" : "y")
    this.updateProfile()
  }

  onDurBygChange(): void {
    const user = this.props.accountStore.user
    user.setDurByg(user.dur_byg == "y" ? "n" : "y")
    this.updateProfile()
  }

  onDurNosaleChange(): void {
    const user = this.props.accountStore.user
    user.setDurNosale(user.dur_nosale == "y" ? "n" : "y")
    this.updateProfile()
  }

  onDurDisChange(): void {
    const user = this.props.accountStore.user
    user.setDurDis(user.dur_dis == "y" ? "n" : "y")
    this.updateProfile()
  }

  onDurPregChange(): void {
    const user = this.props.accountStore.user
    user.setDurPreg(user.dur_preg == "y" ? "n" : "y")
    this.updateProfile()
  }

  async updateProfile() {
    const user = this.props.accountStore.user
    const params = new FormData()
    params.append("user", user.id)
    params.append("presc_noti", user.presc_noti)
    params.append("dur_noti", user.dur_noti)
    params.append("dur_hng", user.dur_hng)
    params.append("dur_byg", user.dur_byg)
    params.append("dur_dis", user.dur_dis)
    params.append("dur_preg", user.dur_preg)
    params.append("dur_nosale", user.dur_nosale)
    params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
    const result = await api.updateProfile(user.id, params)
    if (result.kind == "ok") {
      // user.setFirebaseToken0("")
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    }
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }
}
