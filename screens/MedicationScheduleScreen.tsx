import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { observer } from "mobx-react-lite"
import { onAction } from "mobx-state-tree"
import { Spinner } from "native-base"
import React, { useEffect, useState } from "react"
import { Platform, ScrollView, StatusBar, TextStyle, View, ViewStyle } from "react-native"
import { Calendar, LocaleConfig } from "react-native-calendars"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { withNavigationFocus } from "@react-navigation/compat"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import SvgArrowDownBlack from "../assets/images/arrowDownBlack.svg"
import Day from "../components/day/day"
import { MemberSelectHeader } from "../components/MemberSelectHeader"
import PrescriptionReport from "../components/PrescriptionReport"
import { Screen } from "../components/screen/screen"
import {
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
  TakingDrugStackParamList,
} from "../navigation/types"
import { MedicationProgress } from "../stores/drug-to-take/MedicationProgress"
import { MedicationSchedule } from "../stores/drug-to-take/MedicationSchedule"
import { FamilyMember } from "../stores/FamilyMember"
import { color, typography } from "../theme"

type MedicationScheduleScreenRouteProp = RouteProp<TakingDrugStackParamList, "MedicationSchedule">

type MedicationScheduleScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<TakingDrugStackParamList, "MedicationSchedule">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<HomeStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface MedicationScheduleScreenProps {
  navigation: MedicationScheduleScreenNavigationProp
  route: MedicationScheduleScreenRouteProp
}

const ROOT: ViewStyle = { backgroundColor: color.background }
const CONTENT: ViewStyle = {
  paddingHorizontal: 7,
  marginBottom: 44,
  backgroundColor: color.palette.white,
}
const CALENDAR_THEME = {
  "stylesheet.calendar.header": {
    dayHeader: {
      marginTop: 2,
      marginBottom: 7,
      width: 32,
      textAlign: "center",
      fontSize: 13,
      fontFamily: typography.primary,
      color: "#33302f80",
    },
    monthText: {
      fontSize: 20,
      fontFamily: Platform.select({
        ios: "NotoSansKR-Bold",
        android: "NotoSansKR-Bold-Hestia",
      }),
      color: "#272727",
      margin: 10,
    },
  },
}
const BUTTON_CONTAINER: ViewStyle = {
  flexDirection: "row",
  alignSelf: "center",
  alignContent: "space-between",
  width: 343,
  height: 40,
  marginTop: 20,
  marginBottom: 20,
  borderRadius: 35,
  backgroundColor: color.palette.veryLightPink3,
}
const ALL_BUTTON: ViewStyle = {
  width: 114,
  height: 40,
  borderRadius: 35,
  alignItems: "center",
  justifyContent: "center",
  alignContent: "center",
}
const DRUG_BUTTON: ViewStyle = {
  width: 114,
  height: 40,
  borderRadius: 35,
  alignItems: "center",
  justifyContent: "center",
  alignContent: "center",
}
const HEALTH_BUTTON: ViewStyle = {
  width: 114,
  height: 40,
  borderRadius: 35,
  alignItems: "center",
  justifyContent: "center",
}
const BUTTON_STYLE: TextStyle = {
  ...typography.sub1,
  color: color.palette.grey50,
}
const ACTIVE_BTN: ViewStyle = {
  backgroundColor: color.palette.orange,
}
const DEACTIVE_BTN: ViewStyle = {
  backgroundColor: color.palette.veryLightPink3,
}
const ACTIVE_TEXT: TextStyle = {
  color: "white",
}
const DEACTIVE_TEXT: TextStyle = {
  color: color.palette.grey50,
}

LocaleConfig.locales.kr = {
  monthNames: [
    "1월",
    "2월",
    "3월",
    "4월",
    "5월",
    "6월",
    "7월",
    "8월",
    "9월",
    "10월",
    "11월",
    "12월",
  ],
  monthNamesShort: [
    "1월",
    "2월",
    "3월",
    "4월",
    "5월",
    "6월",
    "7월",
    "8월",
    "9월",
    "10월",
    "11월",
    "12월",
  ],
  dayNames: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
  dayNamesShort: ["일", "월", "화", "수", "목", "금", "토"],
  today: "오늘",
}
LocaleConfig.defaultLocale = "kr"

const MedicationScheduleScreen: React.FunctionComponent<MedicationScheduleScreenProps> = observer(
  (props) => {
    const { accountStore, prescriptionStore, takingDrugStore } = useStores()
    const { navigation, route } = props
    const [viewedCalendarDate, setViewedCalendarDate] = useState(DateTime.local())
    const [allBtn, setAllBtn] = useState(true)
    const [myDrugBtn, setMyDrugBtn] = useState(false)
    const [healthBtn, setHealthBtn] = useState(false)

    let minStartDate: DateTime = DateTime.local().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    })
    let maxEndDate: DateTime = DateTime.local().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    })
    const currentDate = DateTime.local().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    })

    // const activeAllBtn = () => {
    //   setAllBtn(true)
    //   setMyDrugBtn(false)
    //   setHealthBtn(false)
    // }

    // const activeMyDrugBtn = () => {
    //   setAllBtn(false)
    //   setMyDrugBtn(true)
    //   setHealthBtn(false)
    // }

    // const activeHealthBtn = () => {
    //   setAllBtn(false)
    //   setMyDrugBtn(false)
    //   setHealthBtn(true)
    // }

    React.useEffect(() => {
      const unsubscribe = navigation.addListener("focus", () => {
        firebase.analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        })
        amplitude.getInstance().logEvent("screen_view", { screen: route.name })

        setViewedCalendarDate(DateTime.local())
        fetchSchedule()
      })
      return unsubscribe
    }, [navigation])

    const fetchSchedule = () => {
      const { selectedMemberKey, user } = accountStore
      if (!selectedMemberKey || takingDrugStore.isLoading) return

      if (selectedMemberKey.includes("_u")) {
        takingDrugStore.fetchMedicationSchedule(
          user.id,
          selectedMemberKey,
          undefined,
          true,
          viewedCalendarDate.toFormat("yyyy-MM"),
        )
      } else if (selectedMemberKey.includes("_c")) {
        takingDrugStore.fetchMedicationSchedule(
          user.id,
          selectedMemberKey,
          Number(selectedMemberKey.substr(2)),
          true,
          viewedCalendarDate.toFormat("yyyy-MM"),
        )
      } else {
        takingDrugStore.fetchMedicationSchedule(
          Number(selectedMemberKey.substr(2)),
          selectedMemberKey,
          undefined,
          true,
          viewedCalendarDate.toFormat("yyyy-MM"),
        )
      }
    }

    useEffect(() => {
      fetchSchedule()
    }, [accountStore.selectedMemberKey])

    /**
     * 처방전 삭제, 추가되었을 때에 바로 캘린더에 반영하기 위함.
     * 복약관리 화면에 진입할 때 처방전 데이터가 없는 상태에서는 첫번 째 useEffect 와 함께 실행되어
     * fetchMedicationSchedule 이 두 번 실행될 수 있음.
     * 따라서 이곳에서 실행되는 fetchMedicationSchedule 에는 spinner 를 표시하지 않도록 옵션을 설정함.
     * */
    useEffect(() => {
      const actionDisposer = onAction(prescriptionStore, (call) => {
        // setStatus("done") action 도 받으면 불필요하게 fetch 를 빈번하게 호출하게 되어 삭제함.
        if (call.name == "setStatusOfPrescriptions" && call.args[1] === "done") {
          fetchSchedule()
        }
      })

      return () => actionDisposer()
    }, [])

    const onSelectMember = (memberKey: string, member?: FamilyMember) => {}

    const generatePeriods = (date: DateTime, schedules: MedicationSchedule[]) => {
      const arr = []
      schedules.forEach((schedule) => {
        if (date.equals(schedule.start)) {
          arr.push({
            color: color.palette.greenishTeal,
            startingDay: true,
            diseaseName: schedule.disease?.rep_title,
            issueDate: `${schedule.issue_date.toFormat("M월 d일")} 처방`,
          })
        } else if (schedule.start < date && date < schedule.end) {
          arr.push({
            color: color.palette.greenishTeal,
            diseaseName: schedule.disease?.rep_title,
            issueDate: `${schedule.issue_date.toFormat("M월 d일")} 처방`,
          })
        } else if (date.equals(schedule.end)) {
          arr.push({
            color: color.palette.greenishTeal,
            endingDay: true,
            diseaseName: schedule.disease?.rep_title,
            issueDate: `${schedule.issue_date.toFormat("M월 d일")} 처방`,
          })
        }
      })
      return arr
    }

    const getMarkedDates = (schedules: MedicationSchedule[], progress: MedicationProgress[]) => {
      if (schedules.length == 0) {
        return null
      }

      let arr = []
      // 가장 빠른 시작일 구하기
      for (let i = 0; i < schedules.length; i++) {
        const schedule = schedules[i]
        if (i == 0) {
          minStartDate = schedule.start
          continue
        }
        if (minStartDate > schedule.start) {
          minStartDate = schedule.start
        }
      }

      // 가장 늦는 마감일 구하기
      for (let i = 0; i < schedules.length; i++) {
        const schedule = schedules[i]
        if (i == 0) {
          maxEndDate = schedule.end
          continue
        }
        if (maxEndDate < schedule.end) {
          maxEndDate = schedule.end
        }
      }

      // 빠른 시작일 순서로 스케줄 정렬
      const sortedSchedules = schedules.slice()
      sortedSchedules.sort((a, b) => {
        // 0보다 작은 경우 a를 b보다 낮은 색인으로 정렬 (a가 먼저옴)
        if (a.start < b.start) {
          return -1
        } else if (a.start.equals(b.start)) {
          return 0
        } else {
          return 1
        }
      })

      const firstProgressDate = DateTime.fromSQL(progress[0]?.date)
      const lastProgressDate = DateTime.fromSQL(progress[progress.length - 1]?.date)
      const minResultDate = firstProgressDate < minStartDate ? firstProgressDate : minStartDate
      const maxResultDate = lastProgressDate < maxEndDate ? maxEndDate : lastProgressDate
      let progressIndex = 0

      for (let date = minResultDate; date <= maxResultDate; ) {
        if (
          progress.length > progressIndex &&
          DateTime.fromSQL(progress[progressIndex].date).toMillis() == date.toMillis()
        ) {
          date < currentDate
            ? progress[progressIndex].ratio > 0.9
              ? arr.push({
                  // 복약 시작일부터 체크
                  [date.toSQLDate()]: {
                    periods: generatePeriods(date, sortedSchedules),
                    selected: true,
                  },
                })
              : arr.push({
                  // 복약 시작일부터 체크
                  [date.toSQLDate()]: {
                    periods: generatePeriods(date, sortedSchedules),
                    unTaken: true,
                  },
                })
            : arr.push({
                // 미래 복약 날짜는 미선택 표시
                [date.toSQLDate()]: {
                  periods: generatePeriods(date, sortedSchedules),
                },
              })
          progressIndex++
        } else {
          arr.push({
            // 복약 시작일부터 체크
            [date.toSQLDate()]: {
              periods: generatePeriods(date, sortedSchedules),
            },
          })
        }
        date = date.plus({ days: 1 })
      }

      // slicing curly brackets
      arr = arr.map((a) => {
        return JSON.stringify(a).slice(1, -1)
      })

      const arrString = arr.join(",")
      return JSON.parse("{" + arrString + "}")
    }

    const onPressFetchSchedule = (array) => {
      const { selectedMemberKey } = accountStore
      if (selectedMemberKey.includes("_u")) {
        takingDrugStore.fetchMedicationSchedule(
          accountStore.user.id,
          selectedMemberKey,
          undefined,
          true,
          array.toFormat("yyyy-MM"),
        )
      } else if (selectedMemberKey.includes("_c")) {
        takingDrugStore.fetchMedicationSchedule(
          accountStore.user.id,
          selectedMemberKey,
          Number(selectedMemberKey.substr(2)),
          true, // no spinner
          array.toFormat("yyyy-MM"),
        )
      } else {
        takingDrugStore.fetchMedicationSchedule(
          Number(selectedMemberKey.substr(2)),
          selectedMemberKey,
          undefined,
          true,
          array.toFormat("yyyy-MM"),
        )
      }
    }

    return (
      <Screen style={ROOT} preset="fixed">
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <MemberSelectHeader onSelectMember={onSelectMember} />
        {takingDrugStore.isLoading ? (
          <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
        ) : (
          <ScrollView contentContainerStyle={CONTENT}>
            {/* 최상단 탭 메뉴 */}
            {/* <View style={BUTTON_CONTAINER}>
              <TouchableOpacity
                style={[ALL_BUTTON, allBtn ? ACTIVE_BTN : DEACTIVE_BTN]}
                onPress={activeAllBtn}
              >
                <Text style={[BUTTON_STYLE, allBtn ? ACTIVE_TEXT : DEACTIVE_TEXT]}>전체</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[DRUG_BUTTON, myDrugBtn ? ACTIVE_BTN : DEACTIVE_BTN]}
                onPress={activeMyDrugBtn}
              >
                <Text style={[BUTTON_STYLE, myDrugBtn ? ACTIVE_TEXT : DEACTIVE_TEXT]}>처방약</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[HEALTH_BUTTON, healthBtn ? ACTIVE_BTN : DEACTIVE_BTN]}
                onPress={activeHealthBtn}
              >
                <Text style={[BUTTON_STYLE, healthBtn ? ACTIVE_TEXT : DEACTIVE_TEXT]}>
                  건강기능식품
                </Text>
              </TouchableOpacity>
            </View> */}
            {allBtn || myDrugBtn ? (
              <Calendar
                current={viewedCalendarDate}
                style={{ paddingBottom: 30 }}
                // minDate={calendarMinDate?.toFormat("yyyy-MM-dd")}
                // maxDate={calendarMaxDate}
                calendarHeight={360 + 35 * 4 * takingDrugStore.medicationSchedule.length}
                maxDate={DateTime.local().toFormat("yyyy-MM-dd")}
                onDayPress={(day) => {
                  console.tron.log("< MedicationScheduleScreen > onDayPress, day: ", day)
                }}
                onDayLongPress={(day) => {
                  console.tron.log("< MedicationScheduleScreen > onDayLongPress, day: ", day)
                }}
                monthFormat={"yyyy년 MM월"}
                renderArrow={(direction) => {
                  if (direction == "left") {
                    return (
                      <SvgArrowDownBlack
                        width={24}
                        height={24}
                        style={{
                          transform: [{ rotate: "90deg" }],
                        }}
                      />
                    )
                  }
                  if (direction == "right") {
                    return (
                      <SvgArrowDownBlack
                        width={24}
                        height={24}
                        style={{
                          transform: [{ rotate: "-90deg" }],
                        }}
                      />
                    )
                  }
                  return <View style={{ width: 24 }} />
                }}
                hideExtraDays={true}
                disableMonthChange={true}
                onPressArrowLeft={(subtractMonth) => {
                  const firstOfPrevMonth = viewedCalendarDate.minus({ months: 1 }).set({ day: 1 })
                  setViewedCalendarDate(firstOfPrevMonth)
                  onPressFetchSchedule(firstOfPrevMonth)
                  subtractMonth()
                }}
                onPressArrowRight={(addMonth) => {
                  let endOfNextMonth = viewedCalendarDate.plus({ months: 1 })
                  endOfNextMonth = endOfNextMonth.set({ day: endOfNextMonth.daysInMonth })
                  setViewedCalendarDate(endOfNextMonth)
                  onPressFetchSchedule(endOfNextMonth)
                  addMonth()
                }}
                theme={CALENDAR_THEME}
                // getMarkedDates(takingDrugStore.medicationSchedule)
                markedDates={getMarkedDates(
                  takingDrugStore.medicationSchedule,
                  takingDrugStore.medicationProgress,
                )}
                dayComponent={({ date, state, onPress, onLongPress, marking, dayOfWeek }) => (
                  <Day
                    date={date}
                    dayOfWeek={dayOfWeek}
                    state={state}
                    onPress={() => {
                      if (
                        minStartDate <= DateTime.fromSQL(date.dateString) &&
                        DateTime.fromSQL(date.dateString) <= maxEndDate
                      ) {
                        return navigation.navigate("MedicationScheduleDetail", { date: date })
                      } else {
                        return ""
                      }
                    }}
                    onLongPress={() => {
                      if (
                        minStartDate <= DateTime.fromSQL(date.dateString) &&
                        DateTime.fromSQL(date.dateString) <= maxEndDate
                      ) {
                        navigation.navigate("MedicationScheduleDetail", { date: date })
                      }
                    }}
                    marking={marking}
                  >
                    {date.day}
                  </Day>
                )}
              />
            ) : null}
            <PrescriptionReport />
          </ScrollView>
        )}
      </Screen>
    )
  },
)

export default withNavigationFocus(MedicationScheduleScreen)
