import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content } from "native-base"
import React, { Component } from "react"
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Route } from "react-native-tab-view"

import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import DailyFeelinNote from "../components/todayfeelin/DailyFeelinNote"
import {
  DetailsStackParamList,
  HealthTemplateStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  paddingTop: 10,
  backgroundColor: color.backgrounds.lightmode,
}

type MyFeelinNotesRouteProp = RouteProp<HealthTemplateStackParamList, "MyFeelinNotes">

export type MyFeelinNotesNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HealthTemplateStackParamList, "MyFeelinNotes">,
  CompositeNavigationProp<
    StackNavigationProp<HealthTemplateStackParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

/**
 * Props, 컴포넌트에서 직접 사용가능함
 */
export interface Props {
  accountStore: AccountStore
  navigation: MyFeelinNotesNavigationProp
  route: MyFeelinNotesRouteProp
}

/**
 * State, 컴포넌트에서 생성되며 setState로 변경가능
 */
interface State {
  routes: Array<Route>
}

@inject("accountStore")
@observer
export default class MyFeelinNotesScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      routes: [{ key: "healthTemplate", title: "오늘의 컨디션" }],
    }
  }

  /**
   * Header
   */
  static navigationOptions = {
    title: "오늘의 컨디션",
  }

  async componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })

    const { noticeList } = this.props.accountStore
    const lastNoticeId = await AsyncStorage.getItem("lastNoticeId")
    if (noticeList.length > 0 && lastNoticeId != String(noticeList[0].id)) {
      AsyncStorage.setItem("lastNoticeId", String(noticeList[0].id))
    }
  }

  /** 현재는 소식만 출력 */
  render() {
    return (
      <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
        <DailyFeelinNote />
      </ScrollView>
    )
  }
}
