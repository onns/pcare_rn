import { DateTime, Interval } from "luxon"
import { observer } from "mobx-react-lite"
import { Spinner, Toast } from "native-base"
import React, { useEffect, useLayoutEffect, useState } from "react"
import { FlatList, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import DateTimePicker from "react-native-modal-datetime-picker"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"

import { api } from "@api/api"
import { RouteProp, useFocusEffect } from "@react-navigation/native"

import { useStores } from "../App"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import SvgAfternoon from "../assets/images/timeAfternoonOrange.svg"
import SvgEvening from "../assets/images/timeEveningOrange.svg"
import SvgMorning from "../assets/images/timeMorningOrange.svg"
import SvgNight from "../assets/images/timeNightOrange.svg"
import SvgWakeup from "../assets/images/timeWakeupOrange.svg"
import TouchableItem from "../components/button/TouchableItem"
import CloseButton from "../components/CloseButton"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { getText } from "../components/TimeSlotRow"
import { sendLogScreenView } from "../lib/analytics"
import { formatTime, getTimeString } from "../lib/DateUtil"
import { HomeStackParamList } from "../navigation/types"
import { TimeSlotName } from "../stores/TimeSlot"
import { color, styles, typography } from "../theme"

type TimeSlotSettingsScreenRouteProp = RouteProp<HomeStackParamList, "TimeSlotSettings">

export interface TimeSlotSettingsScreenProps {
  navigation: NativeStackNavigationProp<HomeStackParamList>
  route: TimeSlotSettingsScreenRouteProp
}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
}
const ALARM_: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  paddingHorizontal: 24,
  paddingVertical: 16,
  borderBottomColor: color.line,
  borderBottomWidth: 1,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const TIME_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 20,
  lineHeight: 30,
  paddingLeft: 4,
  paddingRight: 8,
}
const TIME_PREFIX_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
}
const TIME_SLOT_ITEM: ViewStyle = {
  height: 80,
  flexDirection: "row",
  paddingHorizontal: 24,
  justifyContent: "space-between",
  alignItems: "center",
}
const TIME_SLOT_ITEM_LEFT: ViewStyle = {
  width: 80,
  flexDirection: "row",
  alignItems: "center",
}
const ROW_VERT_CENTER: ViewStyle = { ...ROW, alignItems: "center" }
const TIME_SLOT_ITEM_RIGHT: ViewStyle = { minWidth: 58 }
const ITEM_DIVIDER = { height: 1, backgroundColor: color.line, marginHorizontal: 24 }
const ADD_TIME_TEXT: TextStyle = { ...typography.title3, color: color.palette.grey70 }
const ADD_TIME_BUTTON: ViewStyle = {
  minHeight: 56,
  marginHorizontal: 16,
  marginVertical: 12,
  borderWidth: 1,
  borderColor: color.palette.gray6,
  borderRadius: 30,
  justifyContent: "center",
  alignItems: "center",
}
const DELETE_BUTTON: ViewStyle = {
  minWidth: 58,
  minHeight: 32,
  borderWidth: 1,
  borderColor: color.palette.red50,
  borderRadius: 15,
  justifyContent: "center",
  alignItems: "center",
}
const DELETE_TEXT = {
  fontFamily: typography.bold,
  fontSize: 14,
  lineHeight: 21,
  color: color.palette.orange2,
}

function getIcon(slot: TimeSlotName) {
  if (slot === "wakeup") {
    return <SvgWakeup width={24} height={24} />
  } else if (slot === "morning") {
    return <SvgMorning width={24} height={24} />
  } else if (slot === "afternoon") {
    return <SvgAfternoon width={24} height={24} color={color.primary} />
  } else if (slot === "evening") {
    return <SvgEvening width={24} height={24} color={color.primary} />
  } else if (slot === "night") {
    return <SvgNight width={24} height={24} color={color.primary} />
  } else {
    return null
  }
}

function showToastMessage(message: string) {
  Toast.show({
    title: <Text style={styles.TOAST_TEXT}>{message}</Text>,
    duration: 2000,
    placement: "top",
    style: styles.TOAST_VIEW,
  })
}
function showErrorMessage() {
  showToastMessage("서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.")
}
function showImpossibleTimeMessage() {
  showToastMessage(
    "선택하신 시간이 다른 시간대에 속해 있습니다. 시간대를 확인하고 다시 입력해 주세요.",
  )
}

export const TimeSlotSettingsScreen: React.FunctionComponent<TimeSlotSettingsScreenProps> = observer(
  (props) => {
    const { navigation, route } = props
    const { prescriptionStore } = useStores()
    const insets = useSafeAreaInsets()
    // const { status, distinctTimeSlots } = prescriptionStore
    const [alarmEnabled, setAlarmEnabled] = useState(true)
    const [timePickerVisible, setTimePickerVisible] = useState(false)
    const [selectedIndex, setSelectedIndex] = useState(-1)
    const [popupVisible, setPopupVisible] = useState(false)
    const [popupTitle, setPopupTitle] = useState("")
    const [popupMessage, setPopupMessage] = useState<any>("")
    const [popupButton1, setPopupButton1] = useState(<View />)
    const [popupButton2, setPopupButton2] = useState(<View />)
    const [timeSlots, setTimeSlots] = useState([])

    useFocusEffect(
      React.useCallback(() => {
        sendLogScreenView({ screenClass: route.name, screenName: route.name })
      }, []),
    )

    useEffect(() => {
      const distinctTimeSlots = prescriptionStore.timeSlots.filter(
        (slot) => slot.category === "pcare5" || slot.category === "user_created",
      )
      setTimeSlots(distinctTimeSlots)
    }, [])

    useLayoutEffect(() => {
      navigation.setOptions({
        headerLeft: () => <CloseButton onPress={navigation.goBack} style={{ marginLeft: 24 }} />,
        headerTitle: "복약알림 설정",
        headerStyle: { elevation: 1 },
      })
    }, [navigation])

    useEffect(() => {
      if (prescriptionStore.timeSlots.length == 0) {
        prescriptionStore.fetchTimeSlots()
      }
    }, [])

    const onAlarmEnabledChange = (value) => {
      setAlarmEnabled(value)
    }

    const handleTimePicked = async (date: Date) => {
      setTimePickerVisible(false)

      if (selectedIndex != -1) {
        const timeSlot = timeSlots[selectedIndex]
        let availTimeRange: Interval
        let availTimeRange2: Interval
        const pickedDateTime = DateTime.fromJSDate(date)
        const time = getTimeString(pickedDateTime.toJSDate())

        switch (timeSlot.name) {
          case TimeSlotName.WakeUp:
            availTimeRange = DateTime.local()
              .set({ hour: 0, minute: 0 })
              .until(prescriptionStore.morningDateTime)
            availTimeRange2 = prescriptionStore.nightDateTime.until(
              DateTime.local().set({ hour: 24, minute: 0 }),
            )
            if (
              !(
                availTimeRange.contains(pickedDateTime) || availTimeRange2.contains(pickedDateTime)
              ) ||
              timeSlots.find((timeSlot) => timeSlot.when === time) // 같은 시간이 존재하는지 확인
            ) {
              showImpossibleTimeMessage()
              return
            }
            break

          case TimeSlotName.Morning:
            availTimeRange = prescriptionStore.wakeupDateTime.until(
              prescriptionStore.afternoonDateTime,
            )
            if (
              !availTimeRange.contains(pickedDateTime) ||
              timeSlots.find((timeSlot) => timeSlot.when === time)
            ) {
              showImpossibleTimeMessage()
              return
            }
            break

          case TimeSlotName.Afternoon:
            availTimeRange = prescriptionStore.morningDateTime.until(
              prescriptionStore.eveningDateTime,
            )
            if (
              !availTimeRange.contains(pickedDateTime) ||
              timeSlots.find((timeSlot) => timeSlot.when === time)
            ) {
              showImpossibleTimeMessage()
              return
            }
            break

          case TimeSlotName.Evening:
            let dateTime = DateTime.local().set({ hour: 23, minute: 59 }) // 일반적으로 '저녁' 시간을 새벽 시간으로 지정하지는 않을 것이다.
            if (
              prescriptionStore.nightDateTime.hour >= 0 &&
              prescriptionStore.nightDateTime.hour < 12
            ) {
              dateTime = dateTime.plus({ days: 1 })
            } else {
              dateTime = prescriptionStore.nightDateTime
            }
            availTimeRange = prescriptionStore.afternoonDateTime.until(dateTime)
            if (
              !availTimeRange.contains(pickedDateTime) ||
              timeSlots.find((timeSlot) => timeSlot.when === time)
            ) {
              showImpossibleTimeMessage()
              return
            }
            break

          case TimeSlotName.Night:
            availTimeRange = prescriptionStore.eveningDateTime.until(
              DateTime.local().set({ hour: 23, minute: 59 }),
            )
            availTimeRange2 = DateTime.local()
              .set({ hour: 0, minute: 0 })
              .until(prescriptionStore.wakeupDateTime)
            if (
              !(
                availTimeRange.contains(pickedDateTime) || availTimeRange2.contains(pickedDateTime)
              ) ||
              timeSlots.find((timeSlot) => timeSlot.when === time)
            ) {
              showImpossibleTimeMessage()
              return
            }
            break

          default:
            if (distinctTimeSlots.find((timeSlot) => timeSlot.when === time)) {
              showImpossibleTimeMessage()
              return
            }
            break
        }
      } else {
        const pickedDateTime = DateTime.fromJSDate(date)
        const time = await getTimeString(pickedDateTime.toJSDate())
        const duplication = await timeSlots.find((timeSlot) => timeSlot.when === time)

        if (duplication) {
          return showImpossibleTimeMessage()
        }
      }

      const params = new FormData()
      params.append("when", getTimeString(date))

      prescriptionStore.setStatus("pending")
      if (selectedIndex === -1) {
        params.append("name", "custom")
        const result = await api.addTimeSlot(params)
        if (result.kind === "ok") {
          prescriptionStore.fetchTimeSlots()
        } else {
          showErrorMessage()
        }
      } else {
        const timeSlotId = timeSlots[selectedIndex].id
        const result = await api.updateTimeSlot(timeSlotId, params)
        if (result.kind === "ok") {
          timeSlots[selectedIndex].setWhen(result.data.when)
        } else {
          showErrorMessage()
        }
      }
      prescriptionStore.setStatus("done")
    }

    const onItemPress = (index: number) => {
      setSelectedIndex(index)
      setTimePickerVisible(true)
    }

    const onAddTimePress = () => {
      setSelectedIndex(-1)
      setTimePickerVisible(true)
    }

    const removeTimeSlotSetting = async (id: number) => {
      const reqParams = {
        id: id,
      }
      const result = await api.deleteTimeSlot(reqParams)
      setPopupVisible(false)

      if (result.kind === "ok") {
        prescriptionStore.removeTimeSlot(id)
      } else if (result.kind === "rejected") {
        setPopupTitle("해당 알림 시간은 현재 사용중입니다.")
        setPopupMessage("현재 사용중인 알림 시간은 삭제가 불가합니다.")
        setPopupButton1(getPopupButton(() => setPopupVisible(false), "확인"))
        setPopupButton2(null)
        setPopupVisible(true)
      } else {
        showErrorMessage()
      }
    }

    const onDeletePress = (id: number) => {
      setPopupTitle("정말로 알림 시간을 삭제하시겠습니까?")
      setPopupMessage(null)
      setPopupButton2(getPopupButton(() => removeTimeSlotSetting(id), "삭제"))
      setPopupButton1(getPopupButton(() => setPopupVisible(false), "취소", "cancel"))
      setPopupVisible(true)
    }

    const renderTime = (item) => {
      const time = formatTime(item.when)
      return (
        <View style={ROW_VERT_CENTER}>
          <Text
            style={{
              ...TIME_PREFIX_TEXT,
              color: alarmEnabled ? color.palette.black : color.palette.grey30,
            }}
          >
            {time[0]}
          </Text>
          <Text
            style={{
              ...TIME_TEXT,
              color: alarmEnabled ? color.palette.black : color.palette.grey30,
            }}
          >
            {time[1]}
          </Text>
        </View>
      )
    }

    const renderSpinner = () => (
      <Spinner
        color={color.primary}
        size="small"
        style={{ flex: 1, justifyContent: "center", alignSelf: "center" }}
      />
    )

    return (
      <Screen style={ROOT} preset="fixed" unsafe statusBar="dark-content">
        {/* <View style={ALARM_}>
          <Text style={typography.body}>알림받기</Text>
          <Switch
            trackColor={{
              false: color.palette.gray4,
              true: color.palette.orange,
            }}
            thumbColor={color.palette.white}
            ios_backgroundColor={color.palette.gray4}
            onValueChange={onAlarmEnabledChange}
            value={alarmEnabled}
          />
        </View> */}
        <FlatList
          data={timeSlots}
          renderItem={({ item, index }) => (
            <View style={TIME_SLOT_ITEM}>
              <View style={TIME_SLOT_ITEM_LEFT}>
                {getIcon(item.name)}
                <Text style={{ ...typography.sub2, marginLeft: 4 }}>{getText(item.name)}</Text>
              </View>
              {prescriptionStore.isLoading && selectedIndex === index ? (
                renderSpinner()
              ) : (
                <TouchableOpacity onPress={() => onItemPress(index)}>
                  <View style={ROW_VERT_CENTER}>
                    {renderTime(item)}
                    <SvgArrowRight width={24} height={24} color={color.primary} />
                  </View>
                </TouchableOpacity>
              )}
              <View style={TIME_SLOT_ITEM_RIGHT}>
                {item.name === "custom" ? (
                  <TouchableItem onPress={() => onDeletePress(item.id)} style={DELETE_BUTTON}>
                    <Text style={DELETE_TEXT}>삭제</Text>
                  </TouchableItem>
                ) : null}
              </View>
            </View>
          )}
          ItemSeparatorComponent={() => <View style={ITEM_DIVIDER} />}
          refreshing={prescriptionStore.isLoading}
        />
        <View style={{ paddingBottom: insets.bottom }}>
          <TouchableItem onPress={onAddTimePress} style={ADD_TIME_BUTTON}>
            <Text style={ADD_TIME_TEXT}>시간 추가</Text>
          </TouchableItem>
        </View>

        <DateTimePicker
          isVisible={timePickerVisible}
          testID="dateTimePicker"
          date={
            selectedIndex === -1
              ? new Date()
              : DateTime.fromSQL(`${timeSlots[selectedIndex].when}`).toJSDate()
          }
          mode="time"
          display="spinner"
          minuteInterval={10}
          cancelTextIOS="취소"
          confirmTextIOS="확인"
          customHeaderIOS={() => <View />}
          onConfirm={handleTimePicked}
          onCancel={() => setTimePickerVisible(false)}
        />
        <Popup
          isVisible={popupVisible}
          title={popupTitle}
          message={popupMessage}
          button1={popupButton1}
          button2={popupButton2}
        />
      </Screen>
    )
  },
)
