import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Toast, View } from "native-base"
import React, { Component } from "react"
import {
  BackHandler,
  Dimensions,
  FlatList,
  Image,
  ImageStyle,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
// import RNBootSplash from "react-native-bootsplash"
import FastImage from "react-native-fast-image"
import LinearGradient from "react-native-linear-gradient"

import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { ParamListBase, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { BackgroundImage } from "../components/BackgroundImage"
import CloseButton from "../components/CloseButton"
import { getPopupButton, Popup } from "../components/Popup"
import { Text } from "../components/StyledText"
import { MyPageStackParamList } from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { styles, typography } from "../theme"

enum Status {
  Current,
  First,
  Second,
  NewFirst,
  NewSecond,
}

export interface Props {
  accountStore: AccountStore
  navigation: StackNavigationProp<ParamListBase>
  route: RouteProp<MyPageStackParamList, "CodeVerification">
}

interface State {
  code: string
  code2: string
  digitDisabled: boolean
  clearDisabled: boolean
  isPopupVisible: boolean
  status: Status
  purpose: "initial" | "change" | "verification"
  preRoute: "Settings" | "AuthLoading" | "Welcome" | "Root"
}

const arrayOfNumbers = [
  { key: 1 },
  { key: 2 },
  { key: 3 },
  { key: 4 },
  { key: 5 },
  { key: 6 },
  { key: 7 },
  { key: 8 },
  { key: 9 },
  { key: 10 },
  { key: 0 },
  { key: 12 },
]

const codeDeleteSource = require("../assets/images/codeDelete.png")
const CENTER_ALIGN: ViewStyle = {
  justifyContent: "center",
  alignItems: "center",
}
const ENTER_VIEW: ViewStyle = {
  alignSelf: "center",
  marginBottom: 45,
  flexDirection: "row",
  justifyContent: "flex-end",
  alignItems: "center",
}
const LIST_CONTAINER: ViewStyle = {
  flex: 6,
  alignItems: "center",
}
const FLAT_LIST: ViewStyle = {
  alignSelf: "center",
}
const DIGIT_VIEW: ViewStyle = {
  width: 74,
  height: 74,
  margin: 10,
}
const DIGIT_VIEW_ROUND: ViewStyle = {
  ...DIGIT_VIEW,
  backgroundColor: "rgba(255, 255, 255, 0.16)",
  borderRadius: 37,
  borderWidth: 1,
  borderColor: "white",
}
const INSTRUCTION: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 20,
  lineHeight: 30,
  color: "white",
  textAlign: "center",
}
const DIGIT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 40,
  fontWeight: "100",
  color: "white",
}
const TEXT_VIEW: ViewStyle = {
  flex: 0.5,
  marginBottom: 15,
}
const deleteIcon: ImageStyle = {
  height: 30,
  width: 30,
}
const DOT: ViewStyle = {
  width: 10,
  height: 10,
  borderRadius: 5,
  backgroundColor: "white",
  marginHorizontal: 10,
}
const DOT_OUTLINE: ViewStyle = {
  width: 10,
  height: 10,
  borderRadius: 5,
  borderWidth: 1,
  borderColor: "white",
  marginHorizontal: 10,
}
const LOGO_CONTAINER: ViewStyle = {
  flex: 5,
  flexDirection: "column",
  alignItems: "center",
}
const CLOSE_BUTTON_ROW: ViewStyle = {
  flexDirection: "row",
  justifyContent: "flex-end",
  marginTop: 20,
  marginRight: 22,
}

@inject("accountStore")
@observer
export default class CodeVerificationScreen extends Component<Props, State> {
  static navigationOptions = {
    headerShown: false,
  }

  counter = 0
  digitStore = [
    { key: 1, value: "" },
    { key: 2, value: "" },
    { key: 3, value: "" },
    { key: 4, value: "" },
  ]
  current: undefined | string

  constructor(props: Props) {
    super(props)
    const { route } = this.props
    const purpose = this.getPurpose(route.params)
    let status = Status.First
    if (purpose === "change") {
      status = Status.Current
    }
    this.state = {
      code: "",
      code2: "",
      digitDisabled: false,
      clearDisabled: false,
      isPopupVisible: false,
      purpose: purpose,
      preRoute: this.getPreRoute(route.params),
      status: status,
    }
  }

  async UNSAFE_componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed)
    this.current = await AsyncStorage.getItem("pinCode")
  }

  componentDidMount() {
    // RNBootSplash.hide({ fade: true })
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed)
  }


  backPressed = () => true

  getPurpose(params: any): any {
    return params.purpose
  }

  getPreRoute(params: any): any {
    return params.preRoute
  }

  onCancelPress = () => {
    this.setState({ isPopupVisible: false })
  }

  onConfirmPress = () => {
    this.setState({ isPopupVisible: false })
    switch (this.state.preRoute) {
      case "Settings":
        this.props.navigation.goBack()
        break

      case "Welcome":
        this.props.navigation.pop()
        this.props.navigation.navigate("Main", {
          screen: "HomeStack",
        })
        break

      default:
        this.props.navigation.goBack()
    }
  }

  onEnteredPinCode(pinCode: string) {
    switch (this.state.purpose) {
      case "initial":
        if (this.state.status == Status.First) {
          this.setState({
            code: pinCode,
            status: Status.Second,
          })
          this.clear()
        } else {
          this.compareAndLock(pinCode, this.state.code)
        }
        break

      case "change":
        switch (this.state.status) {
          case Status.Current:
            if (this.current === pinCode) {
              this.setState({ status: Status.NewFirst })
            } else {
              Toast.show({
                title: (
                  <Text
                    style={styles.TOAST_TEXT}
                  >{`비밀번호가 다릅니다.`}</Text>
                ),
                duration: 2000,
                placement: "top",
                style: styles.TOAST_VIEW,
              })
            }
            this.clear()
            break

          case Status.NewFirst:
            this.setState({
              code: pinCode,
              status: Status.NewSecond,
            })
            this.clear()
            break

          case Status.NewSecond:
            this.compareAndLock(pinCode, this.state.code)
            break
        }
        break

      case "verification":
        this.setState({
          code: pinCode,
        })
        this.verify(this.current, pinCode)
        break
    }
  }

  verify(storedCode: string, inputCode: string) {
    if (storedCode === inputCode) {
      // 마지막 숫자 입력칸 채워지는 것 보이도록 지연
      setTimeout(() => {
        const { accountStore, navigation } = this.props
        navigation.pop()
        if (accountStore.user.id != -1) {
          navigation.navigate("Main", {
            screen: "HomeStack",
            params: { screen: "Home" },
          })
        } else {
          navigation.navigate("SignIn")
        }
      }, 300)
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`비밀번호가 올바르지 않습니다.\n다시 확인해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      this.clear()
      this.setState({})
    }
  }

  compareAndLock(code1: string, code2: string) {
    if (code1 === code2) {
      this.lock(code1)
    } else {
      let text
      if (this.state.purpose === "initial") {
        text = `입력하신 비밀번호와 다릅니다.\n다시 확인해 주세요.`
      } else {
        text = `입력하신 새 비밀번호와 다릅니다.\n다시 확인해 주세요.`
      }
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{text}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      this.clear()
      this.setState({})
    }
  }

  clear() {
    // 마지막 숫자 입력칸 채워지는 것 보이도록 clear 지연
    setTimeout(() => {
      for (let i = 0; i < this.digitStore.length; i++) {
        const element = this.digitStore[i]
        element.value = ""
      }
      this.counter = 0
      this.setState({})
    }, 300)
  }

  async lock(code: string) {
    try {
      await AsyncStorage.setItem("pinCode", code)
      if (this.state.purpose === "initial") {
        AsyncStorage.setItem("pinCodeEnabled", JSON.stringify(true))
      }
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`잠금 설정을 완료하였습니다.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(this.onConfirmPress, 2000)
    } catch (e) {
      console.tron.log('< CodeVerificationScreen > lock, error: ',e)
      setTimeout(() => this.lock(code), 100) //retry
    }
  }

  onEnterDigit(num: number, index: number) {
    if (this.counter + 1 <= 4) {
      this.counter++
      this.digitStore[this.counter - 1].value = String(num)
      this.setState({
        clearDisabled: false,
      })
    }
    if (this.counter === 4) {
      this.onEnteredPinCode(this.joinDigits())
      this.setState({
        digitDisabled: true,
      })
    }
  }

  joinDigits = () => {
    let pinCode = ""
    this.digitStore.forEach((item) => {
      pinCode += `${item.value}`
    })
    return pinCode
  }

  onRemoveDigit = () => {
    if (this.counter - 1 >= 0) {
      --this.counter
      this.digitStore[this.counter].value = ""
      this.setState({
        digitDisabled: false,
      })
    }
  }

  renderItemCell = ({ item, index }: { item: any; index: number }) => {
    // const { withTouchId = true } = this.props
    if (index === 9) {
      return <View style={DIGIT_VIEW} />
    } else if (index === 11) {
      return (
        <TouchableOpacity
          style={[DIGIT_VIEW, CENTER_ALIGN]}
          onPress={this.onRemoveDigit}
          disabled={this.state.clearDisabled}
        >
          <Image source={codeDeleteSource} style={deleteIcon} />
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity
          style={[DIGIT_VIEW_ROUND, CENTER_ALIGN]}
          onPress={() => this.onEnterDigit(item.key, index)}
          disabled={this.state.digitDisabled}
        >
          <Text style={DIGIT}>{item.key}</Text>
        </TouchableOpacity>
      )
    }
  }

  getTitle(status: Status): string {
    switch (status) {
      case Status.Current:
        return "현재 비밀번호 입력"

      case Status.NewFirst:
        return "새 비밀번호 입력"

      case Status.NewSecond:
        return "새 비밀번호 재확인"

      case Status.First:
        return "비밀번호 입력"

      case Status.Second:
        return "비밀번호 재확인"
    }
  }

  render() {
    const deviceWidth = Dimensions.get("window").width
    const deviceHeight = Dimensions.get("window").height
    // const { onCloseView, spaceColor, closeButtonColor } = this.props
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="#e8382f" barStyle="dark-content" />
        <LinearGradient colors={["#e8382f", "#ff841a"]} style={{ flex: 1, paddingTop: 32 }}>
          <View style={CLOSE_BUTTON_ROW}>
            {this.state.purpose != "verification" ? (
              <CloseButton
                imageStyle={{ tintColor: "rgb(255, 255, 255)" }}
                onPress={() => this.setState({ isPopupVisible: true })}
              />
            ) : (
              <View style={{ height: 15.7 }} />
            )}
          </View>

          <View style={{ flex: 1 }} />

          <View style={[TEXT_VIEW, CENTER_ALIGN]}>
            <Text style={INSTRUCTION}>{this.getTitle(this.state.status)}</Text>
          </View>
          <View style={ENTER_VIEW}>
            {this.digitStore.map((item) => (
              <View key={item.key} style={item.value.length > 0 ? DOT : DOT_OUTLINE} />
            ))}
          </View>

          <View style={LOGO_CONTAINER}>
            <View style={LIST_CONTAINER}>
              <BackgroundImage
                source={require("../assets/images/papricaBackground.png")}
                image={{
                  width: deviceWidth,
                  height: deviceHeight * 0.44,
                  marginTop: deviceHeight - deviceHeight * 0.44 - 250,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
              <FlatList
                style={FLAT_LIST}
                data={arrayOfNumbers}
                renderItem={this.renderItemCell}
                numColumns={3}
              />
            </View>
          </View>
        </LinearGradient>

        <Popup
          isVisible={this.state.isPopupVisible}
          title={"비밀번호 설정 취소?"}
          message={
            <Text
              style={{
                fontSize: 16,
                lineHeight: 26,
                paddingHorizontal: 5,
                textAlign: "center",
              }}
            >
              잠금 비밀번호 설정을 취소하시겠습니까?
              {this.state.purpose === "initial" ? (
                <Text
                  style={{
                    fontSize: 13,
                    color: "rgb(132, 132, 132)",
                  }}
                >
                  {"\n"}잠금 비밀번호는 “마이페이지 > 앱설정” 에서 언제든지 변경 가능합니다.
                </Text>
              ) : null}
            </Text>
          }
          button1={getPopupButton(this.onCancelPress, "아니오", "cancel")}
          button2={getPopupButton(this.onConfirmPress, "네")}
        />
      </View>
    )
  }
}
