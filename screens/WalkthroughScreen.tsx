import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import React, { Component } from "react"
import {
  Dimensions,
  Image,
  ScrollView,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
// import RNBootSplash from "react-native-bootsplash"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"
import Carousel, { Pagination } from "react-native-snap-carousel"
import { Button, YStack } from "tamagui"

import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgCapture from "../assets/images/captureAuth.svg"
import SvgGallery from "../assets/images/gallery2.svg"
import SvgWalkthrough0 from "../assets/images/walkthrough0.svg"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  DetailsStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { color, typography } from "../theme"

const viewportWidth = Dimensions.get("window").width

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  flexDirection: "column",
  alignItems: "center",
  paddingHorizontal: 22,
}
const FIRST_ITEM_BOLD_TEXT: TextStyle = { ...typography.title, letterSpacing: -0.64 }
const FIRST_ITEM_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 32,
  fontWeight: "400",
  lineHeight: 42,
  letterSpacing: -0.64,
  color: color.text,
}
const SUBTITLE = {
  fontFamily: typography.bold,
  fontSize: 24,
  lineHeight: 32,
  letterSpacing: -0.2,
  color: color.text,
  marginBottom: 6,
}
const DESCRIPTION_REGULAR = {
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 24,
  letterSpacing: -0.2,
  color: color.text,
}
const IMAGE = {
  width: viewportWidth,
  height: viewportWidth / 1.2,
  marginBottom: 10,
}
const SIGN_UP_BUTTON_TEXT = {
  ...typography.button,
  color: color.palette.white,
}
const LOGIN_BUTTON_TEXT = {
  fontFamily: typography.primary,
  fontSize: 17,
  lineHeight: 20,
  letterSpacing: -0.34,
  color: color.primary,
}
const TOUR_TEXT = {
  ...typography.sub2,
  fontSize: 16,
  opacity: 0.5,
  textDecorationLine: "underline",
}
const SIGN_UP_BUTTON = { marginHorizontal: 30, marginBottom: 10 }
const LOGIN_BUTTON = {
  marginHorizontal: 30,
  marginBottom: 14,
  backgroundColor: color.palette.white,
  borderWidth: 1,
  borderColor: color.palette.paleSalmon,
}
const POPUP_CONTAINER: ViewStyle = {
  borderRadius: 10,
  backgroundColor: "#fff",
  marginTop: 20,
  width: "100%",
  maxHeight: 400,
}
const PERMISSION_MESSAGE_WRAPPER: ViewStyle = {
  flexDirection: "row",
  marginTop: 16,
  marginBottom: 4,
}
const TITLE: TextStyle = {
  ...typography.title4,
}
const MESSAGE: TextStyle = {
  ...typography.sub2,
}
const MESSAGE_BELOW: TextStyle = {
  ...typography.sub2,
  paddingHorizontal: 5,
  marginTop: 16,
}
const TEXT_WRAPPER: ViewStyle = {
  paddingRight: 40,
}

type WalkthroughRouteProp = RouteProp<AuthStackParamList, "Walkthrough">

type WalkthroughNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "Walkthrough">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  navigation: WalkthroughNavigationProp
  route: WalkthroughRouteProp
}

interface State {
  entries: any
  activeSlide: number
  permissionGuideModalVisible: boolean
}

export default class WalkthroughScreen extends Component<Props, State> {
  static navigationOptions = {
    headerShown: false,
  }

  _carousel: any
  _unsubscribeFocus: () => void

  constructor(props: Props) {
    super(props)
    this.state = {
      entries: [{}, {}, {}, {}, {}, {}, {}],
      activeSlide: 0,
      permissionGuideModalVisible: false,
    }
  }

  async componentDidMount() {
    await AsyncStorage.setItem("initialRunning", JSON.stringify(false))
    // RNBootSplash.hide({ fade: true })
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)
  }

  didFocus = () => {
    const { route } = this.props
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    firebase.analytics().logEvent("tutorial_begin")
    amplitude.getInstance().logEvent("tutorial_begin")
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
  }

  renderItem(index: number) {
    if (index == 0) {
      return (
        <View style={CONTENT_CONTAINER}>
          <View style={{ flex: 1, alignItems: "center", justifyContent: "center", paddingTop: 56 }}>
            <Text style={FIRST_ITEM_BOLD_TEXT}>
              파프리카케어
              <Text style={FIRST_ITEM_TEXT}>로</Text>
            </Text>
            <Text style={FIRST_ITEM_BOLD_TEXT}>
              건강관리<Text style={FIRST_ITEM_TEXT}>를 시작하세요.</Text>
            </Text>
            <SvgWalkthrough0 width={viewportWidth} style={{ marginTop: 32 }} />
          </View>
        </View>
      )
    }

    let imageComponent
    let textComponent1
    let textComponent2
    let textComponent3
    switch (index) {
      case 1:
        imageComponent = (
          <Image
            source={require("../assets/images/walkthrough1.png")}
            style={IMAGE}
            resizeMode="cover"
          />
        )
        textComponent1 = <Text style={SUBTITLE}>찍으세요!</Text>
        textComponent2 = <Text style={DESCRIPTION_REGULAR}>암호 같은 처방전</Text>
        textComponent3 = <Text style={DESCRIPTION_REGULAR}>쉽게 알려드려요.</Text>
        break
      case 2:
        imageComponent = (
          <Image
            source={require("../assets/images/walkthrough2.png")}
            style={IMAGE}
            resizeMode="cover"
          />
        )
        textComponent1 = <Text style={SUBTITLE}>알려드려요!</Text>
        textComponent2 = <Text style={DESCRIPTION_REGULAR}>무슨 약을 드실지 때맞춰</Text>
        textComponent3 = <Text style={DESCRIPTION_REGULAR}>알람을 보내드려요.</Text>
        break
      case 3:
        imageComponent = (
          <Image
            source={require("../assets/images/walkthrough3.png")}
            style={IMAGE}
            resizeMode="cover"
          />
        )
        textComponent1 = <Text style={SUBTITLE}>궁금하셨죠?!</Text>
        textComponent2 = <Text style={DESCRIPTION_REGULAR}>내가 먹는 약, 어떤 약인지</Text>
        textComponent3 = <Text style={DESCRIPTION_REGULAR}>쉽게 알려드려요.</Text>
        break
      case 4:
        imageComponent = (
          <Image
            source={require("../assets/images/walkthrough4.png")}
            style={IMAGE}
            resizeMode="cover"
          />
        )
        textComponent1 = <Text style={SUBTITLE}>안심하세요!</Text>
        textComponent2 = <Text style={DESCRIPTION_REGULAR}>여러 병원에서 받은 약,</Text>
        textComponent3 = <Text style={DESCRIPTION_REGULAR}>함께 먹어도 될지 알려드려요.</Text>
        break
      case 5:
        imageComponent = (
          <Image
            source={require("../assets/images/walkthrough5.png")}
            style={IMAGE}
            resizeMode="cover"
          />
        )
        textComponent1 = <Text style={SUBTITLE}>보여주세요!</Text>
        textComponent2 = <Text style={DESCRIPTION_REGULAR}>먹던 약이 무엇인지 의사에게</Text>
        textComponent3 = <Text style={DESCRIPTION_REGULAR}>의료기록을 보여주세요.</Text>
        break
      case 6:
        imageComponent = (
          <Image
            source={require("../assets/images/walkthrough6.png")}
            style={IMAGE}
            resizeMode="cover"
          />
        )
        textComponent1 = <Text style={SUBTITLE}>함께해요!</Text>
        textComponent2 = <Text style={DESCRIPTION_REGULAR}>이런 좋은 서비스, 나만 쓰나요?</Text>
        textComponent3 = <Text style={DESCRIPTION_REGULAR}>가족 추가로 함께하세요.</Text>
        break

      default:
        break
    }

    return (
      <View style={CONTENT_CONTAINER}>
        <View style={{ alignItems: "center" }}>
          {imageComponent}
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            {textComponent1}
            {textComponent2}
            {textComponent3}
          </View>
        </View>
      </View>
    )
  }

  get pagination() {
    const { entries, activeSlide } = this.state
    return (
      <Pagination
        containerStyle={{
          paddingTop: 12,
          paddingBottom: 12,
          marginBottom: 8,
        }}
        dotsLength={entries.length}
        activeDotIndex={activeSlide}
        dotStyle={{
          width: 8,
          height: 8,
          borderRadius: 4,
          marginHorizontal: -5,
          backgroundColor: color.primary,
        }}
        inactiveDotStyle={{
          width: 8,
          height: 8,
          borderRadius: 4,
          backgroundColor: `${color.primary}32`,
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={1}
      />
    )
  }

  confirmPermission = async () => {
    await AsyncStorage.setItem("doNotDisplayPermissionGuide", JSON.stringify(true))
    this.setState({
      permissionGuideModalVisible: false,
    })
    this.start("SignUp")
  }

  renderPermissionGuideMessage() {
    return (
      <ScrollView style={POPUP_CONTAINER}>
        <Text
          style={{
            ...typography.sub2,
            color: color.primary,
            marginTop: 16,
          }}
        >
          선택 접근권한
        </Text>
        <View style={PERMISSION_MESSAGE_WRAPPER}>
          <SvgCapture style={{ marginRight: 8 }} />
          <View style={TEXT_WRAPPER}>
            <Text style={TITLE}>카메라</Text>
            <Text style={MESSAGE}>처방전, 약봉투 촬영을 위해 카메라를 사용합니다.</Text>
          </View>
        </View>

        <View style={PERMISSION_MESSAGE_WRAPPER}>
          <SvgGallery style={{ marginRight: 8 }} />
          <View style={TEXT_WRAPPER}>
            <Text style={TITLE}>앨범</Text>
            <Text style={MESSAGE}>
              처방전/약봉투 등록, 프로필 사진 등록을 위해 저장된 사진을 사용합니다.
            </Text>
          </View>
        </View>

        <View style={{ marginTop: 40 }}>
          <Text style={MESSAGE_BELOW}>
            ※ 선택적 접근권한은 동의하지 않아도 해당 권한을 제외한 서비스 이용이 가능합니다.{" "}
          </Text>
          <Text style={MESSAGE_BELOW}>※ 위 접근 권한 허용은 해당 기능 사용시 요청합니다. </Text>
          <Text style={MESSAGE_BELOW}>
            ※ 휴대폰 {">"} 설정 {">"} 파프리카케어 {">"} 접근 허용 메뉴에서 설정을 변경 하세요.
          </Text>
        </View>
      </ScrollView>
    )
  }

  render() {
    return (
      <SafeAreaInsetsContext.Consumer>
        {(insets) => (
          <Screen preset="fixed" style={{ paddingBottom: insets.bottom }}>
            <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
            <Carousel
              vertical={false}
              ref={(c: any) => {
                this._carousel = c
              }}
              data={this.state.entries}
              renderItem={({ index }: { index: number }) => this.renderItem(index)}
              onSnapToItem={(index) => {
                this.setState({ activeSlide: index })
              }}
              sliderWidth={viewportWidth}
              itemWidth={viewportWidth}
              activeSlideOffset={16}
            />
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              {this.pagination}
            </View>
            <YStack mb="$4">
              <Button
                bc={color.palette.orange}
                br={45}
                onPress={() =>
                  this.setState({
                    permissionGuideModalVisible: !this.state.permissionGuideModalVisible,
                  })
                }
                style={SIGN_UP_BUTTON}
              >
                <Text style={SIGN_UP_BUTTON_TEXT}>회원가입</Text>
              </Button>
              <Button
                bc={color.palette.orange}
                br={45}
                onPress={() => this.start("SignIn")}
                style={{ ...LOGIN_BUTTON, justifyContent: "center", alignItems: "center" }}
              >
                <Text style={LOGIN_BUTTON_TEXT}>로그인</Text>
              </Button>

              <TouchableOpacity
                style={{ marginTop: 10, alignSelf: "center" }}
                onPress={() => this.start("TourGuide")}
              >
                <Text style={TOUR_TEXT}>둘러보기</Text>
              </TouchableOpacity>
            </YStack>

            <Popup
              isVisible={this.state.permissionGuideModalVisible}
              title={`앱 사용을 위한 권한 안내입니다.\n아래 권한을 허용해주세요.`}
              message={this.renderPermissionGuideMessage()}
              button2={getPopupButton(() => this.confirmPermission(), "계속하기")}
              onBackButtonPress={() => this.confirmPermission()}
            />
          </Screen>
        )}
      </SafeAreaInsetsContext.Consumer>
    )
  }

  async start(routeName: "SignUp" | "SignIn" | "TourGuide") {
    firebase.analytics().logEvent("tutorial_complete")
    amplitude.getInstance().logEvent("tutorial_complete")
    this.props.navigation.navigate(routeName)
  }
}
