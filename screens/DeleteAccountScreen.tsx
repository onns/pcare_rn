import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Checkbox, ScrollView, Toast, View } from "native-base"
import React, { Component } from "react"
import { Alert, TextStyle, ViewStyle } from "react-native"

import { api } from "@api/api"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import CloseButton from "../components/CloseButton"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, styles, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  backgroundColor: "#fff",
  paddingTop: 57,
  paddingHorizontal: 20,
}
const ROW: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
}
const CHECKBOX_MESSAGE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  fontWeight: "bold",
  color: color.palette.brownGrey,
  paddingLeft: 10,
  marginBottom: 2,
}
const BOTTOM_BUTTONS: ViewStyle = {
  height: 140,
  justifyContent: "flex-end",
  padding: 20,
  backgroundColor: "#fff",
}

type DeleteAccountRouteProp = RouteProp<MyPageStackParamList, "DeleteAccount">

type DeleteAccountNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "DeleteAccount">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AuthStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: DeleteAccountNavigationProp
  route: DeleteAccountRouteProp
}

interface State {
  agreed: boolean
}

@inject("accountStore")
@observer
export default class DeleteAccountScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "회원 탈퇴",
      /* These values are used instead of the shared configuration! */
      headerBackTitle: null,
      headerLeft: null,
      headerRight: () => (
        <CloseButton
          onPress={() => {
            navigation.pop()
          }}
        />
      ),
    }
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      agreed: false,
    }
  }

  componentDidMount() {
    const { route } = this.props
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })
  }

  async _submit() {
    this.props.navigation.navigate("DeleteAccountWhy")
  }

  async deleteAccount() {
    const userId = this.props.accountStore.user.id

    const result = await api.deleteAccount(userId)
    if (result.kind === "ok") {
      Alert.alert(
        "",
        `그동안 저희 파프리카케어 서비스를 이용해 주셔서 감사합니다. 보다 나은 서비스로 다시 찾아 뵐 수 있기를 바라며 언제나 고객님이 건강하시기를 바랍니다.`,
        [{ text: "확인", onPress: () => this.initiateApp() }],
        { cancelable: false },
      )
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      console.tron.log("< DeleteAccountScreen > deleteAccount, error")
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.pop(), 2000)
    }
  }

  async initiateApp() {
    api.apisauce.deleteHeader("Authorization")
    this.props.accountStore.initiate()

    await AsyncStorage.clear()
    this.props.navigation.navigate("Auth")
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: color.background }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={{ paddingHorizontal: 10, paddingBottom: 110 }}>
            <Text
              style={{
                fontSize: 22,
                fontWeight: "bold",
                letterSpacing: -0.5,
                lineHeight: 30,
                color: color.text,
                textAlign: "center",
              }}
            >
              회원을 탈퇴하시기 전에{"\n"}안내사항을 꼭 확인하세요.
            </Text>
            <Text
              style={{
                fontSize: 18,
                fontWeight: "bold",
                color: "rgb(112, 112, 112)",
                marginTop: 30,
                marginBottom: 5,
              }}
            >
              개인정보 및 이용기록 삭제 내용
            </Text>
            <View
              style={{
                borderWidth: 1,
                borderRadius: 4,
                borderColor: "rgb(206, 206, 206)",
                padding: 15,
                marginBottom: 20,
              }}
            >
              <Text style={{ fontSize: 15, lineHeight: 19 }}>
                *개인정보: 이메일주소(SNS), 이름, 생년월일, 성별, 프로필사진
              </Text>
              <Text style={{ lineHeight: 19, marginTop: 8 }}>
                *본인인증정보: 휴대폰 본인인증 내역, 이름, 생년월일, 성별, 전화번호
              </Text>
              <Text style={{ lineHeight: 19, marginTop: 8 }}>
                *SNS 가입인 경우 해당 SNS 사이트에서 파프리카케어와 연결된 계정을 직접 해제해주셔야
                삭제가 완료됩니다.
              </Text>
            </View>
            <Text style={{ fontSize: 15, color: "rgb(230, 56, 48)", lineHeight: 19 }}>
              개인정보 및 이용 기록은 모두 삭제되며, 삭제된 계정은 복구할 수 없습니다.
            </Text>
          </View>
        </ScrollView>

        <View style={BOTTOM_BUTTONS}>
          <View style={ROW}>
            <Checkbox
              style={{
                width: 26,
                height: 26,
                borderRadius: 13,
                paddingLeft: 3,
                paddingTop: 4,
              }}
              isChecked={this.state.agreed}
              onChange={() => this.onCheckboxValueChange()}
            />
            <Text style={CHECKBOX_MESSAGE}>위 안내 사항 확인 후 회원 탈퇴에 동의합니다.</Text>
          </View>
          <View style={{ height: 42, marginTop: 15 }}>
            <Button
              primary
              block
              style={{ height: 42, flex: 1 }}
              onPress={() => this._submit()}
              disabled={!this.state.agreed}
            >
              <Text style={{ fontSize: 17, fontWeight: "bold", color: "#fff" }}>회원 탈퇴</Text>
            </Button>
          </View>
        </View>
      </View>
    )
  }

  onCheckboxValueChange() {
    this.setState({
      agreed: !this.state.agreed,
    })
  }
}
