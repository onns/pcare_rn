import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content } from "native-base"
import React, { Component } from "react"
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Route, TabBar } from "react-native-tab-view"

import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import {
  DetailsStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { Notice } from "../stores/Notice"
import { color, typography } from "../theme"

const ROW: ViewStyle = {
  flexDirection: "row",
}
const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  paddingTop: 10,
}
const FLEX_ONE: ViewStyle = {
  flex: 1,
}
const SCROLLVIEW_MARGINTOP: ViewStyle = {
  marginTop: 10,
}
const SCROLLVIEW: ViewStyle = {
  backgroundColor: "rgb(255, 255, 255)",
}
const NOTICE_TITLE: TextStyle = {
  fontFamily: typography.primary,
  color: "rgb(85, 85, 85)",
  lineHeight: 23,
  fontSize: 18,
  marginLeft: 16,
}
const NOTICE_DATE: TextStyle = {
  fontFamily: typography.primary,
  color: "rgb(153, 153, 153)",
  lineHeight: 18,
  fontSize: 15,
  marginLeft: 16,
  marginTop: 8,
}
const NOTICE_NEW_ICON: TextStyle = {
  fontFamily: typography.primary,
  color: color.primary,
  borderRadius: 8,
  height: 16,
  borderColor: color.primary,
  borderWidth: 1,
  lineHeight: 14,
  fontSize: 10,
  marginLeft: 6,
  marginTop: 8,
  justifyContent: "center",
  alignItems: "center",
  paddingLeft: 5,
  paddingRight: 3,
  paddingBottom: 2,
}

type NoticeListRouteProp = RouteProp<HomeStackParamList, "NoticeList">

type NoticeListNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "NoticeList">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

/**
 * Props, 컴포넌트에서 직접 사용가능함
 */
export interface Props {
  accountStore: AccountStore
  navigation: NoticeListNavigationProp
  route: NoticeListRouteProp
}

/**
 * State, 컴포넌트에서 생성되며 setState로 변경가능
 */
interface State {
  noticeList: Array<any>
  notificationList: Array<any>
  routes: Array<Route>
}

@inject("accountStore")
@observer
export default class NoticeListScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      noticeList: [],
      notificationList: [],
      routes: [
        { key: "notice", title: "소식" },
        { key: "notification", title: "활동 알림" },
      ],
    }
  }

  /**
   * Header
   */
  static navigationOptions = {
    title: "소식",
  }

  async componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })

    const { noticeList } = this.props.accountStore
    const lastNoticeId = await AsyncStorage.getItem("lastNoticeId")
    if (noticeList.length > 0 && lastNoticeId != String(noticeList[0].id)) {
      AsyncStorage.setItem("lastNoticeId", String(noticeList[0].id))
    }
  }

  /**
   * 공지사항 목록 Item
   */
  _renderItem = ({ item, index }: { item: Notice; index: number }) => {
    return (
      <TouchableOpacity
        style={[
          FLEX_ONE,
          { paddingTop: 20, paddingBottom: 15, backgroundColor: "rgb(255, 255, 255)" },
        ]}
        /** 클릭 시 해당 공지사항의 id값을 넘겨서 NoticeDetail 오픈 */
        onPress={() =>
          this.props.navigation.navigate("NoticeDetail", {
            id: this.props.accountStore.noticeList[index].id,
          })
        }
      >
        <View style={[ROW, { paddingRight: 20 }]}>
          <Image
            source={require("../assets/images/iconNotice.png")}
            style={{ width: 26, height: 26, marginLeft: 17 }}
          />
          <View style={FLEX_ONE}>
            <Text numberOfLines={3} style={NOTICE_TITLE}>
              {item.title}
            </Text>
          </View>
        </View>
        <View style={[ROW, { paddingLeft: 45 }]}>
          <Text style={NOTICE_DATE}>
            {item.created.getFullYear() +
              "년 " +
              (item.created.getMonth() + 1) +
              "월 " +
              item.created.getDate() +
              "일"}
          </Text>
          {item.diffDays > 7 ? <Text style={NOTICE_NEW_ICON}>NEW</Text> : null}
        </View>
      </TouchableOpacity>
    )
  }

  /**
   * 공지사항 목록 Separtor
   */
  _renderSeparator = () => {
    return <View style={{ flex: 1, height: 1, backgroundColor: "rgb(206, 206, 206)" }}></View>
  }

  renderNoticeList() {
    return (
      <View style={{ backgroundColor: color.background }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <FlatList<Notice>
            indicatorStyle="black"
            data={this.props.accountStore.noticeList}
            renderItem={this._renderItem}
            ItemSeparatorComponent={this._renderSeparator}
          />
        </ScrollView>
      </View>
    )
  }

  renderNotificationList() {
    return (
      <ScrollView style={[SCROLLVIEW, SCROLLVIEW_MARGINTOP]}>
        <FlatList
          indicatorStyle="black"
          data={this.state.notificationList}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
        />
      </ScrollView>
    )
  }

  /* 탭으로 바꾸어 알림도 넣을 때!
  render() {
    return (
      <View style={{ backgroundColor: color.background }}>
        <TabView
          navigationState={this.state}
          renderScene={({ route, jumpTo }) => {
            switch (route.key) {
              case "notice":
                return this.renderNoticeList()
              case "notification":
                return this.renderNotificationList()
            }
          }}
          renderTabBar={this.renderTabBar}
          onIndexChange={index => this.setState({ index: index })}
          initialLayout={{ width: Dimensions.get("window").width }}
        />
      </View>
    )
  } */

  /** 현재는 소식만 출력 */
  render() {
    return (
      <View style={{ backgroundColor: color.background }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <FlatList<Notice>
            indicatorStyle="black"
            data={this.props.accountStore.noticeList}
            keyExtractor={(item: Notice) => item.id.toString()}
            renderItem={this._renderItem}
            ItemSeparatorComponent={this._renderSeparator}
          />
        </ScrollView>
      </View>
    )
  }

  renderTabBar = (props: any) => (
    <TabBar
      {...props}
      indicatorStyle={{
        borderBottomWidth: 2,
        borderBottomColor: color.primary,
      }}
      style={{
        height: 40,
        backgroundColor: "#fff",
        borderColor: color.palette.pale,
        borderBottomWidth: 0.5,
        padding: 0,
        elevation: 0,
      }}
      // dynamicWidth
      // scrollEnabled
      tabStyle={{
        // width: "auto",
        height: 40,
        paddingVertical: 0,
        paddingHorizontal: 16,
      }}
      labelStyle={{
        fontFamily: typography.primary,
        fontSize: 15,
        lineHeight: 19,
        letterSpacing: -0.6,
      }}
      activeColor={color.primary}
      inactiveColor="rgb(153, 153, 153)"
    />
  )
}
