import { isEmpty } from "lodash"
import { DateTime } from "luxon"
import { inject, observer } from "mobx-react"
import React from "react"
import {
  Animated,
  Platform,
  SafeAreaView,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"
import StickyParallaxHeader from "react-native-sticky-parallax-header"
import { ProgressCircle } from "react-native-svg-charts"

import { Slider } from "@miblanchard/react-native-slider"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgInfoDark from "../assets/images/infoDark.svg"
import SvgPrescription from "../assets/images/prescription.svg"
import SvgTimeAfternoonOrange from "../assets/images/timeAfternoonOrange.svg"
import SvgTimesCustom from "../assets/images/timeCustom.svg"
import SvgTimeEveningOrange from "../assets/images/timeEveningOrange.svg"
import SvgMorning from "../assets/images/timeMorningOrange.svg"
import SvgNight from "../assets/images/timeNightOrange.svg"
import SvgOnce from "../assets/images/timeOnce.svg"
import SvgWakeup from "../assets/images/timeWakeupOrange.svg"
import TouchableItem from "../components/button/TouchableItem"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { Text } from "../components/StyledText"
import { formatDateFromString } from "../lib/DateUtil"
import {
  DetailsStackParamList,
  MainTabParamList,
  RootStackParamList,
  TakingDrugStackParamList,
} from "../navigation/types"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"

const headerHeight = Platform.OS === "ios" ? 40 : 48
const HEADER_WRAPPER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  height: headerHeight,
  borderBottomWidth: 1, // TODO: scroll 상태에 따라 0/1 업데이트
  borderColor: color.line,
  paddingHorizontal: Platform.OS === "ios" ? 0 : 8,
}

const TO_FIT_CENTER_VIEW: ViewStyle = { width: 24, height: 24, marginRight: 16 }

const ROW: ViewStyle = { flexDirection: "row" }

const CENTER: ViewStyle = {
  justifyContent: "center",
  alignItems: "center",
}

const BOTTOM_HEADER_WRAP: ViewStyle = {
  height: 76,
  flexDirection: "row",
  alignItems: "center",
  paddingTop: 8,
  backgroundColor: color.palette.white,
  paddingBottom: 16,
}

const THUMBNAIL: ViewStyle = {
  marginHorizontal: 6,
  bottom: 8,
}

const GREY_TEXT: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
}

const PROGRESS_CIRCLE: ViewStyle = {
  height: 200,
  backgroundColor: color.palette.white,
}
const PROGRESS_CIRCLE_CENTER_TEXT_WRAP: ViewStyle = {
  flexDirection: "row",
  alignItems: "baseline",
  left: 8,
}

const MEDICATION_SCHEDULE_VIEW: ViewStyle = {
  backgroundColor: color.palette.white,
  marginTop: 8,
  marginBottom: 8,
}

const SCHEDULE_TEXT_VIEW: ViewStyle = {
  width: "100%",
  margin: 24,
}

const FONT_TEXT_BOLD: TextStyle = {
  fontFamily: typography.bold,
  color: color.palette.black,
}
const FONT_TEXT_BOLD_GREY: TextStyle = {
  fontFamily: typography.bold,
  color: color.palette.grey50,
}

const ALIGN_CENTER: ViewStyle = {
  width: "100%",
  alignItems: "center",
}

const BUTTON_SPACE: ViewStyle = {
  width: 190,
  justifyContent: "space-between",
  marginTop: 32,
  marginBottom: 24,
}

const GREY_BUTTON: ViewStyle = {
  backgroundColor: color.palette.veryLightPink3,
  width: 91,
  height: 36,
  borderRadius: 50,
}

const MEDICATION_SCHEDULE_GRAPH_VIEW: ViewStyle = {
  width: 295,
  height: 8,
  marginBottom: 8,
}

const TAKING_DRUGDAYS_VIEW: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  width: 295,
}

const REMAINING_DAYS: TextStyle = {
  fontSize: 13,
  color: color.palette.grey50,
}

const MEDICATION_COMPLIANCE_VIEW: ViewStyle = {
  backgroundColor: color.palette.white,
  maxHeight: "100%",
  marginBottom: 100,
  paddingBottom: 34,
}

const COMPLIANCE_TIMES_DRUGS_WRAP: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  width: "85%",
  marginBottom: 13,
}

const TIMESLOTS_NAME_WRAP: ViewStyle = {
  flexDirection: "row",
  width: Platform.OS === "ios" ? "20%" : "25%",
}
const TIMES_SLIDER_VIEW: ViewStyle = {
  width: "55%",
  marginLeft: 20,
}

const TIMES_PERCENTAGE_VIEW: ViewStyle = {
  width: "15%",
  flexDirection: "row",
  justifyContent: "flex-end",
}
const FONT_SIZE_13: TextStyle = {
  fontSize: 13,
  color: color.palette.black,
  lineHeight: 25,
}
const SLIDER: ViewStyle = { height: 8 }
const SLIDER_TRACK: ViewStyle = { height: 8, borderRadius: 40, backgroundColor: color.palette.pale }

const SLIDER_TOTAL_BAR: ViewStyle = {
  width: "100%",
  height: 8,
  top: 8,
}

const DRUGS_NAMES_VIEW: ViewStyle = {
  width: "20%",
}
const DRUGS_SLIDER_VIEW: ViewStyle = {
  width: "60%",
}
const DRUGS_PERCENTAGE_VIEW: ViewStyle = {
  width: "15%",
  flexDirection: "row",
  justifyContent: "flex-end",
}

const CHECK_NEXT_SCHEDULE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  alignItems: "center",
  marginTop: 16,
  width: 343,
  height: 32,
  marginBottom: 16,
  backgroundColor: color.palette.veryLightPink,
}

type MedicationReportDetailRouteProp = RouteProp<TakingDrugStackParamList, "MedicationReportDetail">

type MedicationReportDetailNavigationProp = CompositeNavigationProp<
  StackNavigationProp<TakingDrugStackParamList, "MedicationReportDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  navigation: MedicationReportDetailNavigationProp
  route: MedicationReportDetailRouteProp
  prescriptionStore: PrescriptionStore
}
interface State {
  bottomSheetMode: "more" | "transfer"
  dosingDays: number
  index: number
  drugAdherenceRating: number
  symptomImprovementRating: number
  memo: string
  memoEdited: boolean
  isModalVisible: boolean
  popupTitle: string
  popupContent: string
  popupDescription: string
  buttonActive: boolean
}

@inject("prescriptionStore", "accountStore")
@observer
export default class MedicationReportDetailScreen extends React.Component<Props, State> {
  state = {
    scroll: new Animated.Value(0),
    buttonActive: true,
    routes: [{ key: "MedicationReportDetail" }],
  }

  renderContent = () => {
    const { buttonActive } = this.state
    const { params } = this.props.route
    const { prescription } = params
    const { compliance, remainingDosingDays, dosingDays } = prescription
    const dosingStart = dosingDays - remainingDosingDays
    const sortTimeslots = compliance.timeslots.slice().sort((a, b) => {
      if (
        a.timeslot_name === "wakeup" &&
        (b.timeslot_name === "morning" ||
          b.timeslot_name === "afternoon" ||
          b.timeslot_name === "evening" ||
          b.timeslot_name === "night" ||
          b.timeslot_name === "once")
      ) {
        return -1
      }
      if (
        a.timeslot_name === "morning" &&
        (b.timeslot_name === "afternoon" ||
          b.timeslot_name === "evening" ||
          b.timeslot_name === "night" ||
          b.timeslot_name === "once")
      ) {
        return -1
      }
      if (
        a.timeslot_name === "afternoon" &&
        (b.timeslot_name === "evening" || b.timeslot_name === "night" || b.timeslot_name === "once")
      ) {
        return -1
      }
      if (
        a.timeslot_name === "evening" &&
        (b.timeslot_name === "night" || b.timeslot_name === "once")
      ) {
        return -1
      }
      if (a.timeslot_name === "night" && b.timeslot_name === "once") {
        return -1
      }
      if (a.timeslot_name === "morning" && b.timeslot_name === "wakeup") {
        return 1
      }
      if (
        a.timeslot_name === "afternoon" &&
        (b.timeslot_name === "wakeup" || b.timeslot_name === "morning")
      ) {
        return 1
      }
      if (
        a.timeslot_name === "evening" &&
        (b.timeslot_name === "wakeup" ||
          b.timeslot_name === "morning" ||
          b.timeslot_name === "afternoon")
      ) {
        return 1
      }
      if (
        a.timeslot_name === "night" &&
        (b.timeslot_name === "wakeup" ||
          b.timeslot_name === "morning" ||
          b.timeslot_name === "afternoon" ||
          b.timeslot_name === "evening")
      ) {
        return 1
      }
      if (
        a.timeslot_name === "once" &&
        (b.timeslot_name === "wakeup" ||
          b.timeslot_name === "morning" ||
          b.timeslot_name === "afternoon" ||
          b.timeslot_name === "evening" ||
          b.timeslot_name === "night")
      ) {
        return 1
      }
    })
    return (
      <View>
        <View style={MEDICATION_SCHEDULE_VIEW}>
          <View style={SCHEDULE_TEXT_VIEW}>
            <Text style={typography.title2}>복약일정</Text>
          </View>
          <View style={ALIGN_CENTER}>
            <Text
              style={
                remainingDosingDays <= 5
                  ? [FONT_TEXT_BOLD, { color: color.palette.orange }]
                  : FONT_TEXT_BOLD
              }
            >
              {remainingDosingDays <= 0 ? "" : "남은기간"}
            </Text>
            <Text
              style={
                remainingDosingDays > 5
                  ? [FONT_TEXT_BOLD, { fontSize: 40 }]
                  : remainingDosingDays <= 0
                  ? [FONT_TEXT_BOLD, { fontSize: 40 }]
                  : [FONT_TEXT_BOLD, { fontSize: 40, color: color.palette.orange }]
              }
            >
              {remainingDosingDays <= 0 ? `복약종료` : `${remainingDosingDays}일`}
            </Text>
            <View style={[MEDICATION_SCHEDULE_GRAPH_VIEW, { marginTop: 24 }]}>
              <Slider
                disabled
                containerStyle={SLIDER}
                value={dosingStart >= dosingDays ? dosingDays : dosingStart}
                trackStyle={SLIDER_TRACK}
                thumbStyle={{ width: 0 }}
                minimumTrackTintColor={color.palette.orange}
                maximumValue={dosingDays}
              />
            </View>
            <View style={TAKING_DRUGDAYS_VIEW}>
              <Text style={[FONT_SIZE_13, { color: color.palette.grey70 }]}>
                {remainingDosingDays <= 0 ? "종료" : `${dosingStart} 일차`}
              </Text>
              <Text style={REMAINING_DAYS}>{`총 ${dosingDays}일`}</Text>
            </View>
            <View style={remainingDosingDays <= 7 ? CHECK_NEXT_SCHEDULE : null}>
              {remainingDosingDays <= 7 && remainingDosingDays >= 3 ? (
                <SvgInfoDark style={{ margin: 4 }} />
              ) : null}
              <Text>
                {remainingDosingDays <= 7 && remainingDosingDays >= 3
                  ? `복약종료일이 ${remainingDosingDays}일 남았습니다.`
                  : ""}
              </Text>
              {remainingDosingDays <= 2 ? <SvgInfoDark style={{ margin: 4 }} /> : null}
              <Text>{remainingDosingDays <= 2 ? "다음 병원 방문일을 확인하세요." : ""}</Text>
            </View>
          </View>
        </View>

        {/*  PRN  */}
        {/* <View style={MEDICATION_SCHEDULE_VIEW}>
          <View style={SCHEDULE_TEXT_VIEW}>
            <Text style={TEXT_BOLD_17}>복약일정</Text>
          </View>
          <View style={ALIGN_CENTER}>
            <Text
              style={
                remainingDosingDays <= 5
                  ? [FONT_TEXT_BOLD, { color: color.palette.orange }]
                  : FONT_TEXT_BOLD
              }
            >
              {remainingDosingDays <= 0 ? "" : "남은횟수"}
            </Text>
            <Text
              style={
                remainingDosingDays > 5
                  ? [FONT_TEXT_BOLD, { fontSize: 40 }]
                  : remainingDosingDays <= 0
                  ? [FONT_TEXT_BOLD, { fontSize: 40 }]
                  : [FONT_TEXT_BOLD, { fontSize: 40, color: color.palette.orange }]
              }
            >
              {remainingDosingDays <= 0 ? `복약종료` : `${remainingDosingDays}회`}
            </Text>
            <View>
              <Text
                style={
                  remainingDosingDays <= 5 ? { color: color.palette.orange } : { fontSize: 20 }
                }
              >
                / 총 24회
              </Text>
            </View>
            <View style={remainingDosingDays <= 5 ? CHECK_NEXT_SCHEDULE : null}>
              {remainingDosingDays <= 5 ? <SvgInfoDark style={{ margin: 4 }} /> : null}
              <Text>{remainingDosingDays <= 5 ? "다음 병원 방문일을 확인하세요." : ""}</Text>
            </View>
          </View>
        </View> */}
        {/* 남은횟수 </View> */}
        <View style={MEDICATION_COMPLIANCE_VIEW}>
          <View style={SCHEDULE_TEXT_VIEW}>
            <Text style={[typography.title2, { marginBottom: 4 }]}>복약순응도</Text>
            <Text style={GREY_TEXT}>얼마나 약을 잘 복용했나요?</Text>
          </View>

          <ProgressCircle
            style={PROGRESS_CIRCLE}
            progress={compliance.total * 0.01}
            strokeWidth={7}
            progressColor={color.palette.orange}
          >
            <View style={CENTER}>
              <Text style={FONT_TEXT_BOLD_GREY}>
                {dosingStart >= dosingDays ? "평균" : `${dosingStart}일차`}
              </Text>
              <View style={PROGRESS_CIRCLE_CENTER_TEXT_WRAP}>
                <Text style={[FONT_TEXT_BOLD, { fontSize: 40 }]}>
                  {Math.floor(compliance.total)}
                </Text>
                <Text style={[FONT_TEXT_BOLD, { fontSize: 24 }]}>%</Text>
              </View>
            </View>
          </ProgressCircle>
          <View style={ALIGN_CENTER}>
            <View style={[ROW, BUTTON_SPACE]}>
              <TouchableOpacity
                onPress={() => this.setState({ buttonActive: true })}
                style={[CENTER, GREY_BUTTON]}
              >
                <Text
                  style={[
                    FONT_TEXT_BOLD,
                    buttonActive ? { color: color.palette.black } : { color: color.palette.grey30 },
                  ]}
                >
                  시간대별
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({ buttonActive: false })}
                style={[CENTER, GREY_BUTTON]}
              >
                <Text
                  style={[
                    FONT_TEXT_BOLD,
                    !buttonActive
                      ? { color: color.palette.black }
                      : { color: color.palette.grey30 },
                  ]}
                >
                  약제별
                </Text>
              </TouchableOpacity>
            </View>
            {buttonActive ? (
              sortTimeslots.map((timeSlot, index) => {
                return timeSlot.compliance === null || 0 ? null : (
                  <View key={index} style={COMPLIANCE_TIMES_DRUGS_WRAP}>
                    <View style={TIMESLOTS_NAME_WRAP}>
                      <View style={{ marginRight: 4 }}>
                        {timeSlot.timeslot_name === "wakeup" ? (
                          <SvgWakeup />
                        ) : timeSlot.timeslot_name === "morning" ? (
                          <SvgMorning />
                        ) : timeSlot.timeslot_name === "afternoon" ? (
                          <SvgTimeAfternoonOrange />
                        ) : timeSlot.timeslot_name === "evening" ? (
                          <SvgTimeEveningOrange />
                        ) : timeSlot.timeslot_name === "night" ? (
                          <SvgNight />
                        ) : timeSlot.timeslot_name === "once" ? (
                          <SvgOnce />
                        ) : (
                          <SvgTimesCustom />
                        )}
                      </View>

                      <Text style={{ ...FONT_SIZE_13, lineHeight: 19 }}>
                        {timeSlot.timeslot_name === "wakeup"
                          ? "기상직후"
                          : timeSlot.timeslot_name === "morning"
                          ? "아침"
                          : timeSlot.timeslot_name === "afternoon"
                          ? "오후"
                          : timeSlot.timeslot_name === "evening"
                          ? "저녁"
                          : timeSlot.timeslot_name === "night"
                          ? "취침전"
                          : timeSlot.timeslot_name === "once"
                          ? `아무때나\n한번`
                          : timeSlot.timeslot_name}
                      </Text>
                    </View>

                    <View style={TIMES_SLIDER_VIEW}>
                      <Slider
                        disabled
                        style={SLIDER_TOTAL_BAR}
                        value={timeSlot.compliance}
                        thumbStyle={{ width: 0 }}
                        trackStyle={SLIDER_TRACK}
                        minimumTrackTintColor={color.palette.orange}
                        maximumValue={100}
                      />
                    </View>
                    <View style={TIMES_PERCENTAGE_VIEW}>
                      <Text style={GREY_TEXT}>
                        {timeSlot.compliance === null ? 0 : Math.floor(timeSlot.compliance)}%
                      </Text>
                    </View>
                  </View>
                )
              })
            ) : (
              <View>
                {compliance.prescription_details.map((item, index) => (
                  <View key={index} style={COMPLIANCE_TIMES_DRUGS_WRAP}>
                    <View style={DRUGS_NAMES_VIEW}>
                      <Text style={{ ...FONT_SIZE_13, lineHeight: 19 }}>{item.shortenedName}</Text>
                    </View>
                    <View style={DRUGS_SLIDER_VIEW}>
                      <Slider
                        disabled
                        style={SLIDER_TOTAL_BAR}
                        value={item.compliance}
                        thumbStyle={{ width: 0 }}
                        trackStyle={SLIDER_TRACK}
                        minimumTrackTintColor={
                          item.pause_take_dt ? color.palette.grey1 : color.palette.orange
                        }
                        maximumValue={100}
                      />
                    </View>
                    <View style={DRUGS_PERCENTAGE_VIEW}>
                      <Text style={GREY_TEXT}>{Math.floor(item.compliance)}%</Text>
                    </View>
                  </View>
                ))}
              </View>
            )}
          </View>
        </View>
      </View>
    )
  }

  renderForeground() {
    const { scroll } = this.state
    const titleOpacity = scroll.interpolate({
      inputRange: [0, 50, 100], // 0 , 106 , 154
      outputRange: [1, 1, 0],
      extrapolate: "clamp",
    })
    const { route } = this.props
    const { params } = route
    const { prescription } = params

    const startDate = DateTime.fromISO(prescription.start_at)
    const endDate = DateTime.fromISO(prescription.end_at)
    const dosingDays = endDate.diff(startDate, "days").days + 1

    let periodText = ""
    const repDiseaseName = !isEmpty(prescription.disease[0]?.one_liner)
      ? prescription.disease[0].one_liner
      : prescription.disease[0].name_kr
    const subDiseasesLength = prescription.subdiseases.length

    if (prescription.start_at) {
      const { start_at, end_at } = prescription
      if (dosingDays == 0 || dosingDays == undefined) {
        periodText = `${formatDateFromString(start_at)}`
      } else {
        periodText = `${formatDateFromString(start_at)} ~ ${formatDateFromString(end_at)}`
      }
    }
    return (
      <SafeAreaView>
        <Animated.View style={{ opacity: titleOpacity }}>
          <View style={BOTTOM_HEADER_WRAP}>
            <SvgPrescription style={THUMBNAIL} />
            <View>
              {subDiseasesLength < 1 ? (
                <Text style={typography.title2} allowFontScaling={false}>
                  {!repDiseaseName ? `질병명 없음` : `${repDiseaseName}`}
                </Text>
              ) : (
                <Text style={typography.title2} allowFontScaling={false}>
                  {prescription.disease.length === 0
                    ? `${
                        prescription.subdiseases[0]?.one_liner
                          ? prescription.subdiseases[0].one_liner
                          : prescription.subdiseases[0].name_kr
                      }`
                    : // : `${repDiseaseName} 외 ${subDiseasesLength}`}
                      `${repDiseaseName} 외 ${subDiseasesLength}`}
                </Text>
              )}
              <Text style={{ ...GREY_TEXT, marginTop: 4 }}>{periodText}</Text>
            </View>
          </View>
        </Animated.View>
      </SafeAreaView>
    )
  }

  renderHeader() {
    const { scroll } = this.state
    const { navigation, route } = this.props
    const { prescription } = route.params
    const opacity = scroll.interpolate({
      inputRange: [0, 50, 100], // 0, 160 , 210
      outputRange: [0, 0, 1],
      extrapolate: "clamp",
    })
    const repDiseaseName = !isEmpty(prescription.disease[0]?.one_liner)
      ? prescription.disease[0].one_liner
      : prescription.disease[0]?.name_kr
    const subDiseasesLength = prescription.subdiseases.length
    return (
      <SafeAreaView style={{ backgroundColor: color.palette.white }}>
        <View style={HEADER_WRAPPER}>
          <TouchableItem onPress={navigation.goBack}>
            <CustomHeaderBackImage />
          </TouchableItem>
          <Animated.View style={{ flex: 1, ...CENTER, opacity }}>
            {subDiseasesLength < 1 ? (
              <Text style={typography.title3}>
                {!repDiseaseName ? `질병명 없음` : `${repDiseaseName}`}
              </Text>
            ) : (
              <Text style={typography.title3}>
                {!repDiseaseName ? `질병명 없음` : `${repDiseaseName} 외 ${subDiseasesLength}`}
              </Text>
            )}
          </Animated.View>
          <View style={TO_FIT_CENTER_VIEW}></View>
        </View>
      </SafeAreaView>
    )
  }

  render() {
    const { scroll } = this.state
    return (
      <SafeAreaInsetsContext.Consumer>
        {(insets) => (
          <StickyParallaxHeader
            foreground={this.renderForeground()}
            bounces={false}
            header={this.renderHeader()}
            parallaxHeight={76} // 질병명, 복용기간 섹션 height
            headerHeight={insets.top + headerHeight}
            headerSize={() => {}}
            scrollEvent={Animated.event([{ nativeEvent: { contentOffset: { y: scroll } } }])}
          >
            {this.renderContent()}
          </StickyParallaxHeader>
        )}
      </SafeAreaInsetsContext.Consumer>
    )
  }
}
