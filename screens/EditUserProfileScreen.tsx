import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, ScrollView, Stack, Toast } from "native-base"
import React, { Component } from "react"
import {
  Alert,
  BackHandler,
  Dimensions,
  Image,
  ImageStyle,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { launchImageLibrary } from "react-native-image-picker"
import Modal from "react-native-modal"

import { api } from "@api/api"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { NavigationEvents } from "@react-navigation/compat"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import SvgApple from "../assets/images/apple.svg"
import { BackgroundImage, FastImage } from "../components/BackgroundImage"
import TouchableItem from "../components/button/TouchableItem"
import CloseButton from "../components/CloseButton"
import { getPopupButton, Popup } from "../components/Popup"
import { Button } from "../components/StyledButton"
import { Input } from "../components/StyledInput"
import { Text } from "../components/StyledText"
import { formatDateWithJoinChar } from "../lib/DateUtil"
import { MainTabParamList, MyPageStackParamList, RootStackParamList } from "../navigation/types"
import { User } from "../stores/account/User"
import { AccountStore } from "../stores/AccountStore"
import { TakingDrugStore } from "../stores/drug-to-take/TakingDrugStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

const cover = { uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png` }

const CONTENT_CONTAINER: ViewStyle = {
  backgroundColor: color.palette.gray5,
}
const PROFILE_NAME: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  paddingTop: 24,
  paddingBottom: 20,
  paddingHorizontal: 20,
  backgroundColor: color.palette.white,
  borderBottomColor: color.line,
  borderBottomWidth: 1,
  marginBottom: 8,
}
const INPUT: ViewStyle = {
  backgroundColor: color.palette.white,
  height: 40,
  marginBottom: 8,
  marginLeft: 0,
}
const PHONE_INPUT: TextStyle = {
  paddingLeft: 0,
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 24,
  fontWeight: "bold",
  color: color.palette.brownGrey,
}
const STACKED_LABEL: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 13,
  color: color.text,
  marginLeft: 15,
  marginBottom: 3,
  paddingBottom: 0,
}
const ROW: ViewStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
}
const BOTTOM_BUTTONS: ViewStyle = {
  // flex: 1,
  justifyContent: "flex-end",
  paddingBottom: 20,
  // alignItems: 'baseline',
}
const MODAL_CONTENT: ViewStyle = {
  backgroundColor: "white",
  margin: 42.5,
  padding: 16.9,
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 20,
  borderColor: "rgba(0, 0, 0, 0.1)",
}
const NEED_PASSWORD_TEXT: TextStyle = {
  fontSize: 15,
  letterSpacing: -0.6,
  marginBottom: 50,
}
const SNS_LOGO: ImageStyle = {
  width: 24,
  height: 24,
  borderRadius: 12,
  marginRight: 8,
}
const SETTING_ITEM: ViewStyle = {
  height: 56,
  flexDirection: "row",
  alignItems: "center",
  backgroundColor: "#fff",
  paddingHorizontal: 16,
}
const MENU_ITEM_DIVIDER = { height: 1, backgroundColor: color.palette.gray4, marginHorizontal: 16 }
const MENU_ITEM_GROUP = {
  backgroundColor: color.palette.white,
  borderTopColor: color.line,
  borderTopWidth: 1,
  borderBottomColor: color.line,
  borderBottomWidth: 1,
  marginBottom: 8,
}
const ROW_RIGHT: ViewStyle = {
  flex: 1,
  justifyContent: "center",
  alignItems: "flex-end",
  paddingRight: 8,
}
const ATTR_VALUE = { flex: 3.5, ...typography.body, opacity: 0.5 }
const ATTR_NAME = { width: 100, ...typography.body }
const ROW_VERT_CENTER: ViewStyle = { flexDirection: "row", alignItems: "center" }
const BUTTON: ViewStyle = {
  height: 56,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: color.palette.white,
  borderColor: color.line,
  borderTopWidth: 1,
  borderBottomWidth: 1,
  marginBottom: 8,
}
const BUTTON_TEXT = { fontFamily: typography.primary, fontSize: 16, color: color.primary }
const DELETE_ACCOUNT_ROW: ViewStyle = { flexDirection: "row", paddingTop: 8, paddingRight: 16 }
const FULL = { flex: 1 }
const DELETE_ACCOUNT_BUTTON = { padding: 8 }
const DELETE_ACCOUNT_TEXT = { ...typography.sub2, opacity: 0.5 }
const PROFILE_IMAGE_OVERLAY: ViewStyle = {
  width: 64,
  height: 64,
  alignItems: "flex-end",
  justifyContent: "flex-end",
}
const CHANGE_BUTTON = { width: 26, height: 26 }
const PROFILE_IMAGE = { width: 64, height: 64, borderRadius: 32 }
const kakaoImage = require("../assets/images/snsKakao.png")
const facebookImage = require("../assets/images/snsFacebook.png")
const naverImage = require("../assets/images/snsNaver.png")
const changeImage = require("../assets/images/profileChangeButton.png")

type EditUserProfileRouteProp = RouteProp<MyPageStackParamList, "EditUserProfile">

type EditUserProfileNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "EditUserProfile">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<MyPageStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  takingDrugStore: TakingDrugStore
  prescriptionStore: PrescriptionStore
  navigation: EditUserProfileNavigationProp
  route: EditUserProfileRouteProp
}

interface State {
  isMale: boolean
  imageUri: string
  imageType: any
  isModalVisible: boolean
  isInfoModal: boolean
  address: string
  fileName: any
}

@inject("accountStore", "prescriptionStore", "takingDrugStore")
@observer
export default class EditUserProfileScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route
    // @ts-ignore
    const prevRouteName = params ? params.prevRouteName : undefined
    if (prevRouteName) {
      return {
        title: "개인정보 수정",
        headerLeft: null,
        headerRight: () => (
          <CloseButton
            onPress={() => {
              navigation.pop()
              navigation.navigate("MyPage")
            }}
          />
        ),
      }
    } else {
      return {
        title: "개인정보 수정",
      }
    }
  }

  birthDate: Date = new Date()
  gender = ""
  user: User
  memberIndex = -1

  constructor(props: Props) {
    super(props)
    this.user = this.props.accountStore.user
    const { params } = this.props.route
    let imageUri
    let imageType
    let fileName
    if (params) {
      imageUri = this.getImageUri(params)
      imageType = this.getImageType(params)
      if (!String(imageUri).includes("content:")) {
        const tempArray = String(imageUri).split("/")
        fileName = tempArray[tempArray.length - 1]
      }
    } else if (this.user.profile_image && !String(imageUri).includes("content:")) {
      const tempArray = this.user.profile_image.split("/")
      fileName = tempArray[tempArray.length - 1]
    }
    this.state = {
      isMale: this.user.gender == "male",
      imageUri:
        params && imageUri ? imageUri : this.user.profile_image ? this.user.profile_image : "",
      imageType: params && imageType ? imageType : undefined,
      isModalVisible: false,
      isInfoModal: false,
      address: params ? this.getAddress(params) : this.user.address,
      fileName: fileName || undefined,
    }
  }

  getAddress(params: any): any {
    return params.address
  }

  getImageUri(params: any): any {
    return params.imageUri
  }

  getImageType(params: any): any {
    return params.imageType
  }

  UNSAFE_componentWillMount() {
    const accountStore = this.props.accountStore
    if (accountStore.user.username.length == 0) {
      accountStore.fetchUser()
    }
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress)
  }

  componentDidMount() {
    const { route } = this.props
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })
  }

  async componentWillUnmount() {
    const reqParams = new FormData()
    reqParams.append("user", this.user.id)
    reqParams.append("nickname", this.user.nickname)

    await api.updateProfile(this.props.accountStore.user.id, reqParams)

    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    const params = nextProps.route.params

    let imageType
    let imageUri
    if (params) {
      imageType = this.getImageType(params)
      imageUri = this.getImageUri(params)
      if (imageUri) {
        this.setState({
          imageUri: imageUri,
        })
      }
    }

    if (imageType) {
      this.submit
    }
  }

  handleBackPress = () => {
    this.props.navigation.pop()
    this.props.navigation.navigate("MyPage") // works best when the goBack is async
    return true
  }

  onChangeEmail = (email: string) => {
    this.user.setEmail(email)
  }

  onChangePassword = (password: string) => {
    this.user.setPassword(password)
  }

  submit = async () => {
    const reqParams = new FormData()
    const now = new Date()

    reqParams.append("user", this.user.id)
    reqParams.append("is_agree", this.user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
    if (this.state.address) {
      reqParams.append("address", this.state.address)
    }
    if (this.state.imageUri.length > 0) {
      reqParams.append("img", {
        uri: this.state.imageUri,
        type: this.state.imageType ? this.state.imageType : "image/jpeg",
        name: this.state.fileName
          ? this.state.fileName
          : this.props.accountStore.user.id + "_" + now.toISOString() + ".jpg",
      })
    }

    const result = await api.updateProfile(this.props.accountStore.user.id, reqParams)

    if (result.kind === "ok") {
      this.props.accountStore.user.setProfileImage(result.data.img)
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`저장하였습니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.pop()
        this.props.navigation.navigate("MyPage")
      }, 2000)
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      Toast.show({
        title: (
          <Text
            style={styles.TOAST_TEXT}
          >{`서버와의 연결이 원활하지 않습니다. 잠시 후 다시 시도해 주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.pop()
        this.props.navigation.navigate("MyPage")
      }, 2000)
    }
  }

  openInfoModal() {
    this.setState({
      isModalVisible: true,
      isInfoModal: true,
    })
  }

  _toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible })

  _handleAddress = (text: string) => {
    this.setState({ address: text })
  }

  _handleProfileImage = () => {
    console.warn("Not implemented")
  }

  launchImageLibrary() {
    launchImageLibrary({ mediaType: "photo", quality: 0.8, includeBase64: true }, (response) => {
      if (response.didCancel) {
        console.tron.log(
          "< EditUserProfileScreen > launchImageLibrary, User cancelled image picker",
        )
      } else if (response.errorMessage) {
        console.tron.log(
          "< EditUserProfileScreen > launchImageLibrary, ImagePicker errorMessage: ",
          response.errorMessage,
        )
      } else {
        this.setState({
          imageUri: response.assets[0].uri,
          imageType: response.assets[0].type,
        })
      }
    })
  }

  logout = async () => {
    const { accountStore, prescriptionStore, navigation, takingDrugStore } = this.props
    // start making calls
    const response = await api.apisauce.post(`/rest-auth/logout/`)
    if (!response.ok) {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    }

    const signPath = await AsyncStorage.getItem("signPath")
    const email = await AsyncStorage.getItem("email")

    const pushNotiEnabled = await AsyncStorage.getItem("pushNotiEnabled")
    const pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")
    const showedDurGuidance = await AsyncStorage.getItem("showedDurGuidance")
    const pinCode = await AsyncStorage.getItem("pinCode")
    const developMode = await AsyncStorage.getItem("developerMode")
    const previewSwipeableRowEnabled = await AsyncStorage.getItem("previewSwipeableRowEnabled")

    await AsyncStorage.clear()
    await AsyncStorage.setItem("initialRunning", JSON.stringify(false))
    if (email && signPath && signPath === "email") {
      await AsyncStorage.setItem("email", email)
    }
    if (pushNotiEnabled) {
      await AsyncStorage.setItem("pushNotiEnabled", pushNotiEnabled)
    }
    if (pinCodeEnabled) {
      await AsyncStorage.setItem("pinCodeEnabled", pinCodeEnabled)
      await AsyncStorage.setItem("pinCode", pinCode)
    }
    if (showedDurGuidance) {
      await AsyncStorage.setItem("showedDurGuidance", showedDurGuidance)
    }
    if (developMode) {
      await AsyncStorage.setItem("developerMode", developMode)
    }
    if (previewSwipeableRowEnabled) {
      await AsyncStorage.setItem("previewSwipeableRowEnabled", previewSwipeableRowEnabled)
    }
    api.apisauce.deleteHeader("Authorization")
    accountStore.initiate()
    takingDrugStore.initiate()
    prescriptionStore.initiate()

    if (signPath) {
      await AsyncStorage.setItem("signPath", signPath)
      accountStore.setSignPath(signPath) // must call after initiate()
    }
    navigation.popToTop()
    navigation.navigate("Auth", { screen: "SignIn" })
  }

  certificate = () => {
    this.props.navigation.navigate("CertificationDetail", {
      previousScreen: "EditUserProfile",
    })
  }

  deleteAccount = () => {
    this.props.navigation.navigate("DeleteAccount")
  }

  render() {
    const deviceWidth = Dimensions.get("window").width
    const deviceHeight =
      Platform.OS === "ios"
        ? Dimensions.get("window").height
        : require("react-native-extra-dimensions-android").get("REAL_WINDOW_HEIGHT")
    const { accountStore, navigation } = this.props

    return (
      <View style={{ flex: 1, backgroundColor: color.palette.gray5 }}>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={PROFILE_NAME}>
            <TouchableOpacity
              onPress={() =>
                // @ts-ignore
                navigation.navigate("ChangeProfileImage", {
                  prevRouteName: this.props.route.name,
                })
              }
            >
              <BackgroundImage
                resizeMode={FastImage.resizeMode.cover}
                image={PROFILE_IMAGE}
                source={this.state.imageUri.length > 0 ? { uri: this.state.imageUri } : cover}
              />
              <View style={PROFILE_IMAGE_OVERLAY}>
                <Image style={CHANGE_BUTTON} source={changeImage} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={MENU_ITEM_GROUP}>
            <TouchableItem
              style={SETTING_ITEM}
              onPress={() => navigation.navigate("EditNickname", { nickname: this.user.nickname })}
            >
              <View style={ROW}>
                <Text style={ATTR_NAME}>이름</Text>
                <Text style={ATTR_VALUE} numberOfLines={1} ellipsizeMode="tail">
                  {this.user.nickname}
                </Text>
                <View style={ROW_RIGHT}>
                  <Text style={typography.sub2}>수정</Text>
                </View>
              </View>
            </TouchableItem>
            <View style={MENU_ITEM_DIVIDER} />
            <View style={SETTING_ITEM}>
              <View style={ROW}>
                <Text style={ATTR_NAME}>생년월일</Text>
                <Text style={ATTR_VALUE}>
                  {this.user!.is_authenticated ? formatDateWithJoinChar(this.user.birth, "-") : "-"}
                </Text>
              </View>
            </View>
            <View style={MENU_ITEM_DIVIDER} />
            <TouchableItem
              style={SETTING_ITEM}
              onPress={() =>
                navigation.navigate("CertificationDetail", {
                  previousScreen: "EditUserProfile",
                })
              }
            >
              <View style={ROW}>
                <Text style={ATTR_NAME}>전화번호</Text>
                <Text style={ATTR_VALUE}>{this.user.phone}</Text>
                <View style={ROW_RIGHT}>
                  {this.user!.is_authenticated ? <Text style={typography.sub2}>재인증</Text> : null}
                </View>
              </View>
            </TouchableItem>
          </View>
          <View style={MENU_ITEM_GROUP}>
            {accountStore.signPath == "email" ? (
              <TouchableItem style={SETTING_ITEM} onPress={() => this.openInfoModal()}>
                <View style={ROW}>
                  <Text style={ATTR_NAME}>이메일</Text>
                  <Text style={ATTR_VALUE}>{this.user.email}</Text>
                </View>
              </TouchableItem>
            ) : (
              <View style={SETTING_ITEM}>
                <View style={ROW}>
                  <Text style={ATTR_NAME}>연동된 계정</Text>
                  {accountStore.signPath == "kakao" ? (
                    <View style={ROW_VERT_CENTER}>
                      <Image source={kakaoImage} style={SNS_LOGO} />
                      <Text style={ATTR_VALUE}>카카오 계정</Text>
                    </View>
                  ) : accountStore.signPath == "facebook" ? (
                    <View style={ROW_VERT_CENTER}>
                      <Image source={facebookImage} style={SNS_LOGO} />
                      <Text style={ATTR_VALUE}>페이스북 계정</Text>
                    </View>
                  ) : accountStore.signPath == "apple" ? (
                    <View style={ROW_VERT_CENTER}>
                      <SvgApple width={30} height={30} style={SNS_LOGO} />
                      <Text style={ATTR_VALUE}>애플 계정</Text>
                    </View>
                  ) : (
                    <View style={ROW_VERT_CENTER}>
                      <Image source={naverImage} style={SNS_LOGO} />
                      <Text style={ATTR_VALUE}>네이버 계정</Text>
                    </View>
                  )}
                </View>
              </View>
            )}
            {this.user!.is_authenticated && accountStore.signPath == "email" ? (
              <View>
                <View style={MENU_ITEM_DIVIDER} />
                <TouchableItem
                  style={SETTING_ITEM}
                  onPress={() => navigation.navigate("EditPassword")}
                >
                  <View style={ROW}>
                    <Text style={ATTR_NAME}>비밀번호 변경</Text>
                    <View style={ROW_RIGHT}>
                      <Text style={typography.sub2}>수정</Text>
                    </View>
                  </View>
                </TouchableItem>
              </View>
            ) : null}
          </View>

          {/* <View>
              <Text style={STACKED_LABEL}>주소</Text>
              <Stack rounded last style={[INPUT]}>
                <Text
                  style={[BIRTH_DATE_INPUT]}
                  // value={this.member.birth_date ? this.member.birth_date.toLocaleDateString("ko-KR", dateOptions) : ""}
                  onPress={() =>
                    navigation.navigate('EditAddress', {
                      email: this.user.email
                    })
                  }
                  >{this.state.address}</Text>
              </Stack>
            </View> */}

          <View style={BOTTOM_BUTTONS}>
            {this.user!.is_authenticated ? null : (
              <TouchableItem onPress={this.certificate} style={BUTTON}>
                <Text style={BUTTON_TEXT}>본인인증하기</Text>
              </TouchableItem>
            )}
            <TouchableItem onPress={this.logout} style={BUTTON}>
              <Text style={BUTTON_TEXT}>로그아웃</Text>
            </TouchableItem>
            <View style={DELETE_ACCOUNT_ROW}>
              <View style={FULL} />
              <TouchableItem onPress={this.deleteAccount} style={DELETE_ACCOUNT_BUTTON}>
                <Text style={DELETE_ACCOUNT_TEXT}>회원탈퇴</Text>
              </TouchableItem>
            </View>
          </View>

          <Modal
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            isVisible={this.state.isModalVisible}
            animationOut={"fadeOutDown"}
            // animationOutTiming={300}
            onBackdropPress={this._toggleModal}
          >
            {this.state.isInfoModal
              ? this.renderInfoModalContent()
              : this.renderSelectMemberModalContent()}
          </Modal>
          <NavigationEvents
            onWillFocus={() => {
              accountStore.fetchUser()
            }}
          />
        </ScrollView>
      </View>
    )
  }

  renderInfoModalContent = () =>
    Popup.renderPopupContent(
      undefined,
      "회원정보 변경 불가",
      "회원 가입시 등록된 정보는 서비스 이용특성 상 변경이 불가합니다.",
      getPopupButton(
        () =>
          this.setState({
            isModalVisible: false,
          }),
        "확인",
      ),
    )

  /**
   * 현재는 사용하지 않는 UI
   */
  renderSelectMemberModalContent = () => (
    <KeyboardAvoidingView style={MODAL_CONTENT} behavior="padding" enabled>
      <View style={ROW}>
        <View style={{ flex: 1 }}></View>
        <TouchableOpacity
          onPress={this._toggleModal}
          style={{ width: 24, height: 24, justifyContent: "flex-start", alignItems: "flex-end" }}
          hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}
        >
          <Image
            source={require("../assets/images/close.png")}
            style={{
              width: 15.7,
              height: 15.7,
              // resizeMode: 'contain',
            }}
          />
        </TouchableOpacity>
      </View>
      <View style={{ padding: 5, marginTop: 20 }}>
        <Text style={NEED_PASSWORD_TEXT}>개인정보를 수정하려면 비밀번호가 필요합니다.</Text>

        <View>
          <Text style={STACKED_LABEL}>비밀번호</Text>
          <Stack rounded last style={[INPUT, { height: 26, borderRadius: 13 }]}>
            <Input
              style={PHONE_INPUT}
              selectionColor={color.primary}
              // placeholder="비밀번호"
              placeholderTextColor={color.placeholder}
              // underlineColorAndroid={color.primary}
              onChangeText={this.onChangePassword}
              // @ts-ignore
              textContentType="password"
              secureTextEntry={true}
            />
          </Stack>
        </View>

        <View style={BOTTOM_BUTTONS}>
          {/* <Button light block rounded style={{ height: 42, marginBottom: 7 }} onPress={this._toggleModal}>
            <Text style={{ fontSize: 17, color: color.primary }}>취소</Text>
          </Button> */}

          <Button primary block rounded onPress={this.submit}>
            <Text style={{ fontSize: 17, fontWeight: "bold", color: "#fff" }}>확인</Text>
          </Button>
        </View>
      </View>
    </KeyboardAvoidingView>
  )

  cancel = () => {
    Alert.alert(
      "",
      "개인정보 수정을 취소하시겠습니까?",
      [
        { text: "아니오", onPress: () => null, style: "cancel" },
        {
          text: "네",
          onPress: () => {
            // this.setState({ isModalVisible: !this.state.isModalVisible })
            this.props.navigation.pop()
            this.props.navigation.navigate("MyPage")
          },
        },
      ],
      { cancelable: false },
    )
  }
}
