import { observer } from "mobx-react-lite"
import React, { useEffect, useLayoutEffect, useState } from "react"
import {
  FlatList,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"

import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import TouchableItem from "../components/button/TouchableItem"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { Screen } from "../components/screen/screen"
import { DetailsStackParamList } from "../navigation/types"
import { color, typography } from "../theme"

type RecordDetailScreenRouteProp = RouteProp<DetailsStackParamList, "RecordDetail">

type RecordDetailScreenNavigationProp = StackNavigationProp<DetailsStackParamList, "RecordDetail">

export interface RecordDetailScreenProps {
  navigation: RecordDetailScreenNavigationProp
  route: RecordDetailScreenRouteProp
}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
}
const HEADER_LEFT = { marginLeft: Platform.OS === "android" ? 10 : 0 }

const LINE_SEPARATOR = {
  height: 1,
  backgroundColor: color.palette.grey70,
  marginTop: 16,
  marginBottom: 15,
}
const ITEM_TEXT = {
  fontFamily: typography.primary,
  fontSize: 16,
  lineHeight: 24,
  color: color.palette.black,
}
const ITEM_CONTAINER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
}
const ITEM_NUMBER_DATE: ViewStyle = { flexDirection: "row", alignItems: "center" }

const CONTENT = { flex: 1, padding: 24 }
const ITEM_DATE = {
  fontFamily: typography.primary,
  fontSize: 14,
  lineHeight: 21,
  color: "#788893",
  marginLeft: 16,
}
const ITEM_SEPARATOR = {
  height: 1,
  backgroundColor: color.palette.veryLightPink3,
  marginVertical: 14,
}

export const RecordDetailScreen: React.FunctionComponent<RecordDetailScreenProps> = observer(
  (props) => {
    const { recordStore } = useStores()
    const { navigation, route } = props
    const { id } = route.params
    const [recordDetail, setRecordDetail] = useState([])
    const { count } = recordStore

    useLayoutEffect(() => {
      navigation.setOptions({
        // eslint-disable-next-line react/display-name
        headerLeft: () => (
          <TouchableItem onPress={navigation.goBack} style={HEADER_LEFT}>
            <CustomHeaderBackImage />
          </TouchableItem>
        ),
        headerTitle: route.params.title,
        headerStyle: { elevation: 1 },
      })
    }, [navigation])

    useEffect(() => {
      async function fetch() {
        await recordStore.fetchRecordDetail(id)
        const temp = await recordStore.recordDetail.map((item, index) => {
          item.id = index + 1
          return item
        })
        // 내림차순
        await temp.sort((a, b) => {
          return b.id - a.id
        })
        await setRecordDetail(temp)
      }
      fetch()
    }, [recordStore.seletedTab])

    const next = async () => {
      recordStore.next()
    }

    const convertDate = (date) => {
      const firstDate = date.toJSON().split("T")[0].replace(/-/g, ".")
      const middleDate = new Date(date.toJSDate()).getHours() < 12 ? "오전" : "오후"
      const lastDate = date
        .toJSON()
        .split("T")[1]
        .split(/\:\d\d\..+/)[0]

      return `${firstDate}. ${middleDate} ${lastDate}`
    }

    return (
      <Screen style={ROOT} preset="fixed" unsafe>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <View style={CONTENT}>
          <Text style={typography.body}>총 {count}회</Text>
          <View style={LINE_SEPARATOR} />
          <FlatList
            data={recordDetail}
            renderItem={({ item }) => (
              <View style={ITEM_CONTAINER}>
                <View style={ITEM_NUMBER_DATE}>
                  <Text style={ITEM_TEXT}>{item.id}회차</Text>
                  {/* <Text style={ITEM_DATE}>{ item.when.toLocaleString(DateTime.DATETIME_SHORT)}</Text> */}
                  <Text style={ITEM_DATE}>{convertDate(item.when)}</Text>
                </View>
                <View>
                  {recordStore.seletedTab !== "BREATHE" && (
                    <Text style={ITEM_TEXT}>{item.data}점</Text>
                  )}
                </View>
              </View>
            )}
            ItemSeparatorComponent={() => <View style={ITEM_SEPARATOR} />}
            keyExtractor={(item) => item.when.toString()}
            onEndReached={next}
            onEndReachedThreshold={2}
          />
        </View>
      </Screen>
    )
  },
)
