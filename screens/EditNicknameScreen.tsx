import { observer } from "mobx-react-lite"
import { FormControl, Input, Stack, Toast } from "native-base"
import React, { useState } from "react"
import { TextStyle, View, ViewStyle } from "react-native"

import { api } from "@api/api"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import { Screen } from "../components/screen/screen"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import { MainTabParamList, MyPageStackParamList, RootStackParamList } from "../navigation/types"
import { color, styles, typography } from "../theme"

type EditNicknameRouteProp = RouteProp<MyPageStackParamList, "EditNickname">

type EditNicknameNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "EditNickname">,
  CompositeNavigationProp<
    StackNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface EditNicknameScreenProps {
  navigation: EditNicknameNavigationProp
  route: EditNicknameRouteProp
}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingTop: 16,
  paddingBottom: 12,
  paddingHorizontal: 16,
}
const FORM_ITEM: ViewStyle = {
  marginTop: 32,
  marginLeft: 16,
  marginRight: 16,
}
const LABEL: TextStyle = {
  width: 76,
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  paddingRight: 0,
}
const INPUT = { fontFamily: typography.bold, fontSize: 16, color: color.text }
const TITLE = {
  fontFamily: typography.bold,
  fontSize: 20,
  lineHeight: 30,
  color: color.text,
  marginLeft: 16,
}
const BUTTON_TEXT = { fontFamily: typography.bold, fontSize: 17, color: color.palette.white }
const BOTTOM_BUTTONS: ViewStyle = { flex: 1, justifyContent: "flex-end" }

export const EditNicknameScreen: React.FunctionComponent<EditNicknameScreenProps> = observer(
  (props) => {
    const { accountStore } = useStores()
    const { navigation, route } = props
    const [nickname, setNickname] = useState(route.params.nickname)

    const submit = async () => {
      const reqParams = new FormData()

      reqParams.append("nickname", nickname)

      const result = await api.updateProfile(accountStore.user.id, reqParams)

      if (result.kind === "ok") {
        Toast.show({
          title: <Text style={styles.TOAST_TEXT}>저장하였습니다.</Text>,
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => {
          navigation.pop()
        }, 2000)
      } else if (result.kind === "unauthorized") {
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => navigation.navigate("Auth"), 2000)
      } else {
        Toast.show({
          title: (
            <Text style={styles.TOAST_TEXT}>
              서버와의 연결이 원활하지 않습니다. 잠시 후 다시 시도해 주세요.
            </Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => {
          navigation.pop()
        }, 2000)
      }
    }

    return (
      <Screen style={ROOT} preset="fixed" keyboardOffset="bottomButton" statusBar="dark-content">
        <Text style={TITLE}>이름 수정</Text>
        <Stack inlineLabel last style={FORM_ITEM}>
          <FormControl.Label style={LABEL}>이름</FormControl.Label>
          <Input
            autoCapitalize="none"
            selectionColor={color.text}
            onChangeText={(text) => setNickname(text)}
            value={nickname}
            style={INPUT}
          />
        </Stack>
        {route.params.nickname != nickname ? (
          <View style={BOTTOM_BUTTONS}>
            <Button block onPress={submit}>
              <Text style={BUTTON_TEXT}>확인</Text>
            </Button>
          </View>
        ) : null}
      </Screen>
    )
  },
)
