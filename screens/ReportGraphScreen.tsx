import { observer } from "mobx-react"
import { Spinner } from "native-base"
import React, { useEffect, useState } from "react"
import {
  Dimensions,
  SafeAreaView,
  ScrollView,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import {
  VictoryArea,
  VictoryAxis,
  VictoryChart,
  VictoryLabel,
  VictoryLine,
  VictoryScatter,
  VictoryZoomContainer,
} from "victory-native"

import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import { getPopupButton, Popup } from "../components/Popup"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { color } from "../theme"

type ReportGraphRouteProp = RouteProp<DetailsStackParamList, "ReportGraph">

type RecordsScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "Records">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface ReportGraphProps {
  navigation: RecordsScreenNavigationProp
  route: ReportGraphRouteProp
}

const CENTER: ViewStyle = { flex: 1, alignSelf: "center" }

const RootView: ViewStyle = {
  flex: 1,
  borderRadius: 20,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "#303550",
  paddingBottom: 10,
  marginLeft: 0,
}

const ReportName: ViewStyle = {
  width: "100%",
  paddingTop: 22,
  paddingBottom: 16,
  paddingLeft: 16,
  paddingRight: 27,
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "flex-start",
}

const ReportNameText: TextStyle = {
  fontSize: 24,
  color: "white",
  fontWeight: "bold",
}

const ReportDetail: ViewStyle = {
  flexDirection: "row",
  backgroundColor: "#2A2E47",
  borderRadius: 30,
  paddingTop: 10,
  paddingBottom: 10,
  paddingLeft: 20,
  paddingRight: 20,
}

const WidthMTView: ViewStyle = {
  marginTop: 20,
  alignItems: "center",
  width: "100%",
}

const InfoButton: ViewStyle = {
  flexDirection: "row",
  backgroundColor: "#2A2E47",
  borderRadius: 30,
  marginLeft: 4,
  paddingTop: 2,
  paddingBottom: 2,
  paddingLeft: 8,
  paddingRight: 8,
}

const TriangleIcon: ViewStyle = {
  backgroundColor: "transparent",
  borderStyle: "solid",
  borderLeftWidth: 8,
  borderRightWidth: 8,
  borderBottomWidth: 16,
  borderLeftColor: "transparent",
  borderRightColor: "transparent",
  opacity: 0.5,
}

const TriangleMiniIcon: ViewStyle = {
  position: "absolute",
  top: -18,
  borderLeftWidth: 8,
  borderRightWidth: 8,
  borderBottomWidth: 16,
  borderLeftColor: "transparent",
  borderRightColor: "transparent",
  opacity: 0.5,
}

const DateButton: ViewStyle = {
  flexDirection: "row",
  backgroundColor: "#2A2E47",
  borderRadius: 30,
  paddingTop: 14,
  paddingBottom: 14,
  paddingLeft: 22,
  paddingRight: 22,
}

const ReportGraphScreen: React.FunctionComponent<ReportGraphProps> = observer((props) => {
  const { recordStore } = useStores()
  const [surveyType, setSurveyType] = useState("")
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [popupTilte, setPopupTitle] = useState("")
  const [popupMessage, setPopupMessage] = useState("")
  const [graphAreaMin, setGraphAreaMin] = useState(0)
  const [graphAreaMax, setGraphAreaMax] = useState(0)
  const [graphViewWidth, setGraphViewWidth] = useState(0)
  const [minLinePosition, setMinLinePosition] = useState(0)
  const [maxLinePosition, setMaxLinePosition] = useState(0)

  useEffect(() => {
    async function fetchAndSetUser() {
      await recordStore.fetchGraphDetail(recordStore.seletedTabNumber)
      await updateGraph()
    }
    fetchAndSetUser()

    if (recordStore.seletedTab === "BREATHE") {
      // 호흡 운동
      setSurveyType("BREATHE")
    } else if (recordStore.seletedTab === "CAT") {
      // 만성폐쇄성 폐질환
      setSurveyType("CAT")
      setPopupTitle("만성폐쇄성폐질환 평가검사 점수")
      setPopupMessage(
        `CAT(만성폐쇄성 폐질환 평가검사) 점수는 회원님의 삶의 질이 어떠한지를 나타냅니다. 삶의 질이 가장 좋은 상태가 0점이며, 삶의 질이 가장 나쁜 상태가 40점입니다. 점수가 높아질수록 삶의 질이 나쁜 것을 의미합니다. 일반적으로 점수가 10점이하이면 저증상군, 10점 이상이면 고증상군에 속합니다.`,
      )
    } else if (recordStore.seletedTab === "ACT") {
      // 천식 조절 평가
      setSurveyType("ACT")
      setPopupTitle("천식조절검사 점수")
      setPopupMessage(
        `ACT(천식조절검사) 점수는 회원님의 천식 조절 상태가 어떠한지를 나타냅니다. 20점이하는 조절이 안되는 상태, 21 ~ 24점은 조절 근접 상태, 25점은 조절 달성 상태로 평가됩니다.`,
      )
    }
  }, [recordStore.seletedTabNumber])

  // 그래프 데이터 주입
  const updateGraph = () => {
    let filterData = null
    if (surveyType === "BREATHE") {
      filterData = recordStore?.records?.filter(
        (item) => item.id === recordStore?.seletedTabNumber,
      )[0]
    } else if (surveyType === "CAT") {
      filterData = recordStore?.records?.filter(
        (item) => item.id === recordStore?.seletedTabNumber,
      )[0]
    } else {
      filterData = recordStore?.records?.filter(
        (item) => item.id === recordStore?.seletedTabNumber,
      )[0]
    }

    setGraphAreaMin(filterData?.survey?.normal_range_start)
    setGraphAreaMax(filterData?.survey?.normal_range_end)
  }

  // 만성폐쇄성 폐질환 최소, 최대 상단 그래프
  useEffect(() => {
    if (recordStore?.graphDetail?.min >= 0 && recordStore?.graphDetail?.max > 0) {
      setMinLinePosition(
        (recordStore?.graphDetail?.min * graphViewWidth) / (surveyType === "CAT" ? 40 : 25),
      )
      setMaxLinePosition(
        (recordStore?.graphDetail?.max * graphViewWidth) / (surveyType === "CAT" ? 40 : 25),
      )
    }
  }, [recordStore?.graphDetail?.min, recordStore?.graphDetail?.max, graphViewWidth])

  // 만성폐쇄성 폐질환용 View 넓이 구하는 함수
  const onGraphViewWidth = (event) => {
    const { width } = event.nativeEvent.layout
    setGraphViewWidth(width)
  }

  const victoryData = () => {
    // victory native 그래프에서 데이터가 1개만 존재할 경우
    // 좌측 Y 축의 데이터가 이상하게 나오는 증상이 있음
    // 데이터가 1개일 경우 count를 string으로 변환
    if (recordStore?.graphDate === "daily") {
      if (recordStore.graphDaily.length === 1) {
        const array = recordStore.graphDaily
        const filtered = array.map((item) => {
          let obj = {}
          obj["date"] = item.date
          obj["count"] = String(item.count)
          obj["avg"] = item.avg
          return obj
        })
        return filtered
      } else {
        return recordStore.graphDaily.slice()
      }
    } else {
      if (recordStore.graphMonthly.length === 1) {
        const array = recordStore.graphMonthly
        const filtered = array.map((x) => {
          let obj = {}
          obj["date"] = x.date
          obj["count"] = String(x.count)
          obj["avg"] = x.avg
          return obj
        })
        return filtered
      } else {
        return recordStore.graphMonthly.slice()
      }
    }
  }

  const deviceWidth = Dimensions.get("window").width
  const deviceHeight = Dimensions.get("window").height

  return (
    <ScrollView>
      <SafeAreaView>
        {recordStore.status !== "done" && recordStore.graphStatus !== "done" ? (
          <Spinner color={color.primary} style={CENTER} />
        ) : (
          <View style={RootView}>
            {/* 리포트 이름 및 자세히 */}
            <View style={ReportName}>
              <View style={{ flexDirection: "column", width: "75%" }}>
                <Text style={ReportNameText}>
                  {/* {props.route.title} {props.route.title.length > 6 ? null : "리포트"} */}
                  {props.route.title}
                  {` `}리포트
                </Text>
                {/* <Text style={ReportNameText}>
                  {props.route.title.length <= 6 ? null : "리포트"}
                </Text> */}
              </View>
              <View style={{ width: "25%", alignItems: "center", justifyContent: "center" }}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate("RecordDetail", {
                      id: Number(props.route.key),
                      title: props.route.title,
                    })
                  }}
                  hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                >
                  <View style={ReportDetail}>
                    <Text style={{ color: "#707070" }}>자세히</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            {/* 만성폐쇄성 폐질환 평가 or 천식 조절 검사 */}
            {surveyType === "CAT" || surveyType === "ACT" ? (
              <View style={WidthMTView}>
                {/* 평균, 점수 */}
                <Text style={{ color: "#C2C1C1", fontSize: 18, marginBottom: 4 }}>평균</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "white", fontSize: 36, fontWeight: "bold" }}>
                    {`   `}
                    {recordStore?.graphDetail?.avg}점
                  </Text>
                  <TouchableOpacity
                    onPress={() => setIsPopupVisible(true)}
                    hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                  >
                    <View style={InfoButton}>
                      <Text style={{ color: "#707070" }}>i</Text>
                    </View>
                  </TouchableOpacity>
                </View>

                {/* 최소, 최대 */}
                <View
                  style={{
                    marginTop: 20,
                    flexDirection: "row",
                    justifyContent: "center",
                    width: "100%",
                  }}
                >
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={[TriangleIcon, { borderBottomColor: "#66CE54" }]} />
                    <Text style={{ color: "white", fontSize: 18, marginLeft: 12 }}>
                      Min : {recordStore?.graphDetail?.min}점
                    </Text>
                  </View>

                  <View>
                    <Text>{`        `}</Text>
                  </View>

                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={[TriangleIcon, { borderBottomColor: "#FF7042" }]} />
                    <Text style={{ color: "white", fontSize: 18, marginLeft: 12 }}>
                      Max : {recordStore?.graphDetail?.max}점
                    </Text>
                  </View>
                </View>

                {/* 최소 최대 그래프 */}
                <View
                  style={{
                    marginTop: 20,
                    backgroundColor: "#2A2E47",
                    borderRadius: 16,
                    width: "85%",
                    height: 120,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ flexDirection: "column", marginRight: 16 }}>
                      {surveyType === "CAT" ? (
                        <>
                          <Text style={{ color: "#66CE54", opacity: 0.8, marginBottom: 2 }}>
                            좋
                          </Text>
                          <Text style={{ color: "#66CE54", opacity: 0.8, marginTop: 2 }}>음</Text>
                        </>
                      ) : (
                        <>
                          <Text style={{ color: "#FF7042", opacity: 0.8, marginBottom: 2 }}>
                            나
                          </Text>
                          <Text style={{ color: "#FF7042", opacity: 0.8, marginTop: 2 }}>쁨</Text>
                        </>
                      )}
                    </View>

                    <View
                      style={{
                        backgroundColor: "#303550",
                        borderRadius: 8,
                        width: "70%",
                        height: 50,
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                          alignItems: "center",
                          height: "100%",
                        }}
                        onLayout={onGraphViewWidth}
                      >
                        <View style={{ flexDirection: "row" }}>
                          {/* 0 */}
                          <View style={{ justifyContent: "center" }}>
                            <Text
                              style={{
                                position: "absolute",
                                color: "white",
                                opacity: 0.2,
                                fontSize: 16,
                                left: 6,
                              }}
                            >
                              0
                            </Text>
                          </View>
                          <View
                            style={{
                              position: "absolute",
                              backgroundColor: "#EBEAEA",
                              opacity: 0.1,
                              height: "100%",
                              left: minLinePosition,
                              width: maxLinePosition - minLinePosition - 0.5,
                            }}
                          >
                            <Text />
                          </View>
                          {/* 최소선 */}
                          {surveyType === "CAT" ? (
                            <View
                              style={{
                                height: 50,
                                borderWidth: 1,
                                borderRadius: 1,
                                borderStyle: "dashed",
                                borderColor: "#66CE54",
                                left: minLinePosition,
                              }}
                            >
                              <Text />
                            </View>
                          ) : (
                            <View
                              style={{
                                height: 50,
                                borderWidth: 1,
                                borderRadius: 1,
                                borderStyle: "dashed",
                                borderColor: "#FF7042",
                                left: minLinePosition,
                              }}
                            >
                              <Text />
                            </View>
                          )}
                          {/* 최대선 */}
                          {surveyType === "CAT" ? (
                            <View
                              style={{
                                height: 50,
                                borderWidth: 1,
                                borderRadius: 1,
                                borderStyle: "dashed",
                                borderColor: "#FF7042",
                                left: maxLinePosition - 1,
                              }}
                            >
                              <Text />
                            </View>
                          ) : (
                            <View
                              style={{
                                height: 50,
                                borderWidth: 1,
                                borderRadius: 1,
                                borderStyle: "dashed",
                                borderColor: "#66CE54",
                                left: maxLinePosition - 1,
                              }}
                            >
                              <Text />
                            </View>
                          )}
                        </View>

                        {/* 40 */}
                        <View>
                          <Text
                            style={{ color: "white", opacity: 0.2, fontSize: 16, marginRight: 6 }}
                          >
                            {surveyType === "CAT" ? 40 : 25}
                          </Text>
                        </View>
                        {/* 기준선 */}
                        <View
                          style={{
                            height: "130%",
                            borderRightWidth: 3,
                            borderRightColor: "#788893",
                            position: "absolute",
                            left: graphViewWidth / 2,
                          }}
                        >
                          <Text />
                        </View>
                      </View>

                      {/* 최소, 초대값 삼각형 */}
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          height: "100%",
                        }}
                      >
                        <View style={{ flexDirection: "row" }}>
                          {/* 최소값 삼각형*/}
                          <View
                            style={[
                              TriangleMiniIcon,
                              {
                                borderBottomColor: surveyType === "CAT" ? "#66CE54" : "#FF7042",
                                left: minLinePosition - 7,
                              },
                            ]}
                          />
                          {/* 최대값 삼각형 */}
                          <View
                            style={[
                              TriangleMiniIcon,
                              {
                                borderBottomColor: surveyType === "CAT" ? "#FF7042" : "#66CE54",
                                left: maxLinePosition - 6,
                              },
                            ]}
                          />
                        </View>
                      </View>
                    </View>
                    <View style={{ flexDirection: "column", marginLeft: 16 }}>
                      {surveyType === "CAT" ? (
                        <>
                          <Text style={{ color: "#FF7042", opacity: 0.8, marginBottom: 2 }}>
                            나
                          </Text>
                          <Text style={{ color: "#FF7042", opacity: 0.8, marginTop: 2 }}>쁨</Text>
                        </>
                      ) : (
                        <>
                          <Text style={{ color: "#66CE54", opacity: 0.8, marginBottom: 2 }}>
                            좋
                          </Text>
                          <Text style={{ color: "#66CE54", opacity: 0.8, marginTop: 2 }}>음</Text>
                        </>
                      )}
                    </View>
                  </View>
                </View>
              </View>
            ) : null}

            {/* 호흡 운동 */}
            {surveyType === "BREATHE" ? (
              <View style={WidthMTView}>
                {/* 총 운동 수 */}
                <View
                  style={{
                    backgroundColor: "#2A2E47",
                    paddingTop: 16,
                    paddingBottom: 16,
                    width: "85%",
                    borderRadius: 14,
                    justifyContent: "center",
                    alignContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text style={{ color: "white", fontSize: 18, marginBottom: 6 }}>총 운동</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "white", fontSize: 36, fontWeight: "bold" }}>
                      {recordStore?.graphDetail?.count}회
                    </Text>
                  </View>
                </View>
              </View>
            ) : null}

            {/* 일별, 월별, 기간별 평균 */}
            <View
              style={{
                width: "90%",
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center",
                marginTop: 50,
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  onPress={() => recordStore.setGraphDate("daily")}
                  hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                >
                  <View
                    style={[
                      DateButton,
                      {
                        marginRight: 6,
                      },
                    ]}
                  >
                    <Text
                      style={
                        recordStore?.graphDate === "daily"
                          ? { color: "white", fontWeight: "bold", fontSize: 16 }
                          : { color: "#707070", fontWeight: "bold", fontSize: 16 }
                      }
                    >
                      일별
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => recordStore.setGraphDate("monthly")}
                  hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                >
                  <View
                    style={[
                      DateButton,
                      {
                        marginLeft: 6,
                      },
                    ]}
                  >
                    <Text
                      style={
                        recordStore?.graphDate === "monthly"
                          ? { color: "white", fontWeight: "bold", fontSize: 16 }
                          : { color: "#707070", fontWeight: "bold", fontSize: 16 }
                      }
                    >
                      월별
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View>
                <Text style={{ color: "#FFF", opacity: 0.3, fontSize: 16 }}>* 기간별 평균</Text>
              </View>
            </View>

            {/* 만성폐쇄성 폐질환 & 천식 차트 */}
            {surveyType === "CAT" || surveyType === "ACT" ? (
              <VictoryChart
                width={deviceWidth}
                height={deviceHeight / 2}
                domainPadding={{ x: 18, y: 30 }}
                // containerComponent={
                //   <VictoryZoomContainer
                //     allowZoom={true}
                //     zoomDimension="x"
                //     zoomDomain={{
                //       x:
                //         recordStore.graphDate === "daily"
                //           ? [0.5, recordStore.graphDaily.length + 0.5]
                //           : [0.5, recordStore.graphMonthly.length + 0.5],
                //     }}
                //   />
                // }
              >
                {surveyType === "CAT" && (
                  <VictoryArea
                    style={{
                      data: {
                        fill: "#FF7042",
                        opacity: 0.07,
                      },
                    }}
                    data={[
                      { date: 10, avg: 40 },
                      { date: 0, avg: 40 },
                    ]}
                    x="date"
                    y="avg"
                  />
                )}
                {surveyType === "CAT" && (
                  <VictoryArea
                    style={{
                      data: {
                        fill: "#344150",
                      },
                    }}
                    data={[
                      { date: 10, avg: 10 },
                      { date: 0, avg: 10 },
                    ]}
                    x="date"
                    y="avg"
                  />
                )}

                {surveyType === "ACT" && (
                  <VictoryArea
                    style={{
                      data: {
                        fill: "#FF7042",
                        opacity: 0.07,
                      },
                    }}
                    data={victoryData()}
                    x="date"
                    y="avg"
                  />
                )}

                <VictoryLine
                  style={{
                    data: { stroke: "#FF7042", strokeWidth: 3 },
                    labels: {
                      fill: "#FF7042",
                      fontSize: 13,
                      fontWeight: "bold",
                    },
                  }}
                  data={victoryData()}
                  labels={({ datum }) => `${datum.avg} 점`}
                  labelComponent={<VictoryLabel renderInPortal={false} dy={-15} />}
                  x="date"
                  y="avg"
                />

                <VictoryAxis
                  style={{
                    axis: {},
                    tickLabels: {
                      fill: "#C2C1C1",
                      fontSize: 13,
                    },
                    grid: {
                      stroke: "#000",
                      strokeWidth: 0.5,
                      pointerEvents: "painted",
                    },
                  }}
                  dependentAxis
                />

                <VictoryAxis
                  style={{
                    axis: {
                      stroke: "#1E233E",
                      marginBottom: 20,
                      paddingBottom: 20,
                    },
                    tickLabels: {
                      fill: "white",
                      fontSize: 14,
                    },
                  }}
                />
                <VictoryScatter
                  style={{
                    data: {
                      fill: "#303550",
                      fillOpacity: 1,
                      stroke: "#FF7042",
                      strokeWidth: 3,
                    },
                  }}
                  data={victoryData()}
                  size={7}
                  x="date"
                  y="avg"
                />
              </VictoryChart>
            ) : null}

            {/* 호흡 운동 차트 */}
            {surveyType === "BREATHE" ? (
              <VictoryChart
                width={deviceWidth}
                height={deviceHeight / 2}
                domainPadding={{ x: 18, y: 30 }}
              >
                <VictoryLine
                  style={{
                    data: { stroke: "#5AC9EF", strokeWidth: 3 },
                    labels: {
                      fill: "#5AC9EF",
                      fontSize: 13,
                      fontWeight: "bold",
                    },
                  }}
                  data={victoryData()}
                  labels={({ datum }) => `${datum.count} 회`}
                  labelComponent={<VictoryLabel renderInPortal={false} dy={-15} />}
                  x="date"
                  y="count"
                />
                <VictoryAxis
                  style={{
                    tickLabels: {
                      fill: "#C2C1C1",
                      fontSize: 13,
                    },
                    grid: {
                      stroke: "#000",
                      strokeWidth: 0.5,
                      pointerEvents: "painted",
                    },
                  }}
                  dependentAxis
                />
                <VictoryAxis
                  style={{
                    tickLabels: {
                      fill: "white",
                      fontSize: 14,
                    },
                  }}
                />
                <VictoryScatter
                  style={{
                    data: {
                      fill: "#303550",
                      fillOpacity: 1,
                      stroke: "#5AC9EF",
                      strokeWidth: 3,
                    },
                  }}
                  data={victoryData()}
                  size={7}
                  x="date"
                  y="count"
                />
              </VictoryChart>
            ) : null}

            {/* Legend */}
            {surveyType === "CAT" ? (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 10,
                  marginBottom: 60,
                }}
              >
                <View
                  style={{
                    width: 12,
                    height: 12,
                    backgroundColor: "#FF7042",
                    opacity: 0.1,
                    marginRight: 10,
                  }}
                />
                <Text style={{ color: "white", opacity: 0.3 }}>고증상</Text>
                <View style={{ marginLeft: 12, marginRight: 12 }} />
                <View
                  style={{
                    width: 12,
                    height: 12,
                    backgroundColor: "#66CE54",
                    opacity: 0.1,
                    marginRight: 10,
                  }}
                />
                <Text style={{ color: "white", opacity: 0.3 }}>저증상</Text>
              </View>
            ) : (
              <View style={{ marginBottom: 60 }}>
                <Text />
              </View>
            )}

            {/* 팝업 */}
            <Popup
              isVisible={isPopupVisible}
              title={popupTilte}
              message={popupMessage}
              button1={getPopupButton(() => setIsPopupVisible(false), "확인", null)}
            />
          </View>
        )}
      </SafeAreaView>
    </ScrollView>
  )
})

export default ReportGraphScreen
