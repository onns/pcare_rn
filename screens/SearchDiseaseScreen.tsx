import debounce from "lodash.debounce"
import { observer } from "mobx-react"
import { Spinner } from "native-base"
import React, { useCallback, useEffect, useRef, useState } from "react"
import {
  FlatList,
  Keyboard,
  StatusBar,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { useSafeAreaInsets } from "react-native-safe-area-context"

import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp, useNavigation } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import SvgDisease from "../assets/images/disease.svg"
import SearchIcon from "../assets/images/searchIcon.svg"
import { Screen } from "../components/screen/screen"
import { Button } from "../components/StyledButton"
import { Input } from "../components/StyledInput"
import { sendLogEvent } from "../lib/analytics"
import { HomeStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { Disease } from "../stores/Disease"
import { color, typography } from "../theme"

type SearchDiseaseScreenRouteProp = RouteProp<HomeStackParamList, "SearchDisease">
type SearchDiseaseScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "SearchDisease">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>
export interface SearchDiseaseScreenProps {
  navigation: SearchDiseaseScreenNavigationProp
  route: SearchDiseaseScreenRouteProp
  mode: SearchDiseaseMode | undefined
}

const SEARCHBOX_INPUT: ViewStyle = {
  height: 50,
  flexDirection: "row",
  backgroundColor: color.palette.veryLightPink3,
  borderRadius: 5,
  borderWidth: 1,
  borderColor: color.palette.gray4,
  marginLeft: 16,
  marginRight: 16,
  marginBottom: 17,
  alignItems: "center",
  padding: 10,
}
const TITLE2_ORANGE: TextStyle = {
  ...typography.body,
  fontFamily: typography.bold,
  color: color.primary,
}
const SEARCH_INPUT: TextStyle = {
  ...typography.title3,
  flex: 1,
  marginLeft: 5,
  height: 50,
  fontWeight: "bold",
}
const BODY_CONTAINER: ViewStyle = {
  flex: 1,
  backgroundColor: color.palette.white,
}
const SECTION_HEADER: ViewStyle = {
  paddingBottom: 16,
  paddingHorizontal: 16,
}
const HEADER_LINE: ViewStyle = {
  borderBottomColor: color.line,
  borderBottomWidth: 1,
}
const SEARCH_RESULTS_CONTAINER: ViewStyle = {
  flex: 9,
  paddingTop: 16,
}
const ITEM_BUTTON: ViewStyle = {
  minHeight: 80,
  flexDirection: "row",
  paddingLeft: 16,
  paddingRight: 8,
  paddingTop: 16,
}
const ITEM_BUTTON_RIGHT = {
  flex: 1,
  marginLeft: 8,
  marginBottom: 4,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const NAME = { ...typography.title3, paddingRight: 18 }
const ONE_LINER = { paddingRight: 16, marginBottom: 16 }
const DISEASE_CODE = {
  fontFamily: typography.bold,
  fontSize: 13,
  color: color.palette.grey50,
}
const DRUG_LINE = {
  backgroundColor: color.palette.pale,
  height: 1,
  marginLeft: 16,
  marginRight: 8,
}
const BOTTOM_BUTTONS: ViewStyle = {
  position: "absolute",
  bottom: 0,
  width: "100%",
}
const CONFIRM_BUTTON = {
  marginHorizontal: 16,
  backgroundColor: color.palette.brightOrange,
}
const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
  color: color.palette.white,
}
const CENTER: ViewStyle = { flex: 1, alignSelf: "center" }
const ICON_DISEASE: ViewStyle = { alignSelf: "flex-start", marginTop: 4 }
const ICON_SEARCH = { fontSize: 24 }
const LIMIT = 40

export enum SearchDiseaseMode {
  /** 통합 검색용 */
  Search,
  /** 처방전 수정 또는 직접 입력 시 */
  AddOrEditPrescription,
}

const Item = ({
  item,
  onPress,
  style,
}: {
  item: Disease
  onPress: () => void
  style: ViewStyle
}) => (
  <TouchableOpacity style={[ITEM_BUTTON, style]} onPress={onPress}>
    <SvgDisease width={32} height={32} style={ICON_DISEASE} />
    <View style={ITEM_BUTTON_RIGHT}>
      <View style={ROW}>
        <Text style={NAME} numberOfLines={2} ellipsizeMode="tail">
          {item.name} <Text style={DISEASE_CODE}>{item.code}</Text>
        </Text>
      </View>
      <View style={ONE_LINER}>
        <Text style={typography.sub2}>{item.feature}</Text>
      </View>
      <View style={ROW}></View>
    </View>
  </TouchableOpacity>
)

export const SearchDiseaseScreen: React.FunctionComponent<SearchDiseaseScreenProps> = observer(
  (props) => {
    const { mode } = props

    const navigation = useNavigation()
    const insets = useSafeAreaInsets()
    const { searchDiseaseStore: searchStore } = useStores()
    const { count, diseases, prevQuery } = searchStore

    const resultsRef = useRef<FlatList<Disease>>(null)
    const [selectedItem, setSelectedItem] = useState<Disease>()
    const [query, setQuery] = useState<string>()

    useEffect(() => {
      searchStore.initiate()
    }, [])

    const search = async () => {
      if (query.length === 0) return

      if (prevQuery !== query) {
        setSelectedItem(null)
        searchStore.search("disease", query, 1, LIMIT)
      } else {
        searchStore.next()
      }
    }

    // A debounce function that delays function invocation by 500 milliseconds.
    const delayedQuery = useCallback(debounce(search, 500), [query])

    // Calls delayedQuery() whenever query or delayedQuery values changes
    useEffect(() => {
      delayedQuery()
      // Cancel previous debounce calls during useEffect cleanup.
      return () => {
        delayedQuery.cancel()
      }
    }, [query, delayedQuery])

    const onChangeKeywords = (value: string) => {
      setQuery(value)
    }

    const addItem = () => {
      navigation.pop()
    }

    const renderItem = ({ item }: { item: Disease }) => {
      const backgroundColor = item.id === selectedItem?.id ? color.palette.veryLightPink3 : null
      return (
        <Item
          item={item}
          onPress={() => {
            sendLogEvent("select_content", {
              content_type: "disease",
              item_id: item.id,
              item_list_name: item.name,
            })
            if (mode === SearchDiseaseMode.Search) {
              navigation.navigate("DiseaseSummary", { id: item.id, code: item.code })
            } else {
              setSelectedItem(item)
              searchStore.setSelectedDisease(item)
              Keyboard.dismiss()
            }
          }}
          style={{ backgroundColor }}
        />
      )
    }

    return (
      <Screen style={BODY_CONTAINER} unsafe>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <View style={SEARCHBOX_INPUT}>
          {/* <Icon name="ios-search" style={ICON_SEARCH} /> */}
          <SearchIcon />
          <Input
            variant="unstyled"
            style={SEARCH_INPUT}
            placeholder="질병명 혹은 질병분류기호로 검색"
            placeholderTextColor={color.placeholder}
            onChangeText={onChangeKeywords}
            value={query}
          />
        </View>
        {/* <View style={HEADER_LINE} /> */}
        <View
          style={{
            ...SEARCH_RESULTS_CONTAINER,
            paddingBottom: insets.bottom + (selectedItem ? 56 : 0),
          }}
        >
          <View style={SECTION_HEADER}>
            <Text style={typography.body}>
              검색결과 <Text style={TITLE2_ORANGE}>{count}</Text>
            </Text>
          </View>
          {searchStore.status === "pending" ? (
            <Spinner color={color.primary} style={CENTER} />
          ) : (
            <FlatList<Disease>
              ref={resultsRef}
              data={diseases}
              renderItem={renderItem}
              ItemSeparatorComponent={() => <View style={DRUG_LINE} />}
              keyExtractor={(item) => item.id.toString()}
              keyboardShouldPersistTaps="handled" // Input 콤포넌트와 함께 있을 때 포커싱 문제 해결
              onEndReached={search}
              onEndReachedThreshold={3}
            />
          )}
        </View>
        {selectedItem && (
          <View style={{ ...BOTTOM_BUTTONS, paddingBottom: insets.bottom + 12 }}>
            <Button block style={CONFIRM_BUTTON} onPress={debounce(addItem, 300)}>
              <Text style={BUTTON_TEXT}>확인</Text>
            </Button>
          </View>
        )}
      </Screen>
    )
  },
)
