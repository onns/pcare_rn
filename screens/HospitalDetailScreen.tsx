import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { ScrollView, Spinner, View } from "native-base"
import React, { Component } from "react"
import { Linking, Platform, TextStyle, TouchableHighlight, ViewStyle } from "react-native"
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { Button } from "../components/StyledButton"
// import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { Text } from "../components/StyledText"
import {
  DetailsStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { HospitalStore } from "../stores/HospitalStore"
import { color } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  paddingTop: 10,
}

const DISEASE_TITLE: TextStyle = {
  fontSize: 20,
  fontWeight: "bold",
  color: color.text,
  paddingBottom: 6,
}

const CONTENT_HEADER: ViewStyle = {
  backgroundColor: "#fff",
  marginBottom: 10,
  paddingTop: 27,
  paddingLeft: 20,
  paddingBottom: 19,
}

const CONTENT_TEXT_WRAP: TextStyle = {
  fontSize: 15,
  letterSpacing: -0.3,
  color: color.text,
  lineHeight: 24,
}

const BOLD: TextStyle = {
  fontWeight: "bold",
}

const MAP: ViewStyle = {
  height: 150,
  paddingHorizontal: 20,
}

const BOTTOM_BUTTON: ViewStyle = {
  // flex: 1,
  flexDirection: "column",
  justifyContent: "flex-end",
  // alignItems: 'stretch',
  marginBottom: 20,
}

type HospitalDetailRouteProp = RouteProp<HomeStackParamList, "HospitalDetail">

type HospitalDetailNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "HospitalDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  hospitalStore: HospitalStore
  navigation: HospitalDetailNavigationProp
  route: HospitalDetailRouteProp
}

interface State {
  index: number
  routes: Array<any>
  region: any
  coordinate: any
  hospitalId: number
}

@inject("hospitalStore")
@observer
export default class HospitalDetailScreen extends Component<Props, State> {
  hospitalId = -1

  static navigationOptions = {
    title: "병원 정보",
    headerStyle: {
      // height: Platform.OS === "ios" ? 76 : undefined,
    },
  }

  constructor(props: Props) {
    super(props)
    const { route } = this.props
    this.state = {
      index: 0,
      routes: [
        { key: "기본정보", title: "기본정보" },
        // { key: "심평원 평가", title: "심평원 평가" },
        // { key: "회원 평가", title: "회원 평가" }
      ],
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
      // region: {
      //   latitude: 37.3910594,
      //   longitude: 127.0788039,
      //   latitudeDelta: 0.0922,
      //   longitudeDelta: 0.0421,
      // },
      coordinate: {
        latitude: 37.3910594,
        longitude: 127.0788039,
      },
      hospitalId: 0,
    }
    this.hospitalId = route.params.id
  }

  setMapSetting = () => {
    const { hospital } = this.props.hospitalStore
    const latitude = parseFloat(hospital?.y_pos)
    const longitude = parseFloat(hospital?.x_pos)

    if (latitude !== undefined && longitude !== undefined) {
      this.setState({
        region: {
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: 0.003,
          longitudeDelta: 0.001,
        },
        coordinate: {
          latitude: latitude,
          longitude: longitude,
        },
      })
    }
  }

  componentDidMount = async () => {
    const { route } = this.props
    await this.props.hospitalStore.fetchHospitalDetail(this.hospitalId)
    await this.setMapSetting()
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })
  }

  convertTime(time: string): string {
    return time.substr(0, time.length - 2) + ":" + time.substr(time.length - 2)
  }

  call = () => {
    Linking.openURL(`tel:${this.props.hospitalStore.hospital!.phone}`)
  }

  render() {
    const store = this.props.hospitalStore
    const data = store.hospital

    let mon_open_time = ""
    let tue_open_time = ""
    let wed_open_time = ""
    let thu_open_time = ""
    let fri_open_time = ""
    let sat_open_time = ""

    let tue_close_time = ""
    let mon_close_time = ""
    let wed_close_time = ""
    let thu_close_time = ""
    let fri_close_time = ""
    let sat_close_time = ""

    if (data != undefined && data.mon_open_time) {
      mon_open_time = this.convertTime(data.mon_open_time)
      tue_open_time = this.convertTime(data.tue_open_time)
      wed_open_time = this.convertTime(data.wed_open_time)
      thu_open_time = this.convertTime(data.thu_open_time)
      fri_open_time = this.convertTime(data.fri_open_time)
      sat_open_time = this.convertTime(data.sat_open_time)

      mon_close_time = this.convertTime(data.mon_close_time)
      tue_close_time = this.convertTime(data.tue_close_time)
      wed_close_time = this.convertTime(data.wed_close_time)
      thu_close_time = this.convertTime(data.thu_close_time)
      fri_close_time = this.convertTime(data.fri_close_time)
      sat_close_time = this.convertTime(data.sat_close_time)
    }

    if (store.status == "pending") {
      return <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
    }

    return (
      <View style={{ flex: 1, backgroundColor: color.background }}>
        <View style={CONTENT_CONTAINER}>
          <View style={CONTENT_HEADER}>
            <Text style={DISEASE_TITLE}>{data?.name ?? ""}</Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              backgroundColor: color.palette.white,
              paddingHorizontal: 20,
              paddingTop: 25,
            }}
            showsVerticalScrollIndicator={Platform.OS === "android"}
          >
            <Text style={BOLD}>진료과목</Text>
            {data?.hospital_specialties.map((specialty) => (
              // <Text key={specialty.m_specialty_name} style={CONTENT_TEXT_WRAP}>{specialty.m_specialty_name} 전문의 {specialty.doctor_num}명</Text>
              <Text key={specialty.m_specialty_name} style={CONTENT_TEXT_WRAP}>
                {specialty.m_specialty_name}
              </Text>
            ))}
            {data && mon_open_time.length > 0 ? (
              <View style={{ marginTop: 27 }}>
                <Text style={BOLD}>진료시간</Text>
                <Text style={CONTENT_TEXT_WRAP}>
                  월요일 {mon_open_time} - {mon_close_time}
                </Text>
                <Text style={CONTENT_TEXT_WRAP}>
                  화요일 {tue_open_time} - {tue_close_time}
                </Text>
                <Text style={CONTENT_TEXT_WRAP}>
                  수요일 {wed_open_time} - {wed_close_time}
                </Text>
                <Text style={CONTENT_TEXT_WRAP}>
                  목요일 {thu_open_time} - {thu_close_time}
                </Text>
                <Text style={CONTENT_TEXT_WRAP}>
                  금요일 {fri_open_time} - {fri_close_time}
                </Text>
                <Text style={CONTENT_TEXT_WRAP}>
                  토요일 {sat_open_time} - {sat_close_time}
                </Text>
              </View>
            ) : null}

            {/* <View style={{ height: 27 }}></View> */}
            {/* <Text style={BOLD}>항생제 처방률</Text>
                  <Text style={CONTENT_TEXT_WRAP}>67%</Text> */}

            <View style={{ height: 27 }}></View>
            <Text style={BOLD}>주소</Text>
            <Text style={CONTENT_TEXT_WRAP}>{data?.address ?? ""}</Text>
            <View style={{ height: 27 }}></View>
            {this.state.region.latitude != 0 ? (
              <MapView provider={PROVIDER_GOOGLE} style={MAP} region={this.state.region}>
                <Marker
                  coordinate={this.state.coordinate}
                  // title={data.}
                  // description={marker.description}
                />
              </MapView>
            ) : null}

            <View style={{ height: 27 }}></View>

            {data && data.web?.length > 0 ? (
              <View>
                <Text style={BOLD}>홈페이지</Text>
                <TouchableHighlight
                  onPress={() =>
                    data
                      ? Linking.openURL(
                          !data.web.includes("http") ? "http://" + data.web : data.web,
                        )
                      : null
                  }
                  underlayColor={color.palette.offWhite}
                >
                  <Text style={CONTENT_TEXT_WRAP}>{data != undefined ? data.web : ""}</Text>
                </TouchableHighlight>
              </View>
            ) : null}
            <View style={{ height: 27 }}></View>
            <View style={BOTTOM_BUTTON}>
              <Button block rounded onPress={this.call}>
                <Text style={{ fontWeight: "bold", color: "#fff" }}>전화하기</Text>
              </Button>
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }
}
