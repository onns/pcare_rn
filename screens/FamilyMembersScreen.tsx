import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { ScrollView, Toast, View } from "native-base"
import React, { Component } from "react"
import { ImageStyle, Platform, TextStyle, TouchableOpacity, ViewStyle } from "react-native"
import FastImage from "react-native-fast-image"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import SvgReceiveRequest from "../assets/images/receiveRequest.svg"
import SvgSendFamilyRequest from "../assets/images/sendFamilyRequest.svg"
import { getPopupButton, Popup } from "../components/Popup"
import { Button } from "../components/StyledButton"
import { Text } from "../components/StyledText"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { FamilyMember } from "../stores/FamilyMember"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

const CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const ALL_CENTER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
}
const VERTICAL_CENTER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const HORIZON_CENTER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
}
const CONTENT_CONTAINER: ViewStyle = {
  paddingHorizontal: 20,
  paddingVertical: 20,
}

const PROFILE_IMAGE: ImageStyle = {
  width: 64,
  height: 64,
  borderRadius: 30,
}

const NAME: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 17,
  fontWeight: "400",
  letterSpacing: -0.68,
  color: color.palette.gray1,
}

const BIRTH_DATE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 15,
  letterSpacing: -0.68,
  color: color.palette.grey50,
  marginTop: Platform.OS === "android" ? 3 : 0,
}

const REQUESTER: TextStyle = {
  marginTop: Platform.OS === "android" ? 3 : 0,
  fontFamily: typography.primary,
  fontSize: 16,
  letterSpacing: -0.64,
  color: color.primary,
}

const CARD_BUTTONS: ViewStyle = {
  maxWidth: 70,
  flexDirection: "row",
  justifyContent: "flex-end",
  alignItems: "center",
  marginRight: 10,
}

const SECTIONLINE: ViewStyle = {
  flex: 1,
  height: 1,
  backgroundColor: color.palette.gray4,
  marginVertical: 24,
}

const ADD_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 17,
  fontWeight: "bold",
  color: color.palette.grey,
  textAlign: "center",
}

const ADD_BTN: ViewStyle = {
  height: 55,
  alignContent: "flex-end",
  borderWidth: 1,
  borderColor: color.palette.gray4,
  borderRadius: 45,
  marginHorizontal: 16,
  marginBottom: 16,
  justifyContent: "center",
  alignItems: "center",
}

const MEMBER_INFO: ViewStyle = {
  flex: 1,
  // height: 60,
  marginLeft: 24,
  justifyContent: "center",
}

const SUBTITLE: TextStyle = {
  fontWeight: "700",
  fontSize: 20,
  lineHeight: 24,
  letterSpacing: -0.8,
  color: color.palette.black,
  marginBottom: 13,
}

type FamilyMembersRouteProp = RouteProp<MyPageStackParamList, "FamilyMembers">

type FamilyMembersNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "FamilyMembers">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: FamilyMembersNavigationProp
  route: FamilyMembersRouteProp
}
interface State {
  visiblePopup: boolean
  rejectPopup: boolean
  requestRemovePopup: boolean
  index: number
  isManager: boolean
  requestedMember: object
  popupTitle: string
  popupMessage: string
  statusBtn1: string
  statusBtn2: string
  typeBtn2: React.ReactElement | undefined
  memberToRemove: any
  memberToRemoveRequest: any
  family: object
  myPrescriptionManagers: any
}

const cover = {
  uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png`,
}

@inject("accountStore", "prescriptionStore")
@observer
export default class FamilyMembersScreen extends Component<Props, State> {
  static navigationOptions = {
    title: "가족회원관리",
  }

  _unsubscribeFocus: () => void

  constructor(props: Props) {
    super(props)

    this.state = {
      visiblePopup: false,
      rejectPopup: false,
      requestRemovePopup: false,
      index: 0,
      isManager: false,
      requestedMember: [],
      popupTitle: "",
      popupMessage: "",
      statusBtn1: "",
      statusBtn2: "",
      typeBtn2: undefined,
      memberToRemove: {},
      memberToRemoveRequest: {},
      family: [],
      myPrescriptionManagers: [],
    }
  }

  componentDidMount() {
    this._unsubscribeFocus = this.props.navigation.addListener("focus", this.didFocus)
    this.fetchStore()
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
  }

  fetchStore = async () => {
    const { accountStore } = this.props
    await accountStore.fetchFamilyToAgree()
    await accountStore.fetchMyPrescriptionManagers()
    await accountStore.fetchFamilyToRequest()

    await this.setState({
      family: accountStore.family,
      myPrescriptionManagers: accountStore.myPrescriptionManagers,
    })
  }

  didFocus = () => {
    this.fetchStore()
  }

  askToRemoveFamily = (member) => {
    this.setState({
      rejectPopup: true,
      popupTitle: "가족회원 동의 요청을 거부하시겠습니까?",
      popupMessage: `${member.name}님이 보낸 가족회원 동의 요청을 거부하시겠습니까?`,
      statusBtn1: "동의 거부",
      memberToRemove: member,
    })
  }

  askToRemoveRequest = (index, member) => {
    this.setState({
      requestRemovePopup: true,
      popupTitle: "가족회원 동의 요청을 철회하시겠습니까?",
      popupMessage: `${member.family_user_phone}님에게 보낸 가족회원이 동의요청을 철회하시겠습니까?`,
      statusBtn1: "요청 철회",
      memberToRemoveRequest: member,
      index: index,
    })
  }

  // 요청취소시 실행될 함수 (요청취소자 db에서 삭제 => delete api 생기면 추후 작성)
  async removeFamilyRequest(member: FamilyMember) {
    // async removeFamilyRequest(index: number, familyId: number, phone: number) {
    const result = await api.deleteFamilyToRequest(member.id)
    if (result.kind === "ok") {
      this.props.accountStore.removeFamilyToRequest(this.state.index, this.props.prescriptionStore)
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => this.props.navigation.navigate("Auth"), 2000)
    } else {
      console.tron.log("< FamilyMembersScreen > removeFamilyRequest, deleteFamilyToRequest error")
    }
  }

  render() {
    const family = this.props.accountStore.family
    return (
      <SafeAreaInsetsContext.Consumer>
        {(insets) => (
          <View style={{ ...CONTAINER, paddingBottom: insets.bottom }}>
            <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
              {family.map((member, index) => (
                <View key={member.id}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.push("Details", {
                        screen: "EditFamilyMember",
                        params: {
                          memberIndex: index,
                          isManager: false,
                        },
                      })
                    }}
                  >
                    <View style={ALL_CENTER}>
                      <View style={ROW}>
                        <FastImage
                          source={member.img ? { uri: member.img } : cover}
                          style={PROFILE_IMAGE}
                          resizeMode={FastImage.resizeMode.cover}
                        />
                        <View style={MEMBER_INFO}>
                          <View style={{ ...ROW, alignItems: "center" }}>
                            <Text style={NAME}>{member.name}</Text>
                          </View>
                          <View style={ROW}>
                            <Text style={BIRTH_DATE}>{this.formatDate(member.birth_date)}</Text>
                            {!member.family_user ? (
                              <Text style={BIRTH_DATE}> (14세 미만)</Text>
                            ) : null}
                          </View>
                        </View>
                        <View style={CARD_BUTTONS}>
                          <SvgArrowRight />
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                  {this.props.accountStore.familyToRequest.length ||
                  this.props.accountStore.familyToAgree.length ? (
                    <View style={SECTIONLINE}></View>
                  ) : this.props.accountStore.family.length - 1 == index ? null : (
                    <View style={SECTIONLINE}></View>
                  )}
                </View>
              ))}
              {this.props.accountStore.familyToRequest.map((member, index) => (
                <View key={member.id} style={HORIZON_CENTER}>
                  <View style={{ flexDirection: "row", marginTop: 15 }}>
                    <SvgSendFamilyRequest />
                    <View style={MEMBER_INFO}>
                      <View style={VERTICAL_CENTER}>
                        <Text style={NAME}>{this.formatNumber(member.family_user_phone)}</Text>
                      </View>
                      <Text style={REQUESTER}>가족회원 동의 요청을{"\n"}보냈습니다.</Text>
                    </View>
                    <View style={[CARD_BUTTONS, { maxWidth: 110 }]}>
                      <Button
                        light
                        block
                        style={{
                          height: 42,
                          alignSelf: "center",
                          backgroundColor: color.palette.gray5,
                        }}
                        onPress={() => this.askToRemoveRequest(index, member)}
                      >
                        <Text
                          style={[
                            NAME,
                            {
                              color: color.palette.copper,
                              textAlign: "center",
                              fontWeight: "bold",
                            },
                          ]}
                        >
                          요청취소
                        </Text>
                      </Button>
                    </View>
                  </View>
                  {this.props.accountStore.familyToAgree.length ? (
                    <View style={SECTIONLINE}></View>
                  ) : this.props.accountStore.familyToRequest.length - 1 == index ? null : (
                    <View style={SECTIONLINE}></View>
                  )}
                </View>
              ))}
              {this.props.accountStore.familyToAgree
                .filter((member) => member.status != "RJ")
                .map((member, index) => (
                  <View key={member.id} style={{ justifyContent: "center" }}>
                    <View style={{ flexDirection: "row", marginTop: 20 }}>
                      <SvgReceiveRequest />
                      <View style={MEMBER_INFO}>
                        <View style={VERTICAL_CENTER}>
                          <Text style={NAME}>{member.name}</Text>
                        </View>
                        <Text style={REQUESTER}>가족회원 동의 요청을{"\n"}수락하시겠습니까?</Text>
                      </View>
                      <View style={CARD_BUTTONS}>
                        <Button
                          light
                          block
                          style={{
                            height: 42,
                            marginRight: 9,
                            alignSelf: "center",
                            backgroundColor: color.palette.gray5,
                          }}
                          onPress={() => this.askToRemoveFamily(member)}
                        >
                          <Text
                            style={[
                              NAME,
                              {
                                color: color.palette.copper,
                                fontSize: Platform.OS === "android" ? 15 : 17,
                                fontWeight: "700",
                              },
                            ]}
                          >
                            거부
                          </Text>
                        </Button>
                        <Button
                          block
                          style={{
                            height: 42,
                            alignSelf: "center",
                            backgroundColor: color.palette.gray5,
                          }}
                          onPress={() => this.acceptFamily(member)}
                        >
                          <Text
                            style={[
                              NAME,
                              {
                                color: color.palette.darkMint,
                                fontSize: Platform.OS === "android" ? 15 : 17,
                                fontWeight: "700",
                              },
                            ]}
                          >
                            승인
                          </Text>
                        </Button>
                      </View>
                    </View>
                    {this.props.accountStore.familyToAgree.length - 1 == index ? null : (
                      <View style={SECTIONLINE}></View>
                    )}
                  </View>
                ))}
              <View style={{ height: 51 }} />
              {this.state.myPrescriptionManagers.length > 0 && (
                <Text style={SUBTITLE}>내 처방전 관리 회원</Text>
              )}
              {this.state.myPrescriptionManagers?.map((member, index) => (
                <View key={member.id} style={{ justifyContent: "center" }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.push("Details", {
                        screen: "EditFamilyMember",
                        params: {
                          memberIndex: index,
                          isManager: true,
                        },
                      })
                    }}
                  >
                    <View style={{ flexDirection: "row", marginTop: 20 }}>
                      <FastImage
                        source={member.img ? { uri: member.img } : cover}
                        style={PROFILE_IMAGE}
                        resizeMode={FastImage.resizeMode.cover}
                      />
                      <View style={MEMBER_INFO}>
                        <View style={VERTICAL_CENTER}>
                          <Text style={NAME}>{member.name}</Text>
                        </View>
                        <View>
                          <Text style={BIRTH_DATE}>{this.formatDate(member.birth_date)}</Text>
                        </View>
                      </View>
                      <View style={CARD_BUTTONS}>
                        <SvgArrowRight />
                      </View>
                    </View>
                  </TouchableOpacity>
                  {this.state.myPrescriptionManagers.length - 1 === index ? null : (
                    <View style={SECTIONLINE}></View>
                  )}
                </View>
              ))}
              {/* 동의 수락 */}
              <Popup
                isVisible={this.state.visiblePopup}
                title={this.state.popupTitle}
                message={this.state.popupMessage}
                button1={getPopupButton(
                  () => this.setState({ visiblePopup: false }),
                  this.state.statusBtn1,
                )}
              />
              {/* 동의 거절 */}
              <Popup
                isVisible={this.state.rejectPopup}
                title={this.state.popupTitle}
                message={this.state.popupMessage}
                button1={getPopupButton(() => {
                  this.rejectFamily(this.state.memberToRemove)
                  this.setState({ rejectPopup: false })
                }, this.state.statusBtn1)}
                button2={getPopupButton(
                  () => this.setState({ rejectPopup: false }),
                  "취소",
                  "cancel",
                )}
              />
              {/* 요청 철회 */}
              <Popup
                isVisible={this.state.requestRemovePopup}
                title={this.state.popupTitle}
                message={this.state.popupMessage}
                button1={getPopupButton(() => {
                  this.removeFamilyRequest(this.state.memberToRemoveRequest)
                  this.setState({ requestRemovePopup: false })
                }, this.state.statusBtn1)}
                button2={getPopupButton(
                  () => this.setState({ requestRemovePopup: false }),
                  "취소",
                  "cancel",
                )}
              />
            </ScrollView>
            <TouchableOpacity
              onPress={() => this.props.navigation.push("Details", { screen: "AddFamilyMember" })}
              style={ADD_BTN}
            >
              <Text style={ADD_TEXT}>가족 추가</Text>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaInsetsContext.Consumer>
    )
  }

  formatDate(d: Date | undefined) {
    if (d == undefined) {
      return ""
    }
    let month = "" + (d.getMonth() + 1)
    let day = "" + d.getDate()
    const year = d.getFullYear()

    if (month.length < 2) month = "0" + month
    if (day.length < 2) day = "0" + day

    return [year, month, day].join(".")
  }

  formatNumber(userPhone: string | undefined) {
    if (userPhone == undefined) {
      return ""
    } else {
      if (userPhone.length == 11) {
        return userPhone.slice(0, 3) + "-" + userPhone.slice(3, 7) + "-" + userPhone.slice(7)
      } else if (userPhone.length == 10) {
        return userPhone.slice(0, 3) + "-" + userPhone.slice(3, 6) + "-" + userPhone.slice(6)
      } else {
        return ""
      }
    }
  }

  async acceptFamily(member: FamilyMember) {
    // start making calls
    const response = await api.apisauce.patch(`/user/family/${member.id}/`, {
      is_agree: true,
      status: "AC",
      user: member.user,
      family_user: member.family_user,
    })
    if (response.ok) {
      this.setState({
        visiblePopup: true,
        popupTitle: "가족회원 동의 수락",
        popupMessage: `축하합니다. ${member.name}님의 가족회원이 되었습니다. 이제 회원님의 처방전을 ${member.name}님이 관리하실 수 있습니다.`,
        statusBtn1: "확인",
      }),
        this.props.accountStore.fetchFamilyToAgree()
      this.props.accountStore.fetchMyPrescriptionManagers()
    } else {
      console.tron.log(
        "< FamilyMembersScreen > acceptFamily, error: ",
        response.status ? response.status : 0,
        JSON.stringify(response.config),
      )
    }
  }

  async rejectFamily(member: FamilyMember) {
    // start making calls
    const response = await api.apisauce.patch(`/user/family/${member.id}/`, {
      is_agree: false,
      status: "RJ",
      user: member.user,
      family_user: member.family_user,
    })
    if (response.ok) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{`가족회원 승인 요청을 거절하였습니다.`}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      this.props.accountStore.fetchFamilyToAgree()
    } else {
      console.tron.log(
        "< FamilyMembersScreen > rejectFamily, error: ",
        response.status ? response.status : 0,
        JSON.stringify(response.config),
      )
    }
  }
}
