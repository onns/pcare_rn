import "@react-native-firebase/analytics"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import IMP from "iamport-react-native"
import { inject, observer } from "mobx-react"
import { Toast } from "native-base"
import React from "react"
import { Alert, Text, View } from "react-native"
import appsFlyer from "react-native-appsflyer"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { AuthStackParamList, MyPageStackParamList } from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { styles } from "../theme"

type CertificationScreenRouteProp = RouteProp<AuthStackParamList, "Certification">

type CertificationScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "Certification">,
  StackNavigationProp<MyPageStackParamList>
>

interface Props {
  accountStore: AccountStore
  navigation: CertificationScreenNavigationProp
  route: CertificationScreenRouteProp
}

export function Loading() {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Text style={{ fontSize: 18 }}>잠시만 기다려주세요...</Text>
    </View>
  )
}

@inject("accountStore")
@observer
export default class CertificationScreen extends React.Component<Props> {
  static navigationOptions = {
    title: "본인인증",
  }

  constructor(props: Props) {
    super(props)
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    firebase.analytics().logEvent("certification_begin")
    amplitude.getInstance().logEvent("certification_begin")
  }

  render() {
    /* [필수입력] 본인인증에 필요한 데이터를 입력합니다. */
    const data = {
      merchant_uid: `mid_${new Date().getTime()}`,
      min_age: "14",
    }

    return (
      <IMP.Certification
        userCode={"imp39658766"} // 가맹점 식별코드
        data={data} // 본인인증 데이터
        callback={this.certificateCallback} // 본인인증 종료 후 콜백
        loading={<Loading />}
      />
    )
  }

  /* [필수입력] 본인인증 종료 후, 라우터를 변경하고 결과를 전달합니다. */
  certificateCallback = async (response: any) => {
    const { accountStore, route } = this.props

    if (response.success) {
      // 인증 성공 시
      // 백엔드에 imp_uid 전달
      const result = await api.apisauce.get(`/user/is_certified/?imp_uid=${response.imp_uid}`)
      if (result.ok) {
        firebase.analytics().logEvent("certification_complete")
        amplitude.getInstance().logEvent("certification_complete")
        appsFlyer.logEvent("af_certification_complete", {})

        accountStore.user.setCertificated(true)
        if (this.getPreviousScreen(route.params) === "CertificationGuide") {
          accountStore.fetchUser()
        }
        Toast.show({
          title: <Text style={styles.TOAST_TEXT}>{`본인인증을 완료하였습니다.`}</Text>,
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => this.next(response), 1500)
      }
      // else if (result.data && String(result.data).includes('already exists')) {
      //   Toast.show({
      //     text: "해당 전화번호로 인증한 다른 아이디가 존재합니다.",
      //     duration: 2000,
      //     textStyle: styles.TOAST_TEXT,
      //     style: styles.TOAST_VIEW,
      //     onClose: () => this.next(response)
      //   })
      //   return
      // }
      else {
        firebase
          .crashlytics()
          .recordError(result.status ? result.status : 0, JSON.stringify(result.config))
        Alert.alert(
          "인증정보 저장 실패",
          `인증정보 저장 중에 오류가 발생하였습니다. \n나중에 다시 시도해주시기 바랍니다.`,
          [{ text: "확인", onPress: () => this.next(response) }],
          { cancelable: false },
        )
      }
    } else {
      firebase.crashlytics().recordError(503, "Error from danal server: " + response.error_msg)
      // 인증 실패 시
      Alert.alert(
        "본인인증 실패",
        `인증에 실패하였습니다. \n에러 내용: ` + response.error_msg,
        [{ text: "확인", onPress: () => this.next(response) }],
        { cancelable: false },
      )
    }
  }

  next(response: any) {
    const { navigation, accountStore, route } = this.props

    const previousScreen = this.getPreviousScreen(route.params)
    if (previousScreen === "CertificationGuide") {
      navigation.replace("Welcome", response)
    } else if (previousScreen === "PrescriptionScanner") {
      navigation.goBack()
    } else {
      navigation.replace("EditUserProfile", {
        prevRouteName: this.getPreviousScreen(route.params),
        ...response,
      })
    }
  }

  getPreviousScreen(params: any): any {
    return params.previousScreen
  }
}
