import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Toast, View } from "native-base"
import React from "react"
import {
  Alert,
  Dimensions,
  Image,
  Linking,
  Platform,
  ScrollView,
  StatusBar,
  Switch,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import AndroidOpenSettings from "react-native-android-open-settings"
import Modal from "react-native-modal"
import { requestNotifications } from "react-native-permissions"

import { api } from "@api/api"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { Picker } from "@react-native-picker/picker"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import SvgArrowRight from "../assets/images/arrowRight.svg"
import { CloseButton } from "../components/CloseButton"
import { Text } from "../components/StyledText"
import {
  DetailsStackParamList,
  MainTabParamList,
  MyPageStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { TakingDrugStore } from "../stores/drug-to-take/TakingDrugStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
  backgroundColor: "#fff",
  marginTop: 9,
  paddingLeft: 20,
  paddingRight: 20,
}

const ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}

const SETTING_ROW: ViewStyle = {
  ...ROW,
  paddingTop: 15,
  paddingBottom: 13.5,
}
const CHECKBOX_ROW: ViewStyle = {
  ...ROW,
  flex: 1,
  marginBottom: 15,
}

const modalContents = {
  preg: {
    title: "임부금기 약제",
    content:
      "임신 또는 임신하고 있을 가능성이 있는 환자가 복용하면 태아기형 및 태아독성 등 태아에 대한 위험성이 있는 약물입니다.",
  },
  byg: {
    title: "병용금기 약제",
    content:
      "두개 이상의 약물을 같이 복용했을 때 예상하지 못한 부작용이 나타나거나 치료 효과가 떨어지는 약물입니다.",
  },
  hng: {
    title: "효능군중복 약제",
    content: "약의 성분은 다르나 효능이 동일한 약물이 2가지 이상있는 경우입니다.",
  },
  dis: {
    title: "동일성분중복 약제",
    content: "약의 효능효과, 성분이 동일한 약물이 2가지 이상 있는 경우입니다.",
  },
  nosale: {
    title: "사용중지 약제",
    content: "해외 또는 국내에서의 약물 부작용보고, 품질부적합 등으로 사용이 중지된 약물입니다.",
  },
}

type SettingsRouteProp = RouteProp<MyPageStackParamList, "Settings">

type SettingsNavigationProp = CompositeNavigationProp<
  StackNavigationProp<MyPageStackParamList, "Settings">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  takingDrugStore: TakingDrugStore
  navigation: SettingsNavigationProp
  route: SettingsRouteProp
}

interface State {
  pinCode: undefined | string
  pinCodeEnabled: boolean
  pushNotiEnabled: boolean
  durNotiEnabled: boolean
  isModalVisible: boolean
  modalContentType: "preg" | "byg" | "hng" | "dis" | "nosale"
  developMode: number
}

const DUR_DESC: TextStyle = {
  fontSize: 15,
  lineHeight: 21,
  color: "rgb(153, 153, 153)",
}
const DUR_DESC_BOLD: TextStyle = {
  fontFamily: typography.primary,
  color: "rgb(132, 132, 132)",
}
const CONCIERGE_DESC: TextStyle = {
  lineHeight: 21,
  color: "rgb(132, 132, 132)",
  marginBottom: 13.5,
}

const DUR_ATTR = {
  fontFamily: typography.primary,
  fontSize: 15,
  lineHeight: 19,
  letterSpacing: -0.6,
  color: color.palette.brownGrey,
}
const DUR_ATTR_UNCHECKED = { ...DUR_ATTR, color: color.palette.brownGrey }
const DUR_ATTR_CHECKED = { ...DUR_ATTR, color: color.text }
const CHECKBOX = {
  width: 26,
  height: 26,
  borderRadius: 4,
  backgroundColor: color.palette.grey5,
}
const checkboxChecked = require("../assets/images/checkboxChecked.png")
const checkboxDisabled = require("../assets/images/checkboxDisabled.png")

const INFO_MARK = { width: 14, height: 14, marginLeft: 5, marginBottom: 1 }
const infoSource = require("../assets/images/info.png")
const hitSlop10 = { top: 10, left: 10, right: 10, bottom: 10 }
const hitSlop20 = { top: 20, left: 20, right: 20, bottom: 20 }
const SECTION_TITLE = {
  fontFamily: typography.primary,
  fontSize: 15,
  color: color.palette.grey1,
  marginTop: 15,
  marginLeft: 20,
}
const PROPERTY_NAME = { fontFamily: typography.primary, fontSize: 18 }

const FLEX_END: ViewStyle = {
  flex: 1,
  justifyContent: "center",
  alignItems: "flex-end",
}
const LINE = { height: 1, backgroundColor: "rgb(237, 237, 237)" }
@inject("accountStore", "prescriptionStore", "takingDrugStore")
@observer
export default class SettingsScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "앱 설정",
  }

  _unsubscribeFocus: () => void

  constructor(props: Props) {
    super(props)

    this.state = {
      pinCode: undefined,
      pinCodeEnabled: false,
      pushNotiEnabled: false,
      durNotiEnabled: false,
      isModalVisible: false,
      modalContentType: "preg",
      developMode: 0,
    }
  }

  componentDidMount() {
    this._unsubscribeFocus = this.props.navigation.addListener("focus", this.didFocus)
    this.fetchDeveloperMode()
  }

  didFocus = async () => {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })

    const pinCode = await AsyncStorage.getItem("pinCode")
    const pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")
    const pushNotiEnabled = this.props.accountStore.user.presc_noti === "y"
    const durNotiEnabled = this.props.accountStore.user.dur_noti === "y"
    this.setState({
      pinCode: pinCode,
      pinCodeEnabled: JSON.parse(pinCodeEnabled),
      pushNotiEnabled: pushNotiEnabled,
      durNotiEnabled: durNotiEnabled,
    })
  }

  componentWillUnmount() {
    this._unsubscribeFocus()
  }

  async onDurNotiValueChange() {
    const enabled = !this.state.durNotiEnabled

    const user = this.props.accountStore.user
    const prevToken = user.firebase_token0s
    if (!enabled) {
      // DB 에서 token 지우기

      this.setState({ durNotiEnabled: enabled })
      // await AsyncStorage.setItem("durNotiEnabled", JSON.stringify(enabled))

      const params = new FormData()
      params.append("user", user.id)
      params.append("dur_noti", "n")
      params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
      const result = await api.updateProfile(user.id, params)
      if (result.kind === "ok") {
        user.setDurNoti("n")
      } else if (result.kind === "unauthorized") {
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => {
          this.props.navigation.navigate("Auth")
        }, 2000)
      }
    } else {
      // DB 에 token 저장
      this.setState({ durNotiEnabled: enabled })
      const authStatus = await firebase.messaging().hasPermission()

      if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
        console.tron.log("< HomeScreen > Push Notifications AUTHORIZED")
      } else {
        console.tron.log("< SettingsScreen > onDurNotiValueChange, user doesn't have permission")

        this.props.accountStore.setTemporaryPinCodeDisabled(true)
        try {
          if (Platform.OS === "ios") {
            const authStatus = await firebase.messaging().requestPermission({
              announcement: true,
            })
            if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
              console.tron.log("User has notification permissions enabled.")
            } else if (authStatus === firebase.messaging.AuthorizationStatus.PROVISIONAL) {
              console.tron.log("User has provisional notification permissions.")
            } else {
              console.tron.log("User has notification permissions disabled")
            }
          } else {
            const { status, settings } = await requestNotifications(["alert", "sound"])
            console.tron.log(status)
            console.tron.log(settings)
          }
        } catch (error) {
          console.tron.log("< SettingsScreen > onDurNotiValueChange, rejected permissions", error)
          if (Platform.OS === "ios") {
            Alert.alert(
              "",
              `알림 설정을 변경하려면, 다음 페이지에서 "알림"을 누르고 "알림 허용"을 버튼을 다시 한번 눌러 주세요.`,
              [
                {
                  text: "확인",
                  onPress: () => {
                    Linking.openURL("app-settings://notification/com.onns.papricacare")
                    // AndroidOpenSettings.appNotificationSettings()
                  },
                },
              ],
              { cancelable: false },
            )
          } else {
            Alert.alert(
              "",
              `알림 설정을 변경하려면, 다음 페이지에서 "알림" 버튼을 눌러 주세요.`,
              [
                {
                  text: "확인",
                  onPress: () => {
                    // Linking.openURL('app-settings://notification/com.onns.papricacare')
                    AndroidOpenSettings.appNotificationSettings()
                  },
                },
              ],
              { cancelable: false },
            )
          }
        }
        this.props.accountStore.setTemporaryPinCodeDisabled(false)
      }

      const fcmToken = await firebase.messaging().getToken()
      // if (prevToken != fcmToken) {
      const params = new FormData()
      params.append("user", user.id)
      params.append("dur_noti", "y")
      params.append("firebase_token0", fcmToken)
      params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
      const result = await api.updateProfile(user.id, params)
      if (result.kind === "ok") {
        user.setDurNoti("y")
        user.setFirebaseToken0(fcmToken)
      } else if (result.kind === "unauthorized") {
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => {
          this.props.navigation.navigate("Auth")
        }, 2000)
      }
      // }
    }
  }

  async onPinCodeEnabledChange() {
    const enabled = !this.state.pinCodeEnabled
    if (!this.state.pinCode) {
      if (enabled) {
        this.props.navigation.navigate("CodeVerification", {
          purpose: "initial",
          preRoute: "Settings",
        })
      } else {
        await AsyncStorage.setItem("pinCodeEnabled", JSON.stringify(enabled))
        this.setState({ pinCodeEnabled: enabled })
      }
    } else {
      await AsyncStorage.setItem("pinCodeEnabled", JSON.stringify(enabled))
      this.setState({ pinCodeEnabled: enabled })
    }
  }

  async onPushNotiValueChange() {
    const enabled = !this.state.pushNotiEnabled

    const user = this.props.accountStore.user
    const prevToken = user.firebase_token0
    if (!enabled) {
      // DB 에서 token 지우기

      Alert.alert(
        "",
        `알림 설정을 끄면 더이상 처방전 처리 완료 메세지를 받을 수 없습니다. \n정말로 알림을 끄시겠습니까?`,
        [
          {
            text: "네",
            onPress: async () => {
              this.setState({ pushNotiEnabled: enabled })
              await AsyncStorage.setItem("pushNotiEnabled", JSON.stringify(enabled))
              const params = new FormData()
              params.append("user", user.id)
              params.append("presc_noti", "n")
              params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
              const result = await api.updateProfile(user.id, params)
              if (result.kind == "ok") {
                user.setPrescNoti("n")
              } else if (result.kind === "unauthorized") {
                Toast.show({
                  title: (
                    <Text
                      style={styles.TOAST_TEXT}
                    >{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
                  ),
                  duration: 2000,
                  placement: "top",
                  style: styles.TOAST_VIEW,
                })
                setTimeout(() => {
                  this.props.navigation.navigate("Auth")
                }, 2000)
              }
            },
            style: "cancel",
          },
          { text: "아니오", onPress: () => null },
        ],
        { cancelable: false },
      )
    } else {
      // DB 에 token 저장
      this.setState({ pushNotiEnabled: enabled })
      await AsyncStorage.setItem("pushNotiEnabled", JSON.stringify(enabled))
      const authStatus = await firebase.messaging().hasPermission()

      if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
        console.tron.log("< HomeScreen > Push Notifications AUTHORIZED")
      } else {
        console.tron.log("< SettingsScreen > onDurNotiValueChange, user doesn't have permission")

        this.props.accountStore.setTemporaryPinCodeDisabled(true)
        try {
          if (Platform.OS === "ios") {
            const authStatus = await firebase.messaging().requestPermission({
              announcement: true,
            })
            if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
              console.tron.log("User has notification permissions enabled.")
            } else if (authStatus === firebase.messaging.AuthorizationStatus.PROVISIONAL) {
              console.tron.log("User has provisional notification permissions.")
            } else {
              console.tron.log("User has notification permissions disabled")
              Alert.alert(
                "",
                `알림 설정을 변경하려면, 다음 페이지에서 "알림"을 누르고 "알림 허용"을 버튼을 다시 한번 눌러 주세요.`,
                [
                  {
                    text: "확인",
                    onPress: () => {
                      Linking.openURL("app-settings://notification/com.onns.papricacare")
                      // AndroidOpenSettings.appNotificationSettings()
                    },
                  },
                ],
                { cancelable: false },
              )
            }
          } else {
            const { status, settings } = await requestNotifications(["alert", "sound"])
            if (status === "blocked") {
              Alert.alert(
                "",
                `알림 설정을 변경하려면, 다음 페이지에서 "알림" 버튼을 눌러 주세요.`,
                [
                  {
                    text: "확인",
                    onPress: () => {
                      // Linking.openURL('app-settings://notification/com.onns.papricacare')
                      AndroidOpenSettings.appNotificationSettings()
                    },
                  },
                ],
                { cancelable: false },
              )
            }
          }
        } catch (error) {
          console.tron.log("< SettingsScreen > onDurNotiValueChange, rejected permissions", error)
          if (Platform.OS === "ios") {
            Alert.alert(
              "",
              `알림 설정을 변경하려면, 다음 페이지에서 "알림"을 누르고 "알림 허용"을 버튼을 다시 한번 눌러 주세요.`,
              [
                {
                  text: "확인",
                  onPress: () => {
                    Linking.openURL("app-settings://notification/com.onns.papricacare")
                    // AndroidOpenSettings.appNotificationSettings()
                  },
                },
              ],
              { cancelable: false },
            )
          } else {
            Alert.alert(
              "",
              `알림 설정을 변경하려면, 다음 페이지에서 "알림" 버튼을 눌러 주세요.`,
              [
                {
                  text: "확인",
                  onPress: () => {
                    // Linking.openURL('app-settings://notification/com.onns.papricacare')
                    AndroidOpenSettings.appNotificationSettings()
                  },
                },
              ],
              { cancelable: false },
            )
          }
        }
        this.props.accountStore.setTemporaryPinCodeDisabled(false)
      }
      const fcmToken = await firebase.messaging().getToken()
      // if (prevToken != fcmToken) {
      const params = new FormData()
      params.append("user", user.id)
      params.append("presc_noti", "y")
      params.append("firebase_token0", fcmToken)
      params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
      const result = await api.updateProfile(user.id, params)
      if (result.kind === "ok") {
        user.setPrescNoti("y")
        user.setFirebaseToken0(fcmToken)
      } else if (result.kind === "unauthorized") {
        Toast.show({
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
        setTimeout(() => {
          this.props.navigation.navigate("Auth")
        }, 2000)
      }
      // }
    }
  }

  renderDevelopMode() {
    return (
      <View>
        <Text style={SECTION_TITLE}>개발자 설정</Text>
        <View style={CONTENT_CONTAINER}>
          <View style={SETTING_ROW}>
            <Text style={PROPERTY_NAME}>테스트 환경</Text>
            <View style={FLEX_END}>
              <Picker
                selectedValue={this.state.developMode.toString()}
                style={{ width: 120 }}
                onValueChange={(itemValue, itemIndex) => {
                  this.setState({ developMode: itemValue })
                  this.changeDeveloperMode(itemValue)
                }}
              >
                <Picker.Item label="Production" value="0" />
                <Picker.Item label="Staging" value="1" />
                <Picker.Item label="Local1" value="2" />
                <Picker.Item label="Local2" value="3" />
                <Picker.Item label="Local3" value="4" />
                <Picker.Item label="Local4" value="5" />
                <Picker.Item label="Local5" value="6" />
                <Picker.Item label="Local6" value="7" />
              </Picker>
            </View>
          </View>
        </View>
      </View>
    )
  }

  render() {
    const deviceWidth = Dimensions.get("window").width
    const deviceHeight = Dimensions.get("window").height
    const user = this.props.accountStore.user
    return (
      <View style={{ flex: 1, backgroundColor: "rgb(239, 239, 239)" }}>
        <ScrollView>
          {this.state.developMode == 0
            ? user.user_type == "a"
              ? this.renderDevelopMode()
              : null
            : this.renderDevelopMode()}
          <Text style={SECTION_TITLE}>앱 잠금</Text>
          <View style={CONTENT_CONTAINER}>
            <View style={SETTING_ROW}>
              <Text style={PROPERTY_NAME}>앱 잠금</Text>
              <View style={FLEX_END}>
                <Switch
                  trackColor={{
                    false: "rgb(228, 228, 228)",
                    true: color.palette.brightCyan,
                  }}
                  thumbColor={"#fff"}
                  ios_backgroundColor={"rgb(228, 228, 228)"}
                  onValueChange={() => this.onPinCodeEnabledChange()}
                  value={this.state.pinCodeEnabled}
                />
              </View>
            </View>
            {this.state.pinCode ? (
              <View>
                <View style={LINE} />
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("CodeVerification", {
                      purpose: "change",
                      preRoute: "Settings",
                    })
                  }
                >
                  <View style={SETTING_ROW}>
                    <Text style={PROPERTY_NAME}>비밀번호 변경</Text>
                    <View style={FLEX_END}>
                      <SvgArrowRight
                        width={24}
                        height={24}
                        color={color.palette.grey30}
                        style={{ marginTop: -4 }}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ) : null}
          </View>
          <Text style={SECTION_TITLE}>알림</Text>
          <View style={CONTENT_CONTAINER}>
            <View style={SETTING_ROW}>
              <Text style={PROPERTY_NAME}>파프리카케어 컨시어지</Text>
              <View style={FLEX_END}>
                <Switch
                  trackColor={{
                    false: "rgb(228, 228, 228)",
                    true: color.palette.brightCyan,
                  }}
                  thumbColor={"#fff"}
                  ios_backgroundColor={"rgb(228, 228, 228)"}
                  onValueChange={() => this.onPushNotiValueChange()}
                  value={this.state.pushNotiEnabled}
                />
              </View>
            </View>
            <Text style={CONCIERGE_DESC}>
              *처방전 처리 완료 알림, 복용 완료일(2일전, 1일전, 완료일) 알림, 복약 알림,
              파프리카케어 사용 안내 알림을 보내드립니다.
            </Text>
            <View style={LINE} />
            <View style={SETTING_ROW}>
              <Text style={PROPERTY_NAME}>의약품 안전 점검(DUR) 알림</Text>
              <View style={FLEX_END}>
                <Switch
                  trackColor={{
                    false: "rgb(228, 228, 228)",
                    true: color.palette.brightCyan,
                  }}
                  thumbColor={"#fff"}
                  ios_backgroundColor={"rgb(228, 228, 228)"}
                  onValueChange={() => this.onDurNotiValueChange()}
                  value={this.state.durNotiEnabled}
                />
              </View>
            </View>
            <View>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 4,
                  borderColor: "rgb(206, 206, 206)",
                  padding: 15,
                  marginBottom: 18.4,
                }}
              >
                <Text style={DUR_DESC}>
                  <Text style={DUR_DESC_BOLD}>*의약품 안전 점검(DUR) 알림:</Text> 등록된 약제를
                  비교분석하여 임부금기, 병용금기, 사용중지, 동일성분 중복, 동일효능군 중복 약제가
                  있을 경우 해당 약제에 대한 PUSH 알림과 상세 설명을 제공합니다.
                  {"\n"}
                  <Text style={DUR_DESC_BOLD}>*비교분석 기간:</Text> 처방전 교부시점 3개월이내에
                  등록된 모든 약제의 의약품 안정성을 비교분석 합니다.
                </Text>
              </View>
              <View style={CHECKBOX_ROW}>
                <Text style={user.dur_preg == "y" ? DUR_ATTR_CHECKED : DUR_ATTR_UNCHECKED}>
                  - 임부금기 약제
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isModalVisible: true,
                      modalContentType: "preg",
                    })
                  }
                  hitSlop={hitSlop20}
                >
                  <Image source={infoSource} style={INFO_MARK} />
                </TouchableOpacity>
                <View style={FLEX_END}>
                  <TouchableOpacity onPress={() => this.onDurPregChange()} hitSlop={hitSlop10}>
                    <Image
                      source={
                        user.dur_preg == "y"
                          ? user.dur_noti == "y"
                            ? checkboxChecked
                            : checkboxDisabled
                          : null
                      }
                      style={CHECKBOX}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={CHECKBOX_ROW}>
                <Text style={user.dur_byg == "y" ? DUR_ATTR_CHECKED : DUR_ATTR_UNCHECKED}>
                  - 병용금기 약제
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isModalVisible: true,
                      modalContentType: "byg",
                    })
                  }
                  hitSlop={hitSlop20}
                >
                  <Image source={infoSource} style={INFO_MARK} />
                </TouchableOpacity>
                <View style={FLEX_END}>
                  <TouchableOpacity onPress={() => this.onDurBygChange()} hitSlop={hitSlop10}>
                    <Image
                      source={
                        user.dur_byg == "y"
                          ? user.dur_noti == "y"
                            ? checkboxChecked
                            : checkboxDisabled
                          : null
                      }
                      style={CHECKBOX}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={CHECKBOX_ROW}>
                <Text style={user.dur_nosale == "y" ? DUR_ATTR_CHECKED : DUR_ATTR_UNCHECKED}>
                  - 사용중지 약제
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isModalVisible: true,
                      modalContentType: "nosale",
                    })
                  }
                  hitSlop={hitSlop20}
                >
                  <Image source={infoSource} style={INFO_MARK} />
                </TouchableOpacity>
                <View style={FLEX_END}>
                  <TouchableOpacity onPress={() => this.onDurNosaleChange()} hitSlop={hitSlop10}>
                    <Image
                      source={
                        user.dur_nosale == "y"
                          ? user.dur_noti == "y"
                            ? checkboxChecked
                            : checkboxDisabled
                          : null
                      }
                      style={CHECKBOX}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={CHECKBOX_ROW}>
                <Text style={user.dur_dis == "y" ? DUR_ATTR_CHECKED : DUR_ATTR_UNCHECKED}>
                  - 동일성분중복 약제
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isModalVisible: true,
                      modalContentType: "dis",
                    })
                  }
                  hitSlop={hitSlop20}
                >
                  <Image source={infoSource} style={INFO_MARK} />
                </TouchableOpacity>
                <View style={FLEX_END}>
                  <TouchableOpacity onPress={() => this.onDurDisChange()} hitSlop={hitSlop10}>
                    <Image
                      source={
                        user.dur_dis == "y"
                          ? user.dur_noti == "y"
                            ? checkboxChecked
                            : checkboxDisabled
                          : null
                      }
                      style={CHECKBOX}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={CHECKBOX_ROW}>
                <Text style={user.dur_hng == "y" ? DUR_ATTR_CHECKED : DUR_ATTR_UNCHECKED}>
                  - 효능군중복 약제
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isModalVisible: true,
                      modalContentType: "hng",
                    })
                  }
                  hitSlop={hitSlop20}
                >
                  <Image source={infoSource} style={INFO_MARK} />
                </TouchableOpacity>
                <View style={FLEX_END}>
                  <TouchableOpacity onPress={() => this.onDurHngChange()} hitSlop={hitSlop10}>
                    <Image
                      source={
                        user.dur_hng == "y"
                          ? user.dur_noti == "y"
                            ? checkboxChecked
                            : checkboxDisabled
                          : null
                      }
                      style={CHECKBOX}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            {/* <View style={ROW}>
              <View style={{ flex: 1, height: 1, backgroundColor: "rgb(237, 237, 237)", marginVertical: 14.5, marginRight: 10 }} />
            </View> */}
            {/* <View style={ROW}>
              <Text style={{ fontSize: 15 }}>처방전 처리 완료 알림</Text>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Switch
                  trackColor={{ false: "rgb(228, 228, 228)", true: color.palette.brightCyan }}
                  thumbColor={"#fff"}
                  onValueChange={() => {}}
                  // value={true}
                />
              </View>
            </View> */}
            <Modal
              deviceWidth={deviceWidth}
              deviceHeight={deviceHeight}
              isVisible={this.state.isModalVisible}
              animationOut={"fadeOutDown"}
              // animationOutTiming={300}
              onBackdropPress={this.toggleModal}
            >
              <View>
                <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
                <View style={{ alignItems: "flex-end" }}>
                  <CloseButton
                    imageStyle={{ tintColor: "rgb(255, 255, 255)" }}
                    onPress={() => this.toggleModal()}
                    style={{
                      width: 15.7,
                      height: 15.7,
                      marginRight: 0,
                      paddingTop: 0,
                      marginBottom: 12.4,
                    }}
                  />
                </View>
                <View
                  style={{
                    borderRadius: 10,
                    backgroundColor: "rgb(239, 239, 239)",
                    padding: 20,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: typography.primary,
                      fontSize: 20,
                      lineHeight: 24,
                      letterSpacing: -0.8,
                      paddingBottom: 15,
                    }}
                  >
                    {modalContents[this.state.modalContentType].title}
                  </Text>
                  <Text
                    style={{
                      lineHeight: 22.5,
                      letterSpacing: -0.3,
                      color: color.palette.black,
                    }}
                  >
                    {modalContents[this.state.modalContentType].content}
                  </Text>
                </View>
              </View>
            </Modal>
          </View>
        </ScrollView>
      </View>
    )
  }

  onDurHngChange(): void {
    const user = this.props.accountStore.user
    user.setDurHng(user.dur_hng == "y" ? "n" : "y")
    this.updateProfile()
  }

  onDurBygChange(): void {
    const user = this.props.accountStore.user
    user.setDurByg(user.dur_byg == "y" ? "n" : "y")
    this.updateProfile()
  }

  onDurNosaleChange(): void {
    const user = this.props.accountStore.user
    user.setDurNosale(user.dur_nosale == "y" ? "n" : "y")
    this.updateProfile()
  }

  onDurDisChange(): void {
    const user = this.props.accountStore.user
    user.setDurDis(user.dur_dis == "y" ? "n" : "y")
    this.updateProfile()
  }

  onDurPregChange(): void {
    const user = this.props.accountStore.user
    user.setDurPreg(user.dur_preg == "y" ? "n" : "y")
    this.updateProfile()
  }

  changeDeveloperMode(type: number): void {
    /** BaseUrl이 바뀌기 전에 로그아웃 부터함 */
    this.logoutApiCall()
    /** setBaseUrl을 통한 변경 */
    this.props.accountStore.changeDeveloperMode(type)
    /** 로컬 DB에 developer Mode를 저장 */
    this.setLocalDeveloperMode(type)
    /** 이후 로그아웃 API 외 로그아웃 프로세스 */
    this.logout()
  }

  async setLocalDeveloperMode(type: number) {
    AsyncStorage.setItem("developerMode", type.toString())
  }

  /** 개발자 모드 확인하여 초기 설정 */
  async fetchDeveloperMode() {
    const developerMode = await AsyncStorage.getItem("developerMode")
    if (!developerMode) {
      /** developerMode가 설정되어 있지 않으면 */
      /** 현재는 아무것도 안함 */
    } else {
      /** developerMode가 설정되어있으면 */
      const mode = Number.parseInt(developerMode)
      this.setState({ developMode: mode })
    }
  }

  async updateProfile() {
    const user = this.props.accountStore.user
    const params = new FormData()
    params.append("user", user.id)
    params.append("presc_noti", user.presc_noti)
    params.append("dur_noti", user.dur_noti)
    params.append("dur_hng", user.dur_hng)
    params.append("dur_byg", user.dur_byg)
    params.append("dur_dis", user.dur_dis)
    params.append("dur_preg", user.dur_preg)
    params.append("dur_nosale", user.dur_nosale)
    params.append("is_agree", user.is_agreed) // 넣지 않으면 is_agree 값이 초기화되는 문제가 있어서 넣고 있음
    const result = await api.updateProfile(user.id, params)
    if (result.kind === "ok") {
      // user.setFirebaseToken0("")
    } else if (result.kind === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => {
        this.props.navigation.navigate("Auth")
      }, 2000)
    }
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  logoutApiCall = async () => {
    // start making calls
    const response = await api.apisauce.post(`/rest-auth/logout/`)
    if (!response.ok) {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    }
    api.apisauce.deleteHeader("Authorization")
  }

  logout = async () => {
    const signPath = await AsyncStorage.getItem("signPath")
    const email = await AsyncStorage.getItem("email")

    this.props.accountStore.initiate()
    this.props.prescriptionStore.initiate()
    this.props.takingDrugStore.initiate()

    const pushNotiEnabled = await AsyncStorage.getItem("pushNotiEnabled")
    const pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")
    const showedDurGuidance = await AsyncStorage.getItem("showedDurGuidance")
    const pinCode = await AsyncStorage.getItem("pinCode")
    const developMode = await AsyncStorage.getItem("developerMode")

    await AsyncStorage.clear()
    await AsyncStorage.setItem("initialRunning", JSON.stringify(false))
    if (email && signPath && signPath === "email") {
      await AsyncStorage.setItem("email", email)
    }
    if (signPath) {
      await AsyncStorage.setItem("signPath", signPath)
      this.props.accountStore.setSignPath(signPath)
    }
    if (pushNotiEnabled) {
      await AsyncStorage.setItem("pushNotiEnabled", pushNotiEnabled)
    }
    if (pinCodeEnabled) {
      await AsyncStorage.setItem("pinCodeEnabled", pinCodeEnabled)
      await AsyncStorage.setItem("pinCode", pinCode)
    }
    if (showedDurGuidance) {
      await AsyncStorage.setItem("showedDurGuidance", showedDurGuidance)
    }
    if (developMode) {
      await AsyncStorage.setItem("developerMode", developMode)
    }
    this.props.navigation.navigate("Auth")
  }
}
