import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import { ApiResponse } from "apisauce"
import { inject, observer } from "mobx-react"
import { FormControl, Input, ScrollView, Stack, Toast } from "native-base"
import React from "react"
import { ActivityIndicator, Alert, TextStyle, View, ViewStyle } from "react-native"
import appsFlyer from "react-native-appsflyer"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"
import { Button } from "tamagui"

import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { api, SignUpParams } from "../api"
import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  DetailsStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, styles, typography } from "../theme"

const BACKGROUND: ViewStyle = {
  backgroundColor: "#fff",
}
const CONTENT_CONTAINER: ViewStyle = {
  flexDirection: "column",
  paddingHorizontal: 16,
}
const TITLE: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  paddingTop: 16,
  paddingBottom: 32,
}
const TITLE_TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 32,
  lineHeight: 37,
  color: color.text,
}
const FORM_ITEM: ViewStyle = {
  backgroundColor: color.input,
  marginBottom: 16,
  marginLeft: 16,
  marginRight: 16,
  paddingLeft: 0,
}
const LABEL: TextStyle = {
  width: 90,
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.text,
  paddingRight: 0,
}
const INPUT = {
  fontFamily: typography.bold,
  fontSize: 16,
  color: color.text,
}
const BOTTOM_BUTTON: ViewStyle = {
  flexDirection: "column",
  justifyContent: "flex-end",
  padding: 16,
}
const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  color: color.palette.white,
}
const WAITING: ViewStyle = {
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: "center",
  justifyContent: "center",
}

type SignUpEmailRouteProp = RouteProp<AuthStackParamList, "SignUpEmail">

type SignUpEmailNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "SignUpEmail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

interface Props {
  accountStore: AccountStore
  navigation: SignUpEmailNavigationProp
  route: SignUpEmailRouteProp
}

interface State {
  waiting: boolean
}

@inject("accountStore")
@observer
export default class SignUpEmailScreen extends React.Component<Props, State> {
  static navigationOptions = {
    title: "",
  }

  // contentRef = React.createRef<ScrollView>()
  contentRef: any
  contentInnerRef: any

  constructor(props: Props) {
    super(props)
    this.props.accountStore.createRequestAccount()
    this.state = {
      waiting: false,
    }
  }

  onInputFocus = () => {
    setTimeout(() => this.contentRef?.scrollToEnd(), 150)
  }

  render() {
    const { route } = this.props
    firebase.analytics().logScreenView({
      screen_class: route.name,
      screen_name: route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })

    const { accountStore } = this.props
    if (!accountStore.reqAccount) {
      accountStore.createRequestAccount()
    }

    const reqAccount = accountStore.reqAccount
    return (
      <SafeAreaInsetsContext.Consumer>
        {(insets) => (
          <View style={{ ...BACKGROUND, paddingBottom: insets.bottom }}>
            <ScrollView
              ref={(ref) => (this.contentRef = ref)}
              contentContainerStyle={CONTENT_CONTAINER}
            >
              <View>
                <View style={TITLE}>
                  <Text style={TITLE_TEXT}>회원가입</Text>
                </View>
                <FormControl>
                  <Stack inlineLabel last style={FORM_ITEM} error={!!reqAccount.emailError}>
                    <FormControl.Label style={LABEL}>이메일</FormControl.Label>
                    <Input
                      autoCapitalize="none"
                      placeholder={"자주 확인하는 이메일 주소"}
                      placeholderTextColor={color.palette.grey30}
                      selectionColor={color.primary}
                      keyboardType={"email-address"}
                      onChangeText={(text) => reqAccount.emailOnChange(text)}
                      onFocus={this.onInputFocus}
                      returnKeyType={"next"}
                      style={INPUT}
                    />
                  </Stack>
                  <Stack inlineLabel last style={FORM_ITEM} error={!!reqAccount.passwordError}>
                    <FormControl.Label style={LABEL}>비밀번호</FormControl.Label>
                    <Input
                      autoCapitalize="none"
                      selectionColor={color.primary}
                      secureTextEntry={true}
                      placeholder={"영문, 숫자를 포함한 8자 이상"}
                      placeholderTextColor={color.palette.grey30}
                      onBlur={() => reqAccount.validatePassword()}
                      onChangeText={(text) => reqAccount.passwordOnChange(text)}
                      onFocus={this.onInputFocus}
                      returnKeyType={"next"}
                      style={INPUT}
                    />
                  </Stack>
                  <Stack inlineLabel last style={FORM_ITEM} error={!!reqAccount.passwordError2}>
                    <FormControl.Label style={LABEL}>비밀번호확인</FormControl.Label>
                    <Input
                      autoCapitalize="none"
                      selectionColor={color.primary}
                      secureTextEntry={true}
                      onBlur={() => reqAccount.validatePassword()}
                      onChangeText={(text) => reqAccount.password2OnChange(text)}
                      style={INPUT}
                    />
                  </Stack>
                </FormControl>
              </View>
              {this.state.waiting && (
                <View style={WAITING}>
                  <ActivityIndicator size="large" />
                </View>
              )}
            </ScrollView>
            <View style={BOTTOM_BUTTON}>
              <Button
                bc={color.palette.orange}
                br={45}
                disabled={this.state.waiting}
                onPress={() => this.signUp()}
              >
                <Text style={BUTTON_TEXT}>가입하기</Text>
              </Button>
            </View>
          </View>
        )}
      </SafeAreaInsetsContext.Consumer>
    )
  }

  async signUp() {
    const { accountStore, navigation } = this.props
    this.setState({ waiting: true })
    const newAccount = accountStore.reqAccount!
    newAccount.setMode("signUp")
    newAccount.validateForm()
    if (newAccount.isValid) {
      // if (true) {
      const params: SignUpParams = {
        // username = newAccount.email.slice(0, newAccount.email.indexOf('@')) + "_" + response.data.user
        // username: newAccount.email,
        email: newAccount.email,
        password1: newAccount.password,
        password2: newAccount.password,
        // user_profile: {
        //   name: newAccount.email.slice(0, newAccount.email.indexOf('@')),
        //   is_authenticated: false,
        //   is_agree: false
        // }
      }
      api.apisauce.deleteHeader("Authorization")

      // start making calls
      const response: ApiResponse<any> = await api.apisauce.post(`/rest-auth/registration/`, params)
      if (response.ok) {
        firebase.analytics().logEvent("sign_up", { signPath: "email" })
        firebase.analytics().logEvent("sign_up_test", { signPath: "email_test" })
        amplitude.getInstance().logEvent("sign_up", { signPath: "email" })
        appsFlyer.logEvent("af_sign_up", { signPath: "email" })

        if (response.data.key) {
          api.apisauce.setHeader("Authorization", "Token ".concat(response.data.key))
          accountStore.user.setId(response.data.user)
          const strUserId = String(response.data.user)
          firebase.crashlytics().setUserId(strUserId)
          firebase.analytics().setUserId(strUserId)
          amplitude.getInstance().setUserId(strUserId)

          // profile 생성
          const emailLocalPart = newAccount.email.slice(0, newAccount.email.indexOf("@"))
          const paramsPostProfile = {
            user: accountStore.user.id,
            nickname: emailLocalPart,
          }
          const responsePostProfile: ApiResponse<any> = await api.apisauce.post(
            `/user/profiles/`,
            paramsPostProfile,
          )

          if (!responsePostProfile.ok && responsePostProfile.data.detail) {
            Alert.alert(
              "",
              responsePostProfile.data.detail,
              [{ text: "확인", onPress: () => null }],
              { cancelable: false },
            )
          }
          accountStore.user.setNickname(emailLocalPart)
          accountStore.setSelectedMemberName(emailLocalPart)
          accountStore.setSelectedMemberKey(`_u${accountStore.user.id}`)

          await AsyncStorage.setItem("userToken", response.data.key)
          await AsyncStorage.setItem("userId", String(response.data.user))
        }

        const emailAddress = newAccount.email
        newAccount.clearStore()
        newAccount.setEmail(emailAddress)
        await AsyncStorage.setItem("signPath", "email")
        accountStore.setSignPath("email")

        // Alert.alert(
        //   "이메일 전송 성공",
        //   `이메일 인증을 위한 메일을 발송하였습니다.\n메일을 확인하셔서 24시간 내에 ‘이메일 인증하기’ 버튼을 누르시면 가입이 완료됩니다.`,
        //   [{ text: "확인", onPress: () => this.props.navigation.navigate("SignIn")}],
        //   { cancelable: false }
        // )

        // if (response.data.detail && String(response.data.detail).includes("확인 이메일을 발송했습니다.")) {
        // } else
        if (response.data.key !== undefined) {
          this.props.navigation.navigate("ServiceAgreement")
          return
        }
      } else if (response.data != null && response.data.username) {
        Toast.show({
          title: <Text style={styles.TOAST_TEXT}>{`이미 파프리카케어에 가입하였습니다.`}</Text>,
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
      } else if (
        response.data.non_field_errors &&
        String(response.data.non_field_errors[0]).includes(
          "User is already registered with this e-mail address.",
        )
      ) {
        Alert.alert(
          "",
          `해당 이메일 주소로 가입된 사용자가 존재합니다. 이전에 가입하신 방법으로 로그인 해주세요.`,
          [{ text: "확인", onPress: () => navigation.navigate("SignIn") }],
          { cancelable: false },
        )
        return
      } else if (response.data.email) {
        Alert.alert("", String(response.data.email), [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
      } else if (response.data.password1) {
        Alert.alert("", String(response.data.password1), [{ text: "확인", onPress: () => null }], {
          cancelable: false,
        })
      } else if (
        response.data.non_field_errors &&
        String(response.data.non_field_errors[0]).includes("제공된 인증데이터")
      ) {
        Alert.alert(
          "",
          `파프리카케어에 이미 등록된 이메일 계정입니다. 페이스북 계정 또는 카카오 계정으로 로그인하신 경우 해당 SNS 계정으로 시작하세요. 비밀번호가 기억나지 않을 때는 로그인 페이지에서 비밀번호 찾기를 눌러 주세요.`,
          [{ text: "확인", onPress: () => null }],
          { cancelable: false },
        )
      } else if (response.data.non_field_errors) {
        Alert.alert(
          "",
          `오류가 발생하였습니다. \n` + response.data.non_field_errors[0],
          [
            {
              text: "확인",
              onPress: () =>
                console.tron.log(
                  "< SignUpEmailScreen > signUp, response.data.non_field_errors: ",
                  response.data.non_field_errors,
                ),
            },
          ],
          { cancelable: false },
        )
      } else {
        firebase
          .crashlytics()
          .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
        Alert.alert(
          "",
          `오류가 발생하였습니다. \n나중에 다시 시도해주세요.`,
          [
            {
              text: "확인",
              onPress: () =>
                console.tron.log("< SignUpEmailScreen > signUp, else error: ", response),
            },
          ],
          { cancelable: false },
        )
      }
    } else if (accountStore.reqAccount!.emailError) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{accountStore.reqAccount!.emailError}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (accountStore.reqAccount!.passwordError) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{accountStore.reqAccount!.passwordError}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (accountStore.reqAccount!.passwordError2) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{accountStore.reqAccount!.passwordError2}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
    this.setState({ waiting: false })
  }
}
