/* eslint-disable camelcase */
import "@react-native-firebase/analytics"
import "@react-native-firebase/crashlytics"
import "@react-native-firebase/messaging"

import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { values, when } from "mobx"
import { inject, observer } from "mobx-react"
import { onAction, onSnapshot } from "mobx-state-tree"
import { Spinner, Stack, Toast, View } from "native-base"
import { length } from "ramda"
import React, { Component } from "react"
import {
  Alert,
  AppState,
  BackHandler,
  FlatList,
  ImageStyle,
  Linking,
  NativeEventSubscription,
  Platform,
  RefreshControl,
  ScrollView,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import AndroidOpenSettings from "react-native-android-open-settings"
import appsFlyer from "react-native-appsflyer"
import DeviceInfo from "react-native-device-info"
// import RNBootSplash from "react-native-bootsplash"
import FastImage from "react-native-fast-image"
import { requestNotifications } from "react-native-permissions"
// import { Notification } from "react-native-firebase/notifications"
import RBSheet from "react-native-raw-bottom-sheet"
import { SafeAreaView } from "react-native-safe-area-context"
import { Button, Image, SizableText, styled, XStack, YStack } from "tamagui"
import { snapshot } from "valtio"

import { api } from "@api/api"
import { supabase } from "@api/supabase"
import MainEventPopup from "@components/MainEventPopup"
import { SvgArrowRight } from "@components/svg/SvgArrowRight"
import DailyFeelinNote from "@components/todayfeelin/DailyFeelinNote"
import { POINT_TYPES } from "@features/points/constants"
import {
  setActivities,
  setMedicationPoints,
  setPrescriptionRegCount,
  setTodayMedication,
  setTotalPoints,
  store,
} from "@features/points/stores/total-points"
import { validatePoints } from "@features/points/validator"
import { getPrescriptionTitle } from "@lib/prescription"
import { addComma } from "@lib/utils"
import AsyncStorage from "@react-native-async-storage/async-storage"
import firebase from "@react-native-firebase/app"
import remoteConfig from "@react-native-firebase/remote-config"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"
import { TimeSlotName } from "@stores/TimeSlot"
import { Search } from "@tamagui/lucide-icons"

import { DEFAULT_API_CONFIG } from "../api/api-config"
import SvgSearch from "../assets/images/searchIcon.svg"
import SelfTestHeart from "../assets/images/selfTestHeart.svg"
import { CampaignRow } from "../components/CampaignRow"
import { EmptyDrugsToTakeList } from "../components/EmptyDrugsToTakeList"
import { MemberSelectHeader } from "../components/MemberSelectHeader"
import { NoPrescriptionBeingTaken } from "../components/NoPrescriptionBeingTaken"
import { RenderSetTime } from "../components/RenderSetTime"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { TakingPrescription } from "../components/TakingPrescription"
import { getNow as getNowTimeSlotName, getText, TimeSlotRow } from "../components/TimeSlotRow"
import { sendLogEvent } from "../lib/analytics"
import { formatTime } from "../lib/DateUtil"
import {
  DetailsStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import packageJson from "../package.json"
import { AccountStore } from "../stores/AccountStore"
import { CampaignStore } from "../stores/campaign/CampaignStore"
import { DrugToTake } from "../stores/drug-to-take/DrugToTake"
import { DrugToTakeGroup } from "../stores/drug-to-take/DrugToTakeGroup"
import { TakingDrugStore } from "../stores/drug-to-take/TakingDrugStore"
import { FamilyMember } from "../stores/FamilyMember"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { RecordStore } from "../stores/record/record-store"
import { color, styles, typography } from "../theme"

type HomeRouteProp = RouteProp<HomeStackParamList, "Home">

export type HomeNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "Home">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  recordStore: RecordStore
  accountStore: AccountStore
  campaignStore: CampaignStore
  prescriptionStore: PrescriptionStore
  takingDrugStore: TakingDrugStore
  navigation: HomeNavigationProp
  route: HomeRouteProp
}

interface State {
  appState: string
  isModalVisible: boolean
  isDurDescPopup: boolean
  requestedReviewDate: number
  todayDate: number
  requestedReviewResponse: string
  showedDurGuidance: boolean
  isDurGuidancePopup: boolean
  withinDurNotiPeriod: boolean
  previewSwipeableRowEnabled: boolean
  isAlarmNew: boolean
  timeSlot: string | undefined
  drugsTodayUnderlineWidth: number
  drugsTodayUnderlineTop: number
  drugsOnceUnderlineWidth: number
  drugsOnceUnderlineTop: number
  date: DateTime
  show_activity_button: boolean
  myPoint: number
}

/// /////////
// 스타일링
/// /////////
const ROOT: ViewStyle = {
  backgroundColor: color.backgrounds.lightmode,
}
const TAKE_DRUG_TOAST_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  lineHeight: 25,
  letterSpacing: -0.68,
  color: color.palette.white,
  paddingHorizontal: 16,
  textAlign: "center",
}
const TAKE_DRUG_TOAST_VIEW: ViewStyle = {
  backgroundColor: "rgba(47, 50, 51, 0.3)",
  marginTop: 44,
  marginHorizontal: 16,
  marginBottom: 20,
  borderRadius: 10,
  paddingTop: 12,
  paddingBottom: 11,
  shadowColor: "#29453b30",
  shadowOffset: {
    width: 0,
    height: 10,
  },
  shadowOpacity: 0.3,
  shadowRadius: 20,
  top: 280,
}
const TAKE_DRUG_TOAST_TEXT_CANCEL: TextStyle = {
  ...TAKE_DRUG_TOAST_TEXT,
  color: color.palette.white,
}
const TAKE_DRUG_TOAST_VIEW_CANCEL: ViewStyle = {
  ...TAKE_DRUG_TOAST_VIEW,
  backgroundColor: "rgba(47, 50, 51, 0.3)",
  borderColor: "rgba(47, 50, 51, 0.3)",
}
const SECTION_HEADER_TIME: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  marginTop: 24,
  // backgroundColor: "black",
  paddingHorizontal: 16,
}
const SECTION_HEADER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  // backgroundColor: "black",
  paddingHorizontal: 16,
}
const TAKING_ITEM_LIST: ViewStyle = {
  backgroundColor: color.backgrounds.default,
  flex: 1,
  // height: 500,
  // alignContent: "center",
}
const ITEM_SEPARATOR: ViewStyle = {
  height: 0,
}
const SEARCH_BUTTON: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
  borderWidth: 2,
  borderColor: "#FC630E",
  borderRadius: 12,
  backgroundColor: "#F5F5F5",
  height: 56,
  padding: 8,
}
const SearchButton = styled(Button, {
  bordered: true,
  borderColor: "$action",
})

const SECTION_HEADER_CONAINER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  backgroundColor: color.surface.neutralSubded,
  paddingHorizontal: 16,
  paddingTop: 24,
  paddingBottom: 16,
  borderBottomWidth: 1,
  borderBottomColor: color.borders.dividerBetweenUI,
}
const SECTION_HEADER_TEXT: TextStyle = {
  ...typography.ui.default,
  color: color.textColor.default,
  opacity: 0.8,
}

/// /////////
// 로직
/// /////////

@inject("accountStore", "campaignStore", "prescriptionStore", "takingDrugStore", "recordStore")
@observer
export default class HomeScreen extends Component<Props, State> {
  static navigationOptions = {
    headerShown: false,
  }

  bottomSheetRef = React.createRef<RBSheet>()
  notificationListener: any
  changeEventListener: NativeEventSubscription
  onActionDisposer: any
  onSnapshotDisposer2: any
  onTokenRefreshListener: any
  pinCodeEnabled: undefined | string
  selectedDrugToTakeGroup: DrugToTakeGroup | undefined
  _unsubscribeFocus: () => void
  onDeepLinkCanceller: any

  drugToTakeGroup: DrugToTakeGroup | undefined

  constructor(props: Props) {
    super(props)
    const { accountStore, prescriptionStore } = this.props

    this.state = {
      appState: AppState.currentState,
      isModalVisible: false,
      isDurDescPopup: false,
      requestedReviewDate: 0,
      todayDate: new Date().getMilliseconds(),
      requestedReviewResponse: "",
      showedDurGuidance: false,
      isDurGuidancePopup: false,
      withinDurNotiPeriod: false,
      previewSwipeableRowEnabled: false,
      isAlarmNew: false,
      timeSlot: undefined,
      drugsTodayUnderlineWidth: 120,
      drugsTodayUnderlineTop: 20,
      drugsOnceUnderlineWidth: 120,
      drugsOnceUnderlineTop: 120,
      date: DateTime.local(),
      show_activity_button: false,
      myPoint: -1,
    }

    prescriptionStore.setSelectedFamilyMemberKey(`_u${accountStore.user.id}`)

    this.onSnapshotDisposer2 = onSnapshot(accountStore.familyToAgree, (call) => {
      const familyToAgree = accountStore.familyToAgree
      if (familyToAgree.length > 0) {
        this.askToAcceptFamily()
      }
    })

    this.onActionDisposer = onAction(prescriptionStore, (call) => {
      if (call.name === "addPrescription") {
        this.refresh()
      }
    })
  }

  async UNSAFE_componentWillMount() {
    const { accountStore, takingDrugStore, prescriptionStore, navigation } = this.props
    const { user } = accountStore

    if (prescriptionStore.timeSlots.length === 0) {
      prescriptionStore.fetchTimeSlots()
    }

    if (user.nickname.length === 0) {
      accountStore.fetchUser()
    }
    accountStore.fetchFamily()
    accountStore.fetchFamilyToAgree()

    if (user.id !== -1) {
      const myDrugsToTake = takingDrugStore.drugsToTake
      if (
        myDrugsToTake.length === 0 ||
        myDrugsToTake[0].owner.type !== "user" ||
        myDrugsToTake[0].owner.id !== user.id
      ) {
        takingDrugStore.fetchDrugsToTake(user.id, `_u${user.id}`)
      }

      // 오늘 처방 등록 또는 복약한 포인트
      const pointStore = snapshot(store)
      if (!pointStore.isTodayMedicationPointsLoaded) {
        const { data, error } = await supabase
          .from("point_activities")
          .select("point,drug_take_id,point_type")
          .eq("user_id", accountStore.user.id)
          // .eq("point_type", POINT_TYPES.drugTaken)
          .gte("created_at", `${DateTime.local().toISODate()}T00:00:00+09:00`)
          .lt("created_at", `${DateTime.local().plus({ days: 1 }).toISODate()}T00:00:00+09:00`)
          .order("created_at", { ascending: false })

        if (data) {
          const sum = data.reduce((partialSum, a) => partialSum + a.point, 0)
          setMedicationPoints(sum)
          setTodayMedication(data.filter((item) => !!item.drug_take_id))
          setPrescriptionRegCount(
            data.filter(
              (item) =>
                item.point_type === POINT_TYPES.firstPrescriptionRegistered ||
                item.point_type === POINT_TYPES.prescriptionRegistered,
            ).length,
          )
        }

        // 중복 복약 체크 거르기 위해 activities fetch 미리함.
        // TODO: 위 fetch 와 하나로 합치기
        const { data: activities } = await supabase
          .from("point_activities")
          .select("*")
          .eq("user_id", accountStore.user.id)
          .order("created_at", { ascending: false })

        if (activities) {
          setActivities(activities)
        }
      }

      // 호흡활동 버튼 데이터 체크
      const result = await api.getUserProfile(user.id)
      if (result.kind === "ok") {
        this.setState({
          show_activity_button:
            result.data.results.length > 0 ? result.data.results[0].show_activity_button : false,
        })
      }
    }

    BackHandler.addEventListener("hardwareBackPress", this.backPressed)

    const showedDurGuidance = await AsyncStorage.getItem("showedDurGuidance")
    const requestedReviewResponse = await AsyncStorage.getItem("requestedReviewResponse")
    const requestedReviewDate = await AsyncStorage.getItem("requestedReviewDate")
    const previewSwipeableRowEnabled = await AsyncStorage.getItem("previewSwipeableRowEnabled")
    this.pinCodeEnabled = await AsyncStorage.getItem("pinCodeEnabled")

    if (requestedReviewDate) {
      this.setState({
        requestedReviewDate: Number(requestedReviewDate),
        requestedReviewResponse: requestedReviewResponse || "",
        todayDate: new Date().getMilliseconds(),
        showedDurGuidance: showedDurGuidance ? JSON.parse(showedDurGuidance) : false,
        previewSwipeableRowEnabled: previewSwipeableRowEnabled
          ? JSON.parse(previewSwipeableRowEnabled)
          : true,
      })
    } else {
      const todayTime = new Date().getMilliseconds()
      this.setState({
        requestedReviewDate: todayTime,
        requestedReviewResponse: requestedReviewResponse || "",
        todayDate: todayTime,
        showedDurGuidance: showedDurGuidance ? JSON.parse(showedDurGuidance) : false,
        previewSwipeableRowEnabled: previewSwipeableRowEnabled
          ? JSON.parse(previewSwipeableRowEnabled)
          : true,
      })
    }
  }

  async checkMyPoint() {
    const { data: totalPoints, error } = await supabase
      .from("total_points")
      .select("*")
      .eq("user_id", this.props.accountStore.user.id)
    console.tron.log("totalPoints", totalPoints)

    if (totalPoints && totalPoints.length > 0) {
      this.setState({ myPoint: totalPoints[0].total_points })
      setTotalPoints(totalPoints[0].total_points)
    } else {
      this.setState({ myPoint: 0 })
      setTotalPoints(0)
    }
  }

  async fetchLastNoticeId() {
    const { accountStore } = this.props
    const lastNoticeId = await AsyncStorage.getItem("lastNoticeId")
    if (lastNoticeId) {
      accountStore.setLastNoticeId(Number(lastNoticeId))
    }
  }

  async componentDidMount() {
    // RNBootSplash.hide({ fade: true })
    const { accountStore, recordStore } = this.props
    this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(async (fcmToken: string) => {
      console.tron.logImportant("< HomeScreen > onTokenRefresh, fcmToken updated: " + fcmToken)
      const user = accountStore.user

      const params = new FormData()
      params.append("user", user.id)
      params.append("firebase_token0", fcmToken)
      params.append("app_ver", packageJson.version)
      let regType
      switch (accountStore.signPath) {
        case "email":
          regType = "e"
          break
        case "kakao":
          regType = "k"
          break
        case "facebook":
          regType = "f"
          break
        case "naver":
          regType = "n"
          break
        case "apple":
          regType = "apple"
          break
        default:
          regType = "u"
      }
      params.append("reg_type", regType)
      params.append("app_device", Platform.OS + " v" + Platform.Version)

      api.updateProfile(user.id, params)
      user.setFirebaseToken0(fcmToken)
    })

    // Foreground 상태에서 notification 을 받았을 경우
    this.notificationListener = firebase.messaging().onMessage((message) => {
      // Process your notification as required
      if (this.props.prescriptionStore) {
        this.props.prescriptionStore.setStatus("pending")
      }
      if (this) {
        this.refresh()
        setTimeout(() => {
          this.props.navigation.setParams({ prescriptionStore: this.props.prescriptionStore }) // TODO: 삭제?
        }, 1000)
      }
      this.props.prescriptionStore.setStatus("done")

      if (message.notification) {
        const { notification } = message
        Toast.show({
          // type: "success",
          title: notification.title,
          description:
            notification.body && notification.body.length > 0 ? "\n\n" + notification.body : null,
          duration: 3500,
          placement: "top",
          style: styles.TOAST_VIEW,
          // onPress: () => {
          //   if (notification.title.includes("브리즈헬러")) {
          //     RootNavigation.navigate("Auth", {
          //       screen: "WebView",
          //       params: { title: "사용법", content: notification.data.content },
          //     })
          //   } else if (notification.title.includes("운동")) {
          //     RootNavigation.navigate("Details", {
          //       screen: "Activity",
          //       params: { title: "운동", recordId: notification.data.record_id },
          //     })
          //   } else {
          //     return null
          //   }
          // },
        })
      }
    })
    this.changeEventListener = AppState.addEventListener("change", this._handleAppStateChange)
    this.checkAndUpdateFCMToken()
    // this.props.navigation.setParams({ prescriptionStore: this.props.prescriptionStore })
    const { navigation } = this.props
    this._unsubscribeFocus = navigation.addListener("focus", this.didFocus)

    // TODO: 복약순응도 기록 관련 코드 삭제
    // 복약종료된 처방전이 있으면 Rating screen 으로 이동
    // 본인처방전만 해당됨 - componentDidMount 안에 있으면 본인 처방전만 해당될 것임
    const pStore = this.props.prescriptionStore
    // if (pStore.dosingEnded.length > 0) {
    //   this.props.navigation.navigate("DrugAdherenceRating", {
    //     prescriptionId: pStore.dosingEnded[0].id,
    //     issueDate: pStore.dosingEnded[0].issue_date,
    //     repTitle:
    //       pStore.dosingEnded[0].disease.length > 0
    //         ? pStore.dosingEnded[0].disease[0].one_liner.length > 0
    //           ? pStore.dosingEnded[0].disease[0].one_liner
    //           : pStore.dosingEnded[0].disease[0].rep_title
    //           ? pStore.dosingEnded[0].disease[0].rep_title
    //           : "질병정보 없음"
    //         : "질병정보 없음",
    //     neededSymptomImprovementRating: true,
    //   })
    //   pStore.removeDosingEndedPrescription(pStore.dosingEnded[0].id)
    // }
    pStore.fetchPrescriptions(accountStore.user.id, `_u${accountStore.user.id}`)

    const showedFirstUseGuide = await AsyncStorage.getItem("showedFirstUseGuide")
    if (!JSON.parse(showedFirstUseGuide)) {
      navigation.navigate("FirstUseGuide")
    }

    await AsyncStorage.setItem("HomeLoading", "OK")

    await appsFlyer.getAppsFlyerUID((err, appsFlyerUID) => {
      if (err) {
        console.error("< Home > componentDidMount appsFlyer getAppsFlyerUID error : ", err)
        console.tron.error("< Home > componentDidMount appsFlyer getAppsFlyerUID error : ", err)
      } else {
        console.log("< Home > componentDidMount appsFlyer getAppsFlyerUID : ", appsFlyerUID)
        console.tron.log("< Home > componentDidMount appsFlyer getAppsFlyerUID : ", appsFlyerUID)
      }
    })

    await appsFlyer.setCustomerUserId(String(accountStore.user.id), (res) => {
      console.log(`< Home > componentDidMount setCustomerUserId result : `, res)
      console.tron.log(`< Home > componentDidMount setCustomerUserId result : `, res)
    })

    this.onDeepLinkCanceller = await appsFlyer.onDeepLink((res) => {
      console.log("< Home > componentDidMount onDeepLink result : ", res)
      console.tron.log("< Home > componentDidMount onDeepLink result : ", res)
      setTimeout(() => {
        // 딥링킹 예시
        if (res?.data?.deep_link_value) {
          navigation.navigate("Auth", {
            screen: res.data.deep_link_value,
          })
        }
      }, 1000)
    })
  }

  didFocus = async () => {
    this.refresh()
    const { accountStore, prescriptionStore, navigation } = this.props
    const { user } = accountStore
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })

    if (!accountStore.selectedMemberKey) {
      return
    }

    // 선택되어 있는 가족회원의 복용할 약인지 확인 후 갱신
    this.onSelectMember(accountStore.selectedMemberKey)

    // 등록된 처방전이 있는 경우, 자료 보존 동의여부 확인 팝업 띄움
    setTimeout(() => {
      if (user.id != -1 && user.newly_agreed == null && prescriptionStore.prescriptionsNumber > 0) {
        navigation.navigate("PrePrescriptionsAgreement")
      }
    }, 1500)

    if (Platform.OS === "ios") {
      setTimeout(() => {
        this.requestReview()
      }, 1000)
    }

    /**
     * 마지막으로 확인한 소식 id 가져오기
     */
    this.fetchLastNoticeId()
    await AsyncStorage.setItem("HomeLoading", "OK")
  }

  componentWillUnmount() {
    this.onActionDisposer()
    this.onSnapshotDisposer2()
    this.onTokenRefreshListener()
    this.notificationListener()
    this._unsubscribeFocus()
    // this.onDeepLinkCanceller()
    this.changeEventListener.remove()
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed)
  }

  showDurGuidance(): boolean {
    if (this.state.showedDurGuidance) {
      return true
    } else {
      this.setState({
        isDurDescPopup: false,
        isDurGuidancePopup: true,
        isModalVisible: true,
        withinDurNotiPeriod: false,
      })
      return false
    }
  }

  backPressed = () => {
    if (this.state.isModalVisible) {
      this.setState({
        isModalVisible: false,
      })
      return true
    } else {
      return this.requestReview()
    }
  }

  requestReview = () => {
    let period = -1
    if (this.state.requestedReviewResponse === "doNotAskAgain") {
      // 다시 묻지 않기는 30일 동안 묻지 않기
      period = 30
    } else if (this.state.requestedReviewResponse === "reviewed") {
      return false
    } else if (this.state.requestedReviewResponse === "refused") {
      // 잔인한 거절은 2주 동안 안 보이기
      period = 14
    } else if (
      this.state.requestedReviewResponse.length == 0 &&
      this.state.requestedReviewDate != 0
    ) {
      period = 30
    }

    // 한달 뒤 다시 묻기
    const prescriptions = this.props.prescriptionStore.prescriptions.get(
      "_u" + this.props.accountStore.user.id,
    )
    if (
      prescriptions &&
      prescriptions.length > 1 &&
      (this.state.todayDate - this.state.requestedReviewDate) / (1000 * 3600 * 24) > period
    ) {
      this.setState({ isModalVisible: true, isDurDescPopup: false, isDurGuidancePopup: false })
      BackHandler.removeEventListener("hardwareBackPress", this.requestReview)
      return true
    }
    return false
  }

  _handleAppStateChange = (nextAppState: string) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
      if (this.pinCodeEnabled && JSON.parse(this.pinCodeEnabled)) {
        // App.tsx 에서 라우팅
        // this.props.navigation.navigate("CodeVerification", { purpose: "verification", preRoute: "Home" })
      } else {
        this.props.prescriptionStore.setStatus("pending")
        this.refresh()
        this.props.prescriptionStore.setStatus("done")
      }
    }
    this.setState({ appState: nextAppState })
  }

  refresh() {
    const { accountStore, takingDrugStore } = this.props
    const { selectedMemberKey } = accountStore

    if (selectedMemberKey) {
      if (selectedMemberKey.includes("_u")) {
        takingDrugStore.fetchDrugsToTake(accountStore.user.id, selectedMemberKey)
      } else if (selectedMemberKey.includes("_c")) {
        takingDrugStore.fetchDrugsToTake(
          accountStore.user.id,
          selectedMemberKey,
          Number(selectedMemberKey.substr(2)),
        )
      } else {
        takingDrugStore.fetchDrugsToTake(
          Number(accountStore.selectedMemberKey.substr(2)),
          selectedMemberKey,
        )
      }
    }

    accountStore.fetchFamily()
    accountStore.fetchFamilyToAgree()

    this.checkMyPoint()
  }

  async checkAndUpdateFCMToken() {
    const { accountStore } = this.props
    const { user, family, countFamilyUnder14 } = accountStore

    firebase.analytics().setUserId(String(user.id))
    firebase.analytics().setUserProperties({
      birth_year: user.birth ? String(user.birth.getFullYear()) : "",
      gender: user.gender ? user.gender : "",
      is_authenticated: String(user.is_authenticated),
      count_family_members: String(family.length),
      count_family_under14: String(countFamilyUnder14),
      count_family_over14: String(family.length - countFamilyUnder14),
    })
    firebase.crashlytics().setUserId(String(user.id))
    const identify = new amplitude.Identify()
      .set("gender", user.gender)
      .set("birth_year", user.birth?.getFullYear())
      .set("is_authenticated", String(user.is_authenticated))
      .set("count_family_members", family.length)
      .set("count_family_under14", countFamilyUnder14)
      .set("count_family_over14", family.length - countFamilyUnder14)
    amplitude.getInstance().identify(identify)

    const prevToken = user.firebase_token0

    // TODO: 알림 권한 요청 관련 리팩토링 필요
    // const pushNotiEnabled = await AsyncStorage.getItem("pushNotiEnabled")
    const pushNotiEnabled = user.presc_noti === "y"
    console.tron.logImportant(
      `< HomeScreen > checkAndUpdateFCMToken, pushNotiEnabled: ${pushNotiEnabled}`,
    )
    if (pushNotiEnabled) {
      const authStatus = await firebase.messaging().hasPermission()

      if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
        console.tron.log("< HomeScreen > Push Notifications AUTHORIZED")
      } else {
        console.tron.log("< HomeScreen > WARNING!! Push Notifications UNAUTHORIZED!!!!")

        this.props.accountStore.setTemporaryPinCodeDisabled(true)
        try {
          if (Platform.OS === "ios") {
            const authStatus = await firebase.messaging().requestPermission({
              announcement: true,
            })
            if (authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED) {
              console.tron.log("User has notification permissions enabled.")
            } else if (authStatus === firebase.messaging.AuthorizationStatus.PROVISIONAL) {
              console.tron.log("User has provisional notification permissions.")
            } else {
              console.tron.log("User has notification permissions disabled")
            }
          } else {
            await requestNotifications(["alert", "sound"])
          }
        } catch (error) {
          console.tron.log("< SettingsScreen > onDurNotiValueChange, rejected permissions", error)
          if (Platform.OS === "ios") {
            Alert.alert(
              "",
              `알림 설정을 변경하려면, 다음 페이지에서 "알림"을 누르고 "알림 허용"을 버튼을 다시 한번 눌러 주세요.`,
              [
                {
                  text: "확인",
                  onPress: () => {
                    Linking.openURL("app-settings://notification/com.onns.papricacare")
                    // AndroidOpenSettings.appNotificationSettings()
                  },
                },
              ],
              { cancelable: false },
            )
          } else {
            Alert.alert(
              "",
              `알림 설정을 변경하려면, 다음 페이지에서 "알림" 버튼을 눌러 주세요.`,
              [
                {
                  text: "확인",
                  onPress: () => {
                    // Linking.openURL('app-settings://notification/com.onns.papricacare')
                    AndroidOpenSettings.appNotificationSettings()
                  },
                },
              ],
              { cancelable: false },
            )
          }
        }
        this.props.accountStore.setTemporaryPinCodeDisabled(false)
      }

      if (__DEV__ && Platform.OS === "ios") {
        const isEmulator = await DeviceInfo.isEmulator()
        if (isEmulator) {
          // https://github.com/invertase/react-native-firebase/commit/8d75b36f485af07ecfa653192ca56f761d0cc5b7
          // 74657374696E67746F6B656E6D6F64756C6172 is hex for "testingtokenmodular"
          await firebase
            .messaging()
            .setAPNSToken("74657374696E67746F6B656E6D6F64756C6172", "sandbox")
        }
      }

      const fcmToken = await firebase.messaging().getToken()
      if (fcmToken) {
        console.tron.logImportant("< HomeScreen > checkAndUpdateFCMToken, fcm Token: ", fcmToken)
        // appsflyer 앱삭제 측정 ~ IOS (APNs), Android(FirebaseToken) 사용
        await appsFlyer.updateServerUninstallToken(fcmToken, (success) => {
          console.log(
            "< Home > checkAndUpdateFCMToken, appsFlyer updateServerUninstallToken",
            success,
          )
          console.tron.log(
            "< Home > checkAndUpdateFCMToken, appsFlyer updateServerUninstallToken",
            success,
          )
        })

        if (prevToken != fcmToken) {
          const params = new FormData()
          params.append("user", user.id)
          params.append("firebase_token0", fcmToken)

          params.append("app_ver", packageJson.version)
          let regType
          switch (accountStore.signPath) {
            case "email":
              regType = "e"
              break
            case "kakao":
              regType = "k"
              break
            case "facebook":
              regType = "f"
              break
            case "naver":
              regType = "n"
              break
            case "apple":
              regType = "apple"
              break
            default:
              regType = "u"
          }
          params.append("reg_type", regType)
          params.append("app_device", `${Platform.OS} v${Platform.Version}`)

          await api.updateProfile(user.id, params)
        }
      } else {
        console.tron.log(
          `< HomeScreen > checkAndUpdateFCMToken, user doesn't have a device token yet`,
        )
      }
    }
  }

  async askToAcceptFamily() {
    const holdingMemberId = await AsyncStorage.getItem("holdFamily")
    this.props.accountStore.familyToAgree.forEach((member) => {
      if (
        member.status === "RJ" ||
        (holdingMemberId && member.id === Number.parseInt(holdingMemberId))
      ) {
        return
      }

      Alert.alert(
        "가족회원 동의 요청",
        `${member.name}님이 당신을 가족회원으로 추가하였습니다. 동의하시겠습니까?`,
        [
          { text: "나중에", onPress: () => this.holdFamily(member), style: "cancel" },
          { text: "거절", onPress: () => this.rejectFamily(member) },
          { text: "동의", onPress: () => this.acceptFamily(member) },
        ],
        { cancelable: false },
      )
    })
  }

  async holdFamily(member: FamilyMember) {
    // start making calls
    const response = await api.apisauce.patch(`/user/family/${member.id}/`, {
      is_agree: false,
      status: "HD",
      user: member.user,
      family_user: member.family_user,
    })
    if (response.ok) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{"가족회원 승인 요청을 보류하였습니다."}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    }
    // 보류 시, 승인 응답자가 앱을 실행시킬 때에만 동일한 팝업을 띄어줌
    AsyncStorage.setItem("holdFamily", String(member.id))
  }

  async acceptFamily(member: FamilyMember) {
    // start making calls
    const response = await api.apisauce.patch(`/user/family/${member.id}/`, {
      is_agree: true,
      status: "AC",
      user: member.user,
      family_user: member.family_user,
    })
    if (response.ok) {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{"가족회원으로 추가되는 것에 동의하였습니다."}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    }
  }

  async rejectFamily(member: FamilyMember) {
    // start making calls
    const response = await api.apisauce.patch(`/user/family/${member.id}/`, {
      is_agree: false,
      status: "RJ",
      user: member.user,
      family_user: member.family_user,
    })
    if (response.ok) {
      Toast.show({
        title: <Text style={styles.TOAST_TEXT}>{"가족회원 승인 요청을 거절하였습니다."}</Text>,
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else {
      firebase
        .crashlytics()
        .recordError(response.status ? response.status : 0, JSON.stringify(response.config))
    }
  }

  setTimeSlot = (timeSlot: string) => {
    this.setState({
      timeSlot: timeSlot,
    })
  }

  //
  // 화면 랜더링
  //
  renderBottomSheet() {
    const { navigation } = this.props
    return (
      <View style={{ marginBottom: 0 }}>
        <View style={styles.BOTTOM_SHEET_HEADER}>
          <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>원하는 항목을 선택해 주세요.</Text>
        </View>
        <View style={{ backgroundColor: color.palette.white }}>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              const pid = this.selectedDrugToTakeGroup?.id.substring(
                0,
                this.selectedDrugToTakeGroup.id.length - 5,
              )
              navigation.navigate("PrescriptionDetail", {
                prescriptionId: pid,
                issueDate: this.selectedDrugToTakeGroup?.drugsToTake[0].prescription.issue_date,
              })
            }}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>처방내역 보기</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.BOTTOM_SHEET_BUTTON}
            underlayColor={color.palette.grey2}
            onPress={() => {
              this.bottomSheetRef.current.close()
              const pid = this.selectedDrugToTakeGroup?.id.substring(
                0,
                this.selectedDrugToTakeGroup.id.length - 5,
              )
              navigation.navigate("Details", {
                screen: "MedicationSettings",
                params: {
                  prescriptionId: pid,
                  issueDate: this.selectedDrugToTakeGroup?.drugsToTake[0].prescription.issue_date,
                },
              })
            }}
          >
            <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>처방내역 수정</Text>
          </TouchableHighlight>
          <SafeAreaView>
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_CANCEL_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={() => this.bottomSheetRef.current.close()}
            >
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>취소</Text>
            </TouchableHighlight>
          </SafeAreaView>
        </View>
      </View>
    )
  }

  render() {
    const {
      accountStore,
      campaignStore,
      navigation,
      takingDrugStore,
      recordStore,
      prescriptionStore,
    } = this.props

    const pStore = this.props.prescriptionStore

    const { timeSlot, date, type, id } = this.state
    const { drugsOnce } = takingDrugStore
    const { distinctTimeSlots } = this.props.prescriptionStore

    const filteredDrugsToTake = this.filterDrugsToTake(
      values(takingDrugStore.drugToTakeGroups),
      timeSlot,
    )

    const filteredDistinctTimeSlots = distinctTimeSlots.filter(() => (distinctTimeSlots.length = 1))
    // { timeSlot } => {getText(timeSlot)}

    const listLength = filteredDrugsToTake.length

    if (pStore.status === "error") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>
            {"서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해주세요."}
          </Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (api.apisauce.headers.Authorization && pStore.status === "unauthorized") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>
            {"서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해주세요."}
          </Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
      setTimeout(() => navigation.navigate("Auth"), 2000)
    }
    return (
      <Screen style={ROOT} preset="fixed">
        <StatusBar backgroundColor={color.backgrounds.lightmode} barStyle="dark-content" />
        <MemberSelectHeader onSelectMember={this.onSelectMember} />
        <View style={{ height: 8 }} />

        {remoteConfig().getBoolean("points_enabled") && (
          <XStack h="$3.5" px={23} jc="space-between" ai="center">
            <Text fow="400" fos={15} lh={18} ls={-0.3} col="#7F8694">
              수집한 포인트
            </Text>
            <XStack
              ai="center"
              onPress={() =>
                navigation.navigate("Details", {
                  screen: "MyPoint",
                })
              }
            >
              <SizableText ff={typography.bold} fos={15} lh={19} ls={-0.5} col="#565E6D">
                {this.state.myPoint === -1 ? "" : `${addComma(this.state.myPoint)}원`}
              </SizableText>

              <SvgArrowRight fill="#7F8694" />
            </XStack>
          </XStack>
        )}

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={() => this.refresh()}
              tintColor={color.primary}
              titleColor={color.primary}
              colors={[color.primary]}
            />
          }
        >
          <View style={{ marginTop: 4 }}>
            {campaignStore.campaigns.length > 0
              ? campaignStore.sortedCampaigns.map((campaign, index) => (
                  <CampaignRow key={`campaign${index}`} data={campaign} />
                ))
              : null}
            {/* #노바티스 01 */}
            {/* {this.state.show_activity_button ? ( */}
            {/* <TouchableOpacity
              onPress={async () => {
                await recordStore.fetchRecords()
                if (recordStore.status === "done" && recordStore.records) {
                  navigation.navigate("Details", {
                    screen: "Records",
                  })
                }
              }}
            >
              <View style={SECTION_SELF_TEST_CONTAINER}>
                <View>
                  <SelfTestHeart style={SELF_TEST_IMAGE} />
                </View>
                <View>
                  <Text style={SELF_TEST_TEXT_BOLD}>호흡기 질환 자가 검사</Text>
                </View>
              </View>
            </TouchableOpacity> */}
            {/* ) : null} */}
            {/* <View
              style={campaignStore.campaigns.length == 0 ? SECTION_HEADER_TIME : SECTION_HEADER}
            >
              <Text style={[typography.title2, { color: color.palette.orange }]}>시간 맞춰 </Text>
              <Text style={typography.title2}>복약하세요 !</Text>
            </View> */}

            {/* {remoteConfig().getBoolean("search_bar_on_home_enabled") && (
              <View
                style={campaignStore.campaigns.length === 0 ? SECTION_HEADER_TIME : SECTION_HEADER}
              >
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={SEARCH_BUTTON}
                  onPress={() => {
                    sendLogEvent("press_search_home")
                    navigation.navigate("Details", { screen: "Search" })
                  }}
                >
                  <Search size={22} color={color.palette.lightGrey} />
                  <Text style={{ marginLeft: 4 }}>
                    질병, 약품 혹은 병원 정보를 검색 할 수 있어요
                  </Text>
                </TouchableOpacity>
              </View>
            )} */}

            {takingDrugStore.drugsToTake.length > 0 && (
              <View>
                <View style={{ height: 12 }} />
                {/* 아침 점심 저녁 스위치 탭 */}
                <TimeSlotRow
                  slots={takingDrugStore.visibleDefaultTimeSlots}
                  onPress={this.setTimeSlot}
                />
              </View>
            )}
            {takingDrugStore.isLoading ? (
              <Spinner color={color.primary} style={{ marginTop: 20, alignSelf: "center" }} />
            ) : (
              <View>
                {/* <FlatList
                  data={filteredDistinctTimeSlots}
                  renderItem={({ item }) => (
                    <View>
                      <RenderSetTime setTime={item} />
                    </View>
                  )}
                  stickyHeaderIndices={[0]}
                /> */}
                {/* <RenderSetTime setTime={filteredDrugsToTake} /> */}
                <FlatList<DrugToTakeGroup>
                  style={TAKING_ITEM_LIST}
                  data={filteredDrugsToTake}
                  renderItem={({ item, index }: { item: DrugToTakeGroup; index: number }) => (
                    <View>
                      <TakingPrescription
                        key={item.id}
                        drugToTakeGroup={item}
                        index={index}
                        listLength={listLength}
                        onPress={() => this.onItemPress(item)}
                        onTakePress={this.onTakePress}
                        date={date}
                      />
                    </View>
                  )}
                  ItemSeparatorComponent={() => (
                    <View style={{ backgroundColor: color.surface.uiDefault }}>
                      <View
                        style={{
                          marginLeft: 16,
                          borderBottomWidth: 1,
                          // borderColor: color.borders.dividerBetweenUI,
                          borderColor: color.borders.dividerBetweenUI,
                        }}
                      />
                    </View>
                  )}
                  keyExtractor={(item) => item.id.toString()}
                  // extraData={{ extra: this.props.prescriptionStore.prescriptions }}
                  ListEmptyComponent={
                    takingDrugStore.drugsToTake.length > 0 ? null : (
                      // 비어있는 아이템으로 표시할게 아니라 아무때나 복용이라도 있으면 유저가 존재하는 아이템에 더 집중 할 수 있게 해야한다.
                      // // 해당 시간에 복약 할 약이 없을 때
                      // // <EmptyDrugsToTakeList timeSlot={timeSlot} />
                      // 복약중인 약이 하나도 없을 때
                      <NoPrescriptionBeingTaken backgroundColor={color.palette.white} />
                    )
                  }
                />
                {drugsOnce.size > 0 ? this.renderDrugsOnce() : null}
              </View>
            )}
          </View>
          {/* 안전영역 */}
          {/* <View style={{ backgroundColor: color.backgrounds.default, height: 16 }} /> */}
          {/* <View style={{ backgroundColor: color.backgrounds.default, height: "100%" }} /> */}
        </ScrollView>
        <RBSheet
          ref={this.bottomSheetRef}
          height={styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * 3 + 14}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            container: {
              backgroundColor: "transparent",
            },
          }}
        >
          <StatusBar backgroundColor={color.dimmedStatusBar} />
          {this.renderBottomSheet()}
        </RBSheet>
        {remoteConfig().getBoolean("main_popup_enabled") && <MainEventPopup />}
      </Screen>
    )
  }

  renderDrugsOnce() {
    const { takingDrugStore } = this.props
    const { date } = this.state
    return (
      <View>
        {/* <View style={[SECTION_HEADER, { marginTop: 18 }]}>
          <Text style={[typography.title2, { color: color.palette.orange }]}>하루에 한번 </Text>
          <Text style={typography.title2}>잊지마세요!</Text>
        </View> */}
        <FlatList<DrugToTake>
          style={TAKING_ITEM_LIST}
          data={values(takingDrugStore.drugsOnce)}
          ListHeaderComponent={
            <View style={SECTION_HEADER_CONAINER}>
              <Text style={SECTION_HEADER_TEXT}>아무때나 한 번</Text>
            </View>
          }
          renderItem={({ item, index }: { item: DrugToTakeGroup; index: number }) => (
            <TakingPrescription
              key={item.id}
              type="once"
              drugToTakeGroup={item}
              index={index}
              listLength={item.drugsToTake?.length}
              onPress={() => this.onItemPress(item)}
              onTakePress={this.onTakePress}
              date={date}
            />
          )}
          ItemSeparatorComponent={() => (
            <View style={{ backgroundColor: color.surface.uiDefault }}>
              <View
                style={{
                  marginLeft: 16,
                  borderBottomWidth: 1,
                  // borderColor: color.borders.dividerBetweenUI,
                  borderColor: color.borders.dividerBetweenUI,
                }}
              />
            </View>
          )}
          refreshControl={
            <RefreshControl
              refreshing={takingDrugStore.isLoading}
              onRefresh={() => this.refresh()}
              tintColor={color.primary}
              titleColor={color.primary}
              colors={[color.primary]}
            />
          }
        />
      </View>
    )
  }

  getImageSource(img: any): any {
    if (img == null || img == "") {
      return {
        uri: `${DEFAULT_API_CONFIG.publicUrl}avatars/avatar_default.png`,
        priority: FastImage.priority.high,
      }
    } else {
      return {
        uri: img,
        priority: FastImage.priority.high,
      }
    }
  }

  toggleModal = () => {
    // DUR 설명 팝업만 작동
    if (this.state.isDurDescPopup) {
      this.setState({ isModalVisible: !this.state.isModalVisible })
    }
  }

  // TODO: needs refactoring? - MedicationScheduleScreen 에 동일한 function 존재함.
  filterDrugsToTake(
    data: ReadonlyArray<DrugToTakeGroup> | undefined,
    timeSlotName?: string,
  ): ReadonlyArray<DrugToTakeGroup> {
    if (!data) return new Array<DrugToTakeGroup>()
    if (data.length == 0) return data

    if (timeSlotName) {
      return data.filter((value) => value.belongsTo(timeSlotName))
    } else {
      const nowSlot = getNowTimeSlotName()
      return data.filter((value) => value.belongsTo(nowSlot))
    }
  }

  filterSetTime(
    data: ReadonlyArray<DrugToTakeGroup> | undefined,
    timeSlotName?: string,
  ): ReadonlyArray<DrugToTakeGroup> {
    if (timeSlotName) {
      return data.filter((value) => value.belongsTo(timeSlotName))
    } else {
      const nowSlot = getNowTimeSlotName()
      return data.filter((value) => value.belongsTo(nowSlot))
    }
  }

  onItemPress = async (item: DrugToTakeGroup) => {
    // this.bottomSheetRef.current.open()
    this.selectedDrugToTakeGroup = item
    const { navigation } = this.props
    const pid = this.selectedDrugToTakeGroup?.id.substring(
      0,
      this.selectedDrugToTakeGroup.id.length - 5,
    )
    navigation.navigate("PrescriptionDetail", {
      prescriptionId: pid,
      issueDate: this.selectedDrugToTakeGroup?.drugsToTake[0].prescription.issue_date,
    })
  }

  onTakePress = async (
    item: DrugToTake,
    takeAllPressed?: boolean,
    allTaken?: boolean,
    groupType?: "once" | undefined,
  ) => {
    const { takingDrugStore } = this.props
    const { drugsOnce, drugToTakeGroups } = takingDrugStore
    const statusToChange = takeAllPressed
      ? allTaken
        ? "taken"
        : "ready"
      : item.status === "ready"
      ? "taken"
      : "ready"

    const drugTakeGroup = takeAllPressed
      ? !groupType
        ? drugToTakeGroups.get(`${item.prescription.id}${item.when}`)
        : drugsOnce.get(`${item.prescription.id}${item.when}`)
      : undefined

    const result = await api.takeDrug(item.id, statusToChange, drugTakeGroup)
    if (result.kind === "ok") {
      if (takeAllPressed) {
        takingDrugStore.setMedicationStatus(
          `${item.prescription.id}${item.when}`,
          statusToChange,
          groupType,
        )
        if (statusToChange === "taken") {
          Toast.show({
            title: <Text style={TAKE_DRUG_TOAST_TEXT}>{"✔︎ 복약 완료"}</Text>,
            duration: 2000,
            placement: "top",
            style: TAKE_DRUG_TOAST_VIEW,
          })

          if (remoteConfig().getBoolean("points_enabled")) {
            const drugTakeIds = drugTakeGroup.drugsToTake.map((item) => item.id)
            const result = validatePoints(POINT_TYPES.drugTaken, drugTakeIds)
            if (result.validPoints > 0) {
              this.props.navigation.navigate("Details", {
                screen: "GetPoint",
                params: {
                  pid: item.prescription.id,
                  description: getPrescriptionTitle(item.prescription),
                  pointType: POINT_TYPES.drugTaken,
                  drugTakeIds: result.validDrugTakeIds,
                  alarmName: item.alarm_name,
                  point: result.validPoints,
                },
              })
            }
          }
        } else {
          Toast.show({
            title: <Text style={TAKE_DRUG_TOAST_TEXT_CANCEL}>{"✘ 복약 취소"}</Text>,
            duration: 2000,
            placement: "top",
            style: TAKE_DRUG_TOAST_VIEW_CANCEL,
          })
        }
      } else {
        item.setStatus(statusToChange)
        if (remoteConfig().getBoolean("points_enabled") && statusToChange === "taken") {
          const result = validatePoints(POINT_TYPES.drugTaken, [item.id])
          if (result.validPoints > 0) {
            this.props.navigation.navigate("Details", {
              screen: "GetPoint",
              params: {
                pid: item.prescription.id,
                description: getPrescriptionTitle(item.prescription),
                pointType: POINT_TYPES.drugTaken,
                drugTakeIds: result.validDrugTakeIds,
                alarmName: item.alarm_name,
                point: result.validPoints,
              },
            })
          }
        }
      }
    } else {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>
            {"서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요."}
          </Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    }
  }

  // 호흡운동 및 평가시험 활성화 버튼, 사용자(본인, 가족)를 선택하였을 경우 실행
  showActivityButton = async (memberId: number) => {
    const result = await api.getUserProfile(memberId)
    if (result.kind === "ok") {
      this.setState({
        show_activity_button:
          result.data.results.length > 0 ? result.data.results[0].show_activity_button : false,
      })
    }
  }

  onSelectMember = (memberKey: string, member?: FamilyMember) => {
    const { accountStore, prescriptionStore, takingDrugStore } = this.props
    const { drugsToTake } = takingDrugStore

    if (prescriptionStore.selectedFamilyMemberKey !== memberKey) {
      prescriptionStore.setSelectedFamilyMemberKey(memberKey)
    }

    let prevMemberKey
    if (drugsToTake.length > 0) {
      prevMemberKey = `_${drugsToTake[0].owner.type.charAt(0)}${drugsToTake[0].owner.id}`
    }

    if (accountStore.status === "error" || prescriptionStore.status === "error") {
      this.refresh()
    } else if (
      memberKey &&
      (drugsToTake.length == 0 || (drugsToTake.length > 0 && memberKey !== prevMemberKey))
    ) {
      if (memberKey.includes("_u")) {
        takingDrugStore.fetchDrugsToTake(accountStore.user.id, memberKey)
        this.showActivityButton(accountStore.user.id)
      } else if (memberKey.includes("_c")) {
        takingDrugStore.fetchDrugsToTake(
          accountStore.user.id,
          memberKey,
          Number(memberKey.substr(2)),
        )
        this.showActivityButton(Number(memberKey.substr(2)))
      } else {
        takingDrugStore.fetchDrugsToTake(
          member?.family_user ?? Number(accountStore.selectedMemberKey.substr(2)),
          memberKey,
        )
        this.showActivityButton(Number(accountStore.selectedMemberKey.substr(2)))
      }
    }
  }
}
