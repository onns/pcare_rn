import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { DateTime } from "luxon"
import { observer } from "mobx-react-lite"
import { Spinner, Toast } from "native-base"
import React, { useEffect, useLayoutEffect, useRef, useState } from "react"
import {
  FlatList,
  StatusBar,
  TextStyle,
  TouchableHighlight,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import FastImage from "react-native-fast-image"
import { ScrollView } from "react-native-gesture-handler"
import RBSheet from "react-native-raw-bottom-sheet"
import { SafeAreaInsetsContext, SafeAreaView } from "react-native-safe-area-context"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { withNavigationFocus } from "@react-navigation/compat"
import { CompositeNavigationProp, RouteProp, useFocusEffect } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import SvgArrowRight from "../assets/images/arrowRight.svg"
import SvgCalendar from "../assets/images/calendar.svg"
import SvgDisease from "../assets/images/disease.svg"
import SvgDrugEmptyImage from "../assets/images/drugEmptyImage.svg"
import SvgHospital from "../assets/images/hospital.svg"
import SvgMedicine from "../assets/images/medicine.svg"
import TouchableItem from "../components/button/TouchableItem"
import CloseButton from "../components/CloseButton"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { getText } from "../components/TimeSlotRow"
import { formatTime } from "../lib/DateUtil"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { Disease } from "../stores/Disease"
import { MedicationSchedule } from "../stores/drug-to-take/MedicationSchedule"
import { FamilyMember } from "../stores/FamilyMember"
import { Hospital } from "../stores/HospitalStore"
import { Prescription, PrescriptionDetail } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"
import { backgroundColor } from "../theme/calendar"
import { EditMedicationDetailMode } from "./EditMedicationDetailScreen"
import { SearchDrugMode } from "./SearchDrugScreen"

type MedicationSettingsScreenRouteProp = RouteProp<DetailsStackParamList, "MedicationSettings">

type MedicationSettingsScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "MedicationSettings">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface MedicationSettingsScreenProps {
  navigation: MedicationSettingsScreenNavigationProp
  route: MedicationSettingsScreenRouteProp
}

const ROOT: ViewStyle = {
  backgroundColor: color.background,
}
const TOUCHABLE_ITEM: ViewStyle = {
  backgroundColor: "#fff",
  paddingTop: 24,
  paddingLeft: 16,
  paddingRight: 16,
  paddingBottom: 24,
}
const TOUCHABLE_MIDDLE_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
}
const TOUCHABLE_LAST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
  paddingBottom: 24,
}
const TOUCHABLE_FIRST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
}
const TOUCHABLE_FIRST_LAST_ITEM: ViewStyle = {
  ...TOUCHABLE_ITEM,
  paddingVertical: 24,
}
const DISEASE_DETAIL_NAME: TextStyle = {
  ...typography.sub2,
  opacity: 0.5,
  marginTop: 6,
}
const RIGHT_ARROW: ViewStyle = {
  paddingRight: 8,
  paddingBottom: 0,
  alignItems: "flex-end",
  justifyContent: "center",
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const DRUG_SECTION_HEADER: ViewStyle = {
  backgroundColor: color.palette.white,
  paddingTop: 24,
  paddingBottom: 8,
  paddingHorizontal: 16,
}
const TITLE2_ORANGE: TextStyle = { ...typography.title2, color: color.primary }
const SECTION1 = {
  backgroundColor: color.palette.white,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
  marginBottom: 8,
  paddingBottom: 24,
}
const SECTION2 = {
  backgroundColor: color.palette.white,
  borderBottomColor: color.palette.pale,
  borderTopColor: color.palette.pale,
  borderBottomWidth: 1,
  borderTopWidth: 1,
  marginBottom: 8,
  paddingBottom: 24,
}
const SECTION3 = {
  backgroundColor: color.palette.white,
  paddingRight: 8,
  borderTopColor: color.palette.pale,
  borderTopWidth: 1,
  paddingBottom: 24,
  marginBottom: 41,
  borderBottomColor: color.palette.pale,
  borderBottomWidth: 1,
}
const ALARM_BOX: ViewStyle = {
  backgroundColor: color.palette.gray5,
  borderRadius: 1,
  borderWidth: 1,
  borderColor: color.palette.pale,
  paddingHorizontal: 7,
  paddingTop: 2,
  paddingBottom: 3,
  marginLeft: 8,
  marginBottom: 8,
}
const ALARM_TIME: TextStyle = { ...typography.sub2, opacity: 0.7 }
const DRUG_BUTTON: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingLeft: 16,
  paddingTop: 24,
  paddingRight: 8,
  marginBottom: 4,
}
const DRUG_LINE = {
  backgroundColor: color.palette.pale,
  height: 1,
  marginTop: 16,
  marginLeft: 16,
  marginRight: 16,
}
const DISEASE_DIVIDER = {
  marginLeft: 16,
  marginRight: 16,
  height: 1,
  backgroundColor: color.palette.gray5,
}
const DRUG_IMAGE: ViewStyle = {
  width: 84,
  height: 48,
  borderRadius: 5,
}
const BORDERED_BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 17,
  color: color.palette.brightOrange,
}
const REMOVE_BUTTON: ViewStyle = {
  height: 32,
  borderRadius: 16,
  backgroundColor: color.palette.gray5,
  paddingVertical: 6,
  paddingHorizontal: 16,
  justifyContent: "center",
  alignItems: "center",
}
const REMOVE_BUTTON_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.copper,
}
const DOCTOR_NAME = {
  ...typography.sub2,
  marginTop: 4,
  opacity: 1,
}
const DOCTOR = {
  ...typography.sub2,
  marginTop: 4,
  opacity: 0.5,
}
const HOSPITAL_DESC = {
  ...typography.body,
  marginTop: 8,
  color: color.palette.grey70,
}
const HOSPITAL_ITEM = { paddingTop: 24, paddingHorizontal: 16 }

const DRUG_NAME = { flex: 1, ...typography.title3, paddingRight: 18 }
const DRUG_BUTTON_RIGHT = {
  flex: 1,
  marginLeft: 8,
  marginBottom: 4,
}
const MEDICATION_PERIOD = {
  ...typography.sub2,
  color: color.palette.grey50,
  marginLeft: 4,
  marginBottom: 4,
}
const DOSING_DAYS = {
  ...typography.sub2,
  color: color.palette.black,
}
const DOSE_INFO_TEXT = { ...typography.sub2, paddingLeft: 8, marginBottom: 8 }
const FULL = { flex: 1 }

/** 처방내역 수정 화면 */
const BOTTOM_SHEET_CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
  borderTopLeftRadius: 30,
  borderTopRightRadius: 30,
}
const MedicationSettingsScreen: React.FunctionComponent<MedicationSettingsScreenProps> = observer(
  (props) => {
    const {
      accountStore,
      takingDrugStore,
      prescriptionStore,
      searchDiseaseStore,
      searchHospitalStore,
    } = useStores()
    const { navigation, route } = props
    const prescriptionId = route.params.prescriptionId
    const issueDate = route.params.issueDate
    const prescription: Prescription = prescriptionStore.prescriptionDetail

    const [popupVisible, setPopupVisible] = useState(false)
    const [popupTitle, setPopupTitle] = useState("")
    const [popupMessage, setPopupMessage] = useState("")
    const [popupButton1, setPopupButton1] = useState(<View />)
    const [popupButton2, setPopupButton2] = useState(<View />)
    const [popupButton3, setPopupButton3] = useState(<View />)
    const [
      selectedPrescriptionDetail,
      setSelectedPrescriptionDetail,
    ] = useState<PrescriptionDetail>()
    const [selectedPrescriptionDetailIdx, setSelectedPrescriptionDetailIdx] = useState(-1)
    const bottomSheetRef = useRef(null)

    let minStartDate: DateTime = DateTime.local()
    let maxEndDate: DateTime = DateTime.local()

    useLayoutEffect(() => {
      navigation.setOptions({
        headerLeft: () => <CloseButton onPress={navigation.goBack} style={{ marginLeft: 24 }} />,
        headerTitle: "처방내역 수정",
        headerStyle: { elevation: 1 },
      })
    }, [navigation])

    useFocusEffect(
      React.useCallback(() => {
        firebase.analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        })
        amplitude.getInstance().logEvent("screen_view", { screen: route.name })

        prescriptionStore.fetchPrescriptionDetail(prescriptionId, accountStore.user.id)

        // selectedDisease 가 존재하면 해당 disease 추가
        const { environment, selectedDisease } = searchDiseaseStore
        const { selectedHospital } = searchHospitalStore
        if (selectedDisease) {
          // hook 에서 async 함수를 사용하기 위함.
          ;(async () => {
            const { id, disease, subdiseases } = prescriptionStore.prescriptionDetail
            let params
            if (disease.length === 0) {
              params = { disease: [selectedDisease.id] }
            } else {
              params = {
                subdiseases: subdiseases.map((disease) => disease.id).concat([selectedDisease.id]),
              }
            }

            const result = await api.patchPrescription(id, params)
            if (result.kind === "ok") {
              prescription.addDisease(selectedDisease)
              const { user, selectedMemberKey } = accountStore
              if (selectedMemberKey.includes("u")) {
                prescriptionStore.fetchPrescriptions(user.id, selectedMemberKey)
              } else if (selectedMemberKey.includes("f")) {
                prescriptionStore.fetchPrescriptions(
                  Number(selectedMemberKey.substr(2)),
                  selectedMemberKey,
                )
              } else {
                prescriptionStore.fetchPrescriptions(
                  user.id,
                  selectedMemberKey,
                  Number(selectedMemberKey.substr(2)),
                )
              }
            } else {
              Toast.show({
                title: (
                  <Text
                    style={styles.TOAST_TEXT}
                  >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
                ),
                duration: 2000,
                placement: "top",
                style: styles.TOAST_VIEW,
              })
            }
            searchDiseaseStore.initiate()
          })()
        }
        // hospital은 array가 아님
        if (selectedHospital) {
          // hook 에서 async 함수를 사용하기 위함.
          ;(async () => {
            const { id, hospital } = prescriptionStore.prescriptionDetail
            let params
            if (!hospital) {
              params = { hospital: selectedHospital.id }
            }

            const result = await api.patchPrescription(id, params)
            if (result.kind === "ok") {
              prescription.setHospital(selectedHospital)
              const { user, selectedMemberKey } = accountStore
              if (selectedMemberKey.includes("u")) {
                prescriptionStore.fetchPrescriptions(user.id, selectedMemberKey)
              } else if (selectedMemberKey.includes("f")) {
                prescriptionStore.fetchPrescriptions(
                  Number(selectedMemberKey.substr(2)),
                  selectedMemberKey,
                )
              } else {
                prescriptionStore.fetchPrescriptions(
                  user.id,
                  selectedMemberKey,
                  Number(selectedMemberKey.substr(2)),
                )
              }
            } else {
              Toast.show({
                title: (
                  <Text
                    style={styles.TOAST_TEXT}
                  >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
                ),
                duration: 2000,
                placement: "top",
                style: styles.TOAST_VIEW,
              })
            }
            searchHospitalStore.initiate()
          })()
        }
      }, [prescriptionStore.prescriptionDetail]),
    )

    useEffect(() => {
      const { selectedMemberKey, user } = accountStore
      prescriptionStore.fetchPrescriptionDetail(prescriptionId, user.id)
      if (!selectedMemberKey) return
      if (selectedMemberKey.includes("_u")) {
        takingDrugStore.fetchMedicationSchedule(user.id, selectedMemberKey)
      } else if (selectedMemberKey.includes("_c")) {
        takingDrugStore.fetchMedicationSchedule(
          user.id,
          selectedMemberKey,
          Number(selectedMemberKey.substr(2)),
        )
      } else {
        takingDrugStore.fetchMedicationSchedule(
          Number(selectedMemberKey.substr(2)),
          selectedMemberKey,
        )
      }
    }, [accountStore.selectedMemberKey])

    const onSelectMember = (memberKey: string, member?: FamilyMember) => {}

    const generatePeriods = (date: DateTime, schedules: MedicationSchedule[]) => {
      const arr = []

      schedules.forEach((schedule) => {
        if (date.equals(schedule.start)) {
          arr.push({
            color: color.palette.greenishTeal,
            startingDay: true,
            diseaseName: schedule.disease.rep_title,
          })
        } else if (schedule.start < date && date < schedule.end) {
          arr.push({ color: color.palette.greenishTeal, diseaseName: schedule.disease.rep_title })
        } else if (date.equals(schedule.end)) {
          arr.push({
            color: color.palette.greenishTeal,
            endingDay: true,
            diseaseName: schedule.disease.rep_title,
          })
        }
      })
      return arr
    }

    const getMarkedDates = (schedules: MedicationSchedule[]) => {
      if (schedules.length == 0) {
        return null
      }

      let arr = []
      // 가장 빠른 시작일 구하기
      for (let i = 0; i < schedules.length; i++) {
        const schedule = schedules[i]
        if (i == 0) {
          minStartDate = schedule.start
          continue
        }
        if (minStartDate > schedule.start) {
          minStartDate = schedule.start
        }
      }

      // 가장 늦는 마감일 구하기
      for (let i = 0; i < schedules.length; i++) {
        const schedule = schedules[i]
        if (i == 0) {
          maxEndDate = schedule.end
          continue
        }
        if (maxEndDate < schedule.end) {
          maxEndDate = schedule.end
        }
      }

      // 빠른 시작일 순서로 스케줄 정렬
      const sortedSchedules = schedules.slice()
      sortedSchedules.sort((a, b) => {
        // 0보다 작은 경우 a를 b보다 낮은 색인으로 정렬 (a가 먼저옴)
        if (a.start < b.start) {
          return -1
        } else if (a.start.equals(b.start)) {
          return 0
        } else {
          return 1
        }
      })

      for (let date = minStartDate; date <= maxEndDate; ) {
        arr.push({
          [date.toSQLDate()]: {
            periods: generatePeriods(date, sortedSchedules),
          },
        })
        date = date.plus({ days: 1 })
      }

      // slicing curly brackets
      arr = arr.map((a) => {
        return JSON.stringify(a).slice(1, -1)
      })

      const arrString = arr.join(",")
      return JSON.parse("{" + arrString + "}")
    }

    const editMedicationDetail = () => {
      bottomSheetRef.current.close()
      navigation.navigate("EditMedicationDetail", {
        mode: EditMedicationDetailMode.EditPrescriptionDetail,
        prescriptionDetailIdx: selectedPrescriptionDetailIdx,
      })
    }

    const removeDisease = async (selectedDisease: Disease, isSubDisease?: boolean) => {
      const { environment } = prescriptionStore
      let params
      if (isSubDisease) {
        params = {
          subdiseases: prescription.subdiseases
            .filter((disease) => disease.id != selectedDisease.id)
            .map((disease) => disease.id),
        }
      } else {
        params = {
          disease: prescription.disease
            .filter((disease) => disease.id != selectedDisease.id)
            .map((disease) => disease.id),
        }
      }
      const result = await api.patchPrescription(prescriptionId, params)

      if (result.kind === "ok") {
        prescription.removeDisease(selectedDisease, isSubDisease)
        prescriptionStore.removeDisease(prescriptionId, selectedDisease.id, isSubDisease)
        setPopupVisible(false)
      }
    }
    const removeHospital = async (selectedHospital: Hospital) => {
      const params = {
        hospital: null,
        doctor: null,
      }

      const result = await api.patchPrescription(prescriptionId, params)

      if (result.kind === "ok") {
        prescription.removeHospital(selectedHospital)
        prescriptionStore.removeHospital(prescriptionId, selectedHospital.id)
        setPopupVisible(false)
      }
    }

    const removePrescriptionDetail = async () => {
      const result = await api.deletePrescriptionDetail(selectedPrescriptionDetail.id)

      if (result.kind === "ok") {
        prescription.removePrescriptionDetail(selectedPrescriptionDetail)
        prescriptionStore.removePrescriptionDetail(prescriptionId, selectedPrescriptionDetail.id)
        setPopupVisible(false)
      }
    }

    const popupRemovePrescriptionDetail = () => {
      bottomSheetRef.current.close()
      setPopupTitle("정말로 삭제하시겠습니까?")
      setPopupMessage(
        "삭제된 정보는 모든 의료기록에서 사라지며, 다시 복구 할 수 없습니다. 정말로 삭제하시겠습니까?",
      )
      setPopupButton1(getPopupButton(removePrescriptionDetail, "확인"))
      setPopupButton2(getPopupButton(() => setPopupVisible(false), "취소", "cancel"))
      setTimeout(() => {
        setPopupVisible(true)
      }, 250) // bottomSheet close 와 동시에 처리 시 popup 이 나타나지 않는 문제가 있음
    }

    const popupRemoveDisease = (disease: Disease, isSubDisease?: boolean) => {
      bottomSheetRef.current.close()
      setPopupTitle("정말로 삭제하시겠습니까?")
      setPopupMessage(
        "삭제된 정보는 모든 의료기록에서 사라지며, 다시 복구 할 수 없습니다. 정말로 삭제하시겠습니까?",
      )
      setPopupButton1(getPopupButton(() => removeDisease(disease, isSubDisease), "확인"))
      setPopupButton2(getPopupButton(() => setPopupVisible(false), "취소", "cancel"))
      setTimeout(() => {
        setPopupVisible(true)
      }, 250) // bottomSheet close 와 동시에 처리 시 popup 이 나타나지 않는 문제가 있음
    }

    const popupRemoveHospital = (hospital: Hospital) => {
      bottomSheetRef.current.close()
      setPopupTitle("정말로 삭제하시겠습니까?")
      setPopupMessage(
        "삭제된 정보는 모든 의료기록에서 사라지며, 다시 복구 할 수 없습니다. 정말로 삭제하시겠습니까?",
      )
      setPopupButton1(getPopupButton(() => removeHospital(hospital), "확인"))
      setPopupButton2(getPopupButton(() => setPopupVisible(false), "취소", "cancel"))
      setTimeout(() => {
        setPopupVisible(true)
      }, 250) // bottomSheet close 와 동시에 처리 시 popup 이 나타나지 않는 문제가 있음
    }

    const renderDiseaseDivider = () => <View style={DISEASE_DIVIDER} />

    const renderBottomSheet = () => {
      return (
        <View>
          <View style={styles.BOTTOM_SHEET_HEADER}>
            <Text style={styles.BOTTOM_SHEET_HEADER_TEXT}>원하시는 항목을 선택해 주세요.</Text>
          </View>
          <View>
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={editMedicationDetail}
            >
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>수정</Text>
            </TouchableHighlight>
            {/* <TouchableHighlight
              style={styles.BOTTOM_SHEET_LAST_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={() => null}
            >
              <Text style={styles.BOTTOM_SHEET_BUTTON_TEXT}>복약 중단</Text>
            </TouchableHighlight> */}
            <TouchableHighlight
              style={styles.BOTTOM_SHEET_LAST_BUTTON}
              underlayColor={color.palette.grey2}
              onPress={popupRemovePrescriptionDetail}
            >
              <Text style={[styles.BOTTOM_SHEET_BUTTON_TEXT, { color: color.palette.copper }]}>
                삭제
              </Text>
            </TouchableHighlight>
          </View>
          <TouchableHighlight
            style={{
              ...styles.BOTTOM_SHEET_CANCEL_BUTTON,
            }}
            underlayColor={color.palette.grey2}
            onPress={() => bottomSheetRef.current.close()}
          >
            <Text style={styles.BOTTOM_SHEET_CANCEL_BUTTON_TEXT}>취소</Text>
          </TouchableHighlight>
        </View>
      )
    }

    if (prescriptionStore.status == "error") {
      Toast.show({
        title: (
          <Text style={styles.TOAST_TEXT}>{`서버로부터 데이터를 가져오지 못 하였습니다.`}</Text>
        ),
        duration: 2000,
        placement: "top",
        style: styles.TOAST_VIEW,
      })
    } else if (prescriptionStore.status == "pending") {
      return <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
    }

    return (
      <Screen style={ROOT} preset="fixed" unsafe>
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView>
          <View style={SECTION1}>
            {prescription &&
              prescription.disease.map((disease, index) => (
                <View key={disease.id}>
                  <View
                    // style={prescriptionDetail!.disease.length == (index + 1) && prescriptionDetail!.subdiseases.length == 0 ? TOUCHABLE_ITEM : TOUCHABLE_FIRST_ITEM}
                    style={
                      index !== 0
                        ? prescription!.disease.length === index + 1
                          ? prescription.subdiseases.length === 0
                            ? TOUCHABLE_LAST_ITEM
                            : TOUCHABLE_MIDDLE_ITEM
                          : TOUCHABLE_MIDDLE_ITEM
                        : prescription.subdiseases.length === 0
                        ? TOUCHABLE_FIRST_LAST_ITEM
                        : TOUCHABLE_FIRST_ITEM
                    }
                  >
                    <View style={ROW}>
                      <SvgDisease
                        width={40}
                        height={40}
                        style={{ marginLeft: -4, marginRight: 8 }}
                      />
                      <View style={{ flex: 7 }}>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                          {disease.feature || disease.rep_title}
                        </Text>
                        <Text style={DISEASE_DETAIL_NAME}>{disease.code + " " + disease.name}</Text>
                      </View>
                      <View style={RIGHT_ARROW}>
                        <TouchableItem
                          style={REMOVE_BUTTON}
                          onPress={() => popupRemoveDisease(disease)}
                        >
                          <Text style={REMOVE_BUTTON_TEXT}>삭제</Text>
                        </TouchableItem>
                      </View>
                    </View>
                  </View>
                  {prescription!.disease.length === index + 1
                    ? prescription.subdiseases.length === 0
                      ? null
                      : renderDiseaseDivider()
                    : renderDiseaseDivider()}
                </View>
              ))}
            {prescription &&
              prescription.subdiseases.map((disease: Disease, index: number) => (
                <View key={disease.id}>
                  <View
                    style={TOUCHABLE_MIDDLE_ITEM}
                    // onPress={() => {}}
                  >
                    <View style={ROW}>
                      <SvgDisease
                        width={40}
                        height={40}
                        style={{ marginLeft: -4, marginRight: 8 }}
                      />
                      <View style={{ flex: 7 }}>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                          {disease.feature || disease.rep_title}
                        </Text>
                        <Text style={DISEASE_DETAIL_NAME}>
                          {disease.code} {disease.name}
                        </Text>
                      </View>
                      <View style={RIGHT_ARROW}>
                        <TouchableItem
                          style={REMOVE_BUTTON}
                          onPress={() => popupRemoveDisease(disease, true)}
                        >
                          <Text style={REMOVE_BUTTON_TEXT}>삭제</Text>
                        </TouchableItem>
                      </View>
                    </View>
                  </View>
                  {prescription!.subdiseases.length === index + 1 ? null : renderDiseaseDivider()}
                </View>
              ))}
            {prescription?.disease.length === 0 && prescription?.subdiseases.length === 0 ? (
              <View>
                <View style={{ ...HOSPITAL_ITEM, paddingBottom: 8 }}>
                  <Text style={typography.title2}>질병</Text>
                  <Text style={HOSPITAL_DESC}>어떤 질병으로 약을 복용하시나요?</Text>
                </View>
              </View>
            ) : null}
            <TouchableItem
              style={styles.LARGE_RADIUS_BUTTON}
              onPress={() => navigation.navigate("SearchDisease")}
            >
              <Text style={styles.LARGE_RADIUS_BUTTON_TEXT}>
                {prescription?.disease.length === 0 && prescription?.subdiseases.length === 0
                  ? `질병 입력`
                  : `질병 추가`}
              </Text>
            </TouchableItem>
          </View>
          {prescription?.hospital ? (
            <View style={SECTION2}>
              <View
                style={HOSPITAL_ITEM}
                // onPress={
                // navigation.navigate("HospitalDetail", {
                //   id: prescriptionDetail != undefined ? prescriptionDetail.hospital!.id : -1,
                // })
                // }
              >
                <View style={ROW}>
                  <SvgHospital width={40} height={40} style={{ marginLeft: -4, marginRight: 8 }} />
                  <View style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                      <Text numberOfLines={1} ellipsizeMode="tail" style={typography.title3}>
                        {prescription?.hospital?.name ?? ""}
                      </Text>
                    </View>
                    {prescription?.doctor?.name ? (
                      <View style={ROW}>
                        <Text style={DOCTOR}>담당의 </Text>
                        <Text style={DOCTOR_NAME}>
                          {prescription.doctor.name}
                          {prescription.doctor.narrow_major
                            ? `(${prescription.doctor.narrow_major ?? ""})`
                            : ""}
                        </Text>
                      </View>
                    ) : (
                      <Text style={DOCTOR}>의료진 정보가 없습니다.</Text>
                    )}
                  </View>
                  <View style={RIGHT_ARROW}>
                    <TouchableItem
                      style={REMOVE_BUTTON}
                      onPress={() => popupRemoveHospital(prescription.hospital)}
                    >
                      <Text style={REMOVE_BUTTON_TEXT}>삭제</Text>
                    </TouchableItem>
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={SECTION2}>
              <View style={{ ...HOSPITAL_ITEM, paddingBottom: 8 }}>
                <Text style={typography.title2}>병원</Text>
                <Text style={HOSPITAL_DESC}>처방 받으신 병원을 입력해 주세요.</Text>
              </View>
              <TouchableItem
                style={styles.LARGE_RADIUS_BUTTON}
                onPress={() => navigation.navigate("SearchHospital")}
              >
                <Text style={styles.LARGE_RADIUS_BUTTON_TEXT}>병원 입력</Text>
              </TouchableItem>
            </View>
          )}

          <View style={SECTION3}>
            <View style={DRUG_SECTION_HEADER}>
              <Text style={typography.title2}>
                약 <Text style={TITLE2_ORANGE}>{prescription?.prescription_details?.length}</Text>
              </Text>
            </View>
            {prescription?.prescription_details?.length > 0 ? (
              <FlatList
                data={prescription.prescription_details.slice()}
                renderItem={({ item, index }: { item: PrescriptionDetail; index: number }) => (
                  <View>
                    <TouchableOpacity
                      onPress={() => {
                        setSelectedPrescriptionDetail(item)
                        setSelectedPrescriptionDetailIdx(index)
                        bottomSheetRef.current.open()
                      }}
                      style={DRUG_BUTTON}
                    >
                      {item.drug.image_url ? (
                        <FastImage source={{ uri: item.drug.image_url }} style={DRUG_IMAGE} />
                      ) : (
                        <SvgDrugEmptyImage
                          width={84}
                          height={48}
                          style={{ alignSelf: "flex-start" }}
                        />
                      )}
                      <View style={DRUG_BUTTON_RIGHT}>
                        <View style={ROW}>
                          <Text style={DRUG_NAME} numberOfLines={1} ellipsizeMode="tail">
                            {item.drug.shortenedName}
                          </Text>
                          <SvgArrowRight width={24} height={24} />
                        </View>
                        <View style={ROW}>
                          <SvgCalendar width={16} height={16} style={{ marginTop: 1 }} />
                          <Text style={MEDICATION_PERIOD}>
                            {item.start_at.toFormat("yyyy.MM.dd")} ~{" "}
                            {item.end_at.toFormat("yyyy.MM.dd")} |{" "}
                            <Text style={DOSING_DAYS}>총{item.dosing_days_num}일</Text>
                          </Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <View style={{ ...ROW, paddingLeft: 16 }}>
                      <SvgMedicine width={84} height={24} />
                      <View style={FULL}>
                        <Text style={DOSE_INFO_TEXT}>
                          {item.dose}
                          {item.drug.unitConverted} | 하루 {item.doses_num}번 |{" "}
                          {item.per_days == 1 ? "매일" : `${item.per_days}일간격`}
                        </Text>
                        <View style={{ ...ROW, flexWrap: "wrap" }}>
                          {item.timeslots.map((timeSlot, index) => (
                            <View key={index} style={ALARM_BOX}>
                              <Text style={ALARM_TIME}>
                                {timeSlot.name == "custom"
                                  ? formatTime(timeSlot.when)
                                  : getText(timeSlot.name)}
                              </Text>
                            </View>
                          ))}
                          {/* <View style={ALARM_BOX}>
                          <Text style={ALARM_TIME}>아침 (오전 8:00)</Text>
                        </View>
                        <View style={ALARM_BOX}>
                          <Text style={ALARM_TIME}>점심 (오후 1:00)</Text>
                        </View>
                        <View style={ALARM_BOX}>
                          <Text style={ALARM_TIME}>저녁 (오후 7:00)</Text>
                        </View>
                        <View style={ALARM_BOX}>
                          <Text style={ALARM_TIME}>오후 8:00</Text>
                        </View> */}
                        </View>
                        {/* {item.drug.one_liner ? (
                          <View style={DRUG_ONE_LINER}>
                            <Text style={typography.body}>{item.drug.one_liner}</Text>
                          </View>
                        ) : null} */}
                      </View>
                    </View>
                  </View>
                )}
                ItemSeparatorComponent={() => <View style={DRUG_LINE} />}
                // onRefresh={pStore.fetchPrescriptions}
                // refreshing={pStore.isLoading}
                keyExtractor={(item) => item.id.toString()}
                // extraData={{ extra: store.prescriptions }}
                // ListEmptyComponent={<EmptyPrescriptionList />}
              />
            ) : null}
            <View style={{ marginTop: 16 }}>
              <TouchableItem
                style={styles.LARGE_RADIUS_BUTTON}
                onPress={() =>
                  navigation.navigate("SearchDrug", { mode: SearchDrugMode.NewPrescriptionDetail })
                }
              >
                <Text style={styles.LARGE_RADIUS_BUTTON_TEXT}>약 추가</Text>
              </TouchableItem>
            </View>
          </View>
        </ScrollView>
        {/* <Button
          primary
          block
          onPress={() => null}
          style={{ height: 56, marginHorizontal: 16, marginBottom: 16 }}
        >
          <Text style={{ ...typography.title3, color: "#fff" }}>확인</Text>
        </Button> */}
        <Popup
          isVisible={popupVisible}
          title={popupTitle}
          message={popupMessage}
          button1={popupButton1}
          button2={popupButton2}
          button3={popupButton3}
          // buttonGroupStyle={popupButtonGroupStyle}
        />
        <RBSheet
          ref={bottomSheetRef}
          height={styles.bottomSheetHeaderHeight + styles.bottomSheetButtonHeight * 3 + 14}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            container: BOTTOM_SHEET_CONTAINER,
          }}
        >
          <StatusBar backgroundColor={color.dimmedStatusBar} />
          {renderBottomSheet()}
        </RBSheet>
      </Screen>
    )
  },
)

export default withNavigationFocus(MedicationSettingsScreen)
