import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content, ScrollView, Spinner } from "native-base"
import React, { Component } from "react"
import { Image, Text, TextStyle, View, ViewStyle } from "react-native"
import { WebView, WebViewMessageEvent } from "react-native-webview"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import {
  DetailsStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { color, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  paddingTop: 10,
}
const CONTENT_HEADER: ViewStyle = {
  backgroundColor: "#fff",
  marginBottom: 10,
  paddingBottom: 15,
  paddingTop: 20,
}
const ROW: ViewStyle = {
  flexDirection: "row",
}
const FLEX_ONE: ViewStyle = {
  flex: 1,
}
const NOTICE_TITLE: TextStyle = {
  fontFamily: typography.primary,
  color: "rgb(85, 85, 85)",
  lineHeight: 23,
  fontSize: 18,
  marginLeft: 16,
}
const NOTICE_DATE: TextStyle = {
  fontFamily: typography.primary,
  color: "rgb(153, 153, 153)",
  lineHeight: 18,
  fontSize: 15,
  marginLeft: 16,
  marginTop: 8,
}

const NOTICE_NEW_ICON: TextStyle = {
  fontFamily: typography.primary,
  color: color.primary,
  borderRadius: 8,
  height: 16,
  borderColor: color.primary,
  borderWidth: 1,
  lineHeight: 14,
  fontSize: 10,
  marginLeft: 6,
  marginTop: 8,
  justifyContent: "center",
  alignItems: "center",
  paddingLeft: 5,
  paddingRight: 3,
  paddingBottom: 2,
}

type NoticeDetailRouteProp = RouteProp<HomeStackParamList, "NoticeDetail">

type NoticeDetailNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "NoticeDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

/**
 * Props, 컴포넌트에서 직접 사용가능함
 */
export interface Props {
  accountStore: AccountStore
  navigation: NoticeDetailNavigationProp
  route: NoticeDetailRouteProp
}

/**
 * State, 컴포넌트에서 생성되며 setState로 변경가능
 */
interface State {
  isNew: boolean
  webViewHeight: number
}

@inject("accountStore")
@observer
export default class NoticeDetailScreen extends Component<Props, State> {
  static navigationOptions = {
    title: "",
  }

  constructor(props: Props) {
    super(props)

    this.state = {
      isNew: true,
      webViewHeight: 1000,
    }
  }

  UNSAFE_componentWillMount() {
    const noticeId = this.getId(this.props.route.params)
    this.props.accountStore.fetchNoticeDetail(noticeId)
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  getId(params: any): any {
    return params.id
  }

  setIsNew() {
    const diffDays = this.props.accountStore.noticeDetail.diffDays

    if (diffDays < 7) {
      this.setState({
        isNew: true,
      })
    } else {
      this.setState({
        isNew: false,
      })
    }
  }

  render() {
    const store = this.props.accountStore

    if (store.status == "pending") {
      return <Spinner color={color.primary} style={{ flex: 1, alignSelf: "center" }} />
    }

    return (
      <View style={{ backgroundColor: color.background }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER} scrollIndicatorInsets={{ right: 1 }}>
          <View style={CONTENT_HEADER}>
            <View style={[ROW, { paddingRight: 20 }]}>
              <Image
                source={require("../assets/images/iconNotice.png")}
                style={{ width: 26, height: 26, marginLeft: 17 }}
              />
              <View style={FLEX_ONE}>
                <Text numberOfLines={3} style={NOTICE_TITLE}>
                  {this.props.accountStore.noticeDetail.title}
                </Text>
              </View>
            </View>
            <View style={[ROW, { paddingLeft: 45 }]}>
              <Text style={NOTICE_DATE}>
                {store.noticeDetail.created.getFullYear() +
                  "년 " +
                  (store.noticeDetail.created.getMonth() + 1) +
                  "월 " +
                  store.noticeDetail.created.getDate() +
                  "일"}
              </Text>
              {this.state.isNew ? <Text style={NOTICE_NEW_ICON}>NEW</Text> : null}
            </View>
          </View>
          <WebView
            ref="webview"
            androidLayerType={"hardware"}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            source={{ html: this.props.accountStore.noticeDetail.content }}
            containerStyle={{ margin: 0, padding: 0 }}
            style={{ height: this.state.webViewHeight, backgroundColor: color.background }}
            onMessage={this.onMessage}
            injectedJavaScript="window.ReactNativeWebView.postMessage(document.body.scrollHeight)"
          />
        </ScrollView>
      </View>
    )
  }

  onMessage = (event: WebViewMessageEvent) => {
    this.setState({ webViewHeight: Number(event.nativeEvent.data) })
  }
}
