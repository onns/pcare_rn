import "@react-native-firebase/analytics"
import "@react-native-firebase/messaging"
import "@react-native-firebase/crashlytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import React, { Component } from "react"
import { WebView } from "react-native-webview"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import {
  AuthStackParamList,
  DetailsStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"

type TermsOfServiceRouteProp = RouteProp<AuthStackParamList, "TermsOfService">

type TermsOfServiceNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, "TermsOfService">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<DetailsStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  navigation: TermsOfServiceNavigationProp
  route: TermsOfServiceRouteProp
}

@inject("accountStore")
@observer
export default class TermsOfServiceScreen extends Component<Props> {
  static navigationOptions = {
    title: "",
    headerStyle: {
      borderBottomWidth: 0,
      elevation: 0,
      shadowColor: "transparent",
    },
  }

  constructor(props: Props) {
    super(props)
  }

  render() {
    return (
      <WebView
        ref="webview"
        androidLayerType={"hardware"}
        onMessage={(event) => {
          const message = event.nativeEvent.data
          /* event.nativeEvent.data must be string, i.e. window.postMessage
             should send only string.
          */
          this.props.accountStore.user.setAddress(message)
          this.props.navigation.goBack()
        }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        source={{ uri: "https://web.papricacare.com/agrees/term-condition-mobile.html" }}
      />
    )
  }

  componentDidMount() {
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: this.props.route.name })
  }

  onMessage(data: any) {
    console.tron.log(data)
  }
}
