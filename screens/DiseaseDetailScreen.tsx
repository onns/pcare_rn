import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { ScrollView, View } from "native-base"
import React, { Component } from "react"
import { Alert, Dimensions, Linking, Platform, TextStyle, ViewStyle } from "react-native"
import { TabBar, TabView } from "react-native-tab-view"
import { WebView } from "react-native-webview"

import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { Text } from "../components/StyledText"
import {
  AuthStackParamList,
  MainTabParamList,
  PrescriptionsStackParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { PrescriptionStore } from "../stores/PrescriptionStore"
import { color, typography } from "../theme"

const CONTENT_CONTAINER: ViewStyle = {
  // flex: 1,
}
const DISEASE_TITLE: TextStyle = {
  fontSize: 20,
  fontWeight: "bold",
  color: color.text,
  paddingBottom: 6,
}

type DiseaseDetailRouteProp = RouteProp<PrescriptionsStackParamList, "DiseaseDetail">

type NavigationProp = CompositeNavigationProp<
  StackNavigationProp<PrescriptionsStackParamList, "DiseaseDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AuthStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: NavigationProp
  route: DiseaseDetailRouteProp
}

interface State {
  diseaseIdx: number
  diseaseDetailIdx: number
  index: number
  routes: Array<any>
  data: any
  contentHeight: number
}

@inject("accountStore", "prescriptionStore")
@observer
export default class DiseaseDetailScreen extends Component<Props, State> {
  static navigationOptions = {
    title: "관련 질병",
    headerStyle: {
      height: Platform.OS === "ios" ? 76 : undefined,
      borderBottomWidth: 0,
      elevation: 0,
      shadowColor: "transparent",
    },
  }

  diseaseDetailId: number
  diseaseIdx: number
  diseaseDetailIdx: number
  isSubDisease = false

  constructor(props: Props) {
    super(props)
    const { route } = this.props
    this.state = {
      index: 0,
      diseaseIdx: this.getDiseaseIdx(route.params),
      diseaseDetailIdx: this.getDiseaseDetailIdx(route.params),
      routes: [{ key: "", title: "" }],
      data: {
        rep_title: "",
        title1: "",
        desc1: "",
        title2: "",
        desc2: "",
      },
      contentHeight: -1,
    }

    this.diseaseDetailId = this.getDiseaseDetailId(route.params)
    this.diseaseIdx = this.getDiseaseIdx(route.params)
    this.diseaseDetailIdx = this.getDiseaseDetailIdx(route.params)
    this.isSubDisease = this.getIsSubDisease(route.params)
  }

  UNSAFE_componentWillMount() {
    this.fetchDiseaseDetail(this.diseaseDetailId, this.isSubDisease)
  }

  componentDidMount() {
    const { route } = this.props
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    firebase.analytics().logEvent("disease_detail_screen")
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })
  }

  getDiseaseDetailId(params: any): any {
    return params.id
  }

  getDiseaseIdx(params: any): any {
    return params.diseaseIdx != undefined ? params.diseaseIdx : -1
  }

  getDiseaseDetailIdx(params: any): any {
    return params.diseaseDetailIdx
  }

  getIsSubDisease(params: any): any {
    return params.isSubDisease
  }

  render() {
    // let jsCode = `!function(){var e=function(e,n,t){if(n=n.replace(/^on/g,""),"addEventListener"in window)e.addEventListener(n,t,!1);else if("attachEvent"in window)e.attachEvent("on"+n,t);else{var o=e["on"+n];e["on"+n]=o?function(e){o(e),t(e)}:t}return e},n=document.querySelectorAll("a[href]");if(n)for(var t in n)n.hasOwnProperty(t)&&e(n[t],"onclick",function(e){new RegExp( ‘^https?:\/\/’, ‘gi’ )..test(this.href)||(e.preventDefault(),window.postMessage(JSON.stringify({external_url_open:this.href})))})}();`

    return (
      // <View style={{ backgroundColor: color.background }}>
      <ScrollView
        contentContainerStyle={{
          ...CONTENT_CONTAINER,
          height: this.state.contentHeight !== -1 ? this.state.contentHeight : 2000,
        }}
        showsVerticalScrollIndicator={Platform.OS === "android"}
      >
        <View
          style={{
            backgroundColor: "#fff",
            paddingTop: 8,
            paddingLeft: 20,
            paddingBottom: 19,
          }}
        >
          <Text style={DISEASE_TITLE}>{this.state.data.rep_title}</Text>
        </View>

        <TabView
          navigationState={this.state}
          renderScene={({ route }) => {
            let desc: string = this.state.data.desc1
            switch (route.key) {
              case this.state.data.title1:
                desc = this.state.data.desc1
                break
              case this.state.data.title2:
                desc = this.state.data.desc2
                break
              case this.state.data.title3:
                desc = this.state.data.desc3
                break
              case this.state.data.title4:
                desc = this.state.data.desc4
                break
              case this.state.data.title5:
                desc = this.state.data.desc5
                break
              case this.state.data.title6:
                desc = this.state.data.desc6
                break
              case this.state.data.title7:
                desc = this.state.data.desc7
                break
              case this.state.data.title8:
                desc = this.state.data.desc8
                break
              case this.state.data.title9:
                desc = this.state.data.desc9
                break
              case this.state.data.title10:
                desc = this.state.data.desc10
                break
              default:
                desc = this.state.data.desc1
                break
            }

            return (
              <WebView
                ref="webview"
                style={{ flex: 1 }}
                // javaScriptEnabled={true}
                // onMessage={this.onMessage.bind(this)}
                // injectedJavaScript={jsCode}
                // domStorageEnabled={true}
                source={{
                  html:
                    `
                  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"} />
                  <style>
                      div {
                        padding: 10;
                        font-size: 17;
                        color: rgb(85, 85, 85);
                        line-height: 19pt;
                        list-style: none;
                      }

                      h2 {
                        font-size: 19;
                        font-weight: 'bold';
                        color: rgb(85, 85, 85);
                      }

                      h3 {
                        font-size: 18;
                        font-weight: 'bold';
                        color: rgb(85, 85, 85);
                      }

                      h4 {
                        font-size: 17;
                        font-weight: 'bold';
                        color: rgb(255, 132, 26);
                      }

                      h5 {
                        font-size: 16;
                        font-weight: 'bold';
                        color: rgb(85, 85, 85);
                      }

                      p, ul, li {
                        font-size: 17;
                        color: rgb(85, 85, 85);
                        line-height: 19pt;
                        list-style: none;
                      }

                      a {
                        color: rgb(242, 74, 16);
                      }

                      img {
                        width: 100%;
                      }

                      </style>` + desc.replace(/<(\/[ab]|[ab])([^>]*)>/gi, ""),
                }}
                androidLayerType={"hardware"}
                scalesPageToFit={false}
                originWhitelist={["*"]}
                onLayout={(event) => {
                  const windowHeight = Dimensions.get("window").height
                  const layoutHeight = event.nativeEvent.layout.height
                  if (
                    this.state.contentHeight === -1 &&
                    windowHeight < layoutHeight &&
                    layoutHeight + 500 > 2000
                  ) {
                    this.setState({ contentHeight: event.nativeEvent.layout.height + 500 })
                  }
                }}
              />
            )
          }}
          renderTabBar={this.renderTabBar}
          onIndexChange={(index: number) => this.setState({ index })}
          initialLayout={{ height: 0, width: Dimensions.get("window").width }}
        />
      </ScrollView>
      // </View>
    )
  }

  onMessage(e: any) {
    // retrieve event data
    let data = e.nativeEvent.data
    // maybe parse stringified JSON
    try {
      data = JSON.parse(data)
    } catch (e) {}

    // check if this message concerns us
    if (typeof data === "object" && data.external_url_open) {
      // proceed with URL open request
      return Alert.alert(
        "External URL",
        "Do you want to open this URL in your browser?",
        [
          { text: "Cancel", style: "cancel" },
          { text: "확인", onPress: () => Linking.openURL(data.external_url_open) },
        ],
        { cancelable: false },
      )
    }
  }

  async fetchDiseaseDetail(id: number, isSubDisease: boolean) {
    const { disease, prescriptionDetail } = this.props.prescriptionStore
    let diseaseDetail
    if (this.diseaseIdx != -1) {
      if (isSubDisease) {
        diseaseDetail = prescriptionDetail!.subdiseases[this.diseaseIdx].diseaseDetail[
          this.diseaseDetailIdx
        ]
      } else {
        diseaseDetail = prescriptionDetail!.disease[this.diseaseIdx].diseaseDetail[
          this.diseaseDetailIdx
        ]
      }
    } else {
      diseaseDetail = disease.diseaseDetail[this.diseaseDetailIdx]
    }

    const routes = []

    if (diseaseDetail.title1 != null && diseaseDetail.title1.length > 0) {
      routes.push({ key: diseaseDetail.title1, title: diseaseDetail.title1 })
    }
    if (diseaseDetail.title2 != null && diseaseDetail.title2.length > 0) {
      routes.push({ key: diseaseDetail.title2, title: diseaseDetail.title2 })
    }
    if (diseaseDetail.title3 != null && diseaseDetail.title3.length > 0) {
      routes.push({ key: diseaseDetail.title3, title: diseaseDetail.title3 })
    }
    if (diseaseDetail.title4 != null && diseaseDetail.title4.length > 0) {
      routes.push({ key: diseaseDetail.title4, title: diseaseDetail.title4 })
    }
    if (diseaseDetail.title5 != null && diseaseDetail.title5.length > 0) {
      routes.push({ key: diseaseDetail.title5, title: diseaseDetail.title5 })
    }
    if (diseaseDetail.title6 != null && diseaseDetail.title6.length > 0) {
      routes.push({ key: diseaseDetail.title6, title: diseaseDetail.title6 })
    }
    if (diseaseDetail.title7 != null && diseaseDetail.title7.length > 0) {
      routes.push({ key: diseaseDetail.title7, title: diseaseDetail.title7 })
    }
    if (diseaseDetail.title8 != null && diseaseDetail.title8.length > 0) {
      routes.push({ key: diseaseDetail.title8, title: diseaseDetail.title8 })
    }
    if (diseaseDetail.title9 != null && diseaseDetail.title9.length > 0) {
      routes.push({ key: diseaseDetail.title9, title: diseaseDetail.title9 })
    }
    if (diseaseDetail.title10 != null && diseaseDetail.title10.length > 0) {
      routes.push({ key: diseaseDetail.title10, title: diseaseDetail.title10 })
    }

    if (routes.length == 0) {
      // https://app.asana.com/0/880342869624505/1111904600543924
      routes.push({ key: "설명", title: "설명" })
    }

    this.setState({
      routes: routes,
      data: diseaseDetail,
    })
  }

  renderTabBar = (props: any) => (
    <TabBar
      {...props}
      indicatorStyle={{
        borderBottomWidth: 2,
        borderBottomColor: color.primary,
      }}
      style={{
        height: 40,
        backgroundColor: "#fff",
        borderColor: color.palette.pale,
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        padding: 0,
        elevation: 0,
      }}
      dynamicWidth
      scrollEnabled
      tabStyle={{ width: "auto", height: 40, paddingVertical: 0, paddingHorizontal: 14 }}
      labelStyle={{
        fontFamily: typography.primary,
        fontSize: 15,
      }}
      activeColor={color.primary}
      inactiveColor="rgb(153, 153, 153)"
    />
  )
}
