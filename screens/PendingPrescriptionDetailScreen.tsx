import "@react-native-firebase/analytics"

import amplitude from "amplitude-js"
import { inject, observer } from "mobx-react"
import { Container, Content, ScrollView, Toast, View } from "native-base"
import React, { Component } from "react"
import {
  Alert,
  BackHandler,
  Dimensions,
  Image,
  ImageStyle,
  Text,
  TextStyle,
  ViewStyle,
} from "react-native"

import { api } from "@api/api"
import firebase from "@react-native-firebase/app"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { BackgroundImage, FastImage } from "../components/BackgroundImage"
import CloseButton from "../components/CloseButton"
import { Button } from "../components/StyledButton"
import {
  AddPrescriptionStackParamList,
  HomeStackParamList,
  MainTabParamList,
  RootStackParamList,
} from "../navigation/types"
import { AccountStore } from "../stores/AccountStore"
import { Prescription, PrescriptionStore } from "../stores/PrescriptionStore"
import { color, styles, typography } from "../theme"

const deviceWidth = Dimensions.get("window").width
const deviceHeight = Dimensions.get("window").height

const PHOTO_FRAME: ViewStyle = {
  flexDirection: "column",
  height: deviceHeight > deviceWidth ? deviceWidth * 1.39 : 355,
}
const BACKGROUND_IMAGE: ViewStyle = {
  height: deviceHeight > deviceWidth ? deviceWidth * 1.414 : 355,
  width: deviceWidth,
}
const CONTENT_CONTAINER: ViewStyle = {
  paddingTop: 16,
}
const BOTTOM_CONTENT: ViewStyle = {
  marginTop: 24,
  marginHorizontal: 16,
}
const RETURN_REASON_TITLE: TextStyle = {
  ...typography.title2,
  marginBottom: 8,
}
const RETURN_REASON_DESCRIPTION: TextStyle = {
  ...typography.body,
}
const BUTTONS: ViewStyle = {
  flex: 1,
  justifyContent: "flex-end",
  paddingHorizontal: 16,
  paddingTop: 20,
  paddingBottom: 16,
}
const BUTTON_TEXT: TextStyle = {
  ...typography.title3,
  color: "#fff",
}
const WARNING_MARK: ImageStyle = { width: 40.3, height: 41, marginLeft: 5 }

type PendingPrescriptionDetailRouteProp = RouteProp<HomeStackParamList, "PendingPrescriptionDetail">

type PendingPrescriptionDetailNavigationProp = CompositeNavigationProp<
  StackNavigationProp<HomeStackParamList, "PendingPrescriptionDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    CompositeNavigationProp<
      StackNavigationProp<AddPrescriptionStackParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >
>

export interface Props {
  accountStore: AccountStore
  prescriptionStore: PrescriptionStore
  navigation: PendingPrescriptionDetailNavigationProp
  route: PendingPrescriptionDetailRouteProp
}

interface State {
  prescription: Prescription
  index: any
}

function getPrevRouteName(params: any): any {
  return params.prevRouteName
}

@inject("accountStore", "prescriptionStore")
@observer
export default class PendingPrescriptionDetailScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation, route }) => {
    const { params } = route

    return {
      title: "처리 중인 처방전",
      headerTitleStyle: styles.HEADER_TITLE,
      headerLeft: () => (
        <CloseButton
          imageStyle={{ tintColor: color.palette.gray3, marginLeft: 16 }}
          onPress={() => {
            navigation.pop()
            const prevRouteName = getPrevRouteName(params)
            if (prevRouteName != "Home") {
              navigation.navigate(prevRouteName, {
                myPreRouteName: prevRouteName,
              })
            }
          }}
        />
      ),
    }
  }

  prescription: Prescription

  constructor(props: Props) {
    super(props)
    this.state = {
      prescription: (this.prescription = this.getPrescription(this.props.route.params)),
      index: this.getIndex(this.props.route.params),
    }
  }

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress)
  }

  componentDidMount() {
    const { route } = this.props
    firebase.analytics().logScreenView({
      screen_class: this.props.route.name,
      screen_name: this.props.route.name,
    })
    amplitude.getInstance().logEvent("screen_view", { screen: route.name })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress)
  }

  handleBackPress = () => {
    const prevRouteName = getPrevRouteName(this.props.route.params)
    this.props.navigation.pop()
    this.props.navigation.navigate(prevRouteName, {
      myPreRouteName: prevRouteName,
    })
    return true
  }

  getPrescription(params: any): any {
    return params.item
  }

  getIndex(params: any): any {
    return params.index
  }

  render() {
    const { navigation, route } = this.props
    const { params } = route
    return (
      <View style={{ backgroundColor: color.background }}>
        <ScrollView contentContainerStyle={CONTENT_CONTAINER}>
          <View style={PHOTO_FRAME}>
            <BackgroundImage
              resizeMode={FastImage.resizeMode.contain}
              image={BACKGROUND_IMAGE}
              source={{ uri: this.prescription.image }}
            />
            {this.prescription.status == "RJ" ? (
              <View>
                <Image source={require("../assets/images/warning_mark.png")} style={WARNING_MARK} />
              </View>
            ) : null}
          </View>
          {this.prescription.status == "RJ" ? (
            <View style={BOTTOM_CONTENT}>
              <Text style={RETURN_REASON_TITLE}>반려사유:</Text>
              <Text style={RETURN_REASON_DESCRIPTION}>{this.prescription.returnReason}</Text>
            </View>
          ) : this.prescription.status == "IP" || this.prescription.status == "UL" ? (
            <View style={BOTTOM_CONTENT}>
              <Text style={RETURN_REASON_TITLE}>분석 내용을 검토 중입니다</Text>
              <Text style={RETURN_REASON_DESCRIPTION}>
                처방전 분석 알고리즘이 보다 고도화 되기까지 운영진이 분석 내용을 확인하고 승인하는
                절차로 인하여 다소 시간이 소요될 수 있습니다. 기다려 주셔서 감사합니다.
              </Text>
            </View>
          ) : null}
          {this.prescription.status == "RJ" ? (
            <View style={BUTTONS}>
              <Button block rounded style={{ height: 48, marginBottom: 10 }} onPress={this.photo}>
                <Text style={BUTTON_TEXT}>재촬영하기</Text>
              </Button>
              <Button dark block rounded style={{ height: 48 }} onPress={this.remove}>
                <Text style={BUTTON_TEXT}>삭제</Text>
              </Button>
            </View>
          ) : this.prescription.status == "IP" || this.prescription.status == "UL" ? (
            <View style={BUTTONS}>
              <Button
                block
                style={{ height: 48 }}
                onPress={() => {
                  navigation.pop()
                  const prevRouteName = getPrevRouteName(params)
                  if (prevRouteName != "Home") {
                    navigation.navigate(prevRouteName, {
                      myPreRouteName: prevRouteName,
                    })
                  }
                }}
              >
                <Text style={BUTTON_TEXT}>확인</Text>
              </Button>
            </View>
          ) : null}
        </ScrollView>
      </View>
    )
  }

  remove = () => {
    const navigation = this.props.navigation
    const { params } = this.props.route

    Alert.alert(
      "",
      "삭제하시겠습니까?",
      [
        {
          text: "취소",
          onPress: () => null,
          style: "cancel",
        },
        {
          text: "삭제",
          onPress: async () => {
            const result = await api.deletePrescription(this.prescription.id)
            if (result.kind === "ok") {
              this.props.prescriptionStore.setStatus("pending")
              this.props.prescriptionStore.removePrescription(
                this.prescription,
                this.props.prescriptionStore.selectedFamilyMemberKey,
              )
              this.props.prescriptionStore.setStatus("done")
              Toast.show({
                title: <Text style={styles.TOAST_TEXT}>{`처방전을 삭제하였습니다.`}</Text>,
                duration: 2000,
                placement: "top",
                style: styles.TOAST_VIEW,
              })
              setTimeout(() => {
                navigation.pop()
                const prevRouteName = getPrevRouteName(params)
                navigation.navigate(prevRouteName, {
                  myPreRouteName: prevRouteName,
                })
              }, 2000)
            } else if (result.kind === "unauthorized") {
              Toast.show({
                title: (
                  <Text
                    style={styles.TOAST_TEXT}
                  >{`세션이 만료되었습니다. \n다시 로그인 해주세요.`}</Text>
                ),
                duration: 2000,
                placement: "top",
                style: styles.TOAST_VIEW,
              })
              setTimeout(() => {
                this.props.navigation.navigate("Auth")
              }, 2000)
            } else {
              Toast.show({
                title: (
                  <Text
                    style={styles.TOAST_TEXT}
                  >{`처방전 삭제에 실패하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
                ),
                duration: 3000,
                placement: "top",
                style: styles.TOAST_VIEW,
              })
            }
          },
        },
      ],
      { cancelable: false },
    )
  }

  photo = () => {
    this.props.navigation.pop()
    // @ts-ignore
    this.props.navigation.navigate("PrescriptionScanner", {
      rePhotoIndex: this.state.index,
      rePhotoId: this.prescription.id,
    })
  }
}
