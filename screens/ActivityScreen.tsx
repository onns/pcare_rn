import { observer } from "mobx-react-lite"
import React, { useEffect, useLayoutEffect, useState } from "react"
import { Platform, StatusBar, TextStyle, View, ViewStyle } from "react-native"
import { FlatList, ScrollView } from "react-native-gesture-handler"
import OrientationLocker from "react-native-orientation-locker"
import Video from "react-native-video"

import { api } from "@api/api"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { CompositeNavigationProp, RouteProp, useNavigation } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import CloseButton from "../components/CloseButton"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { RecordVideo } from "../stores/record/record-video"
import { color, typography } from "../theme"

type ActivityScreenRouteProp = RouteProp<DetailsStackParamList, "Activity">

type ActivityScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "Activity">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>
export interface ActivityScreenProps {
  navigation: ActivityScreenNavigationProp
  route: ActivityScreenRouteProp
  recordVideo: RecordVideo
}

const ROOT: ViewStyle = {
  backgroundColor: color.background,
  marginHorizontal: 24,
}
const TITLE: TextStyle = { ...typography.title3 }
const SUBTITLE: TextStyle = { ...typography.sub2, opacity: 0.5 }
const MARGIN_VERTICAL: ViewStyle = {
  marginVertical: Platform.OS === "android" ? 8 : 0,
}
const LAST_ITEM_BOTTOM: ViewStyle = { marginBottom: 100 }
const VIDEO_VIEW: ViewStyle = {
  width: "100%",
  height: 180,
  marginVertical: 16,
  borderRadius: 8,
}

export const ActivityScreen: React.FunctionComponent<ActivityScreenProps> = observer((props) => {
  const { params } = props.route
  const { takingDrugStore, recordStore } = useStores()
  const { recordVideo } = recordStore
  const navigation = useNavigation()
  const [playedVideo1, setPlayedVideo1] = useState(false)
  const [playedVideo2, setPlayedVideo2] = useState(false)
  const [playedVideo3, setPlayedVideo3] = useState(false)
  const [videoData, setVideoData] = useState(false)
  // staging 에서는 데이터가 뒤집혀서 보내지며, title에 숫자를 참조하여 sort함.
  const sortVideo = recordVideo.slice().sort((a, b) => {
    if (parseInt(a.title) < parseInt(b.title)) {
      return -1
    }
  })

  const recordActivity = async (recordId) => {
    const response = await api.postSurvey(recordId)
    if (response.kind !== "ok") {
      console.tron.log(`< ActivityScreen > post error`)
    }
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => {
        return <CloseButton onPress={navigation.goBack} style={{ marginLeft: 24 }} />
      },
      headerTitle: params.title,
      headerStyle: { borderBottomWidth: 1, elevation: 0, shadowColor: "transparent" },
    })
  }, [navigation])

  useEffect(() => {
    async function checkVideoData(recordId) {
      if (recordVideo.length === 0) {
        await recordStore.fetchVideo(recordId)
        if (recordStore.status === "done" && recordStore.recordVideo) {
          setVideoData(true)
        }
      } else {
        setVideoData(true)
      }
    }
    checkVideoData(params.recordId)

    if (playedVideo1 || playedVideo2 || playedVideo3) {
      recordActivity(params.recordId)
    }
    // 세로, 가로모드 모두 가능하도록 설정
    OrientationLocker.unlockAllOrientations()
    return () => {
      // unmount될 때 세로모드 고정
      OrientationLocker.lockToPortrait()
    }
  }, [playedVideo1, playedVideo2, playedVideo3])

  const renderItem = ({ item, index }: { item: RecordVideo; index: number }) => {
    const videoTime = (e) => {
      // seekableDuration = 동영상 전체 시간 , currentTime = 현재재생시간
      const checkPlayed = Math.floor(e.seekableDuration % 1) === Math.floor(e.currentTime)
      switch (index) {
        case 0:
          checkPlayed && setPlayedVideo1(true)
          break
        case 1:
          checkPlayed && setPlayedVideo2(true)
          break
        case 2:
          checkPlayed && setPlayedVideo3(true)
          break
      }
    }
    return (
      <View style={{ marginTop: 8 }}>
        <Text style={TITLE}>{item.title}</Text>
        {item.sub_title ? <Text style={SUBTITLE}>{item.sub_title}</Text> : null}
        <Video
          controls={true}
          paused={true}
          source={{
            uri: item.source,
          }}
          ignoreSilentSwitch={"ignore"}
          style={VIDEO_VIEW}
          onProgress={(e) => videoTime(e)}
        />
      </View>
    )
  }

  return (
    // ScrollView가 있으면 props에 'preset'을 넣어줄것
    videoData && (
      <Screen preset="fixed">
        <StatusBar backgroundColor="#f9f9f9" barStyle="dark-content" />
        <ScrollView style={ROOT} bounces={false} showsVerticalScrollIndicator={false}>
          <FlatList
            data={sortVideo}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            style={MARGIN_VERTICAL}
            contentContainerStyle={LAST_ITEM_BOTTOM}
          />
        </ScrollView>
      </Screen>
    )
  )
})
