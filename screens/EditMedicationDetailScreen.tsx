import amplitude from "amplitude-js"
import debounce from "lodash.debounce"
import { DateTime } from "luxon"
import { observer } from "mobx-react-lite"
import { clone } from "mobx-state-tree"
import { Input, Toast } from "native-base"
import React, { useEffect, useLayoutEffect, useRef, useState } from "react"
import { Dimensions, Platform, TextStyle, View, ViewStyle } from "react-native"
import { Calendar } from "react-native-calendars"
import FastImage from "react-native-fast-image"
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import Carousel, { Pagination } from "react-native-snap-carousel"

import { api } from "@api/api"
import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs"
import { withNavigationFocus } from "@react-navigation/compat"
import { CompositeNavigationProp, RouteProp, useFocusEffect } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { useStores } from "../App"
import SvgArrowDownBlack from "../assets/images/arrowDownBlack.svg"
import SvgDrugEmptyImage from "../assets/images/drugEmptyImage.svg"
import SvgInfoDark from "../assets/images/infoDark.svg"
import TouchableItem from "../components/button/TouchableItem"
import CustomHeaderBackImage from "../components/CustomHeaderBackImage"
import { getPopupButton, Popup } from "../components/Popup"
import { Screen } from "../components/screen/screen"
import { Text } from "../components/StyledText"
import { getTimeText } from "../components/TimeSlotRow"
import { sendLogScreenView } from "../lib/analytics"
import { formatTime } from "../lib/DateUtil"
import { DetailsStackParamList, MainTabParamList, RootStackParamList } from "../navigation/types"
import { env } from "../stores"
import { PrescriptionDetailModel } from "../stores/PrescriptionDetail"
import { PrescriptionDetail } from "../stores/PrescriptionStore"
import { TimeSlot, TimeSlotName } from "../stores/TimeSlot"
import { color, styles, typography } from "../theme"
import { textDayStyle, todayBackgroundColor } from "../theme/calendar"

type EditMedicationDetailScreenRouteProp = RouteProp<DetailsStackParamList, "EditMedicationDetail">

type EditMedicationDetailScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<DetailsStackParamList, "EditMedicationDetail">,
  CompositeNavigationProp<
    BottomTabNavigationProp<MainTabParamList>,
    StackNavigationProp<RootStackParamList>
  >
>

export interface EditMedicationDetailScreenProps {
  navigation: EditMedicationDetailScreenNavigationProp
  route: EditMedicationDetailScreenRouteProp
}

const ROOT: ViewStyle = {
  flex: 1,
  backgroundColor: color.palette.white,
}
const DRUG_INFO: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingVertical: 24,
  paddingHorizontal: 16,
  marginBottom: 4,
}
const DRUG_IMAGE: ViewStyle = {
  width: 42,
  height: 24,
  borderRadius: 3,
}
const DRUG_NAME = { flex: 1, ...typography.title3, marginLeft: 8 }
const BUTTONS_CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
  flexDirection: "row",
  alignItems: "flex-end",
  paddingHorizontal: 8,
}
const NEXT_BUTTON_TEXT = { ...typography.title3, color: "#fff" }
const BACK_BUTTON_TEXT = { ...typography.title3, color: color.palette.grey50 }
const DIVIDER = {
  height: 8,
  backgroundColor: color.palette.gray5,
  borderBottomColor: color.palette.veryLightPink3,
  borderBottomWidth: 1,
  borderTopColor: color.palette.veryLightPink3,
  borderTopWidth: 1,
}
const { width: viewportWidth } = Dimensions.get("window")

const BUTTONS: ViewStyle = {
  flex: 1,
  minHeight: 48,
  borderRadius: 25,
  borderWidth: 1,
  justifyContent: "center",
  alignItems: "center",
  marginHorizontal: 8,
}
const SELECT_BUTTON: ViewStyle = {
  ...BUTTONS,
  borderColor: color.palette.red50,
}
const SELECT_BUTTON_SELECTED: ViewStyle = {
  ...BUTTONS,
  borderColor: color.palette.red10,
  backgroundColor: color.palette.red10,
}
const SELECT_BUTTON_TEXT = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.red70,
}
const SELECT_BUTTON_TEXT_SELECTED = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.primary,
}

enum MedicationCycle {
  EveryDay,
  SpecificDaysInterval,
  PRN,
}
enum StartDate {
  Today,
  SpecificDate,
}
enum EndDate {
  None,
  DosingDays,
  SpecificDate,
}

const BACK_BUTTON: ViewStyle = {
  flex: 1,
  height: 56,
  borderRadius: 25,
  marginHorizontal: 4,
  backgroundColor: color.palette.veryLightPink3,
  alignItems: "center",
  justifyContent: "center",
}
const NEXT_BUTTON: ViewStyle = {
  flex: 1,
  height: 56,
  borderRadius: 25,
  marginHorizontal: 4,
  backgroundColor: color.primary,
  alignItems: "center",
  justifyContent: "center",
}
const STEP_TITLE = { flex: 1, fontFamily: typography.bold, fontSize: 24, color: color.text }
const STEP_QUESTION = {
  ...typography.body,
  color: color.palette.grey70,
  paddingLeft: 8,
  marginBottom: 32,
}
const STEP_TOP: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  marginBottom: 8,
  paddingLeft: 16,
  paddingTop: 24,
}
const INPUT: TextStyle = {
  minHeight: 48,
  borderRadius: 45,
  fontFamily: typography.bold,
  fontSize: 16,
  color: color.text,
  borderWidth: 1,
  borderColor: color.palette.grey30,
  textAlign: "center",
  marginHorizontal: 8,
}
const CALENDAR_THEME = {
  "stylesheet.calendar.header": {
    dayHeader: {
      marginTop: 2,
      marginBottom: 7,
      width: 32,
      textAlign: "center",
      fontSize: 13,
      fontFamily: typography.primary,
      color: color.text,
    },
    monthText: {
      fontSize: 20,
      fontFamily: typography.primary,
      color: color.text,
      margin: 10,
    },
  },
  "stylesheet.day.basic": {
    base: {
      width: 42,
      height: 42,
      justifyContent: "center",
      alignItems: "center",
    },
    text: {
      fontSize: 16,
      fontFamily: typography.primary,
      fontWeight: "normal",
      color: color.text,
      backgroundColor: "rgba(255, 255, 255, 0)",
      ...textDayStyle,
    },
    alignedText: {},
    selected: {
      backgroundColor: color.palette.red10,
      borderRadius: 21,
    },
    today: {
      backgroundColor: todayBackgroundColor,
      borderRadius: 21,
    },
    selectedText: {
      fontFamily: typography.bold,
      fontWeight: "bold",
      color: color.palette.orange,
    },
  },
  // textDayFontFamily: typography.primary,
  // textDayFontSize: 16,
  todayTextColor: color.palette.orange,
  // selectedDayBackgroundColor: color.palette.red10,
  // selectedDayTextColor: color.palette.orange,
}
const ROW_RIGHT = { flex: 1, marginHorizontal: 8 }
const INPUT_ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  marginBottom: 16,
}
const ROW: ViewStyle = { flexDirection: "row" }
const SELECT_BUTTONS_ROW: ViewStyle = { flexWrap: "wrap", flexDirection: "row", marginBottom: 16 }
const SAVE_TEXT = { fontFamily: typography.primary, fontSize: 17, color: color.primary }

const START_END_DATE_TEXT = {
  fontFamily: typography.primary,
  fontSize: 20,
  lineHeight: 30,
  color: color.palette.realBlack,
}
const START_END_DATE_SUBJECT = { ...typography.body, color: color.palette.grey50, marginBottom: 8 }
const START_END_DATE_BOX = { marginTop: 16, paddingHorizontal: 8, paddingBottom: 32 }
const INFO_BOX: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingTop: 6,
  paddingBottom: 7,
  paddingHorizontal: 9,
  marginHorizontal: 8,
  marginTop: -16,
  marginBottom: 32,
  backgroundColor: color.palette.softBloom,
  borderRadius: 1,
}

const INFO_TEXT = { ...typography.sub2, color: color.palette.black, marginLeft: 4 }
const SAVE_POPUP_TEXT = {
  marginVertical: 16,
  fontFamily: typography.primary,
  fontSize: 14,
  lineHeight: 21,
  color: color.palette.gray1,
  textAlign: "center",
}
const SAVE_POPUP_SUB_TEXT = {
  fontFamily: typography.primary,
  fontSize: 14,
  lineHeight: 21,
  color: color.palette.grey50,
  textAlign: "center",
}
const SAVE_POPUP_BOLD_TEXT = { ...SAVE_POPUP_TEXT, fontFamily: typography.bold }
const HEADER_LEFT = { marginLeft: Platform.OS === "android" ? 10 : 0 }

export enum EditMedicationDetailMode {
  /** 복약 내용 수정 모드 */
  EditPrescriptionDetail,
  /** 새로운 복약 내용을 추가하는 모드 */
  NewPrescriptionDetail,
  /** 처방전 찍기(+) 버튼 > 직접 입력 선택하여 새로운 처방전을 사용자가 입력하는 모드 */
  NewPrescription,
}

/**
 * prescriptionDetailIdx 값을 전달받지 않은 경우, 새로운 약을 추가하는 것으로 처리
 */
const EditMedicationDetailScreen: React.FunctionComponent<EditMedicationDetailScreenProps> = observer(
  (props) => {
    const { navigation, route } = props
    const { prescriptionDetailIdx, drug, mode } = route.params
    const { accountStore, prescriptionStore } = useStores()
    const isNewPrescription = mode === EditMedicationDetailMode.NewPrescription

    // const addMode = prescriptionDetailIdx == undefined ? true : false // 약 추가 모드 or 수정 모드

    const [prescriptionDetail, setPrescriptionDetail] = useState<PrescriptionDetail>(
      mode == EditMedicationDetailMode.NewPrescriptionDetail ||
        mode == EditMedicationDetailMode.NewPrescription
        ? PrescriptionDetailModel.create(
            {
              id: -1,
              drug: clone(drug, false),
              dose: 1,
              unit: "정",
              doses_num: 1,
              per_days: 1,
              dosing_days_num: 1,
              start_at: DateTime.local().set({
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0,
              }),
              end_at: DateTime.local().set({
                hour: 23,
                minute: 59,
                second: 59,
                millisecond: 0,
              }),
              timeslots: [],
              prescription:
                mode == EditMedicationDetailMode.NewPrescription
                  ? -1
                  : prescriptionStore.editingPrescription.id,
            },
            env,
          )
        : prescriptionStore.editingPrescription.prescription_details[prescriptionDetailIdx],
    )

    const carouselRef = useRef<Carousel>(null)
    const [selectedCycle, setSelectedCycle] = useState(
      prescriptionDetail.per_days == 1
        ? MedicationCycle.EveryDay
        : MedicationCycle.SpecificDaysInterval,
    )
    const [perDays, setPerDays] = useState<string>(
      prescriptionDetail.per_days > 1 ? String(prescriptionDetail.per_days) : undefined,
    )

    const [selectedStartDate, setSelectedStartDate] = useState(
      Math.floor(prescriptionDetail.start_at.diffNow("days").days) == 0
        ? StartDate.Today
        : StartDate.SpecificDate,
    )
    const [selectedEndDate, setSelectedEndDate] = useState(
      prescriptionDetail.end_at ? EndDate.SpecificDate : EndDate.None,
    )

    // 시작일/종료일 달력에서 이전/다음 달 이동을 위한 state
    const [startDate, setStartDate] = useState(prescriptionDetail.start_at.toFormat("yyyy-MM-dd"))
    const [endDate, setEndDate] = useState(prescriptionDetail.end_at.toFormat("yyyy-MM-dd"))

    // const [selectedTime, setSelectedTime] = useState(prescriptionDetail.timeslots)
    const [dosingDays, setDosingDays] = useState<string>(String(prescriptionDetail.dosing_days_num))
    const [dose, setDose] = useState<string>(String(prescriptionDetail.dose))
    const [stepIndex, setStepIndex] = useState(0)
    const [inputFocused, setInputFocused] = useState(false)

    const [selectedWakeUp, setSelectedWakeUp] = useState(
      prescriptionDetail.includesTimeSlot(TimeSlotName.WakeUp),
    )
    const [selectedMorning, setSelectedMorning] = useState(
      prescriptionDetail.includesTimeSlot(TimeSlotName.Morning),
    )
    const [selectedAfternoon, setSelectedAfternoon] = useState(
      prescriptionDetail.includesTimeSlot(TimeSlotName.Afternoon),
    )
    const [selectedEvening, setSelectedEvening] = useState(
      prescriptionDetail.includesTimeSlot(TimeSlotName.Evening),
    )
    const [selectedNight, setSelectedNight] = useState(
      prescriptionDetail.includesTimeSlot(TimeSlotName.Night),
    )
    const [selectedOnce, setSelectedOnce] = useState(
      prescriptionDetail.includesTimeSlot(TimeSlotName.Once),
    )
    const [selectedCreatedSlots, setSelectedCreatedSlots] = useState(
      prescriptionDetail.selectedUserCreatedTimeSlots,
    )
    const [popupVisible, setPopupVisible] = useState(false)
    const [popupTitle, setPopupTitle] = useState("")
    const [popupMessage, setPopupMessage] = useState<any>("")
    const [popupButton1, setPopupButton1] = useState(<View />)
    const [popupButton2, setPopupButton2] = useState(<View />)
    const insets = useSafeAreaInsets()

    useFocusEffect(
      React.useCallback(() => {
        // 복약알림 설정 화면에서 시간을 추가한 후에 재진입 한 경우 selectedCreatedSlots 를 재구성 해줘야 rendering 에 문제없음
        if (selectedCreatedSlots.length != prescriptionDetail.selectedUserCreatedTimeSlots.length) {
          setSelectedCreatedSlots(prescriptionDetail.selectedUserCreatedTimeSlots)
        }

        sendLogScreenView({ screenClass: route.name, screenName: route.name })
      }, []),
    )

    useEffect(() => {
      if (prescriptionStore.timeSlots.length == 0) {
        prescriptionStore.fetchTimeSlots()
      }
    }, [])

    const onBackPress = () => {
      setPopupTitle("약 수정을 종료하시겠습니까?")
      setPopupMessage("진행 중인 프로세스를 종료 시 입력하신 정보가 삭제됩니다.")
      setPopupButton1(getPopupButton(navigation.goBack, "종료"))
      setPopupButton2(getPopupButton(() => setPopupVisible(false), "취소", "cancel"))
      setPopupVisible(true)
    }

    const save = async () => {
      prescriptionDetail.setPerDays(selectedCycle == MedicationCycle.EveryDay ? 1 : Number(perDays))

      if (mode == EditMedicationDetailMode.NewPrescription) {
        prescriptionStore.setNewPrescriptionDetail(prescriptionDetail)
        navigation.pop()
        return
      }

      const { environment } = prescriptionStore

      let params
      if (selectedEndDate == EndDate.DosingDays) {
        params = {
          dose: prescriptionDetail.dose,
          doses_num: prescriptionDetail.doses_num,
          per_days: prescriptionDetail.per_days,
          dosing_days_num: prescriptionDetail.dosing_days_num,
          start_at: prescriptionDetail.start_at.toJSON(),
          timeslots: prescriptionDetail.timeSlotIds,
        }
      } else {
        params = {
          dose: prescriptionDetail.dose,
          doses_num: prescriptionDetail.doses_num,
          per_days: prescriptionDetail.per_days,
          start_at: prescriptionDetail.start_at.toJSON(),
          end_at: prescriptionDetail.end_at.toJSON(),
          timeslots: prescriptionDetail.timeSlotIds,
        }
      }

      let result
      if (mode == EditMedicationDetailMode.NewPrescriptionDetail) {
        params = {
          ...params,
          drug_id: prescriptionDetail.drug.id,
          prescription: prescriptionDetail.prescription,
        }
        result = await api.postPrescriptionDetail(params)
      } else {
        result = await api.patchPrescriptionDetail(prescriptionDetail.id, params)
      }

      if (result.kind === "ok") {
        prescriptionStore.savePrescriptionDetail(result.data, prescriptionDetailIdx)
        const { user, selectedMemberKey } = accountStore
        if (selectedMemberKey.includes("u")) {
          prescriptionStore.fetchPrescriptions(user.id, selectedMemberKey)
        } else if (selectedMemberKey.includes("f")) {
          prescriptionStore.fetchPrescriptions(
            Number(selectedMemberKey.substr(2)),
            selectedMemberKey,
          )
        } else {
          prescriptionStore.fetchPrescriptions(
            user.id,
            selectedMemberKey,
            Number(selectedMemberKey.substr(2)),
          )
        }

        setPopupTitle("수정하신 내용이 저장되었습니다.")
        setPopupMessage(undefined)
        setPopupButton1(getPopupButton(navigation.goBack, "확인"))
        setPopupButton2(null)
        setPopupVisible(true)
        amplitude
          .getInstance()
          .logEvent("medication_detail_modified", { id: prescriptionDetail.id, ...params })
        // prescriptionStore.setPrescription(clone(editingPrescription))
      } else {
        Toast.show({
          id: 0,
          title: (
            <Text
              style={styles.TOAST_TEXT}
            >{`서버와 통신 중 오류가 발생하였습니다. \n나중에 다시 시도해 주세요.`}</Text>
          ),
          duration: 2000,
          placement: "top",
          style: styles.TOAST_VIEW,
        })
      }
    }

    useLayoutEffect(() => {
      navigation.setOptions({
        // eslint-disable-next-line react/display-name
        headerLeft: () => (
          <TouchableItem onPress={onBackPress} style={HEADER_LEFT}>
            <CustomHeaderBackImage />
          </TouchableItem>
        ),
        headerTitle:
          mode == EditMedicationDetailMode.NewPrescriptionDetail ||
          mode == EditMedicationDetailMode.NewPrescription
            ? "약 추가"
            : "약 수정",
        // 이 저장 버튼으로 save 시 변경된 selectedEndDate 값으로 처리되지 않는 이슈가 있어 주석 처리함.
        // headerRight: () => (
        //   <TouchableOpacity onPress={save} style={{ marginRight: 24 }}>
        //     <Text style={SAVE_TEXT}>저장</Text>
        //   </TouchableOpacity>
        // ),
      })
    }, [navigation])

    const onWakeUpPress = () => {
      prescriptionDetail.updateTimeSlot(TimeSlotName.WakeUp, !selectedWakeUp)
      setSelectedWakeUp(!selectedWakeUp)
      setSelectedOnce(false)
    }
    const onMorningPress = () => {
      prescriptionDetail.updateTimeSlot(TimeSlotName.Morning, !selectedMorning)
      setSelectedMorning(!selectedMorning)
      setSelectedOnce(false)
    }
    const onAfternoonPress = () => {
      prescriptionDetail.updateTimeSlot(TimeSlotName.Afternoon, !selectedAfternoon)
      setSelectedAfternoon(!selectedAfternoon)
      setSelectedOnce(false)
    }
    const onEveningPress = () => {
      prescriptionDetail.updateTimeSlot(TimeSlotName.Evening, !selectedEvening)
      setSelectedEvening(!selectedEvening)
      setSelectedOnce(false)
    }
    const onNightPress = () => {
      prescriptionDetail.updateTimeSlot(TimeSlotName.Night, !selectedNight)
      setSelectedNight(!selectedNight)
      setSelectedOnce(false)
    }
    const onOncePress = () => {
      prescriptionDetail.updateTimeSlot(TimeSlotName.Once, !selectedOnce)
      setSelectedWakeUp(false)
      setSelectedMorning(false)
      setSelectedAfternoon(false)
      setSelectedEvening(false)
      setSelectedNight(false)
      setSelectedOnce(!selectedOnce)

      // 사용자가 추가한 시간들 모두 선택 해제
      const timeSlots = prescriptionDetail.selectedUserCreatedTimeSlots
      const newSelectedCreatedSlots: Array<{ selected: boolean; timeSlot: TimeSlot }> = []
      for (let i = 0; i < timeSlots.length; i++) {
        newSelectedCreatedSlots.push({ selected: false, timeSlot: timeSlots[i].timeSlot })
      }
      prescriptionDetail.updateCreatedTimeSlot(newSelectedCreatedSlots)
      setSelectedCreatedSlots(newSelectedCreatedSlots)
    }
    const onUserCreatedTimeSlotPress = (index: number) => {
      const newArray: Array<{ selected: boolean; timeSlot: TimeSlot }> = []
      for (let i = 0; i < selectedCreatedSlots.length; i++) {
        if (i == index) {
          newArray.push({
            selected: !selectedCreatedSlots[i].selected,
            timeSlot: selectedCreatedSlots[i].timeSlot,
          })
        } else {
          newArray.push(selectedCreatedSlots[i])
        }
      }
      prescriptionDetail.updateCreatedTimeSlot(newArray)
      setSelectedCreatedSlots(newArray)
      setSelectedOnce(false)
    }

    const onEndDatePress = (selectedOption: EndDate) => {
      if (selectedOption == EndDate.None) {
        // '종료일 없음' 은 10년 후로 설정하기로 협의함.
        prescriptionDetail.setEndDate(
          prescriptionDetail.start_at.plus({ years: 10 }).toFormat("yyyy-MM-dd"),
        )
      }
      setSelectedEndDate(selectedOption)
    }

    const onChangeDosingDays = (text: string) => {
      setDosingDays(text)
      const dosingDays = Number(text)
      prescriptionDetail.setEndDate(
        prescriptionDetail.start_at.plus({ days: dosingDays - 1 }).toFormat("yyyy-MM-dd"),
      )
      prescriptionDetail.setDosingDaysNum(dosingDays)
    }

    const onChangeDose = (text: string) => {
      setDose(text)
      prescriptionDetail.setDose(Number(text))
    }

    const addTimeSlot = () => navigation.navigate("TimeSlotSettings")

    const renderBottomButtons = () => {
      if (inputFocused) {
        return null
      }
      return (
        <View style={{ ...BUTTONS_CONTAINER, paddingBottom: insets.bottom + 12 }}>
          {stepIndex != 0 ? (
            <TouchableItem onPress={() => carouselRef.current.snapToPrev()} style={BACK_BUTTON}>
              <Text style={BACK_BUTTON_TEXT}>이전</Text>
            </TouchableItem>
          ) : (
            <View style={{ flex: 1, marginHorizontal: 4 }} />
          )}
          <TouchableItem onPress={debounce(next, 300)} style={NEXT_BUTTON}>
            <Text style={NEXT_BUTTON_TEXT}>{stepIndex != 4 ? "다음" : "확인"}</Text>
          </TouchableItem>
        </View>
      )
    }

    const renderMedicationCycle = () => {
      return (
        <View style={{ paddingHorizontal: 8 }}>
          <Text style={STEP_QUESTION}>얼마나 자주 복약하나요?</Text>
          {isNewPrescription ? null : (
            <View style={INFO_BOX}>
              <SvgInfoDark />
              <Text style={INFO_TEXT}>복약 주기를 변경할 경우 복약기록이 초기화됩니다.</Text>
            </View>
          )}
          <View style={{ flexDirection: "row", marginBottom: 16 }}>
            <TouchableItem
              style={
                selectedCycle == MedicationCycle.EveryDay ? SELECT_BUTTON_SELECTED : SELECT_BUTTON
              }
              onPress={() => setSelectedCycle(MedicationCycle.EveryDay)}
            >
              <Text
                style={
                  selectedCycle == MedicationCycle.EveryDay
                    ? SELECT_BUTTON_TEXT_SELECTED
                    : SELECT_BUTTON_TEXT
                }
              >
                매일
              </Text>
            </TouchableItem>
            <TouchableItem
              style={
                selectedCycle == MedicationCycle.SpecificDaysInterval
                  ? SELECT_BUTTON_SELECTED
                  : SELECT_BUTTON
              }
              onPress={() => setSelectedCycle(MedicationCycle.SpecificDaysInterval)}
            >
              <Text
                style={
                  selectedCycle == MedicationCycle.SpecificDaysInterval
                    ? SELECT_BUTTON_TEXT_SELECTED
                    : SELECT_BUTTON_TEXT
                }
              >
                특정 일수 간격
              </Text>
            </TouchableItem>
          </View>
          {/* <View style={ROW}>
            <TouchableItem
              style={selectedCycle == MedicationCycle.PRN ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={() => setSelectedCycle(MedicationCycle.PRN)}
            >
              <Text
                style={
                  selectedCycle == MedicationCycle.PRN
                    ? SELECT_BUTTON_TEXT_SELECTED
                    : SELECT_BUTTON_TEXT
                }
              >
                필요시 복용*
              </Text>
            </TouchableItem>
            <View style={ROW_RIGHT} />
          </View> */}
          {selectedCycle == MedicationCycle.SpecificDaysInterval ? (
            <View style={{ marginTop: 48 }}>
              <Text style={STEP_QUESTION}>며칠마다 복약하나요?</Text>
              <View style={INPUT_ROW}>
                <Input
                  placeholder="간격 일수"
                  placeholderTextColor={color.placeholder}
                  keyboardType={"decimal-pad"}
                  onChangeText={(text) => setPerDays(text)}
                  onFocus={() => setInputFocused(true)}
                  onBlur={() => setInputFocused(false)}
                  // onSubmitEditing={() => validateInterval()}
                  value={perDays}
                  returnKeyType={"next"}
                  style={INPUT}
                />
                <View style={ROW_RIGHT}>
                  <Text style={typography.body}>일마다</Text>
                </View>
              </View>
            </View>
          ) : null}
        </View>
      )
    }

    const renderStartDate = () => {
      return (
        <View style={{ paddingHorizontal: 8, paddingBottom: 70 }}>
          <Text style={STEP_QUESTION}>언제 복약을 시작하나요?</Text>
          {isNewPrescription ? null : (
            <View style={INFO_BOX}>
              <SvgInfoDark />
              <Text style={INFO_TEXT}>시작일을 변경할 경우 복약기록이 초기화됩니다.</Text>
            </View>
          )}

          <View style={SELECT_BUTTONS_ROW}>
            <TouchableItem
              style={selectedStartDate == StartDate.Today ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={() => {
                setSelectedStartDate(StartDate.Today)
                prescriptionDetail.setStartDate(DateTime.local().toFormat("yyyy-MM-dd"))
              }}
            >
              <Text
                style={
                  selectedStartDate == StartDate.Today
                    ? SELECT_BUTTON_TEXT_SELECTED
                    : SELECT_BUTTON_TEXT
                }
              >
                오늘
              </Text>
            </TouchableItem>
            <TouchableItem
              style={
                selectedStartDate == StartDate.SpecificDate ? SELECT_BUTTON_SELECTED : SELECT_BUTTON
              }
              onPress={() => setSelectedStartDate(StartDate.SpecificDate)}
            >
              <Text
                style={
                  selectedStartDate == StartDate.SpecificDate
                    ? SELECT_BUTTON_TEXT_SELECTED
                    : SELECT_BUTTON_TEXT
                }
              >
                특정 날짜
              </Text>
            </TouchableItem>
          </View>
          {selectedStartDate == StartDate.SpecificDate ? (
            // @ts-ignore
            <Calendar
              // Initially visible month. Default = Date()
              current={startDate}
              calendarHeight={360 + 35 * 4}
              onDayPress={(day) => {
                prescriptionDetail.setStartDate(day.dateString)
                setStartDate(day.dateString)
              }}
              onDayLongPress={(day) => {
                prescriptionDetail.setStartDate(day.dateString)
                setStartDate(day.dateString)
              }}
              // Handler which gets executed when press arrow icon left. It receive a callback can go back month
              onPressArrowLeft={(subtractMonth) => {
                setStartDate(
                  DateTime.fromSQL(startDate).minus({ months: 1 }).toFormat("yyyy-MM-dd"),
                )
                subtractMonth()
              }}
              // Handler which gets executed when press arrow icon right. It receive a callback can go next month
              onPressArrowRight={(addMonth) => {
                setStartDate(DateTime.fromSQL(startDate).plus({ months: 1 }).toFormat("yyyy-MM-dd"))
                addMonth()
              }}
              markedDates={{
                [prescriptionDetail.start_at.toFormat("yyyy-MM-dd")]: {
                  selected: true,
                  disableTouchEvent: true,
                },
              }}
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat={"yyyy년 MM월"}
              renderArrow={(direction) => {
                if (direction == "left")
                  return (
                    <SvgArrowDownBlack
                      width={24}
                      height={24}
                      style={{
                        transform: [{ rotate: "90deg" }],
                      }}
                    />
                  )
                if (direction == "right")
                  return (
                    <SvgArrowDownBlack
                      width={24}
                      height={24}
                      style={{
                        transform: [{ rotate: "-90deg" }],
                      }}
                    />
                  )
                return <View style={{ width: 24 }} />
              }}
              hideExtraDays={true}
              // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
              // day from another month that is visible in calendar page. Default = false
              disableMonthChange={true}
              theme={CALENDAR_THEME}
            />
          ) : null}
          <View style={START_END_DATE_BOX}>
            <Text style={START_END_DATE_SUBJECT}>시작일</Text>
            <Text style={START_END_DATE_TEXT}>
              {prescriptionDetail.start_at.toFormat("yyyy년 M월 d일")}
            </Text>
          </View>
        </View>
      )
    }

    const renderEndDate = () => {
      return (
        <View style={{ paddingHorizontal: 8, paddingBottom: 70 }}>
          <Text style={STEP_QUESTION}>언제 복약을 종료하나요?</Text>
          {isNewPrescription ? null : (
            <View style={INFO_BOX}>
              <SvgInfoDark />
              <Text style={INFO_TEXT}>종료일을 변경할 경우 복약기록이 초기화됩니다.</Text>
            </View>
          )}

          <View style={SELECT_BUTTONS_ROW}>
            <TouchableItem
              style={selectedEndDate == EndDate.None ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={() => onEndDatePress(EndDate.None)}
            >
              <Text
                style={
                  selectedEndDate == EndDate.None ? SELECT_BUTTON_TEXT_SELECTED : SELECT_BUTTON_TEXT
                }
              >
                종료일 없음
              </Text>
            </TouchableItem>
            <TouchableItem
              style={selectedEndDate == EndDate.DosingDays ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={() => onEndDatePress(EndDate.DosingDays)}
            >
              <Text
                style={
                  selectedEndDate == EndDate.DosingDays
                    ? SELECT_BUTTON_TEXT_SELECTED
                    : SELECT_BUTTON_TEXT
                }
              >
                총 복약 일수 입력
              </Text>
            </TouchableItem>
          </View>
          <View style={ROW}>
            <TouchableItem
              style={
                selectedEndDate == EndDate.SpecificDate ? SELECT_BUTTON_SELECTED : SELECT_BUTTON
              }
              onPress={() => onEndDatePress(EndDate.SpecificDate)}
            >
              <Text
                style={
                  selectedEndDate == EndDate.SpecificDate
                    ? SELECT_BUTTON_TEXT_SELECTED
                    : SELECT_BUTTON_TEXT
                }
              >
                특정 날짜
              </Text>
            </TouchableItem>
            <View style={ROW_RIGHT} />
          </View>
          {selectedEndDate == EndDate.DosingDays ? (
            <View style={{ marginTop: 48 }}>
              <Text style={STEP_QUESTION}>총 복약 일수를 입력하세요.</Text>
              <View style={INPUT_ROW}>
                <Input
                  placeholder="총 복약 일수"
                  placeholderTextColor={color.placeholder}
                  keyboardType={"decimal-pad"}
                  onChangeText={onChangeDosingDays}
                  onFocus={() => setInputFocused(true)}
                  onBlur={() => setInputFocused(false)}
                  value={dosingDays}
                  returnKeyType={"next"}
                  style={INPUT}
                />
                <View style={ROW_RIGHT}>
                  <Text style={typography.body}>일</Text>
                </View>
              </View>
            </View>
          ) : selectedEndDate == EndDate.SpecificDate ? (
            // @ts-ignore
            <Calendar
              // Initially visible month. Default = Date()
              current={endDate}
              calendarHeight={360 + 35 * 4}
              minDate={prescriptionDetail.start_at.toFormat("yyyy-MM-dd")}
              onDayPress={(day) => {
                prescriptionDetail.setEndDate(day.dateString)
                setEndDate(day.dateString)
              }}
              onDayLongPress={(day) => {
                prescriptionDetail.setEndDate(day.dateString)
                setEndDate(day.dateString)
              }}
              // Handler which gets executed when press arrow icon left. It receive a callback can go back month
              onPressArrowLeft={(subtractMonth) => {
                setEndDate(DateTime.fromSQL(endDate).minus({ months: 1 }).toFormat("yyyy-MM-dd"))
                subtractMonth()
              }}
              // Handler which gets executed when press arrow icon right. It receive a callback can go next month
              onPressArrowRight={(addMonth) => {
                setEndDate(DateTime.fromSQL(endDate).plus({ months: 1 }).toFormat("yyyy-MM-dd"))
                addMonth()
              }}
              markedDates={{
                [prescriptionDetail.end_at.toFormat("yyyy-MM-dd")]: {
                  selected: true,
                  disableTouchEvent: true,
                },
              }}
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat={"yyyy년 MM월"}
              renderArrow={(direction) => {
                if (direction == "left")
                  return (
                    <SvgArrowDownBlack
                      width={24}
                      height={24}
                      style={{
                        transform: [{ rotate: "90deg" }],
                      }}
                    />
                  )
                if (direction == "right")
                  return (
                    <SvgArrowDownBlack
                      width={24}
                      height={24}
                      style={{
                        transform: [{ rotate: "-90deg" }],
                      }}
                    />
                  )
                return <View style={{ width: 24 }} />
              }}
              hideExtraDays={true}
              // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
              // day from another month that is visible in calendar page. Default = false
              disableMonthChange={true}
              theme={CALENDAR_THEME}
            />
          ) : null}
          <View style={START_END_DATE_BOX}>
            <Text style={START_END_DATE_SUBJECT}>종료일</Text>
            <Text style={START_END_DATE_TEXT}>
              {selectedEndDate == EndDate.None
                ? "종료일 없음"
                : prescriptionDetail.end_at.toFormat("yyyy년 M월 d일")}
            </Text>
          </View>
        </View>
      )
    }

    const renderUserCreatedTimeslots = () => {
      const length = prescriptionStore.userCreatedTimeSlots.length

      return prescriptionStore.userCreatedTimeSlots.map((timeSlot, index, timeSlots) => {
        if (selectedCreatedSlots.length != length) return null // 에러 발생 가능성 차단

        if (index % 2 == 0) {
          return (
            <View key={index} style={SELECT_BUTTONS_ROW}>
              <TouchableItem
                style={
                  selectedCreatedSlots[index].selected ? SELECT_BUTTON_SELECTED : SELECT_BUTTON
                }
                onPress={() => onUserCreatedTimeSlotPress(index)}
              >
                <Text
                  style={
                    selectedCreatedSlots[index].selected
                      ? SELECT_BUTTON_TEXT_SELECTED
                      : SELECT_BUTTON_TEXT
                  }
                >
                  {formatTime(timeSlot.when)}
                </Text>
              </TouchableItem>
              {length > index + 1 ? (
                <TouchableItem
                  style={
                    selectedCreatedSlots[index + 1].selected
                      ? SELECT_BUTTON_SELECTED
                      : SELECT_BUTTON
                  }
                  onPress={() => onUserCreatedTimeSlotPress(index + 1)}
                >
                  <Text
                    style={
                      selectedCreatedSlots[index + 1].selected
                        ? SELECT_BUTTON_TEXT_SELECTED
                        : SELECT_BUTTON_TEXT
                    }
                  >
                    {formatTime(timeSlots[index + 1].when)}
                  </Text>
                </TouchableItem>
              ) : (
                <TouchableItem style={SELECT_BUTTON} onPress={addTimeSlot}>
                  <Text style={SELECT_BUTTON_TEXT}>+ 시간 추가</Text>
                </TouchableItem>
              )}
            </View>
          )
        } else if (length == index + 1) {
          return (
            <View style={SELECT_BUTTONS_ROW}>
              <TouchableItem style={SELECT_BUTTON} onPress={addTimeSlot}>
                <Text style={SELECT_BUTTON_TEXT}>+ 시간 추가</Text>
              </TouchableItem>
              <View style={ROW_RIGHT} />
            </View>
          )
        } else {
          return null
        }
      })
    }

    const renderMedicationTime = () => {
      // const timeSlots = prescriptionDetail.timeslots
      return (
        <View style={{ paddingHorizontal: 8, paddingBottom: 56 }}>
          <Text style={STEP_QUESTION}>하루 중 언제 복약하나요?</Text>
          {isNewPrescription ? null : (
            <View style={INFO_BOX}>
              <SvgInfoDark />
              <Text style={INFO_TEXT}>복약 시간을 변경할 경우 복약기록이 초기화됩니다.</Text>
            </View>
          )}

          <View style={SELECT_BUTTONS_ROW}>
            <TouchableItem
              style={selectedWakeUp ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={onWakeUpPress}
            >
              <Text style={selectedWakeUp ? SELECT_BUTTON_TEXT_SELECTED : SELECT_BUTTON_TEXT}>
                기상직후 ({getTimeText(TimeSlotName.WakeUp)})
              </Text>
            </TouchableItem>
            <TouchableItem
              style={selectedMorning ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={onMorningPress}
            >
              <Text style={selectedMorning ? SELECT_BUTTON_TEXT_SELECTED : SELECT_BUTTON_TEXT}>
                아침 ({getTimeText(TimeSlotName.Morning)})
              </Text>
            </TouchableItem>
          </View>
          <View style={SELECT_BUTTONS_ROW}>
            <TouchableItem
              style={selectedAfternoon ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={onAfternoonPress}
            >
              <Text style={selectedAfternoon ? SELECT_BUTTON_TEXT_SELECTED : SELECT_BUTTON_TEXT}>
                점심 ({getTimeText(TimeSlotName.Afternoon)})
              </Text>
            </TouchableItem>
            <TouchableItem
              style={selectedEvening ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={onEveningPress}
            >
              <Text style={selectedEvening ? SELECT_BUTTON_TEXT_SELECTED : SELECT_BUTTON_TEXT}>
                저녁 ({getTimeText(TimeSlotName.Evening)})
              </Text>
            </TouchableItem>
          </View>
          <View style={SELECT_BUTTONS_ROW}>
            <TouchableItem
              style={selectedNight ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={onNightPress}
            >
              <Text style={selectedNight ? SELECT_BUTTON_TEXT_SELECTED : SELECT_BUTTON_TEXT}>
                취침전 ({getTimeText(TimeSlotName.Night)})
              </Text>
            </TouchableItem>
            <TouchableItem
              style={selectedOnce ? SELECT_BUTTON_SELECTED : SELECT_BUTTON}
              onPress={onOncePress}
            >
              <Text style={selectedOnce ? SELECT_BUTTON_TEXT_SELECTED : SELECT_BUTTON_TEXT}>
                아무때나 한번
              </Text>
            </TouchableItem>
          </View>
          {renderUserCreatedTimeslots()}
        </View>
      )
    }

    const renderDosage = () => {
      return (
        <View style={{ paddingHorizontal: 8 }}>
          <Text style={STEP_QUESTION}>한번에 얼마나 복약하나요?</Text>
          <View style={INPUT_ROW}>
            <Input
              variant="unstyled"
              placeholder="1회 복약량"
              placeholderTextColor={color.placeholder}
              keyboardType={"decimal-pad"}
              onChangeText={onChangeDose}
              onFocus={() => setInputFocused(true)}
              onBlur={() => setInputFocused(false)}
              value={dose}
              returnKeyType={"next"}
              style={INPUT}
            />
            <View style={ROW_RIGHT}>
              <Text style={typography.body}>{prescriptionDetail.drug.unitConverted}</Text>
            </View>
          </View>
        </View>
      )
    }

    const renderItem = ({ item, index }: { item: any; index: number }) => {
      switch (index) {
        case 0:
          return renderMedicationCycle()

        case 1:
          return renderStartDate()

        case 2:
          return renderEndDate()

        case 3:
          return renderMedicationTime()

        case 4:
          return renderDosage()

        default:
          break
      }
    }

    const validateInterval = () => {
      return perDays?.length > 0 && Number(perDays) > 0
    }
    const validateStartDate = () => {
      return prescriptionDetail.start_at != undefined || prescriptionDetail.start_at != null
    }
    const validateDosingDays = () => {
      return true
    }
    const validateEndDate = () => {
      return prescriptionDetail.end_at != undefined || prescriptionDetail.end_at != null
    }
    const validateTimeSlots = () => {
      let selectedCreatedSlot = false
      for (let i = 0; i < selectedCreatedSlots.length; i++) {
        const slot = selectedCreatedSlots[i]
        if (slot.selected) {
          selectedCreatedSlot = true
          break
        }
      }
      return (
        selectedWakeUp ||
        selectedMorning ||
        selectedAfternoon ||
        selectedEvening ||
        selectedNight ||
        selectedOnce ||
        selectedCreatedSlot
      )
    }
    const validateDose = () => {
      return prescriptionDetail.dose > 0
    }

    const next = () => {
      switch (stepIndex) {
        // 복약주기
        case 0:
        default:
          if (selectedCycle == MedicationCycle.SpecificDaysInterval && !validateInterval()) {
            Toast.show({
              id: 0,
              title: <Text style={styles.TOAST_TEXT}>{`간격 일수를 입력해 주세요.`}</Text>,
              duration: 2000,
              placement: "top",
              style: styles.TOAST_VIEW,
            })
            return
          }
          break

        // 시작일
        case 1:
          if (selectedStartDate == StartDate.SpecificDate && !validateStartDate()) {
            Toast.show({
              id: 0,
              title: <Text style={styles.TOAST_TEXT}>{`달력에서 시작일을 선택해 주세요.`}</Text>,
              duration: 2000,
              placement: "top",
              style: styles.TOAST_VIEW,
            })
            return
          }
          break

        // 종료일
        case 2:
          if (selectedEndDate == EndDate.DosingDays && !validateDosingDays()) {
          } else if (selectedEndDate == EndDate.SpecificDate && !validateEndDate()) {
            Toast.show({
              id: 0,
              title: <Text style={styles.TOAST_TEXT}>{`달력에서 종료일을 선택해 주세요.`}</Text>,
              duration: 2000,
              placement: "top",
              style: styles.TOAST_VIEW,
            })
            return
          }
          break

        // 복약 시간
        case 3:
          if (!validateTimeSlots()) {
            Toast.show({
              id: 0,
              title: <Text style={styles.TOAST_TEXT}>{`복약하실 시간을 선택해 주세요.`}</Text>,
              duration: 2000,
              placement: "top",
              style: styles.TOAST_VIEW,
            })
            return
          }
          break

        // 1회 복약량
        case 4:
          if (!validateDose()) {
            Toast.show({
              id: 0,
              title: <Text style={styles.TOAST_TEXT}>{`1회 복약량을 입력해 주세요.`}</Text>,
              duration: 2000,
              placement: "top",
              style: styles.TOAST_VIEW,
            })
            return
          } else if (mode == EditMedicationDetailMode.NewPrescription) {
            save()
          } else {
            setPopupTitle("변경 사항을 저장하시겠습니까?")
            setPopupMessage(
              <View style={{ paddingHorizontal: 12 }}>
                <Text style={SAVE_POPUP_TEXT}>
                  1회 복용량을 제외한{" "}
                  <Text style={SAVE_POPUP_BOLD_TEXT}>복약 주기, 복약 시간, 시작일, 종료일</Text>을
                  변경한 경우, 해당 약제의{" "}
                  <Text style={SAVE_POPUP_BOLD_TEXT}>복약 기록이 모두 지워집니다.</Text>
                </Text>
                <Text style={SAVE_POPUP_SUB_TEXT}>
                  * 1회 복용량만 변경한 경우에는 복약 기록이 지워지지 않아요~
                </Text>
              </View>,
            )
            setPopupButton1(getPopupButton(save, "저장"))
            setPopupButton2(getPopupButton(() => setPopupVisible(false), "취소", "cancel"))
            setPopupVisible(true)
            return
          }
      }
      carouselRef.current?.snapToNext()
    }

    const getStepTitle = () => {
      switch (stepIndex) {
        case 0:
        default:
          return "복약 주기"

        case 1:
          return "시작일"

        case 2:
          return "종료일"

        case 3:
          return "복약 시간"

        case 4:
          return "1회 복약량"
      }
    }

    return (
      <View style={ROOT}>
        <ScrollView>
          <View style={DRUG_INFO}>
            {prescriptionDetail.drug.image_url ? (
              <FastImage source={{ uri: prescriptionDetail.drug.image_url }} style={DRUG_IMAGE} />
            ) : (
              <SvgDrugEmptyImage width={42} height={24} style={{ alignSelf: "flex-start" }} />
            )}
            <Text style={DRUG_NAME} numberOfLines={1} ellipsizeMode="tail">
              {prescriptionDetail.drug.shortenedName}
            </Text>
          </View>
          <View style={DIVIDER} />
          <View style={STEP_TOP}>
            <Text style={STEP_TITLE}>{getStepTitle()}</Text>
            <Pagination
              containerStyle={{ paddingVertical: 0 }}
              dotsLength={5}
              activeDotIndex={stepIndex}
              // containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}
              dotStyle={{
                width: 16,
                height: 8,
                borderRadius: 4,
                marginHorizontal: -5,
                backgroundColor: color.primary,
              }}
              inactiveDotStyle={{
                width: 8,
                height: 8,
                borderRadius: 4,
                backgroundColor: `${color.primary}32`,
              }}
              inactiveDotOpacity={1}
              inactiveDotScale={1}
            />
          </View>
          <Carousel
            ref={carouselRef}
            // data={new Array(5)}
            data={[{ index: 0 }, { index: 1 }, { index: 2 }, { index: 3 }, { index: 4 }]}
            renderItem={renderItem}
            onSnapToItem={(index) => setStepIndex(index)}
            sliderWidth={viewportWidth}
            itemWidth={viewportWidth}
            activeSlideOffset={16}
            inactiveSlideScale={1.0}
            scrollEnabled={false}
          />
        </ScrollView>
        {renderBottomButtons()}
        <Popup
          isVisible={popupVisible}
          title={popupTitle}
          message={popupMessage}
          button1={popupButton1}
          button2={popupButton2}
        />
      </View>
    )
  },
)

export default withNavigationFocus(EditMedicationDetailScreen)
